
## Setting up a new daemon machine:

```sh
sudo adduser --uid 500 --disabled-password throbol
sudo mkdir /home/throbol/blobs
# maybe: link blobs to directory on a large disk
sudo chown -R throbol /home/throbol
```
