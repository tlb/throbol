# Throbol sheet file. See http://throbol.com.
# Format is YAML. It's reasonable to edit the front matter, or formulas below the `---` by hand.
# Note that YAML `#` comments are lost when loading & saving. Use `//` comments on formulas instead
version: 1
positions:
  comment: -2 14
  system.nr: -2 9
  crank.moi: -2 7
  stroke: -2 5
  nocrash: -2 2
  crank.angle: -2 -1
  crank.vel: -2 -2
  crank.accel: -2 -3
  displacer.bore: 5 15
  displacer.hot_len: 5 13
  displacer.cold_len: 5 11
  displacer.piston_len: 5 9
  exp.len: 5 7
  exp.piston_len: 5 5
  exp.bore: 5 3
  exp.pos: 5 1
  displacer.pos: 5 -0
  displacer.hot_vol: 5 -2
  displacer.cold_vol: 5 -3
  exp.vol: 5 -4
  displacer.cold_temp: 5 -6
  displacer.hot_temp: 5 -7
  exp.temp: 5 -8
  pressure: 5 -9
  carnot: 13 5
  drawing: 13 -0
ui:
  cursorPos:
    - 11.3884811
    - -2.32922482
    - 0
  visCursIndex: 110
  zoomScale: 1.38921404
  showRerunsWindow: false
  visMagnify: 1
  insets:
    R: ~
    L: ~
---
# Formulas
dt: 0.01
tick_count: 500
comment: |
  // Simulation of a Stirling engine
system.nr: 2.0~10
crank.moi: 2~5
stroke: 1~2
nocrash: |
  check(displacer.hot_len > -displacer.pos && displacer.cold_len > displacer.pos)
crank.angle: |
  initial ? 0.2 : crank.angle[-dt] + dt * crank.vel
crank.vel: |
  initial ? 0.0 : crank.vel[-dt] + dt * crank.accel
crank.accel: |
  pressure[-dt] * exp.bore^2*pi/4 * stroke/2*sin(crank.angle[-dt])/crank.moi
displacer.bore: 1.44~2
displacer.hot_len: 0.53~1
displacer.cold_len: 0.55~1
displacer.piston_len: 0.92~1
exp.len: 0.59~1
exp.piston_len: 0.1~1
exp.bore: 0.48~1
exp.pos: |
  stroke/2 * cos(crank.angle)
displacer.pos: |
  stroke/2 * sin(crank.angle)
displacer.hot_vol: |
  (displacer.hot_len + displacer.pos) * pi/4*displacer.bore^2
displacer.cold_vol: |
  (displacer.cold_len - displacer.pos) * pi/4*displacer.bore^2
exp.vol: |
  (exp.len - exp.pos) * exp.bore^2 * pi/4
displacer.cold_temp: 330~1000
displacer.hot_temp: 500~1000
exp.temp: 400~1000
pressure: |
  system.nr/(displacer.cold_vol/displacer.cold_temp + displacer.hot_vol/displacer.hot_temp + exp.vol/exp.temp)
carnot: |
  linePlot(exp.vol, pressure){label_x="V", label_y="P"}
drawing: |
  dc=[-1,-1]; ec=[1, 1]; cc=[1, -1];
  rect(ec + [0.0, exp.pos], [0, exp.piston_len/2], [exp.bore/2, 0]) |
  rect(ec + [0, exp.len/2 + exp.pos/2 + exp.piston_len/2], [exp.bore/2, 0], [0, (exp.len - exp.pos)/2]){color=#00cc00ff}  |
  rect(dc + [displacer.pos, 0.0], [displacer.piston_len/2,0], [0, displacer.bore/2]){color=#ccccccff}  |
  rect(dc + [-displacer.piston_len/2 - displacer.hot_len/2 + displacer.pos/2, 0], [(displacer.hot_len + displacer.pos)/2, 0], [0, displacer.bore/2+0.001]){color=#cc0000ff} |
  rect(dc + [+displacer.piston_len/2 + displacer.cold_len/2 + displacer.pos/2, 0], [(displacer.cold_len - displacer.pos)/2, 0], [0, displacer.bore/2+0.001]){color=#0000ccff} |
  line(cc + [displacer.pos, exp.pos], dc + [displacer.pos, 0]) |
  line(cc + [displacer.pos, exp.pos], ec + [0, exp.pos]) |
  line(cc + [0, 0], cc + [displacer.pos, exp.pos]){color=#ff8822ff, thickness=0.15} |
  spline([[-1.66,-0.32,0.000], [-0.92,1.12,0], [0.160,1.56,0], [1.0,1.60,0]]) |
  spline([[-0.26,-0.32,0.000], [-0.24,0.48,0], [0.200,1.40,0], [1.00,1.60,0]])
