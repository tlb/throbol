import cv2
import numpy as np
#import matplotlib.pyplot as plt
import numpy as np
import torch

path = "examples/elephant2.png"

image = cv2.imread(path)
image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
_, image = cv2.threshold(image, 64, 255, 0)

contours, dummy = cv2.findContours(image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
  
x = contours[0][:, 0, 0]*1.0
y = contours[0][:, 0, 1]*1.0
x -= np.mean(x)
y -= np.mean(y)
x *= (-10 / np.std(x))
y *= (-10 / np.std(y))

x = torch.tensor(x)
y = torch.tensor(y)

n_parameters = 24*2

dtype = torch.float; device=torch.device('cpu')

parameters = torch.randn((n_parameters//2, 2), device=device, dtype=dtype, requires_grad=True)

t = torch.linspace(0, 2*np.pi, len(x)).reshape(1, -1)
k = torch.tensor([i*sign for i in range(1, n_parameters//4+1) for sign in [-1, 1]]).reshape(-1, 1)
thetas = k * t

tc = torch.cos(thetas)
ts = torch.sin(thetas)

optimizer = torch.optim.RMSprop([parameters], lr=1e-2)
loss_fn = torch.nn.MSELoss(reduction='sum')

for iter in range(2000):

    # exp(i * theta) * (a + bi)
    # = (cos(theta) + i sin(theta)) * (a + bi)
    # = cos(theta) * a - sin(theta) * b + (sin(theta) * a + cos(theta) * b) * i

    ap = parameters[:, 0:1]
    bp = parameters[:, 1:2]
    xest = torch.sum(ap * tc - bp * ts, dim=0)
    yest = torch.sum(ap * ts + bp * tc, dim=0)

    loss = loss_fn(x, xest) + loss_fn(y, yest)
    if iter%100==0: print("loss", loss.item())

    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

terms = []
for parami in range(parameters.shape[0]):
    if k[parami].item() == 0: continue
    terms.append("  exp({0:+.0f}im*th) * ({1:.3f} + {2:.3f}im)".format(k[parami].item(), parameters[parami][0].item(), parameters[parami][1].item()))

print(',\n'.join(terms))

#plt.plot(xest.detach().numpy(), yest.detach().numpy())
#plt.show()
