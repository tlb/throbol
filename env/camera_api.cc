#include "../src/core.h"
#include "../src/vbmainloop.h"
#include "../src/emit.h"

#include "../remote/sim_camera_client.h"


struct CameraApi : SheetApi {
  
  string camType;
  bool live = false;
  shared_ptr<SimCameraClient> client;
  ApiRef<VideoFrame, API_MONITOR> vid{*this, "vid"};

  CameraApi(CompiledSheet &_compiled, ApiSpec const &_spec, string const &_camType)
    : SheetApi(_compiled, _spec, "camera.2"),
      camType(_camType)
  {
    compiled.hasLiveApi = true;
  }

  void stopLive() override;
  bool startLive() override;
  void rxMonitors(LiveRobotState &lrt) override;
  void txWarmup(LiveRobotState &lrt) override;

};


static ApiRegister regCamera1("camera.v4l", [](CompiledSheet &_compiled, ApiSpec const &_spec) {
  return make_shared<CameraApi>(_compiled, _spec, "v4l");
});
static ApiRegister regCamera2("camera.pylon", [](CompiledSheet &_compiled, ApiSpec const &_spec) {
  return make_shared<CameraApi>(_compiled, _spec, "pylon");
});
static ApiRegister regCamera3("camera.axis", [](CompiledSheet &_compiled, ApiSpec const &_spec) {
  return make_shared<CameraApi>(_compiled, _spec, "axis");
});


bool CameraApi::startLive()
{
  if (!SheetApi::startLive()) return false;
  if (vid.valid()) {
    if (!live) {
      live = true;
#if !defined(EMSCRIPTEN_NOTYET)
      if (!assoc_bool(apiProps, "disable", false)) {
        RemoteCameraConfig config;
        config.camName = apiInstance;
        config.cellName = apiInstance;
        config.camType = camType;
        config.spoolHost = assoc_string_view(apiProps, "spool_host", "");
        config.apiProps = apiProps;

        if (verbose) L() << "Create SimCameraClient " << config;
        client = make_shared<SimCameraClient>(sh, config);
        client->startCameraClient();
      }
#endif
    }
  }
  return true;
}

void CameraApi::stopLive()
{
  if (verbose) L() << label << ": stopLive. releasing cameras, traces";
#if !defined(EMSCRIPTEN_NOTYET)
  if (client) {
    if (verbose) L() << label << ": stopping camera";
    client->stop();
    client = nullptr;
  }
#endif
}

void CameraApi::txWarmup(LiveRobotState &lrt)
{
#if !defined(EMSCRIPTEN_NOTYET)
  if (client && client->videoFrames.empty()) {
    // Give up after 500 ticks and proceed without this camera
    if (lrt.warmupTicks < 500) {
      lrt.needMoreWarmup.push_back(label);
    }
  }
#endif
}

void CameraApi::rxMonitors(LiveRobotState &lrt)
{
#if !defined(EMSCRIPTEN_NOTYET)
  if (vid.valid() && client) {
    if (!client->videoFrames.empty()) {
      auto &vf = client->videoFrames.back();
      if (verbose >= 3) ll() << ": got " << vf;

      if (chunks) {
        chunks->insert(vf.blobRef.chunkRef());
      }

      vid.replayVal(lastData) = vf;
    }
  }
#endif
}
