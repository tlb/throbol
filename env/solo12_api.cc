#include "../src/api.h"
#include "../src/emit.h"
#include "../remote/cereal_box.h"
#include "../src/live_robot_ticker.h"

#include "master_board_sdk/defines.h"

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#if defined(__linux__)
#include <linux/if_arp.h>
#endif

std::ostream & operator << (std::ostream &s, Eigen::Matrix<double, 12, 1> const &a)
{
  s.precision(3);
  for (Index rowi = 0; rowi < a.rows(); rowi++) {
    s << ((rowi == 0) ? "[" : ", ");
    s << a(rowi);
  }
  s << "]";
  return s;
}


struct OdriJoint {
  
  SheetApi &owner;
  string name;
  int index;

  ApiSection section {owner, name + " joint"};
  ApiRef<F, API_SENSOR> pos {owner, name + ".pos"};
  ApiRef<F, API_SENSOR> vel {owner, name + ".vel"};
  ApiRef<F, API_SENSOR> torque {owner, name + ".torque"};
  ApiRef<F, API_ACTUATOR> goalpos {owner, name + ".goal_pos"};
  ApiRef<F, API_ACTUATOR> goalvel {owner, name + ".goal_vel"};
  ApiRef<F, API_ACTUATOR> goaltorque {owner, name + ".goal_torque"};
  ApiRef<F, API_ACTUATOR> stiffen {owner, name + ".stiffen"};
  ApiRef<F, API_ACTUATOR> dampen {owner, name + ".dampen"};

  OdriJoint(SheetApi &_owner, string const &_name, int _index)
    :owner(_owner),
     name(_name),
     index(_index)
  {
  }

};

struct Solo12Api : SheetApi {
  /*
    Joint ID	Joint Name	Motor Driver Card	Motor Port	Motor Index
    0	  FL_HAA	          0	                0	          0
    1	  FL_HFE	          1	                1	          3
    2	  FL_K	            1	                0	          2
    3	  FR_HAA	          0	                1	          1
    4	  FR_HFE	          2	                1	          5
    5	  FR_K	            2	                0	          4
    6	  HL_HAA	          3	                0	          6
    7	  HL_HFE	          4	                1	          9
    8	  HL_K	            4	                0	          8
    9	  HR_HAA	          3	                1	          7
    10	HR_HFE	          5	                1	          11
    11	HR_K	            5	                0	          10
  */
  OdriJoint fl_haa{*this, "fl.haa", 0};
  OdriJoint fl_hfe{*this, "fl.hfe", 1};
  OdriJoint fl_k{*this, "fl.k", 2};
  OdriJoint fr_haa{*this, "fr.haa", 3};
  OdriJoint fr_hfe{*this, "fr.hfe", 4};
  OdriJoint fr_k{*this, "fr.k", 5};
  OdriJoint hl_haa{*this, "hl.haa", 6};
  OdriJoint hl_hfe{*this, "hl.hfe", 7};
  OdriJoint hl_k{*this, "hl.k", 8};
  OdriJoint hr_haa{*this, "hr.haa", 9};
  OdriJoint hr_hfe{*this, "hr.hfe", 10};
  OdriJoint hr_k{*this, "hr.k", 11};

  std::array<OdriJoint *, 12> joints = {
    &fl_haa, &fl_hfe, &fl_k,
    &fr_haa, &fr_hfe, &fr_k,
    &hl_haa, &hl_hfe, &hl_k,
    &hr_haa, &hr_hfe, &hr_k
  };

  std::array<OdriJoint *, 12> jointByMotorIndex {
    &fl_haa, &fr_haa, &fl_k,
    &fl_hfe, &fr_k, &fr_hfe,
    &hl_haa, &hr_haa, &hl_k,
    &hl_hfe, &hr_k, &hr_hfe,
  };

  ApiSection imuSection {*this, "IMU on torso"};
  ApiRef<vec3, API_SENSOR> imuGyro {*this, "imu.gyro"};
  ApiRef<vec3, API_SENSOR> imuLinearAccel {*this, "imu.linear_accel"};
  ApiRef<vec3, API_SENSOR> imuAccel {*this, "imu.accel"};
  ApiRef<vec3, API_SENSOR> imuAttitude {*this, "imu.attitude"};
  ApiRef<mat3, API_SENSOR> imu_orientation {*this, "imu.orientation"};

  ApiSection globalSection {*this, "Global"};
  ApiRef<F, API_SENSOR> hwTime{*this, "hwtime"};
  ApiRef<F, API_ACTUATOR> wussify{*this, "wussify"};

  static constexpr int N_MOTORS = 12;

  Eigen::Vector<double, N_MOTORS> posScale;
  Eigen::Vector<double, N_MOTORS> velScale;
  Eigen::Vector<double, N_MOTORS> torqueScale;
  Eigen::Vector<double, N_MOTORS> kpScale;
  Eigen::Vector<double, N_MOTORS> kdScale;
  Eigen::Vector<double, N_MOTORS> isatScale;

#if !defined(__EMSCRIPTEN__)
  asio::generic::raw_protocol::socket rawSock{io_context};
  asio::generic::raw_protocol::endpoint rxRawSrc;
#endif
  string rawSockInterfaceName;
  string rxRawBuf;

  ack_packet_t lastAckPacket;
  sensor_packet_t lastSensorPacket;
  sensor_packet_t loggedSensorPacket;
  double lastAckPacketTime = 0.0, lastSensorPacketTime = 0.0;
  uint16_t session_id = 0;
  uint16_t command_index = 0;

  enum {
    S_rawIdle,
    S_rawAckWait,
    S_rawSensorWait,
    S_rawRunning,
    S_rawError,
  } rawState;

  Solo12Api(CompiledSheet &_compiled, ApiSpec const &_spec)
    : SheetApi(_compiled, _spec, "solo12.2")
  {
    compiled.hasLiveApi = true;
    memset(&lastAckPacket, 0, sizeof(lastAckPacket));
    memset(&lastSensorPacket, 0, sizeof(lastSensorPacket));
    memset(&loggedSensorPacket, 0, sizeof(loggedSensorPacket));
  }

  virtual ~Solo12Api();

  void txWarmup(LiveRobotState &lrt) override;
  void txActuators(CellVals *vals, LiveRobotState &lrt) override;
  void txCooldown(LiveRobotState &lrt) override;
  void rxSensors(LiveRobotState &lrt) override;

  void asyncRxRaw();
  void txRaw(void *data, size_t size);
  void txInit();
  void txCommand(CellVals *vals, F softstart);
  void setupScales();

  virtual bool startLive() override;
  void stopLive() override;

};

Solo12Api::~Solo12Api()
{
}

ostream & operator << (ostream &s, Eigen::Matrix<bool, Dynamic, 1> const &v)
{
  s << "[";
  for (Index i = 0; i < v.size(); i++) s << v(i);
  s << "]";
  return s;
}

ostream & operator << (ostream &s, Eigen::Matrix<int, Dynamic, 1> const &v)
{
  s << "[";
  for (Index i = 0; i < v.size(); i++) {
    if (i > 0) s << ", ";
    s << v(i);
  }
  s << "]";
  return s;
}

#if !defined(EMSCRIPTEN_NOTYET)

struct ethernet_packet_t {
  uint8_t dst_mac[6];
  uint8_t src_mac[6];
  uint16_t ethertype;
  uint16_t length;
  uint8_t payload[0];
} __attribute__((__packed__));

void Solo12Api::asyncRxRaw()
{
  rxRawBuf.resize(1024);
  rawSock.async_receive_from(asio::buffer(rxRawBuf.data(), rxRawBuf.size()),
    rxRawSrc,
    [this, keepalive=shared_from_this()](const boost::system::error_code error, size_t nr) {
      if (error) {
        if (error == asio::error::operation_aborted) return;
        ll() << "recv: " << error.message();
        return;
      }
      if (nr == 0) {
        if (verbose >= 2) ll() << "rx eth empty";
        return asyncRxRaw();
      }
      if (nr < sizeof(ethernet_packet_t)) {
        if (verbose >= 2) ll() << "rx eth short nr=" << nr;
        return asyncRxRaw();
      }

      auto epkt = reinterpret_cast<ethernet_packet_t *>(rxRawBuf.data());
      // Shouldn't I be byte-swapping these?
      if (verbose >= 3) ll() << "rx eth type=" << epkt->ethertype << " len=" << epkt->length;

      if (epkt->length == sizeof(ack_packet_t)) {

        // length is the only clue to what kind of packet this is :-(
        auto pkt = reinterpret_cast<ack_packet_t *>(epkt->payload);
        if (rawState == S_rawAckWait) {
          if (pkt->session_id == session_id) {
            if (verbose >= 1) {
              auto s = ll();
              s << "rx ack in ackWait: Correct session, spi_connected=";
              for (int i = 0; i < N_MOTORS/2; i++) {
                s << ((pkt->spi_connected & (1 << i)) ? "C" : "c");
              }
            }
            rawState = S_rawSensorWait;
            lastAckPacket = *pkt;
            lastAckPacketTime = realtime();
          }
          else {
            if (verbose >= 1) ll() << "rx ack in ackWait: Wrong session got " << pkt->session_id << " expected " << session_id;
          }
        }
      }
      else if (epkt->length == sizeof(sensor_packet_t)) {
        auto pkt = reinterpret_cast<sensor_packet_t *>(epkt->payload);
        if (rawState == S_rawSensorWait) {
          if (pkt->session_id == session_id) {
            if (verbose >= 1) ll() << "rx sensor in sensorWait: Correct session, now running";
            rawState = S_rawRunning;
            lastSensorPacket = *pkt;
            lastSensorPacketTime = realtime();
          }
          else {
            if (verbose >= 1) ll() << "rx sensor in sensorWait: Wrong session got " << pkt->session_id << " expected " << session_id;
          }
        }
        else if (rawState == S_rawRunning) {
          if (pkt->session_id == session_id) {
            lastSensorPacket = *pkt;
            lastSensorPacketTime = realtime();
          }
          else {
            if (verbose >= 1) ll() << "rx sensor in running: Wrong session. Got " << pkt->session_id << " expected " << session_id;
          }
        }
      }
      else {
        ll() << "rx raw nr=" << nr << " length=" << epkt->length << " not " << 
          sizeof(ack_packet_t) << " or " << sizeof(sensor_packet_t);
      }

      asyncRxRaw();
    });
}

void Solo12Api::txRaw(void *data, size_t size)
{
  auto pkt = reinterpret_cast<ethernet_packet_t *>(malloc(sizeof(ethernet_packet_t) + size));
  memcpy(pkt->payload, data, size);
  pkt->dst_mac[0] = 0xff;
  pkt->dst_mac[1] = 0xff;
  pkt->dst_mac[2] = 0xff;
  pkt->dst_mac[3] = 0xff;
  pkt->dst_mac[4] = 0xff;
  pkt->dst_mac[5] = 0xff;
  pkt->src_mac[0] = 0xa0;
  pkt->src_mac[1] = 0x1d;
  pkt->src_mac[2] = 0x48;
  pkt->src_mac[3] = 0x12;
  pkt->src_mac[4] = 0xa0;
  pkt->src_mac[5] = 0xc5;
  pkt->ethertype = 0xb588;
  pkt->length = size;
  rawSock.send(asio::buffer(pkt, sizeof(ethernet_packet_t) + size));
  free(pkt);
}

void Solo12Api::txInit()
{
  init_packet_t pkt;
  memset(&pkt, 0, sizeof(pkt));
  pkt.protocol_version = PROTOCOL_VERSION;
  pkt.session_id = session_id;
  txRaw(&pkt, sizeof(pkt));
}

void Solo12Api::txCommand(CellVals *vals, F softstart)
{
  command_packet_t pkt;
  memset(&pkt, 0, sizeof(pkt));
  pkt.session_id = session_id;

  for (int motori = 0; motori < N_MOTORS; motori++) {
    int boardi = motori / 2;
    int subi = motori % 2;
    auto joint = jointByMotorIndex[motori];

#ifndef DISABLE_ODRI_DRIVER_WORKAROUND
    // pending a fix to https://github.com/open-dynamic-robot-initiative/udriver_firmware/issues/13
    int timeout = 0; 
#else
    int timeout = 10;
#endif

    bool enable = true;

    auto &md = pkt.dual_motor_driver_command_packets[boardi];

    if (enable) {
      if (subi == 0) {
        md.mode |= UD_COMMAND_MODE_ES | UD_COMMAND_MODE_EM1;
      }
      else {
        md.mode |= UD_COMMAND_MODE_ES | UD_COMMAND_MODE_EM2;
      }
    }
    md.mode |= (UD_COMMAND_MODE_TIMEOUT & timeout);

    if (vals) {
      md.position_ref[subi] = FLOAT_TO_D32QN(posScale[motori] * double(joint->goalpos.memVal(vals)), UD_QN_POS);
      md.velocity_ref[subi] = FLOAT_TO_D16QN(velScale[motori] * double(joint->goalvel.memVal(vals)), UD_QN_VEL);
      md.current_ref[subi] = FLOAT_TO_D16QN(torqueScale[motori] * double(joint->goaltorque.memVal(vals) * softstart), UD_QN_IQ);

      F kp = exp(joint->stiffen.memVal(vals));
      F kd = exp(joint->stiffen.memVal(vals) + joint->dampen.memVal(vals));

      md.kp[subi] = FLOAT_TO_uD16QN(kpScale[motori] * double(kp * softstart), UD_QN_KP);
      md.kd[subi] = FLOAT_TO_uD16QN(kdScale[motori] * double(kd * softstart), UD_QN_KD);
      md.i_sat[subi] = FLOAT_TO_uD8QN(isatScale[motori] * double(softstart), UD_QN_ISAT);
    }
    if (verbose >= 3) ll() << joint->name << "=motor[" << motori << 
      "] pos=" << md.position_ref[subi] << "=" << double(md.position_ref[subi]) / (1<<UD_QN_POS) <<
      " vel=" << md.velocity_ref[subi] << "=" << double(md.velocity_ref[subi]) / (1<<UD_QN_VEL) <<
      " cur=" << md.current_ref[subi] << "=" << double(md.current_ref[subi]) / (1<<UD_QN_IQ) <<
      " kp=" << md.kp[subi] << "=" << double(md.kp[subi]) / (1 << UD_QN_KP) <<
      " kd=" << md.kd[subi] << "=" << double(md.kd[subi]) / (1 << UD_QN_KD) <<
      " i=" << int(md.i_sat[subi])  << "=" << double(md.i_sat[subi]) / (1 << UD_QN_ISAT);
  }
  pkt.command_index = command_index++;
  txRaw(&pkt, sizeof(pkt));
}

void Solo12Api::rxSensors(LiveRobotState &lrt)
{
  SheetApi::rxSensors(lrt);
  R now = realtime();
  if (now - lastSensorPacketTime > 0.1) {
    ll() << "Sensor packet too old";
    lrt.pleaseStopHard();
    return;
  }

  for (int motori = 0; motori < N_MOTORS; motori++) {
    int boardi = motori / 2;
    int subi = motori % 2;
    auto joint = jointByMotorIndex[motori];

    auto &ms = lastSensorPacket.dual_motor_driver_sensor_packets[boardi];

    joint->pos.replayVal(lastData) = double(ms.position[subi]) / (1 << UD_QN_POS)  / posScale[motori];
    joint->vel.replayVal(lastData) = double(ms.velocity[subi]) / (1 << UD_QN_VEL) / velScale[motori];
    joint->torque.replayVal(lastData) = double(ms.current[subi]) / (1 << UD_QN_IQ) / torqueScale[motori];
  }

  auto &accel = imuAccel.replayVal(lastData);
  for (int i = 0; i < 3; i ++) {
    accel[i] = 9.80665 * double(lastSensorPacket.imu.accelerometer[i]) / (1 << IMU_QN_ACC);
  }

  auto &gyro = imuGyro.replayVal(lastData);
  for (int i = 0; i < 3; i ++) {
    gyro[i] = double(lastSensorPacket.imu.gyroscope[i]) / (1 << IMU_QN_GYR);
  }
  
  auto &linear_accel = imuLinearAccel.replayVal(lastData);
  for (int i = 0; i < 3; i ++) {
    linear_accel[i] = double(lastSensorPacket.imu.linear_acceleration[i]) / (1 << IMU_QN_ACC);
  }

  auto &attitude = imuAttitude.replayVal(lastData);
  for (int i = 0; i < 3; i ++) {
    attitude[i] = double(lastSensorPacket.imu.attitude[i]) / (1 << IMU_QN_EF);
  }

  auto &orientation = imu_orientation.replayVal(lastData);
  orientation = Eigen::AngleAxisf(-attitude[2], vec3::UnitZ())
      * Eigen::AngleAxisf(attitude[1], vec3::UnitY())
      * Eigen::AngleAxisf(-attitude[0] + float(M_PI), vec3::UnitX());


  hwTime.replayVal(lastData) = lrt.hwTimeVal;

  bool statusChanged = false;
  for (int motori = 0; motori < N_MOTORS; motori += 2) {
    int boardi = motori / 2;
    auto &logms = loggedSensorPacket.dual_motor_driver_sensor_packets[boardi];
    auto &ms = lastSensorPacket.dual_motor_driver_sensor_packets[boardi];
    if ((logms.status ^ ms.status) & (
      UD_SENSOR_STATUS_SE | UD_SENSOR_STATUS_ERROR |
      UD_SENSOR_STATUS_M1E | UD_SENSOR_STATUS_IDX1D |
      UD_SENSOR_STATUS_M2E | UD_SENSOR_STATUS_IDX2D)) {
      statusChanged = true;
    }
  }
  if (statusChanged) {
    auto s = ll();
    s << "Board status:";
    vector<string> problems;
    for (int motori = 0; motori < N_MOTORS; motori += 2) {
      int boardi = motori / 2;
      auto &ms = lastSensorPacket.dual_motor_driver_sensor_packets[boardi];
      auto st = ms.status;

      s << " " << jointByMotorIndex[motori + 0]->name << "=";
      s << ((st & UD_SENSOR_STATUS_SE) ? "S" : "s");
      switch (st & UD_SENSOR_STATUS_ERROR) {
        case UD_SENSOR_STATUS_ERROR_NO_ERROR:
          s << ".";
        case UD_SENSOR_STATUS_ERROR_ENCODER1:
          problems.push_back(jointByMotorIndex[motori + 0]->name + " encoder error");
          s << "[ENC1]";
          break;
        case UD_SENSOR_STATUS_ERROR_SPI_RECV_TIMEOUT:
          problems.push_back(jointByMotorIndex[motori + 0]->name + "/" + jointByMotorIndex[motori + 1]->name + " SPI receive timeout");
          s << "[SPI]";
          break;
        case UD_SENSOR_STATUS_ERROR_CRIT_TEMP:
          problems.push_back(jointByMotorIndex[motori + 0]->name + "/" + jointByMotorIndex[motori + 1]->name + " critical temperature");
          s << "[TEMP]";
          break;
        case UD_SENSOR_STATUS_ERROR_POSCONV:
          problems.push_back(jointByMotorIndex[motori + 0]->name + "/" + jointByMotorIndex[motori + 1]->name + " position conversion");
          s << "[POS]";
          break;
        case UD_SENSOR_STATUS_ERROR_POS_ROLLOVER:
          problems.push_back(jointByMotorIndex[motori + 0]->name + "/" + jointByMotorIndex[motori + 1]->name + " position rollover");
          s << "[ROL]";
          break;
        case UD_SENSOR_STATUS_ERROR_ENCODER2:
          problems.push_back(jointByMotorIndex[motori + 1]->name + " encoder error");
          s << "[ENC2]";
          break;
        case UD_SENSOR_STATUS_ERROR_OTHER:
          problems.push_back(jointByMotorIndex[motori + 1]->name + " other error");
          s << "[OTH]";
          break;
        case UD_SENSOR_STATUS_CRC_ERROR:
          problems.push_back(jointByMotorIndex[motori + 0]->name + "/" + jointByMotorIndex[motori + 1]->name + " CRC error");
          s << "[CRC]";
          break;
        default:
          s << "[ERR" << int(st & UD_SENSOR_STATUS_ERROR) << "]";
      }

      s << ((st & UD_SENSOR_STATUS_M1E) ? "E" : "e");
      s << ((st & UD_SENSOR_STATUS_IDX1D) ? "D" : "d");
      //s << ((st & UD_SENSOR_STATUS_IDX1T) ? "T" : "t");

      s << " " << jointByMotorIndex[motori + 1]->name << "=";
      //s << ((st & UD_SENSOR_STATUS_SE) ? "S" : "s");
      s << ((st & UD_SENSOR_STATUS_M2E) ? "E" : "e");
      s << ((st & UD_SENSOR_STATUS_IDX2D) ? "D" : "d");
      //s << ((st & UD_SENSOR_STATUS_IDX2T) ? "T" : "t");
    }
    loggedSensorPacket = lastSensorPacket;
    for (auto &p : problems) {
      s << "\n  " << p;
    }
  }

}


void Solo12Api::txActuators(CellVals *vals, LiveRobotState &lrt)
{
  SheetApi::txActuators(vals, lrt);
  F softstart = float(min(1.0, lrt.hwTimeVal / 0.4));
  auto wussFactor = max(0.0f, min(2.0f, wussify.memVal(vals)));
  txCommand(vals, softstart * wussFactor);
}

void Solo12Api::txCooldown(LiveRobotState &lrt)
{

  command_packet_t pkt;
  memset(&pkt, 0, sizeof(pkt));
  pkt.session_id = session_id;

  for (int motori = 0; motori < N_MOTORS; motori++) {
    int boardi = motori / 2;
    int subi = motori % 2;
    auto &md = pkt.dual_motor_driver_command_packets[boardi];

    md.mode |= UD_COMMAND_MODE_ES | UD_COMMAND_MODE_EM1 | UD_COMMAND_MODE_EM2;
    md.mode |= (UD_COMMAND_MODE_TIMEOUT & 50);

    md.kd[subi] = FLOAT_TO_uD16QN(kdScale[motori] * 2.0, UD_QN_KD);
  }
  pkt.command_index = command_index++;
  txRaw(&pkt, sizeof(pkt));

  if (lrt.cooldownTicks < 500) {
    lrt.needMoreCooldown.push_back(label);
  }
}


void Solo12Api::setupScales()
{
  Eigen::Array<R, N_MOTORS, 1> gearRatios;
  gearRatios.setConstant(double(assoc_F(apiProps, "gear_ratio", 9.0f)));

  Eigen::Array<R, N_MOTORS, 1> motorConstants;
  motorConstants.setConstant(double(assoc_F(apiProps, "motor_constant", 0.025f)));

  Eigen::Array<R, N_MOTORS, 1> polarities;
  polarities.setConstant(1);
  polarities[3] = polarities[2] = polarities[1] = polarities[9] = polarities[8] = polarities[7] = -1;

  Eigen::Array<R, N_MOTORS, 1> base_kps;
  base_kps.setConstant(double(assoc_F(apiProps, "base_kps", 4.0f)));

  Eigen::Array<R, N_MOTORS, 1> base_kds;
  base_kds.setConstant(double(assoc_F(apiProps, "base_kds", 0.1f)));

  posScale = polarities * gearRatios / (2.0 * M_PI);
  velScale = polarities * gearRatios * 60.0 / (2000.0 * M_PI);
  torqueScale = polarities / (gearRatios * motorConstants);
  kpScale = M_2PI * base_kps / (gearRatios * gearRatios * motorConstants);
  kdScale = M_2PI * 1000.0 / 60.0 * base_kds / (gearRatios * gearRatios * motorConstants);

  isatScale.setConstant(double(assoc_F(apiProps, "base_isat", 15.0f)));

  ll() << "Scales: pos=" << posScale << " vel=" << velScale << " torque=" << torqueScale <<
    " kp=" << kpScale <<
    " kd=" << kdScale <<
    " isat=" << isatScale;
}


bool Solo12Api::startLive()
{
  if (!SheetApi::startLive()) return false;

  rawSockInterfaceName = "en0";
  if (auto it = assoc_string_view(apiProps, "raw_sock_interface")) {
    rawSockInterfaceName = *it;
  }

  setupScales();

  session_id = random() & 0xffff;
  command_index = 1;

  rawState = S_rawAckWait;

#if defined(__linux__)
  boost::system::error_code openError;
  rawSock.open(asio::generic::raw_protocol(PF_PACKET, SOCK_RAW), openError);
  if (openError) {
    ll() << "open rawSock: " << openError.message();
    return false;
  }

  {
    struct ifreq ifReq;
    memset(&ifReq, 0, sizeof(ifReq));
    strncpy(ifReq.ifr_name, rawSockInterfaceName.c_str(), IFNAMSIZ);

    if (ioctl(rawSock.native_handle(), SIOCGIFINDEX, &ifReq) < 0) {
      ll() << "Looking up interface " << rawSockInterfaceName << " : " << strerror(errno);
      return false;
    }
    if (verbose >= 1) ll() << "from " << rawSockInterfaceName << " found ifindex=" << ifReq.ifr_ifindex;

    struct sockaddr_ll dstAddr;
    memset(&dstAddr, 0, sizeof(dstAddr));

    dstAddr.sll_family = PF_PACKET;
	  dstAddr.sll_protocol = htons(ETH_P_ALL);
	  dstAddr.sll_ifindex = ifReq.ifr_ifindex;
    
    boost::system::error_code bindError;
    asio::generic::raw_protocol::endpoint ep((void *)&dstAddr, sizeof(dstAddr), htons(ETH_P_ALL));
    rawSock.bind(ep, bindError);
    if (bindError) {
      ll() << "bind rawSock: " << bindError.message();
      return false;
    }
    if (verbose >= 1) ll() << "Listening on " << rawSockInterfaceName << " verbose=" << verbose;
  }
  // WRITEME: #elif defined(__APPLE__)
#else
  ll() << "raw socket connections not implemented for this OS";
  return false;
#endif

  asyncRxRaw();

  return true;
}

void Solo12Api::txWarmup(LiveRobotState &lrt)
{
  if (rawState == S_rawAckWait) {
    if (lrt.warmupTicks > 500) {
      ll() << "No ack back from masterboard after 500 ticks";
      lrt.pleaseStopHard();
      return;
    }
    txInit();
    lrt.needMoreWarmup.push_back(label + " masterboard ack");
  }
  else if (rawState == S_rawSensorWait) {
    txCommand(nullptr, 0.0);
    lrt.needMoreWarmup.push_back(label + " sensors");
  }
  else if (rawState == S_rawRunning) {
    // waiting for another api, so keep sending null commands
    txCommand(nullptr, 0.0);
  }
}

void Solo12Api::stopLive()
{
  SheetApi::stopLive();
  rawSock.close();
}

#else

void Solo12Api::stopLive()
{
  SheetApi::stopLive();
}

bool Solo12Api::startLive()
{
  return false;
}

void Solo12Api::txWarmup(LiveRobotState &lrt)
{
}

void Solo12Api::txActuators(CellVals *vals, LiveRobotState &lrt)
{
}

void Solo12Api::txCooldown(LiveRobotState &lrt)
{
}

void Solo12Api::rxSensors(LiveRobotState &lrt)
{
}

#endif



static ApiRegister regSolo12("solo12", [](CompiledSheet &_compiled, ApiSpec const &_spec) {
  return make_shared<Solo12Api>(_compiled, _spec);
});

