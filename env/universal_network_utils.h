#pragma once
#include "./univarm.h"

struct UniversalRobotEngine;

static inline void forEachLine(std::basic_string<u_char> &rxBuf, std::function<void(string const &s)> onLine)
{
  auto curIt = rxBuf.begin();
  auto endIt = rxBuf.end();
  while (curIt < endIt) {
    auto eolIt = find(curIt, endIt, (u_char)10);
    if (eolIt == endIt) break;
    auto endContent = eolIt;
    if (endContent > curIt && endContent[-1] == 13) endContent--;
    onLine(string(curIt, endContent));
    curIt = eolIt + 1;
  }
  rxBuf.erase(rxBuf.begin(), curIt);
}

/*
  An RTDE packet is:
    2 byte big-endian length (including this header)
    1 byte type
    length-3 bytes data
*/
static inline void forEachRtdePacket(std::basic_string<u_char> &rxBuf, std::function<void(U8 cmd, packet &rx)> onPacket)
{
  auto curIt = rxBuf.begin();
  auto endIt = rxBuf.end();
  while (curIt + 3 <= endIt) {
    auto pktSize = ((U32)curIt[0] << 8) | ((U32)curIt[1] << 0);
    auto cmd = (U8)curIt[2];

    if (curIt + pktSize > endIt) break;

    packet rx(&curIt[3], pktSize - 3);
    onPacket(cmd, rx);
    curIt += pktSize;
  }
  rxBuf.erase(rxBuf.begin(), curIt);
}

/*
  A control packet is:
    4 byte big-endian length
    length-4 bytes data
*/
static inline void forEachControlPacket(string &rxBuf, std::function<void(packet &rx)> onPacket)
{
  auto curIt = rxBuf.begin();
  auto endIt = rxBuf.end();
  while (curIt + 4 <= endIt) {
    auto pktSize = (((U32)curIt[0] & 0xff) << 24) | (((U32)curIt[1] & 0xff) << 16) | (((U32)curIt[2] & 0xff) << 8) | (((U32)curIt[3] & 0xff) << 0);
    if (pktSize > 10000) {
      L() << "forEachControlPacket: silly packet size " << pktSize << ", probable sync loss";
      abort();
    }

    if (curIt + pktSize > endIt) break;

    packet rx((u_char *)&curIt[4], pktSize - 4);
    try {
      onPacket(rx);
    }
    catch (std::exception const &ex) {
      L() << "forEachControlPacket: caught exception in onPacket: " << ex.what() << " pkt=" << rx.dump_hex();
    }
    curIt += pktSize;
  }
  rxBuf.erase(rxBuf.begin(), curIt);
}

// From MessageSources tab in ClientInterface spreadsheet
static inline string universalMessageSourceName(U8 code)
{
  switch (code) {
    case 255: return "undefined"s;
    case 254: return "robot_interface"s;
    case 253: return "rtmachine"s;
    case 252: return "simulated_robot"s;
    case 251: return "gui"s;
    case 116: return "tool_a"s;
    case 126: return "tool_b"s;
    case 7: return "controller"s;
    case 8: return "rtde"s;
    default: return "message source "s + to_string((int)code);
  }
}

// From ReportLevels tab in ClientInterface spreadsheet
static inline string universalReportLevelName(uint32_t code)
{
  switch (code) {
    case 0: return "debug"s;
    case 1: return "info"s;
    case 2: return "warning"s;
    case 3: return "violation"s;
    case 4: return "fault"s;
    case 128: return "devel_debug";
    case 129: return "devel_info"s;
    case 130: return "devel_warning"s;
    case 131: return "devel_violation"s;
    case 132: return "devel_fault"s;

    default: return "report level "s + to_string((int)code);
  }
}

static vector<string> splitCommas(string const &s)
{
  vector<string> ret;

  string::size_type pos = 0;
  while (pos < s.size()) {
    auto commaPos = s.find(',', pos);
    if (commaPos == pos) {
      pos = commaPos + 1;
    }
    else if (commaPos == string::npos) {
      ret.push_back(string(&s[pos], &s[s.size()]));
      break;
    }
    else {
      ret.push_back(string(&s[pos], &s[commaPos]));
      pos = commaPos + 1;
    }
  }
  return ret;
}

static vector<string> splitNewlines(string const &s)
{
  vector<string> ret;

  string::size_type pos = 0;
  while (pos < s.size()) {
    auto commaPos = s.find('\n', pos);
    if (commaPos == pos) {
      pos = commaPos + 1;
    }
    else if (commaPos == string::npos) {
      ret.push_back(string(&s[pos], &s[s.size()]));
      break;
    }
    else {
      ret.push_back(string(&s[pos], &s[commaPos]));
      pos = commaPos + 1;
    }
  }
  return ret;
}

static YAML::Node parseVariableDump(packet &rx)
{
  auto t = rx.get_be_uint8();
  if (t == 0) {
    return YAML::Node();
  }
  else if (t == 3 || t == 4) { // string
    return YAML::Node(rx.get_lenbe16_string());
  }
  else if (t == 5) { // list
    auto len = rx.get_be_uint16();
    YAML::Node ret;
    for (int i = 0; i < len; i++) {
      ret.push_back(parseVariableDump(rx));
    }
    return ret;
  }
  else if (t == 10) { // pose
    vector<F> pose;
    for (int i = 0; i < 6; i++) {
      pose.push_back(rx.get_be_float());
    }
    return YAML::Node(pose);
  }
  else if (t == 12) { // bool
    return YAML::Node(rx.get_be_uint8() ? true : false);
  }
  else if (t == 13) { // num
    return YAML::Node(rx.get_be_float());
  }
  else if (t == 14) { // int
    return YAML::Node((int32_t)rx.get_be_uint32());
  }
  else if (t == 15) { // float or num
    return YAML::Node(rx.get_be_float());
  }
  else {
    return YAML::Node("Unknown type tag " + to_string((int)t));
  }
}
