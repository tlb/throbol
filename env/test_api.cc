#include "../src/api.h"
#include "../src/trace.h"
#include "../src/emit.h"
#include "../remote/sim_camera_client.h"

struct TestApi : SheetApi {
  
  ApiRef<F, API_ACTUATOR> actForce {*this, "force"};
  ApiRef<F, API_SENSOR> sensePos {*this, "pos"};

  F pos = 0.0f;

  TestApi(CompiledSheet &_compiled, ApiSpec const &_spec)
    : SheetApi(_compiled, _spec, "test.1")
  {
    compiled.hasLiveApi = true;
  }

  virtual void txActuators(CellVals *vals, LiveRobotState &lrt) override
  {
    F force = actForce.memVal(vals);
    pos += 0.1f * force + float(0.05 * frandom_normal());
    if (0) L() << "TestApi: force=" << force << " pos=" << pos;
  }

  virtual void rxSensors(LiveRobotState &lrt) override
  {
    sensePos.replayVal(lastData) = pos;
  }

};

static ApiRegister regTest("test", [](CompiledSheet &_compiled, ApiSpec const &_spec) {
  return make_shared<TestApi>(_compiled, _spec);
});
