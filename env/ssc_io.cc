#include "./ssc_io.h"
#include "common/packetbuf_types.h"

namespace packetio 
  string packet_get_typetag(SscRawDump const &x)
  {
    return "SscRawDump:1";
  }
  void packet_wr_value(packet &p, SscRawDump const &x)
  {
    packetio::packet_wr_value(p, x.ts);
    packetio::packet_wr_value(p, x.values);
  }
  void packet_rd_value(packet &p, SscRawDump &x)
  {
    packetio::packet_rd_value(p, x.ts);
    packetio::packet_rd_value(p, x.values);
  }
  std::function<void(packet &, SscRawDump &)> packet_rd_value_compat(SscRawDump const &x, string const &typetag)
  {
    if (typetag == "SscRawDump:1") {
      return static_cast<void(*)(packet &, SscRawDump &)>(packetio::packet_rd_value);
    }
    return nullptr;
  }
}

INSTANTIATE_PACKETIO(SscRawDump);




string sscFmtSysEFlag(U16 v)
{
  if (v == 0) return "";
  string ret;
  if (v & (1 << 15)) ret += " watchdog_timer";
  if (v & (1 << 13)) ret += " ekf_divergence";
  if (v & (1 << 12)) ret += " gyro_sat";
  if (v & (1 << 11)) ret += " mag_disturb";
  if (v & (1 << 10)) ret += " linear_accel";
  if (v & (1 << 7)) ret += " proc_overrun";
  if (v & (1 << 6)) ret += " flash_update_fail";
  if (v & (1 << 5)) ret += " inertial_self_test_fail";
  if (v & (1 << 4)) ret += " sensor_overrange";
  if (v & (1 << 3)) ret += " spi_comm_error";
  if (v & (1 << 0)) ret += " alarm";
  if (!ret.empty()) return "("s + ret.substr(1) + ")"s;
  return "";
}

string sscFmtDiagSts(U16 v)
{
  if (v == 0) return "";
  string ret;
  if (v & (1 << 11)) ret += " barom";
  if (v & (1 << 10)) ret += " mag_z";
  if (v & (1 << 9)) ret += " mag_y";
  if (v & (1 << 8)) ret += " mag_x";
  if (v & (1 << 5)) ret += " accel_z";
  if (v & (1 << 4)) ret += " accel_y";
  if (v & (1 << 3)) ret += " accel_x";
  if (v & (1 << 2)) ret += " gyro_z";
  if (v & (1 << 1)) ret += " gyro_y";
  if (v & (1 << 0)) ret += " gyro_x";
  if (!ret.empty()) return "("s + ret.substr(1) + ")"s;
  return "";
}

string sscFmtAlmSts(U16 v)
{
  if (v == 0) return "";
  string ret;
  if (v & (1 << 11)) ret += " barom";
  if (v & (1 << 10)) ret += " mag_z";
  if (v & (1 << 9)) ret += " mag_y";
  if (v & (1 << 8)) ret += " mag_x";
  if (v & (1 << 5)) ret += " accel_z";
  if (v & (1 << 4)) ret += " accel_y";
  if (v & (1 << 3)) ret += " accel_x";
  if (v & (1 << 2)) ret += " gyro_z";
  if (v & (1 << 1)) ret += " gyro_y";
  if (v & (1 << 0)) ret += " gyro_x";
  if (!ret.empty()) return "("s + ret.substr(1) + ")"s;
  return "";
}


