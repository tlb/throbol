#include "../src/api.h"
#include "../src/scope_math.h"

/*
  The formula for adjusting a parameter based on the number of clicks a wheel has turned.
   With a hack to get back to actual zero
*/
static void tweakInput(F &val, int adjust)
{
  auto oldVal = val;
  val += adjust * 0.02f * max(1.0f, quantizeIntervalUp(abs(val)));
  if (val * oldVal < 0.0f) val = 0.0f;
}

struct VideoGamePaddleApi : SheetApi {
  
  ApiRef<F, API_SENSOR> paddle {*this, "paddle"};
  ApiRef<DrawOpHandle, API_ACTUATOR> display {*this, "display"};

  VideoGamePaddleApi(CompiledSheet &_compiled, ApiSpec const &_spec)
    : SheetApi(_compiled, _spec, "VideoGamePaddle.1")
  {
  }

  void doInteractive(ReplayBuffer &buffer, vector<int> const &doAdjustParams, int visCursIndex, bool shift) override;

};

static ApiRegister reg("videogamepaddle", [](CompiledSheet &_compiled, ApiSpec const &_spec) {
  return make_shared<VideoGamePaddleApi>(_compiled, _spec);
});

void VideoGamePaddleApi::doInteractive(ReplayBuffer &buffer, vector<int> const &doAdjustParams, int visCursIndex, bool shift)
{
  if (doAdjustParams.size() >= 1) {
    bool doCarryFwdPaddle = false;
    std::shared_lock lock(buffer.replayMutex);
    auto &blobs = buffer.replayByApiInstance[apiInstance];
    for (int cursi = visCursIndex; cursi < int(blobs.size()); cursi++) {
      auto buf1 = blobs[cursi];
      if (doCarryFwdPaddle) {
        paddle.replayVal(buf1) = paddle.replayVal(blobs[cursi-1]);
      }
      else {
        tweakInput(paddle.replayVal(buf1), doAdjustParams[0]);
        if (shift) {
          doCarryFwdPaddle = true;
        }
      }
    }
  }
}

