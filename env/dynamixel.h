#pragma once
#include "../src/api.h"
#include "../remote/cereal_box.h"
#if !defined(__EMSCRIPTEN__)
#include "dynamixel_sdk.h"
#endif


struct DynamixelJoint {
  SheetApi &owner;
  string name;

  ApiRef<F, API_SENSOR> pos {owner, name + ".pos"};
  ApiRef<F, API_SENSOR> vel {owner, name + ".vel"};

  DynamixelJoint(SheetApi &_owner, string const &_name)
   :owner(_owner),
    name(_name)
  {
  }
};

struct DynamixelJointActive : DynamixelJoint {

  ApiRef<F, API_SENSOR> pwm {owner, name + ".pwm"};
  ApiRef<F, API_SENSOR> current {owner, name + ".current"};

  ApiRef<F, API_ACTUATOR> goal_pos {owner, name + ".goal_pos"};

  DynamixelJointActive(SheetApi &_owner, string const &_name)
   :DynamixelJoint(_owner, _name)
  {
  }

};

struct DynamixelBus {

  string bus_name;
  string dev_fn;
  bool active = false;
  bool do_bulk_read = false;
  bool do_bulk_write = false;
#if !defined(__EMSCRIPTEN__)
  dynamixel::PortHandler *port = nullptr;
  dynamixel::GroupBulkRead *bulk_reader = nullptr;
  dynamixel::GroupBulkWrite *bulk_writer = nullptr;
#endif


  DynamixelBus(string const &_bus_name)
    :bus_name(_bus_name)
  {
  }
  // We take the address of these so disallow copying
  DynamixelBus(DynamixelBus && other) = delete;
  DynamixelBus(DynamixelBus const &other) = delete;

  ~DynamixelBus()
  {
#if !defined(__EMSCRIPTEN__)
    port = nullptr; // owned by dynamixel
    delete bulk_reader;
    delete bulk_writer;
#endif
  }

};

struct DynamixelApi : SheetApi {

  static constexpr uint8_t PROTOCOL_VERSION = 2;
  static constexpr int BAUD_RATE = 1000000;
  enum {
    // These are for an XM430
    ADDR_RETURN_DELAY_TIME = 9,
    ADDR_DRIVE_MODE = 10,
    ADDR_OPERATING_MODE = 11,
    ADDR_SHADOW_ID = 12,
    ADDR_HOMING_OFFSET = 20,
    ADDR_CURRENT_LIMIT = 38,
    ADDR_ACCELERATION_LIMIT = 40,
    ADDR_VELOCITY_LIMIT = 44,
    ADDR_MAX_POSITION_LIMIT = 48,
    ADDR_MIN_POSITION_LIMIT = 52,
    ADDR_TORQUE_ENABLE = 64,

    ADDR_VELOCITY_I_GAIN = 76,
    ADDR_VELOCITY_P_GAIN = 78,
    ADDR_POSITION_D_GAIN = 80,
    ADDR_POSITION_I_GAIN = 82,
    ADDR_POSITION_P_GAIN = 84,
    ADDR_FEEDFORWARD_2ND_GAIN = 88,
    ADDR_FEEDFORWARD_1ST_GAIN = 90,      

    ADDR_GOAL_PWM = 100,
    ADDR_GOAL_CURRENT = 102,
    ADDR_GOAL_VELOCITY = 104,
    ADDR_PROFILE_ACCELERATION = 108,
    ADDR_PROFILE_VELOCITY = 112,
    ADDR_GOAL_POSITION = 116,
    ADDR_PRESENT_PWM = 124,
    ADDR_PRESENT_CURRENT = 126,
    ADDR_PRESENT_VELOCITY = 128,
    ADDR_PRESENT_POSITION = 132,
    ADDR_PRESENT_INPUT_VOLTAGE = 144,
    ADDR_PRESENT_TEMPERATURE = 146,
  };
  static constexpr double RADIANS_TO_COUNTS = 651.8986;

  enum state_t {
    S_initial,
    S_setup,
    S_running,
    S_shutdown,
    S_error,
    S_finished,
  };

#if !defined(__EMSCRIPTEN__)
  dynamixel::PacketHandler *packet = nullptr;
#endif

  atomic<state_t> state = S_initial;
  atomic<bool> please_exit = false;
  atomic<CellVals *> txVals;
  std::thread task_thread;

  vector<DynamixelBus *> active_buses;

  vector<std::function<void()>> setup_funcs;
  vector<std::function<void()>> tx1_funcs;
  vector<std::function<void()>> rx1_funcs;
  vector<std::function<void()>> rx2_funcs;
  vector<std::function<void()>> teardown_funcs;
  

  DynamixelApi(CompiledSheet &_compiled, ApiSpec const &_spec, string _apiVersion)
    : SheetApi(_compiled, _spec, _apiVersion)
  {
    compiled.hasLiveApi = true;
  }

#if !defined(__EMSCRIPTEN__)
  void setActiveBus(DynamixelBus &bus, string const &fn)
  {
    active_buses.push_back(&bus);
    bus.dev_fn = fn;
    bus.active = true;
  }

  bool startLive() override
  {
    if (!SheetApi::startLive()) return false;
    task_thread = std::thread([this]() {
      state = S_setup;
      if (verbose >= 1) ll() << "setup";
      dxl_setup();
      if (state == S_error) return;
      state = S_running;
      if (verbose >= 1) ll() << "running";
      while (1) {
        if (please_exit) break;
        if (state == S_error) break;
        dxl_update();
      }
      if (state != S_error) {
        state = S_shutdown;
        if (verbose >= 1) ll() << "shutdown";
      }
      dxl_off();
      dxl_teardown();
      if (state != S_error) {
        state = S_finished;
        if (verbose >= 1) ll() << "finished";
      }
    });
    return true;
  }

  void stopLive() override
  {
    please_exit = true;
    txVals = nullptr;
    task_thread.join();
  }

  void write1ByteTxRxCheck(DynamixelBus &bus, int dxlid, uint16_t addr, uint8_t data)
  {
    uint8_t dxl_err;
    int comm_err = packet->write1ByteTxRx(bus.port, dxlid, addr, data, &dxl_err);
    if (comm_err != COMM_SUCCESS) {
      ll() << bus.bus_name << ":" << dxlid << ": comm error " << packet->getTxRxResult(comm_err);
      state = S_error;
      return;
    }
    if (dxl_err != 0) {
      ll() << bus.bus_name << ":" << dxlid << ": rx error " << packet->getRxPacketError(dxl_err) <<
        " while writing 1 byte to " << addr;
      state = S_error;
      return;
    }
  }

  void write2ByteTxRxCheck(DynamixelBus &bus, int dxlid, uint16_t addr, uint16_t data)
  {
    uint8_t dxl_err;
    int comm_err = packet->write2ByteTxRx(bus.port, dxlid, addr, data, &dxl_err);
    if (comm_err != COMM_SUCCESS) {
      ll() << bus.bus_name << ":" << dxlid << ": comm error " << packet->getTxRxResult(comm_err);
      state = S_error;
      return;
    }
    if (dxl_err != 0) {
      ll() << bus.bus_name << ":" << dxlid << ": rx error " << packet->getRxPacketError(dxl_err) <<
        " while writing 2 bytes to " << addr;
      state = S_error;
      return;
    }
  }

  void write4ByteTxRxCheck(DynamixelBus &bus, int dxlid, uint16_t addr, int32_t data)
  {
    uint8_t dxl_err;
    int comm_err = packet->write4ByteTxRx(bus.port, dxlid, addr, data, &dxl_err);
    if (comm_err != COMM_SUCCESS) {
      ll() << bus.bus_name << ":" << dxlid << ": comm error " << packet->getTxRxResult(comm_err);
      state = S_error;
      return;
    }
    if (dxl_err != 0) {
      ll() << bus.bus_name << ":" << dxlid << ": rx error " << packet->getRxPacketError(dxl_err) <<
        " while writing 4 bytes to " << addr;
      state = S_error;
      return;
    }
  }

  int32_t read4ByteTxRxCheck(DynamixelBus &bus, int dxlid, uint16_t addr)
  {
    uint8_t dxl_err;
    uint32_t ret;
    int comm_err = packet->read4ByteTxRx(bus.port, dxlid, addr, &ret, &dxl_err);

    if (comm_err != COMM_SUCCESS) {
      ll() << bus.bus_name << ":" << dxlid << ": comm error " << packet->getTxRxResult(comm_err);
      state = S_error;
      return 0;
    }
    if (dxl_err != 0) {
      ll() << bus.bus_name << ":" << dxlid << ": rx error " << packet->getRxPacketError(dxl_err) <<
        " while reading 4 bytes from " << addr;
      state = S_error;
      return 0;
    }
    return ret;
  }


  void addServoConfig1(DynamixelBus &bus, int dxlid, uint32_t addr, uint8_t data)
  {
    setup_funcs.push_back([this, &bus, dxlid, addr, data]() {
      ll() << bus.bus_name << ":" << dxlid << ": setup reg[" << addr << "] = " << uint32_t(data);
      write1ByteTxRxCheck(bus, dxlid, addr, data);
    });
  }
  void addServoConfig2(DynamixelBus &bus, int dxlid, uint32_t addr, uint16_t data)
  {
    setup_funcs.push_back([this, &bus, dxlid, addr, data]() {
      ll() << bus.bus_name << ":" << dxlid << ": setup reg[" << addr << "] = " << uint32_t(data);
      write2ByteTxRxCheck(bus, dxlid, addr, data);
    });
  }
  void addServoConfig4(DynamixelBus &bus, int dxlid, uint32_t addr, int32_t data)
  {
    setup_funcs.push_back([this, &bus, dxlid, addr, data]() {
      ll() << bus.bus_name << ":" << dxlid << ": setup reg[" << addr << "] = " << int32_t(data);
      write4ByteTxRxCheck(bus, dxlid, addr, data);
    });
  }

  void addServoSetup(DynamixelBus &bus, int dxlid, bool torque_enable)
  {
    setup_funcs.push_back([this, &bus, dxlid, torque_enable]() {
      ll() << bus.bus_name << ":" << dxlid << ": setup torque_enable=" << (torque_enable?1:0);
      write1ByteTxRxCheck(bus, dxlid, ADDR_TORQUE_ENABLE, torque_enable ? 1 :0);
    });
  }

  void addServoTorqueOffAtCooldown(DynamixelBus &bus, int dxlid)
  {
    teardown_funcs.push_back([this, &bus, dxlid]() {
      ll() << bus.bus_name << ":" << dxlid << ": disable torque";
      write1ByteTxRxCheck(bus, dxlid, ADDR_TORQUE_ENABLE, 0);
    });
  }

  void addServoRxPresents(DynamixelBus &bus, int dxlid, 
    ApiRef<F, API_SENSOR> *pwm_sensor,
    ApiRef<F, API_SENSOR> *current_sensor,
    ApiRef<F, API_SENSOR> *velocity_sensor,
    ApiRef<F, API_SENSOR> *pos_sensor)
  {
    static constexpr int BLOCK_START = ADDR_PRESENT_PWM;
    static constexpr int BLOCK_SIZE = ADDR_PRESENT_POSITION + sizeof(int32_t) - ADDR_PRESENT_PWM;

    rx1_funcs.push_back([this, &bus, dxlid]() {
      bus.do_bulk_read = true;
      if (!bus.bulk_reader->addParam(dxlid, BLOCK_START, BLOCK_SIZE)) {
        ll() << bus.bus_name << ":" << dxlid << ": groupBulkReadAddParam(PRESENTs) failed";
        state = S_error;
        return;
      }
    });

    rx2_funcs.push_back([this, &bus, pwm_sensor, current_sensor, velocity_sensor, pos_sensor, dxlid]() {
      if (!bus.bulk_reader->isAvailable(dxlid, BLOCK_START, BLOCK_SIZE)) {
        ll() << bus.bus_name << ":" << dxlid << ": groupBulkReadIsAvailable(PRESENT_POSITION) failed";
        state = S_error;
        return;
      }
      if (pwm_sensor) {
        int16_t raw = int16_t(bus.bulk_reader->getData(dxlid, ADDR_PRESENT_PWM, sizeof(int16_t))); 
        // reports 1 per 0.113%
        pwm_sensor->replayVal(lastData) = float(raw) * 0.00113;
      }
      if (current_sensor) {
        int16_t raw = int16_t(bus.bulk_reader->getData(dxlid, ADDR_PRESENT_CURRENT, sizeof(int16_t)));
        // reports 1 per 2.69 mA
        current_sensor->replayVal(lastData) = float(raw) * 2.69e-3;
      }
      if (velocity_sensor) {
        int raw = bus.bulk_reader->getData(dxlid, ADDR_PRESENT_VELOCITY, sizeof(int32_t));
        // reports 1 per 0.229 RPM. Convert to radians/sec
        velocity_sensor->replayVal(lastData) = float(raw) * (0.229 * 60 / M_2PI);
      }
      if (pos_sensor) {
        int raw = bus.bulk_reader->getData(dxlid, ADDR_PRESENT_POSITION, sizeof(int32_t));
        pos_sensor->replayVal(lastData) = float(raw) * (M_2PI / 4096.0) - M_PI;
      }
    });
  }

  void addServoTxGoalPosition(DynamixelBus &bus, int dxlid, ApiRef<F, API_ACTUATOR> *actuator, int min_position_limit, int max_position_limit)
  {
    tx1_funcs.push_back([this, &bus, actuator, dxlid, min_position_limit, max_position_limit]() {
      if (auto vals = txVals.load()) {
        bus.do_bulk_write = true;
        int32_t goal_pos = clamp(int(RADIANS_TO_COUNTS * (actuator->memVal(vals) + M_PI)),
          min_position_limit, max_position_limit);
        if (!bus.bulk_writer->addParam(dxlid, ADDR_GOAL_POSITION, sizeof(int32_t), (uint8_t *)&goal_pos)) {
          ll() << bus.bus_name << ":" << dxlid << ": groupBulkWriteAddParam failed";
          state = S_error;
        }
      }
    });
  }

  void addServoReversedShadow(DynamixelBus &bus, int dxlid, int primary_dxlid)
  {
    if (!bus.active) return;
    setup_funcs.push_back([this, &bus, dxlid, primary_dxlid]() {

      write1ByteTxRxCheck(bus, dxlid, ADDR_TORQUE_ENABLE, 0);
      // Important to set drive mode before reading position
      write1ByteTxRxCheck(bus, dxlid, ADDR_DRIVE_MODE, 0x01);
      write4ByteTxRxCheck(bus, dxlid, ADDR_HOMING_OFFSET, 0);

      int32_t primary_pos = read4ByteTxRxCheck(bus, primary_dxlid, ADDR_PRESENT_POSITION);
      int32_t shadow_pos = read4ByteTxRxCheck(bus, dxlid, ADDR_PRESENT_POSITION);

      ll() << bus.bus_name << ":" << dxlid << ": setup as reversed shadow of " << primary_dxlid <<
        " primary_pos=" << primary_pos <<
        " shadow_pos=" << shadow_pos <<
        " offset=" << (primary_pos - shadow_pos);
      write4ByteTxRxCheck(bus, dxlid, ADDR_HOMING_OFFSET, primary_pos - shadow_pos);

      write1ByteTxRxCheck(bus, dxlid, ADDR_DRIVE_MODE, 0x01); // reverse mode
      write1ByteTxRxCheck(bus, dxlid, ADDR_SHADOW_ID, primary_dxlid);
      write1ByteTxRxCheck(bus, dxlid, ADDR_TORQUE_ENABLE, 1); // XXX TEMP
    });
  }

  void dxl_setup()
  {
    for (auto bus : active_buses) {
      bus->port = dynamixel::PortHandler::getPortHandler(bus->dev_fn.c_str());
    }
    packet = dynamixel::PacketHandler::getPacketHandler(2.0);

    for (auto bus : active_buses) {
      bus->bulk_writer = new dynamixel::GroupBulkWrite(bus->port, packet);
      bus->bulk_reader = new dynamixel::GroupBulkRead(bus->port, packet);
    }

    for (auto bus : active_buses) {
      if (bus->active) {
        bus->port->setBaudRate(BAUD_RATE);
      }
    }

    for (auto &it : setup_funcs) {
      it();
    }
  }

  
  void dxl_update()
  {
    // Write

    for (auto bus : active_buses) {
      bus->do_bulk_write = false;
    }
    for (auto &tx1_func : tx1_funcs) {
      tx1_func(); // will set do_bulk_write if needed
    }
    for (auto bus : active_buses) {
      if (bus->do_bulk_write) {
        bus->bulk_writer->txPacket();
      }
    }
    for (auto bus : active_buses) {
      bus->bulk_writer->clearParam();
    }

    // Read

    for (auto bus : active_buses) {
      bus->do_bulk_read = false;
    }
    for (auto &rx1_func : rx1_funcs) {
      rx1_func(); // will set do_bulk_read if needed
    }

    for (auto bus : active_buses) {
      if (bus->do_bulk_read) {
        int comm_err = bus->bulk_reader->txPacket();
        if (comm_err != COMM_SUCCESS) {
          ll() << bus->bus_name << ": groupBulkReadTxRxPacket: " << packet->getTxRxResult(comm_err);
          state = S_error;
          return;
        }
      }
    }

    for (auto bus : active_buses) {
      if (bus->do_bulk_read) {
        int comm_err = bus->bulk_reader->rxPacket();
        if (comm_err != COMM_SUCCESS) {
          ll() << bus->bus_name << ": groupBulkReadTxRxPacket: " << packet->getTxRxResult(comm_err);
          state = S_error;
          return;
        }
      }
    }

    for (auto &rx2_func : rx2_funcs) {
      rx2_func();
    }
    for (auto bus : active_buses) {
      bus->bulk_reader->clearParam();
    }
  }

  void dxl_off()
  {
    for (auto &it : teardown_funcs) {
      it();
    }
  }

  void dxl_teardown()
  {
    for (auto bus : active_buses) {
      ll() << "Close " << bus->bus_name;
      bus->port->closePort();
    }
  }


  void txWarmup(LiveRobotState &lrt) override
  {
    switch (state.load()) {
      case S_initial:
        lrt.needMoreWarmup.push_back(label + " initial");
        break;

      case S_setup:
        lrt.needMoreWarmup.push_back(label + " setup");
        break;

      case S_error:
      case S_shutdown:
      case S_finished:
        lrt.pleaseStopHard();
        break;

      case S_running:
        break;
    }
  }

  void txActuators(CellVals *vals, LiveRobotState &lrt) override
  {
    txVals = vals;
  }

  void txCooldown(LiveRobotState &lrt) override
  {
    // WRITEME: send puppets back to a nice home position
  }

  void rxSensors(LiveRobotState &lrt) override
  {
  }
#endif
};

