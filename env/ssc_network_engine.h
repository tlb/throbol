#pragma once
#include "../src/api.h"
#include "../src/vbmainloop.h"
#include "common/packetbuf.h"
/*
  An SscNetworkEngine provides fast real-time communication to hardware devices.
  It uses a simple binary protocol over UDP.
  See the Real-Time Networking section in the README
*/

struct SscNetworkConfig {

  string remoteHostname;
  string remotePort;
  string localHostname;
  string localPort;
  int boardInstance = 0;
  
};

#if !defined(__EMSCRIPTEN__)
struct SscNetworkEngine : enable_shared_from_this<SscNetworkEngine> {

  string label;
  string remoteHostname;
  string remotePort;
  int localPort = 0;
  int verbose = 0;

  asio::ip::udp::socket sock{io_context};
  asio::ip::udp::endpoint rxSrc;
  string rxBuf;

  asio::ip::udp::endpoint localAddr;
  asio::ip::udp::endpoint remoteAddr;


  double lastUpdateRequestTs = 0.0;
  double lastLogTs = 0.0;
  bool rxAnyFlag = false, ready = false;

  std::function<void(double rxTime, packet &rx, bool &error)> rxHandlers[256];
  std::function<void(double rxTime, packet &rx, U64 pktTicks, bool &error)> updateHandlers[256];

  SscNetworkEngine(string const &_label, string const &_remoteHostname, string const &_remotePort);
  ~SscNetworkEngine();
  SscNetworkEngine(SscNetworkEngine const &) = delete;
  SscNetworkEngine(SscNetworkEngine &&) = delete;
  SscNetworkEngine &operator=(SscNetworkEngine const &) = delete;
  SscNetworkEngine &operator=(SscNetworkEngine &&) = delete;

  void start();
  void setupSockets();
  void setupHandlers();
  void closeSockets();
  void asyncRx();

  void rxPkt(packet &rx);
  void txPkt(packet &tx);
  void sendAdminCmd(string const &s);
  bool isReady();

  R fromExternalTime(R t) { return t; }
};
#endif
