#pragma once
/*
  Compile with -include env/mujoco_port.h to fix Mujoco portability issues
*/

#if defined(__EMSCRIPTEN__)


#define __STDC_VERSION_TIME_H__ 1

#ifdef __cplusplus
extern "C" {
#endif

/*
  Mujoco's engine_crossplatform.h depends on qsort_r, which is missing from emscripten
  so we bring in a polyfill sort_r library
*/

#include "../deps/sort_r/sort_r.h"
#define qsort_r sort_r

#ifdef __cplusplus
}
#endif

#endif
