#include "../src/api.h"
#include "../src/trace.h"
#include "../src/emit.h"
#include "../remote/sim_camera_client.h"
#include "imgui.h"

struct KbdApi : SheetApi {
  
  vector<pair<int, ApiRef<F, API_SENSOR> *>> keyApis;

  std::array<bool, API_KBD_NKEYS> down;

  KbdApi(CompiledSheet &_compiled, ApiSpec const &_spec)
    : SheetApi(_compiled, _spec, "kbd.1")
  {
    compiled.hasLiveApi = true;
    for (int i = 0; i < API_KBD_NKEYS; i++) {
      down[i] = 0;
    }
    addKey(ImGuiKey_Y, "Y");
    addKey(ImGuiKey_U, "U");
    addKey(ImGuiKey_I, "I");
    addKey(ImGuiKey_O, "O");
    addKey(ImGuiKey_P, "P");

    addKey(ImGuiKey_H, "H");
    addKey(ImGuiKey_J, "J");
    addKey(ImGuiKey_K, "K");
    addKey(ImGuiKey_L, "L");
    addKey(ImGuiKey_Semicolon, "SEMICOLON");

    addKey(ImGuiKey_B, "B");
    addKey(ImGuiKey_N, "N");
    addKey(ImGuiKey_M, "M");
    addKey(ImGuiKey_Comma, "COMMA");
    addKey(ImGuiKey_Period, "PERIOD");

    if (verbose >= 1) L() << label << " replayBufferSize=" << replayBufferSize;
  }

  void addKey(int code, char const *name)
  {
    auto api = mkDynamic<F, API_SENSOR>(name);
    keyApis.emplace_back(code - ImGuiKey_NamedKey_BEGIN, api);
  }

  void rxSensors(LiveRobotState &lrt) override
  {
    for (auto &it : keyApis) {
      it.second->replayVal(lastData) = down[it.first] ? 1.0f : 0.0f;
    }
  }

  void setKbd(std::array<bool, API_KBD_NKEYS> const &_down) override
  {
    down = _down;
  }

};

static ApiRegister regKbd("kbd", [](CompiledSheet &_compiled, ApiSpec const &_spec) {
  return make_shared<KbdApi>(_compiled, _spec);
});
