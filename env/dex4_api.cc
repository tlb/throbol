#include "../src/api.h"
#include "./ssc_network_engine.h"
#include "./ssc_io.h"
#include "../src/emit.h"
#include "../src/live_robot_ticker.h"

struct SscPneuservoApi {

  SheetApi &owner;
  string joint;

  ApiSection section {owner, joint + " SscPneuservo"};
  ApiRef<F, API_SENSOR> sense_ext_pressure {owner, joint + ".ext_pressure"};
  ApiRef<F, API_SENSOR> sense_ret_pressure {owner, joint + ".ret_pressure"};
  ApiRef<F, API_SENSOR> sense_torque {owner, joint + ".torque"};
  ApiRef<F, API_SENSOR> sense_pos {owner, joint + ".pos"};
  ApiRef<F, API_SENSOR> sense_vel_prefilter {owner, joint + ".vel_prefilter"};
  ApiRef<F, API_SENSOR> sense_vel {owner, joint + ".vel"};
  ApiRef<F, API_SENSOR> sense_torque_feedback {owner, joint + ".torque_feedback"};
  ApiRef<F, API_SENSOR> sense_pos_feedback {owner, joint + ".pos_feedback"};
  ApiRef<F, API_SENSOR> sense_vel_feedback {owner, joint + ".vel_feedback"};
  ApiRef<F, API_SENSOR> sense_vel_feedforward {owner, joint + ".vel_feedforward"};
  ApiRef<F, API_SENSOR> sense_valve_prefilter {owner, joint + ".valve_prefilter"};
  ApiRef<F, API_SENSOR> sense_valve {owner, joint + ".valve"};
  ApiRef<F, API_SENSOR> sense_base_valve {owner, joint + ".given_base_valve"};
  ApiRef<F, API_SENSOR> sense_desired_torque {owner, joint + ".given_desired_torque"};
  ApiRef<F, API_SENSOR> sense_desired_pos {owner, joint + ".given_desired_pos"};
  ApiRef<F, API_SENSOR> sense_desired_vel {owner, joint + ".given_desired_vel"};

  ApiRef<F, API_ACTUATOR> act_base_valve {owner, joint + ".base_valve"};
  ApiRef<F, API_ACTUATOR> act_desired_torque {owner, joint + ".desired_torque"};
  ApiRef<F, API_ACTUATOR> act_desired_pos {owner, joint + ".desired_pos"};
  ApiRef<F, API_ACTUATOR> act_desired_vel {owner, joint + ".desired_vel"};
  ApiRef<F, API_ACTUATOR> act_torque_feedback_coeff {owner, joint + ".torque_feedback_coeff"};
  ApiRef<F, API_ACTUATOR> act_torque_feedback_lim_lo {owner, joint + ".torque_feedback_lim_lo"};
  ApiRef<F, API_ACTUATOR> act_torque_feedback_lim_hi {owner, joint + ".torque_feedback_lim_hi"};
  ApiRef<F, API_ACTUATOR> act_pos_feedback_coeff {owner, joint + ".pos_feedback_coeff"};
  ApiRef<F, API_ACTUATOR> act_pos_feedback_lim_lo {owner, joint + ".pos_feedback_lim_lo"};
  ApiRef<F, API_ACTUATOR> act_pos_feedback_lim_hi {owner, joint + ".pos_feedback_lim_hi"};
  ApiRef<F, API_ACTUATOR> act_vel_feedback_coeff {owner, joint + ".vel_feedback_coeff"};
  ApiRef<F, API_ACTUATOR> act_vel_feedback_lim_lo {owner, joint + ".vel_feedback_lim_lo"};
  ApiRef<F, API_ACTUATOR> act_vel_feedback_lim_hi {owner, joint + ".vel_feedback_lim_hi"};
  ApiRef<F, API_ACTUATOR> act_vel_feedforward_coeff {owner, joint + ".vel_feedforward_coeff"};

  SscPneuservoApi(SheetApi &_owner, string const &_joint)
    :owner(_owner),
     joint(_joint)
  {
  }

  void rxPneuservoState(packet &rx, Blob &vals)
  {
    rxDsp824(rx, sense_ext_pressure.replayVal(vals));
    rxDsp824(rx, sense_ret_pressure.replayVal(vals));
    rxDsp1616(rx, sense_torque.replayVal(vals));
    rxDsp824(rx, sense_pos.replayVal(vals));
    rxDsp824(rx, sense_vel_prefilter.replayVal(vals));
    rxDsp824(rx, sense_vel.replayVal(vals));
    rxDsp824(rx, sense_torque_feedback.replayVal(vals));
    rxDsp1616(rx, sense_pos_feedback.replayVal(vals));
    rxDsp1616(rx, sense_vel_feedback.replayVal(vals));
    rxDsp824(rx, sense_vel_feedforward.replayVal(vals));
    rxDsp824(rx, sense_valve_prefilter.replayVal(vals));
    rxDsp824(rx, sense_valve.replayVal(vals));
    rxDsp824(rx, sense_base_valve.replayVal(vals));
    rxDsp1616(rx, sense_desired_torque.replayVal(vals));
    rxDsp824(rx, sense_desired_pos.replayVal(vals));
    rxDsp824(rx, sense_desired_vel.replayVal(vals));
  }

  void txPneuservoAct(packet &tx, CellVals *vals)
  {
    txDsp824(tx, act_base_valve.memVal(vals));
    txDsp1616(tx, act_desired_torque.memVal(vals));
    txDsp824(tx, act_desired_pos.memVal(vals));
    txDsp824(tx, act_desired_vel.memVal(vals));

    txDsp824(tx, act_torque_feedback_coeff.memVal(vals));
    txDsp824(tx, act_torque_feedback_lim_lo.memVal(vals));
    txDsp824(tx, act_torque_feedback_lim_hi.memVal(vals));

    txDsp1616(tx, act_pos_feedback_coeff.memVal(vals));
    txDsp1616(tx, act_pos_feedback_lim_lo.memVal(vals));
    txDsp1616(tx, act_pos_feedback_lim_hi.memVal(vals));

    txDsp1616(tx, act_vel_feedback_coeff.memVal(vals));
    txDsp1616(tx, act_vel_feedback_lim_lo.memVal(vals));
    txDsp1616(tx, act_vel_feedback_lim_hi.memVal(vals));

    txDsp824(tx, act_vel_feedforward_coeff.memVal(vals));
  }


};

struct SscRegulatorApi {

  SheetApi &owner;
  string name;

  ApiSection section {owner, name + " SscRegulator"};
  ApiRef<F, API_SENSOR> senseSupPressure {owner, name + ".sup_pressure"};
  ApiRef<F, API_SENSOR> senseExhPressure {owner, name + ".exh_pressure"};

  ApiRef<F, API_ACTUATOR> actSetpoint {owner, name + ".setpoint"};

  SscRegulatorApi(SheetApi &_owner, string const &_name)
    :owner(_owner),
     name(_name)
  {
  }

  void rxRegulatorState(packet &rx, Blob &vals)
  {
    rxDsp824(rx, senseSupPressure.replayVal(vals));
    rxDsp824(rx, senseExhPressure.replayVal(vals));
  }

  void txRegulatorAct(packet &tx, CellVals *vals)
  {
    txDsp824(tx, actSetpoint.memVal(vals));
  }

};

struct SscFootApi {

  SheetApi &owner;
  string name;

  ApiSection section {owner, name + " SscFoot"};
  ApiRef<F, API_SENSOR> senseFti {owner, name + ".fti"};
  ApiRef<F, API_SENSOR> senseFto {owner, name + ".fto"};
  ApiRef<F, API_SENSOR> senseFh {owner, name + ".fh"};
  ApiRef<F, API_SENSOR> senseAi {owner, name + ".ai"};
  ApiRef<F, API_SENSOR> senseAo {owner, name + ".ao"};

  SscFootApi(SheetApi &_owner, string const &_name)
    :owner(_owner),
     name(_name)
  {
  }

  void rxFootState(packet &rx, Blob &vals)
  {
    rxDsp1616(rx, senseFti.replayVal(vals));
    rxDsp1616(rx, senseFto.replayVal(vals));
    rxDsp1616(rx, senseFh.replayVal(vals));
    rxDsp824(rx, senseAi.replayVal(vals));
    rxDsp824(rx, senseAo.replayVal(vals));
  }
};


struct SscGyroApi {

  SheetApi &owner;
  string name;

  ApiSection section {owner, name + " SscGyro"};
  ApiRef<vec3, API_SENSOR> senseGyro {owner, name + ".gyro"};
  ApiRef<vec3, API_SENSOR> senseAccel {owner, name + ".accel"};
  ApiRef<vec3, API_SENSOR> senseDeltang {owner, name + ".deltang"};
  ApiRef<vec3, API_SENSOR> senseDeltvel {owner, name + ".deltvel"};
  ApiRef<F, API_SENSOR> senseBarom {owner, name + ".barom"};
  ApiRef<mat3, API_SENSOR> senseOrient {owner, name + ".orient"};
  ApiRef<vec3, API_SENSOR> senseMag {owner, name + ".mag"};
  ApiRef<F, API_SENSOR> senseTemperature {owner, name + ".temperature"};
  ApiRef<F, API_SENSOR> senseSysEFlag {owner, name + ".sys_e_flag"};
  ApiRef<F, API_SENSOR> senseDiagSts {owner, name + ".diag_sts"};
  ApiRef<F, API_SENSOR> senseAlmSts {owner, name + ".alm_sts"};

  // bits from sys_e_flag
  ApiRef<F, API_SENSOR> senseWatchdogTimer {owner, name + ".watchdog_timer"};
  ApiRef<F, API_SENSOR> senseEkfDivergence {owner, name + ".ekf_divergence"};
  ApiRef<F, API_SENSOR> senseGyroSet {owner, name + ".gyro_sat"};
  ApiRef<F, API_SENSOR> senseMagDisturb {owner, name + ".mag_disturb"};
  ApiRef<F, API_SENSOR> senseLinearAccel {owner, name + ".linear_accel"};
  ApiRef<F, API_SENSOR> senseProcOverrun {owner, name + ".proc_overrun"};
  ApiRef<F, API_SENSOR> senseInertialSelfTestFail {owner, name + ".inertial_self_test_fail"};
  ApiRef<F, API_SENSOR> senseSensorOverrange {owner, name + ".sensor_overrange"};
  ApiRef<F, API_SENSOR> senseSpiCommError {owner, name + ".spi_comm_error"};
  ApiRef<F, API_SENSOR> senseAlarm {owner, name + ".alarm"};

  SscGyroApi(SheetApi &_owner, string const &_name)
    :owner(_owner),
     name(_name)
  {
  }

  void rxGyro(packet &rx, Blob &vals)
  {
    S32 gyroX, gyroY, gyroZ;
    rx.get(gyroX);
    rx.get(gyroY);
    rx.get(gyroZ);

    senseGyro.replayVal(vals) = vec3(
      sscConvGyro(gyroX),
      sscConvGyro(gyroY),
      sscConvGyro(gyroZ));
  }

  void rxAccel(packet &rx, Blob &vals)
  {
    S32 accelX, accelY, accelZ;
    rx.get(accelX);
    rx.get(accelY);
    rx.get(accelZ);

    senseAccel.replayVal(vals) = vec3(
      sscConvAccel(accelX),
      sscConvAccel(accelY),
      sscConvAccel(accelZ));
  }

  void rxDeltang(packet &rx, Blob &vals)
  {
    S32 deltangX, deltangY, deltangZ;
    rx.get(deltangX);
    rx.get(deltangY);
    rx.get(deltangZ);

    senseDeltang.replayVal(vals) = vec3(
      sscConvDeltang(deltangX),
      sscConvDeltang(deltangY),
      sscConvDeltang(deltangZ));
  }

  void rxDeltvel(packet &rx, Blob &vals)
  {
    S32 deltvelX, deltvelY, deltvelZ;
    rx.get(deltvelX);
    rx.get(deltvelY);
    rx.get(deltvelZ);

    senseDeltvel.replayVal(vals) = vec3(
      sscConvDeltvel(deltvelX),
      sscConvDeltvel(deltvelY),
      sscConvDeltvel(deltvelZ));
  }

  void rxBarom(packet &rx, Blob &vals)
  {
    S32 baromRaw;
    rx.get(baromRaw);
    senseBarom.replayVal(vals) = sscConvBarom(baromRaw);
  }

  void rxOrient(packet &rx, Blob &vals)
  {
    S16 orient11, orient12, orient13;
    S16 orient21, orient22, orient23;
    S16 orient31, orient32, orient33;
    rx.get(orient11);
    rx.get(orient12);
    rx.get(orient13);
    rx.get(orient21);
    rx.get(orient22);
    rx.get(orient23);
    rx.get(orient31);
    rx.get(orient32);
    rx.get(orient33);

    mat3 orientMat;
    orientMat(0, 0) = sscConvOrient(orient11);
    orientMat(1, 0) = sscConvOrient(orient12);
    orientMat(2, 0) = sscConvOrient(orient13);
    orientMat(0, 1) = sscConvOrient(orient21);
    orientMat(1, 1) = sscConvOrient(orient22);
    orientMat(2, 1) = sscConvOrient(orient23);
    orientMat(0, 2) = sscConvOrient(orient31);
    orientMat(1, 2) = sscConvOrient(orient32);
    orientMat(2, 2) = sscConvOrient(orient33);

    if (0) {
      L() << "Orient " <<
        orient11 << " " << orient12 << " " << orient13 << " / " <<
        orient21 << " " << orient22 << " " << orient23 << " / " <<
        orient31 << " " << orient32 << " " << orient33 << " = " <<
        orientMat;
    }
    senseOrient.replayVal(vals) = orientMat;
  }

  void rxMag(packet &rx, Blob &vals)
  {
    S16 magX, magY, magZ;
    rx.get(magX);
    rx.get(magY);
    rx.get(magZ);

    senseMag.replayVal(vals) = vec3(
      sscConvMag(magX),
      sscConvMag(magY),
      sscConvMag(magZ));
  }

  void rxTemperature(packet &rx, Blob &vals)
  {
    S16 temperatureRaw;
    rx.get(temperatureRaw);
    senseTemperature.replayVal(vals) = sscConvTemperature(temperatureRaw);
  }

  void rxFlags(packet &rx, Blob &vals)
  {
    U16 sysEFlagRaw;
    U16 diagStsRaw;
    U16 almStsRaw;
    rx.get(sysEFlagRaw);
    rx.get(diagStsRaw);
    rx.get(almStsRaw);

    senseSysEFlag.replayVal(vals) = sysEFlagRaw;

    senseWatchdogTimer.replayVal(vals) = (sysEFlagRaw & (1<<15)) ? 1.0f : 0.0f;
    senseEkfDivergence.replayVal(vals) = (sysEFlagRaw & (1<<13)) ? 1.0f : 0.0f;
    senseGyroSet.replayVal(vals) = (sysEFlagRaw & (1<<12)) ? 1.0f : 0.0f;
    senseMagDisturb.replayVal(vals) = (sysEFlagRaw & (1<<11)) ? 1.0f : 0.0f;
    senseLinearAccel.replayVal(vals) = (sysEFlagRaw & (1<<10)) ? 1.0f : 0.0f;
    senseProcOverrun.replayVal(vals) = (sysEFlagRaw & (1<<7)) ? 1.0f : 0.0f;
    senseInertialSelfTestFail.replayVal(vals) = (sysEFlagRaw & (1<<5)) ? 1.0f : 0.0f;
    senseSensorOverrange.replayVal(vals) = (sysEFlagRaw & (1<<4)) ? 1.0f : 0.0f;
    senseSpiCommError.replayVal(vals) = (sysEFlagRaw & (1<<3)) ? 1.0f : 0.0f;
    senseAlarm.replayVal(vals) = (sysEFlagRaw & (1<<0)) ? 1.0f : 0.0f;

    senseDiagSts.replayVal(vals) = diagStsRaw;
    senseAlmSts.replayVal(vals) = almStsRaw;
  }

  void rxGyroState(packet &rx, Blob &vals)
  {
    rxGyro(rx, vals);
    rxAccel(rx, vals);
    rxDeltang(rx, vals);
    rxDeltvel(rx, vals);
    rxBarom(rx, vals);
    rxOrient(rx, vals);
    rxMag(rx, vals);
    rxTemperature(rx, vals);
    rxFlags(rx, vals);
    if (0) L() << "rx gyro.senseAccel replay = " << senseAccel.replayVal(vals);
  }

};



struct SscHarnessApi {

  SheetApi &owner;
  string name;

  ApiSection lifterSection {owner, name + " SscHarness"};
  ApiRef<F, API_SENSOR> sense_ext_pressure {owner, name + ".ext_pressure"};
  ApiRef<F, API_SENSOR> sense_ret_pressure {owner, name + ".ret_pressure"};
  ApiRef<F, API_SENSOR> sense_pos {owner, name + ".pos"};
  ApiRef<F, API_SENSOR> senseForce {owner, name + ".force"};
  ApiRef<F, API_SENSOR> sense_pos_feedback {owner, name + ".pos_feedback"};
  ApiRef<F, API_SENSOR> senseForce_feedback {owner, name + ".force_feedback"};
  ApiRef<F, API_SENSOR> sense_valve {owner, name + ".valve"};


  ApiRef<F, API_ACTUATOR> act_base_valve {owner, name + ".base_valve"};
  ApiRef<F, API_ACTUATOR> act_desired_force {owner, name + ".desired_force"};
  ApiRef<F, API_ACTUATOR> act_desired_pos {owner, name + ".desired_pos"};
  ApiRef<F, API_ACTUATOR> act_ext_pressure_to_force {owner, name + ".ext_pressure_to_force"};
  ApiRef<F, API_ACTUATOR> act_ret_pressure_to_force {owner, name + ".ret_pressure_to_force"};
  ApiRef<F, API_ACTUATOR> act_force_feedback_coeff {owner, name + ".force_feedback_coeff"};
  ApiRef<F, API_ACTUATOR> act_force_feedback_lim_lo {owner, name + ".force_feedback_lim_lo"};
  ApiRef<F, API_ACTUATOR> act_force_feedback_lim_hi {owner, name + ".force_feedback_lim_hi"};
  ApiRef<F, API_ACTUATOR> act_pos_feedback_coeff {owner, name + ".pos_feedback_coeff"};
  ApiRef<F, API_ACTUATOR> act_pos_feedback_lim_lo {owner, name + ".pos_feedback_lim_lo"};
  ApiRef<F, API_ACTUATOR> act_pos_feedback_lim_hi {owner, name + ".pos_feedback_lim_hi"};

  ApiSection clapperSection {owner, name + " clapper"};

  ApiRef<F, API_SENSOR> senseClapperDigit {owner, name + ".clapperDigit"};
  ApiRef<F, API_SENSOR> senseCamtrig {owner, name + ".camtrig"};
  
  SscHarnessApi(SheetApi &_owner, string const &_name)
    :owner(_owner),
     name(_name)
  {
  }

  void rxLiftState(packet &rx, Blob &vals)
  {
    rxDsp824(rx, sense_ext_pressure.replayVal(vals));
    rxDsp824(rx, sense_ret_pressure.replayVal(vals));
    rxDsp824(rx, sense_pos.replayVal(vals));
    rxDsp1616(rx, senseForce.replayVal(vals));
    rxDsp1616(rx, sense_pos_feedback.replayVal(vals));
    rxDsp824(rx, senseForce_feedback.replayVal(vals));
    rxDsp824(rx, sense_valve.replayVal(vals));
  }

  void rxHarnessState(packet &rx, Blob &vals)
  {
    rxLiftState(rx, vals);
  }

  void rxClapperEvent(packet &rx, Blob &vals)
  {
    uint8_t digitRaw;
    rx.get(digitRaw);
    senseClapperDigit.replayVal(vals) = (F)(S32)(U32)digitRaw;
  }

  void rxCamtrigEvent(packet &rx, Blob &vals)
  {
    uint8_t trigiRaw;
    rx.get(trigiRaw);
    senseCamtrig.replayVal(vals) = (F)(S32)(U32)trigiRaw;
  }


  void txLiftAct(packet &tx, CellVals *vals)
  {
    txDsp824(tx, act_base_valve.memVal(vals));
    txDsp1616(tx, act_desired_force.memVal(vals));
    txDsp824(tx, act_desired_pos.memVal(vals));
    txDsp1616(tx, act_ext_pressure_to_force.memVal(vals));
    txDsp1616(tx, act_ret_pressure_to_force.memVal(vals));
    txDsp824(tx, act_force_feedback_coeff.memVal(vals));
    txDsp824(tx, act_force_feedback_lim_lo.memVal(vals));
    txDsp824(tx, act_force_feedback_lim_hi.memVal(vals));
    txDsp1616(tx, act_pos_feedback_coeff.memVal(vals));
    txDsp1616(tx, act_pos_feedback_lim_lo.memVal(vals));
    txDsp1616(tx, act_pos_feedback_lim_hi.memVal(vals));
  }

  void txHarnessAct(packet &tx, CellVals *vals)
  {
    txLiftAct(tx, vals);
    tx.add((U8)0); // was camtrigMask
  }

};

/*
  Be careful to increment the API version number below when adding or deleting
  or rearranging any ApiRefs.
  TODO: we save the exact schema in the replaySchema table, so we should be
  able to upgrade versions with an external tool
*/

struct Dex4Api : SheetApi {
  
#if !defined(__EMSCRIPTEN__)
  shared_ptr<SscNetworkEngine> lLegEngine;
  shared_ptr<SscNetworkEngine> rLegEngine;
  shared_ptr<SscNetworkEngine> lFootEngine;
  shared_ptr<SscNetworkEngine> rFootEngine;
  shared_ptr<SscNetworkEngine> gyroEngine;
  shared_ptr<SscNetworkEngine> harnessEngine;
#endif

  bool
    lLegOk = false,
    rLegOk = false,
    lFootOk = false,
    rFootOk = false,
    gyroOk = false,
    harnessOk = false;

  SscPneuservoApi lht{*this, "lht"};
  SscPneuservoApi lhf{*this, "lhf"};
  SscPneuservoApi lhs{*this, "lhs"};
  SscPneuservoApi lki{*this, "lki"};
  SscPneuservoApi lko{*this, "lko"};
  SscPneuservoApi lai{*this, "lai"};
  SscPneuservoApi lao{*this, "lao"};

  SscPneuservoApi rht{*this, "rht"};
  SscPneuservoApi rhf{*this, "rhf"};
  SscPneuservoApi rhs{*this, "rhs"};
  SscPneuservoApi rki{*this, "rki"};
  SscPneuservoApi rko{*this, "rko"};
  SscPneuservoApi rai{*this, "rai"};
  SscPneuservoApi rao{*this, "rao"};

  SscRegulatorApi reg0{*this, "reg0"};

  SscFootApi lf{*this, "lf"};
  SscFootApi rf{*this, "rf"};

  SscGyroApi gyro{*this, "imu"};

  SscHarnessApi harness{*this, "harness"};

  ApiSection overallSection{*this, "overall"};
  ApiRef<F, API_ACTUATOR> actValveOpenMode{*this, "valveOpenMode"};

  Dex4Api(CompiledSheet &_compiled, ApiSpec const &_spec)
    : SheetApi(_compiled, _spec, "dex4.3")
  {
    compiled.hasLiveApi = true;
  }

#if !defined(__EMSCRIPTEN__)
  void txWarmup(LiveRobotState &lrt) override;
  void txActuators(CellVals *vals, LiveRobotState &lrt) override;
  void txUpdate(R &now, packet &tx, SscNetworkEngine *engine);

  void setupEngines();
  bool startLive() override;
  void stopLive() override;
#endif

};

static ApiRegister reg("dex4", [](CompiledSheet &_compiled, ApiSpec const &_spec) {
  return make_shared<Dex4Api>(_compiled, _spec);
});

#if !defined(__EMSCRIPTEN__)
bool Dex4Api::startLive()
{
  if (!SheetApi::startLive()) return false;
  setupEngines();
  return true;
}

void Dex4Api::stopLive()
{
  SheetApi::stopLive();
  if (lLegEngine) {
    lLegEngine->closeSockets();
    lLegEngine = nullptr;
  }
  if (rLegEngine) {
    rLegEngine->closeSockets();
    rLegEngine = nullptr;
  }
  if (lFootEngine) {
    lFootEngine->closeSockets();
    lFootEngine = nullptr;
  }
  if (rFootEngine) {
    rFootEngine->closeSockets();
    rFootEngine = nullptr;
  }
  if (gyroEngine) {
    gyroEngine->closeSockets();
    gyroEngine = nullptr;
  }
  if (harnessEngine) {
    harnessEngine->closeSockets();
    harnessEngine = nullptr;
  }
}

void Dex4Api::setupEngines()
{
  if (auto h = assoc_string_view(apiProps, "l_leg_host")) {
    lLegEngine = make_shared<SscNetworkEngine>("l_leg", string(*h), "10000");
    lLegEngine->updateHandlers[(U8)'l'] = [this](R rxTime, packet &rx, U64 pktTicks, bool &error) {
      R ts;
      rxSscTimestamp(*rLegEngine, rx, ts, rxTime, pktTicks);

      reg0.rxRegulatorState(rx, lastData);
      lhs.rxPneuservoState(rx, lastData);
      lki.rxPneuservoState(rx, lastData);
      lhf.rxPneuservoState(rx, lastData);
      lht.rxPneuservoState(rx, lastData);
      lko.rxPneuservoState(rx, lastData);
      lai.rxPneuservoState(rx, lastData);
      lao.rxPneuservoState(rx, lastData);
      lLegOk = true;
    };
    lLegEngine->start();
  }

  if (auto h = assoc_string_view(apiProps, "r_leg_host")) {
    rLegEngine = make_shared<SscNetworkEngine>("r_leg", string(*h), "10000");
    rLegEngine->updateHandlers[(U8)'l'] = [this](R rxTime, packet &rx, U64 pktTicks, bool &error) {
      R ts;
      rxSscTimestamp(*rLegEngine, rx, ts, rxTime, pktTicks);

      rao.rxPneuservoState(rx, lastData);
      rai.rxPneuservoState(rx, lastData);
      rko.rxPneuservoState(rx, lastData);
      rht.rxPneuservoState(rx, lastData);
      rhf.rxPneuservoState(rx, lastData);
      rki.rxPneuservoState(rx, lastData);
      rhs.rxPneuservoState(rx, lastData);
      rLegOk = true;
    };
    rLegEngine->start();
  }

  if (auto h = assoc_string_view(apiProps, "l_foot_host")) {
    lFootEngine = make_shared<SscNetworkEngine>("l_foot", string(*h), "10000");
    lFootEngine->updateHandlers[(U8)'f'] = [this](R rxTime, packet &rx, U64 pktTicks, bool &error) {
      R ts;
      rxSscTimestamp(*rLegEngine, rx, ts, rxTime, pktTicks);
      lf.rxFootState(rx, lastData);
      lFootOk = true;
    };
    lFootEngine->start();
  }

  if (auto h = assoc_string_view(apiProps, "r_foot_host")) {
    rFootEngine = make_shared<SscNetworkEngine>("r_foot", string(*h), "10000");
    rFootEngine->updateHandlers[(U8)'f'] = [this](R rxTime, packet &rx, U64 pktTicks, bool &error) {
      R ts;
      rxSscTimestamp(*rLegEngine, rx, ts, rxTime, pktTicks);
      rf.rxFootState(rx, lastData);
      rFootOk = true;
    };
    rFootEngine->start();
  }


  if (auto h = assoc_string_view(apiProps, "harness_host")) {
    harnessEngine = make_shared<SscNetworkEngine>("harness", string(*h), "10000");
    harnessEngine->updateHandlers[(U8)'h'] = [this](R rxTime, packet &rx, U64 pktTicks, bool &error) {
      R ts;
      rxSscTimestamp(*rLegEngine, rx, ts, rxTime, pktTicks);
      harness.rxHarnessState(rx, lastData);
      harnessOk = true;
    };
    harnessEngine->updateHandlers[(U8)'c'] = [this](R rxTime, packet &rx, U64 pktTicks, bool &error) {
      R ts;
      rxSscTimestamp(*rLegEngine, rx, ts, rxTime, pktTicks);
      harness.rxClapperEvent(rx, lastData);
    };
    harnessEngine->updateHandlers[(U8)'t'] = [this](R rxTime, packet &rx, U64 pktTicks, bool &error) {
      R ts;
      rxSscTimestamp(*rLegEngine, rx, ts, rxTime, pktTicks);
      harness.rxCamtrigEvent(rx, lastData);
    };
    harnessEngine->start();
  }

  if (auto h = assoc_string_view(apiProps, "gyro_host")) {
    gyroEngine = make_shared<SscNetworkEngine>("gyro", string(*h), "10000");
    gyroEngine->updateHandlers[(U8)'a'] = [this](R rxTime, packet &rx, U64 pktTicks, bool &error) {
      R ts;
      rxSscTimestamp(*rLegEngine, rx, ts, rxTime, pktTicks);
      U32 seqno;
      rx.get(seqno);
      gyro.rxGyroState(rx, lastData);
      gyroOk = true;
    };
    gyroEngine->start();
  }
}

void Dex4Api::txUpdate(R &now, packet &tx, SscNetworkEngine *engine)
{
  if (now - engine->lastUpdateRequestTs > 0.2) {
    tx.add((U8)'U');
    engine->lastUpdateRequestTs = now;
  }
}

void Dex4Api::txWarmup(LiveRobotState &lrt)
{
  R now = realtime();

  if (auto e = lLegEngine.get(); e && e->isReady()) {
    packet tx;
    txUpdate(now, tx, e);
    e->txPkt(tx);
  }
  if (auto e = rLegEngine.get(); e && e->isReady()) {
    packet tx;
    txUpdate(now, tx, e);
    e->txPkt(tx);
  }
  if (auto e = lFootEngine.get(); e && e->isReady()) {
    packet tx;
    txUpdate(now, tx, e);
    e->txPkt(tx);
  }
  if (auto e = rFootEngine.get(); e && e->isReady()) {
    packet tx;
    txUpdate(now, tx, e);
    e->txPkt(tx);
  }
  if (auto e = gyroEngine.get(); e && e->isReady()) {
    packet tx;
    txUpdate(now, tx, e);
    e->txPkt(tx);
  }
  if (auto e = harnessEngine.get(); e && e->isReady()) {
    packet tx;
    txUpdate(now, tx, e);
    e->txPkt(tx);
  }

  if (!lLegOk) lrt.needMoreWarmup.push_back(lLegEngine ? lLegEngine->label : "l_leg(null)");
  if (!rLegOk) lrt.needMoreWarmup.push_back(rLegEngine ? rLegEngine->label : "r_leg(null)");
  if (!lFootOk) lrt.needMoreWarmup.push_back(lFootEngine ? lFootEngine->label : "l_foot(null)");
  if (!rFootOk) lrt.needMoreWarmup.push_back(rFootEngine ? rFootEngine->label : "r_foot(null)");
  if (!gyroOk) lrt.needMoreWarmup.push_back(gyroEngine ? gyroEngine->label : "gyro(null)");
  if (!harnessOk) lrt.needMoreWarmup.push_back(harnessEngine ? harnessEngine->label : "harness(null)");

}

void Dex4Api::txActuators(CellVals *vals, LiveRobotState &lrt)
{

  R now = realtime();
  if (auto e = lLegEngine.get(); e && e->isReady()) {
    packet tx;
    txUpdate(now, tx, e);
    tx.add((U8)'V');
    tx.add(actValveOpenMode.memVal(vals) > 0.5f ? (U8)'O' : (U8)'.');
    reg0.txRegulatorAct(tx, vals);
    lhs.txPneuservoAct(tx, vals);
    lki.txPneuservoAct(tx, vals);
    lhf.txPneuservoAct(tx, vals);
    lht.txPneuservoAct(tx, vals);
    lko.txPneuservoAct(tx, vals);
    lai.txPneuservoAct(tx, vals);
    lao.txPneuservoAct(tx, vals);
    e->txPkt(tx);
  }
  if (auto e = rLegEngine.get(); e && e->isReady()) {
    packet tx;
    txUpdate(now, tx, e);
    tx.add((U8)'V');
    tx.add(actValveOpenMode.memVal(vals) > 0.5f ? (U8)'O' : (U8)'.');
    rao.txPneuservoAct(tx, vals);
    rai.txPneuservoAct(tx, vals);
    rko.txPneuservoAct(tx, vals);
    rht.txPneuservoAct(tx, vals);
    rhf.txPneuservoAct(tx, vals);
    rki.txPneuservoAct(tx, vals);
    rhs.txPneuservoAct(tx, vals);
    e->txPkt(tx);
  }
  if (auto e = lFootEngine.get(); e && e->isReady()) {
    packet tx;
    txUpdate(now, tx, e);
    e->txPkt(tx);
  }
  if (auto e = rFootEngine.get(); e && e->isReady()) {
    packet tx;
    txUpdate(now, tx, e);
    e->txPkt(tx);
  }
  if (auto e = gyroEngine.get(); e && e->isReady()) {
    packet tx;
    txUpdate(now, tx, e);
    e->txPkt(tx);
  }
  if (auto e = harnessEngine.get(); e && e->isReady()) {
    packet tx;
    txUpdate(now, tx, e);
    tx.add((U8)'V');
    harness.txHarnessAct(tx, vals);
    e->txPkt(tx);
  }
}
#endif
