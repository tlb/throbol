#pragma once
#include "../src/api.h"
#include "common/packetbuf.h"

const double SSC_INVERSE_TIME_QUANTUM = 1048576;                // binary microseconds
const double SSC_TIME_QUANTUM = 1.0 / SSC_INVERSE_TIME_QUANTUM; // binary microseconds


struct SscRawDump {
  double ts = 0.0;
  vector<double> values;
};

DECL_PACKETIO(SscRawDump);

template <class T>
double sscConvTimestamp(T &engine, double rxTime, U64 sampleTicks, U64 pktTicks)
{
  double sampleAge = (S64)(pktTicks - sampleTicks) * (1.0 / 66.0e6);
  if (sampleAge < 0 || sampleAge > 0.010) {
    L() << "sscConvTimestamp: " << engine.label <<
      ": sampleAge=" << repr_0_6f(sampleAge) <<
      " (sampleTicks=" << repr_016x(sampleTicks) <<
      " pktTicks=" << repr_016x(pktTicks) <<
      ")\n";
  }
  return round((rxTime - sampleAge) * SSC_INVERSE_TIME_QUANTUM) * SSC_TIME_QUANTUM;
}

template <class T>
void rxSscTimestamp(T &engine, packet &rx, double &ts, double rxTime, U64 pktTicks)
{
  U64 sampleTicks;
  rx.get(sampleTicks);
  if (sampleTicks != 0) {
    R tsExt = 0.0;
    tsExt = sscConvTimestamp(engine, rxTime, sampleTicks, pktTicks);
    ts = engine.fromExternalTime(tsExt);
  }
  else {
    ts = 0.0;
  }
}

// ----------------------------------------------------------------------

inline void rxDsp824(packet &rx, F &value)
{
  S32 raw;
  rx.get(raw);
  value = (F)raw * (1.0f / 16777216.0f);
}

inline void txDsp824(packet &tx, F const &value)
{
  S32 raw = (S32)(min(125.0f, max(-125.0f, value)) * 16777216.0f);
  tx.add(raw);
}

inline void rxDsp1616(packet &rx, F &value)
{
  S32 raw = 0;
  rx.get(raw);
  value = (F)raw * (1.0f / 65536.0f);
}

inline void rxBool(packet &rx, F &value)
{
  U8 raw = 0;
  rx.get(raw);
  value = raw ? 1.0f : 0.0f;
}

inline void txDsp1616(packet &tx, F const &value)
{
  S32 raw = (S32)(min(32500.0f, max(-32500.0f, value)) * 65535.0f);
  tx.add(raw);
}


inline F sscConvGyro(S32 raw)
{
  // Convert to radians/sec, see table 10 & 14.
  return F(raw * (0.02 * (M_PI / 180.0) / 65536.0));
}

inline F sscConvAccel(S32 raw)
{
  // Convert to m/s/s, see table 17
  return F(raw * (0.0008 * 9.81 / 65536.0));
}

inline F sscConvDeltang(S32 raw)
{
  // Convert to radians
  return F(raw * (4.0 * M_PI / 32768.0 / 65536.0));
}

inline F sscConvDeltvel(S32 raw)
{
  // Conver to m/s, see table 31
  return F(raw * (200.0 / 32768.0 / 65536.0));
}

inline F sscConvBarom(S32 raw)
{
  // Convert to bar
  return F(raw * (0.000040 / 65536.0));
}

inline F sscConvOrient(S16 raw)
{
  // Convert to orientation matrix entries, see table 42
  return F(raw * (1.0 / 32768.0));
}

inline F sscConvMag(S16 raw)
{
  // Convert to gauss, see table 38
  // Earth's magnetic field in Northern California is about 0.5 gauss with an incliation of -52 degrees
  // https://en.wikipedia.org/wiki/Earth%27s_magnetic_field
  return F(raw * 0.0001);
}

inline F sscConvTemperature(S16 raw)
{
  // Convert to celcius
  return F(raw * 0.00565 + 25.0);
}

string sscFmtSysEFlag(U16 v);
string sscFmtDiagSts(U16 v);
string sscFmtAlmSts(U16 v);
