#pragma once
#include "./univarm.h"
#include "./universal_network_utils.h"

struct UniversalRobotControl : enable_shared_from_this<UniversalRobotControl> {

  string rHost, rPort;
  string label;
  asio::ip::tcp::socket sock{io_context};
  int verbose = 2;

  std::function<void(packet &rx, R rxTime)> onRxStatus;
  std::function<void(packet &rx, R rxTime)> onRxKinematics;
  std::function<void(YAML::Node &rx, R rxTime)> onRxGlobalVarDump;
  std::function<void(packet &rx, R rxTime)> onRobotStateModeData;
  std::function<void(packet &rx, R rxTime)> onRobotStateJointData;

  enum {
    S_init,
    S_waitVersion,
    S_running,
    S_eof,
    S_error,

  } state{S_init};

  string mainScript;

  string rxBuf;
  bool rxAnyFlag = false;

  int rxCommCount = 0;
  int txScriptCount = 0;

  vector<string> globalVarNames;


  UniversalRobotControl(string _rHost, string _rPort)
      : rHost(_rHost),
        rPort(_rPort),
        label("urctl(" + rHost + ":" + rPort + ")")
  {
  }

  bool error()
  {
    return state == S_error;
  }

  bool eof()
  {
    return state == S_eof;
  }

  void start()
  {
    asio::ip::tcp::resolver resolver(io_context);
    boost::system::error_code resolveError;
    // FIXME: should be async
    auto eps = resolver.resolve(asio::ip::tcp::v4(), rHost, rPort, resolveError);
    if (resolveError) {
      L() << label << ": resolve: " << resolveError;
      return;
    }

    if (verbose >= 1) L() << label << ": connecting...";
    asio::async_connect(sock, eps,
      [this, keepalive=shared_from_this()](const boost::system::error_code error, asio::ip::tcp::endpoint const &endpoint) {
        if (error) {
          L() << label << ": " << error.message();
          state = S_error;
          return;
        }
        sock.set_option(asio::ip::tcp::no_delay(true));
        commStart();
        return asyncRx();
      });
  }

  void asyncRx()
  {
    size_t rxBufStart = rxBuf.size();
    int const maxLen = 4096;
    rxBuf.resize(rxBuf.size() + maxLen);
    sock.async_read_some(asio::buffer(rxBuf.data() + rxBufStart, maxLen),
      [this, rxBufStart](boost::system::error_code error, size_t nr) {
        if (error) {
          if (error == asio::error::eof) {
            state = S_eof;
            return;
          }
          L() << label << ": " << error.message();
          state = S_error;
          return;
        }
        rxBuf.resize(rxBufStart + nr);

        double rxTime = realtime();
        forEachControlPacket(rxBuf, [this, rxTime](packet &rx) {
          rxPkt(rx, rxTime);
        });
        asyncRx();
      });
  }

  void close()
  {
    if (sock.is_open()) {
      sock.close();
    }
  }

  void commStart() {
    state = S_waitVersion;
  }

  void rxEof()
  {
    L() << label << " > EOF\n";
    state = S_eof;
  }

  void rxPkt(packet &rx, double rxTime)
  {
    auto pktType = rx.get_be_uint8();
    if (pktType == 0) {
      if (verbose >= 2) L() << label << " > robot_mode_data\n";
    }
    else if (pktType == 1) {
      if (verbose >= 2) L() << label << " > joint_data\n";
    }
    else if (pktType == 20) {
      auto timestamp = rx.get_be_uint64();
      (void)timestamp;
      auto source = rx.get_be_uint8();
      auto robotMessageType = rx.get_be_uint8();
      if (robotMessageType == 3) {
        string projectName = rx.get_len8_string();
        int majorVersion = rx.get_be_uint8();
        int minorVersion = rx.get_be_uint8();
        int bugfixVersion = rx.get_be_uint32();
        int buildNumber = rx.get_be_uint32();
        string buildDate = rx.get_remainder_string();
        if (verbose >= 1) L() << label << " > " << universalMessageSourceName(source) <<
           " version: projectName=" << projectName << " " << majorVersion << "." << minorVersion << "." <<
            bugfixVersion << "." << buildNumber << " on " << buildDate;
        if (state == S_waitVersion) {
          if (mainScript.size()) {
            txScript(mainScript);
          }
          state = S_running;
        }
      }
      else if (robotMessageType == 6) {
        if (verbose >= 3) L() << label << " > " << universalMessageSourceName(source) << " comm: pkt=" << rx.dump_hex();

        auto code = rx.get_be_uint32();
        auto arg = rx.get_be_uint32();
        auto level = rx.get_be_uint32();
        auto dataType = rx.get_be_uint32();
        auto text = rx.get_remainder_string();
        rxCommCount++;
        if (verbose >= 1) L() << label << " > " << universalMessageSourceName(source) <<
          " comm #"  << rxCommCount << " code=" << code <<
          " arg=" << arg << " level=" << universalReportLevelName(level) <<
          " dataType=" << dataType << " text=" + shellEscape(text);
      }
      else if (robotMessageType == 10) {
        auto scriptLineNumber = rx.get_be_uint32();
        auto scriptColNumber = rx.get_be_uint32();
        auto text = rx.get_remainder_string();
        if (verbose >= 1) L() << label << " > " << universalMessageSourceName(source) << 
          " runtime_exception at " << scriptLineNumber << ":" << scriptColNumber <<
          " " << shellEscape(text);
      }
      else if (robotMessageType == 1) {
        auto id = rx.get_be_uint32();
        auto text = rx.get_remainder_string();
        if (verbose >= 1) L() << label << " > " << universalMessageSourceName(source) <<
          " id=" << id << ": " << shellEscape(text);
      }
      else if (robotMessageType == 0) {
        auto text = rx.get_remainder_string();
        if (verbose >= 1) L() << label << " > " << universalMessageSourceName(source) <<
          " " << shellEscape(text);
      }
      else if (robotMessageType == 7) {
        auto code = rx.get_be_uint32();
        auto arg = rx.get_be_uint32();
        auto title = rx.get_len8_string();
        auto text = rx.get_remainder_string();
        if (verbose >= 1) L() << label << " > " << universalMessageSourceName(source) <<
          " KeyMessage: code=" << code << " arg=" << arg <<
          " title=" << shellEscape(title) << " " << shellEscape(text);
        if (title == "PROGRAM_XXX_STOPPED") {
          state = S_error;
        }
      }
      else {
        if (verbose >= 2) L() << label << " > " << universalMessageSourceName(source) <<
          " unknown message pktType=" << int(pktType) <<
          " robotMessageType=" << int(robotMessageType) <<
          " pkt=" << rx.dump_hex();
      }
    }
    else if (pktType == 5) {
      if (rx.remaining() == 4) {
        if (verbose >= 4) L() << label << " > kinematics_info <empty>\n";
      }
      else {
        if (verbose >= 2) L() << label << " > kinematics_info\n";
        if (onRxKinematics) onRxKinematics(rx, rxTime);
      }
    }
    else if (pktType == 25) {
      auto timestamp = rx.get_be_uint64();
      (void)timestamp;
      auto robotMessageType = rx.get_be_uint8();
      if (robotMessageType == 2) { // variable_update
        if (verbose >= 2) L() << label << " > variable_update (WRITEME)\n";
      }
      else if (robotMessageType == 0) { // global_variables_setup
        auto startIndex = rx.get_be_uint16();
        string allVars = rx.get_remainder_string();
        if (verbose >= 2) L() << label << " > global variables setup "  << startIndex << " " << shellEscape(allVars);
        auto allVarsVec = splitNewlines(allVars);
        globalVarNames.resize(max(globalVarNames.size(), startIndex + allVarsVec.size()));
        for (size_t i = 0; i < allVarsVec.size(); i++) {
          globalVarNames[i + startIndex] = allVarsVec[i];
        }
      }
      else if (robotMessageType == 1) { // global_variables_update
        size_t startIndex = (size_t)rx.get_be_uint16();
        YAML::Node globalVars;
        while (rx.remaining() > 0) {
          auto varDump = parseVariableDump(rx);
          string varName = startIndex < globalVarNames.size() ? globalVarNames[startIndex] : "global"s + repr(startIndex);
          globalVars[varName] = varDump;
          if (rx.remaining() > 0) {
            auto nl = rx.get_be_uint8(); // newline terminator, despite otherwise binary format
            assert(nl == '\n');
            startIndex++;
          }
        }
        if (verbose >= 4) L() << label << " > global var update pkt=" << rx.dump_hex();
        if (verbose >= 3) L() << label << " > global var update " << globalVars;
        if (onRxGlobalVarDump) onRxGlobalVarDump(globalVars, rxTime);
      }
      else {
        if (verbose >= 2) L() << label << " > pktType=" << int(pktType) << 
          " robotMessageType=" << int(robotMessageType) << " (WRITEME)\n";
      }
    }
    else if (pktType == 16) { // RobotState. Supposedly only on secondary IF, but I see it on primary
      while (rx.remaining() > 0) {
        auto subPackageLen = rx.get_be_uint32();
        assertlog(subPackageLen < 10000, LOGV(label) << LOGV(subPackageLen));
        assertlog(rx.remaining() >= subPackageLen - 4, LOGV(rx.remaining()) << LOGV(subPackageLen));

        auto subPackageType = rx.get_be_uint8();
        if (subPackageType == 0) { // mode data
          auto timestamp = rx.get_be_uint64();
          (void)timestamp;
          if (verbose >= 3) L() << label << " > mode data\n";
          if (onRobotStateModeData) {
            onRobotStateModeData(rx, rxTime);
          }
          else {
            rx.get_skip(subPackageLen - 5 - 8);
          }
        }
        else if (subPackageType == 1) { // joint data
          if (verbose >= 3) L() << label << " > joint data\n";
          if (onRobotStateJointData) {
            onRobotStateJointData(rx, rxTime);
          }
          else {
            rx.get_skip(subPackageLen - 5);
          }
        }
        else {
          if (verbose >= 3) L() << label << " > robot state packageType=" << int(subPackageType) << " len=" << subPackageLen;
          rx.get_skip(subPackageLen - 5);
        }
      }
    }
    else if (pktType == 23 || pktType == 24) {
      if (verbose >= 3) L() << label << " > message type " << int(pktType) << " ignoring pkt=" << rx.dump_hex();
    }
    else {
      if (verbose >= 2) L() << label << " > unknown message " << int(pktType) << " pkt=" << rx.dump_hex();
    }
  }

  void txScript(string const &script)
  {
    txScriptCount++;
    if (verbose >= 1) {
      L() << label << " < script #" << txScriptCount << " " << shellEscape(script.substr(0, 50) + "...");
    }
    boost::system::error_code writeError;
    asio::write(sock, asio::const_buffer(script.data(), script.size()), writeError);
    if (writeError) {
      L() << label << ": write: " << writeError.message();
    }
  }

};
