#include "../src/api.h"
#include "../src/emit.h"
#include "../src/mjutil.h"

static map<string, shared_ptr<mjModel>> modelCache;

struct MjDataHolder : AllocTrackingMixin<MjDataHolder> {
  explicit MjDataHolder(mjModel *_m)
   : d(mj_makeData(_m))
  {
    static bool didlog;
    if (!didlog) {
      if (0) L() << "Mujoco state size: stack=" << d->nstack*sizeof(mjtNum) << " buffer=" << d->nbuffer;
      didlog = true;
    }
  }
  ~MjDataHolder()
  {
    mj_deleteData(d);
    d = nullptr;
  }
  MjDataHolder(MjDataHolder const &other) = delete;
  MjDataHolder(MjDataHolder &&other) = delete;
  MjDataHolder & operator = (MjDataHolder const &other) = delete;

  mjData *d;
};

struct MjModelDeleter {
  void operator()(mjModel *p) const { mj_deleteModel(p); }
};

struct MjScene : AllocTrackingMixin<MjScene> {
  explicit MjScene()
  {
    mjv_defaultScene(&scn);
  }
  ~MjScene()
  {
    mjv_freeScene(&scn);
  }
  mjvScene scn;
};


struct MujocoApi : SheetApi, enable_shared_from_this<MujocoApi>, AllocTrackingMixin<MujocoApi> {

  shared_ptr<mjModel> m;

  // rx means receiving from the simulation to the spreadsheet.
  vector<function<void(mjData *, CellVals *, Trace *)>> rxops;
  vector<function<void(mjData *, CellVals *)>> txops;

  ApiRef<DrawOpHandle, API_MONITOR> renderScene{*this, "render.scene"};

  mjvOption opt;                      // visualization options
  mjvCamera cam;
  int geomAllocSize = 100;

  explicit MujocoApi(CompiledSheet &_compiled, ApiSpec const &_spec)
    : SheetApi(_compiled, _spec, "muj1.0")
  {
    verbose = 0;
    compiled.hasSimApi = true;

    loadModel();
    if (!ok()) return;

    mjv_defaultCamera(&cam);
    mjv_defaultOption(&opt);

    setupSensors();
    setupBodies();
    setupJoints();
    setupActuators();
    setupRender();
  }

  virtual ~MujocoApi()
  {
  }

  void loadModel()
  {
    char error[1000] = "Could not load binary model";
    string modelFn;

    auto page = sh.pageByCell[apiCell];
    auto pageFileName = sh.fileNameByPage[page];
    auto pageFileDir = myDirname(pageFileName);

    if (auto it = assoc_string_view(apiProps, "model")) {
      modelFn = *it;
      if (modelFn.at(0) != '/') {
        modelFn = pageFileDir + "/"s + modelFn;
      }
    }
    if (modelFn.empty()) {
      logError("No mujoco model specified");
      return;
    }

    // Reading XML models is expensive and we create new API objects
    // every time we compile, so cache it here.
    auto &mslot = modelCache[modelFn];
    if (!mslot) {
      mjModel *loaded = nullptr;
      if (endsWith(modelFn, ".mjb")) {
        loaded = mj_loadModel(modelFn.c_str(), 0);
      }
      else {
        loaded = mj_loadXML(modelFn.c_str(), 0, error, sizeof(error));
      }
      if (!loaded) {
        logError("Mujoco Load model error: "s + error);
        return;        
      }
      mslot = shared_ptr< mjModel >(loaded, MjModelDeleter());
    }
    m = mslot;

    if (verbose >= 1) ll() << 
      "nq=" << m->nq <<
      " nv=" << m->nv <<
      " nu=" << m->nu <<
      " na=" << m->na <<
      " nbody=" << m->nbody <<
      " njnt=" << m->njnt <<
      " nsensor=" << m->nsensor;
  }

  string getname(int adr, string const &prefix, int defaultIndex)
  {
    auto namep = &m->names[adr];
    if (*namep) {
      auto ret = prefix;
      for (char const *p = namep; *p; p++) {
        if (*p == '-') {
          ret += '_';
        }
        else {
          ret += *p;
        }
      }
      return ret;
    }
    else {
      return prefix + "n"s + to_string(defaultIndex);
    }
  }


  void setupSensors()
  {
    for (int si = 0; si < m->nsensor; si++) {

      auto name = getname(m->name_sensoradr[si], "s.", si);

      auto type = m->sensor_type[si];
      mjtNum cutoff = m->sensor_cutoff[si];
      int sensor_adr = m->sensor_adr[si];
      int sensor_dim = m->sensor_dim[si];

      if (verbose >= 1) ll() << "Sensor " << si << 
        " type=" << type << " cutoff=" << cutoff <<
        " adr=" << sensor_adr << " dim=" << sensor_dim;

      auto sen = mkDynamic<F, API_SENSOR>(name);

      rxops.push_back([
        sen,
        sensor_adr
      ](mjData *d, CellVals *vals, Trace *tr) {
        sen->memVal(vals) = d->sensordata[sensor_adr];
      });

    }
  }

  void setupBodies()
  {
    for (int bi = 0; bi < m->nbody; bi++) {
      auto name = getname(m->name_bodyadr[bi], "body.", bi);

      auto sen = mkDynamic<mat4, API_SENSOR>(name);

      if (verbose >= 1) ll() << "body[" << bi << "] " << name;

      rxops.push_back([
        sen,
        bi
      ](mjData *d, CellVals *vals, Trace *tr) {
        sen->memVal(vals) = mjMat4FromQuatPos(&d->xquat[bi * 4], &d->xpos[bi * 3]);
      });

    }
  }

  void setupJoints()
  {
    for (int ji = 0; ji < m->njnt; ji++) {
      auto qposadr = m->jnt_qposadr[ji];
      auto qveladr = m->jnt_dofadr[ji];

      if (m->jnt_type[ji] == mjJNT_HINGE) {

        auto posname = getname(m->name_jntadr[ji], "j.", ji);
        auto velname = getname(m->name_jntadr[ji], "jvel.", ji);

        auto pos_sense = mkDynamic<F, API_SENSOR>(posname);
        auto vel_sense = mkDynamic<F, API_SENSOR>(velname);

        F lo = -M_PI, hi = M_PI;
        if (m->jnt_limited[ji]) {
          lo = m->jnt_range[2*ji];
          hi = m->jnt_range[2*ji+1];
          pos_sense->comment = "range=[" + repr_0_3g(lo) + "," + repr_0_3g(hi) + "]";
        }

        if (verbose >= 1) ll() << "Joint hinge " << ji <<
          " name=" << posname << "/" << velname <<
          " lo=" << lo << " hi=" << hi << 
          " qposadr=" << qposadr << 
          " qveladr=" << qveladr;


        rxops.push_back(
          [
            pos_sense,
            vel_sense,
            qposadr,
            qveladr
          ](mjData *d, CellVals *vals, Trace *tr) {
            pos_sense->memVal(vals) = d->qpos[qposadr];
            vel_sense->memVal(vals) = d->qvel[qveladr];
          }
        );
      }

      else if (m->jnt_type[ji] == mjJNT_SLIDE) {

        auto posname = getname(m->name_jntadr[ji], "j.", ji);
        auto velname = getname(m->name_jntadr[ji], "jvel.", ji);
        auto pos_sense = mkDynamic<F, API_SENSOR>(posname);
        auto vel_sense = mkDynamic<F, API_SENSOR>(velname);

        F lo = -1, hi = 1;
        if (m->jnt_limited[ji]) {
          lo = m->jnt_range[2*ji];
          hi = m->jnt_range[2*ji+1];
          pos_sense->comment = "range=[" + repr_0_3g(lo) + "," + repr_0_3g(hi) + "]";
        }

        if (verbose >= 1) ll() << "Joint slide " << ji <<
          " name=" << posname << "/" << velname <<
          " lo=" << lo << " hi=" << hi << 
          " qposadr=" << qposadr << 
          " qveladr=" << qveladr;

        rxops.push_back(
          [
            pos_sense,
            vel_sense,
            qposadr,
            qveladr
          ](mjData *d, CellVals *vals, Trace *tr) {
            pos_sense->memVal(vals) = d->qpos[qposadr];
            vel_sense->memVal(vals) = d->qvel[qveladr];
          }
        );
      }
      else if (m->jnt_type[ji] == mjJNT_BALL) {

        auto posname = getname(m->name_jntadr[ji], "j.", ji);
        auto pos_sense = mkDynamic<mat3, API_SENSOR>(posname);

        F lo = -1, hi = 1;
        if (m->jnt_limited[ji]) {
          lo = m->jnt_range[2*ji];
          hi = m->jnt_range[2*ji+1];
          pos_sense->comment = "range=[" + repr_0_3g(lo) + "," + repr_0_3g(hi) + "]";
        }

        if (verbose >= 1) ll() << "Joint ball " << ji <<
          " name=" << posname <<
          " lo=" << lo << " hi=" << hi << 
          " qposadr=" << qposadr;

        rxops.push_back(
          [
            pos_sense,
            qposadr
          ](mjData *d, CellVals *vals, Trace *tr) {
            pos_sense->memVal(vals) = mjMat3FromQuat(&d->qpos[qposadr]);
          }
        );
      }      
      else if (m->jnt_type[ji] == mjJNT_FREE) {

        auto posname = getname(m->name_jntadr[ji], "orient.", ji);

        if (verbose >= 1) ll() << "Joint free " << ji <<
          " name=" << posname <<
          " qposadr=" << qposadr;

        auto pos_sense = mkDynamic<mat4, API_SENSOR>(posname);

        rxops.push_back(
          [
            pos_sense,
            qposadr
          ](mjData *d, CellVals *vals, Trace *tr) {
            // We have 3 entries for position, 4 for an orientation quaternion
            pos_sense->memVal(vals) = mjMat4FromQuatPos(&d->qpos[qposadr + 3], &d->qpos[qposadr + 0]);
          }
        );

      }
      else {
        if (verbose >= 0) ll() << "Joint unknown " << 
          ji << "/" << m->njnt <<
          " type=" << m->jnt_type[ji];
      }
    }
  }

  void setupActuators()
  {
    if (verbose >= 1) ll() << "nu=" << m->nu;
    for (int ai = 0; ai < m->nu; ai++) {
      string name = getname(m->name_actuatoradr[ai], "", ai);

      F lo = -1, hi = +1;
      if (m->actuator_ctrllimited[ai]) {
        lo = m->actuator_ctrlrange[2*ai];
        hi = m->actuator_ctrlrange[2*ai+1];
      }

      if (verbose >= 1) ll() << "Actuator " << ai <<
        " name=" << name << " lo=" << lo << " hi=" << hi;

      auto act = mkDynamic<F, API_ACTUATOR>(name);

      txops.push_back([
        act,
        lo,
        hi,
        ai
      ](mjData *d, CellVals *vals) {
        d->ctrl[ai] = double(clamp(act->memVal(vals), lo, hi));
      });
    }
  }

  void setupRender()
  {
    rxops.push_back([this](mjData *d, CellVals *vals, Trace *tr) {
      if (renderScene.valid()) {
        auto scene = tr->tracelife->mkObject<MjScene>();

        while (1) {
          mjv_makeScene(m.get(), &scene->scn, geomAllocSize);
          mjv_updateScene(m.get(), d, &opt, nullptr, &cam, mjCAT_ALL, &scene->scn);
          if (scene->scn.ngeom < geomAllocSize) {
            geomAllocSize = scene->scn.ngeom + 4;
            break;
          }
          geomAllocSize = max(64, 2 * geomAllocSize);
        }

        renderScene.memVal(vals) = tr->tracelife->mk<DrawOpMujocoScene>(m.get(), &scene->scn);
      }
    });
  }

  MjDataHolder *getState(Trace *tr)
  {
    if (!tr->simState) {
      auto p = make_shared<MjDataHolder>(m.get());
      tr->simState = p;
    }
    return static_cast<MjDataHolder *>(tr->simState.get());
  }

  virtual void simAdvance(CellVals *prev, CellVals *next, Trace *tr, int bi) override
  {
    auto s = getState(tr);
    for (auto &it : txops) {
      it(s->d, prev);
    }
    R goaltime = (tr->vals.size() + 1) * double(tr->dt);
    while (s->d->time < goaltime) {
      auto oldTime = s->d->time;
      mj_step(m.get(), s->d);
      auto newTime = s->d->time;
      if (newTime < oldTime) {
        ll() << "Mujoco time went back: " << oldTime << " -> " << newTime;
        tr->checkFailuresByCell[apiCell] = true;
        tr->simFailure = true;
        break;
      }
    }
    if (verbose >= 3) ll() << "steps to " << s->d->time;
    for (auto &it : rxops) {
      it(s->d, next, tr);
    }
  }

};



static ApiRegister reg("mujoco", [](CompiledSheet &_compiled, ApiSpec const &_spec) {
  return make_shared<MujocoApi>(_compiled, _spec);
});
