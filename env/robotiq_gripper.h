#pragma once
#include "./univarm.h"
#include "./universal_network_utils.h"

struct UniversalRobotiqGripper : enable_shared_from_this<UniversalRobotiqGripper> {

  const size_t UPTO_NL = 1000010;

  string rHost, rPort;
  string label;

  asio::ip::tcp::socket sock{io_context};

  int verbose = 2;
  int iterActive = 0;

  std::basic_string<u_char> rxBuf;
  bool rxAnyFlag = false;

  vector<int> sidList;
  deque<pair<size_t, std::function<void(string const &rx)>>> ackQueue;

  function<void(F obj, F flt, F sta, F pre)> onRxGripStatus;

  enum {
    S_init,
    S_running,
    S_resetting,
    S_eof,
    S_error,
  } state{S_init};

  UniversalRobotiqGripper(string _rHost, string _rPort)
    : rHost(_rHost), rPort(_rPort), label("urgrip(" + rHost + ":" + rPort + ")")
  {
  }
  
  bool error()
  {
    return state == S_error;
  }

  bool eof()
  {
    return state == S_error;
  }


  void start()
  {
    asio::ip::tcp::resolver resolver(io_context);
    boost::system::error_code resolveError;
    // FIXME: should be async
    auto eps = resolver.resolve(asio::ip::tcp::v4(), rHost, rPort, resolveError);
    if (resolveError) {
      L() << label << ": resolve: " << resolveError;
      return;
    }

    if (verbose >= 1) L() << label << ": connecting...";
    asio::async_connect(sock, eps,
      [this, keepalive=shared_from_this()](const boost::system::error_code error, asio::ip::tcp::endpoint const &endpoint) {
        if (error) {
          L() << label << ": " << error.message();
          state = S_error;
          return;
        }
        sock.set_option(asio::ip::tcp::no_delay(true));
        commStart();
        return asyncRx();
      });
  }

  void asyncRx()
  {
    size_t start = rxBuf.size();
    int const maxLen = 4096;
    rxBuf.resize(rxBuf.size() + maxLen);
    sock.async_read_some(asio::buffer(rxBuf.data() + start, maxLen),
      [this, start](boost::system::error_code error, size_t nr) {
        if (error) {
          if (error == asio::error::eof) {
            state = S_eof;
            return;
          }
          L() << label << ": " << error.message();
          state = S_error;
          return;
        }
        rxBuf.resize(start + nr);

        if (!rxAnyFlag) {
          rxAnyFlag = true;
          if (verbose >= 1) L() << label << ": receiving on control socket";
        }

        while (!ackQueue.empty() && !rxBuf.empty()) {
          auto expectAck = ackQueue.front();
          if (verbose >= 5) L() << label << ": grp | expect " << expectAck.first;
          if (expectAck.first == UPTO_NL) {
            auto eolIt = find(rxBuf.begin(), rxBuf.end(), (u_char)10);
            if (verbose >= 5) L() << label << ": grp | grab " << (eolIt - rxBuf.begin()) << " of "s  << rxBuf.size();
            if (eolIt == rxBuf.end()) break;
            string rx = string(reinterpret_cast<char *>(rxBuf.data()), eolIt - rxBuf.begin());
            rxBuf.erase(rxBuf.begin(), eolIt + 1);
            if (verbose >= 4) L() << label << ": grp > raw " << shellEscape(rx);
            expectAck.second(rx);
            ackQueue.pop_front();
          }
          else {
            if (rxBuf.size() < expectAck.first) break;
            string rx = string(reinterpret_cast<char *>(rxBuf.data()), expectAck.first);
            if (verbose >= 4) L() << label << ": grp > raw " << shellEscape(rx);
            rxBuf.erase(rxBuf.begin(), rxBuf.begin() + expectAck.first);
            expectAck.second(rx);
            ackQueue.pop_front();
          }
        }
        asyncRx();
      });
  }

  void close()
  {
    if (sock.is_open()) {
      sock.close();
    }
  }

  void commStart()
  {
    state = S_resetting;
    txCmd("GET VER\n", UPTO_NL, 
      [this, keepalive=shared_from_this()](string const &reply) {
        if (verbose >= 2) L() << label << ": grp < " << shellEscape(reply);
        if (reply == "VER DCU-2.0.0"s) {
          remoteActivate([this, thisp = shared_from_this()](string const &err) {
            if (!err.empty()) {
              if (verbose >= 0) L() << label << ": grp > remoteActivate: " << err;
              state = S_error;
              return;
            }
            state = S_running;
          });
        }
        else {
          if (verbose >= 0) L() << label << ": grp < GET VER: " << shellEscape(reply) << " (WRITEME: retry? accept?)\n";
          state = S_error;
        }
      });
  }

  void txCmd(string const &tx, size_t ackSize, std::function<void(string const &rx)> ackHandler)
  {
    if (ackSize != 0) {
      ackQueue.emplace_back(ackSize, ackHandler);
    }
    if (verbose >= 2) L() << label << ": grp < cmd " << shellEscape(tx);
    assertlog(tx.empty() || tx.back() != '\n', LOGV(shellEscape(tx)));

    // Sync, but this shouldn't normally block
    boost::system::error_code writeError;
    asio::write(sock, asio::const_buffer(tx.data(), tx.size()), writeError);
    if (writeError) {
      L() << label << ": write: " << writeError.message();
    }
  }

  void remoteSet(string const &regName, int v, std::function<void(string const &err)> cb = nullptr)
  {
    txCmd("SET "s + regName + " "s + repr(v) + "\n"s, 3, [this, regName, v, cb](string const &reply) {
      if (reply == "ack") {
        if (verbose >= 3) L() << label << ": grp Set " << regName << "=" << v << ": ack\n";
        if (cb) cb("");
      }
      else {
        if (verbose >= 1) L() << label << ": grp Set " << regName << "="s << v << ": " << shellEscape(reply) << " (WRITEME: retry?)\n";
        if (cb) cb("Expected ack, got " + shellEscape(reply));
      }
    });
  }

  void remoteIsConnected(int sid, std::function<void(string const &, bool)> cb)
  {
    txCmd("SET SID "s + repr(sid) + "\n", 3, [cb](string const &reply) {
      if (reply == "ack") {
        cb("", true);
      }
      else {
        cb("", false);
      }
    });
  }

  void remoteGetSid(std::function<void(string const &err)> cb)
  {
    txCmd("GET SID\n"s, UPTO_NL, [this, cb](string const &reply) {
      if (reply.size() >= 6 && reply.substr(0, 5) == "SID [" && reply.substr(reply.size()-1, 1) == "]") {
        auto sidsStr = reply.substr(5, reply.size() - 6);
        vector<string> sids = splitChar(sidsStr, ',');

        sidList.clear();
        for (auto &it : sids) {
          sidList.push_back(strtol(it.c_str(), nullptr, 10));
        }
        if (verbose >= 1) L() << label << ": sids=" << sidList;
        cb("");
      }
      else {
        cb("Expected SID, got " + shellEscape(reply));
      }
    });
  }

  void remoteReset(std::function<void(string const &err)> cb)
  {
    remoteSet("ACT", 0);
    remoteSet("ATR", 0, cb);
  }

  void remoteStop(std::function<void(string const &err)> cb)
  {
    remoteSet("GTO", 0, cb);
  }

  void remoteAutoReleaseOpen(std::function<void(string const &err)> cb)
  {
    remoteSet("ARD", 0);
    remoteSet("ACT", 1);
    remoteSet("ATR", 0, [this, cb](string const &err) {
      if (!err.empty()) return cb(err);
      // pause?
      remoteSet("ATR", 1, [cb](string const &err) { return cb(err); });
    });
  }

  void remoteActivate(std::function<void(string const &err)> cb)
  {
    remoteGetSid([this, cb](string const &err) {
      if (!err.empty()) return cb(err);
      if (sidList.empty()) return cb("no grippers. sidList=" + repr(sidList));
      remoteSet("ACT", 1, [this, cb](string const &err) {
        if (!err.empty()) return cb(err);
        waitForStatusActivated(cb);
      });
    });
  }

  void waitForStatusActivated(std::function<void(string const &err)> cb)
  {
    txCmd("GET STA\n"s, UPTO_NL, [this, cb](string const &reply) {
      if (reply == "STA ?") {
        return cb("Expected STA 3, got " + shellEscape(reply) + ". Power up the robot from the control panel.");
      }
      else if (reply == "STA 1") {
        return cb("Expected STA 3, got " + shellEscape(reply) + ". Activate the gripper from the control panel.");
      }
      else if (reply == "STA 3") {
        return cb("");
      }
      else {
        if (verbose >= 2) L() << label << " : waiting for STA=3, got " << shellEscape(reply) + "\n";
        waitForStatusActivated(cb);
      }
    });
  }

  void remoteResetAndActivate(std::function<void(string const &err)> cb)
  {
    remoteReset([this, cb](string const &err) {
      (void)this;
      if (!err.empty()) return cb(err);
      remoteActivate([cb](string const &err) {
        if (!err.empty()) return cb(err);
      });
    });
  }

  void remoteSetGrip(int pos, int speed, int force, std::function<void(string const &err)> cb = nullptr)
  {
    if (verbose >= 2) L() << label << ": grp < setGrip: pos=" << pos << " speed=" << speed << " force=" << force;
    txCmd(
        "SET POS "s + repr(pos) + " SPE "s + repr(speed) + " FOR "s + repr(force) + "\n"s,
        3,
        [this, cb](string const &reply) {
          (void)this;
          if (reply == "ack") {
            if (cb) cb(""s);
          }
          else {
            if (verbose >= 1) L() << label << ": grp > SetGrip: " << shellEscape(reply) << " (WRITEME: retry?)\n";
            if (cb) cb("Expected ack, got " + shellEscape(reply));
          }
        });
  }

  void remoteIsObjectDetected(std::function<void(string const &err, int detected)> cb)
  {
    txCmd("GET OBJ\n"s, 1, [cb](string const &reply) {
      if (isdigit(reply[0])) {
        return cb(""s, atoi(reply.c_str()));
      }
      else {
        return cb("Expected digit, got "s + shellEscape(reply), 0);
      }
    });
  }

  void remoteGetGrip(std::function<void(string const &err, int obj, int flt, int sta, int pre)> cb)
  {
    struct Results {
      int obj = 0;
      int flt = 0;
      int sta = 0;
      int pre = 0;
      int todo = 0;
      std::function<void(string const &err, int obj, int flt, int sta, int pre)> cb = nullptr;
      string err;
      shared_ptr<UniversalRobotiqGripper> thisp;

      void checkdone()
      {
        todo--;
        if (todo == 0) {
          if (thisp->verbose >= 1) L() << thisp->label << ": grp > obj=" << obj << " flt=" << flt << " sta=" << sta << " pre=" << pre;
          cb(err, obj, flt, sta, pre);
        }
      };
    };
    auto r = make_shared<Results>();
    r->cb = cb;
    r->todo = 4;
    r->thisp = shared_from_this();

    txCmd("GET OBJ\n"s, UPTO_NL, [this, r](string const &reply) {
      (void)this;
      if (reply.substr(0, 4) == "OBJ ") {
        r->obj = atoi(reply.substr(4).c_str());
      }
      else {
        r->err = "Expected OBJ <d>, got \""s + reply + "\""s;
      }
      r->checkdone();
    });

    txCmd("GET FLT\n"s, UPTO_NL, [this, r](string const &reply) {
      (void)this;
      if (reply.substr(0, 4) == "FLT ") {
        r->flt = atoi(reply.substr(4).c_str());
      }
      else {
        r->err = "Expected FLT <d>, got \""s + reply + "\""s;
      }
      r->checkdone();
    });

    txCmd("GET STA\n"s, UPTO_NL, [this, r](string const &reply) {
      (void)this;
      if (reply.substr(0, 4) == "STA ") {
        r->sta = atoi(reply.substr(4).c_str());
      }
      else {
        r->err = "Expected STA <d>, got \""s + reply + "\""s;
      }
      r->checkdone();
    });

    txCmd("GET PRE\n"s, UPTO_NL, [r](string const &reply) {
      if (reply.substr(0, 4) == "PRE ") {
        r->pre = atoi(reply.substr(4).c_str());
      }
      else {
        r->err = "Expected PRE <d>, got \""s + reply + "\""s;
      }
      r->checkdone();
    });
  }

  void remoteIter(F pos, F vel, F force)
  {
    iterActive++;

    remoteSetGrip(
        min(255, max(0, int(pos * 255.0f))),
        min(255, max(0, int(vel * 255.0f))),
        min(255, max(0, int(force * 255.0f))),
        [this, thisp = shared_from_this()](string const &err) {
          if (!err.empty()) {
            if (verbose >= 0) L() << label << ": grp > error: " << shellEscape(err) << " (WRITEME: retry?)\n";
            state = S_error;
          }
          remoteGetGrip([this, thisp = shared_from_this()](string const &err, int obj, int flt, int sta, int pre) {
            if (!err.empty()) {
              if (verbose >= 0) L() << label << ": grp > error: " << shellEscape(err) << " (WRITEME: retry?)\n";
              state = S_error;
            }
            if (onRxGripStatus) {
              onRxGripStatus(F(obj), F(flt), F(sta), F(pre)/255.0f);
            }
            iterActive--;
          });
        });
  }

};
