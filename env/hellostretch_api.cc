#include "../src/api.h"
#include "../src/emit.h"

struct HelloStretchApi : SheetApi {
  
  ApiRef<F, API_ACTUATOR> out_lwheel_vel {*this, "out.lwheel.vel"};
  ApiRef<F, API_ACTUATOR> out_rwheel_vel {*this, "out.rwheel.vel"};

  ApiRef<F, API_SENSOR> in_lwheel_pos {*this, "in.lwheel.pos"};
  ApiRef<F, API_SENSOR> in_rwheel_pos {*this, "in.rwheel.pos"};

  HelloStretchApi(CompiledSheet &_compiled, ApiSpec const &_spec)
    : SheetApi(_compiled, _spec, "HelloStretch.1")
  {
    compiled.hasLiveApi = true;
  }
  
  virtual void txActuators(CellVals *vals, LiveRobotState &lrt) override;
  virtual void rxSensors(LiveRobotState &lrt) override;

};

#ifdef notyet
static ApiRegister reg("hellostretch", [](CompiledSheet &compiled, string apiName, U32 apiId, string_view apiYaml) {
  return make_shared<HelloStretchApi>(compiled, apiName, apiId, apiYaml);
});
#endif

void HelloStretchApi::txActuators(CellVals *vals, LiveRobotState &lrt)
{
  // WRITEME
}

void HelloStretchApi::rxSensors(LiveRobotState &lrt)
{
  // WRITEME
}
