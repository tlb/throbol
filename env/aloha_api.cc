#include "../src/api.h"
#include "../src/emit.h"
#include "../remote/cereal_box.h"
#include "../src/live_robot_ticker.h"
#include "./dynamixel.h"

template<typename Joint>
struct Vx300s {
  SheetApi &owner;
  string name;

  Joint waist {owner, name + ".waist"};
  Joint shoulder {owner, name + ".shoulder"};
  Joint elbow {owner, name + ".elbow"};
  Joint forearm_roll {owner, name + ".forearm_roll"};
  Joint wrist_angle {owner, name + ".wrist_angle"};
  Joint wrist_rotate {owner, name + ".wrist_rotate"};
  Joint gripper {owner, name + ".gripper"};

  Vx300s(SheetApi &_owner, string const &_name)
    :owner(_owner),
     name(_name)
  {
  }

};


struct AlohaApi : DynamixelApi {

  Vx300s<DynamixelJoint> master_left {*this, "master_left"};
  Vx300s<DynamixelJoint> master_right {*this, "master_right"};
  Vx300s<DynamixelJointActive> puppet_left {*this, "puppet_left"};
  Vx300s<DynamixelJointActive> puppet_right {*this, "puppet_right"};

  DynamixelBus master_left_bus {"master_left"};
  DynamixelBus puppet_left_bus {"puppet_left"};
  DynamixelBus master_right_bus {"master_right"};
  DynamixelBus puppet_right_bus {"puppet_right"};

  AlohaApi(CompiledSheet &_compiled, ApiSpec const &_spec)
    : DynamixelApi(_compiled, _spec, "aloha.1")
  {
#if !defined(__EMSCRIPTEN__)
    setActiveBus(master_right_bus, "/dev/ttyDXL_master_right");
    setActiveBus(puppet_right_bus, "/dev/ttyDXL_puppet_right");
    setActiveBus(master_left_bus, "/dev/ttyDXL_master_left");
    setActiveBus(puppet_left_bus, "/dev/ttyDXL_puppet_left");

    setServoMaster(master_right_bus, 1, &master_right.waist);
    setServoPuppet(puppet_right_bus, 1, &puppet_right.waist);
    setServoMaster(master_right_bus, 3, &master_right.shoulder);
    setServoPuppet(puppet_right_bus, 2, &puppet_right.shoulder);
    if (1) addServoReversedShadow(puppet_right_bus, 3, 2);
    setServoMaster(master_right_bus, 5, &master_right.elbow);
    setServoPuppet(puppet_right_bus, 4, &puppet_right.elbow);
    if (1) addServoReversedShadow(puppet_right_bus, 5, 4);
    setServoMaster(master_right_bus, 6, &master_right.forearm_roll);
    setServoPuppet(puppet_right_bus, 6, &puppet_right.forearm_roll);
    setServoMaster(master_right_bus, 7, &master_right.wrist_angle);
    setServoPuppet(puppet_right_bus, 7, &puppet_right.wrist_angle);
    setServoMaster(master_right_bus, 8, &master_right.wrist_rotate);
    setServoPuppet(puppet_right_bus, 8, &puppet_right.wrist_rotate);

    // Gripper
    setServoMaster(master_right_bus, 9, &master_right.gripper);
    addServoConfig1(puppet_right_bus, 9, ADDR_TORQUE_ENABLE, 0);
    addServoConfig1(puppet_right_bus, 9, ADDR_OPERATING_MODE, 5); // current-based position
    addServoConfig2(puppet_right_bus, 9, ADDR_CURRENT_LIMIT, 300);
    setServoPuppet(puppet_right_bus, 9, &puppet_right.gripper);


    setServoMaster(master_left_bus, 1, &master_left.waist);
    setServoPuppet(puppet_left_bus, 1, &puppet_left.waist);
    setServoMaster(master_left_bus, 3, &master_left.shoulder);
    setServoPuppet(puppet_left_bus, 2, &puppet_left.shoulder);
    if (1) addServoReversedShadow(puppet_left_bus, 3, 2);
    setServoMaster(master_left_bus, 5, &master_left.elbow);
    setServoPuppet(puppet_left_bus, 4, &puppet_left.elbow);
    if (1) addServoReversedShadow(puppet_left_bus, 5, 4);
    setServoMaster(master_left_bus, 6, &master_left.forearm_roll);
    setServoPuppet(puppet_left_bus, 6, &puppet_left.forearm_roll);
    setServoMaster(master_left_bus, 7, &master_left.wrist_angle);
    setServoPuppet(puppet_left_bus, 7, &puppet_left.wrist_angle);
    setServoMaster(master_left_bus, 8, &master_left.wrist_rotate);
    setServoPuppet(puppet_left_bus, 8, &puppet_left.wrist_rotate);

    setServoMaster(master_left_bus, 9, &master_left.gripper);
    addServoConfig1(puppet_left_bus, 9, ADDR_TORQUE_ENABLE, 0);
    addServoConfig1(puppet_left_bus, 9, ADDR_OPERATING_MODE, 5); // current-based position
    addServoConfig2(puppet_left_bus, 9, ADDR_CURRENT_LIMIT, 300);
    setServoPuppet(puppet_left_bus, 9, &puppet_left.gripper);
    addServoTorqueOffAtCooldown(master_left_bus, 9);
    addServoTorqueOffAtCooldown(master_right_bus, 9);
#endif
  }

#if !defined(__EMSCRIPTEN__)
  void setServoMaster(DynamixelBus &bus, int dxlid, DynamixelJoint *joint)
  {
    if (!bus.active) return;
    addServoSetup(bus, dxlid, false);
    addServoRxPresents(bus, dxlid, nullptr, nullptr, &joint->vel, &joint->pos);
  }

  void setServoPuppet(DynamixelBus &bus, int dxlid, DynamixelJointActive *joint, int min_position_limit = 0, int max_position_limit = 4095)
  {
    if (!bus.active) return;
    addServoSetup(bus, dxlid, true);
    addServoRxPresents(bus, dxlid, &joint->pwm, &joint->current, &joint->vel, &joint->pos);
    addServoTxGoalPosition(bus, dxlid, &joint->goal_pos, min_position_limit, max_position_limit);
  }
#endif

};


static ApiRegister regAloha("aloha", [](CompiledSheet &_compiled, ApiSpec const &_spec) {
  return make_shared<AlohaApi>(_compiled, _spec);
});

