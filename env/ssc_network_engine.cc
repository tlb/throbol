#include "./ssc_network_engine.h"
#include "common/packetbuf_types.h"
#include "../remote/cereal_box.h"

static int sscLocalPortCounter = 10001;

SscNetworkEngine::SscNetworkEngine(string const &_label, string const &_remoteHostname, string const &_remotePort)
 :label(_label),
  remoteHostname(_remoteHostname),
  remotePort(_remotePort)
{
  localPort = sscLocalPortCounter++;
}

SscNetworkEngine::~SscNetworkEngine()
{
  closeSockets();
}

void SscNetworkEngine::closeSockets()
{
  if (sock.is_open()) {
    sock.close();
  }
}

void SscNetworkEngine::start()
{
  setupHandlers();
  setupSockets();
}

void SscNetworkEngine::setupSockets()
{
  static asio::ip::udp::resolver resolver(io_context);

  sock.open(asio::ip::udp::v4());
  sock.bind(asio::ip::udp::endpoint(asio::ip::udp::v4(), localPort));

  resolver.async_resolve(remoteHostname, remotePort,
    [this, keepalive=shared_from_this()](const boost::system::error_code error, auto results) {
      if (error) {
        L() << label << ": failed to resolve " << remoteHostname << ": " << error.message();
        return;
      }

      async_connect(sock, results,
        [this, keepalive](boost::system::error_code error, auto where) {
          if (error) {
            L() << label << ": failed to connect to " << remoteHostname << ": " << error.message();
            return;
          }
          L() << label << ": connected to " << where;
          ready = true;
          asyncRx();
        });
    });
}

void SscNetworkEngine::asyncRx()
{
  rxBuf.resize(2048);
  sock.async_receive_from(asio::buffer(rxBuf.data(), rxBuf.size()),
    rxSrc,
    [this](const boost::system::error_code error, size_t nr) {
      if (error) {
        L() << label << ": " << error.message();
        sock.close();
        return;
      }
      if (nr == 0) {
        return asyncRx();
      }

      packet rx(reinterpret_cast<U8 const *>(rxBuf.data()), nr);
      rxPkt(rx);

      return asyncRx();
    });
}

void SscNetworkEngine::setupHandlers()
{
  rxHandlers[(U8)'!'] = [this](double rxTime, packet &rx, bool &error) {
    string msg(reinterpret_cast<char *>(rx.rd_ptr()), rx.remaining());
    L() << label << ": msg " << msg;
    rx.get_skip(rx.remaining());
  };

  rxHandlers[(U8)'m'] = [this](double rxTime, packet &rx, bool &error) {
    string msg(reinterpret_cast<char *>(rx.rd_ptr()), rx.remaining());
    L() << label << ": msg " << msg;
    rx.get_skip(rx.remaining());
  };

  rxHandlers[(U8)'u'] = [this](double rxTime, packet &rx, bool &error) {
    U64 pktTicks;
    rx.get(pktTicks);
    while (!error && rx.remaining() > 0) {
      U8 subtype;
      rx.get(subtype);
      if (subtype == 0) {
        if (rx.remaining() > 0) {
          L() << label << ": packet not at end. Remaining=" << rx.remaining();
        }
        return;
      }

      auto &uh = updateHandlers[subtype];
      if (uh) {
        uh(rxTime, rx, pktTicks, error);
      }
      else {
        L() << label <<
          ": bad update subtype=" << repr_02x(subtype) <<
          " from " << rxSrc;
        rx.dump();
        error = true;
      }
    }
  };
}


void SscNetworkEngine::rxPkt(packet &rx)
{
  double rxTime = realtime();
  bool error = false;
  while (!error && rx.remaining() > 0) {
    U8 type;
    rx.get(type);
    auto &ph = rxHandlers[type];
    if (!ph) {
      L() << label << ": bad pkt type=" << repr_02x(type);
      rx.dump();
      error = true;
    }
    else {
      ph(rxTime, rx, error);
    }
  }
  //if (rti) rti->requestUpdate();
}

void SscNetworkEngine::txPkt(packet &tx)
{
  if (!ready) return; // still waiting for DNS resolution?
  if (tx.size() == 0) return;
  if (verbose >= 2) L() << label << ": tx " << tx.size();

  // FIXME: make sync

  unique_ptr<char []> d(new char[tx.size()]);
  memcpy(d.get(), tx.begin(), tx.size());

  sock.async_send(asio::buffer(d.get(), tx.size()),
    [this, d=std::move(d)](const boost::system::error_code &error, size_t nw) {
      if (error) {
        L() << label << ": txPkt: " << error.message();
        sock.close();
        ready = false;
        return;
      }
    });
}

void SscNetworkEngine::sendAdminCmd(string const &s)
{
  packet tx;
  tx.add_remainder_string(s);
  txPkt(tx);
}

bool SscNetworkEngine::isReady()
{
  return ready;
}
