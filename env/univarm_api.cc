#include "./univarm.h"
#include "../src/api.h"
#include "../src/trace.h"
#include "../src/emit.h"
#include "./universal_network_monitor.h"
#include "./universal_network_control.h"
#include "./universal_network_dashboard.h"
#include "./robotiq_gripper.h"
#include "../remote/sim_camera_client.h"
#include "../src/live_robot_ticker.h"


struct UnivarmApi : SheetApi {

  string robot_host;
  shared_ptr<UniversalRobotMonitor> mon;
  shared_ptr<UniversalRobotControl> ctl;
  shared_ptr<UniversalRobotiqGripper> gripper;
  shared_ptr<UniversalRobotDashboard> dash;

  ApiRef<F, API_ACTUATOR> actTargetPos[6] {
    {*this, "targetPos.base"},
    {*this, "targetPos.shoulder"},
    {*this, "targetPos.elbow"},
    {*this, "targetPos.wrist1"},
    {*this, "targetPos.wrist2"},
    {*this, "targetPos.wrist3"}
  };

  ApiRef<F, API_ACTUATOR> actTargetVel[6] {
    {*this, "targetVel.base"},
    {*this, "targetVel.shoulder"},
    {*this, "targetVel.elbow"},
    {*this, "targetVel.wrist1"},
    {*this, "targetVel.wrist2"},
    {*this, "targetVel.wrist3"}
  };

  ApiRef<F, API_ACTUATOR> actServoGain {*this, "servoGain"};
  ApiRef<F, API_ACTUATOR> actLookahead {*this, "lookahead"};

  ApiRef<F, API_ACTUATOR> actGripPos { *this, "grip.pos" };
  ApiRef<F, API_ACTUATOR> actGripVel { *this, "grip.vel" };
  ApiRef<F, API_ACTUATOR> actGripForce { *this, "grip.force" };


  ApiRef<F, API_SENSOR> senseTargetPos[6] {
    {*this, "targetPos.base"},
    {*this, "targetPos.shoulder"},
    {*this, "targetPos.elbow"},
    {*this, "targetPos.wrist1"},
    {*this, "targetPos.wrist2"},
    {*this, "targetPos.wrist3"}
  };

  ApiRef<F, API_SENSOR> senseTargetVel[6] {
    {*this, "targetVel.base"},
    {*this, "targetVel.shoulder"},
    {*this, "targetVel.elbow"},
    {*this, "targetVel.wrist1"},
    {*this, "targetVel.wrist2"},
    {*this, "targetVel.wrist3"}
  };

  ApiRef<F, API_SENSOR> senseTargetCurrent[6] {
    {*this, "targetCurrent.base"},
    {*this, "targetCurrent.shoulder"},
    {*this, "targetCurrent.elbow"},
    {*this, "targetCurrent.wrist1"},
    {*this, "targetCurrent.wrist2"},
    {*this, "targetCurrent.wrist3"}
  };

  ApiRef<F, API_SENSOR> senseTargetMoment[6] {
    {*this, "targetMoment.base"},
    {*this, "targetMoment.shoulder"},
    {*this, "targetMoment.elbow"},
    {*this, "targetMoment.wrist1"},
    {*this, "targetMoment.wrist2"},
    {*this, "targetMoment.wrist3"}
  };


  ApiRef<F, API_SENSOR> senseActualPos[6] {
    {*this, "actualPos.base"},
    {*this, "actualPos.shoulder"},
    {*this, "actualPos.elbow"},
    {*this, "actualPos.wrist1"},
    {*this, "actualPos.wrist2"},
    {*this, "actualPos.wrist3"}
  };

  ApiRef<F, API_SENSOR> senseActualVel[6] {
    {*this, "actualVel.base"},
    {*this, "actualVel.shoulder"},
    {*this, "actualVel.elbow"},
    {*this, "actualVel.wrist1"},
    {*this, "actualVel.wrist2"},
    {*this, "actualVel.wrist3"}
  };

  ApiRef<F, API_SENSOR> senseActualVoltage[6] {
    {*this, "actualVoltage.base"},
    {*this, "actualVoltage.shoulder"},
    {*this, "actualVoltage.elbow"},
    {*this, "actualVoltage.wrist1"},
    {*this, "actualVoltage.wrist2"},
    {*this, "actualVoltage.wrist3"}
  };

  ApiRef<F, API_SENSOR> senseActualCurrent[6] {
    {*this, "actualCurrent.base"},
    {*this, "actualCurrent.shoulder"},
    {*this, "actualCurrent.elbow"},
    {*this, "actualCurrent.wrist1"},
    {*this, "actualCurrent.wrist2"},
    {*this, "actualCurrent.wrist3"}
  };

  ApiRef<F, API_SENSOR> senseActualTemperature[6] {
    {*this, "actualTemperature.base"},
    {*this, "actualTemperature.shoulder"},
    {*this, "actualTemperature.elbow"},
    {*this, "actualTemperature.wrist1"},
    {*this, "actualTemperature.wrist2"},
    {*this, "actualTemperature.wrist3"}
  };

  ApiRef<F, API_SENSOR> senseActualTcpPos[6] {
    {*this, "actualTcpPos.x"},
    {*this, "actualTcpPos.y"},
    {*this, "actualTcpPos.z"},
    {*this, "actualTcpPos.rx"},
    {*this, "actualTcpPos.ry"},
    {*this, "actualTcpPos.rz"}
  };

  ApiRef<F, API_SENSOR> senseActualTcpVel[6] {
    {*this, "actualTcpVel.x"},
    {*this, "actualTcpVel.y"},
    {*this, "actualTcpVel.z"},
    {*this, "actualTcpVel.rx"},
    {*this, "actualTcpVel.ry"},
    {*this, "actualTcpVel.rz"}
  };

  ApiRef<F, API_SENSOR> senseActualTcpForce[6] {
    {*this, "actualTcpForce.x"},
    {*this, "actualTcpForce.y"},
    {*this, "actualTcpForce.z"},
    {*this, "actualTcpForce.rx"},
    {*this, "actualTcpForce.ry"},
    {*this, "actualTcpForce.rz"}
  };

  ApiRef<F, API_SENSOR> senseJointMode[6] {
    {*this, "jointMode.base"},
    {*this, "jointMode.shoulder"},
    {*this, "jointMode.elbow"},
    {*this, "jointMode.wrist1"},
    {*this, "jointMode.wrist2"},
    {*this, "jointMode.wrist3"}
  };

  ApiRef<F, API_SENSOR> senseGripObj { *this, "grip.obj" };
  ApiRef<F, API_SENSOR> senseGripFlt { *this, "grip.flt" };
  ApiRef<F, API_SENSOR> senseGripSta { *this, "grip.sta" };
  ApiRef<F, API_SENSOR> senseGripPre { *this, "grip.pre" };

  ApiRef<F, API_SENSOR> senseSafetyMode { *this, "safetyMode" };

  ApiRef<F, API_SENSOR> sensePhysicalRobotConnected { *this, "physicalRobotConnected" };
  ApiRef<F, API_SENSOR> senseRealRobotEnabled { *this, "realRobotEnabled" };
  ApiRef<F, API_SENSOR> senseRobotPowerOn { *this, "robotPowerOn" };
  ApiRef<F, API_SENSOR> senseEmergencyStopped { *this, "emergencyStopped" };
  ApiRef<F, API_SENSOR> senseProtectiveStopped { *this, "protectiveStopped" };
  ApiRef<F, API_SENSOR> senseProgramRunning { *this, "programRunning" };
  ApiRef<F, API_SENSOR> senseProgramPaused { *this, "programPaused" };
  ApiRef<F, API_SENSOR> senseRobotMode { *this, "robotMode" };
  ApiRef<F, API_SENSOR> senseControlMode { *this, "controlMode" };
  ApiRef<F, API_SENSOR> senseTargetSpeedFraction { *this, "targetSpeedFraction" };
  ApiRef<F, API_SENSOR> senseSpeedScaling { *this, "speedScaling" };
  ApiRef<F, API_SENSOR> senseTargetSpeedFractionLimit { *this, "targetSpeedFractionLimit" };

  UnivarmApi(CompiledSheet &_compiled, ApiSpec const &_spec)
    : SheetApi(_compiled, _spec, "univarm.2")
  {
    compiled.hasLiveApi = true;
    verbose = 1;
  }

  virtual bool startLive() override
  {
    if (!SheetApi::startLive()) return false;

    L() << label << ": startLive";

    robot_host = assoc_string_view(apiProps, "robot_host", "");
    if (verbose >= 1) L() << label << ": replayBufferSize=" << replayBufferSize;

    dash = make_shared<UniversalRobotDashboard>(robot_host, "29999");
    dash->makeRobotNormal(
      [this]() {
        startSession();
      },
      [this]() {
        stopLive();
      });
    dash->start();
    return true;
  }
  
  void startSession()
  {
    setupMon();
    setupCtl();
    if (!assoc_bool(apiProps, "no_gripper", false)) setupGripper();
  }
  
  void txWarmup(LiveRobotState &lrt) override
  {
    if (dash && dash->state != UniversalRobotDashboard::S_robotNormal) {
      lrt.needMoreWarmup.push_back(label + " dashboard");
    }
    else {
      if (mon && (mon->state != mon->S_running)) lrt.needMoreWarmup.push_back(label + " mon");
      if (ctl && (ctl->state != ctl->S_running)) lrt.needMoreWarmup.push_back(label + " ctl");
      if (gripper && (gripper->state != gripper->S_running)) lrt.needMoreWarmup.push_back(label + " gripper");
    }
  }

  void rxSensors(LiveRobotState &lrt) override
  {
    if ((mon && (mon->state == mon->S_error || mon->state == mon->S_eof)) ||
      (ctl && (ctl->state == ctl->S_error || ctl->state == ctl->S_eof)) ||
      (gripper && (gripper->error() || gripper->eof()))) {
        lrt.pleaseStopHard();
    }
  }

  virtual void stopLive() override
  {
    SheetApi::stopLive();
    if (mon) {
      mon->close();
    }
    if (ctl) {
      ctl->close();
    }
    if (gripper) {
      gripper->close();
    }
    if (dash) {
      dash->close();
    }
  }

  void setupMon()
  {
    mon = make_shared<UniversalRobotMonitor>(robot_host, "30004");
    mon->onRxDataPackage = [this](U8 recipeId, packet &rx, R rxTime) {
      rxArmObs(rx, lastData);
      rx.check_at_end();
    };

    // Must match rxArmObs
    mon->addOutput("VECTOR6D", "target_q");
    mon->addOutput("VECTOR6D", "target_qd");
    mon->addOutput("VECTOR6D", "target_current");
    mon->addOutput("VECTOR6D", "target_moment");
    mon->addOutput("VECTOR6D", "actual_q");
    mon->addOutput("VECTOR6D", "actual_qd");
    mon->addOutput("VECTOR6D", "actual_current");

    mon->addOutput("VECTOR6D", "actual_TCP_pose");
    mon->addOutput("VECTOR6D", "actual_TCP_speed");
    mon->addOutput("VECTOR6D", "actual_TCP_force");

    mon->addOutput("VECTOR6INT32", "joint_mode");
    mon->addOutput("INT32", "safety_mode");

    for (int i = 0; i < 14; i++) {
      mon->addInput("DOUBLE", "input_double_register_"s + to_string(i));
    }
    mon->addInput("INT32", "input_int_register_0");
    mon->start();
  }

  void setupCtl()
  {
    auto script = R"(def servoloop2():
  set_gravity([0.0, 0.0, 9.82])
  set_tcp(p[0.0,0.0,0.0,0.0,0.0,0.0])
  set_payload(0.0)
  set_tool_communication(False, 115200, 0, 1, 1.5, 3.5)
  set_tool_output_mode(0)
  set_tool_digital_output_mode(0, 1)
  set_tool_digital_output_mode(1, 1)
  set_safety_mode_transition_hardness(1)
  set_standard_analog_input_domain(0, 1)
  set_standard_analog_input_domain(1, 1)
  set_tool_analog_input_domain(0, 1)
  set_tool_analog_input_domain(1, 1)
  set_analog_outputdomain(0, 0)
  set_analog_outputdomain(1, 0)
  set_tool_voltage(0)
  set_input_actions_to_default()
  global q_target = get_actual_joint_positions()
  global qd_target = [0,0,0,0,0,0]
  global orig_q_target = get_actual_joint_positions()
  rtde_set_watchdog("input_int_register_0", 2.0, "stop")
  thread Thread_1():
    while (True):
      local q_target_tmp = [0,0,0,0,0,0]
      local qd_target_tmp = [0,0,0,0,0,0]
      q_target_tmp[0] = read_input_float_register(0)
      q_target_tmp[1] = read_input_float_register(1)
      q_target_tmp[2] = read_input_float_register(2)
      q_target_tmp[3] = read_input_float_register(3)
      q_target_tmp[4] = read_input_float_register(4)
      q_target_tmp[5] = read_input_float_register(5)
      qd_target_tmp[0] = read_input_float_register(6)
      qd_target_tmp[1] = read_input_float_register(7)
      qd_target_tmp[2] = read_input_float_register(8)
      qd_target_tmp[3] = read_input_float_register(9)
      qd_target_tmp[4] = read_input_float_register(10)
      qd_target_tmp[5] = read_input_float_register(11)
      servogain = read_input_float_register(12)
      lookahead = read_input_float_register(13)
      if (q_target_tmp != [0,0,0,0,0,0]):
        global q_target = q_target_tmp
      end
      global qd_target = qd_target_tmp
      servoj(q_target, 0, 0, 0.002, lookahead, servogain)
      #speedj(qd_target, 0.5, 0.002)
    end
  end
  threadId_Thread_1 = run Thread_1()
  while (True):
    sleep(1.0)
  end
end
)";

    ctl = make_shared<UniversalRobotControl>(robot_host, "30001");
    ctl->mainScript = script;
    ctl->onRobotStateModeData = [this](packet &rx, R rxTime) {
      rxModeData(rx, lastData);
    };
    ctl->start();
  }

  void setupGripper()
  {
    gripper = make_shared<UniversalRobotiqGripper>(robot_host, "63352");
    gripper->onRxGripStatus = [this](F obj, F flt, F sta, F pre) {
      senseGripObj.replayVal(lastData) = obj;
      senseGripFlt.replayVal(lastData) = flt;
      senseGripSta.replayVal(lastData) = sta;
      senseGripPre.replayVal(lastData) = pre;
    };
    gripper->start();
  }


  virtual void txActuators(CellVals *vals, LiveRobotState &lrt) override
  {
    mon->txDataPackage([this, vals](packet &tx) {
      txArmAct(tx, vals);
    });
  }

  void txArmAct(packet &tx, CellVals *vals)
  {
    for (int i = 0; i < 6; i++) tx.add_be_double(double(actTargetPos[i].memVal(vals)));
    for (int i = 0; i < 6; i++) tx.add_be_double(double(actTargetVel[i].memVal(vals)));
    tx.add_be_double(double(actServoGain.memVal(vals)));
    tx.add_be_double(double(actLookahead.memVal(vals)));
    tx.add_be_uint32(1); // ?
    if (0) L() << label << ": txArmAct" <<
      " actTargetPos=[" << actTargetPos[0].memVal(vals) << ", " << actTargetPos[1].memVal(vals) << ", " << actTargetPos[2].memVal(vals) << ", " <<
      actTargetPos[3].memVal(vals) << ", " << actTargetPos[4].memVal(vals) << ", " << actTargetPos[5].memVal(vals) << "]\n";
  }

  void rxArmObs(packet &rx, Blob &vals)
  {
    for (int i = 0; i < 6; i++) senseTargetPos[i].replayVal(vals) = rx.get_be_double();
    for (int i = 0; i < 6; i++) senseTargetVel[i].replayVal(vals) = rx.get_be_double();
    for (int i = 0; i < 6; i++) senseTargetCurrent[i].replayVal(vals) = rx.get_be_double();
    for (int i = 0; i < 6; i++) senseTargetMoment[i].replayVal(vals) = rx.get_be_double();
    for (int i = 0; i < 6; i++) senseActualPos[i].replayVal(vals) = rx.get_be_double();
    for (int i = 0; i < 6; i++) senseActualVel[i].replayVal(vals) = rx.get_be_double();
    for (int i = 0; i < 6; i++) senseActualCurrent[i].replayVal(vals) = rx.get_be_double();
    for (int i = 0; i < 6; i++) senseActualTcpPos[i].replayVal(vals) = rx.get_be_double();
    for (int i = 0; i < 6; i++) senseActualTcpVel[i].replayVal(vals) = rx.get_be_double();
    for (int i = 0; i < 6; i++) senseActualTcpForce[i].replayVal(vals) = rx.get_be_double();
    for (int i = 0; i < 6; i++) senseJointMode[i].replayVal(vals) = rx.get_be_uint32();
    senseSafetyMode.replayVal(vals) = rx.get_be_uint32();
    if (0) L() << label << ": rxArmObs" <<
      " senseTargetPos=[" <<
        senseTargetPos[0].replayVal(vals) << ", " << senseTargetPos[1].replayVal(vals) << ", " << senseTargetPos[2].replayVal(vals) << ", " <<
        senseTargetPos[3].replayVal(vals) << ", " << senseTargetPos[4].replayVal(vals) << ", " << senseTargetPos[5].replayVal(vals) << "]" <<
      " senseActualPos=[" << 
        senseActualPos[0].replayVal(vals) << ", " << senseActualPos[1].replayVal(vals) << ", " << senseActualPos[2].replayVal(vals) << ", " <<
        senseActualPos[3].replayVal(vals) << ", " << senseActualPos[4].replayVal(vals) << ", " << senseActualPos[5].replayVal(vals) << "]\n";
  }

#if 0
  void rxJointData(packet &rx, Blob &vals)
  {
    for (int i = 0; i < 6; i++) {
      senseActualPos[i].replayVal(vals) = rx.get_be_double();
      senseTargetPos[i].replayVal(vals) = rx.get_be_double();
      senseActualVel[i].replayVal(vals) = rx.get_be_double();
      senseActualCurrent[i].replayVal(vals) = rx.get_be_float();
      senseActualVoltage[i].replayVal(vals) = rx.get_be_float();
      senseActualTemperature[i].replayVal(vals) = rx.get_be_float();
      rx.get_skip(4); // T_micro claimed to be double but seems to be 4 bytes
      senseJointMode[i].replayVal(vals) = rx.get_be_uint8();
    }
  }
#endif
#if 0
  void rxKinematics(packet &rx, Blob &vals, ){
    for (int i = 0; i < 6; i++) senseChecksums[i].replayVal(vals) = rx.get_be_uint32();
    for (int i = 0; i < 6; i++) senseDhTheta[i].replayVal(vals) = rx.get_be_double();
    for (int i = 0; i < 6; i++) senseDha[i].replayVal(vals) = rx.get_be_double();
    for (int i = 0; i < 6; i++) senseDhd[i].replayVal(vals) = rx.get_be_double();
    for (int i = 0; i < 6; i++) senseDhAlpha[i].replayVal(vals) = rx.get_be_double();

    senseCalibrationStatus.replayVal(vals) = rx.get_be_uint8();
  }
#endif

  void rxModeData(packet &rx, Blob &vals)
  {
    sensePhysicalRobotConnected.replayVal(vals) = rx.get_be_uint8() ? 1.0f : 0.0f;
    senseRealRobotEnabled.replayVal(vals) = rx.get_be_uint8() ? 1.0f : 0.0f;
    senseRobotPowerOn.replayVal(vals) = rx.get_be_uint8() ? 1.0f : 0.0f;
    senseEmergencyStopped.replayVal(vals) = rx.get_be_uint8() ? 1.0f : 0.0f;
    senseProtectiveStopped.replayVal(vals) = rx.get_be_uint8() ? 1.0f : 0.0f;
    senseProgramRunning.replayVal(vals) = rx.get_be_uint8() ? 1.0f : 0.0f;
    senseProgramPaused.replayVal(vals) = rx.get_be_uint8() ? 1.0f : 0.0f;
    senseRobotMode.replayVal(vals) = (F)(int)rx.get_be_uint8();
    senseControlMode.replayVal(vals) = (F)(int)rx.get_be_uint8();
    senseTargetSpeedFraction.replayVal(vals) = rx.get_be_double();
    senseSpeedScaling.replayVal(vals) = rx.get_be_double();
    senseTargetSpeedFractionLimit.replayVal(vals) = rx.get_be_double();
    rx.get_be_uint8(); // dummy
  }

};

static ApiRegister regUnivarm("univarm", [](CompiledSheet &_compiled, ApiSpec const &_spec) {
  return make_shared<UnivarmApi>(_compiled, _spec);
});
