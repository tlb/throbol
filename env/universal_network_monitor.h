#pragma once
#include "./univarm.h"

#include "./universal_network_utils.h"

/*
  RTDE spec:
  https://www.universal-robots.com/how-tos-and-faqs/how-to/ur-how-tos/real-time-data-exchange-rtde-guide-22229/

  UR Script spec:
  https://www.universal-robots.com/how-tos-and-faqs/how-to/ur-how-tos/ethernet-socket-communication-via-urscript-15678/
*/

enum {
  RTDE_REQUEST_PROTOCOL_VERSION = 86,      // ascii V
  RTDE_GET_URCONTROL_VERSION = 118,        // ascii v
  RTDE_TEXT_MESSAGE = 77,                  // ascii M
  RTDE_DATA_PACKAGE = 85,                  // ascii U
  RTDE_CONTROL_PACKAGE_SETUP_OUTPUTS = 79, // ascii O
  RTDE_CONTROL_PACKAGE_SETUP_INPUTS = 73,  // ascii I
  RTDE_CONTROL_PACKAGE_START = 83,         // ascii S
  RTDE_CONTROL_PACKAGE_PAUSE = 80,         // ascii P

  RTDE_PROTOCOL_VERSION = 2
};

struct UniversalRobotMonitor : enable_shared_from_this<UniversalRobotMonitor> {

  string rHost, rPort;
  asio::ip::tcp::socket sock{io_context};
  string label;

  std::function<void(U8 recipeId, packet &rx, double rxTime)> onRxDataPackage;

  enum {
    S_init,
    S_protocolVersionWait,
    S_urcontrolVersionWait,
    S_setupInputsWait,
    S_setupOutputsWait,
    S_controlPackageStartWait,
    S_running,
    S_eof,
    S_error,
  } state{S_init};

  std::basic_string<u_char> rxBuf;
  bool rxAnyFlag = false;
  double monitorSpeed = 125.0;
  U8 negotiatedInputId = 0;
  vector<string> inputVariables;
  vector<string> inputTypes;
  U8 negotiatedOutputId = 0;
  vector<string> outputVariables;
  vector<string> outputTypes;

  int verbose = 2;

  UniversalRobotMonitor(string _rHost, string _rPort)
      : rHost(_rHost), rPort(_rPort),
        label("urmon(" + rHost + ":" + rPort + ")")
  {
  }

  void addOutput(string const &type, string const &name)
  {
    outputTypes.push_back(type);
    outputVariables.push_back(name);
  }

  void addInput(string const &type, string const &name)
  {
    inputTypes.push_back(type);
    inputVariables.push_back(name);
  }


  bool error()
  {
    return state == S_error;
  }

  bool eof()
  {
    return state == S_eof;
  }

  void start()
  {
    asio::ip::tcp::resolver resolver(io_context);
    boost::system::error_code resolveError;
    // FIXME: should be async
    auto eps = resolver.resolve(asio::ip::tcp::v4(), rHost, rPort, resolveError);
    if (resolveError) {
      L() << label << ": resolve: " << resolveError;
      return;
    }

    if (verbose >= 1) L() << label << ": connecting...";
    asio::async_connect(sock, eps,
      [this, keepalive=shared_from_this()](const boost::system::error_code error, asio::ip::tcp::endpoint const &endpoint) {
        if (error) {
          L() << label << ": " << error.message();
          state = S_error;
          return;
        }
        sock.set_option(asio::ip::tcp::no_delay(true));
        commStart();
        return asyncRx();
      });
  }

  void asyncRx()
  {
    size_t rxBufStart = rxBuf.size();
    int const maxLen = 4096;
    rxBuf.resize(rxBuf.size() + maxLen);
    sock.async_read_some(asio::buffer(rxBuf.data() + rxBufStart, maxLen),
      [this, rxBufStart](boost::system::error_code error, size_t nr) {
        if (error) {
          if (error == boost::asio::error::eof) {
            L() << label << " > EOF\n";
            state = S_eof;
            return;
          }
          L() << label << ": " << error.message();
          state = S_error;
          return;
        }
        rxBuf.resize(rxBufStart + nr);

        double rxTime = realtime();
        if (!rxAnyFlag) {
          rxAnyFlag = true;
          if (verbose >= 1) L() << label << ": receiving on monitor socket\n";
        }

        forEachRtdePacket(rxBuf, [this, rxTime](U8 type, packet &rx) {
          rxPkt(type, rx, rxTime);
        });
        asyncRx();
      });
  }

  void close()
  {
    if (sock.is_open()) {
      sock.close();
    }
  }

  void rxPkt(U8 type, packet &rx, double rxTime)
  {
    if (verbose >= 4) {
      L() << label << " > pkt type="s << charname_hex(type) << " payload=" << rx.dump_hex();
    }

    switch ((int)type) {

      case RTDE_REQUEST_PROTOCOL_VERSION: {
        if (rx.size() != 1) {
          L() << label << " > RTDE_REQUEST_PROTOCOL_VERSION: bad length "s << rx.size();
          return;
        }
        bool success = !!rx.get_be_uint8();
        if (verbose >= 2) L() << label << " > requestProtocolVersion " << (success ? "ok" : "fail");
        rxRequestProtocolVersion(success, rxTime);
        break;
      }

      case RTDE_GET_URCONTROL_VERSION: {
        if (rx.size() != 16) {
          L() << label << " > RTDE_GET_URCONTROL_VERSION: bad length " << rx.size();
          return;
        }
        U32 major = rx.get_be_uint32();
        U32 minor = rx.get_be_uint32();
        U32 bugfix = rx.get_be_uint32();
        U32 build = rx.get_be_uint32();
        if (verbose >= 2) L() << label << " > getUrcontrolVersion " << major << "." << minor << "." << bugfix << "." << build;
        rxGetUrcontrolVersion(major, minor, bugfix, build, rxTime);
        break;
      }

      case RTDE_TEXT_MESSAGE: {
        string message = rx.get_len8_string();
        string source = rx.get_len8_string();
        uint32_t level = rx.get_be_uint8();
        if (verbose >= 2) L() << label << " > textMessage " << shellEscape(message) << " from " << shellEscape(source) << " level " << level;
        rxTextMessage(message, source, level, rxTime);
        break;
      }

      case RTDE_DATA_PACKAGE: {
        U8 recipeId = rx.get_be_uint8();
        if (onRxDataPackage) {
          onRxDataPackage(recipeId, rx, rxTime);
        }
        break;
      }

      case RTDE_CONTROL_PACKAGE_SETUP_OUTPUTS: {
        int id = rx.get_be_uint8();
        string types = rx.get_remainder_string();
        if (verbose >= 2) L() << label << " > controlPackageSetupOutputs id=" << id << " types=" << shellEscape(types);
        rxControlPackageSetupOutputs(id, splitCommas(types), rxTime);
        break;
      }

      case RTDE_CONTROL_PACKAGE_SETUP_INPUTS: {
        int id = rx.get_be_uint8();
        string types = rx.get_remainder_string();
        if (verbose >= 2) L() << label << " > controlPackageSetupInputs id=" << id << " types=" << shellEscape(types);
        rxControlPackageSetupInputs(id, splitCommas(types), rxTime);
        break;
      }

      case RTDE_CONTROL_PACKAGE_START: {
        bool success = !!rx.get_be_uint8();
        if (verbose >= 2) L() << label << " > controlPackageStart " << (success ? "ok" : "fail");
        rxControlPackageStart(success, rxTime);
        break;
      }

      case RTDE_CONTROL_PACKAGE_PAUSE: {
        bool success = !!rx.get_be_uint8();
        if (verbose >= 2) L() << label << " > controlPackagePause " << (success ? "ok" : "fail");
        rxControlPackagePause(success, rxTime);
        break;
      }

      default:
        L() << label << " > Unknown packet type " << repr_02x(type);
    }
  }

  void rxRequestProtocolVersion(bool success, double rxTime)
  {
    if (state == S_protocolVersionWait) {
      if (success) {
        txGetUrcontrolVersion();
        state = S_urcontrolVersionWait;
      }
      else {
        if (verbose >= 0) L() << label << " > requestProtocolVersion: failed to negotiate protocol version\n";
        state = S_error;
      }
    }
  }

  void rxGetUrcontrolVersion(U32 major, U32 minor, U32 bugfix, U32 build, double rxTime)
  {
    if (state == S_urcontrolVersionWait) {
      if (major == 3 && minor <= 2 && bugfix < 19171) {
        if (verbose >= 0) L() << label << " > rxGetUrcontrolVersion: robot version too old\n";
        state = S_error;
        return;
      }
      txControlPackageSetupInputs();
      state = S_setupInputsWait;
    }
  }

  void rxTextMessage(string message, string source, U8 level, double rxTime)
  {
  }

  void rxControlPackageSetupOutputs(int id, vector<string> types, double rxTime)
  {
    if (types != outputTypes) {
      if (verbose >= 0) L() << label << " > controlPackageSetupOutputs: type mismatch. Expected " << outputTypes << " got " << types;
      state = S_error;
      return;
    }
    negotiatedOutputId = id;
    if (state == S_setupOutputsWait) {
      txControlPackageStart();
      state = S_controlPackageStartWait;
    }
  }

  void rxControlPackageSetupInputs(U8 id, vector<string> types, double rxTime)
  {
    if (types != inputTypes) {
      if (verbose >= 0) L() << label << " > controlPackageSetupInputs: type mismatch. Expected " << inputTypes << ", got " << types;
      state = S_error;
      return;
    }
    negotiatedInputId = id;
    if (state == S_setupInputsWait) {
      txControlPackageSetupOutputs();
      state = S_setupOutputsWait;
    }
  }

  void rxControlPackageStart(bool success, double rxTime)
  {
    if (state == S_controlPackageStartWait) {
      if (success) {
        state = S_running;
      }
      else {
        if (verbose >= 0) L() << label << ": rxControlPackageStart: starting failed\n";
        state = S_error;
      }
    }
  }

  void rxControlPackagePause(bool success, double rxTime)
  {
  }

  void commStart()
  {
    state = S_protocolVersionWait;
    txRequestProtocolVersion(2);
  }

  void txPkt(U8 cmd, packet &tx)
  {
    size_t txSize = tx.size();
    assert(txSize >= 3);
    tx[0] = (U8)(txSize >> 8);
    tx[1] = (U8)(txSize >> 0);
    tx[2] = cmd;
    if (verbose >= 3) L() << label << " < cmd=" << charname_hex(cmd) << " pkt=" << tx.dump_hex();

    // Sync, but this shouldn't normally block
    boost::system::error_code writeError;
    asio::write(sock, asio::const_buffer(tx.begin(), tx.size()), writeError);
    if (writeError) {
      L() << label << ": write: " << writeError.message();
    }
  }

  void setupTxHeader(packet &tx)
  {
    tx.add_be_uint16(0);
    tx.add_be_uint8(0);
  }

  void txRequestProtocolVersion(U16 version)
  {
    packet tx;
    setupTxHeader(tx);
    tx.add_be_uint16(version);
    if (verbose >= 2) L() << label << " < requestProtocolVersion " << version;
    txPkt(RTDE_REQUEST_PROTOCOL_VERSION, tx);
  }

  void txGetUrcontrolVersion()
  {
    packet tx;
    setupTxHeader(tx);
    if (verbose >= 2) L() << label << " < getUrcontrolVersion\n";
    txPkt(RTDE_GET_URCONTROL_VERSION, tx);
  }

  void txControlPackageSetupInputs()
  {
    packet tx;
    setupTxHeader(tx);
    string variablesStr = joinChar(inputVariables, ',');
    tx.add_remainder_string(variablesStr);
    if (verbose >= 2) L() << label << " < controlPackageSetupInputs variables=" << variablesStr;
    txPkt(RTDE_CONTROL_PACKAGE_SETUP_INPUTS, tx);
  }

  void txControlPackageSetupOutputs()
  {
    packet tx;
    setupTxHeader(tx);
    tx.add_be_double(monitorSpeed);
    string variablesStr = joinChar(outputVariables, ',');
    tx.add_remainder_string(variablesStr);
    if (verbose >= 2) L() << label << " < controlPackageSetupOutputs speed=" << monitorSpeed << " variables=" << variablesStr;
    txPkt(RTDE_CONTROL_PACKAGE_SETUP_OUTPUTS, tx);
  }

  void txControlPackageStart()
  {
    packet tx;
    setupTxHeader(tx);
    if (verbose >= 2) L() << label << " < controlPackageStart\n";
    txPkt(RTDE_CONTROL_PACKAGE_START, tx);
  }

  void txControlPackagePause()
  {
    packet tx;
    setupTxHeader(tx);
    if (verbose >= 2) L() << label << " < controlPackagePause\n";
    txPkt(RTDE_CONTROL_PACKAGE_PAUSE, tx);
  }

  void txTextMessage(string message, string source, U8 type)
  {
    packet tx;
    setupTxHeader(tx);
    tx.add_len8_string(message);
    tx.add_len8_string(source);
    tx.add_be_uint8(type);
    if (verbose >= 2) L() << label << " < textMessage message=" << shellEscape(message) <<
      " source=" << shellEscape(source) << " type=" << charname_hex(type);
    txPkt(RTDE_TEXT_MESSAGE, tx);
  }

  void txDataPackage(std::function<void(packet &tx)> fillPkt)
  {
    packet tx;
    setupTxHeader(tx);
    tx.add_be_uint8(negotiatedInputId);
    fillPkt(tx);
    txPkt(RTDE_DATA_PACKAGE, tx);
  }

};
