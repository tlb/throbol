#pragma once
#include "./univarm.h"
#include "./universal_network_utils.h"

/*

  See https://www.universal-robots.com/articles/ur/dashboard-server-e-series-port-29999/

  telnet 172.17.2.138 29999
  robotmode
  Robotmode: RUNNING
  safetymode
  Safetymode: PROTECTIVE_STOP
  unlock protective stop
  Protective stop releasing
  safetymode
  Safetymode: NORMAL  
*/

struct UniversalRobotDashboard : enable_shared_from_this<UniversalRobotDashboard> {

  string rHost, rPort;
  string label;

  asio::ip::tcp::socket sock{io_context};
  string rxBuf;
  int verbose = 2;
  string safetyMode;
  string robotMode;

  deque<function<void()>> onRobotNormal;
  deque<function<void()>> onRobotBoned;

  enum {
    S_init,
    S_error,
    S_eof,
    S_qSafetyMode,
    S_qRobotMode,
    S_waitUnlock,
    S_robotNormal,
  } state{S_init};

  enum {
    G_none,
    G_robotNormal,
  } goal{G_none};

  UniversalRobotDashboard(string _rHost, string _rPort)
      : rHost(_rHost),
        rPort(_rPort),
        label("urdash(" + rHost + ":" + rPort + ")")
  {
  }

  void start()
  {
    asio::ip::tcp::resolver resolver(io_context);
    boost::system::error_code resolveError;
    // FIXME: should be async
    auto eps = resolver.resolve(asio::ip::tcp::v4(), rHost, rPort, resolveError);
    if (resolveError) {
      L() << label << ": resolve: " << resolveError;
      return;
    }

    if (verbose >= 1) L() << label << ": connecting...";
    asio::async_connect(sock, eps,
      [this, keepalive=shared_from_this()](const boost::system::error_code error, asio::ip::tcp::endpoint const &endpoint) {
        if (error) {
          L() << label << ": " << error.message();
          state = S_error;
          return;
        }
        sock.set_option(asio::ip::tcp::no_delay(true));
        commStart();
        return asyncRx();
      });
  }

  void asyncRx()
  {
    size_t start = rxBuf.size();
    int const maxLen = 4096;
    rxBuf.resize(rxBuf.size() + maxLen);
    sock.async_read_some(asio::buffer(rxBuf.data() + start, maxLen),
      [this, start](boost::system::error_code error, size_t nr) {
        if (error) {
          if (error == asio::error::eof) {
            state = S_eof;
            return;
          }
          L() << label << ": " << error.message();
          state = S_error;
          return;
        }
        rxBuf.resize(start + nr);

        forEachLine(rxBuf, [this](string const &l) {
          if (verbose >= 2) L() << label << " > " << shellEscape(l);
          if (startsWith(l, "Safetymode: ")) {
            safetyMode = l.substr(12);
            if (state == S_qSafetyMode) {
              txCmd("robotmode\n");
              state = S_qRobotMode;
            }
            else {
              L() << label << ": unexpected Safetymode:\n";
              state = S_error;
            }
          }
          else if (startsWith(l, "Robotmode: ")) {
            robotMode = l.substr(11);
            if (state == S_qRobotMode) {
              if (robotMode == "RUNNING" && safetyMode == "PROTECTIVE_STOP") {
                txCmd("unlock protective stop\n");
                state = S_waitUnlock;
              }
              else if (robotMode == "RUNNING" && safetyMode == "NORMAL") {
                state = S_robotNormal;
              }
              else {
                L() << label << ": robotMode=" << robotMode << " safetyMode=" << safetyMode;
                state = S_error;
              }
            }
            else {
              L() << label << ": unexpected Robotmode:\n";
              state = S_error;
            }
          }
          else if (l == "Protective stop releasing") {
            state = S_robotNormal;
          }
          else if (startsWith(l, "Connected: ")) {
            if (goal == G_robotNormal && state == S_init) {
              state = S_qSafetyMode;
              txCmd("safetymode\n");
            }
          }
          else {
            L() << "Unknown dashboard msg: " << shellEscape(l);
            state = S_error;
          }
        });
        doCallbacks();
        asyncRx();
      });
  }

  void txCmd(string const &l)
  {
    if (verbose >= 2) L() << label << " < " << shellEscape(l);

    // Sync, but this shouldn't normally block
    boost::system::error_code writeError;
    asio::write(sock, asio::const_buffer(l.data(), l.size()), writeError);
    if (writeError) {
      L() << label << ": write: " << writeError.message();
    }
  }

  void makeRobotNormal(function<void()> onSuccess, function<void()> onFailure)
  {
    onRobotNormal.push_back(onSuccess);
    onRobotBoned.push_back(onFailure);
    doCallbacks();
    goal = G_robotNormal;
  }

  void commStart()
  {
  }

  void doCallbacks()
  {
    if (state == S_robotNormal) {
      while (!onRobotNormal.empty()) {
        onRobotNormal.front()();
        onRobotNormal.pop_front();
      }
      close();
    }
    else if (state == S_error || state == S_eof) {
      while (!onRobotBoned.empty()) {
        onRobotBoned.front()();
        onRobotBoned.pop_front();
      }
      close();
    }
  }

  void close()
  {
    if (sock.is_open()) {
      sock.close();
    }
  }
  
};
