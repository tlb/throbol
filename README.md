# Throbol

See our [home page](http://throbol.com) for an overview

See [language reference](./LANGUAGE.md)

## Installation
(Or see [more detail](./INSTALL.md))

On Mac or Linux:
```sh
git clone --recursive https://gitlab.com/tlb/throbol
cd throbol
make install.deps
make
```

To try a standalone example:
```
bin/tb examples/lorenz.tb
```
