
## Overview

Throbol is a time-domain spreadsheet. Like a spreadsheet, every cell has a name and formula, and cells can refer to each other by name. The recalculation engine automatically figures out what order to calculate them in. Unlike most spreadsheets, each cell's value is a time series. You can refer to a cell's current value as `foo`, and its previous value as `foo[-dt]`.

Throbol can be used in 3 modes:

 - controlling live robot hardware.
 - controlling a simulated robot.
 - a pure spreadsheet.

When controlling a live robot, runs must be started with ⌘L. With a simulated robot, it will re-run the simulation whenever a formula or parameter changes.




