
# Sed script to modify the .mjs linker file to fix some emscripten bugs

# Otherwise, it throws an error when closing window
s/if (typeof target.onwheel != "undefined")/if (target \&\& typeof target.onwheel != "undefined")/g

# These are puzzling. Not sure how it ever worked.
s/const {createRequire: createRequire} = await import.*//g
s/const { createRequire } = await import.*//g
s/scriptDirectory = require.*//g

# Avoid warning from chrome that preventDefault can't be called on a non-passive event
s/addEventListener(eventHandler.eventTypeString, jsEventHandler, eventHandler.useCapture)/addEventListener(eventHandler.eventTypeString, jsEventHandler, {passive: false, capture: eventHandler.useCapture})/g

