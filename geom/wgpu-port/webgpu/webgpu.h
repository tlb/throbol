
#if defined(__EMSCRIPTEN__)
#include <webgpu/webgpu.h>
#else
#include "ffi/webgpu-headers/webgpu.h"
#include "ffi/wgpu.h"
#endif

#define WEBGPU_BACKEND_WGPU 1
#define WEBGPU_FILTER_FLOAT 0
#define wgpuInstanceRelease wgpuInstanceDrop
#define wgpuBindGroupRelease wgpuBindGroupDrop
#define wgpuBindGroupLayoutRelease wgpuBindGroupLayoutDrop
#define wgpuBufferRelease wgpuBufferDrop
#define wgpuRenderPipelineRelease wgpuRenderPipelineDrop
#define wgpuPipelineLayoutRelease wgpuPipelineLayoutDrop
#define wgpuSamplerRelease wgpuSamplerDrop
#define wgpuShaderModuleRelease wgpuShaderModuleDrop
#define wgpuTextureViewRelease wgpuTextureViewDrop
#define wgpuTextureRelease wgpuTextureDrop
#define wgpuSwapChainRelease wgpuSwapChainDrop
#define wgpuAdapterRelease wgpuAdapterDrop

#if defined(__linux__)
#define wgpuQueueRelease(X) do {} while (0)
#else
#define wgpuQueueRelease wgpuQueueDrop
#endif

