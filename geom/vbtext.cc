#include "./vbtext.h"
#include "./gl_headers.h"

vector<pair<char32_t, U32>> vbParseUtf8(string_view s)
{
  vector<pair<char32_t, U32>> ret;
  char const *p = s.data();
  char const *pend = s.data() + s.size();

  while (p != pend) {
    U32 fofs = p - s.data();
    char32_t c = char32_t(U8(*p));
    if (c == 0) {
      break;
    }
    if (c < 0x80) {
      p ++;
    }
    else {
      static_assert(sizeof(char32_t) == sizeof(U32));
      p += ImTextCharFromUtf8((U32 *)&c, p, pend);
      if (c == 0) {
        break;
      }
    }
    ret.emplace_back(c, fofs);
  }
  ret.emplace_back(0, s.size());
  return ret;
}
