#pragma once
#include "common/std_headers.h"
#include "numerical/numerical.h"
#define USE_STL_SOLID 1
#include "./gl_headers.h"
#include "./gl_buffer.h"
#include "./geom.h"
#include <typeindex>
#include "tiny_obj_loader.h"
#include "../src/hashcode.h"

struct MainWindow;
struct G8GeomCacheEnt;
struct G8DrawCmd;
struct G8ShaderSetup;

constexpr size_t MAX_G0_DYNAMIC_UNIFORMS = 3;
constexpr size_t MAX_G1_BINDINGS = 2;
constexpr size_t MAX_GRAPH_TRACES = 40;

struct G8GeomCacheEnt {
  uint32_t elemIndex = 0;
  uint32_t elemCount = 0;
  uint32_t viBufIndex = 0;

  G8GeomCacheEnt(struct G8IdxBlaster const &ib);

};

// per-shader, per-window
struct G8ResourcesPerShaderWindow {
  bool setupDone = false;
  WGPUProgrammableStageDescriptor vtxHandle{}, frgHandle{};
  WGPUPipelineLayout pipelineLayout = nullptr;
  WGPURenderPipeline pipeline = nullptr;

  /*
    We only have to remake this bind group if the size of the
    uniforms buffer changes.
  */
  WGPUBindGroup bindGroup0 = nullptr;
  size_t bindGroup0UniformsSize = 0;

  struct {
    int projviewUniforms = -1;
    int modelUniforms = -1;
    int traceUniforms = -1;
    int dialUniforms = -1;
    int count = 0;
  } g0DynamicOffsets;


  /*
    Bind groups that get created every call (say, since the texture changes)
    are stashed here and released after rendering.
  */
  vector<WGPUBindGroup> ephemeralBindGroups;
  vector<WGPUTextureView> ephemeralTextureViews;

  vector<WGPUBindGroupLayout> bgLayouts;

  vector<G8MemBuffer> idxBufs;
  vector<G8MemBuffer> vtxBufs;

  vector<G8GpuBuffer> idxGpus;
  vector<G8GpuBuffer> vtxGpus;
  G8Cache<G8GeomCacheEnt> geomCache;

  WGPUSampler fontSampler = nullptr;

  WGPUSampler imgSampler = nullptr;

  vector<WGPUTexture> textures;
  vector<WGPUBuffer> buffers;

  G8ResourcesPerShaderWindow() = default;
  G8ResourcesPerShaderWindow(G8ResourcesPerShaderWindow && other) = default;
  ~G8ResourcesPerShaderWindow();
};


struct G8ResourcesPerWindow {

  G8MemBuffer skinUniformsBuf;
  optional<G8GpuBuffer> skinUniformsGpu;

  G8MemBuffer uniformsBuf;
  optional<G8GpuBuffer> uniformsGpu;

  G8Cache<WGPUTexture> textureCache;

};


struct StlSolid;

struct ProjViewUniforms {
  mat4 proj;
  mat4 view;
};
static_assert(sizeof(ProjViewUniforms) % 16 == 0);

struct ModelUniforms {
  mat4 model;
  float inflation;
  float _pad0, _pad1, _pad2;

  ModelUniforms(mat4 const &_model, float _inflation=0.0f)
    : model(_model),
      inflation(_inflation)
  {
  }
};
static_assert(sizeof(ModelUniforms) % 16 == 0);

enum TraceVisInfoType {
  TVIT_line,
  TVIT_beh,
  TVIT_check,
};

struct TraceVisInfo {
  uint32_t traceType;
  uint32_t colorIndex;
  uint32_t dataIndex;
  uint32_t dataCount;
  uint32_t dataStride;
  float alpha;
  float thick;
  float _pad0;
};

struct TraceUniforms {
  float scalePos;
  float scaleNeg;
  float range;
  uint32_t nSamples;
  uint32_t isCheckFailure;
  uint32_t isRewardCell;
  uint32_t _pad[2];
};
static_assert(sizeof(TraceUniforms) % 16 == 0);


struct DialUniforms {
  float value;
  float rangeLo;
  float rangeHi;
};
static_assert(sizeof(TraceUniforms) % 16 == 0);


struct SkinUniforms {
  vec4 dialBgCol;
  vec4 dialPosCol;
  vec4 dialNegCol;
  vec4 dialRangeCol;
  vec4 traceBgColBase;
  vec4 traceBgColFailure;
  vec4 traceBgColReward;

  vec4 goodGraphColors[MAX_GRAPH_TRACES];
};
static_assert(sizeof(SkinUniforms) % 16 == 0);

enum G8VertexType {
  // p: vec3 pos
  // n: vec3 normal
  // u: vec2 for texture
  // c: vec4 color
  // v: vec4 values
  // l: float layer
  G8VertexType_pnl,
  G8VertexType_pul,
  G8VertexType_puvl,
  G8VertexType_pcl,
  G8VertexType_pncl,
  G8VertexType_pucl,
  G8VertexType_pnul,
  G8VertexType_pnucl,
  // ADD vertex type (search for this elsewhere when adding)
};

ostream & operator <<(ostream &s, G8VertexType vt);


struct G8Vtx_pnl {

  F pos[3];
  F normal[3];
  F layer;

  static constexpr G8VertexType vt = G8VertexType_pnl;
  static WGPUVertexAttribute attribute_desc[3];

  G8Vtx_pnl() noexcept {}
  G8Vtx_pnl(vec3 const &_pos, vec3 const &_normal, F _layer) noexcept
  {
    pos[0] = _pos[0]; pos[1] = _pos[1]; pos[2] = _pos[2];
    normal[0] = _normal[0]; normal[1] = _normal[1]; normal[2] = _normal[2];
    layer = _layer;
  }
};

struct G8Vtx_pcl {

  F pos[3];
  F color[4];
  F layer;

  static constexpr G8VertexType vt = G8VertexType_pcl;
  static WGPUVertexAttribute attribute_desc[3];

  G8Vtx_pcl() noexcept {}
  G8Vtx_pcl(vec3 const &_pos, vec4 const &_color, F _layer) noexcept
  {
    pos[0] = _pos[0]; pos[1] = _pos[1]; pos[2] = _pos[2];
    color[0] = _color[0]; color[1] = _color[1]; color[2] = _color[2]; color[3] = _color[3];
    layer = _layer;
  }
};

struct G8Vtx_pul {

  F pos[3];
  F uv[2];
  F layer;

  static constexpr G8VertexType vt = G8VertexType_pul;
  static WGPUVertexAttribute attribute_desc[3];

  G8Vtx_pul() noexcept {}
  G8Vtx_pul(vec3 const &_pos, vec2 const &_uv, F _layer) noexcept
  {
    pos[0] = _pos[0]; pos[1] = _pos[1]; pos[2] = _pos[2];
    uv[0] = _uv[0]; uv[1] = _uv[1];
    layer = _layer;
  }
};


struct G8Vtx_puvl {

  F pos[3];
  F uv[2];
  F value[4];
  F layer;

  static constexpr G8VertexType vt = G8VertexType_puvl;
  static WGPUVertexAttribute attribute_desc[4];

  G8Vtx_puvl() noexcept {}
  G8Vtx_puvl(vec3 const &_pos, vec2 const &_uv, vec4 const &_value, F _layer) noexcept
  {
    pos[0] = _pos[0]; pos[1] = _pos[1]; pos[2] = _pos[2];
    uv[0] = _uv[0]; uv[1] = _uv[1];
    value[0] = _value[0]; value[1] = _value[1]; value[2] = _value[2]; value[3] = _value[3];
    layer = _layer;
  }
};

struct G8Vtx_pucl {

  F pos[3];
  F uv[2];
  F color[4];
  F layer;

  static constexpr G8VertexType vt = G8VertexType_pucl;
  static WGPUVertexAttribute attribute_desc[4];

  G8Vtx_pucl() noexcept {}
  G8Vtx_pucl(vec3 const &_pos, vec2 const &_uv, vec4 const &_color, F _layer) noexcept
  {
    pos[0] = _pos[0]; pos[1] = _pos[1]; pos[2] = _pos[2];
    uv[0] = _uv[0]; uv[1] = _uv[1];
    color[0] = _color[0]; color[1] = _color[1]; color[2] = _color[2]; color[3] = _color[3];
    layer = _layer;
  }
};

struct G8Vtx_pncl {
  F pos[3];
  F normal[3];
  F color[4];
  F layer;

  static constexpr G8VertexType vt = G8VertexType_pncl;
  static WGPUVertexAttribute attribute_desc[4];

  G8Vtx_pncl() noexcept {}
  G8Vtx_pncl(vec3 const &_pos, vec3 const &_normal, vec4 const &_color, F _layer) noexcept
  {
    pos[0] = _pos[0]; pos[1] = _pos[1]; pos[2] = _pos[2];
    normal[0] = _normal[0]; normal[1] = _normal[1]; normal[2] = _normal[2];
    color[0] = _color[0]; color[1] = _color[1]; color[2] = _color[2]; color[3] = _color[3];
    layer = _layer;
  }
};

struct G8Vtx_pnul {

  F pos[3];
  F normal[3];
  F uv[2];
  F layer;

  static constexpr G8VertexType vt = G8VertexType_pnul;
  static WGPUVertexAttribute attribute_desc[4];

  G8Vtx_pnul() noexcept {}
  G8Vtx_pnul(vec3 const &_pos, vec3 const &_normal, vec2 const &_uv, F _layer) noexcept
  {
    pos[0] = _pos[0]; pos[1] = _pos[1]; pos[2] = _pos[2];
    normal[0] = _normal[0]; normal[1] = _normal[1]; normal[2] = _normal[2];
    uv[0] = _uv[0]; uv[1] = _uv[1];
    layer = _layer;
  }
};

struct G8Vtx_pnucl {

  F pos[3];
  F normal[3];
  F uv[2];
  F color[4];
  F layer;

  static constexpr G8VertexType vt = G8VertexType_pnucl;
  static WGPUVertexAttribute attribute_desc[5];

  G8Vtx_pnucl() noexcept {}
  G8Vtx_pnucl(vec3 const &_pos, vec3 const &_normal, vec2 const &_uv, vec4 const &_color, F _layer) noexcept
  {
    pos[0] = _pos[0]; pos[1] = _pos[1]; pos[2] = _pos[2];
    normal[0] = _normal[0]; normal[1] = _normal[1]; normal[2] = _normal[2];
    uv[0] = _uv[0]; uv[1] = _uv[1];
    color[0] = _color[0]; color[1] = _color[1]; color[2] = _color[2]; color[3] = _color[3];
    layer = _layer;
  }
};


struct G8ShaderGeneric {

  string name;
  string vtxMain;
  string frgMain;

  bool doDepthTest = true;
  bool doDepthWrite = true;

  struct {
    int imgSampler = -1;
    int fontSampler = -1;
    int fontTex = -1;
    int modelUniforms = -1;
    int traceUniforms = -1;
    int skinUniforms = -1;
    int dialUniforms = -1;
  } g0Binding;

  struct {
    int imgTex = -1;
    int traceData = -1;
    int traceVisInfo = -1;
  } g1Binding;

  int shaderIndex = -1;
  int cmdBufIndex = 0;
  static vector<G8ShaderGeneric *> *allShaders;

  G8ShaderGeneric(string_view _name,initializer_list<string_view> args);
  virtual ~G8ShaderGeneric();
  void parseArgs(initializer_list<string_view> args);
  void compile(G8ShaderSetup &setup, WGPUDevice wgpu_device, WGPUQueue wgpu_queue);
  void compile1(WGPUProgrammableStageDescriptor &out, WGPUDevice wgpu_device, string const &code);
  WGPUBindGroup mkBindGroup0(G8ShaderSetup &setup, MainWindow &win);
  WGPUBindGroup mkBindGroup1(G8ShaderSetup &setup, MainWindow &win,
    std::array<uint32_t, MAX_G1_BINDINGS> &g1Data);

  virtual WGPUVertexBufferLayout getVertexBufferLayout() const = 0;
  virtual size_t getVertexSize() const = 0;
  virtual G8VertexType getVertexType() const = 0;

};

template<typename VertexT>
struct G8Shader : G8ShaderGeneric {

  G8Shader(string_view _name, initializer_list<string_view> args)
    :G8ShaderGeneric(_name, args)
  {
  }

  size_t getVertexSize() const override
  {
    return sizeof(VertexT);
  }

  G8VertexType getVertexType() const override
  {
    return VertexT::vt;
  }

  WGPUVertexBufferLayout getVertexBufferLayout() const override
  {
    WGPUVertexBufferLayout ret = {
      .arrayStride = sizeof(VertexT),
      .stepMode = WGPUVertexStepMode_Vertex,
      .attributeCount = sizeof(VertexT::attribute_desc) / sizeof(WGPUVertexAttribute),
      .attributes = VertexT::attribute_desc
    };
    return ret;
  }

};


extern G8Shader<G8Vtx_pcl> rawColorShader;
extern G8Shader<G8Vtx_pncl> litSolidShader;
extern G8Shader<G8Vtx_pnul> litTexSolidShader;
extern G8Shader<G8Vtx_pul> videoShader;
extern G8Shader<G8Vtx_pncl> unshinySolidShader;
extern G8Shader<G8Vtx_pucl> glowingTextShader;
extern G8Shader<G8Vtx_pcl> maskShader;
extern G8Shader<G8Vtx_pul> traceShader;
extern G8Shader<G8Vtx_pul> dialShader;

struct G8DrawCmd {

  int shaderIndex = -1;
  int viBufIndex = -1;
  uint32_t elemIndex = 0;
  uint32_t elemCount = 0;
  std::array<uint32_t, MAX_G0_DYNAMIC_UNIFORMS> g0Data;
  std::array<uint32_t, MAX_G1_BINDINGS> g1Data;

  G8DrawCmd(
    int _shaderIndex,
    int _viBufIndex,
    uint32_t _elemIndex,
    uint32_t _elemCount,
    std::array<uint32_t, MAX_G0_DYNAMIC_UNIFORMS> const &_g0Data,
    std::array<uint32_t, MAX_G1_BINDINGS> const &_g1Data) noexcept
    : shaderIndex(_shaderIndex),
      viBufIndex(_viBufIndex),
      elemIndex(_elemIndex),
      elemCount(_elemCount),
      g0Data(_g0Data),
      g1Data(_g1Data)
  {
  }

};

// Per-shader, per-window, per-drawlist
struct G8ShaderSetup {
  G8ShaderGeneric *shader = nullptr;
  struct G8ResourcesPerShaderWindow *perSW = nullptr;
  struct G8ResourcesPerWindow *perW = nullptr;
  G8VertexType vt;
  bool uniformsInited = false;
  int viBufIndex = 0;

  std::array<uint32_t, MAX_G0_DYNAMIC_UNIFORMS> g0Data;
  std::array<uint32_t, MAX_G1_BINDINGS> g1Data;
};

template<typename VertexT>
struct G8VtxBlaster {

  G8ShaderSetup &setup;
  size_t maxAdd = 0;

  VertexT *startp = nullptr;
  VertexT *p = nullptr;
  

  G8VtxBlaster(G8ShaderSetup &_setup, size_t _maxAdd)
  : setup(_setup),
    maxAdd(_maxAdd)
  {
    setup.perSW->vtxBufs.resize(max(setup.perSW->vtxBufs.size(), size_t(setup.viBufIndex + 1)));
    assertlog(setup.shader->getVertexSize() == sizeof(VertexT), "shader=" << setup.shader->name << LOGV(setup.shader->getVertexSize()) << LOGV(sizeof(G8VertexType)));
    setup.perSW->vtxBufs[setup.viBufIndex].need(maxAdd * sizeof(VertexT));
    startp = (VertexT *)setup.perSW->vtxBufs[setup.viBufIndex].cur;
    p = startp;
  }

  G8VtxBlaster(G8ShaderSetup &_setup, size_t _maxAdd, uint32_t &vi)
    : G8VtxBlaster(_setup, _maxAdd)
  {
    vi = startp - (VertexT *)setup.perSW->vtxBufs[setup.viBufIndex].begin;
  }

  ~G8VtxBlaster()
  {
    assertlog(ssize_t(p - startp) <= ssize_t(maxAdd), "overrun. " << LOGV(p - startp) << LOGV(maxAdd));
    setup.perSW->vtxBufs[setup.viBufIndex].cur = (U8 *)p;
  }

  bool operator ()() const { return startp != nullptr; }

  template<typename ...Args>
  void operator() (Args&& ...args)
  {
    new(p) VertexT(std::forward<Args>(args)...);
    p ++;
  }

};


struct G8IdxBlaster {

  uint32_t *p = nullptr;
  G8ShaderSetup &setup;
  size_t maxAdd = 0;
  uint32_t *startp = nullptr;

  G8IdxBlaster(G8ShaderSetup &_setup, size_t _maxAdd)
  : setup(_setup),
    maxAdd(_maxAdd)
  {
    setup.perSW->idxBufs.resize(max(setup.perSW->idxBufs.size(), size_t(setup.viBufIndex + 1)));
    setup.perSW->idxBufs[setup.viBufIndex].need(maxAdd * sizeof(uint32_t));
    startp = (uint32_t *)setup.perSW->idxBufs[setup.viBufIndex].cur;
    p = startp;
  }

  G8IdxBlaster(G8ShaderSetup &_setup, size_t _maxAdd, uint32_t &vi)
    : G8IdxBlaster(_setup, _maxAdd)
  {
    vi = startp - (uint32_t *)setup.perSW->idxBufs[setup.viBufIndex].begin;
  }

  ~G8IdxBlaster()
  {
    assertlog(ssize_t(p - startp) <= ssize_t(maxAdd), "overrun. " << LOGV(p - startp) << LOGV(maxAdd));
    assertlog((U8 *)p <= setup.perSW->idxBufs[setup.viBufIndex].end, LOGV((void *)p) << LOGV((void *)setup.perSW->idxBufs[setup.viBufIndex].end));
    setup.perSW->idxBufs[setup.viBufIndex].cur = (U8 *)p;
  }

  bool operator ()() const { return startp != nullptr; }

  uint32_t getElemCount() const { return p - startp; }
  uint32_t getStartIndex() const { return startp - (uint32_t *)setup.perSW->idxBufs[setup.viBufIndex].begin; }

  void point(uint32_t a)
  {
    *p++ = a;
  }

  void line(uint32_t a, uint32_t b)
  {
    *p++ = a;
    *p++ = b;
  }

  void tri(uint32_t a, uint32_t b, uint32_t c)
  {
    *p++ = a;
    *p++ = b;
    *p++ = c;
  }

  /*
    A   B
    C   D
    Add 2 triangles ABC and BDC, both in clockwise order
  */
  void tripair(uint32_t a, uint32_t b, uint32_t c, uint32_t d)
  {
    *p++ = a;
    *p++ = b;
    *p++ = c;

    *p++ = b;
    *p++ = d;
    *p++ = c;
  }

};



template<typename T>
struct PutterBacker {
  T *where = nullptr;
  T oldval;

  PutterBacker(T *_where, T const &newval)
    :where(_where)
  {
    oldval = *where;
    *where = newval;
  }
  PutterBacker()
    :where(nullptr)
  {
  }

  ~PutterBacker()
  {
    if (where) {
      *where = oldval;
      where = nullptr;
    }
  }
  void permanent()
  {
    where = nullptr;
  }
  PutterBacker(PutterBacker const &other) = delete;
  PutterBacker &operator =(PutterBacker const &other) = delete;
  PutterBacker(PutterBacker &&other)
  {
    where = other.where;
    oldval = other.oldval;
    other.where = nullptr;
  }
};


struct G8DrawList {

  MainWindow &win;
  vector<vector<G8DrawCmd>> cmdBufs;

  vector<G8ShaderSetup> setups;

  vec3 homeEyepos;
  vec3 homeLookat;
  vec3 homeUp;
  float homeFov = -1.0;

  vec3 eyepos;
  vec3 lookat;
  vec3 up;
  float fov = -1.0;
  float dt = -1.0;
  double lastDrawTime = 0.0;
  float nearZ = -1.0;
  float farZ = -1.0;

  std::function<void(G8DrawList &dl)> calcProjView;
  mat4 proj, view;
  mat4 iproj, iview;

  bool fullScreen = false;
  int vpL = 0, vpT = 0, vpR = 0, vpB = 0;
  float glVpL = 0, glVpR = 0, glVpT = 0, glVpB = 0;

  vec3 pickRayOrigin, pickRayDir;
  bool pickRayActive = false;

  vec3 saveDir;
  bool saveDirActive = false;


  G8DrawList(MainWindow &_win);
  ~G8DrawList();

  G8ShaderSetup &getSetup(G8ShaderGeneric const *shader);

  void webgpuRender(WGPURenderPassEncoder wgpu_pass, bool clearDepthBuffer=false);

  void camFov(float target);
  void camEyepos(vec3 target);
  void camLookat(vec3 target);
  void camStrafe(vec3 dir);
  void camHoverFly(vec3 dir);
  void camFlyHome();
  void camPan(vec2 dir);
  void camOrbitElevate(float orbit, float elevate);
  void camHome();
  void setHoverHome();
  void camIso();
  void camCheckSanity();
  void setProjView();

  void firstPersonControls();
  void hoverControls();

  void setInterval();
  void setPickRay();
  void clearPickRay();
  ImVec2 projectToScreen(vec3 pos);
  vector<tuple<vec3, vec3>> screenCornerRays();
  vector<vec3> planeVisibleRegion(vec3 planeOrig, vec3 planeNormal);

  void clear();
  void badShader(G8ShaderGeneric const *shader, char const *where, initializer_list<G8VertexType> vts);

  void addCmd(G8ShaderSetup &setup, uint32_t elemIndex, uint32_t elemCount);
  void addCmd(G8ShaderSetup &setup, G8GeomCacheEnt ent);
  void addCmd(G8IdxBlaster &blaster);

  F pixelSize(vec3 pos, F thick);
  F sizeForPixels(vec3 pos, F numPixels);

  PutterBacker<uint32_t> withModelUniforms(ModelUniforms const &modelUniforms, G8ShaderGeneric const *shader);
  PutterBacker<uint32_t> withTraceUniforms(TraceUniforms const &traceUniforms, G8ShaderGeneric const *shader);
  PutterBacker<uint32_t> withDialUniforms(DialUniforms const &dialUniforms, G8ShaderGeneric const *shader);
  PutterBacker<uint32_t> withImgTex(WGPUTexture texture, G8ShaderGeneric const *shader);
  PutterBacker<uint32_t> withTraceData(WGPUBuffer texture, G8ShaderGeneric const *shader);
  PutterBacker<uint32_t> withTraceVisInfo(WGPUBuffer texture, G8ShaderGeneric const *shader);
  PutterBacker<int> withViBuf(int viBufIndex, G8ShaderGeneric const *shader);

  void addDot(
    vec3 const &a,
    vec4 const &color,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &rawColorShader);

  void addLine(
    vec3 const &a, vec4 const &aCol,
    vec3 const &b, vec4 const &bCol,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &rawColorShader);

  void addTriangle(
    vec3 const &a, vec4 const &aCol,
    vec3 const &b, vec4 const &bCol,
    vec3 const &c, vec4 const &cCol,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &litSolidShader);

  void addCircle(
    vec3 const &center,
    vec3 const &axis0,
    vec3 const &axis1,
    vec4 const &color,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &litSolidShader);

  void addQuad(
    vec3 const &a, vec4 const &aCol,
    vec3 const &b, vec4 const &bCol,
    vec3 const &c, vec4 const &cCol,
    vec3 const &d, vec4 const &dCol,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &litSolidShader);

  void addQuad(
    vec3 const &a, vec2 const &auv, vec4 const &aCol,
    vec3 const &b, vec2 const &buv, vec4 const &bCol,
    vec3 const &c, vec2 const &cuv, vec4 const &cCol,
    vec3 const &d, vec2 const &duv, vec4 const &dCol,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &litSolidShader);

  void addDial(
    vec3 const &center, vec3 const &xdir, vec3 const &ydir,
    vec4 const &value,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &dialShader);

  void addQuad(
    vec3 const &a, vec3 const &aNorm, vec4 const &aCol,
    vec3 const &b, vec3 const &bNorm, vec4 const &bCol,
    vec3 const &c, vec3 const &cNorm, vec4 const &cCol,
    vec3 const &d, vec3 const &dNorm, vec4 const &dCol,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &litSolidShader);

  void addQuad(
    vec3 const &a, vec2 const &auv,
    vec3 const &b, vec2 const &buv,
    vec3 const &c, vec2 const &cuv,
    vec3 const &d, vec2 const &duv,
    F layer,
    G8ShaderGeneric const *shader = &traceShader);

  void addBox(
    mat4 const &m,
    vec4 const &col,
    vec3 const &dim,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &litSolidShader);

  void addAnnulus(
    vec3 const &center,
    vec3 const &radx, vec3 const &rady,
    F innerRad, F outerRad,
    F th0, F th1,
    vec4 const &color,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &litSolidShader);

  void addTriangle(
    vec3 const &a, vec3 const &aNorm, vec4 const &aCol,
    vec3 const &b, vec3 const &bNorm, vec4 const &bCol,
    vec3 const &c, vec3 const &cNorm, vec4 const &cCol,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &litSolidShader);

  void addGlyph(
    vec3 &curPos, vec3 const &xDir, vec3 const &yDir, vec3 const &normal,
    vec4 const &color,
    const ImFontGlyph* glyph,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &glowingTextShader);

  void alignText(
    vec3 &curPos, vec3 const &xDir, vec3 const &yDir,
    string_view text,
    ImFont *font,
    F xJust, F yJust);

  void addTextAdvance(
    vec3 &curPos, vec3 const &xDir, vec3 const &yDir, vec3 const &norm,
    string_view text,
    vec4 const &color,
    ImFont *font,
    F layer = 0.25f,
    G8ShaderGeneric const *shader = &glowingTextShader);

  void addText(
    vec3 const &curPos, vec3 const &xDir, vec3 const &yDir, vec3 const &norm,
    string_view text,
    vec4 const &color,
    ImFont *font,
    F xJust, F yJust,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &glowingTextShader);

  void addCapsule(
    mat4 const &tpos,
    vec4 const &col,
    vec3 const &size,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &litSolidShader);

  void addArrow(
    vec3 const &root,
    vec3 const &axis,
    vec3 const &ca, 
    vec3 const &cb, 
    vec4 const &color,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &litSolidShader);

  void addArrowTriple(
    mat4 const &model,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &litSolidShader);

  void addNoodle(
    vec3 const &p0, vec4 const &col0,
    vec3 const &p1, vec4 const &col1, 
    vec3 &tubeAxis, // Provide an initial axis, then it's modified to track a continuous sequence
    float tubeRad,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &litSolidShader);

  void addNoodle(
    vec3 const &p0, vec4 const &col0,
    vec3 const &p1, vec4 const &col1, 
    float tubeRad,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &litSolidShader);

  void addCatenary(
    vec3 const &srcp, vec4 const &srccol,
    vec3 const &dstp, vec4 const &dstcol,
    vec3 const &up,
    float sagDepth, // depth at maximum sag
    float sagShape,
    float tubeRad,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &litSolidShader);

  void addThickCurve(
    vector<vec3> const &pts,
    vec3 const &up,
    vec4 const &color,
    F width,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &glowingTextShader);

  void addBand(
    vector<pair<vec3, vec3>> const &pts,
    vec3 const &up,
    vec4 const &color,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &glowingTextShader);

  void addVideo(
    mat4 const &model,
    WGPUTexture tex,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &videoShader);

  void addSphere(
    vec3 const &center,
    vec3 const &axis0,
    vec3 const &axis1,
    vec3 const &axis2,
    vec4 const &color,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &litSolidShader);

#ifdef USE_STL_SOLID
  StlSolid *getNamedSolid(
    string const &fn);

  void addSolid(
    StlSolid const &solid,
    mat4 const &m,
    vec4 const &color,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &litSolidShader);
#endif

  static tinyobj::ObjReader *getNamedObj(string const &fn);

  void addSolid(
    tinyobj::mesh_t const &mesh, tinyobj::attrib_t const &attrib,
    mat4 const &m,
    vec4 const &color,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &litSolidShader);

  void addSolid(
    string const &fn,
    mat4 const &m,
    vec4 const &color,
    F layer = 0.0f);

  // Red text that says "Loading" at the point.
  void addLoading(
    vec3 const &p0,
    F fontSize,
    F layer = 0.0f,
    G8ShaderGeneric const *shader = &glowingTextShader);

  bool hitTestTriangle(vec3 const &a, vec3 const &b, vec3 const &c, vec3 &hit, float &distance);
  bool hitTestQuad(vec3 const &a, vec3 const &b, vec3 const &c, vec3 const &d, vec3 &hit, float &distance);
  bool hitTestPlane(vec3 const &planeOrig, vec3 const &planeNormal, vec3 &hit, float &distance);

};

void panel3dInit();

struct G8PickList {

  struct PickItem {
    F distance;
    vec3 hit;
    function<void (bool picked)> draw;
    function<void (vec3 const &hit)> onClick;
  };

  G8DrawList &dl;
  vector<PickItem> items;
  vec4 pickedBgCol{bgcolor(0x27, 0x70, 0xf9, 0xcc)};

  G8PickList(G8DrawList &_dl) : dl(_dl) {}

  void add(
    vec3 const &a, vec3 const &b, vec3 const &c, vec3 const &d,
    F layer,
    function<void (bool picked)> draw,
    function<void (vec3 const &hit)> onClick);

  void addText(
    vec3 const &curPos, vec3 const &xDir, vec3 const &yDir, vec3 const &norm,
    string_view text,
    vec4 const &color,
    ImFont *font,
    F xJust, F yJust,
    F layer,
    function<void (vec3 const &hit)> onClick);

  void finish();

};

