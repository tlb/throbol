#include "./geom.h"
#include "common/str_utils.h"

/*
  Note about Eigen:
  Woe betide you if you pass types like mat4 by value. The C++ calling convention doesn't seem to respect
  alignment requirements, so you'll get random alignment failures when it tries to access them with
  512-bit AVX instrutions, which require 16-byte alignment. (Stack alignment is only guaranteed to be 8 bytes
  on (at least) OSX/x86_64/Clang 10
  Use const & refs.
*/

mat4 lookAt(vec3 const &eye, vec3 const &center, vec3 const &up)
{
  vec3 f = (center - eye).normalized();
  vec3 s = f.cross(up).normalized();
  vec3 u = s.cross(f);

  return mat4{
    {s[0], s[1], s[2], -s.dot(eye)},
    {u[0], u[1], u[2], -u.dot(eye)},
    {-f[0], -f[1], -f[2], f.dot(eye)},
    {0, 0, 0, 1}};
}

mat4 perspective(F fovy, F aspect, F zNear, F zFar)
{
  F tanHalfFovy = tan(fovy / 2.0f);

  mat4 ret = mat4::Zero();
  ret(0, 0) = 1.0f / (aspect * tanHalfFovy);
  ret(1, 1) = 1.0f / tanHalfFovy;
  ret(2, 2) = -(zFar + zNear) / (zFar - zNear);
  ret(3, 2) = -1;
  ret(2, 3) = -(2.0f * zFar * zNear) / (zFar - zNear);
  return ret;
}

bool intersectRayTriangle(vec3 const &orig, vec3 const &dir,
  vec3 const &vert0, vec3 const &vert1, vec3 const &vert2,
  vec2 &baryPosition, F &distance)
{

  vec3 edge1 = vert1 - vert0;
  vec3 edge2 = vert2 - vert0;

  vec3 p = dir.cross(edge2);
  F det = edge1.dot(p);
  vec3 perpendicular(0, 0, 0);
  const F eps = std::numeric_limits<F>::epsilon();

  F baryX, baryY;

  if (det > eps) {
    vec3 dist = orig - vert0;
    baryX = dist.dot(p);
    if (baryX < 0 || baryX > det) return false;
    perpendicular = dist.cross(edge1);
    baryY = dir.dot(perpendicular);
    if (baryY < 0.0f || baryX + baryY > det) return false;
  }
  else if (det < -eps) {
    vec3 dist = orig - vert0;
    baryX = dist.dot(p);
    if (baryX > 0 || baryX < -det) return false;
    perpendicular = dist.cross(edge1);
    baryY = dir.dot(perpendicular);
    if (baryY > 0.0f || baryX + baryY < det) return false;
  }
  else {
    return false;
  }

  F inv_det = 1.0f / det;

  distance = edge2.dot(perpendicular) * inv_det;
  baryPosition = vec2(baryX * inv_det, baryY * inv_det);
  return true;
}


std::ostream & operator << (std::ostream &s, vec2 const &a)
{
  s.precision(3);
  for (Index rowi = 0; rowi < a.rows(); rowi++) {
    s << ((rowi == 0) ? "[" : ", ");
    s << a(rowi);
  }
  s << "]";
  return s;
}

std::ostream & operator << (std::ostream &s, vec3 const &a)
{
  s.precision(3);
  for (Index rowi = 0; rowi < a.rows(); rowi++) {
    s << ((rowi == 0) ? "[" : ", ");
    s << a(rowi);
  }
  s << "]";
  return s;
}

std::ostream & operator << (std::ostream &s, vec4 const &a)
{
  s.precision(3);
  for (Index rowi = 0; rowi < a.rows(); rowi++) {
    s << ((rowi == 0) ? "[" : ", ");
    s << a(rowi);
  }
  s << "]";
  return s;
}

std::ostream & operator << (std::ostream &s, Eigen::Map<Eigen::Array<F, 4, 1>> const &a)
{
  s.precision(3);
  for (Index rowi = 0; rowi < a.rows(); rowi++) {
    s << ((rowi == 0) ? "[" : ", ");
    s << a(rowi);
  }
  s << "]";
  return s;
}

std::ostream & operator << (std::ostream &s, Eigen::Map<Eigen::Array<complex<float>, 4, 1>> const &a)
{
  s.precision(3);
  for (Index rowi = 0; rowi < a.rows(); rowi++) {
    s << ((rowi == 0) ? "[" : ", ");
    s << a(rowi);
  }
  s << "]";
  return s;
}


std::ostream & operator << (std::ostream &s, Eigen::Map<Eigen::Array<F, 8, 1>> const &a)
{
  s.precision(3);
  for (Index rowi = 0; rowi < a.rows(); rowi++) {
    s << ((rowi == 0) ? "[" : ", ");
    s << a(rowi);
  }
  s << "]";
  return s;
}

std::ostream & operator << (std::ostream &s, Eigen::Map<Eigen::Array<complex<float>, 8, 1>> const &a)
{
  s.precision(3);
  for (Index rowi = 0; rowi < a.rows(); rowi++) {
    s << ((rowi == 0) ? "[" : ", ");
    s << a(rowi);
  }
  s << "]";
  return s;
}

std::ostream & operator << (std::ostream &s, mat2 const &a)
{
  s.precision(3);
  for (Index coli = 0; coli < a.cols(); coli++) {
    s << ((coli == 0) ? "[" : ", ");
    for (Index rowi = 0; rowi < a.rows(); rowi++) {
      s << ((rowi == 0) ? "[" : ", ");
      s << a(rowi, coli);
    }
    s << "]";
  }
  s << "]";
  return s;
}

std::ostream & operator << (std::ostream &s, mat4 const &a)
{
  s.precision(3);
  for (Index coli = 0; coli < a.cols(); coli++) {
    s << ((coli == 0) ? "[" : ", ");
    for (Index rowi = 0; rowi < a.rows(); rowi++) {
      s << ((rowi == 0) ? "[" : ", ");
      s << a(rowi, coli);
    }
    s << "]";
  }
  s << "]";
  return s;
}

std::ostream & operator << (std::ostream &s, MatrixXf const &a)
{
  s.precision(3);
  for (Index coli = 0; coli < a.cols(); coli++) {
    s << ((coli == 0) ? "[" : ", ");
    for (Index rowi = 0; rowi < a.rows(); rowi++) {
      s << ((rowi == 0) ? "[" : ", ");
      s << a(rowi, coli);
    }
    s << "]";
  }
  s << "]";
  return s;
}

std::ostream & operator << (std::ostream &s, VectorXf const &a)
{
  s.precision(3);
  for (Index rowi = 0; rowi < a.rows(); rowi++) {
    s << ((rowi == 0) ? "[" : ", ");
    s << a(rowi);
  }
  s << "]";
  return s;
}


std::ostream & operator << (std::ostream &s, MatrixXd const &a)
{
  s.precision(3);
  for (Index coli = 0; coli < a.cols(); coli++) {
    s << ((coli == 0) ? "[" : ", ");
    for (Index rowi = 0; rowi < a.rows(); rowi++) {
      s << ((rowi == 0) ? "[" : ", ");
      s << a(rowi, coli);
    }
    s << "]";
  }
  s << "]";
  return s;
}

std::ostream & operator << (std::ostream &s, VectorXd const &a)
{
  s.precision(3);
  for (Index rowi = 0; rowi < a.rows(); rowi++) {
    s << ((rowi == 0) ? "[" : ", ");
    s << a(rowi);
  }
  s << "]";
  return s;
}



std::ostream & operator << (std::ostream &s, mat const &a)
{
  s.precision(3);
  for (Index coli = 0; coli < a.cols(); coli++) {
    if (a.cols() > 1) s << ((coli == 0) ? "[" : ", ");
    for (Index rowi = 0; rowi < a.rows(); rowi++) {
      s << ((rowi == 0) ? "[" : ", ");
      s << a(rowi, coli);
    }
    s << "]";
  }
  if (a.cols()) s << "]";
  return s;
}


std::ostream & operator << (std::ostream &s, vec const &a)
{
  s.precision(3);
  for (Index rowi = 0; rowi < a.rows(); rowi++) {
    s << ((rowi == 0) ? "[" : ", ");
    s << a(rowi, 0);
  }
  s << "]";
  return s;
}


std::ostream & operator << (std::ostream &s, cvec const &a)
{
  s.precision(3);
  for (Index rowi = 0; rowi < a.rows(); rowi++) {
    s << ((rowi == 0) ? "[" : ", ");
    s << a(rowi, 0);
  }
  s << "]";
  return s;
}

std::ostream & operator << (std::ostream &s, complex<float> const &a)
{
  s << real(a) << "+" << imag(a) << "im";
  return s;
}

std::ostream & operator << (std::ostream &s, cmat const &a)
{
  s.precision(3);
  for (Index coli = 0; coli < a.cols(); coli++) {
    if (a.cols() > 1) s << ((coli == 0) ? "[" : ", ");
    for (Index rowi = 0; rowi < a.rows(); rowi++) {
      s << ((rowi == 0) ? "[" : ", ");
      s << a(rowi, coli);
    }
    s << "]";
  }
  if (a.cols()) s << "]";
  return s;
}
