#include "./g8.h"
#include "./shaders.h"
#ifdef USE_STL_SOLID
#include "../geom/solid_geometry.h"
#endif
#include "../geom/vbtext.h"
#include <Eigen/Geometry>
#if defined(__APPLE__)
#include "TargetConditionals.h"
#endif
#include "imgui.h"
#include "../src/main_window.h"

/*
  Reading:
     https://toji.dev/webgpu-best-practices/bind-groups
*/

#define VERBOSE 0

extern bool uiPollLive;

static_assert(std::is_nothrow_move_constructible_v<G8Vtx_pnl>);
static_assert(std::is_nothrow_move_constructible_v<G8Vtx_pcl>);
static_assert(std::is_nothrow_move_constructible_v<G8Vtx_pul>);
static_assert(std::is_nothrow_move_constructible_v<G8Vtx_puvl>);
static_assert(std::is_nothrow_move_constructible_v<G8Vtx_pucl>);
static_assert(std::is_nothrow_move_constructible_v<G8Vtx_pncl>);
static_assert(std::is_nothrow_move_constructible_v<G8Vtx_pnul>);
static_assert(std::is_nothrow_move_constructible_v<G8Vtx_pnucl>);


WGPUVertexAttribute G8Vtx_pnl::attribute_desc[3] = {
  { WGPUVertexFormat_Float32x3, (uint64_t)offsetof(G8Vtx_pnl, pos), 0 },
  { WGPUVertexFormat_Float32x3, (uint64_t)offsetof(G8Vtx_pnl, normal),  1 },
  { WGPUVertexFormat_Float32,   (uint64_t)offsetof(G8Vtx_pnl, layer),  2 },
};

WGPUVertexAttribute G8Vtx_pcl::attribute_desc[3] = {
  { WGPUVertexFormat_Float32x3, (uint64_t)offsetof(G8Vtx_pcl, pos), 0 },
  { WGPUVertexFormat_Float32x4, (uint64_t)offsetof(G8Vtx_pcl, color),  1 },
  { WGPUVertexFormat_Float32,   (uint64_t)offsetof(G8Vtx_pcl, layer),  2 },
};

WGPUVertexAttribute G8Vtx_pul::attribute_desc[3] = {
  { WGPUVertexFormat_Float32x3, (uint64_t)offsetof(G8Vtx_pul, pos), 0 },
  { WGPUVertexFormat_Float32x2, (uint64_t)offsetof(G8Vtx_pul, uv),  1 },
  { WGPUVertexFormat_Float32,   (uint64_t)offsetof(G8Vtx_pul, layer),  2 },
};

WGPUVertexAttribute G8Vtx_puvl::attribute_desc[4] = {
  { WGPUVertexFormat_Float32x3, (uint64_t)offsetof(G8Vtx_puvl, pos), 0 },
  { WGPUVertexFormat_Float32x2, (uint64_t)offsetof(G8Vtx_puvl, uv),  1 },
  { WGPUVertexFormat_Float32x4, (uint64_t)offsetof(G8Vtx_puvl, value),  2 },
  { WGPUVertexFormat_Float32,   (uint64_t)offsetof(G8Vtx_puvl, layer),  3 },
};

WGPUVertexAttribute G8Vtx_pucl::attribute_desc[4] = {
  { WGPUVertexFormat_Float32x3, (uint64_t)offsetof(G8Vtx_pucl, pos), 0 },
  { WGPUVertexFormat_Float32x2, (uint64_t)offsetof(G8Vtx_pucl, uv),  1 },
  { WGPUVertexFormat_Float32x4, (uint64_t)offsetof(G8Vtx_pucl, color),  2 },
  { WGPUVertexFormat_Float32,   (uint64_t)offsetof(G8Vtx_pucl, layer),  3 },
};

WGPUVertexAttribute G8Vtx_pncl::attribute_desc[4] = {
  { WGPUVertexFormat_Float32x3, (uint64_t)offsetof(G8Vtx_pncl, pos), 0 },
  { WGPUVertexFormat_Float32x3, (uint64_t)offsetof(G8Vtx_pncl, normal),  1 },
  { WGPUVertexFormat_Float32x4, (uint64_t)offsetof(G8Vtx_pncl, color),  2 },
  { WGPUVertexFormat_Float32,   (uint64_t)offsetof(G8Vtx_pncl, layer),  3 },
};

WGPUVertexAttribute G8Vtx_pnul::attribute_desc[4] = {
  { WGPUVertexFormat_Float32x3, (uint64_t)offsetof(G8Vtx_pnul, pos), 0 },
  { WGPUVertexFormat_Float32x3, (uint64_t)offsetof(G8Vtx_pnul, normal),  1 },
  { WGPUVertexFormat_Float32x2, (uint64_t)offsetof(G8Vtx_pnul, uv),  2 },
  { WGPUVertexFormat_Float32,   (uint64_t)offsetof(G8Vtx_pnul, layer),  3 },
};

WGPUVertexAttribute G8Vtx_pnucl::attribute_desc[5] = {
  { WGPUVertexFormat_Float32x3, (uint64_t)offsetof(G8Vtx_pnucl, pos), 0 },
  { WGPUVertexFormat_Float32x3, (uint64_t)offsetof(G8Vtx_pnucl, normal),  1 },
  { WGPUVertexFormat_Float32x2, (uint64_t)offsetof(G8Vtx_pnucl, uv),  2 },
  { WGPUVertexFormat_Float32x4, (uint64_t)offsetof(G8Vtx_pnucl, color),  3 },
  { WGPUVertexFormat_Float32,   (uint64_t)offsetof(G8Vtx_pnucl, layer),  4 },
};


static char const *std_decls = R"(
  struct G8Vtx_pnl {
    @location(0) pos: vec3<f32>,
    @location(1) normal: vec3<f32>,
    @location(2) layer: f32,
  };
  struct G8Vtx_pul {
    @location(0) pos: vec3<f32>,
    @location(1) uv: vec2<f32>,
    @location(2) layer: f32,
  };
  struct G8Vtx_puvl {
    @location(0) pos: vec3<f32>,
    @location(1) uv: vec2<f32>,
    @location(2) value: vec4<f32>,
    @location(3) layer: f32,
  };
  struct G8Vtx_pcl {
    @location(0) pos: vec3<f32>,
    @location(1) color: vec4<f32>,
    @location(2) layer: f32,
  };
  struct G8Vtx_pucl {
    @location(0) pos: vec3<f32>,
    @location(1) uv: vec2<f32>,
    @location(2) color: vec4<f32>,
    @location(3) layer: f32,
  };
  struct G8Vtx_pncl {
    @location(0) pos: vec3<f32>,
    @location(1) normal: vec3<f32>,
    @location(2) color: vec4<f32>,
    @location(3) layer: f32,
  };
  struct G8Vtx_pnul {
    @location(0) pos: vec3<f32>,
    @location(1) normal: vec3<f32>,
    @location(2) uv: vec2<f32>,
    @location(3) layer: f32,
  };
  struct G8Vtx_pnucl {
    @location(0) pos: vec3<f32>,
    @location(1) normal: vec3<f32>,
    @location(2) uv: vec2<f32>,
    @location(3) color: vec4<f32>,
    @location(4) layer: f32,
  };

  struct Frag_sc {
    @builtin(position) screenpos: vec4<f32>,
    @location(0) color: vec4<f32>,
  };

  struct Frag_su {
    @builtin(position) screenpos: vec4<f32>,
    @location(0) uv: vec2<f32>,
  };

  struct Frag_suc {
    @builtin(position) screenpos: vec4<f32>,
    @location(0) uv: vec2<f32>,
    @location(1) color: vec4<f32>,
  };

  struct Frag_suv {
    @builtin(position) screenpos: vec4<f32>,
    @location(0) uv: vec2<f32>,
    @location(1) value: vec4<f32>,
  };

  struct Frag_spnlc {
    @builtin(position) screenpos: vec4<f32>,
    @location(0) pos: vec3<f32>,
    @location(1) normal: vec3<f32>,
    @location(2) lightpos: vec3<f32>,
    @location(3) color: vec4<f32>,
  };

  struct Frag_spnlcr {
    @builtin(position) screenpos: vec4<f32>,
    @location(0) pos: vec3<f32>,
    @location(1) normal: vec3<f32>,
    @location(2) lightpos: vec3<f32>,
    @location(3) color: vec4<f32>,
    @location(4) rawpos: vec3<f32>,
  };

  struct Frag_spnlcu {
    @builtin(position) screenpos: vec4<f32>,
    @location(0) pos: vec3<f32>,
    @location(1) normal: vec3<f32>,
    @location(2) lightpos: vec3<f32>,
    @location(3) color: vec4<f32>,
    @location(4) uv: vec2<f32>,
  };


  struct ProjViewUniforms {
    proj: mat4x4<f32>,
    view: mat4x4<f32>,
  };

  struct ModelUniforms {
    model: mat4x4<f32>,
    inflation: f32,
    _pad0: f32,
    _pad1: f32,
    _pad2: f32,
  };

  const TVIT_line = 0u;
  const TVIT_beh = 1u;
  const TVIT_check = 2u;

  struct TraceVisInfo {
    traceType: u32, // a TVIT_xxx constant
    colorIndex: u32,
    dataIndex: u32,
    dataCount: u32,
    dataStride: u32,
    alpha: f32,
    thick: f32,
    _pad0: f32,
  };

  struct TraceUniforms {
    scalePos: f32,
    scaleNeg: f32,
    range: f32,
    nSamples: u32,
    isCheckFailure: u32,
    isRewardCell: u32,
    _pad1: u32,
    _pad2: u32,
  };

  const MAX_GRAPH_TRACES = 40u;

  struct SkinUniforms {
    dialBgCol: vec4<f32>,
    dialPosCol: vec4<f32>,
    dialNegCol: vec4<f32>,
    dialRangeCol: vec4<f32>,
    traceBgColBase: vec4<f32>,
    traceBgColFailure: vec4<f32>,
    traceBgColReward: vec4<f32>,

    goodGraphColors: array<vec4<f32>, MAX_GRAPH_TRACES>,
  };

  struct DialUniforms {
    value: f32,
    rangeLo: f32,
    rangeHi: f32,
  };

)";


static string glslAvailVersion;
static string glslUsingVersion;
static string glVersion;

vector<G8ShaderGeneric *> *G8ShaderGeneric::allShaders;


ostream & operator <<(ostream &s, G8VertexType vt)
{
  switch (vt) {
    case G8VertexType_pnl: s << "pnl"; break;
    case G8VertexType_pul: s << "pul"; break;
    case G8VertexType_puvl: s << "puvl"; break;
    case G8VertexType_pcl: s << "pcl"; break;
    case G8VertexType_pncl: s << "pncl"; break;
    case G8VertexType_pucl: s << "pucl"; break;
    case G8VertexType_pnul: s << "pnul"; break;
    case G8VertexType_pnucl: s << "pnucl"; break;
    // ADD vertex type (search for this elsewhere when adding)
    default: s << int(vt);
  }
  return s;
}

G8ResourcesPerShaderWindow::~G8ResourcesPerShaderWindow()
{
  if (pipeline) {
    wgpuRenderPipelineRelease(pipeline);
    pipeline = nullptr;
  }
  if (pipelineLayout) {
    wgpuPipelineLayoutRelease(pipelineLayout);
    pipelineLayout = nullptr;
  }
  if (bindGroup0) {
    wgpuBindGroupRelease(bindGroup0);
    bindGroup0 = nullptr;
  }
  for (auto it : bgLayouts) {
    wgpuBindGroupLayoutRelease(it);
  }
  bgLayouts.clear();
  if (fontSampler) {
    wgpuSamplerRelease(fontSampler);
    fontSampler = nullptr;
  }
  if (imgSampler) {
    wgpuSamplerRelease(imgSampler);
    imgSampler = nullptr;
  }
  if (frgHandle.module) {
    wgpuShaderModuleRelease(frgHandle.module);
    frgHandle.module = nullptr;
  }
  if (vtxHandle.module) {
    wgpuShaderModuleRelease(vtxHandle.module);
    vtxHandle.module = nullptr;
  }
}

G8ShaderGeneric::G8ShaderGeneric(string_view _name, initializer_list<string_view> args)
  : name(_name)
{
  parseArgs(args);
}

void G8ShaderGeneric::parseArgs(initializer_list<string_view> args)
{
  for (auto argit = args.begin(); argit != args.end(); ) {

    auto argError = [&](string_view msg) {
      dielog("while parsing shader " << name << ": " << msg);
    };

    auto getStr = [&]() {
      if (argit == args.end()) argError("Missing shader arg");
      auto ret = *argit;
      argit++;
      return ret;
    };

    auto isArg = [&](string_view const &name) {
      if (argit == args.end()) argError("Missing shader arg");
      if (*argit == name) {
        argit++;
        return true;
      }
      return false;
    };

    auto getInt = [&]() {
      if (argit == args.end()) argError("Missing shader arg");
      istringstream iss((string(*argit)));
      int ret = 0;
      iss >> ret;
      argit++;
      return ret;
    };

    auto getBool = [&]() {
      return getInt() > 0;
    };

    if (isArg("doDepthTest:")) {
      doDepthTest = getBool();
    }
    else if (isArg("doDepthWrite:")) {
      doDepthWrite = getBool();
    }
    else if (isArg("g0Binding.fontTex:")) {
      g0Binding.fontTex = getInt();
    }
    else if (isArg("g0Binding.traceUniforms:")) {
      g0Binding.traceUniforms = getInt();
    }
    else if (isArg("g0Binding.dialUniforms:")) {
      g0Binding.dialUniforms = getInt();
    }
    else if (isArg("g0Binding.skinUniforms:")) {
      g0Binding.skinUniforms = getInt();
    }
    else if (isArg("g0Binding.imgSampler:")) {
      g0Binding.imgSampler = getInt();
    }
    else if (isArg("g0Binding.fontSampler:")) {
      g0Binding.fontSampler = getInt();
    }
    else if (isArg("g0Binding.modelUniforms:")) {
      g0Binding.modelUniforms = getInt();
    }
    else if (isArg("g1Binding.imgTex:")) {
      g1Binding.imgTex = getInt();
      assert(g1Binding.imgTex < int(MAX_G1_BINDINGS));
    }
    else if (isArg("g1Binding.traceData:")) {
      g1Binding.traceData = getInt();
      assert(g1Binding.traceData < int(MAX_G1_BINDINGS));
    }
    else if (isArg("g1Binding.traceVisInfo:")) {
      g1Binding.traceVisInfo = getInt();
      assert(g1Binding.traceVisInfo < int(MAX_G1_BINDINGS));
    }
    else if (isArg("cmdBufIndex:")) {
      cmdBufIndex = getInt();
    }
    else if (isArg("vtx:")) {
      vtxMain = getStr();
    }
    else if (isArg("frg:")) {
      frgMain = getStr();
    }
    else {
      dielog(name << ": no option " << *argit);
    }
  }


  vtxMain =
    string("// vertex shader ") + name + "\n" +
    string(std_decls) +
    vtxMain;
  frgMain = 
    string("// fragment shader ") + name + "\n" +
    string(std_decls) +
    frgMain;

  if (!allShaders) allShaders = new vector<G8ShaderGeneric *>();
  shaderIndex = allShaders->size();
  allShaders->push_back(this);
}

G8ShaderGeneric::~G8ShaderGeneric()
{
  for (auto &it : *allShaders) {
    if (it == this) it = nullptr;
  }
}

G8Cache<WGPUTexture> globalTexCache;

void G8ShaderGeneric::compile1(WGPUProgrammableStageDescriptor &out, WGPUDevice wgpu_device, string const &code)
{
  WGPUShaderModuleWGSLDescriptor shaderCodeDesc = {
    .chain = {
      .next = nullptr,
      .sType = WGPUSType_ShaderModuleWGSLDescriptor
    },
#if defined(WEBGPU_BACKEND_WGPU)
    .code = code.c_str()
#else
    .source = code.c_str()
#endif
  };

  WGPUShaderModuleDescriptor shaderDesc = {
    .nextInChain = &shaderCodeDesc.chain
  };

  out.module = wgpuDeviceCreateShaderModule(wgpu_device, &shaderDesc);
  if (!out.module) {
    L() << "Compilation failed for " << name << "\n" << code;
    throw runtime_error("wgpuDeviceCreateShaderModule failed");
  }
  out.entryPoint = "main";

}

void G8ShaderGeneric::compile(G8ShaderSetup &setup, WGPUDevice wgpu_device, WGPUQueue wgpu_queue)
{
  setup.perSW->g0DynamicOffsets.count = 0;
  vector<WGPUBindGroupLayoutEntry> g0LayoutEnts;
  vector<WGPUBindGroupLayoutEntry> g1LayoutEnts;
  if (1) {
    setup.perSW->g0DynamicOffsets.projviewUniforms = setup.perSW->g0DynamicOffsets.count++;
    g0LayoutEnts.push_back(WGPUBindGroupLayoutEntry{
      .binding = 0,
      .visibility = WGPUShaderStage_Vertex | WGPUShaderStage_Fragment,
      .buffer = {
        .type = WGPUBufferBindingType_Uniform,
        .hasDynamicOffset = true
      }});
  }
  if (g0Binding.modelUniforms != -1) {
    setup.perSW->g0DynamicOffsets.modelUniforms = setup.perSW->g0DynamicOffsets.count ++;
    g0LayoutEnts.push_back(WGPUBindGroupLayoutEntry{
      .binding = uint32_t(g0Binding.modelUniforms),
      .visibility = WGPUShaderStage_Vertex | WGPUShaderStage_Fragment,
      .buffer = {
        .type = WGPUBufferBindingType_Uniform,
        .hasDynamicOffset = true
      }});
  }
  if (g0Binding.skinUniforms != -1) {
    g0LayoutEnts.push_back(WGPUBindGroupLayoutEntry{
      .binding = uint32_t(g0Binding.skinUniforms),
      .visibility = WGPUShaderStage_Vertex | WGPUShaderStage_Fragment,
      .buffer = {
        .type = WGPUBufferBindingType_Uniform,
        .hasDynamicOffset = false
      }});
  }
  if (g0Binding.traceUniforms != -1) {
    setup.perSW->g0DynamicOffsets.traceUniforms = setup.perSW->g0DynamicOffsets.count ++;
    g0LayoutEnts.push_back(WGPUBindGroupLayoutEntry{
      .binding = uint32_t(g0Binding.traceUniforms),
      .visibility = WGPUShaderStage_Vertex | WGPUShaderStage_Fragment,
      .buffer = {
        .type = WGPUBufferBindingType_Uniform,
        .hasDynamicOffset = true
      }});
  }
  if (g0Binding.dialUniforms != -1) {
    setup.perSW->g0DynamicOffsets.dialUniforms = setup.perSW->g0DynamicOffsets.count ++;
    g0LayoutEnts.push_back(WGPUBindGroupLayoutEntry{
      .binding = uint32_t(g0Binding.dialUniforms),
      .visibility = WGPUShaderStage_Vertex | WGPUShaderStage_Fragment,
      .buffer = {
        .type = WGPUBufferBindingType_Uniform,
        .hasDynamicOffset = true
      }});
  }
  if (g0Binding.imgSampler != -1) {
    g0LayoutEnts.push_back(WGPUBindGroupLayoutEntry{
      .binding = uint32_t(g0Binding.imgSampler),
      .visibility = WGPUShaderStage_Fragment,
      .sampler = {
        .type = WGPUSamplerBindingType_Filtering
      }});
  }
  if (g0Binding.fontSampler != -1) {
    g0LayoutEnts.push_back(WGPUBindGroupLayoutEntry{
      .binding = uint32_t(g0Binding.fontSampler),
      .visibility = WGPUShaderStage_Fragment,
      .sampler = {
        .type = WGPUSamplerBindingType_Filtering
      }});
  }
  if (g0Binding.fontTex != -1) {
    g0LayoutEnts.push_back(WGPUBindGroupLayoutEntry{
      .binding = uint32_t(g0Binding.fontTex),
      .visibility = WGPUShaderStage_Fragment,
      .texture = {
        .sampleType = WGPUTextureSampleType_Float,
        .viewDimension = WGPUTextureViewDimension_2D
      }});
  }

  if (g1Binding.imgTex != -1) {
    g1LayoutEnts.push_back(WGPUBindGroupLayoutEntry{
      .binding = uint32_t(g1Binding.imgTex),
      .visibility = WGPUShaderStage_Fragment,
      .texture = {
        .sampleType = WGPUTextureSampleType_Float,
        .viewDimension = WGPUTextureViewDimension_2D
      }});
  }
  if (g1Binding.traceData != -1) {
    g1LayoutEnts.push_back(WGPUBindGroupLayoutEntry{
      .binding = uint32_t(g1Binding.traceData),
      .visibility = WGPUShaderStage_Fragment,
      .buffer = {
        .type = WGPUBufferBindingType_ReadOnlyStorage,
      }});
  }
  if (g1Binding.traceVisInfo != -1) {
    g1LayoutEnts.push_back(WGPUBindGroupLayoutEntry{
      .binding = uint32_t(g1Binding.traceVisInfo),
      .visibility = WGPUShaderStage_Fragment,
      .buffer = {
        .type = WGPUBufferBindingType_ReadOnlyStorage,
      }});
  }

  assert(setup.perSW->g0DynamicOffsets.count <= int(MAX_G0_DYNAMIC_UNIFORMS));

  if (!g0LayoutEnts.empty()) {
    WGPUBindGroupLayoutDescriptor bg_layout_desc = {
      .entryCount = uint32_t(g0LayoutEnts.size()),
      .entries = g0LayoutEnts.data()
    };

    setup.perSW->bgLayouts.push_back(wgpuDeviceCreateBindGroupLayout(wgpu_device, &bg_layout_desc));
  }
  if (!g1LayoutEnts.empty()) {
    WGPUBindGroupLayoutDescriptor bg_layout_desc = {
      .entryCount = uint32_t(g1LayoutEnts.size()),
      .entries = g1LayoutEnts.data()
    };

    setup.perSW->bgLayouts.push_back(wgpuDeviceCreateBindGroupLayout(wgpu_device, &bg_layout_desc));
  }

  WGPUPipelineLayoutDescriptor pipeline_layout_desc = {
    .bindGroupLayoutCount = uint32_t(setup.perSW->bgLayouts.size()),
    .bindGroupLayouts = setup.perSW->bgLayouts.data()
  };
  setup.perSW->pipelineLayout = wgpuDeviceCreatePipelineLayout(wgpu_device, &pipeline_layout_desc);

  WGPUBlendState blend_state = {
    .color = {
      .operation = WGPUBlendOperation_Add,
      .srcFactor = WGPUBlendFactor_SrcAlpha,
      .dstFactor = WGPUBlendFactor_OneMinusSrcAlpha
    },
    .alpha = {
      .operation = WGPUBlendOperation_Add,
      .srcFactor = WGPUBlendFactor_One,
      .dstFactor = WGPUBlendFactor_OneMinusSrcAlpha
    },
  };

  WGPUColorTargetState color_state = {
    .format = WGPUTextureFormat_BGRA8Unorm,
    .blend = &blend_state,
    .writeMask = WGPUColorWriteMask_All
  };

  if (!vtxMain.empty()) {
    compile1(setup.perSW->vtxHandle, wgpu_device, vtxMain.c_str());
  }

  WGPUFragmentState fragment_state = {};
  if (!frgMain.empty()) {
    compile1(setup.perSW->frgHandle, wgpu_device, frgMain.c_str());
    fragment_state = WGPUFragmentState {
      .module = setup.perSW->frgHandle.module,
      .entryPoint = setup.perSW->frgHandle.entryPoint,
      .targetCount = 1,
      .targets = &color_state,
    };
  }

  vector<WGPUVertexBufferLayout> buffer_layouts;
  buffer_layouts.push_back(getVertexBufferLayout());

  WGPUDepthStencilState depth_stencil_state = {
    .format = WGPUTextureFormat_Depth32Float,
    .depthWriteEnabled = doDepthWrite,
    .depthCompare = doDepthTest ? WGPUCompareFunction_Less : WGPUCompareFunction_Always,
    .stencilFront = {
      .compare = WGPUCompareFunction_Always,
      .failOp = WGPUStencilOperation_Keep,
      .depthFailOp = WGPUStencilOperation_Keep,
      .passOp = WGPUStencilOperation_Keep
    },
    .stencilBack = {
      .compare = WGPUCompareFunction_Always,
      .failOp = WGPUStencilOperation_Keep,
      .depthFailOp = WGPUStencilOperation_Keep,
      .passOp = WGPUStencilOperation_Keep
    },
    .stencilReadMask = 0,
    .stencilWriteMask = 0,
    .depthBias = 0,
    .depthBiasSlopeScale = 0.0f,
    .depthBiasClamp = 0.0f
  };

  WGPURenderPipelineDescriptor render_pipeline_desc = {
    .label = name.c_str(),
    .layout = setup.perSW->pipelineLayout,
    .vertex = {
      .module = setup.perSW->vtxHandle.module,
      .entryPoint = setup.perSW->vtxHandle.entryPoint,
      .bufferCount = uint32_t(buffer_layouts.size()),
      .buffers = buffer_layouts.data()
    },
    .primitive = {
      .topology = WGPUPrimitiveTopology_TriangleList,
      .stripIndexFormat = WGPUIndexFormat_Undefined,
      .frontFace = WGPUFrontFace_CW,
      .cullMode = WGPUCullMode_None
    },
    .depthStencil = &depth_stencil_state,
    .multisample = {
      .count = 1,
      .mask = UINT_MAX,
      .alphaToCoverageEnabled = false
    },
    .fragment = &fragment_state,
  };
  setup.perSW->pipeline = wgpuDeviceCreateRenderPipeline(wgpu_device, &render_pipeline_desc);

  // A fixed size for the uniforms buffer. I can't figure out how to allocate a new buffer
  // when we exceed this, since the buffer is bound during the pipeline setup.
  const size_t uniformsSize = 1024;
  setup.perW->uniformsGpu.emplace(wgpu_device, "uniforms", WGPUBufferUsage_CopyDst | WGPUBufferUsage_Uniform, uniformsSize);

  WGPUSamplerDescriptor imgSampler_desc = {
    .addressModeU = WGPUAddressMode_Repeat,
    .addressModeV = WGPUAddressMode_Repeat,
    .addressModeW = WGPUAddressMode_Repeat,
    .magFilter = WGPUFilterMode_Linear,
    .minFilter = WGPUFilterMode_Linear,
#if defined(WEBGPU_BACKEND_WGPU)
    .mipmapFilter = WGPUMipmapFilterMode_Linear,
#else
    .mipmapFilter = WGPUFilterMode_Linear,
#endif
    .maxAnisotropy = 1,
  };
  setup.perSW->imgSampler = wgpuDeviceCreateSampler(wgpu_device, &imgSampler_desc);


  WGPUSamplerDescriptor fontSampler_desc = {
    .addressModeU = WGPUAddressMode_Repeat,
    .addressModeV = WGPUAddressMode_Repeat,
    .addressModeW = WGPUAddressMode_Repeat,
    .magFilter = WGPUFilterMode_Linear,
    .minFilter = WGPUFilterMode_Linear,
#if defined(WEBGPU_BACKEND_WGPU)
    .mipmapFilter = WGPUMipmapFilterMode_Linear,
#else
    .mipmapFilter = WGPUFilterMode_Linear,
#endif
    .maxAnisotropy = 1,
  };
  setup.perSW->fontSampler = wgpuDeviceCreateSampler(wgpu_device, &fontSampler_desc);

}

WGPUBindGroup G8ShaderGeneric::mkBindGroup0(
  G8ShaderSetup &setup, MainWindow &win)
{
  // Create resource bind group
  vector<WGPUBindGroupEntry> bges;
  if (setup.perW->uniformsGpu) {
    bges.push_back(WGPUBindGroupEntry{
      .binding = 0,
      .buffer = setup.perW->uniformsGpu->wgpu_buffer,
      .size = sizeof(ProjViewUniforms)
    });
  }
  if (g0Binding.modelUniforms != -1 && setup.perW->uniformsGpu) {
    bges.emplace_back(WGPUBindGroupEntry{
      .binding = uint32_t(g0Binding.modelUniforms),
      .buffer = setup.perW->uniformsGpu->wgpu_buffer,
      .size = sizeof(ModelUniforms)
    });
  }
  if (g0Binding.traceUniforms != -1 && setup.perW->uniformsGpu) {
    bges.emplace_back(WGPUBindGroupEntry{
      .binding = uint32_t(g0Binding.traceUniforms),
      .buffer = setup.perW->uniformsGpu->wgpu_buffer,
      .size = sizeof(TraceUniforms)
    });
  }
  if (g0Binding.dialUniforms != -1 && setup.perW->uniformsGpu) {
    bges.emplace_back(WGPUBindGroupEntry{
      .binding = uint32_t(g0Binding.dialUniforms),
      .buffer = setup.perW->uniformsGpu->wgpu_buffer,
      .size = sizeof(DialUniforms)
    });
  }
  if (g0Binding.skinUniforms != -1 && setup.perW->skinUniformsGpu) {
    bges.emplace_back(WGPUBindGroupEntry{
      .binding = uint32_t(g0Binding.skinUniforms),
      .buffer = setup.perW->skinUniformsGpu->wgpu_buffer,
      .size = sizeof(SkinUniforms)
    });
  }

  if (g0Binding.imgSampler != -1 && setup.perSW->imgSampler) {
    bges.emplace_back(WGPUBindGroupEntry{
      .binding = uint32_t(g0Binding.imgSampler),
      .sampler = setup.perSW->imgSampler
    });
  }
  if (g0Binding.fontSampler != -1 && setup.perSW->fontSampler) {
    bges.emplace_back(WGPUBindGroupEntry{
      .binding = uint32_t(g0Binding.fontSampler),
      .sampler = setup.perSW->fontSampler
    });
  }

  if (g0Binding.fontTex != -1) {
    auto &wgpu_fontTex = globalTexCache[ObjectKey(win.wgpu_device, "font")];

    if (!wgpu_fontTex) {
      ImGuiIO& io = ImGui::GetIO();
      unsigned char *pixels = nullptr;
      int width = -1, height = -1, size_pp = -1;
      io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height, &size_pp);

      WGPUTextureDescriptor tex_desc = {
        .usage = WGPUTextureUsage_CopyDst | WGPUTextureUsage_TextureBinding,
        .dimension = WGPUTextureDimension_2D,
        .size = {
          .width = uint32_t(width),
          .height = uint32_t(height),
          .depthOrArrayLayers = 1
        },
        .format = WGPUTextureFormat_RGBA8Unorm,
        .mipLevelCount = 1,
        .sampleCount = 1,
      };
      wgpu_fontTex = wgpuDeviceCreateTexture(win.wgpu_device, &tex_desc);

      WGPUImageCopyTexture dst_view = {
        .texture = *wgpu_fontTex,
        .mipLevel = 0,
        .origin = { .x = 0, .y = 0, .z = 0 },
        .aspect = WGPUTextureAspect_All,
      };
      WGPUTextureDataLayout layout = {
        .offset = 0,
        .bytesPerRow = uint32_t(width * size_pp),
        .rowsPerImage = uint32_t(height)
      };
      WGPUExtent3D size = { 
        .width = uint32_t(width),
        .height = uint32_t(height),
        .depthOrArrayLayers = 1
      };
      wgpuQueueWriteTexture(win.wgpu_queue, &dst_view, pixels, (uint32_t)(width * size_pp * height), &layout, &size);
    }

    WGPUTextureViewDescriptor tex_view_desc = {
      .format = WGPUTextureFormat_RGBA8Unorm,
      .dimension = WGPUTextureViewDimension_2D,
      .baseMipLevel = 0, .mipLevelCount = 1,
      .baseArrayLayer = 0, .arrayLayerCount = 1,
      .aspect = WGPUTextureAspect_All
    };
    auto tv = wgpuTextureCreateView(*wgpu_fontTex, &tex_view_desc);
    
    bges.emplace_back(WGPUBindGroupEntry{
      .binding = uint32_t(g0Binding.fontTex),
      .textureView = tv,
    });
  }

  WGPUBindGroupDescriptor common_bg_descriptor = {
    .layout = setup.perSW->bgLayouts[0],
    .entryCount = uint32_t(bges.size()),
    .entries = bges.data(),
  };

  return wgpuDeviceCreateBindGroup(win.wgpu_device, &common_bg_descriptor);
}

WGPUBindGroup G8ShaderGeneric::mkBindGroup1(
  G8ShaderSetup &setup, MainWindow &win,
  std::array<uint32_t, MAX_G1_BINDINGS> &g1Data)
{
  vector<WGPUBindGroupEntry> bges;
  bges.reserve(4);
  if (g1Binding.imgTex != -1) {
    auto texi = g1Data.at(g1Binding.imgTex);
    auto tb = setup.perSW->textures.at(texi);

    WGPUTextureViewDescriptor tex_view_desc = {
      .format = WGPUTextureFormat_BGRA8Unorm,
      .dimension = WGPUTextureViewDimension_2D,
      .baseMipLevel = 0, .mipLevelCount = 1,
      .baseArrayLayer = 0, .arrayLayerCount = 1,
      .aspect = WGPUTextureAspect_All
    };
    auto tv = wgpuTextureCreateView(tb, &tex_view_desc);
    setup.perSW->ephemeralTextureViews.push_back(tv);

    bges.emplace_back(WGPUBindGroupEntry{
      .binding = uint32_t(g1Binding.imgTex),
      .textureView = tv
    });
  }

  if (g1Binding.traceData != -1) {
    auto tb = setup.perSW->buffers.at(g1Data.at(g1Binding.traceData));
    bges.emplace_back(WGPUBindGroupEntry{
      .binding = uint32_t(g1Binding.traceData),
      .buffer = tb,
      .size = uint32_t(wgpuBufferGetSize(tb)),
    });
  }
  if (g1Binding.traceVisInfo != -1) {
    auto tb = setup.perSW->buffers.at(g1Data.at(g1Binding.traceVisInfo));
    bges.emplace_back(WGPUBindGroupEntry{
      .binding = uint32_t(g1Binding.traceVisInfo),
      .buffer = tb,
      .size = uint32_t(wgpuBufferGetSize(tb)),
    });
  }

  WGPUBindGroupDescriptor common_bg_descriptor = {
    .layout = setup.perSW->bgLayouts[1],
    .entryCount = uint32_t(bges.size()),
    .entries = bges.data(),
  };

  return wgpuDeviceCreateBindGroup(win.wgpu_device, &common_bg_descriptor);

}


void panel3dInit()
{
}

G8DrawList::G8DrawList(MainWindow &_win)
  : win(_win),
    setups(G8ShaderGeneric::allShaders->size()),
    homeEyepos(-0.01, +2.0, 0.0),
    homeLookat(0, 0, -0.5),
    homeUp(0, 0, 1),
    homeFov(40.0 * M_PI/180.0),
    eyepos(homeEyepos),
    lookat(homeLookat),
    up(homeUp),
    fov(homeFov),
    dt(1.0/60.0),
    nearZ(5.0),
    farZ(500.0),
    pickRayOrigin(0, 0, 0),
    pickRayDir(0, 0, 0),
    saveDir(0, 0, 0)
{
  win.g8ResourcesPerShaderWindow.resize(max(win.g8ResourcesPerShaderWindow.size(), G8ShaderGeneric::allShaders->size()));

  for (auto shader : *G8ShaderGeneric::allShaders) {
    if (!shader) continue;
    auto &setup = setups[shader->shaderIndex];
    setup.shader = shader;
    setup.perSW = &win.g8ResourcesPerShaderWindow[shader->shaderIndex];
    setup.perW = &win.g8ResourcesPerWindow;
    setup.vt = shader->getVertexType();

    if (!setup.perSW->setupDone) {
      setup.perSW->setupDone = true;
      shader->compile(setup, win.wgpu_device, win.wgpu_queue);
    }

    cmdBufs.resize(max(cmdBufs.size(), size_t(shader->cmdBufIndex + 1)));
  }
  calcProjView = [](G8DrawList &dl) {
    float aspect = float(dl.vpR - dl.vpL) / float(dl.vpB - dl.vpT);
    dl.proj = perspective(dl.fov, aspect, dl.nearZ, dl.farZ);
    dl.view = lookAt(dl.eyepos, dl.lookat, dl.up);
    if (0) L() << "fov=" << dl.fov << " aspect=" << aspect << " proj=" << dl.proj << " view=" << dl.view;
  };

  camHome();
}

G8DrawList::~G8DrawList()
{
}

G8ShaderSetup &G8DrawList::getSetup(G8ShaderGeneric const *shader)
{
  assert(shader);
  auto &setup = setups.at(shader->shaderIndex);
  if (!setup.uniformsInited) {
    setup.uniformsInited = true;

    std::fill(setup.g0Data.begin(), setup.g0Data.end(), uint32_t(-1));
    std::fill(setup.g1Data.begin(), setup.g1Data.end(), uint32_t(-1));

    setup.g0Data[setup.perSW->g0DynamicOffsets.projviewUniforms] = setup.perW->uniformsBuf.addDynamic(
      ProjViewUniforms{ proj, view });

    if (setup.shader->g0Binding.modelUniforms != -1) {
      setup.g0Data[setup.perSW->g0DynamicOffsets.modelUniforms] = setup.perW->uniformsBuf.addDynamic(ModelUniforms(
        mat4::Identity(),
        0.0f
      ));
    }
  }

  return setup;
}

void G8DrawList::camHome()
{
  if (lookat == homeLookat && eyepos == homeEyepos && fov == homeFov && up == homeUp) return;
  lookat = homeLookat;
  eyepos = homeEyepos;
  fov = homeFov;
  up = homeUp;
  uiPollLive = true;
}

void G8DrawList::setHoverHome()
{
  homeLookat = vec3(0, 0, 0);
  homeEyepos = vec3(0, -15, 15);
  homeFov = 40 * M_PI/180.0;
  homeUp = vec3(0, 0, 1);
  camHome();
}

void G8DrawList::camIso()
{
  vec3 newLookat = vec3(0, 0, 0.0);
  vec3 newEyepos = vec3(0.0, +2.0, 0.0);
  float newFov = 41.0 * M_PI/180.0;
  vec3 newUp = vec3(0, 0, 1);
  if (lookat == newLookat && eyepos == newEyepos && fov == newFov && up == newUp) return;
  lookat = newLookat;
  eyepos = newEyepos;
  fov = newFov;
  up = newUp;
  uiPollLive = true;
}

void G8DrawList::camCheckSanity()
{
  if (isnan(lookat.x()) || isnan(lookat.y()) || isnan(lookat.z()) || 
      isnan(eyepos.x()) || isnan(eyepos.y()) || isnan(eyepos.z()) ||
      isnan(up.x()) || isnan(up.y()) || isnan(up.z()) || 
      isnan(fov)) {
    L() << "NaNs in G8DrawList lookat=" << lookat <<
      " eyepos=" << eyepos <<
      " up=" << up <<
      " fov=" << fov;
  }
}

void G8DrawList::setInterval()
{
  double curTime = realtime();
  if (curTime - lastDrawTime > 0.1 && dt < 0.1f) {
    dt = 1.0f/60.0f;
  }
  else {
    dt = float(clamp(curTime - lastDrawTime, 1.0/60.0, 0.1));
  }
  lastDrawTime = curTime;
}

void G8DrawList::clear()
{
  for (auto &cmdBuf : cmdBufs) {
    cmdBuf.clear();
  }
  for (auto &setup : setups) {
    setup.uniformsInited = false;
  }
}



void G8DrawList::badShader(G8ShaderGeneric const *shader, char const *where, initializer_list<G8VertexType> vts)
{
  auto l = L();
  l << where << ": bad shader " << shader->name <<
    " expecting vertex type " << shader->getVertexType() <<
    " but this draw routine only supports types [";
  for (auto it : vts) {
     l << " " << it;
  }
  l << "]";
}


void G8DrawList::addCmd(G8ShaderSetup &setup, uint32_t elemIndex, uint32_t elemCount)
{
  int shaderIndex = &setup - &setups[0];
  auto &cmdBuf = cmdBufs[setup.shader->cmdBufIndex];
  // Check the last 3 entries to see if we can coalesce.
  // This can put drawing slightly out of order!

#if !defined(_NDEBUG)
  if (setup.shader->g1Binding.imgTex != -1) {
    assert(setup.g1Data.at(setup.shader->g1Binding.imgTex) < setup.perSW->textures.size());
  }
#endif

  for (size_t back = 1; back < cmdBuf.size() && back <= 1; back++) {
    auto &last = cmdBuf[cmdBuf.size() - back];
    if (last.shaderIndex == shaderIndex &&
        last.viBufIndex == setup.viBufIndex &&
        last.g0Data == setup.g0Data &&
        last.g1Data == setup.g1Data &&
        last.elemIndex + last.elemCount == elemIndex) {
      // This is the common case
      last.elemCount += elemCount;
      return;
    }
  }

  if (1) {
    for (int i = 0; i < setup.perSW->g0DynamicOffsets.count; i++) {
      assert(setup.g0Data[i] != uint32_t(-1));
    }
  }

  cmdBuf.emplace_back(shaderIndex, setup.viBufIndex, elemIndex, elemCount, setup.g0Data, setup.g1Data);
}

void G8DrawList::addCmd(G8ShaderSetup &setup, G8GeomCacheEnt ent)
{
  int shaderIndex = &setup - &setups[0];
  auto &cmdBuf = cmdBufs[setup.shader->cmdBufIndex];
  cmdBuf.emplace_back(shaderIndex, ent.viBufIndex, ent.elemIndex, ent.elemCount, setup.g0Data, setup.g1Data);
}

void G8DrawList::addCmd(G8IdxBlaster &blaster)
{
  addCmd(blaster.setup, blaster.getStartIndex(), blaster.getElemCount());
}

PutterBacker<uint32_t> G8DrawList::withModelUniforms(ModelUniforms const &modelUniforms, G8ShaderGeneric const *shader)
{
  auto &setup = getSetup(shader);
  if (shader->g0Binding.modelUniforms != -1) {
    return PutterBacker<uint32_t>(&setup.g0Data[setup.perSW->g0DynamicOffsets.modelUniforms], setup.perW->uniformsBuf.addDynamic(modelUniforms));
  }
  return PutterBacker<uint32_t>();
}

PutterBacker<uint32_t> G8DrawList::withTraceUniforms(TraceUniforms const &traceUniforms, G8ShaderGeneric const *shader)
{
  auto &setup = getSetup(shader);
  if (shader->g0Binding.traceUniforms != -1) {
    return PutterBacker<uint32_t>(&setup.g0Data[setup.perSW->g0DynamicOffsets.traceUniforms], setup.perW->uniformsBuf.addDynamic(traceUniforms));
  }
  return PutterBacker<uint32_t>();
}

PutterBacker<uint32_t> G8DrawList::withDialUniforms(DialUniforms const &dialUniforms, G8ShaderGeneric const *shader)
{
  auto &setup = getSetup(shader);
  if (shader->g0Binding.dialUniforms != -1) {
    return PutterBacker<uint32_t>(&setup.g0Data[setup.perSW->g0DynamicOffsets.dialUniforms], setup.perW->uniformsBuf.addDynamic(dialUniforms));
  }
  return PutterBacker<uint32_t>();
}


PutterBacker<uint32_t> G8DrawList::withImgTex(WGPUTexture texture, G8ShaderGeneric const *shader)
{
  if (!texture) throw runtime_error("withImgTex: null texture");
  auto &setup = getSetup(shader);
  if (shader->g1Binding.imgTex != -1) {

    auto &texiSlot = setup.g1Data.at(shader->g1Binding.imgTex);
    if (texiSlot >= 0 && texiSlot < setup.perSW->textures.size()) {
      if (setup.perSW->textures.at(texiSlot) == texture) {
        return PutterBacker<uint32_t>();
      }
    }

    int newval = setup.perSW->textures.size();
    setup.perSW->textures.push_back(texture);
    return PutterBacker<uint32_t>(&texiSlot, newval);
  }
  return PutterBacker<uint32_t>();
}


PutterBacker<uint32_t> G8DrawList::withTraceData(WGPUBuffer buffer, G8ShaderGeneric const *shader)
{
  auto &setup = getSetup(shader);
  if (shader->g1Binding.traceData != -1) {

    auto &slot = setup.g1Data.at(shader->g1Binding.traceData);
    if (slot >= 0 && slot < setup.perSW->buffers.size()) {
      if (setup.perSW->buffers[slot] == buffer) {
        return PutterBacker<uint32_t>();
      }
    }

    int newval = setup.perSW->buffers.size();
    setup.perSW->buffers.push_back(buffer);

    return PutterBacker<uint32_t>(&slot, newval);
  }
  return PutterBacker<uint32_t>();
}


PutterBacker<uint32_t> G8DrawList::withTraceVisInfo(WGPUBuffer buffer, G8ShaderGeneric const *shader)
{
  auto &setup = getSetup(shader);
  if (shader->g1Binding.traceVisInfo != -1) {

    auto &slot = setup.g1Data.at(shader->g1Binding.traceVisInfo);
    if (slot >= 0 && slot < setup.perSW->buffers.size()) {
      if (setup.perSW->buffers[slot] == buffer) {
        return PutterBacker<uint32_t>();
      }
    }

    int newval = setup.perSW->buffers.size();
    setup.perSW->buffers.push_back(buffer);

    return PutterBacker<uint32_t>(&slot, newval);
  }
  return PutterBacker<uint32_t>();
}


PutterBacker<int> G8DrawList::withViBuf(int viBufIndex, G8ShaderGeneric const *shader)
{
  auto &setup = getSetup(shader);
  return PutterBacker<int>(&setup.viBufIndex, viBufIndex);
}


void G8DrawList::addDot(
  vec3 const &a, vec4 const &aCol,
  F layer,
  G8ShaderGeneric const *shader)
{
  auto &setup = getSetup(shader);
  uint32_t vi = 0;
  if (a.hasNaN()) return;

  F diag = 0.001;

  if (setup.vt == G8Vtx_pcl::vt) {
    G8VtxBlaster<G8Vtx_pcl> vb(setup, 4, vi);
    vb(a + vec3(-diag, -diag, 0), aCol, layer);
    vb(a + vec3(+diag, -diag, 0), aCol, layer);
    vb(a + vec3(+diag, +diag, 0), aCol, layer);
    vb(a + vec3(-diag, +diag, 0), aCol, layer);
  }
  else {
    return badShader(setup.shader, "addDot", {G8Vtx_pcl::vt});
  }

  G8IdxBlaster ib(setup, 6);
  ib.tripair(vi + 0, vi + 1, vi + 2, vi + 3);
  addCmd(ib);
}


void G8DrawList::addLine(
  vec3 const &a, vec4 const &aCol,
  vec3 const &b, vec4 const &bCol,
  F layer,
  G8ShaderGeneric const *shader)
{
  auto &setup = getSetup(shader);
  uint32_t vi = 0;
  if (a.hasNaN() || b.hasNaN()) return;

  if (setup.vt == G8Vtx_pcl::vt) {
    G8VtxBlaster<G8Vtx_pcl> vb(setup, 2, vi);
    vb(a, aCol, layer);
    vb(b, bCol, layer);
  }
  else if (setup.vt == G8Vtx_pucl::vt) {
    G8VtxBlaster<G8Vtx_pucl> vb(setup, 2, vi);
    vb(a, vec2(0, 0), aCol, layer);
    vb(b, vec2(0, 0), bCol, layer);
  }
  else if (setup.vt == G8Vtx_pncl::vt) {
    G8VtxBlaster<G8Vtx_pncl> vb(setup, 2, vi);
    vb(a, vec3(0, 0, 1), aCol, layer);
    vb(b, vec3(0, 0, 1), bCol, layer);
  }
  else {
    return badShader(setup.shader, "addLine", {G8Vtx_pcl::vt, G8Vtx_pucl::vt, G8Vtx_pncl::vt});
  }

  G8IdxBlaster ib(setup, 2);
  ib.line(vi + 0, vi + 1);
  // FIXME addCmd(ib, WGPUPrimitiveTopology_LineList);
}

void G8DrawList::addThickCurve(
  vector<vec3> const &pts,
  vec3 const &up,
  vec4 const &col,
  F width,
  F layer,
  G8ShaderGeneric const *shader)
{
  if (pts.size() < 2) return;

  auto &setup = getSetup(shader);

  G8IdxBlaster ib(setup, pts.size() * 18);

  uint32_t vi = 0;
  if (setup.vt == G8Vtx_pucl::vt) {
    G8VtxBlaster<G8Vtx_pucl> vb(setup, 6 + 6 * pts.size(), vi);

    // if zero up vector, make it be towards viewer
    vec3 realup = up;
    if (realup.squaredNorm() == 0.0f) {
      realup = iview.topLeftCorner<3,3>() * vec3(0, 0, 1);
    }

    auto thickPixels = pixelSize(pts[pts.size()/2], width);
    auto uvl = GetIO().Fonts->TexUvLines[max(1, min(IM_DRAWLIST_TEX_LINES_WIDTH_MAX-1, int(thickPixels)))];

    F halfWidth = 0.5f * width;

    vec2 tex0(uvl.x, uvl.y);
    vec2 tex1(uvl.z, uvl.w);

    vec3 lastDir = (pts[1] - pts[0]).normalized();
    vec3 lastNormal = lastDir.cross(realup).normalized();
    vb(pts[0] + halfWidth * lastNormal, tex0, col, layer);
    vb(pts[0] - halfWidth * lastNormal, tex1, col, layer);

    for (size_t i = 0; i + 1 < pts.size(); i++) {
      vec3 dir = (pts[i+1] - pts[i]).normalized();
      vec3 normal;
      if (dir.dot(lastDir) < 0.75f) {
        vb(pts[i] + halfWidth * lastNormal, tex0, col, layer);
        vb(pts[i] - halfWidth * lastNormal, tex1, col, layer);
        ib.tripair(vi+0, vi+1, vi+2, vi+3);
        vi += 2;

        normal = dir.cross(realup).normalized();
        vb(pts[i] + halfWidth * normal, tex0, col, layer);
        vb(pts[i] - halfWidth * normal, tex1, col, layer);
        ib.tripair(vi-2, vi-1, vi+0, vi+1);
        vi += 2;
      }
      else {
        normal = (dir + lastDir).cross(realup).normalized();
      }
      if (i + 1 == pts.size()) break;

      vb(pts[i+1] + halfWidth * normal, tex0, col, layer);
      vb(pts[i+1] - halfWidth * normal, tex1, col, layer);
      ib.tripair(vi+0, vi+1, vi+2, vi+3);
      vi += 2;
      lastDir = dir;
      lastNormal = normal;
    }
  }
  else if (setup.vt == G8Vtx_pcl::vt) {
    G8VtxBlaster<G8Vtx_pcl> vb(setup, 6 + 6 * pts.size(), vi);

    // if zero up vector, make it be towards viewer
    vec3 realup = up;
    if (realup.squaredNorm() == 0.0f) {
      realup = iview.topLeftCorner<3,3>() * vec3(0, 0, 1);
    }

    F halfWidth = 0.5f * width;

    vec3 lastDir = (pts[1] - pts[0]).normalized();
    vec3 lastNormal = lastDir.cross(realup).normalized();
    vb(pts[0] + halfWidth * lastNormal, col, layer);
    vb(pts[0] - halfWidth * lastNormal, col, layer);

    for (size_t i = 0; i + 1 < pts.size(); i++) {
      vec3 dir = (pts[i+1] - pts[i]).normalized();
      vec3 normal;
      if (dir.dot(lastDir) < 0.75f) {
        vb(pts[i] + halfWidth * lastNormal, col, layer);
        vb(pts[i] - halfWidth * lastNormal, col, layer);
        ib.tripair(vi+0, vi+1, vi+2, vi+3);
        vi += 2;

        normal = dir.cross(realup).normalized();
        vb(pts[i] + halfWidth * normal, col, layer);
        vb(pts[i] - halfWidth * normal, col, layer);
        ib.tripair(vi-2, vi-1, vi+0, vi+1);
        vi += 2;
      }
      else {
        normal = (dir + lastDir).cross(realup).normalized();
      }
      if (i + 1 == pts.size()) break;

      vb(pts[i+1] + halfWidth * normal, col, layer);
      vb(pts[i+1] - halfWidth * normal, col, layer);
      ib.tripair(vi+0, vi+1, vi+2, vi+3);
      vi += 2;
      lastDir = dir;
      lastNormal = normal;
    }
  }    
  else {
    return badShader(setup.shader, "addThickCurve (width>0)", {G8Vtx_pucl::vt, G8Vtx_pcl::vt});
  }
  addCmd(ib);
}


void G8DrawList::addBand(
  vector<pair<vec3, vec3>> const &pts,
  vec3 const &up,
  vec4 const &col,
  F layer,
  G8ShaderGeneric const *shader)
{
  auto &setup = getSetup(shader);
  G8IdxBlaster ib(setup, pts.size() * 6);
  uint32_t vi = 0;
  if (setup.vt == G8Vtx_pucl::vt) {
    G8VtxBlaster<G8Vtx_pucl> vb(setup, 2 * pts.size(), vi);

    for (size_t i = 0; i < pts.size(); i++) {

      auto thickPixels = pixelSize(pts[i].first, (pts[i].first - pts[i].second).norm());

      auto uvl = GetIO().Fonts->TexUvLines[max(1, min(IM_DRAWLIST_TEX_LINES_WIDTH_MAX-1, int(thickPixels)))];
      vec2 tex0(uvl.x, uvl.y);
      vec2 tex1(uvl.z, uvl.w);

      vb(pts[i].first, tex0, col, layer);
      vb(pts[i].second, tex1, col, layer);

      if (i > 0) {
        ib.tripair(vi-2, vi-1, vi+0, vi+1);
      }
      vi += 2;
    }
  }
  else {
    return badShader(shader, "addBand", {G8Vtx_pucl::vt});
  }

  addCmd(ib);
}



void G8DrawList::addTriangle(
  vec3 const &a, vec4 const &aCol,
  vec3 const &b, vec4 const &bCol,
  vec3 const &c, vec4 const &cCol,
  F layer,
  G8ShaderGeneric const *shader)
{
  vec3 normal = (b-a).cross(c-a).normalized();
  addTriangle(
    a, normal, aCol,
    b, normal, bCol,
    c, normal, cCol,
    layer, shader);
}

void G8DrawList::addTriangle(
  vec3 const &a, vec3 const &aNorm, vec4 const &aCol,
  vec3 const &b, vec3 const &bNorm, vec4 const &bCol,
  vec3 const &c, vec3 const &cNorm, vec4 const &cCol,
  F layer,
  G8ShaderGeneric const *shader)
{
  auto &setup = getSetup(shader);
  uint32_t vi = 0;
  if (a.hasNaN() || b.hasNaN() || c.hasNaN()) return;

  if (setup.vt == G8Vtx_pncl::vt) {
    G8VtxBlaster<G8Vtx_pncl> vb(setup, 3, vi);
    vb(a, aNorm, aCol, layer);
    vb(b, bNorm, bCol, layer);
    vb(c, cNorm, cCol, layer);
  }
  else if (setup.vt == G8Vtx_pnucl::vt) {
    G8VtxBlaster<G8Vtx_pnucl> vb(setup, 3, vi);
    vb(a, aNorm, vec2(0, 0), aCol, layer);
    vb(b, bNorm, vec2(0, 0), bCol, layer);
    vb(c, cNorm, vec2(0, 0), cCol, layer);
  }
  else if (setup.vt == G8Vtx_pucl::vt) {
    G8VtxBlaster<G8Vtx_pucl> vb(setup, 3, vi);
    vb(a, vec2(0, 0), aCol, layer);
    vb(b, vec2(0, 0), bCol, layer);
    vb(c, vec2(0, 0), cCol, layer);
  }
  else if (setup.vt == G8Vtx_pcl::vt) {
    G8VtxBlaster<G8Vtx_pcl> vb(setup, 3, vi);
    vb(a, aCol, layer);
    vb(b, bCol, layer);
    vb(c, cCol, layer);
  }
  else {
    return badShader(shader, "addTriangle (pnc)", {G8Vtx_pncl::vt, G8Vtx_pnucl::vt, G8Vtx_pucl::vt, G8Vtx_pcl::vt});
  }

  G8IdxBlaster ib(setup, 3);
  ib.tri(vi+0, vi+1, vi+2);
  addCmd(ib);
}

void G8DrawList::addCircle(
  vec3 const &center,
  vec3 const &axis0,
  vec3 const &axis1,
  vec4 const &col,
  F layer,
  G8ShaderGeneric const *shader)
{
  auto &setup = getSetup(shader);
  if (center.hasNaN() || axis0.hasNaN() || axis1.hasNaN()) return;
  const int segments = 16;

  vec3 normal = axis0.cross(axis1).normalized();
  float segmentAngle = M_2PI / segments;

  uint32_t vi = 0;
  if (setup.vt == G8Vtx_pncl::vt) {
    G8VtxBlaster<G8Vtx_pncl> vb(setup, 1 + 1 * (segments + 1), vi);
    vb(center, normal, col, layer);
    for (int i = 0; i < segments + 1; i++) {
      float theta = i * segmentAngle;
      vb(center + cos(theta) * axis0 + sin(theta) * axis1, normal, col, layer);
    }
  }
  else if (setup.vt == G8Vtx_pnucl::vt) {
    G8VtxBlaster<G8Vtx_pnucl> vb(setup, 1 + 1 * (segments + 1), vi);
    auto uvl = GetFontTexUvWhitePixel();
    vec2 uv(uvl.x, uvl.y);
    vb(center, normal, uv, col, layer);
    for (int i = 0; i < segments + 1; i++) {
      float theta = i * segmentAngle;
      vb(center + cos(theta) * axis0 + sin(theta) * axis1, normal, uv, col, layer);
    }
  }
  else if (setup.vt == G8Vtx_pucl::vt) {
    G8VtxBlaster<G8Vtx_pucl> vb(setup, 1 + 1 * (segments + 1), vi);
    auto uvl = GetFontTexUvWhitePixel();
    vec2 uv(uvl.x, uvl.y);
    vb(center, uv, col, layer);
    for (int i = 0; i < segments + 1; i++) {
      float theta = i * segmentAngle;
      vb(center + cos(theta) * axis0 + sin(theta) * axis1, uv, col, layer);
    }
  }
  else {
    return badShader(shader, "addCircle", {G8Vtx_pncl::vt, G8Vtx_pnucl::vt, G8Vtx_pucl::vt});
  }

  G8IdxBlaster ib(setup, segments * 3);
  for (int i = 0; i < segments; i++) {
    ib.tri(vi+0, vi+i+1, vi+i+2);
  }
  addCmd(ib);
}

void G8DrawList::addSphere(
  vec3 const &center,
  vec3 const &axis0,
  vec3 const &axis1,
  vec3 const &axis2,
  vec4 const &col,
  F layer,
  G8ShaderGeneric const *shader)
{
  auto &setup = getSetup(shader);
  if (center.hasNaN() || axis0.hasNaN() || axis1.hasNaN() || axis2.hasNaN()) return;

  const int NTH = 16, NDM = 8;
  uint32_t vi = 0;
  if (setup.vt == G8Vtx_pncl::vt) {
    G8VtxBlaster<G8Vtx_pncl> vb(setup, 2 + (NDM - 1) * NTH, vi);

    vb(
      center + axis2,
      axis2,
      col, layer);
    vb(
      center - axis2,
      -axis2,
      col, layer);

    for (int dmi = 1; dmi < NDM; dmi ++) {
      F dm = dmi * (M_PI/NDM);
      for (int thi = 0; thi < NTH; thi ++) {
        F th = thi * (M_2PI/NTH);
        vb(
          center + 
            sin(th) * sin(dm) * axis0 + 
            cos(th) * sin(dm) * axis1 +
            cos(dm) * axis2,
          (sin(th) * sin(dm) * axis0 + cos(th) * sin(dm) * axis1 + cos(dm) * axis2),
          col, layer);
      }
    }
    auto vtxi=[vi](int dmi, int thi) {
      if (dmi == 0) return vi + 0;
      if (dmi == NDM) return vi + 1;
      return vi + 2 + NTH * (dmi - 1) + (thi % NTH);
    };

    G8IdxBlaster ib(setup, NDM * NTH * 6);
    for (int dmi = 0; dmi < NDM; dmi ++) {
      for (int thi = 0; thi < NTH; thi ++) {
        ib.tripair(
          vtxi(dmi + 0, thi + 0),
          vtxi(dmi + 0, thi + 1),
          vtxi(dmi + 1, thi + 0),
          vtxi(dmi + 1, thi + 1));
      }
    }
    addCmd(ib);
  }
  else {
    return badShader(shader, "addSphere", {G8Vtx_pncl::vt});
  }
}

void G8DrawList::addQuad(
  vec3 const &a, vec2 const &auv, vec4 const &aCol,
  vec3 const &b, vec2 const &buv, vec4 const &bCol,
  vec3 const &c, vec2 const &cuv, vec4 const &cCol,
  vec3 const &d, vec2 const &duv, vec4 const &dCol,
  F layer,
  G8ShaderGeneric const *shader)
{
  auto &setup = getSetup(shader);
  // NaNs in the vertex buffer seem like bad luck
  if (a.hasNaN() || b.hasNaN() || c.hasNaN() || d.hasNaN()) return;

  uint32_t vi = 0;
  if (setup.vt == G8Vtx_pucl::vt) {
    G8VtxBlaster<G8Vtx_pucl> vb(setup, 4, vi);
    vb(a, auv, aCol, layer);
    vb(b, buv, bCol, layer);
    vb(c, cuv, cCol, layer);
    vb(d, duv, dCol, layer);
  }
  else {
    return badShader(shader, "addQuad (pucl)", {G8Vtx_pucl::vt});
  }

  G8IdxBlaster ib(setup, 6);
  ib.tripair(vi+0, vi+1, vi+2, vi+3);
  addCmd(ib);
}


void G8DrawList::addQuad(
  vec3 const &a, vec2 const &auv,
  vec3 const &b, vec2 const &buv,
  vec3 const &c, vec2 const &cuv,
  vec3 const &d, vec2 const &duv,
  F layer,
  G8ShaderGeneric const *shader)
{
  auto &setup = getSetup(shader);
  // NaNs in the vertex buffer seem like bad luck
  if (a.hasNaN() || b.hasNaN() || c.hasNaN() || d.hasNaN()) return;

  uint32_t vi = 0;
  if (setup.vt == G8Vtx_pul::vt) {
    G8VtxBlaster<G8Vtx_pul> vb(setup, 4, vi);
    vb(a, auv, layer);
    vb(b, buv, layer);
    vb(c, cuv, layer);
    vb(d, duv, layer);
  }
  else {
    return badShader(shader, "addQuad (pul)", {G8Vtx_pul::vt});
  }

  G8IdxBlaster ib(setup, 6);
  ib.tripair(vi+0, vi+1, vi+2, vi+3);
  addCmd(ib);
}


void G8DrawList::addQuad(
  vec3 const &a, vec3 const &aNorm, vec4 const &aCol,
  vec3 const &b, vec3 const &bNorm, vec4 const &bCol,
  vec3 const &c, vec3 const &cNorm, vec4 const &cCol,
  vec3 const &d, vec3 const &dNorm, vec4 const &dCol,
  F layer,
  G8ShaderGeneric const *shader)

{
  auto &setup = getSetup(shader);

  if (a.hasNaN() || b.hasNaN() || c.hasNaN() || d.hasNaN()) return;

  uint32_t vi = 0;
  if (setup.vt == G8Vtx_pnucl::vt) {
    G8VtxBlaster<G8Vtx_pnucl> vb(setup, 4, vi);
    vb(a, aNorm, vec2(0, 0), aCol, layer);
    vb(b, bNorm, vec2(0, 0), bCol, layer);
    vb(c, cNorm, vec2(0, 0), cCol, layer);
    vb(d, dNorm, vec2(0, 0), dCol, layer);
  }
  else if (setup.vt == G8Vtx_pncl::vt) {
    G8VtxBlaster<G8Vtx_pncl> vb(setup, 4, vi);
    vb(a, aNorm, aCol, layer);
    vb(b, bNorm, bCol, layer);
    vb(c, cNorm, cCol, layer);
    vb(d, dNorm, dCol, layer);
  }
  else if (setup.vt == G8Vtx_pucl::vt) {
    G8VtxBlaster<G8Vtx_pucl> vb(setup, 4, vi);
    vb(a, vec2(0, 0), aCol, layer);
    vb(b, vec2(0, 0), bCol, layer);
    vb(c, vec2(0, 0), cCol, layer);
    vb(d, vec2(0, 0), dCol, layer);
  }
  else if (setup.vt == G8Vtx_pcl::vt) {
    G8VtxBlaster<G8Vtx_pcl> vb(setup, 4, vi);
    vb(a, aCol, layer);
    vb(b, bCol, layer);
    vb(c, cCol, layer);
    vb(d, dCol, layer);
  }
  else {
    return badShader(shader, "addQuad (pnc)", {G8Vtx_pnucl::vt, G8Vtx_pncl::vt, G8Vtx_pcl::vt});
  }

  G8IdxBlaster ib(setup, 6);
  ib.tripair(vi+0, vi+1, vi+2, vi+3);
  addCmd(ib);
}

void G8DrawList::addQuad(
  vec3 const &a, vec4 const &aCol,
  vec3 const &b, vec4 const &bCol,
  vec3 const &c, vec4 const &cCol,
  vec3 const &d, vec4 const &dCol,
  F layer,
  G8ShaderGeneric const *shader)
{
  vec3 normal = (b - a).cross(c - a).normalized();
  addQuad(
    a, normal, aCol,
    b, normal, bCol,
    c, normal, cCol,
    d, normal, dCol,
    layer, shader);
}

void G8DrawList::addBox(
  mat4 const &m,
  vec4 const &col,
  vec3 const &dim,
  F layer,
  G8ShaderGeneric const *shader)
{
  auto scale = mat4 {
      {dim.x(), 0, 0, 0},
      {0, dim.y(), 0, 0},
      {0, 0, dim.z(), 0},
      {0, 0, 0, 1}};

  auto muScope = withModelUniforms(ModelUniforms(m * scale), shader);

  auto &setup = getSetup(shader);
  auto restoreViBuf = withViBuf(1, shader);

  auto &permcacheSlot = setup.perSW->geomCache[ObjectKey("addBox", 0)];
  if (!permcacheSlot) {
    uint32_t vi = 0;
    if (setup.vt == G8Vtx_pncl::vt) {
      G8VtxBlaster<G8Vtx_pncl> vb(setup, 24, vi);
      vb(vec3(-1, -1, -1), vec3(0, -1, 0), col, layer);
      vb(vec3(-1, -1, +1), vec3(0, -1, 0), col, layer);
      vb(vec3(+1, -1, -1), vec3(0, -1, 0), col, layer);
      vb(vec3(+1, -1, +1), vec3(0, -1, 0), col, layer);

      vb(vec3(-1, +1, -1), vec3(0, +1, 0), col, layer);
      vb(vec3(-1, +1, +1), vec3(0, +1, 0), col, layer);
      vb(vec3(+1, +1, -1), vec3(0, +1, 0), col, layer);
      vb(vec3(+1, +1, +1), vec3(0, +1, 0), col, layer);
    
      vb(vec3(+1, -1, -1), vec3(+1, 0, 0), col, layer);
      vb(vec3(+1, -1, +1), vec3(+1, 0, 0), col, layer);
      vb(vec3(+1, +1, -1), vec3(+1, 0, 0), col, layer);
      vb(vec3(+1, +1, +1), vec3(+1, 0, 0), col, layer);

      vb(vec3(-1, -1, -1), vec3(-1, 0, 0), col, layer);
      vb(vec3(-1, -1, +1), vec3(-1, 0, 0), col, layer);
      vb(vec3(-1, +1, -1), vec3(-1, 0, 0), col, layer);
      vb(vec3(-1, +1, +1), vec3(-1, 0, 0), col, layer);

      vb(vec3(-1, -1, +1), vec3(0, 0, +1), col, layer);
      vb(vec3(-1, +1, +1), vec3(0, 0, +1), col, layer);
      vb(vec3(+1, -1, +1), vec3(0, 0, +1), col, layer);
      vb(vec3(+1, +1, +1), vec3(0, 0, +1), col, layer);

      vb(vec3(-1, -1, -1), vec3(0, 0, -1), col, layer);
      vb(vec3(-1, +1, -1), vec3(0, 0, -1), col, layer);
      vb(vec3(+1, -1, -1), vec3(0, 0, -1), col, layer);
      vb(vec3(+1, +1, -1), vec3(0, 0, -1), col, layer);
    }
    else {
      return badShader(shader, "AddBox", {G8Vtx_pncl::vt});
    }

    G8IdxBlaster ib(setup, 6*6);
    ib.tripair(vi+0, vi+1, vi+2, vi+3);
    ib.tripair(vi+4, vi+5, vi+6, vi+7);
    ib.tripair(vi+8, vi+9, vi+10, vi+11);
    ib.tripair(vi+12, vi+13, vi+14, vi+15);
    ib.tripair(vi+16, vi+17, vi+18, vi+19);
    ib.tripair(vi+20, vi+21, vi+22, vi+23);
    permcacheSlot = ib;
  }
  addCmd(setup, *permcacheSlot);
}


void G8DrawList::addAnnulus(
  vec3 const &center,
  vec3 const &radx, vec3 const &rady,
  F innerRad, F outerRad,
  F th0, F th1,
  vec4 const &col,
  F layer,
  G8ShaderGeneric const *shader)
{
  auto &setup = getSetup(shader);
  if (center.hasNaN() || radx.hasNaN() || rady.hasNaN()) return;

  uint32_t vi = 0;

  vec3 normal = radx.cross(rady).normalized();

  int segments = max(1, int(ceil(16.0/M_PI * abs(th1 - th0))));
  F segmentAngle = (th1 - th0) / F(segments);

  if (setup.vt == G8Vtx_pncl::vt) {
    G8VtxBlaster<G8Vtx_pncl> vb(setup, 2 * (segments + 1), vi);
    for (int i = 0; i < segments + 1; i++) {
      float theta = th0 + segmentAngle * i;
      vb(center + cos(theta) * outerRad * radx + sin(theta) * outerRad * rady, normal, col, layer);
      vb(center + cos(theta) * innerRad * radx + sin(theta) * innerRad * rady, normal, col, layer);
    }
  }
  else {
    return badShader(shader, "addAnnulus", {G8Vtx_pncl::vt});
  }

  G8IdxBlaster ib(setup, segments * 6);

  for (int i = 0; i < segments; i++) {
    ib.tripair(vi + i*2 + 0, vi + i*2 + 1, vi + i*2 + 2, vi + i*2 + 3);
  }
  addCmd(ib);
}

void G8DrawList::addGlyph(
  vec3 &curPos, vec3 const &xDir, vec3 const &yDir, vec3 const &normal,
  vec4 const &col,
  const ImFontGlyph* glyph,
  F layer,
  G8ShaderGeneric const *shader)
{
  if (glyph->Visible) {
    auto &setup = getSetup(shader);

    vec3 p00 = curPos + (glyph->X0 * xDir + glyph->Y0 * yDir);
    vec3 p01 = curPos + (glyph->X1 * xDir + glyph->Y0 * yDir);
    vec3 p10 = curPos + (glyph->X0 * xDir + glyph->Y1 * yDir);
    vec3 p11 = curPos + (glyph->X1 * xDir + glyph->Y1 * yDir);

    vec2 uv00(glyph->U0, glyph->V0);
    vec2 uv01(glyph->U1, glyph->V0);
    vec2 uv10(glyph->U0, glyph->V1);
    vec2 uv11(glyph->U1, glyph->V1);

    if (0 && (glyph->Codepoint == 0x2d || glyph->Codepoint == 0x5f || glyph->Codepoint == 0x41)) {
      L() << glyph->Codepoint << 
        " uv00=" << uv00.x()*1024 << " " << uv00.y()*2048 <<
        " uv01=" << uv01.x()*1024 << " " << uv01.y()*2048 << 
        " uv10=" << uv10.x()*1024 << " " << uv10.y()*2048 << 
        " uv11=" << uv11.x()*1024 << " " << uv11.y()*2048;
    }

    uint32_t vi = 0;
    if (setup.vt == G8Vtx_pul::vt) {
      G8VtxBlaster<G8Vtx_pul> vb(setup, 4, vi);
      vb(p00, uv00, layer);
      vb(p01, uv01, layer);
      vb(p10, uv10, layer);
      vb(p11, uv11, layer);
    }
    else if (setup.vt == G8Vtx_pucl::vt) {
      G8VtxBlaster<G8Vtx_pucl> vb(setup, 4, vi);
      vb(p00, uv00, col, layer);
      vb(p01, uv01, col, layer);
      vb(p10, uv10, col, layer);
      vb(p11, uv11, col, layer);
    }
    else {
      return badShader(shader, "addGlyph", {G8Vtx_pul::vt, G8Vtx_pucl::vt});
    }

    G8IdxBlaster ib(setup, 6);
    ib.tripair(vi+0, vi+1, vi+2, vi+3);
    addCmd(ib);
  }
  curPos += glyph->AdvanceX * xDir;
}

void G8DrawList::alignText(
  vec3 &curPos, vec3 const &xDir, vec3 const &yDir,
  string_view text,
  ImFont *font,
  F xJust, F yJust)
{
  F fscale = 1.0f / font->FontSize;

  vec3 lineStart(0, 0, 0);

  F relx = 0.0f, rely = 0.0f;
  F maxWidth = 0.0f;

  for (auto [c, fofs] : vbParseUtf8(text)) {
    if (c == '\n') {
      rely += font->FontSize;
      relx = 0.0f;
      continue;
    }
    else if (c == '\r' || c == 0) {
      continue;
    }

    const ImFontGlyph* glyph = font->FindGlyph((ImWchar)c);
    if (glyph) {
      relx += glyph->AdvanceX;
      maxWidth = max(maxWidth, relx);
    }
  }
  rely += font->FontSize;
  
  curPos -= xDir * fscale * maxWidth * xJust;
  curPos -= yDir * fscale * rely * yJust;

}

void G8DrawList::addTextAdvance(
  vec3 &curPos, vec3 const &xDir, vec3 const &yDir, vec3 const &normal,
  string_view text,
  vec4 const &col,
  ImFont *font,
  F layer,
  G8ShaderGeneric const *shader)
{ 
  F fscale = 1.0f / font->FontSize;

  auto lineStart = curPos;

  for (auto [c, fofs] : vbParseUtf8(text)) {
    if (c == '\n') {
      lineStart += yDir;
      curPos = lineStart;
      continue;
    }
    else if (c == '\r' || c == 0) {
      continue;
    }

    const ImFontGlyph* glyph = font->FindGlyph((ImWchar)c);
    if (glyph) {
      addGlyph(curPos, xDir * fscale, yDir * fscale, normal,
        col,
        glyph,
        layer, shader);
    }
  }
}

void G8DrawList::addText(
  vec3 const &curPos, vec3 const &xDir, vec3 const &yDir, vec3 const &norm,
  string_view text,
  vec4 const &col,
  ImFont *font,
  F xJust, F yJust,
  F layer,
  G8ShaderGeneric const *shader)
{
  auto curPosMut = curPos;
  if (xJust != 0.0f || yJust != 0.0f) {
    alignText(curPosMut, xDir, yDir, text, font, xJust, yJust);
  }
  addTextAdvance(curPosMut, xDir, yDir, norm, text, col, font, layer, shader);
}


void G8DrawList::addCapsule(
  mat4 const &tpos,
  vec4 const &col,
  vec3 const &size,
  F layer,
  G8ShaderGeneric const *shader)
{
  auto &setup = getSetup(shader);

  mat3 tdir = tpos.topLeftCorner<3, 3>();

  auto xpos = [tpos](F x, F y, F z) -> vec3 {
    return (tpos * vec4(x, y, z, 1.0)).head<3>();
  };

  auto xdir = [tdir](F x, F y, F z) -> vec3 {
    return tdir * vec3(x, y, z);
  };

  const int NTH = 16, NDM = 8;
  F av = 0.5f * (size[0] + size[1]);

  G8IdxBlaster ib(setup, NTH * (12 * NDM + 6));

  uint32_t vi = 0;
  if (setup.vt == G8Vtx_pncl::vt) {
    G8VtxBlaster<G8Vtx_pncl> vb(setup, NTH * (8 * NDM + 4), vi);

    for (int thi = 0; thi < NTH; thi ++) {
      F th0 = (thi + 0) * (M_2PI/NTH);
      F th1 = (thi + 1) * (M_2PI/NTH);
      F sth0 = sin(th0), cth0 = cos(th0), sth1 = sin(th1), cth1 = cos(th1);

      vb(xpos(sth0 * size[0], cth0 * size[1], -size[2]), 
        xdir(sth0 * size[0], cth0 * size[1], 0),
        col, layer);
      vb(xpos(sth1 * size[0], cth1 * size[1], -size[2]),
        xdir(sth1 * size[0], cth1 * size[1], 0),
        col, layer);
      vb(xpos(sth0 * size[0], cth0 * size[1], +size[2]),
        xdir(sth1 * size[0], cth0 * size[1], 0),
        col, layer);
      vb(xpos(sth1 * size[0], cth1 * size[1], +size[2]),
        xdir(sth1 * size[0], cth1 * size[1], 0),
        col, layer);
      ib.tripair(vi+0, vi+1, vi+2, vi+3);
      vi += 4;

      for (int dmi = 0; dmi < NDM; dmi ++) {
        F dm0 = (dmi + 0) * (M_PI_2/NDM);
        F dm1 = (dmi + 1) * (M_PI_2/NDM);
        F sdm0 = sin(dm0), cdm0 = cos(dm0), sdm1 = sin(dm1), cdm1 = cos(dm1);

        vb(xpos(sth0 * size[0] * cdm0, cth0 * size[1] * cdm0, +size[2] + av * sdm0),
          xdir(sth0 * size[0] * cdm0, cth0 * size[1] * cdm0, av * sdm0),
          col, layer);
        vb(xpos(sth1 * size[0] * cdm0, cth1 * size[1] * cdm0, +size[2] + av * sdm0),
          xdir(sth1 * size[0] * cdm0, cth1 * size[1] * cdm0, av * sdm0),
          col, layer);
        vb(xpos(sth0 * size[0] * cdm1, cth0 * size[1] * cdm1, +size[2] + av * sdm1),
          xdir(sth0 * size[0] * cdm1, cth0 * size[1] * cdm1, av * sdm1),
          col, layer);
        vb(xpos(sth1 * size[0] * cdm1, cth1 * size[1] * cdm1, +size[2] + av * sdm1),
          xdir(sth1 * size[0] * cdm1, cth1 * size[1] * cdm1, av * sdm1),
          col, layer);
        ib.tripair(vi+0, vi+1, vi+2, vi+3);
        vi += 4;

        vb(xpos(sth0 * size[0] * cdm0, cth0 * size[1] * cdm0, -size[2] - av * sdm0),
          xdir(sth0 * size[0] * cdm0, cth0 * size[1] * cdm0, -av * sdm0),
          col, layer);
        vb(xpos(sth1 * size[0] * cdm0, cth1 * size[1] * cdm0, -size[2] - av * sdm0),
          xdir(sth1 * size[0] * cdm0, cth1 * size[1] * cdm0, -av * sdm0),
          col, layer);
        vb(xpos(sth0 * size[0] * cdm1, cth0 * size[1] * cdm1, -size[2] - av * sdm1),
          xdir(sth0 * size[0] * cdm1, cth0 * size[1] * cdm1, -av * sdm1),
          col, layer);
        vb(xpos(sth1 * size[0] * cdm1, cth1 * size[1] * cdm1, -size[2] - av * sdm1),
          xdir(sth1 * size[0] * cdm1, cth1 * size[1] * cdm1, -av * sdm1),
          col, layer);
        ib.tripair(vi+0, vi+1, vi+2, vi+3);
        vi += 4;
      }
    }
  }
  addCmd(ib);
}

void G8DrawList::addArrow(
  vec3 const &root,
  vec3 const &axis,
  vec3 const &ca,
  vec3 const &cb,
  vec4 const &col,
  F layer,
  G8ShaderGeneric const *shader)
{
  if (root.hasNaN() || axis.hasNaN() || ca.hasNaN() || cb.hasNaN()) return;

  const int NR = 16;

  float shaftRad = 0.1;
  float headRad = 0.2;
  float tipRad = 0.0;

  float basePos = 0.0;
  float headPos = 0.7;
  float tipPos = 0.9;

  auto &setup = getSetup(shader);
  uint32_t vi = 0;
  if (setup.vt == G8Vtx_pncl::vt) {
    G8VtxBlaster<G8Vtx_pncl> vb(setup, 5 * NR, vi);

    // don't normalize normals
    for (int i = 0; i<NR; i++) {
      float theta = (i+0) * (M_2PI/NR);
      vec3 rad = (sinf(theta) * ca + cosf(theta) * cb);

      vb(root, -axis, col, layer);
      vb(root + basePos * axis + shaftRad * rad, -axis, col, layer);
      vb(root + headPos * axis + shaftRad * rad, rad, col, layer);
      vb(root + headPos * axis + headRad * rad, rad, col, layer);
      vb(root + tipPos * axis + tipRad * rad, tipRad * axis + (tipPos - headPos) * rad, col, layer);
    }
  }
  else {
    return badShader(shader, "addArrow", {G8Vtx_pncl::vt});
  }

  G8IdxBlaster ib(setup, NR * 18);

  for (int i = 0; i < NR; i++) {
    auto vi0 = vi + 5 * i;
    auto vi1 = vi + 5 * ((i + 1) % NR);
    
    ib.tri(vi0+0, vi0+1, vi1+1); // base
    ib.tripair(vi0+1, vi0+2, vi1+1, vi1+2); // shaft
    ib.tripair(vi0+2, vi0+3, vi1+2, vi1+3); // underside
    ib.tri(vi0+3, vi1+3, vi0+4); // tip
  }
  addCmd(ib);

}

void G8DrawList::addArrowTriple(mat4 const &model, F layer, G8ShaderGeneric const *shader)

{
  addArrow((model * vec4(0, 0, 0, 1)).head<3>(), (model * vec4(0.25, 0, 0, 0)).head<3>(), (model * vec4(0, 0.25, 0, 0)).head<3>(), (model * vec4(0, 0, 0.25, 0)).head<3>(), vec4(1.0, 0.0, 0.0, 0.75), layer, shader);
  addArrow((model * vec4(0, 0, 0, 1)).head<3>(), (model * vec4(0, 0.25, 0, 0)).head<3>(), (model * vec4(0.25, 0, 0, 0)).head<3>(), (model * vec4(0, 0, 0.25, 0)).head<3>(), vec4(0.0, 1.0, 0.0, 0.75), layer, shader);
  addArrow((model * vec4(0, 0, 0, 1)).head<3>(), (model * vec4(0, 0, 0.25, 0)).head<3>(), (model * vec4(0.25, 0, 0, 0)).head<3>(), (model * vec4(0, 0.25, 0, 0)).head<3>(), vec4(0.0, 0.0, 1.0, 0.75), layer, shader);
}

void G8DrawList::addVideo(
  mat4 const &model,
  WGPUTexture tex,
  F layer,
  G8ShaderGeneric const *shader)
{
  if (!tex) return;
  auto &setup = getSetup(shader);

  uint32_t vi = 0;
  if (setup.vt == G8Vtx_pul::vt) {
    G8VtxBlaster<G8Vtx_pul> vb(setup, 4, vi);
    vb((model * vec4(-1, +1, 0, 1)).head<3>(), vec2(0, 0), layer);
    vb((model * vec4(+1, +1, 0, 1)).head<3>(), vec2(1, 0), layer);
    vb((model * vec4(-1, -1, 0, 1)).head<3>(), vec2(0, 1), layer);
    vb((model * vec4(+1, -1, 0, 1)).head<3>(), vec2(1, 1), layer);
  }
  else {
    return badShader(shader, "addVideo", {G8Vtx_pul::vt});
  }

  G8IdxBlaster ib(setup, 6);
  ib.tripair(vi+0, vi+1, vi+2, vi+3);
  auto restoreTexture = withImgTex(tex, shader);
  assert(setup.perSW->textures.size() > 0);
  addCmd(ib);
}


G8GeomCacheEnt::G8GeomCacheEnt(G8IdxBlaster const &ib)
  : elemIndex(ib.getStartIndex()),
    elemCount(ib.getElemCount()),
    viBufIndex(ib.setup.viBufIndex)
{
}

template<>
void G8Cache<G8GeomCacheEnt>::cacheRelease(G8GeomCacheEnt &it)
{
}


/*
  TODO: Make this return a new class, possibly with a permcacheEnt,
  loaded using tinyobjloader
*/

#ifdef USE_STL_SOLID
map<string, shared_ptr<StlSolid>> solids;

StlSolid *G8DrawList::getNamedSolid(string const &fn)
{
  auto &solid = solids[fn];
  if (!solid) {
    solid = StlSolid::getNamedSolid(fn, 1.0, dmat4::Identity());
  }
  return solid.get();
}
#endif

static map<string, shared_ptr<tinyobj::ObjReader>> tinyobjs;

tinyobj::ObjReader *G8DrawList::getNamedObj(string const &fn)
{
  auto &obj = tinyobjs[fn];
  if (!obj) {
    obj = make_shared<tinyobj::ObjReader>();
    obj->ParseFromFile(fn);
  }
  return obj.get();
}


void G8DrawList::addSolid(
  string const &fn,
  mat4 const &m,
  vec4 const &col,
  F layer)
{
  if (0) {
  }
#ifdef USE_STL_SOLID
  else if (endsWith(fn, ".stl") || endsWith(fn, ".STL")) {
    auto solid = getNamedSolid(fn);
    if (!solid) return;
    addSolid(*solid, m, col, layer);
  }
#endif
  else if (endsWith(fn, ".obj") || endsWith(fn, ".OBJ")) {
    auto solid = getNamedObj(fn);
    if (!solid || !solid->Valid()) return;
    for (auto &shape : solid->GetShapes()) {
      addSolid(shape.mesh, solid->GetAttrib(), m, col, layer);
    }
  }
}

#ifdef USE_STL_SOLID
void G8DrawList::addSolid(
  StlSolid const &solid,
  mat4 const &m,
  vec4 const &col,
  F layer,
  G8ShaderGeneric const *shader)

{
  if (solid.faces.empty()) return;

  auto restoreViBuf = withViBuf(1, shader);
  auto &setup = getSetup(shader);
  auto &permcacheSlot = setup.perSW->geomCache[ObjectKey(&solid, 0)];
  if (!permcacheSlot) {
    G8IdxBlaster ib(setup, 3 * solid.faces.size());
    uint32_t vi = 0;
    if (setup.vt == G8Vtx_pnl::vt) {
      G8VtxBlaster<G8Vtx_pnl> vb(setup, 3 * solid.faces.size(), vi);
      for (auto &face : solid.faces) {
        vb(face.v0.cast<float>(), face.normal.cast<float>(), layer);
        vb(face.v1.cast<float>(), face.normal.cast<float>(), layer);
        vb(face.v2.cast<float>(), face.normal.cast<float>(), layer);
        ib.tri(vi + 0, vi + 1, vi + 2);
        vi += 3;
      }
    }
    else if (setup.vt == G8Vtx_pncl::vt) {
      G8VtxBlaster<G8Vtx_pncl> vb(setup, 3 * solid.faces.size(), vi);
      for (auto &face : solid.faces) {
        vb(face.v0.cast<float>(), face.normal.cast<float>(), col, layer);
        vb(face.v1.cast<float>(), face.normal.cast<float>(), col, layer);
        vb(face.v2.cast<float>(), face.normal.cast<float>(), col, layer);
        ib.tri(vi + 0, vi + 1, vi + 2);
        vi += 3;
      }
    }
    else {
      return badShader(shader, "addSolid(stl)", { G8Vtx_pnl::vt, G8Vtx_pncl::vt });
    }
    permcacheSlot = ib;
  }


  auto restoreModelUniforms = withModelUniforms(ModelUniforms(m), shader);
  addCmd(setup, *permcacheSlot);
}
#endif


void G8DrawList::addSolid(
  tinyobj::mesh_t const &mesh, tinyobj::attrib_t const &attrib,
  mat4 const &m,
  vec4 const &col,
  F layer,
  G8ShaderGeneric const *shader)
{
  auto &setup = getSetup(shader);
  auto restoreViBuf = withViBuf(1, shader);
  auto &permcacheSlot = setup.perSW->geomCache[ObjectKey(&mesh, 0)];
  if (!permcacheSlot) {
    // WRITEME

    G8IdxBlaster ib(setup, 3 * mesh.num_face_vertices.size());
    uint32_t vi = 0;

    if (setup.vt == G8Vtx_pnl::vt) {
      G8VtxBlaster<G8Vtx_pnl> vb(setup, 3 * mesh.num_face_vertices.size(), vi);

      size_t meshvi = 0;
      for (auto vertices : mesh.num_face_vertices) {
        if (vertices != 3) throw runtime_error("Expected triangles only");

        for (int k = 0; k < 3; k++) {
          auto idxs = mesh.indices[meshvi + k];

          vb(
            vec3(
              attrib.vertices[3 * idxs.vertex_index + 0],
              attrib.vertices[3 * idxs.vertex_index + 1],
              attrib.vertices[3 * idxs.vertex_index + 2]),
            vec3(
              attrib.normals[3 * idxs.normal_index + 0],
              attrib.normals[3 * idxs.normal_index + 1],
              attrib.normals[3 * idxs.normal_index + 2]),
            layer);
        }

        ib.tri(vi + 0, vi + 1, vi + 2);

        vi += 3;
        meshvi += 3;
      }
    }

    else if (setup.vt == G8Vtx_pnul::vt) {
      G8VtxBlaster<G8Vtx_pnul> vb(setup, 3 * mesh.num_face_vertices.size(), vi);

      size_t meshvi = 0;
      for (auto vertices : mesh.num_face_vertices) {
        if (vertices != 3) throw runtime_error("Expected triangles only");

        for (int k = 0; k < 3; k++) {
          auto idxs = mesh.indices[meshvi + k];

          vb(
            vec3(
              attrib.vertices[3 * idxs.vertex_index + 0],
              attrib.vertices[3 * idxs.vertex_index + 1],
              attrib.vertices[3 * idxs.vertex_index + 2]),
            vec3(
              attrib.normals[3 * idxs.normal_index + 0],
              attrib.normals[3 * idxs.normal_index + 1],
              attrib.normals[3 * idxs.normal_index + 2]),
            vec2(
              attrib.texcoords[2 * idxs.texcoord_index + 0],
              attrib.texcoords[2 * idxs.texcoord_index + 1]),
            layer);
        }

        ib.tri(vi + 0, vi + 1, vi + 2);

        vi += 3;
        meshvi += 3;
      }
    }
    else if (setup.vt == G8Vtx_pncl::vt) {
      G8VtxBlaster<G8Vtx_pncl> vb(setup, 3 * mesh.num_face_vertices.size(), vi);

      size_t meshvi = 0;
      for (auto vertices : mesh.num_face_vertices) {
        if (vertices != 3) throw runtime_error("Expected triangles only");

        for (int k = 0; k < 3; k++) {
          auto idxs = mesh.indices[meshvi + k];

          vb(
            vec3(
              attrib.vertices[3 * idxs.vertex_index + 0],
              attrib.vertices[3 * idxs.vertex_index + 1],
              attrib.vertices[3 * idxs.vertex_index + 2]),
            vec3(
              attrib.normals[3 * idxs.normal_index + 0],
              attrib.normals[3 * idxs.normal_index + 1],
              attrib.normals[3 * idxs.normal_index + 2]),
            col,
            layer);
        }

        ib.tri(vi + 0, vi + 1, vi + 2);

        vi += 3;
        meshvi += 3;
      }
    }

    else {
      return badShader(shader, "addSolid(mesh)", {G8Vtx_pnl::vt, G8Vtx_pnul::vt, G8Vtx_pncl::vt});
    }

    permcacheSlot = ib;
  }

  auto restoreModelUniforms = withModelUniforms(ModelUniforms(m), shader);
  addCmd(setup, *permcacheSlot);
}


void G8DrawList::addNoodle(
  vec3 const &p0, vec4 const &col0,
  vec3 const &p1, vec4 const &col1, 
  vec3 &tubeAxis,
  float tubeRad,
  F layer,
  G8ShaderGeneric const *shader)
{
  if (tubeRad == 0.0f) {
    addLine(p0, col0, p1, col1, layer, shader);
    return;
  }
  const int nth = 8;
  auto tubeX = (p1 - p0).cross(tubeAxis);
  if (tubeX.squaredNorm() < 1e-6f) {
    tubeAxis = vec3(1, 0, 0);
    tubeX = (p1 - p0).cross(tubeAxis);
  }
  if (tubeX.squaredNorm() < 1e-6f) {
    tubeAxis = vec3(0, 1, 0);
    tubeX = (p1 - p0).cross(tubeAxis);
  }
  if (tubeX.squaredNorm() < 1e-6f) {
    return;
  }
  tubeX = tubeX.normalized();
  auto tubeY = (p1 - p0).cross(tubeX).normalized();
  tubeAxis = tubeY;
  for (int thi = 0; thi < nth; thi+=1) {
    float th0 = float(thi + 0) * float(M_2PI / nth);
    float th1 = float(thi + 1) * float(M_2PI / nth);
    vec3 norm0 = cos(th0) * tubeX + sin(th0) * tubeY;
    vec3 norm1 = cos(th1) * tubeX + sin(th1) * tubeY;
    vec3 rad0 = tubeRad * norm0;
    vec3 rad1 = tubeRad * norm1;
    addQuad(p0 + rad0, norm0, col0, p0 + rad1, norm1, col0, p1 + rad0, norm0, col1, p1 + rad1, norm1, col1, layer, shader);
  }
}

void G8DrawList::addNoodle(
  vec3 const &p0, vec4 const &col0,
  vec3 const &p1, vec4 const &col1, 
  float tubeRad,
  F layer,
  G8ShaderGeneric const *shader)
{
  vec3 tubeAxis(0, 0, 1);
  addNoodle(p0, col0, p1, col1, tubeAxis, tubeRad, layer, shader);
}


void G8DrawList::addCatenary(
  vec3 const &srcp, vec4 const &srccol,
  vec3 const &dstp, vec4 const &dstcol,
  vec3 const &up,
  float sagDepth, float sagShape, float tubeRad,
  F layer,
  G8ShaderGeneric const *shader)
{
  auto restoreViBuf = withViBuf(1, shader);
  auto &setup = getSetup(shader);

  vec3 centerp = 0.5f * srcp + 0.5f * dstp;

  auto meshSizeQuant = round(log((dstp - srcp).norm())*5.0f);
  auto meshSize = exp(meshSizeQuant/5.0f);

  vec3 dirp = (dstp - centerp) / meshSize;
  vec3 sidep = up.cross(dirp);

  mat4 model {
    {dirp[0], sidep[0], sagDepth * up[0], centerp[0]},
    {dirp[1], sidep[1], sagDepth * up[1], centerp[1]},
    {dirp[2], sidep[2], sagDepth * up[2], centerp[2]},
    {0, 0, 0, 1}
  };

  auto restoreModelUniforms = withModelUniforms(ModelUniforms(model, tubeRad), shader);

  auto &permcacheSlot = setup.perSW->geomCache[ObjectKey("addCatenary", U64(100+meshSizeQuant))]; // FIXME: add sagShape
  if (!permcacheSlot) {
    if (0) L() << "Create catenary " << meshSize;

    const int segments = 32;
    const int nrad = 8;
    vec4 centercol = 0.5f * srccol + 0.5f * dstcol;
    vec4 scalecol = 0.5f * (dstcol - srccol);
    F coshsag = cosh(sagShape);

    vector<pair<vec3, vec4>> centerpts(segments + 1);
    for (int i = 0; i < segments + 1; i++) {
      F f0 = (F(i) / segments - 0.5f) * 2.0f;
      F z0 = ((cosh(f0 * sagShape) / coshsag) - 1.0f);
      centerpts[i].first = vec3(f0 * meshSize, 0.0, z0);
      centerpts[i].second = centercol + f0 * scalecol;
    }

    vec3 tubeX(1, 0, 0), tubeY(0, 0, 1);

    uint32_t vi = 0;
    if (setup.vt == G8Vtx_pncl::vt) {
      G8VtxBlaster<G8Vtx_pncl> vb(setup, (segments + 1) * nrad, vi);
      for (int segi = 0; segi < segments + 1; segi++) {
        vec3 dir = (centerpts[min(segments, segi+1)].first - centerpts[max(0, segi-1)].first).normalized();
        tubeX = tubeY.cross(dir);
        tubeY = dir.cross(tubeX).normalized();
        for (int thi = 0; thi < nrad; thi++) {
          F theta = F(thi) * F(M_2PI / nrad);
          vec3 normal = (cos(theta) * tubeX + sin(theta) * tubeY).normalized();
          vb(centerpts[segi].first, normal, centerpts[segi].second, layer);
        }
      }
    }
    else {
      return badShader(shader, "addCatenary", {G8Vtx_pncl::vt});
    }
    G8IdxBlaster ib(setup, 6 * segments * nrad);
    for (int segi = 0; segi < segments; segi++) {
      for (int thi = 0; thi < nrad; thi++) {
        auto vir00 = vi + (segi * nrad + thi);
        auto vir10 = vi + ((segi + 1) * nrad + thi);
        auto vir01 = vi + (segi * nrad + (thi + 1) % nrad);
        auto vir11 = vi + ((segi + 1) * nrad + (thi + 1) % nrad);
        
        ib.tripair(vir00, vir10, vir01, vir11);
      }
    }
    permcacheSlot = ib;
  }
  addCmd(setup, *permcacheSlot);
}

void G8DrawList::addLoading(vec3 const &p0, F fontSize,
  F layer,
  G8ShaderGeneric const *shader)

{
  addText(p0,
    vec3(fontSize, 0, 0), vec3(0, -fontSize, 0), vec3(0, 0, 1),
    "Loading",
    fgcolor(0xff, 0x00, 0x00, 0xcc),
    ysFonts.lrgFont,
    0.5, 0.5,
    layer, shader);
}


// -----------

void G8DrawList::webgpuRender(WGPURenderPassEncoder wgpu_pass, bool clearDepthBuffer)
{

  if (vpR == vpL || vpT == vpB) return;

  // viewport starts from lower left, and needs retina-adjusted coordinates
  auto dd = ImGui::GetDrawData();
  assert(dd);

  glVpL = vpL * dd->FramebufferScale.x;
  glVpR = vpR * dd->FramebufferScale.x;
  glVpT = vpT * dd->FramebufferScale.y;
  glVpB = vpB * dd->FramebufferScale.y;

  // .setViewport() bombs if we set anything outside the frame buffer, so clamp to bounds
  float glVpClampL = clamp(glVpL, 0.0f, dd->FramebufferScale.x * (dd->DisplayPos.x + dd->DisplaySize.x));
  float glVpClampR = clamp(glVpR, 0.0f, dd->FramebufferScale.x * (dd->DisplayPos.x + dd->DisplaySize.x));

  float glVpClampT = clamp(glVpT, 0.0f, dd->FramebufferScale.y * (dd->DisplayPos.y + dd->DisplaySize.y));
  float glVpClampB = clamp(glVpB, 0.0f, dd->FramebufferScale.y * (dd->DisplayPos.y + dd->DisplaySize.y));
  wgpuRenderPassEncoderSetViewport(wgpu_pass,
    glVpClampL, glVpClampT,
    glVpClampR - glVpClampL, glVpClampB - glVpClampT,
    0.0, 1.0);

  // but we can set the scissor rect correctly
  wgpuRenderPassEncoderSetScissorRect(wgpu_pass, 
    glVpClampL, glVpClampT,
    glVpClampR - glVpClampL, glVpClampB - glVpClampT);

  setProjView();

  int lastSetupIndex = -1;
  int lastViBufIndex = -1;
  std::array<uint32_t, MAX_G0_DYNAMIC_UNIFORMS> lastG0Data;
  std::array<uint32_t, MAX_G1_BINDINGS> lastG1Data;

  WGPUColor blend_color = { 0.0f, 0.0f, 0.0f, 0.0f };
  wgpuRenderPassEncoderSetBlendConstant(wgpu_pass, &blend_color);

  for (auto &cmdBuf : cmdBufs) {
    for (auto &cmd : cmdBuf) {
      auto &setup = setups.at(cmd.shaderIndex);

      if (cmd.shaderIndex != lastSetupIndex) {
        lastSetupIndex = cmd.shaderIndex;
        lastG0Data = cmd.g0Data;

        wgpuRenderPassEncoderSetPipeline(wgpu_pass, setup.perSW->pipeline);

        if (setup.perSW->bindGroup0 &&
            setup.perSW->bindGroup0UniformsSize != setup.perW->uniformsBuf.allocSize()) {
          wgpuBindGroupRelease(setup.perSW->bindGroup0);
          setup.perSW->bindGroup0 = nullptr;
        }

        if (!setup.perSW->bindGroup0) {
          if (0) L() << "uniformsSize " << setup.perSW->bindGroup0UniformsSize << " => " << setup.perW->uniformsBuf.allocSize();
          setup.perSW->bindGroup0 = setup.shader->mkBindGroup0(setup, win);
          setup.perSW->bindGroup0UniformsSize = setup.perW->uniformsBuf.allocSize();
        }
        for (int i = 0; i < setup.perSW->g0DynamicOffsets.count; i++) {
          assert(cmd.g0Data[i] != uint32_t(-1));
        }
        wgpuRenderPassEncoderSetBindGroup(wgpu_pass, 0, setup.perSW->bindGroup0,
          setup.perSW->g0DynamicOffsets.count, cmd.g0Data.data());
        lastG0Data = cmd.g0Data;
        lastViBufIndex = -1;
      }
      if (cmd.viBufIndex != lastViBufIndex) {
        lastViBufIndex = cmd.viBufIndex;
        wgpuRenderPassEncoderSetIndexBuffer(wgpu_pass, setup.perSW->idxGpus[lastViBufIndex].wgpu_buffer,
          WGPUIndexFormat_Uint32, 0, setup.perSW->idxBufs[lastViBufIndex].validSize());
        wgpuRenderPassEncoderSetVertexBuffer(wgpu_pass, 0, setup.perSW->vtxGpus[lastViBufIndex].wgpu_buffer,
          0, setup.perSW->vtxBufs[lastViBufIndex].validSize());
      }
      if (cmd.g0Data != lastG0Data) {
        lastG0Data = cmd.g0Data;
        for (int i = 0; i < setup.perSW->g0DynamicOffsets.count; i++) {
          assert(cmd.g0Data[i] != uint32_t(-1));
        }
        wgpuRenderPassEncoderSetBindGroup(wgpu_pass, 0, setup.perSW->bindGroup0,
          setup.perSW->g0DynamicOffsets.count, cmd.g0Data.data());
      }

      if (setup.shader->g1Binding.imgTex != -1 || 
          setup.shader->g1Binding.traceData != -1 ||
          setup.shader->g1Binding.traceVisInfo != -1) {
        lastG1Data = cmd.g1Data;

        auto bg1 = setup.shader->mkBindGroup1(setup, win, cmd.g1Data);
        wgpuRenderPassEncoderSetBindGroup(wgpu_pass, 1, bg1,
          0, nullptr);
        setup.perSW->ephemeralBindGroups.push_back(bg1);
      }

      assertlog(cmd.elemIndex + cmd.elemCount <= setup.perSW->idxBufs[lastViBufIndex].validSize() / sizeof(uint32_t),
        LOGV(cmd.elemIndex) << LOGV(cmd.elemCount) <<
        LOGV(setup.perSW->idxBufs[lastViBufIndex].validSize() / sizeof(uint32_t)));

      wgpuRenderPassEncoderDrawIndexed(wgpu_pass, 
        cmd.elemCount, 1, cmd.elemIndex, 0, 0);
    }
  }
}

void G8DrawList::camFov(float target)
{
  if (isnan(target)) {
    L() << "G8DrawList::camFov got nan: " << fov;
    return;
  }
  float dir = target - fov;
  float dirnorm = abs(dir);
  if (dirnorm < 0.00001f) return;
  fov += dir * min(1.0f, dt * max(6.0f, 0.05f/dirnorm));
  uiPollLive = true;
}

void G8DrawList::camEyepos(vec3 target)
{
  if (isnan(target.x()) || isnan(target.y()) || isnan(target.z())) {
    L() << "G8DrawList::camEyepos got nan: " << target;
    return;
  }
  auto dir = target - eyepos;
  auto dirnorm = dir.norm();
  if (dirnorm < 0.001f) return;
  eyepos += dir * min(1.0f, dt * max(10.0f, 0.1f/dirnorm));
  uiPollLive = true;
}

void G8DrawList::camLookat(vec3 target)
{
  if (isnan(target.x()) || isnan(target.y()) || isnan(target.z())) {
    L() << "G8DrawList::camLookat got nan: " << target;
    return;
  }
  auto dir = target - lookat;
  auto dirnorm = dir.norm();
  if (dirnorm < 0.001f) return;
  lookat += dir * min(1.0f, dt * max(10.0f, 0.1f/dirnorm));
  uiPollLive = true;
}

void G8DrawList::camStrafe(vec3 dir)
{
  if (isnan(dir.x()) || isnan(dir.y()) || isnan(dir.z())) {
    L() << "G8DrawList::camStrafe got nan: " << dir;
    return;
  }
  if (dir.squaredNorm() < 1e-8f) return;
  eyepos += dir * dt;
  lookat += dir * dt;
  uiPollLive = true;
}

void G8DrawList::camHoverFly(vec3 dir)
{
  if (isnan(dir.x()) || isnan(dir.y()) || isnan(dir.z())) {
    L() << "G8DrawList::camHoverFly got nan: " << dir;
    return;
  }
  if (dir.squaredNorm() < 1e-8f) return;
  eyepos += dir * dt;
  lookat.x() += dir.x() * dt;
  lookat.y() += dir.y() * dt;
  lookat.z() = 0.0f;
  uiPollLive = true;
}


void G8DrawList::camFlyHome()
{
  auto dir = homeLookat - lookat;
  auto dirnorm = dir.norm();
  if (dirnorm < 0.001f) return;

  vec3 oldEyeToLook = lookat - eyepos;
  lookat += dir * min(1.0f, dt * max(5.0f, 0.1f/dirnorm));

  vec3 newEyeToLook = lookat - eyepos;

  auto oldEyeToLookXY = hypot(oldEyeToLook.x(), oldEyeToLook.y());
  auto newEyeToLookXY = hypot(newEyeToLook.x(), newEyeToLook.y());

  auto moveXY = oldEyeToLookXY - newEyeToLookXY;

  vec3 newEyepos(
    eyepos.x() - moveXY/oldEyeToLookXY * oldEyeToLook.x(),
    eyepos.y() - moveXY/oldEyeToLookXY * oldEyeToLook.y(),
    eyepos.z());

  eyepos = newEyepos;

  uiPollLive = true;
}


void G8DrawList::camPan(vec2 dir)
{
  if (isnan(dir.x()) || isnan(dir.y())) {
    L() << "G8DrawList::camPan got nan: " << dir;
    return;
  }
  if (abs(dir.x()) + abs(dir.y()) < 0.0001f) return;


  mat3 tx(AngleAxis<F>(+0.05f*dir.x(), vec3::UnitZ()));
  mat3 ty(AngleAxis<F>(-0.05f*dir.y(), vec3::UnitX()));

  lookat = eyepos + tx * (lookat - eyepos) + ty * (lookat - eyepos);
  
  uiPollLive = true;
}


void G8DrawList::camOrbitElevate(float orbit, float elevate)
{
  if (isnan(orbit) || isnan(elevate)) {
    L() << "G8DrawList::camOrbitElevate got nan: " << orbit << ", " << elevate;
    return;
  }
  vec3 lookToEye = eyepos - lookat;
  eyepos = lookat + AngleAxis<F>(-0.2f * orbit, vec3::UnitZ()) * lookToEye;

  lookToEye = eyepos - lookat;
  auto side = lookToEye.cross(up);
  auto updog = lookToEye.normalized().dot(up);
  if (updog > 0.95f && elevate > 0) return;
  if (updog < -0.95f && elevate < 0) return;
  vec3 newLookToEye = AngleAxis<F>(0.1f * elevate, side) * lookToEye;

  eyepos = newLookToEye + lookat;
  uiPollLive = true;
}

// -------------

void G8DrawList::setProjView()
{
  calcProjView(*this);
  iproj = proj.inverse();
  iview = view.inverse();
}

F G8DrawList::pixelSize(vec3 pos, F thick)
{
  vec4 tpos = proj * (view * pos.homogeneous());
  vec3 tvelx = proj.topLeftCorner<3, 3>() * (view.topLeftCorner<3, 3>() * vec3(1, 0, 0));
  vec3 tvely = proj.topLeftCorner<3, 3>() * (view.topLeftCorner<3, 3>() * vec3(0, 1, 0));
  const F scale = 0.25f;
  auto slopex = hypot(tvelx.x(), tvely.x()) / tpos.w();
  auto slopey = hypot(tvelx.y(), tvely.y()) / tpos.w();
  auto ret = scale * thick * hypot(
    slopex * (glVpR - glVpL),
    slopey * (glVpB - glVpT));
  if (0) L() << "pixelSize pos=" << pos << " thick=" << thick <<
    "\n tpos=" << tpos <<
    "\n tvelx=" << tvelx << " tvely=" << tvely <<
    "\n slopex=" << slopex << " slopey=" << slopey <<
    "\n pixelSize=" << ret;
  return ret;
}

F G8DrawList::sizeForPixels(vec3 pos, F numPixels)
{
  return numPixels / pixelSize(pos, 1.0f);
}

ImVec2 G8DrawList::projectToScreen(vec3 pos)
{
  vec3 projected = (proj * (view * pos.homogeneous())).hnormalized();

  return ImVec2(
    vpL + (0.5f + 0.5f * projected.x()) * (vpR - vpL),
    vpB + (0.5f + 0.5f * projected.y()) * (vpT - vpB));
}

vector<tuple<vec3, vec3>> G8DrawList::screenCornerRays()
{
  vector<tuple<vec3, vec3>> ret;

  for (auto sy : {-1.0f, 0.0f, 1.0f}) {
    for (auto sx : {-1.0f, 0.0f, 1.0f}) {

      vec4 nearFrustPos = vec4(sx, sy, 0.0, 1.0);
      vec4 farFrustPos = vec4(sx, sy, 1.0, 1.0);
      vec3 nearEyeRay = (iproj * nearFrustPos).hnormalized();
      vec3 farEyeRay = (iproj * farFrustPos).hnormalized();

      vec3 nearWorldPos = (iview * nearEyeRay.homogeneous()).hnormalized();
      vec3 farWorldPos = (iview * farEyeRay.homogeneous()).hnormalized();

      vec3 dir = (farWorldPos - nearWorldPos).normalized();
      vec3 origin = nearWorldPos;

      ret.emplace_back(origin, dir);
    }
  }
  return ret;
}

bool intersectRayPlaneSym(vec3 orig, vec3 dir, vec3 planeOrig, vec3 planeNormal, vec3 &hit, F &distance)
{
  auto d = dir.dot(planeNormal);
  if (d > 0.0f) {
    d = -d;
    planeNormal = -planeNormal;
  }
  if (d < -std::numeric_limits<F>::epsilon()) {
    distance = (planeOrig - orig).dot(planeNormal) / d;
    hit = orig + distance * dir;
    return true;
  }
  distance = 0.0f;
  hit = vec3(0, 0, 0);
  return false;
}

vector<vec3> G8DrawList::planeVisibleRegion(vec3 planeOrig, vec3 planeNormal)
{
  vector<vec3> ret;
  for (auto [orig, dir] : screenCornerRays()) {
    F distance;
    vec3 hit;
    if (intersectRayPlaneSym(orig, dir, planeOrig, planeNormal, hit, distance)) {
      ret.push_back(hit);
    }
  }
  return ret;
}

void G8DrawList::clearPickRay()
{
  pickRayActive = false;
}

void G8DrawList::setPickRay()
{
  pickRayActive = false;
  auto &io = ImGui::GetIO();
  if (io.MousePos.x >= vpL && io.MousePos.x <= vpR && io.MousePos.y >= vpT && io.MousePos.y <= vpB &&
      vpR > vpL && vpB > vpT) {
    float pickX = (io.MousePos.x - vpL) / float(vpR - vpL) * 2.0f - 1.0f;
    float pickY = (io.MousePos.y - vpB) / float(vpT - vpB) * 2.0f - 1.0f;
    if (VERBOSE) L() << "pick = " << pickX << "," << pickY;

    /*
      We're undoing this projection (from lit_solid.glsl):
        gl_Position = proj * view * vec4(pos, 1);
      Since we only have a 2D screen coordinate, we unproject twice: once at
      z=0 and once at z=1. This gives us a line that intersects anything on the
      screen at that point.
    */

    vec4 nearFrustPos = vec4(pickX, pickY, 0.0, 1.0);
    vec4 farFrustPos = vec4(pickX, pickY, 1.0, 1.0);
    vec3 nearEyeRay = (iproj * nearFrustPos).hnormalized();
    vec3 farEyeRay = (iproj * farFrustPos).hnormalized();

    vec3 nearWorldPos = (iview * nearEyeRay.homogeneous()).hnormalized(); // scale?
    vec3 farWorldPos = (iview * farEyeRay.homogeneous()).hnormalized();

    pickRayDir = (farWorldPos - nearWorldPos).normalized();
    pickRayOrigin = nearWorldPos;
    pickRayActive = true;

    if (VERBOSE) {
      L() <<
        " proj  = " << proj << "\n" <<
        " iproj = " << iproj << "\n" <<
        " view  = " << view << "\n" <<
        " iview = " << iview << "\n" <<
        " nearFrustPos = " << nearFrustPos << "\n" <<
        " farFrustPos = " << farFrustPos << "\n" <<
        " nearEyeRay = " << nearEyeRay << "\n" <<
        " farEyeRay = " << farEyeRay << "\n" <<
        " nearWorldPos = " << nearWorldPos << "\n" <<
        " farWorldPos = " << farWorldPos << "\n" <<
        " pickRayOrigin = " << pickRayOrigin << 
        " pickRayDir = " << pickRayDir << "\n";
    }
  }
  else {
    if (0) L() << "Mouse " << io.MousePos.x << "," << io.MousePos.y << " outside " << 
      vpL << "-" << vpR << " , " << vpT << "-" << vpB << "\n";
  }
}

bool G8DrawList::hitTestTriangle(vec3 const &a, vec3 const &b, vec3 const &c, vec3 &hit, float &distance)
{
  if (!pickRayActive) return false;

  vec2 bary;
  if (intersectRayTriangle(pickRayOrigin, pickRayDir, a, b, c, bary, distance)) {
    hit = pickRayOrigin + distance * pickRayDir;
    return true;
  }

  return false;
}


bool G8DrawList::hitTestQuad(vec3 const &a, vec3 const &b, vec3 const &c, vec3 const &d, vec3 &hit, float &distance)
{
  if (!pickRayActive) return false;

  vec2 bary;
  if (intersectRayTriangle(pickRayOrigin, pickRayDir, a, b, c, bary, distance)) {
    hit = pickRayOrigin + distance * pickRayDir;
    return true;
  }
  if (intersectRayTriangle(pickRayOrigin, pickRayDir, b, d, c, bary, distance)) {
    hit = pickRayOrigin + distance * pickRayDir;
    return true;
  }

  return false;
}


bool G8DrawList::hitTestPlane(vec3 const &planeOrig, vec3 const &planeNormal, vec3 &hit, float &distance)
{
  if (!pickRayActive) return false;

  return intersectRayPlaneSym(pickRayOrigin, pickRayDir, planeOrig, planeNormal, hit, distance);
}

void G8DrawList::firstPersonControls()
{
  bool shift = ImGui::IsKeyDown(ImGuiKey_LeftShift) || ImGui::IsKeyDown(ImGuiKey_RightShift);
  bool cmd = ImGui::IsKeyDown(ImGuiKey_LeftSuper) || ImGui::IsKeyDown(ImGuiKey_RightSuper);
  bool alt = ImGui::IsKeyDown(ImGuiKey_LeftAlt) || ImGui::IsKeyDown(ImGuiKey_RightAlt);

  if (cmd || alt) return;

  float speed = shift ? 0.01 : 1.0;

  vec3 strafe(0, 0, 0);
  if (ImGui::IsKeyDown(ImGuiKey_W) || ImGui::IsKeyDown(ImGuiKey_UpArrow)) {
    strafe += speed * (lookat - eyepos).normalized();
  }
  if (ImGui::IsKeyDown(ImGuiKey_A) || ImGui::IsKeyDown(ImGuiKey_LeftArrow)) {
    mat3 rot(AngleAxis<F>(M_PI_2, vec3::UnitZ()));
    strafe += speed * rot * (lookat - eyepos).normalized();
  }
  if (ImGui::IsKeyDown(ImGuiKey_S) || ImGui::IsKeyDown(ImGuiKey_DownArrow)) {
    strafe -= speed * (lookat - eyepos).normalized();
  }
  if (ImGui::IsKeyDown(ImGuiKey_D) || ImGui::IsKeyDown(ImGuiKey_RightArrow)) {
    mat3 rot(AngleAxis<F>(M_PI_2, vec3::UnitZ()));
    strafe -= speed * rot * (lookat - eyepos).normalized();
  }
  if (ImGui::IsKeyDown(ImGuiKey_E)) {
    strafe += speed * vec3(0, 0, 1);
  }
  if (ImGui::IsKeyDown(ImGuiKey_C)) {
    strafe -= speed * vec3(0, 0, 1);
  }
  camStrafe(strafe);

  ImGuiIO& io = ImGui::GetIO();
  if (io.MouseWheelH != 0.0f || io.MouseWheel != 0.0f) {
    camPan(vec2(io.MouseWheelH, io.MouseWheel));
  }

  camFov(ImGui::IsKeyDown(ImGuiKey_Q) ? float(5.0 * M_PI/180.0) : float(40.0 * M_PI/180.0));

  if (ImGui::IsKeyDown(ImGuiKey_X)) {
    camEyepos(vec3(shift ? -1 : 1, 0, 0));
    camLookat(vec3(0, 0, 0));
  }
  if (ImGui::IsKeyDown(ImGuiKey_Y)) {
    camEyepos(vec3(0, shift ? -1 : 1, 0));
    camLookat(vec3(0, 0, 0));
  }
  if (ImGui::IsKeyDown(ImGuiKey_Z)) {
    camEyepos(vec3(0, 0, shift ? -1 : 1));
    camLookat(vec3(0, 0, 0));
  }
  if (ImGui::IsKeyPressed(ImGuiKey_H)) {
    camHome();
  }
  camCheckSanity();
}



void G8DrawList::hoverControls()
{
  bool shift = ImGui::IsKeyDown(ImGuiKey_LeftShift) || ImGui::IsKeyDown(ImGuiKey_RightShift);
  bool cmd = ImGui::IsKeyDown(ImGuiKey_LeftSuper) || ImGui::IsKeyDown(ImGuiKey_RightSuper);
  bool alt = ImGui::IsKeyDown(ImGuiKey_LeftAlt) || ImGui::IsKeyDown(ImGuiKey_RightAlt);

  if (cmd || alt) return;

  float speed = shift ? 0.01 : 1.0;
  speed *= max(1.0f, eyepos.z());

  auto fwdDir = (lookat - eyepos).normalized();
  fwdDir.z() = 0.0;
  mat3 rot(AngleAxis<F>(M_PI_2, vec3::UnitZ()));
  vec3 rightDir = rot * fwdDir;

  vec3 strafe(0, 0, 0);
  if (ImGui::IsKeyDown(ImGuiKey_W) || ImGui::IsKeyDown(ImGuiKey_UpArrow)) {
    strafe += speed * fwdDir;
  }
  if (ImGui::IsKeyDown(ImGuiKey_A) || ImGui::IsKeyDown(ImGuiKey_LeftArrow)) {
    strafe += speed * rightDir;
  }
  if (ImGui::IsKeyDown(ImGuiKey_S) || ImGui::IsKeyDown(ImGuiKey_DownArrow)) {
    strafe += -speed * fwdDir;
  }
  if (ImGui::IsKeyDown(ImGuiKey_D) || ImGui::IsKeyDown(ImGuiKey_RightArrow)) {
    strafe += -speed * rightDir;
  }
  if (ImGui::IsKeyDown(ImGuiKey_E)) {
    strafe += speed * vec3(0, 0, 0.75f);
  }
  if (ImGui::IsKeyDown(ImGuiKey_C)) {
    strafe += speed * vec3(0, 0, -0.75f);
  }

  camHoverFly(strafe);

  ImGuiIO& io = ImGui::GetIO();

  if (ImGui::IsKeyDown(ImGuiKey_Q)) {
    if (!saveDirActive) {
      saveDirActive = true;
      saveDir = lookat - eyepos;
    }
    camFov(5.0 * M_PI/180.0);
    if (io.MouseWheelH != 0.0f || io.MouseWheel != 0.0f) {
      camPan(vec2(0.1f * io.MouseWheelH, 0.1f * io.MouseWheel));
    }
  }
  else {
    camFov(homeFov);
    if (io.MouseWheelH != 0.0f || io.MouseWheel != 0.0f) {
      camOrbitElevate(io.MouseWheelH, io.MouseWheel);
    }
    if (saveDirActive) {
      auto revertLookat = eyepos + saveDir;
      if ((revertLookat - lookat).squaredNorm() < 1e-6f) {
        saveDirActive = false;
      }
      else {
        camLookat(revertLookat);
      }
    }
  }

  if (ImGui::IsKeyDown(ImGuiKey_H)) {
    camFlyHome();
  }
  camCheckSanity();
}




void G8PickList::add(
  vec3 const &a, vec3 const &b, vec3 const &c, vec3 const &d,
  F layer,
  function<void (bool picked)> draw,
  function<void (vec3 const &hit)> onClick)
{
  vec3 hit;
  F distance = 0;
  if (dl.hitTestQuad(a, b, c, d, hit, distance)) {
    items.push_back(PickItem{distance - layer, hit, draw, onClick});
  }
  else {
    items.push_back(PickItem{FLT_MAX, hit, draw, nullptr});
  }
}

void G8PickList::addText(
  vec3 const &_curPos, vec3 const &xDir, vec3 const &yDir, vec3 const &norm,
  string_view text,
  vec4 const &col,
  ImFont *font,
  F xJust, F yJust,
  F layer,
  function<void (vec3 const &hit)> onClick)
{
  vec3 curPos = _curPos;
  ImVec2 textSize = font->CalcTextSizeA(1.0f, FLT_MAX, -1.0f, text.data(), text.data() + text.size());
  if (xJust != 0.0f || yJust != 0.0f) {
    curPos -= xDir * xJust * textSize[0] + yDir * yJust * textSize[1];
  }

  vec3 a = curPos + xDir * -0.3f + yDir * (textSize.y + 0.15f);
  vec3 b = curPos + xDir * (textSize.x + 0.3f) + yDir * (textSize.y + 0.15f);
  vec3 c = curPos + xDir * -0.3f + yDir * -0.15f;
  vec3 d = curPos + xDir * (textSize.x + 0.3f) + yDir * -0.15f;

  add(a, b, c, d, layer,
    [this, a, b, c, d, curPos, xDir, yDir, norm, texts=string(text), col, font, layer](bool picked) {
      if (picked) {
        dl.addQuad(a, pickedBgCol, b, pickedBgCol, c, pickedBgCol, d, pickedBgCol,
          layer, &glowingTextShader);
      }
      dl.addText(curPos, xDir, yDir, norm, texts, col, font, 0.0, 0.0,
        layer, &glowingTextShader);
    },
    onClick);

}

void G8PickList::finish()
{
  F bestDistance = FLT_MAX;
  PickItem *bestItem = nullptr;
  for (auto &it : items) {
    if (it.distance < bestDistance) {
      bestDistance = it.distance;
      bestItem = &it;
    }
  }
  for (auto &it : items) {
    if (&it != bestItem) it.draw(false);
  }
  if (bestItem) {
    bestItem->draw(true);
    if (IsMouseClicked(0, false)) {
      bestItem->onClick(bestItem->hit);
    }
  }
}
