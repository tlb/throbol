#pragma once
#include "./geom.h"

extern bool darkenFg;
extern bool lightenBg;

inline vec4 hexcolor(U32 r, U32 g, U32 b, U32 a=0xff)
{
  return vec4{r/255.0f, g/255.0f, b/255.0f, a/255.0f};
}

vec4 fgcolor(vec4 const &base);
vec4 bgcolor(vec4 const &base);
inline vec4 fgcolor(U32 r, U32 g, U32 b, U32 a=0xff) { return fgcolor(hexcolor(r,g,b,a)); }
inline vec4 bgcolor(U32 r, U32 g, U32 b, U32 a=0xff) { return bgcolor(hexcolor(r,g,b,a)); }

constexpr int N_goodGraphColors = 38;

vec4 goodGraphColor(size_t i);

#if 0
/*
  Nice idea, but doesn't work because vec4 has no constexpr constructors
*/
constexpr inline bool rgba_isHexDigit(char c)
{
  return (c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F');
}

constexpr inline int rgba_fromHexDigit(char c)
{
  if (c < 0) return 0;
  if (c >= '0' && c <= '9') return (int)(c - '0');
  if (c >= 'a' && c <= 'f') return (int)(c - 'a') + 10;
  if (c >= 'A' && c <= 'F') return (int)(c - 'A') + 10;
  return 0;
}

constexpr inline vec4 operator "" _rgba(const char *data, size_t size)
{
  if (size == 8 &&
      rgba_isHexDigit(data[0]) && rgba_isHexDigit(data[1]) && 
      rgba_isHexDigit(data[2]) && rgba_isHexDigit(data[3]) && 
      rgba_isHexDigit(data[4]) && rgba_isHexDigit(data[5]) && 
      rgba_isHexDigit(data[6]) && rgba_isHexDigit(data[7])) {
    return vec4(
      rgba_fromHexDigit(data[0]) / 16.0  + rgba_fromHexDigit(data[1]) / 256.0,
      rgba_fromHexDigit(data[2]) / 16.0  + rgba_fromHexDigit(data[3]) / 256.0,
      rgba_fromHexDigit(data[4]) / 16.0  + rgba_fromHexDigit(data[5]) / 256.0,
      rgba_fromHexDigit(data[6]) / 16.0  + rgba_fromHexDigit(data[7]) / 256.0
    );
  }
  throw runtime_error("Unknown RGBA format");
}
#endif
