#include "../src/defs.h"
#include "./gl_headers.h"
extern "C" {
#include <png.h>
}
#include <dirent.h>

bool showDebugUi;

YsFontDir ysFonts;

bool SmallCheckbox(const char* label, bool* v)
{
  ImGuiContext& g = *GImGui;
  float savePaddingY = g.Style.FramePadding.y;
  g.Style.FramePadding.y = 0.0f;
  bool pressed = Checkbox(label, v);
  g.Style.FramePadding.y = savePaddingY;
  return pressed;
}

bool SelectablePopupMenu(string const &menuLabel)
{
  auto menuFlags = ImGuiSelectableFlags_NoHoldingActiveID | ImGuiSelectableFlags_SelectOnClick | ImGuiSelectableFlags_SpanAvailWidth;
  if (Selectable(menuLabel.c_str(), IsPopupOpen(menuLabel.c_str()), menuFlags)) {
    OpenPopup(menuLabel.c_str());
  }
  return BeginPopup(menuLabel.c_str());
}



PolylineBuf::PolylineBuf()
{
}

PolylineBuf::~PolylineBuf()
{
}

void PolylineBuf::reserve(size_t n)
{
  pts.reserve(n);
}

void PolylineBuf::draw(U32 color, bool closed, float width)
{
  auto dl = GetWindowDrawList();

  size_t i = 0;
  while (i + 1 < pts.size()) {

    /*
      AddPolyline doesn't handle acute angles well, leading to vanishingly thin parts
      on either side of a sharp angle. We avoid this by doing multiple calls to
      AddPolyline, breaking when there's an acute angle which we detect by a negative
      dot product between the two lines.
    */
    int nlin = 2;
    while (i + nlin < pts.size()) {
      auto [x0, y0] = pts[i+nlin-2];
      auto [x1, y1] = pts[i+nlin-1];
      auto [x2, y2] = pts[i+nlin];
      auto s0x = x1-x0, s0y = y1-y0; 
      auto s1x = x2-x1, s1y = y2-y1;
      auto dotprod = s1x*s0x + s1y*s0y;
      if (dotprod < 0) break;
      nlin ++;
    }

    dl->AddPolyline(&pts[i], nlin, color, false, width);
    i += nlin-1;
  }
}

PolybandBuf::PolybandBuf()
{
}

PolybandBuf::~PolybandBuf()
{
}

void PolybandBuf::reserve(size_t n)
{
  pts.reserve(n);
}

void PolybandBuf::fill(U32 color)
{
  if (pts.size() < 2) return;
  auto dl = GetWindowDrawList();

  dl->PrimReserve(6 * (pts.size() - 1), 4 * (pts.size() - 1));

  ImVec2 uv(dl->_Data->TexUvWhitePixel);
  for (size_t i = 0; i + 1 < pts.size(); i++) {
    dl->PrimQuadUV(
      ImVec2(pts[i].x, pts[i].y1),
      ImVec2(pts[i+1].x, pts[i+1].y1),
      ImVec2(pts[i+1].x, pts[i+1].y0),
      ImVec2(pts[i].x, pts[i].y0),
      uv, uv, uv, uv,
      color);
  }
}

void drawText(float x, float y, U32 color, string const &text, float hAlign, float vAlign)
{
  if (hAlign != 0.0f) {
    auto size = CalcTextSize(text.c_str());
    x -= hAlign * size.x;
    y -= vAlign * size.y;
  }
  else if (vAlign != 0.0f) {
    y -= vAlign * GetTextLineHeight();
  }
  GetWindowDrawList()->AddText(ImVec2(x, y), color, text.c_str());
}

bool drawWinopIcon(float &x, float y, U32 color, char const *label)
{
  bool ret = false;
  ImGuiContext& g = *GImGui;
  GetWindowDrawList()->AddText(ImVec2(x, y), color, label);

  if (IsMouseHoveringRect(ImVec2(x-2, y), ImVec2(x+GetFontSize(), y+GetTextLineHeight()))) {
    if (g.IO.MouseClicked[0]) {
      ret = true;
    }
  }
  x += 1.25f * GetFontSize();
  return ret;
}

shared_ptr<GLFWimage> readGlfwImagePng(string const &fn)
{
  png_image png;

  memset(&png, 0, sizeof(png));
  png.version = PNG_IMAGE_VERSION;
  png.opaque = nullptr;

  if (png_image_begin_read_from_file(&png, fn.c_str()) < 0) {
    L() << "PNG begin-read " << shellEscape(fn) << " failed: " << png.warning_or_error;
    return nullptr;
  }
  png.format = PNG_FORMAT_RGBA;

  size_t rowStride = png.width * 4;
  size_t allocSize = png.height * rowStride + sizeof(GLFWimage);

  auto mem = (u_char *)malloc(allocSize);
  auto ret = new(mem) GLFWimage();
  
  ret->width = png.width;
  ret->height = png.height;
  ret->pixels = mem + sizeof(GLFWimage);

  png_image_finish_read(&png, nullptr, ret->pixels, rowStride, nullptr);
  if (png.warning_or_error) {
    L() << "PNG read " << shellEscape(fn) << " failed: " << png.warning_or_error;
    return nullptr;
  }

  png_image_free(&png);

  return shared_ptr<GLFWimage>(ret);
}


struct YogaFontIcon {
  
  string fn;
  int codePoint = 0;
  ImFont *font = nullptr;
  png_image png;
  Blob pngPixels;
  int rectId = 0;
  ImFontAtlasCustomRect const *dst = nullptr;

  YogaFontIcon() = default;
  YogaFontIcon(YogaFontIcon &&other) = default;
  YogaFontIcon(YogaFontIcon const &other) = delete;
};

vector<YogaFontIcon> ysIcons;

void loadPngForFont(ImFont *font, string const &fn, int codePoint)
{
  ImGuiIO& io = GetIO();

  FILE *fp = fopen(fn.c_str(), "r");
  if (!fp) {
    L() << "Warning: no " << fn;
    return;
  }

  YogaFontIcon icon;
  icon.fn = fn;
  icon.codePoint = codePoint;
  icon.font = font;
  memset(&icon.png, 0, sizeof(icon.png));
  icon.png.version = PNG_IMAGE_VERSION;
  icon.png.opaque = nullptr;

  if (png_image_begin_read_from_stdio(&icon.png, fp) < 0) {
    L() << "PNG begin-read " << shellEscape(icon.fn) << " failed: "  << icon.png.warning_or_error;
    return;
  }
  icon.png.format = PNG_FORMAT_RGBA;

  U32 rowStride = icon.png.width * sizeof(U32);
  icon.pngPixels.alloc(icon.png.height * rowStride);
  void *colormap = nullptr;

  png_image_finish_read(&icon.png, nullptr, icon.pngPixels.data(), rowStride, colormap);
  if (icon.png.warning_or_error) {
    L() << "PNG read " << shellEscape(icon.fn) << " failed: " << icon.png.warning_or_error;
    fclose(fp);
    return;
  }
  fclose(fp);

  icon.rectId = io.Fonts->AddCustomRectFontGlyph(font, icon.codePoint,
    (int)icon.png.width, (int)icon.png.height, (float)icon.png.width,
    ImVec2(0.0, 0.0));

  ysIcons.emplace_back(std::move(icon));
}

void blitIconsIntoFont()
{
  ImGuiIO& io = GetIO();
  U8 *texPixels = nullptr;
  int texWidth = 0, texHeight = 0, texBytesPerPixel = 0;
  io.Fonts->GetTexDataAsRGBA32(&texPixels, &texWidth, &texHeight, &texBytesPerPixel);
  if (0) L() << "Font texture " << texWidth << "x" << texHeight <<
    "x" << texBytesPerPixel*8;

  for (auto &icon : ysIcons) {
    icon.dst = io.Fonts->GetCustomRectByIndex(icon.rectId);
    assert(icon.png.width >= icon.dst->Width);
    assert(icon.png.height >= icon.dst->Height);

    if (0) L() << "Font character 0x" << repr_04x(icon.codePoint) <<
      " at " << icon.dst->X << "," << icon.dst->Y <<
      icon.dst->Width << "x" << icon.dst->Height;

    for (int y = 0; y < icon.dst->Height; y++) {
      ImU32* dstStart = reinterpret_cast<ImU32*>(texPixels) + (icon.dst->Y + y) * texWidth + icon.dst->X;
      U8 *srcStart = reinterpret_cast<U8 *>(&icon.pngPixels[y * icon.png.width * sizeof(U32)]);
      for (int x = 0; x < icon.dst->Width; x++) {
        U32 r = srcStart[x*4+0];
        U32 g = srcStart[x*4+1];
        U32 b = srcStart[x*4+2];
        U32 a = srcStart[x*4+3];
        dstStart[x] = IM_COL32(r, g, b, a);
      }
    }
  }
  ysIcons.clear();
}

ImFont *addFontWithIcons(string fontFn, string iconFn, float size)
{
  ImGuiIO& io = GetIO();
#if 0 // these ttf files don't have anything beyond iso-latin
  static const ImWchar text_ranges[] = {
    0x0020, 0x00ff,
    0x2300, 0x23ff, // misc technical
    0
  };

  auto font = io.Fonts->AddFontFromFileTTF(fontFn.c_str(), size, nullptr, &text_ranges[0]);
#else
  auto font = io.Fonts->AddFontFromFileTTF(fontFn.c_str(), size);
#endif

#if 1
  if (!iconFn.empty()) {
    ImFontConfig config;
    config.MergeMode = true;
    config.GlyphMinAdvanceX = size; // Use if you want to make the icon monospaced
    static const ImWchar icon_ranges[] = { 0xf000, 0xfa00, 0 };
    io.Fonts->AddFontFromFileTTF(iconFn.c_str(), size, &config, icon_ranges);
  }
#endif

  if (0) L() << "Loading fontFn=" + fontFn + " iconFn=" + iconFn + " size=" + repr(size);

  /*
    Load every file matching images/<code point>@<size>.png
  */
  auto imgDir = installDir + "/images";
  if (auto dir = opendir(imgDir.c_str())) {
    auto suffix = "@h" + repr(size) + ".png";
    while (auto dp = readdir(dir)) {
      string ent(dp->d_name);
      if (endsWith(ent, suffix)) {
        char *endp = nullptr;
        auto codePoint = strtol(ent.c_str(), &endp, 16);
        if (codePoint >=0 && *endp == '@') {
          auto fn = imgDir + "/" + ent;
          loadPngForFont(font, fn, codePoint);
        }
      }
    }
    closedir(dir);
  }

  return font;
};




ostream & operator << (ostream &s, ImVec2 const &a)
{
  s << "ImVec2("s << a.x << "," << a.y << ")";
  return s;
}

ostream & operator <<(ostream &s, ImRect const &a)
{
  s << "ImRect("s << a.Min.x << 
    "," << a.Min.y <<
    " to " << a.Max.x <<
    "," << a.Max.y << ")";
  return s;
}
