#pragma once
#include "../geom/geom.h"

// returns pairs of (character, byte offset).
// Includes a fake terminating null, ie [0, s.size()]
vector<pair<char32_t, U32>> vbParseUtf8(string_view s);
