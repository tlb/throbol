#include "./gl_buffer.h"

uint64_t G8MemBuffer::globalEpoch = 123;


G8GpuBuffer::G8GpuBuffer(WGPUDevice dev, char const *_label, WGPUBufferUsageFlags _usage, size_t want)
  : label(_label),
    usage(_usage)
{
  realloc(dev, want);
}

G8GpuBuffer::G8GpuBuffer(G8GpuBuffer &&other) noexcept
  : AllocTrackingMixin<G8GpuBuffer>(std::move(other)),
    label(other.label),
    wgpu_buffer(other.wgpu_buffer),
    usage(other.usage),
    buffer_size(other.buffer_size),
    valid_size(other.valid_size),
    epoch(other.epoch)
{
  other.wgpu_buffer = nullptr;
  other.buffer_size = 0;
  other.valid_size = 0;
}

G8GpuBuffer::~G8GpuBuffer()
{
  if (wgpu_buffer) {
    wgpuBufferDestroy(wgpu_buffer);
    wgpuBufferRelease(wgpu_buffer);
    wgpu_buffer = nullptr;
  }
}

void G8GpuBuffer::realloc(WGPUDevice dev, size_t want)
{
  if (buffer_size < want) {
    if (wgpu_buffer) {
      wgpuBufferDestroy(wgpu_buffer);
      wgpuBufferRelease(wgpu_buffer);
      wgpu_buffer = nullptr;
    }
    WGPUBufferDescriptor desc = {
      .usage = usage,
      .size = want
    };
    wgpu_buffer = wgpuDeviceCreateBuffer(dev, &desc);
    if (!wgpu_buffer) throw runtime_error("wgpuDeviceCreateBuffer failed");
    buffer_size = want;
    epoch = -1;
    valid_size = 0;
  }
}



void G8GpuBuffer::sync(WGPUDevice dev, WGPUQueue queue, G8MemBuffer &mem)
{
  if (epoch != mem.epoch) {
    epoch = mem.epoch;
    valid_size = 0;
  }
  if (valid_size > mem.validSize()) {
    throw runtime_error("G8GpuBuffer shrunk?");
  }

  if (valid_size < mem.validSize()) {
    realloc(dev, mem.allocSize());
    wgpuQueueWriteBuffer(queue, wgpu_buffer, 
      valid_size, 
      mem.begin + valid_size,
      mem.validSize() - valid_size);
    valid_size = mem.validSize();
  }
}



G8MemBuffer::G8MemBuffer()
{
  epoch = globalEpoch++;
}

G8MemBuffer::~G8MemBuffer()
{
  if (begin) {
    free(begin);
  }
  begin = nullptr;
  end = nullptr;
  cur = nullptr;
}

G8MemBuffer::G8MemBuffer(G8MemBuffer &&other)
  : AllocTrackingMixin<G8MemBuffer>(std::move(other)),
    cur(other.cur),
    begin(other.begin),
    end(other.end)
{
  other.cur = nullptr;
  other.begin = nullptr;
  other.end = nullptr;
}

void G8MemBuffer::need(size_t maxAdd)
{
  if (cur + maxAdd > end) {
    auto oldSize = size_t(end - begin);
    size_t newSize = max(size_t(0), max(cur - begin + maxAdd, 2*oldSize));
    U8 *newBegin = (U8 *)malloc(newSize);
    if (begin) {
      memcpy(newBegin, begin, cur - begin);
      free(begin);
    }
    auto newCur = newBegin + (cur - begin);
    auto newEnd = newBegin + newSize;

    begin = newBegin;
    cur = newCur;
    end = newEnd;
  }
}


void G8MemBuffer::clear()
{
  cur = begin;
  epoch = ++globalEpoch;
}

void G8MemBuffer::maybeClear()
{
  if (cur - begin > 16*1024*1024) clear();
}


template<>
void G8Cache<WGPUTexture>::cacheRelease(WGPUTexture &it)
{
  wgpuTextureRelease(it);
  it = nullptr;
}

template<>
void G8Cache<WGPUBuffer>::cacheRelease(WGPUBuffer &it)
{
  wgpuBufferRelease(it);
  it = nullptr;
}

