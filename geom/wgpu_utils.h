#pragma once
#include "../src/core.h"
#include "../geom/gl_headers.h"

WGPUAdapter requestAdapter(WGPUInstance instance, WGPURequestAdapterOptions const * options);
WGPUDevice requestDevice(WGPUAdapter adapter, WGPUDeviceDescriptor const * descriptor);
