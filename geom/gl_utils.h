#pragma once
#include "./gl_headers.h"
#include "./colors.h"
#include "GLFW/glfw3.h"

struct YsFontDir {
  ImFont *tinyFont;
  ImFont *smlFont;
  ImFont *medFont;
  ImFont *lrgFont;
  ImFont *hugeFont;
};

extern YsFontDir ysFonts;


struct PolylineBuf {

  vector<ImVec2> pts;

  PolylineBuf();
  ~PolylineBuf();

  void reserve(size_t n);

  inline void operator() (float x, float y) {
    pts.emplace_back(x, y);
  }

  inline void operator() (ImVec2 pt) {
    pts.push_back(pt);
  }

  void draw(U32 color, bool closed=false, float width=1.0f);
};


struct PolybandBuf {

  struct Pt {
    float x, y0, y1;
  };

  vector<Pt> pts;

  PolybandBuf();
  ~PolybandBuf();

  void reserve(size_t n);

  void operator() (float x, float y0, float y1) {
    pts.push_back({round(x), y0, y1});
  }

  void fill(U32 color);
};


void drawText(float x, float y, U32 color, string const &text, float hAlign=0.0f, float vAlign=0.0f);
void loadPngForFont(ImFont *font, string const &fn, int codePoint);
void blitIconsIntoFont();
ImFont *addFontWithIcons(string fontFn, string iconFn, float size);

shared_ptr<GLFWimage> readGlfwImagePng(string const &fn);


struct Textp {
  ostringstream s;

  Textp()
  {
    copyfmt();
  }

  Textp(string const &initial)
    :s(initial, std::ios_base::ate)
  {
    copyfmt();
  }

  void copyfmt()
  {
    s.precision(cerr.precision());
    s.flags(cerr.flags());
    s.width(cerr.width());
  }

  ~Textp()
  {
    string contents = s.str();
    TextUnformatted(contents.data(), contents.data() + contents.size());
  }

  template<typename T> Textp & operator <<(T const &a) { s << a; return *this; }

};
