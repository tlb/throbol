#pragma once
#include "../src/defs.h"
#include "../src/blobs.h"
#include "./gl_headers.h"

struct MainWindow;

struct TexCache {

  MainWindow &win;
  size_t n_tex = 0;
  vector<WGPUTexture> textures;
  vector<U64> lastUses;
  vector<BlobRef> keys;
  vector<vec2> sizes;
  U64 lastUseCounter = 0;

  TexCache(MainWindow &_win);
  ~TexCache();

  void alloc(size_t _n_tex);
  bool getIndexFor(BlobRef const &key, int &bestIndex);
  void setIndexFor(BlobRef const &key, int bestIndex);
  void setPixelsBGRA(int texIndex, U32 width, U32 height, U8 *pixels);
  bool setBGRA(int texIndex, int width, int height, Blob &buffer);
  bool setJpeg(int texIndex, Blob &buffer);
  bool setJpegFile(int texIndex, string const &fn);
  bool setPngFile(int texIndex, string const &fn);
  WGPUTexture getTexture(int texIndex);
  void *getTexidFromImagePng(BlobRef const &key, string const &fn);

};
