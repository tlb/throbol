#pragma once
#include "common/std_headers.h"
#include "common/str_utils.h"
#define IMGUI_DEFINE_MATH_OPERATORS
#include "imgui.h"
#include "imgui_internal.h"
#include "webgpu/webgpu.h"

#include "backends/imgui_impl_wgpu.h"
#include "imgui_utils.h"

using namespace ImGui;

#include "./geom.h"

#include "./gl_utils.h"
