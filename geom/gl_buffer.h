#pragma once
#include "common/std_headers.h"
#include "./gl_headers.h"
#include "../src/hashcode.h"

struct G8MemBuffer;
struct G8ShaderGeneric;

struct G8GpuBuffer : AllocTrackingMixin<G8GpuBuffer> {
  char const *label = nullptr;
  WGPUBuffer wgpu_buffer = nullptr;
  WGPUBufferUsageFlags usage = 0;
  size_t buffer_size = 0;
  size_t valid_size = 0;
  uint64_t epoch = -1;

  G8GpuBuffer() = delete;
  G8GpuBuffer(G8GpuBuffer const &other) = delete;
  G8GpuBuffer(G8GpuBuffer &&other) noexcept;
  G8GpuBuffer(WGPUDevice dev, char const *_label, WGPUBufferUsageFlags _usage, size_t want);
  ~G8GpuBuffer();
  void realloc(WGPUDevice dev, size_t want);
  void sync(WGPUDevice dev, WGPUQueue queue, G8MemBuffer &mem);
};


struct G8MemBuffer : AllocTrackingMixin<G8MemBuffer>  {

  U8 *cur = nullptr;
  U8 *begin = nullptr;
  U8 *end = nullptr;
  uint64_t epoch = 0;
  static uint64_t globalEpoch;

  static constexpr size_t dynamic_stride = 256;

  G8MemBuffer();
  ~G8MemBuffer();
  G8MemBuffer(G8MemBuffer const &other) = delete;
  G8MemBuffer(G8MemBuffer &&other);

  void need(size_t maxAdd);

  // This is used for uniforms, but also see G8IdxBlaster and G8VtxBlaster. 
  template<typename T>
  size_t addDynamic(T const &it)
  {
    assert((cur - begin) % dynamic_stride == 0);
    auto padsize = ((sizeof(T) + dynamic_stride - 1) / dynamic_stride) * dynamic_stride;
    need(padsize);
    memcpy(cur, &it, sizeof(T));
    auto ret = cur - begin;
    cur += padsize;
    return ret;
  }

  size_t validSize()
  {
    return cur - begin;
  }

  size_t allocSize()
  {
    return end - begin;
  }

  void clear();
  void maybeClear();

};

template<typename Ent>
struct G8Cache {

  unordered_map<ObjectKey, pair<uint64_t, optional<Ent>>> ents;
  uint64_t generation = 0;
  uint64_t lifetime = 10;

  ~G8Cache()
  {
    clear();
  }

  optional<Ent> & operator [](ObjectKey const &key)
  {
    auto &ret = ents[key];
    ret.first = generation;
    return ret.second;
  }

  void gc()
  {
    generation ++;
    for (auto it = ents.begin(); it != ents.end(); ) {
      if (generation - it->first >= lifetime) {
        if (it->second.second) {
          cacheRelease(it->second->second);
          it->second->second = nullopt;
        }
        it = ents.erase(it);
      }
      else {
        it++;
      }
    }
  }

  void clear()
  {
    for (auto it = ents.begin(); it != ents.end(); ) {
      if (it->second.second) {
        cacheRelease(*it->second.second);
        it->second.second = nullopt;
      }
      it = ents.erase(it);
    }
    ents.clear();
  }

  bool empty()
  {
    return ents.empty();
  }

  size_t size()
  {
    return ents.size();
  }

  void cacheRelease(Ent &it);

};