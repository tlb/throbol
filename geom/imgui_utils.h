

namespace ImGui {

inline bool IsModNone() // No modifier keys
{
  auto &io = GetIO();
  return !io.KeyCtrl && !io.KeySuper && !io.KeyShift && !io.KeyAlt;
}

inline bool IsModText() // No mods but shift is ok
{
  auto &io = GetIO();
  return !io.KeySuper && !io.KeyCtrl && !io.KeyAlt;
}

inline bool IsModOption() // ⌥
{
  auto &io = GetIO();
  return !io.KeyCtrl && !io.KeySuper && !io.KeyShift && io.KeyAlt;
}

inline bool IsModShift() // ⇧
{
  auto &io = GetIO();
  return !io.KeyCtrl && !io.KeySuper && io.KeyShift && !io.KeyAlt;
}

inline bool IsModOptionShift() // ⌥⇧
{
  auto &io = GetIO();
  return !io.KeyCtrl && !io.KeySuper && io.KeyShift && io.KeyAlt;
}

inline bool IsModCmd() // ⌘
{
  auto &io = GetIO();
  return !io.KeyCtrl && io.KeySuper && !io.KeyShift && !io.KeyAlt;
}

inline bool IsModOptionCmd() // ⌥⌘
{
  auto &io = GetIO();
  return !io.KeyCtrl && io.KeySuper && !io.KeyShift && io.KeyAlt;
}

inline bool IsModShiftCmd() // ⇧⌘
{
  auto &io = GetIO();
  return !io.KeyCtrl && io.KeySuper && io.KeyShift && !io.KeyAlt;
}

inline bool IsModOptionShiftCmd() // ⌥⇧⌘
{
  auto &io = GetIO();
  return !io.KeyCtrl && io.KeySuper && io.KeyShift && io.KeyAlt;
}

inline bool IsModCtrl() // ⌃
{
  auto &io = GetIO();
  return io.KeyCtrl && !io.KeySuper && !io.KeyShift && !io.KeyAlt;
}

inline bool IsModCtrlOption() // ^⌥
{
  auto &io = GetIO();
  return io.KeyCtrl && !io.KeySuper && !io.KeyShift && io.KeyAlt;
}

inline bool IsModCtrlShift() // ^⇧
{
  auto &io = GetIO();
  return io.KeyCtrl && !io.KeySuper && io.KeyShift && !io.KeyAlt;
}

inline bool IsModCtrlOptionShift() // ^⌥⇧
{
  auto &io = GetIO();
  return io.KeyCtrl && !io.KeySuper && io.KeyShift && io.KeyAlt;
}

inline bool IsModCtrlCmd() // ^⌘
{
  auto &io = GetIO();
  return io.KeyCtrl && io.KeySuper && !io.KeyShift && !io.KeyAlt;
}

inline bool IsModCtrlOptionCmd() // ^⌥⌘
{
  auto &io = GetIO();
  return io.KeyCtrl && io.KeySuper && !io.KeyShift && io.KeyAlt;
}

inline bool IsModCtrlShiftCmd() // ^⇧⌘
{
  auto &io = GetIO();
  return io.KeyCtrl && io.KeySuper && io.KeyShift && !io.KeyAlt;
}

inline bool IsModCtrlOptionShiftCmd() // ^⌥⇧⌘
{
  auto &io = GetIO();
  return io.KeyCtrl && io.KeySuper && io.KeyShift && io.KeyAlt;
}


inline bool IsAnyPopupOpen()
{
  return IsPopupOpen(nullptr, ImGuiPopupFlags_AnyPopupId + ImGuiPopupFlags_AnyPopupLevel);
}

}
