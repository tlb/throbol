#pragma once
#include "common/std_headers.h"
#include <Eigen/Dense>


using vec2 = Eigen::Vector2f;
using vec3 = Eigen::Vector3f;
using vec4 = Eigen::Vector4f;
using mat2 = Eigen::Matrix2f;
using mat3 = Eigen::Matrix3f;
using mat4 = Eigen::Matrix4f;

using dvec2 = Eigen::Vector2d;
using dvec3 = Eigen::Vector3d;
using dvec4 = Eigen::Vector4d;
using dmat2 = Eigen::Matrix2d;
using dmat3 = Eigen::Matrix3d;
using dmat4 = Eigen::Matrix4d;

using Eigen::VectorXf;
using Eigen::MatrixXf;
using Eigen::VectorXd;
using Eigen::MatrixXd;
using Eigen::VectorXcf;
using Eigen::MatrixXcf;
using Eigen::Index;
using Eigen::Dynamic;
using Eigen::Map;
using Eigen::AngleAxis;
using Eigen::Projective;
using Eigen::Translation3f;
using Eigen::Transform;

using mat = Eigen::Map<MatrixXf>;
using cmat = Eigen::Map<MatrixXcf>;

using vec = Eigen::Map<VectorXf>;
using cvec = Eigen::Map<VectorXcf>;


std::ostream & operator << (std::ostream &s, vec2 const &a);
std::ostream & operator << (std::ostream &s, vec3 const &a);
std::ostream & operator << (std::ostream &s, vec4 const &a);
std::ostream & operator << (std::ostream &s, Eigen::Map<Eigen::Array<F, 4, 1>> const &a);
std::ostream & operator << (std::ostream &s, Eigen::Map<Eigen::Array<F, 8, 1>> const &a);
std::ostream & operator << (std::ostream &s, Eigen::Map<Eigen::Array<complex<float>, 4, 1>> const &a);
std::ostream & operator << (std::ostream &s, Eigen::Map<Eigen::Array<complex<float>, 8, 1>> const &a);
std::ostream & operator << (std::ostream &s, mat2 const &a);
std::ostream & operator << (std::ostream &s, mat4 const &a);
std::ostream & operator << (std::ostream &s, MatrixXf const &a);
std::ostream & operator << (std::ostream &s, VectorXf const &a);
std::ostream & operator << (std::ostream &s, MatrixXd const &a);
std::ostream & operator << (std::ostream &s, VectorXd const &a);
std::ostream & operator << (std::ostream &s, mat const &a);
std::ostream & operator << (std::ostream &s, cmat const &a);
std::ostream & operator << (std::ostream &s, vec const &a);
std::ostream & operator << (std::ostream &s, cvec const &a);
std::ostream & operator << (std::ostream &s, complex<float> const &a);


bool intersectRayTriangle(vec3 const &orig, vec3 const &dir,
  vec3 const &vert0, vec3 const &vert1, vec3 const &vert2,
  vec2 &baryPosition, F &distance);

mat4 lookAt(vec3 const &eye, vec3 const &center, vec3 const &up);
mat4 perspective(F fovy, F aspect, F zNear, F zFar);


inline F clamp01(F a)
{
  return std::clamp(a, 0.0f, 1.0f);
}

inline vec3 append0(vec2 const &a) { return vec3{a(0), a(1), 0}; }
inline vec4 append0(vec3 const &a) { return vec4{a(0), a(1), a(2), 0}; }
