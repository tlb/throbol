#include "./shaders.h"

/*
  WGSL reference: https://www.w3.org/TR/WGSL/
*/


G8Shader<G8Vtx_pcl> rawColorShader("rawColor", {
  "vtx:", R"(

    @group(0) @binding(0) var<uniform> projViewUniforms: ProjViewUniforms;

    @vertex
    fn main(in: G8Vtx_pcl) -> Frag_sc
    {
      var out: Frag_sc;
      out.screenpos = projViewUniforms.proj * projViewUniforms.view * vec4(in.pos, 1.0) + 
        vec4(0.0, 0.0, -in.layer, 0.0);
      out.color = in.color;
      return out;
    }

  )",
  "frg:", R"(

    @group(0) @binding(0) var<uniform> projViewUniforms: ProjViewUniforms;

    @fragment
    fn main(in: Frag_sc) -> @location(0) vec4<f32> {
      return in.color;
    }
  )"
});


G8Shader<G8Vtx_pncl> litSolidShader("litSolid", {
  "g0Binding.modelUniforms:", "1",
  "vtx:", R"(

    @group(0) @binding(0) var<uniform> projViewUniforms: ProjViewUniforms;
    @group(0) @binding(1) var<uniform> modelUniforms: ModelUniforms;

    @vertex
    fn main(in: G8Vtx_pncl) -> Frag_spnlc
    {
      var out: Frag_spnlc;
      out.color = in.color;

      var infpos = modelUniforms.model * vec4(in.pos, 1.0);
      infpos += normalize(modelUniforms.model * vec4(in.normal, 1.0)) * modelUniforms.inflation;

      out.normal = (projViewUniforms.view * modelUniforms.model * vec4(in.normal, 0.0)).xyz;
      out.pos = (projViewUniforms.view * infpos).xyz;
      out.lightpos = (projViewUniforms.view * vec4(-15000.0, 25000.0, 50000.0, 1.0)).xyz;
      out.screenpos = projViewUniforms.proj * projViewUniforms.view * infpos + vec4(0.0, 0.0, -in.layer, 0.0);
      return out;
    }
  )",
  "frg:", R"(

    @group(0) @binding(0) var<uniform> projViewUniforms: ProjViewUniforms;

    @fragment
    fn main(in: Frag_spnlc) -> @location(0) vec4<f32> {
      var norm = normalize(in.normal);
      var lightDir = normalize(in.lightpos - in.pos);
      var viewDir = normalize(-in.pos);
      var reflectDir = reflect(-lightDir, norm);

      var ambient = vec3(0.5, 0.5, 0.5);
      var diffuse = vec3(0.6, 0.6, 0.6) * max(dot(norm, lightDir), 0.0);
      var specular = vec3(0.65, 0.65, 0.70) * pow(max(dot(viewDir, reflectDir), 0.0), 32.0);  

      return vec4(specular, 0.0) + vec4(ambient + diffuse, 1.0) * in.color;
    }
  )"
});


G8Shader<G8Vtx_pncl> unshinySolidShader("unshinySolid", {
  "vtx:", R"(

    @group(0) @binding(0) var<uniform> projViewUniforms: ProjViewUniforms;

    @vertex
    fn main(in: G8Vtx_pncl) -> Frag_spnlc
    {
      var out: Frag_spnlc;
      out.color = in.color;
      out.normal = (projViewUniforms.view * vec4(in.normal, 0.0)).xyz;
      out.pos = (projViewUniforms.view * vec4(in.pos, 1.0)).xyz;
      out.lightpos = (projViewUniforms.view * vec4(-15000.0, 25000.0, 50000.0, 1.0)).xyz;
      out.screenpos = projViewUniforms.proj * projViewUniforms.view * vec4(in.pos, 1.0) + vec4(0.0, 0.0, -in.layer, 0.0);
      return out;
    }
  )",
  "frg:", R"(

    @group(0) @binding(0) var<uniform> projViewUniforms: ProjViewUniforms;

    @fragment
    fn main(in: Frag_spnlc) -> @location(0) vec4<f32> {
      var norm = normalize(in.normal);
      var lightDir = normalize(in.lightpos - in.pos);
      var viewDir = normalize(-in.pos);
      var reflectDir = reflect(-lightDir, norm);

      var ambient = vec3(0.5, 0.5, 0.5);
      var diffuse = vec3(0.6, 0.6, 0.6) * max(dot(norm, lightDir), 0.0);

      return vec4(ambient + diffuse, 1.0) * in.color;
    }
  )"
});



G8Shader<G8Vtx_pnul> litTexSolidShader("litTexSolid", {
  "g0Binding.modelUniforms:", "1",
  "g0Binding.imgSampler:", "2",
  "g1Binding.imgTex:", "0",
  "vtx:", R"(

    @group(0) @binding(0) var<uniform> projViewUniforms: ProjViewUniforms;
    @group(0) @binding(1) var<uniform> modelUniforms: ModelUniforms;

    @vertex
    fn main(in: G8Vtx_pnul) -> Frag_spnlcu
    {
      var out: Frag_spnlcu;
      out.uv = in.uv;
      out.normal = (projViewUniforms.view * modelUniforms.model * vec4(in.normal, 0.0)).xyz;
      out.pos = (projViewUniforms.view * modelUniforms.model * vec4(in.pos, 1.0)).xyz;
      out.lightpos = (projViewUniforms.view * vec4(-15000.0, 25000.0, 50000.0, 1.0)).xyz;
      out.screenpos = projViewUniforms.proj * projViewUniforms.view * modelUniforms.model * vec4(in.pos, 1.0) + vec4(0.0, 0.0, -in.layer, 0.0);
      return out;
    }
  )",
  "frg:", R"(

    @group(0) @binding(0) var<uniform> projViewUniforms: ProjViewUniforms;
    @group(0) @binding(2) var samp : sampler;
    @group(1) @binding(0) var imgTex : texture_2d<f32>;

    @fragment
    fn main(in: Frag_spnlcu) -> @location(0) vec4<f32> {
      var normal = normalize(in.normal);
      var lightDir = normalize(in.lightpos - in.pos);
      var viewDir = normalize(-in.pos);
      var reflectDir = reflect(-lightDir, normal);

      var ambient = vec3(0.5, 0.5, 0.5);
      var diffuse = vec3(0.6, 0.6, 0.6) * max(dot(normal, lightDir), 0.0);
      var specular = vec3(0.65, 0.65, 0.70) * pow(max(dot(viewDir, reflectDir), 0.0), 32.0);  

      return vec4(specular, 0.0) + vec4(ambient + diffuse, 1.0) * textureSample(imgTex, samp, in.uv);
    }
  )"
});



G8Shader<G8Vtx_pul> videoShader("video", {
  "g0Binding.imgSampler:", "1",
  "g1Binding.imgTex:", "0",
  "vtx:", R"(

    @group(0) @binding(0) var<uniform> projViewUniforms: ProjViewUniforms;

    @vertex
    fn main(in: G8Vtx_pul) -> Frag_su
    {
      var out: Frag_su;
      out.uv = in.uv;
      out.screenpos = projViewUniforms.proj * projViewUniforms.view * vec4(in.pos, 1.0) + vec4(0.0, 0.0, -in.layer, 0.0);
      return out;
    }
  )", 
  "frg:", R"(

    @group(0) @binding(0) var<uniform> projViewUniforms: ProjViewUniforms;
    @group(0) @binding(1) var samp : sampler;
    @group(1) @binding(0) var imgTex : texture_2d<f32>;

    @fragment
    fn main(in: Frag_su) -> @location(0) vec4<f32> {
      return textureSample(imgTex, samp, in.uv);
    }
  )"
});

G8Shader<G8Vtx_pucl> glowingTextShader("glowingText", {
  "doDepthTest:", "0",
  "doDepthWrite:", "0",
  "g0Binding.fontSampler:", "1",
  "g0Binding.fontTex:", "2",
  "cmdBufIndex:", "0",
  "vtx:", R"(

    @group(0) @binding(0) var<uniform> projViewUniforms: ProjViewUniforms;

    @vertex
    fn main(in: G8Vtx_pucl) -> Frag_suc
    {
      var out: Frag_suc;
      out.color = in.color;
      out.uv = in.uv;
      out.screenpos = projViewUniforms.proj * projViewUniforms.view * vec4(in.pos, 1.0) + vec4(0.0, 0.0, -0.005 - in.layer, 0.0);
      return out;
    }
  )",
  "frg:", R"(

    @group(0) @binding(0) var<uniform> projViewUniforms: ProjViewUniforms;
    @group(0) @binding(1) var samp : sampler;
    @group(0) @binding(2) var tex : texture_2d<f32>;

    @fragment
    fn main(in: Frag_suc) -> @location(0) vec4<f32> {
      return in.color * textureSample(tex, samp, in.uv);
    }
  )"
});


G8Shader<G8Vtx_pcl> maskShader("mask", {
  "doDepthTest:", "0",
  "doDepthWrite:", "1",
  "cmdBufIndex:", "0",
  "vtx:", R"(

    @group(0) @binding(0) var<uniform> projViewUniforms: ProjViewUniforms;

    @vertex
    fn main(in: G8Vtx_pcl) -> Frag_sc
    {
      var out: Frag_sc;
      out.color = in.color;
      out.screenpos = projViewUniforms.proj * projViewUniforms.view * vec4(in.pos, 1.0) + vec4(0.0, 0.0, -0.005 - in.layer, 0.0);
      return out;
    }
  )",
  "frg:", R"(

    @group(0) @binding(0) var<uniform> projViewUniforms: ProjViewUniforms;

    @fragment
    fn main(in: Frag_sc) -> @location(0) vec4<f32> {
      return in.color;
    }
  )"
});



G8Shader<G8Vtx_pul> traceShader("trace", {
  "doDepthTest:", "0",
  "doDepthWrite:", "1",
  "g0Binding.traceUniforms:", "2",
  "g0Binding.skinUniforms:", "3",
  "g1Binding.traceVisInfo:", "0",
  "g1Binding.traceData:", "1",
  "vtx:", R"(

    @group(0) @binding(0) var<uniform> projViewUniforms: ProjViewUniforms;

    @vertex
    fn main(in: G8Vtx_pul) -> Frag_su
    {
      var out: Frag_su;
      out.uv = in.uv;
      out.screenpos = projViewUniforms.proj * projViewUniforms.view * vec4(in.pos, 1.0) + vec4(0.0, 0.0, -0.005 - in.layer, 0.0);
      return out;
    }
  )",
  "frg:", R"(

    @group(0) @binding(0) var<uniform> projViewUniforms: ProjViewUniforms;
    @group(0) @binding(2) var<uniform> traceUniforms: TraceUniforms;
    @group(0) @binding(3) var<uniform> skinUniforms : SkinUniforms;
    @group(1) @binding(0) var<storage> traceVisInfo : array<TraceVisInfo>;
    @group(1) @binding(1) var<storage> traceData : array<f32>;

    fn pulse(ofs: f32) -> f32
    {
      return max(0.0, 1.0 - ofs * ofs);
    }

    fn dps(pt: vec2<f32>, p1: vec2<f32>, p2: vec2<f32>) -> f32
    {
      let v = p2 - p1;
      let w = pt - p1;

      let c1 = dot(w, v);
      if (c1 <= 0.0) {
        return distance(pt, p1);
      }
      let c2 = dot(v, v);
      if (c2 < c1) {
        return distance(pt, p2);
      }
      let b = c1 / c2;
      let pb = p1 + b*v;
      return distance(pt, pb);
    }

    @fragment
    fn main(in: Frag_su) -> @location(0) vec4<f32> {
      var ret = vec4<f32>(0.0, 0.0, 0.0, 0.0);

      var bg = 0.0;
      var bgalpha = 0.0;

      var pixScaleX : f32 = 1.0 / max(0.01, fwidth(in.uv.x));
      var pixScaleY : f32 = 1.0 / max(0.01, fwidth(in.uv.y));

      let xcenter = min(traceUniforms.nSamples - 1u, max(0u, u32(round(in.uv.x))));

      let x0u = min(traceUniforms.nSamples - 1u, max(0u, u32(floor(in.uv.x - 1.0))));
      let x1u = min(traceUniforms.nSamples - 1u, max(0u, u32(ceil(in.uv.x + 1.0 + 1.0 * fwidth(in.uv.x)))));

      let uvpx = vec2f(in.uv.x * pixScaleX, in.uv.y * pixScaleY);
      let nVisInfo = arrayLength(&traceVisInfo);

      var overlaps = 0.0;

      for (var vi = 0u; vi < nVisInfo; vi++) {
        let visInfo = traceVisInfo[vi];

        if (visInfo.traceType == TVIT_line) {

          if (in.uv.y >= -1.0 &&
              in.uv.y <= 1.0) {
            bg = 1.0 - pulse(in.uv.y * 50.0);
            bgalpha = 1.0;
          }

          var dist = 99.9;

          for (var x : u32 = x0u; x <= x1u; x++) {
            let y0 = traceData[visInfo.dataStride * min(traceUniforms.nSamples - 1u, x + 0u) + visInfo.dataIndex] / traceUniforms.range;
            let y1 = traceData[visInfo.dataStride * min(traceUniforms.nSamples - 1u, x + 1u) + visInfo.dataIndex] / traceUniforms.range;

            dist = min(dist, dps(uvpx,
                vec2f(f32(x + 0u) * pixScaleX, y0 * pixScaleY),
                vec2f(f32(x + 1u) * pixScaleX, y1 * pixScaleY)));
          }
          let pwide = pulse(dist * 0.5 / visInfo.thick);
          let pnarrow = pulse(dist * 0.8 / visInfo.thick);
          bg -= pwide;
          overlaps += pnarrow;
          ret += pnarrow * skinUniforms.goodGraphColors[visInfo.colorIndex % MAX_GRAPH_TRACES];
        }

        else if (visInfo.traceType == TVIT_beh) {
          if (in.uv.y >= -1.0 &&
              in.uv.y <= 1.0) {
            bg = 1.0;
            bgalpha = 1.0;
          }

          // uv.y goes from -1 to 1. Convert to -0.5 .. nBehStrips-0.5
          let strip = (0.5 + 0.5 * in.uv.y) * f32(max(visInfo.dataCount, 4u)) - 0.5;
          let stripi = u32(floor(strip));
          
          if (stripi < visInfo.dataCount && fract(strip) < 0.25) {
            let pnarrow = traceData[visInfo.dataStride * x0u + visInfo.dataIndex + stripi];
            if (stripi + 1u == visInfo.dataCount) {
              ret += max(0.0, -pnarrow) * vec4(0.8, 0.0, 0.0, 0.0);
              ret += max(0.0, pnarrow) * vec4(0.0, 0.8, 0.0, 0.0);
              if (pnarrow == 0.0) {
                ret += vec4(1.0, 1.0, 1.0, 0.0);
              }
            }
            else {
              ret += pnarrow * skinUniforms.goodGraphColors[(visInfo.colorIndex + stripi) % MAX_GRAPH_TRACES];
            }
          }
        }     
        else if (visInfo.traceType == TVIT_check) {
          if (in.uv.y >= -1.0 &&
              in.uv.y <= 1.0) {
            bg = 1.0;
            bgalpha = 1.0;
          }

          let pass1 = traceData[visInfo.dataStride * xcenter + visInfo.dataIndex + 3u];

          var dist = 99.9;

          for (var x : u32 = x0u; x <= x1u; x++) {
            let y0 = traceData[visInfo.dataStride * min(traceUniforms.nSamples - 1u, x + 0u) + visInfo.dataIndex] / traceUniforms.range;
            let y1 = traceData[visInfo.dataStride * min(traceUniforms.nSamples - 1u, x + 1u) + visInfo.dataIndex] / traceUniforms.range;

            dist = min(dist, dps(uvpx,
                vec2f(f32(x + 0u) * pixScaleX, y0 * pixScaleY),
                vec2f(f32(x + 1u) * pixScaleX, y1 * pixScaleY)));
          }
          
          let pwide = pulse(dist * 0.5 / visInfo.thick);
          let pnarrow = pulse(dist * 0.8 / visInfo.thick);
          bg -= pwide;
          overlaps += pnarrow;
          if (pass1 > 0.0) {
            ret += pnarrow * vec4(0.5, 1.0, 0.5, 0.0);
          }
          else if (pass1 < 0.0) {
            ret += pnarrow * vec4(1.0, 0.5, 0.5, 0.0);
          }
          else {
            ret += pnarrow * vec4(0.9, 0.9, 0.9, 0.0);
          }

          if (pwide < 0.1) {
            let lo1 = traceData[visInfo.dataStride * x1u + visInfo.dataIndex + 1u] / traceUniforms.range;
            let hi1 = traceData[visInfo.dataStride * x1u + visInfo.dataIndex + 2u] / traceUniforms.range;
            if (in.uv.y > lo1 && in.uv.y < hi1) {
              bg = 0.0;
            }
          }
        }
      }

      if (overlaps > 1.5) {
        ret *= 1.5 / overlaps;
      }
      if (bg > 0.0) {
        if (traceUniforms.isCheckFailure > 0u) {
          ret += bg * skinUniforms.traceBgColFailure;
        }
        else if (traceUniforms.isRewardCell > 0u) {
          ret += bg * skinUniforms.traceBgColReward;
        }
        else {
          ret += bg * skinUniforms.traceBgColBase;
        }
      }
      ret.a = bgalpha;
      return ret;
    }

  )"
});





G8Shader<G8Vtx_pul> dialShader("dial", {
  "doDepthTest:", "1",
  "doDepthWrite:", "1",
  "cmdBufIndex:", "0",
  "g0Binding.dialUniforms:", "1",
  "g0Binding.skinUniforms:", "2",
  "vtx:", R"(

    @group(0) @binding(0) var<uniform> projViewUniforms: ProjViewUniforms;

    @vertex
    fn main(in: G8Vtx_pul) -> Frag_su
    {
      var out: Frag_su;
      out.uv = in.uv;
      out.screenpos = projViewUniforms.proj * projViewUniforms.view * vec4(in.pos, 1.0) + vec4(0.0, 0.0, -0.005 - in.layer, 0.0);
      return out;
    }
  )",
  "frg:", R"(

    @group(0) @binding(0) var<uniform> projViewUniforms: ProjViewUniforms;
    @group(0) @binding(1) var<uniform> dialUniforms : DialUniforms;
    @group(0) @binding(2) var<uniform> skinUniforms : SkinUniforms;

    @fragment
    fn main(in: Frag_su) -> @location(0) vec4<f32> {
      var PI = 3.14159;
      var rad = sqrt(in.uv.x * in.uv.x + in.uv.y * in.uv.y);
      var th = atan2(in.uv.x, in.uv.y) / (2.0*PI);

      var v = dialUniforms.value;

      if (v >= 0.0) {
        if (th < 0.0) {
          th = th + 1.0;
        }
      }
      else {
        if (th > 0.0) {
          th = th - 1.0;
        }
      }


      if (rad >= 0.75 && rad <= 0.88) {
        if (v > 0.0 && th < v) {
          return skinUniforms.dialPosCol;
        }
        if (v < 0.0 && th > v) {
          return skinUniforms.dialNegCol;
        }
        return skinUniforms.dialBgCol;
      }
      else if (rad >= 0.9 && rad <= 1.0) {
        if (th > dialUniforms.rangeLo && th < dialUniforms.rangeHi) {
          return skinUniforms.dialRangeCol;
        }
      }
      return vec4(
        0.0,
        0.0,
        0.0,
        0.0);
    }
  )"
});

