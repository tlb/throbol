#include "./texcache.h"
#include "common/str_utils.h"
#include "../src/main_window.h"
#include <csetjmp>
extern "C" {
#include <jpeglib.h>
#include <png.h>
}

TexCache::TexCache(MainWindow &_win)
  :win(_win)
{
}

TexCache::~TexCache()
{
  while (n_tex > 0) {
    wgpuTextureRelease(textures[--n_tex]);
    n_tex --;
  }
}

void TexCache::alloc(size_t _n_tex)
{
  if (n_tex != 0) return;
  n_tex = _n_tex;
  textures.resize(n_tex);
  lastUses.resize(n_tex);
  keys.resize(n_tex);
  sizes.resize(n_tex);

}

bool TexCache::getIndexFor(BlobRef const &key, int &bestIndex)
{
  if (n_tex == 0) alloc(16);
  for (size_t texi = 0; texi < n_tex; texi ++) {
    if (key == keys[texi]) {
      lastUses[texi] = lastUseCounter++;
      bestIndex = texi;
      return true;
    }
  }

  bestIndex = 0;
  for (size_t texi = 1; texi < n_tex; texi ++) {
    if (lastUses[texi] < lastUses[bestIndex]) bestIndex = texi;
  }
  return false;
}

void TexCache::setIndexFor(BlobRef const &key, int bestIndex)
{
  keys[bestIndex] = key;
  lastUses[bestIndex] = lastUseCounter++;
}

WGPUTexture TexCache::getTexture(int texIndex)
{
  lastUses[texIndex] = lastUseCounter++;
  return textures[texIndex];
}

void TexCache::setPixelsBGRA(int index, U32 width, U32 height, U8 *pixels)
{
  if (textures[index]) wgpuTextureRelease(textures[index]);

    WGPUTextureDescriptor tex_desc = {
    .label = "TexCache",
    .usage = WGPUTextureUsage_CopyDst | WGPUTextureUsage_TextureBinding,
    .dimension = WGPUTextureDimension_2D,
    .size = {
      .width = width,
      .height = height,
      .depthOrArrayLayers = 1,
    },
    .format = WGPUTextureFormat_BGRA8Unorm,
    .mipLevelCount = 1,
    .sampleCount = 1,
  };


  textures[index] = wgpuDeviceCreateTexture(win.wgpu_device, &tex_desc);

  WGPUImageCopyTexture dst_view = {
    .texture = textures[index],
    .mipLevel = 0,
    .origin = { .x = 0, .y = 0, .z = 0 },
    .aspect = WGPUTextureAspect_All
  };
  WGPUTextureDataLayout layout = {
    .offset = 0,
    .bytesPerRow = uint32_t(width * 4),
    .rowsPerImage = uint32_t(height)
  };
  WGPUExtent3D size = { 
    .width = uint32_t(width),
    .height = uint32_t(height),
    .depthOrArrayLayers = 1
  };

  wgpuQueueWriteTexture(win.wgpu_queue, &dst_view, pixels, (uint32_t)(width * 4 * height), &layout, &size);

  sizes[index] = vec2(width, height);
}


struct MyJpegErrorMgr {
  struct jpeg_error_mgr pub; // must be first member
  jmp_buf setjmpBuf;
  string errMsg;
};

static void jpegErrorExit(j_common_ptr cinfo)
{
  MyJpegErrorMgr *mgr = reinterpret_cast<MyJpegErrorMgr *>(cinfo->err);

  char errBuf[JMSG_LENGTH_MAX];
  (*cinfo->err->format_message)(cinfo, errBuf);
  mgr->errMsg = errBuf;
}


bool TexCache::setJpeg(int texIndex, Blob &buffer)
{
  assert(texIndex >= 0 && texIndex < int(n_tex));
  if (buffer.size() < 2) {
    L() << "TexCache::setJpeg: short blob size=" << buffer.size();
    return false;
  }
  if (buffer[0] != 0xff || buffer[1] != 0xd8) {
    if (buffer[0] != 0 || buffer[1] != 0) {
      L() << "TexCache::setJpeg: not a jpeg " << repr_02x(buffer[0]) << " " << repr_02x(buffer[1]);
    }
    return false;
  }

  struct jpeg_source_mgr srcmgr;
  struct jpeg_decompress_struct cinfo;

  struct MyJpegErrorMgr jerr;
  cinfo.err = jpeg_std_error(&jerr.pub);
  jerr.pub.error_exit = &jpegErrorExit;
  if (setjmp(jerr.setjmpBuf)) {
    L() << "jpeg error " << jerr.errMsg;
    jpeg_destroy_decompress(&cinfo);
    return false;
  }

  cinfo.src = &srcmgr;
  jpeg_create_decompress(&cinfo);

  jpeg_mem_src(&cinfo, buffer.data(), buffer.size());
  
  jpeg_read_header(&cinfo, (boolean)1);
  cinfo.do_fancy_upsampling = (boolean)0;
  cinfo.dct_method = JDCT_IFAST;
#if defined(__EMSCRIPTEN__)
  cinfo.out_color_space = JCS_RGB; // FIXME
#else
  cinfo.out_color_space = JCS_EXT_BGRA;
#endif
  jpeg_start_decompress(&cinfo);

  if (cinfo.output_components != 4) {
    L() << "jpeg: wrong color space, failing";
    jpeg_abort_decompress(&cinfo);
    jpeg_destroy_decompress(&cinfo);
    return false;
  }

  auto row_stride = cinfo.output_width * cinfo.output_components;
  Blob pixels(row_stride * cinfo.image_height);

  const int MAX_LINES = 16;
  assert(cinfo.rec_outbuf_height < MAX_LINES);
  u_char *lines[MAX_LINES];

  for (u_int y = 0; y < cinfo.image_height; y += cinfo.rec_outbuf_height) {
    for (int i = 0; i < cinfo.rec_outbuf_height; i++) {
      lines[i] = &pixels[(y + i) * row_stride];
    }
    jpeg_read_scanlines(&cinfo, lines, min(
      cinfo.rec_outbuf_height, 
      (int)(cinfo.image_height - y)));
  }
  jpeg_finish_decompress(&cinfo);
  jpeg_destroy_decompress(&cinfo);

  if (0) L() << "jpeg decompressed to " <<
      cinfo.output_width << "x" <<
      cinfo.image_height << "x" <<
      cinfo.output_components;

  setPixelsBGRA(texIndex, cinfo.image_width, cinfo.image_height, pixels.data());
  return true;
}

bool TexCache::setBGRA(int texIndex, int width, int height, Blob &buffer)
{
  setPixelsBGRA(texIndex, width, height, buffer.data());
  return true;
}

bool TexCache::setJpegFile(int texIndex, string const &fn)
{
  int fd = open(fn.c_str(), O_RDONLY, 0777);
  if (fd < 0) {
    L() << fn << ": " << strerror(errno);
    return false;
  }

  struct stat st;
  if (fstat(fd, &st) < 0) {
    return false;
  }
  auto size = st.st_size;

  Blob buf(size);

  auto nr = pread(fd, buf.data(), size, 0);

  if (nr < 0) {
    L() << fn << ": " << strerror(errno);
    close(fd);
    return false;
  }
  else if (nr == 0) {
    L() << fn << ": empty";
    close(fd);
    return false;
  }
  else if (nr < size) {
    L() << fn << ": partial read";
    close(fd);
    return false;
  }

  return setJpeg(texIndex, buf);
}

bool TexCache::setPngFile(int texIndex, string const &fn)
{
  FILE *fp = fopen(fn.c_str(), "r");
  if (!fp) {
    L() << fn << ": " << strerror(errno);
    return false;
  }

  png_image png;

  memset(&png, 0, sizeof(png));
  png.version = PNG_IMAGE_VERSION;
  png.opaque = nullptr;

  if (png_image_begin_read_from_stdio(&png, fp) < 0) {
    L() << "PNG begin-read " << shellEscape(fn) << " failed: " << png.warning_or_error;
    return false;
  }
  png.format = PNG_FORMAT_BGRA;

  U32 rowStride = png.width * sizeof(U32);
  Blob pixels(png.height * rowStride);
  
  png_image_finish_read(&png, 0x00000000, pixels.data(), rowStride, nullptr);
  if (png.warning_or_error) {
    L() << "PNG read " << shellEscape(fn) << " failed: " << png.warning_or_error;
    return false;
  }
  fclose(fp);

  setPixelsBGRA(texIndex, png.width, png.height, pixels.data());

  png_image_free(&png);

  if (0) L() << 
      fn << ": size=" << png.width << "x" << png.height << " rowStride=" << rowStride;

  return true;
}

void *TexCache::getTexidFromImagePng(BlobRef const &key, string const &fn)
{
  int index = 0;
  if (!getIndexFor(key, index)) {
    setIndexFor(key, index);
    setPngFile(index, installDir + "/images/" + fn);
  }

  return reinterpret_cast<void *>((size_t)getTexture(index));
}

