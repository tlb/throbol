# Architecture Notes

## UI

The UI is drawn through OpenGL. It uses [Dear Imgui](https://github.com/ocornut/imgui) for menus & info windows, but most of the spreadsheet is drawn directly in OpenGL, through the G8 class.

## Files

Spreadsheets are saved in `.tb` files, which use YAML as the underlying format. Each cell has a formula string associated with it.

The data from live runs are stored in a [sqlite3](https://sqlite.org/index.html) database named by replacing `.tb` with `.replays` in the first sheet filename. 

## Computing the spreadsheet

Recomputing spreadsheets is mostly done on worker threads. The function `simulateTraceAsync` enqueues a trace for background computation and calls a callback on the main thread when done. It uses C++ `std::mutex` and `std::condition_variable` to manage the queue.

## Running live

When controlling a real robot, it connects to a daemon running on the robot, sends the spreadsheet formulas to it, and receives streaming data updates while the robot runs. 

The robot runs the daemon `throbold`, which listens on port 6912. The UI will connect directly to that port for the duration of a live run. 

The same daemon also listens on port 6914 to support cameras watching the robot. In this mode, it streams data from a camera to disk in real time It supports commodity Axis security cameras, or fancy high frame-rate Basler USB cameras for accurate analysis of fast movement. And it listens on port 6916 to serve video frames to the UI. 

Both the UI and daemon run their main loop and network IO on top of [libuv](https://libuv.org), using the C++ wrapper [UVW](https://github.com/skypjack/uvw). Both IO callbacks and UI redraw are done on the main loop, so very little locking is required.

The daemon controls the robot itself through various protocols depending on the robot:
- Universal Robots arms use their TCP-based protocol.
- Umbrella Research robots use a custom UDP-based protocol

## 


## Compiler

Overview:
  * Parses each cell's formula into an abstract syntax tree. This can be done for each cell independently, without any context from other cells. [See `FormulaParser` and `AstNode`]
  * Scans each AST and produces a list of dependencies: cells that must be evaluated before. [See `FlowAnalyzer`]
  * Traverses the dependency graph and emits code for each cell. [See `CompiledSheet`, `CellEmitter`, and `NodeEmitter`]
  * When emitting code for a cell, it traverses the AST and calls functions like `emit_mul` (for an AST node corresponding to a multiply).
  * 

### Parsing

Parsing is done by a straightforward top-down parser. One notable feature is that it records the exact source position of numeric parameters and ranges (like `3.7~5`) so they can be adjusted interactively in the UI.

### Type and dependency resolution

Throbol statically determines the type of everything in the spreadsheet. It can infer almost all types, except
when there's a circular reference. For instance a cell `t` that refers to its previous value:
```
  t = t[-dt] + dt
```
is ambiguous, as t could be a scalar or complex. A type hint is sufficient:
```
  t = scalar(t[-dt] + dt) // preferred
  t = scalar(t[-dt]) + dt // ok
```

### Emitting code for a cell

For example, here's how addition of scalars (`F` means `float`) is defined:

```c++
deftype(add)
{
  return typetag<>();
}

defemit(add) // (simplified)
{
  defop2(F, F, F, r = a + b);
  ...
}
```

defop2 is a macro that expands to (approximately):

```c++
  if (evalArgs() == 2 && args[0].ist<F>() && args[1].ist<F>()) { // if 2 arguments of type (F, F)

    auto rv = allocResult<F>(); // allocate space for a return value of type F in the pad

    evalop([rv, av=args[0], bv=args[1]](CpuState &cpu, Pad &pad, auto batchdef) { // append this lambda to the code

      F const &a = pad.rdreg<F>(av); // bind a and b as const refs into the pad
      F const &b = pad.rdreg<F>(bv);
      F &r = pad.wrreg<F>(rv); // bind r as a mutable ref
      r = a + b; // perform the addition
    });
    return rv;
  }
```

The `defop...` macros **return from the enclosing function** with the value slot if they succeed.
So you can go through a series of type combinations in increasing order of generality, and the first
match will win. `emit_add` contains several cases:

```c++
deftype(add)
{
  return typetag<>();
}

defemit(add) // (simplified)
{
  defop2(F, F, F, r = a + b);
  defop2(vec2, vec2, vec2, r = a + b);
  defop2(string_view, string_view, string_view, r = pad.pool->strconcatview(a, b));
  ....
  return setError("Invalid types"); // return an undef value, which will trigger an error
}
```

When defop succeeds it calls `CellEmitter::evalop`, which adds the lambda to a vector of lambdas
that execute the program. The code can be executed by doing something like this:

```c++
  Pad pad(pool, compiled->padSize);
  for (auto &op : compiled->evalFuncs) {
    op(cpu, pad);
  }
```

### Batching

Execution of a spreadsheet can be done in batches of BATCH_SIZE at a time.
This spreads the overhead from dispatching opcodes across more computations.
BATCH_SIZE=4 for cpu, and should probably be 32 for an nVidia GPU to use warps.

A CpuState has 4 sets of CellVals structures, and every member of a batch
reads and writes the same offset in their respective CellVal structure.
However, values in a Pad are laid out in batch-minor order, so an offset into
a pad references BATCH_SIZE consecutive values.

Believe it or not, Clang + Eigen is able to emit SIMD instructions for most
arithmetic ops like `mul` and `add` on x64 and arm64. These can be barely more than
5 instructions: 2 SIMD loads, a SIMD arithmetic op, and a SIMD store.
For this to work, we need a fixed-size batch. But batches are formed
opportunistically from available recalculation requests, so there are often
less than the fixed number. To handle this:
 - reads from cells return 0 beyond the end of the batch
 - writes to cells are ignored beyond the end of the batch
 - arithmetic ops proceed as usual, working on zeros.
 - complex ops involving pointers must cope with null pointers


### Parallel recalculation

The native version spawns several worker threads to run recalculations.
The API is `simulateTraceAsync(trace, onDone)`, where `onDone` gets called
asynchronously when the recalculation is done. `simulatePollWait(cond)`
waits for `cond` to be true, which should be set by enough calls to `onDone`.

The wasm-multithread version also spawns worker threads, but I've found it
to be unreliable so the wasm-singlethread version is used instead. In this case,
`simulatePollWait(cond)` actually calls the execution engine and grinds through
the queue (still batching compatible recalculations) until `cond` is true.

Both calling `onDone` and checking `cond` in `simulatePollWait` are done
while holding the same mutex. See [minpack_search.cc](./src/minpack_search.cc)
for an example of aggressive parallelism.

