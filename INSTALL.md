# Installation

On Mac or Linux:
```sh
git clone --recursive https://gitlab.com/tlb/throbol
cd throbol
make install.deps
make
```

Caution: running `make install.deps` runs `sudo apt-get install ...` on Linux, and `brew install ...` on Mac.

It requires a C++17 compiler, like Clang 10 or later.

To try a standalone example:
```
bin/tb examples/lorenz.tb
```


Dependencies:
- [Dear Imgui](https://github.com/ocornut/imgui): included as a git submodule
- [sqlite3](https://sqlite.org/index.html): installed by `make install.deps`
- [sqlite_modern_cpp](https://github.com/SqliteModernCpp/sqlite_modern_cpp): included as a git submodule
- [tlbcore](https://gitlab.com/tlb/tlbcore): included as a git submodule
- [Cereal](https://uscilab.github.io/cereal/index.html): installed by `make install.deps`
- [UVW](https://github.com/skypjack/uvw): included as a git submodule
- [libuv](https://libuv.org): installed by `make install.deps`
- [YAML-cpp](https://github.com/jbeder/yaml-cpp): installed by `make install.deps`
- [Eigen 3](https://eigen.tuxfamily.org): installed by `make install.deps`
- [SDL2](https://libsdl.org): installed by `make install.deps`
- [Catch2](https://github.com/catchorg/Catch2): included as a git submodule
- [hidapi](https://github.com/libusb/hidapi): installed by `make install.deps`
- [apr](https://apr.apache.org): installed by `make install.deps`
- [xxHash](https://cyan4973.github.io/xxHash/): installed by `make install.deps`


