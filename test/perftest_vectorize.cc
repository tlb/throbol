/*
  Standalone program to demonstrate a missed optimization in Clang.
  It can't convince itself that variables captured by value don't change
  through a loop.
  Set RUIN_PERFORMANCE=1 to see lots of extra address calculations done
  inside the loop.
*/

#define RUIN_PERFORMANCE 0
#include "../geom/geom.h"
#include <memory>
#include <vector>
#include <functional>
#include <cstdlib>
#include <iostream>
#include <valarray>
using namespace std;

using U32 = unsigned int;
using F = float;
#define BATCH_SIZE 4

struct Val {
  U32 const t;
  U32 const ofs;
};

struct Pad {
  Pad() {
    pad = (char *)malloc(BATCH_SIZE * 3 * sizeof(float));
  }
  char * pad;

  inline float const &rd_F(Val v, int batchi) const {
    return (*(float *)(pad + v.ofs + batchi * sizeof(float)));
  }
  inline float &wr_F(Val v, int batchi) const {
    return (*(float *)(pad + v.ofs + batchi * sizeof(float)));
  }

  inline auto rd_F_v(Val v) const {
    return Map<Eigen::Vector<F, BATCH_SIZE>>((float *)(pad + v.ofs)).array();
  }
  inline auto wr_F_v(Val v) const {
    return Map<Eigen::Vector<F, BATCH_SIZE>>((float *)(pad + v.ofs)).array();
  }

};

function<void(Pad &pad)> emit_mul()
{
  Val av{99, 0};
  Val bv{99, 4*BATCH_SIZE};
  Val rv{99, 8*BATCH_SIZE};

#if 1

  return [rv=rv, av=av, bv=bv](Pad &pad) {
    decltype(auto) r = pad.wr_F_v(rv);
    decltype(auto) a = pad.rd_F_v(av);
    decltype(auto) b = pad.rd_F_v(bv);
    r = a * b;
  };


#elif 0

  return [rv=rv, av=av, bv=bv](Pad &pad) {
    float * __restrict rp = pad.wr_Fv(rv);
    float const * __restrict ap = pad.rd_Fv(av);
    float const * __restrict bp = pad.rd_Fv(bv);

    float tmp[4];
    for (int batchi = 0; batchi < 4; batchi++) tmp[batchi] = ap[batchi] * bp[batchi];
    for (int batchi = 0; batchi < 4; batchi++) rp[batchi] = tmp[batchi];
  };

#elif 0

  return [rv=rv, av=av, bv=bv](Pad &pad) {
    float * __restrict rp = pad.wr_Fv(rv);
    float const * __restrict ap = pad.rd_Fv(av);
    float const * __restrict bp = pad.rd_Fv(bv);

    foreach_batchi {
      rp[batchi] = ap[batchi] * bp[batchi];
    }
  };

#elif 1

  return [rv=rv, av=av, bv=bv](Pad &pad) {
    alignas(16) float atmp[BATCH_SIZE];
    alignas(16) float btmp[BATCH_SIZE];
    alignas(16) float rtmp[BATCH_SIZE];
    foreach_batchi {
      atmp[batchi] = pad.rd_F(av, batchi);
      btmp[batchi] = pad.rd_F(bv, batchi);
    }
    foreach_batchi {
      rtmp[batchi] = atmp[batchi] * btmp[batchi];
    }
    foreach_batchi {
      pad.wr_F(rv, batchi) = rtmp[batchi];
    }
  };

#else

  return [rv1=rv, av1=av, bv1=bv](Pad &pad) {
    Val av = av1, bv = bv1, rv = rv1;
    foreach_batchi {
      float &r = pad.wr_F(rv, batchi);
      float const &a = pad.rd_F(av, batchi);
      float const &b = pad.rd_F(bv, batchi);
      r = a * b;
    }
  };

#endif
}



int main()
{
  auto const l = emit_mul();
  Pad pad;
  for (int i = 0; i < 1000000000; i++) {
    l(pad);
  }
  return 0;
}
