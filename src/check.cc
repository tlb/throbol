#include "./check.h"
#include "./ast.h"


Checkp op_and(CpuState &cpu, Checkp a, Checkp b)
{
  CheckStatus status;

  if (a->status == Check_fail || b->status == Check_fail) {
    status = Check_fail;
  }
  else if (a->status == Check_notnow && b->status == Check_notnow) {
    status = Check_notnow;
  }
  else if (a->status == Check_pass || b->status == Check_pass) {
    status = Check_pass;
  }
  else {
    L() << "op_and(Checkp, Checkp): unhandled types " << a->status << " " << b->status;
    return nullptr;
  }

  return cpu.pool->mk<CheckAnd>(status, a, b);
}


Checkp op_or(CpuState &cpu, Checkp a, Checkp b)
{
  CheckStatus status;

  if (a->status == Check_pass || b->status == Check_pass) {
    status = Check_pass;
  }
  else if (a->status == Check_notnow && b->status == Check_notnow) {
    status = Check_notnow;
  }
  else if (a->status == Check_fail || b->status == Check_fail) {
    status = Check_fail;
  }
  else {
    L() << "op_or(Checkp, Checkp): unhandled types " << a->status << " " << b->status;
    return nullptr;
  }

  return cpu.pool->mk<CheckOr>(status, a, b);
}


bool op_at(CpuState &cpu, F time, int batchi)
{
  return (round(time / cpu.dts[batchi]) == cpu.timeIndex);
}


Checkp op_checkat(CpuState &cpu, F time, Checkp arg, int batchi, CellIndex cell)
{
  if (round(time / cpu.dts[batchi]) == cpu.timeIndex) {
    if (arg->status == Check_fail) {
      cpu.checkFailures[batchi].push_back(cell);
    }
    return cpu.pool->mk<CheckAt>(arg->status, time, arg);
  }
  else {
    return cpu.pool->mk<CheckAt>(Check_notnow, time, arg);
  }
}


Checkp op_checkbetween(CpuState &cpu, F t0, F t1, Checkp arg, int batchi, CellIndex cell)
{
  if (round(t0 / cpu.dts[batchi]) <= cpu.timeIndex && cpu.timeIndex <= round(t1 / cpu.dts[batchi])) {
    if (arg->status == Check_fail) {
      cpu.checkFailures[batchi].push_back(cell);
    }
    return cpu.pool->mk<CheckBetween>(arg->status, t0, t1, arg);
  }
  else {
    return cpu.pool->mk<CheckBetween>(Check_notnow, t0, t1, arg);
  }
}

Checkp op_checkalways(CpuState &cpu, Checkp arg, int batchi, CellIndex cell)
{
  if (arg->status == Check_fail) {
    cpu.checkFailures[batchi].push_back(cell);
  }
  return arg;
}

Checkp op_check_bool(CpuState &cpu, F condition)
{
  if (condition >= 0.5f) {
    return cpu.pool->mk<CheckBool>(Check_pass, condition);
  }
  else {
    return cpu.pool->mk<CheckBool>(Check_fail, condition);
  }
}

Checkp op_not(CpuState &cpu, Checkp a)
{
  return cpu.pool->mk<CheckNot>(a->status == Check_pass ? Check_fail : Check_pass, a);
}


template<typename T> bool ist(Checkp it)
{
  return it && it->ist<T>();
}


template<typename T> T * ast(Checkp it)
{
  if (it && it->ist<T>()) return (T *)it;
  return nullptr;
}

/*

*/
optional<tuple<F, F, F>> findRange(Checkp it)
{
  if (auto it1 = ast<CheckAt>(it)) {
    return findRange(it1->a);
  }
  if (auto it1 = ast<CheckBetween>(it)) {
    return findRange(it1->a);
  }
  if (auto it1 = ast<CheckAnd>(it)) {
    if (auto a1 = ast<CheckCompare>(it1->a)) {
      if (auto b1 = ast<CheckCompare>(it1->b)) {
        if ((a1->pred == A_le || a1->pred == A_lt) &&
            (b1->pred == A_le || b1->pred == A_lt)) {
          if (a1->rhs == b1->lhs) {
            return make_tuple(a1->lhs, a1->rhs, b1->rhs);
          }
        }
      }
    }
  }
  return nullopt;
}

#if 0
optional<tuple<F, F, F>> findLess(Checkp it)
{
  if (auto it1 = ast<CheckAt>(it)) {
    return findLess(it1->a);
  }
  if (auto it1 = ast<CheckCompare>(it)) {
    if ((it1->pred == A_le || it1->pred == A_lt) &&
        (b1->pred == A_le || b1->pred == A_lt)) {
      if (a1->rhs == b1->lhs) {
        return make_tuple(a1->lhs, a1->rhs, b1->rhs);
      }
    }
  }
  return nullopt;
}
#endif

ostream & operator<< (ostream &s, Checkp const &a1)
{
  if (!a1) return s << "null";
  if (auto a = ast<CheckRange>(a1)) {
    return s << "range(" << a->status << ", " << a->val << ", " << a->lo << ", " << a->hi << ")";
  }
  else if (auto a = ast<CheckCompare>(a1)) {
    return s << "compare(" << a->status << ", " << a->pred << ", " << a->lhs << ", " << a->rhs << ")";
  }
  else if (auto a = ast<CheckAnd>(a1)) {
    return s << "and(" << a->status << ", " << a->a << ", " << a->b << ")";
  }
  else if (auto a = ast<CheckOr>(a1)) {
    return s << "or(" << a->status << ", " << a->a << ", " << a->b << ")";
  }
  else if (auto a = ast<CheckNot>(a1)) {
    return s << "not(" << a->status << ", " << a->a << ")";
  }
  else if (auto a = ast<CheckAt>(a1)) {
    return s << "at(" << a->status << ", " << a->checkTime << ", " << a->a << ")";
  }
  else if (auto a = ast<CheckBetween>(a1)) {
    return s << "between(" << a->status << ", " << a->checkT0 << ", " << a->checkT1 << ", " << a->a << ")";
  }
  else if (auto a = ast<CheckBool>(a1)) {
    return s << "bool(" << a->status << ", " << a->a << ")";
  }
  else {
    return s << "?";
  }
}


ostream &operator <<(ostream &s, FailureCountType const &a)
{
  switch (a) {
    case FailureCount_numCells:  return s << "FailureCount_numCells";
    case FailureCount_time:      return s << "FailureCount_time";
    default:                     return s << "FailureCount(" << int(a) << ")";
  }
}


ostream &operator <<(ostream &s, CheckStatus const &a)
{
  switch (a) {
    case Check_notnow:      return s << "notnow";
    case Check_pass:        return s << "pass";
    case Check_fail:        return s << "fail";
    default:                return s << "CheckStatus(" << int(a) << ")";
  }
}

ostream &operator <<(ostream &s, CheckType const &a)
{
  switch (a) {
    case CheckType_range:     return s << "range";
    case CheckType_compare:   return s << "compare";
    case CheckType_and:       return s << "and";
    case CheckType_or:        return s << "or";
    case CheckType_not:       return s << "not";
    case CheckType_at:        return s << "at";
    case CheckType_between:   return s << "between";
    case CheckType_bool:      return s << "bool";
    default:                  return s << "CheckType(" << int(a) << ")";
  }
}
