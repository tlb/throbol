#include "./types.h"
#include "./boxed.h"
#include "../remote/protocol.h"

Assocp merge(Arena *pool, Assocp a, Assocp b)
{
  if (!a) return b;
  if (!b) return a;
  return pool->mkAssoc(a, "", b);
}

Assocp assoc(Assocp alist, string_view name)
{
  if (!alist) return nullptr;
  if (alist->key == name) return alist;
  if (alist->key.size() == 0 && alist->val.ist<Assocp>()) {
    auto chain = alist->val.get<Assocp>();
    if (auto ret = assoc(chain, name)) return ret;
  }
  if (auto ret = assoc(alist->next, name)) return ret;
  return nullptr;
}

void printAssoc(ostream &s, Sheet &sh, Assocp a, char const *sep)
{
  if (!a) {
    s << "}";
    return;
  }
  if (a->val.ist<Assocp>()) {
    s << sep << "&" << WithSheet(sh, a->val.get<Assocp>());
  }
  else {
    s << sep << a->key << "=" << WithSheet(sh, a->val);
  }
  if (a->next) {
    printAssoc(s, sh, a->next, ", ");
  }
  else {
    s << "}";
  }
}

ostream & operator <<(ostream &s, WithSheet<Assocp> const &sha)
{
  auto &[sh, a] = sha;
  s << "{";
  printAssoc(s, sh, a, "");
  return s;
}


optional<string_view> assoc_string_view(Assocp alist, string_view name)
{
  if (auto o = assoc(alist, name)) {
    if (o->val.ist<string_view>()) {
      return o->val.get<string_view>();
    }
  }
  return nullopt;
}


string_view assoc_string_view(Assocp alist, string_view name, string_view dflt)
{
  auto it = assoc_string_view(alist, name);
  return it ? *it : dflt;
}

optional<F> assoc_F(Assocp alist, string_view name)
{
  if (auto o = assoc(alist, name)) {
    if (o->val.ist<F>()) {
      return o->val.get<F>();
    }
  }
  return nullopt;
}

F assoc_F(Assocp alist, string_view name, F dflt)
{
  auto it = assoc_F(alist, name);
  return it ? *it : dflt;
}

optional<bool> assoc_bool(Assocp alist, string_view name)
{
  if (auto o = assoc(alist, name)) {
    if (o->val.ist<F>()) {
      return o->val.get<F>() >= 0.5f;
    }
  }
  return nullopt;
}

bool assoc_bool(Assocp alist, string_view name, bool dflt)
{
  auto it = assoc_bool(alist, name);
  return it ? *it : dflt;
}

optional<vec2> assoc_vec2(Assocp alist, string_view name)
{
  if (auto o = assoc(alist, name)) {
    if (o->val.ist<vec2>()) {
      return o->val.get<vec2>();
    }
  }
  return nullopt;
}

optional<vec2> assoc_range(Assocp alist, string_view name)
{
  if (auto o = assoc(alist, name)) {
    if (o->val.ist<vec2>()) {
      return o->val.get<vec2>();
    }
    else if (o->val.ist<F>()) {
      return vec2(- o->val.get<F>(), o->val.get<F>());
    }
  }
  return nullopt;
}

optional<vec4> assoc_vec4(Assocp alist, string_view name)
{
  if (auto o = assoc(alist, name)) {
    if (o->val.ist<vec4>()) {
      return o->val.get<vec4>();
    }
  }
  return nullopt;
}

optional<mat4> assoc_mat4(Assocp alist, string_view name)
{
  if (auto o = assoc(alist, name)) {
    if (o->val.ist<mat4>()) {
      return o->val.get<mat4>();
    }
  }
  return nullopt;
}

Assocp assoc_assoc(Assocp alist, string_view name)
{
  if (auto o = assoc(alist, name)) {
    if (o->val.ist<Assocp>()) {
      return o->val.get<Assocp>();
    }
  }
  return nullptr;
}


shared_ptr<AssocValue> toShared(Assocp it)
{
  if (!it) return nullptr;
  return make_shared<AssocValue>(toShared(it->next), it->key, it->val);
}



AssocValue *assoc(shared_ptr<AssocValue> alist, string_view name)
{
  if (!alist) return nullptr;
  if (alist->key == name) return alist.get();
  if (alist->key.size() == 0 && alist->val.ist<Assocp>()) {
    auto chain = alist->val.get<shared_ptr<AssocValue>>();
    if (auto ret = assoc(chain, name)) return ret;
  }
  if (auto ret = assoc(alist->next, name)) return ret;
  return nullptr;
}


optional<string_view> assoc_string_view(shared_ptr<AssocValue> alist, string_view name)
{
  if (auto o = assoc(alist, name)) {
    if (o->val.ist<string_view>()) {
      return o->val.get<string>();
    }
  }
  return nullopt;
}

string_view assoc_string_view(shared_ptr<AssocValue> alist, string_view name, string_view dflt)
{
  auto it = assoc_string_view(alist, name);
  return it ? *it : dflt;
}

optional<F> assoc_F(shared_ptr<AssocValue> alist, string_view name)
{
  if (auto o = assoc(alist, name)) {
    if (o->val.ist<F>()) {
      return o->val.get<F>();
    }
  }
  return nullopt;
}

F assoc_F(shared_ptr<AssocValue> alist, string_view name, F dflt)
{
  auto it = assoc_F(alist, name);
  return it ? *it : dflt;
}

optional<bool> assoc_bool(shared_ptr<AssocValue> alist, string_view name)
{
  if (auto o = assoc(alist, name)) {
    if (o->val.ist<F>()) {
      return o->val.get<F>() >= 0.5f;
    }
  }
  return nullopt;
}

bool assoc_bool(shared_ptr<AssocValue> alist, string_view name, bool dflt)
{
  auto it = assoc_bool(alist, name);
  return it ? *it : dflt;
}

optional<mat4> assoc_mat4(shared_ptr<AssocValue> alist, string_view name)
{
  if (auto o = assoc(alist, name)) {
    if (o->val.ist<mat4>()) {
      return o->val.get<mat4>();
    }
  }
  return nullopt;
}

shared_ptr<AssocValue> assoc_assoc(shared_ptr<AssocValue> alist, string_view name)
{
  if (auto o = assoc(alist, name)) {
    if (o->val.ist<Assocp>()) {
      return o->val.get<shared_ptr<AssocValue>>();
    }
  }
  return nullptr;
}
