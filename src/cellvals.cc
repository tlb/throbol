#include "./core.h"
#include "numerical/numerical.h"
#include "./emit.h"
#include "./uipoll.h"


CellVals::CellVals(CompiledSheet *_compiled)
  : sh(_compiled->sh),
    compiled(_compiled),
    bufSize(compiled->valueDataSize)
{
  assert(size_t(this) % MAX_ALIGN == 0);
  memset(&buf, 0, bufSize);
}


CellVals::~CellVals()
{
}

void CellVals::reportOverflow(BoxedRef const &ref) const
{
  W() << "CellVals reference overflow. Requested " << ref << 
    " bufSize=" << bufSize <<
    " compiled=" << compiled <<
    (compiled->ok ? " (not ok)" : "") <<
    " compiled->valueDataSize=" << compiled->valueDataSize <<
    " compiled->padSize=" << compiled->padSize1 << "/" << compiled->padSize8 << "/" << compiled->padSize32;
}


vector<BoxedPtr> CellVals::ptrsByCell(CellIndex cell) const
{
  vector<BoxedPtr> ret;
  for (auto ctx : compiled->contexts) {
    auto doneit = ctx->refByCell.find(cell);
    if (doneit != ctx->refByCell.end()) {
      ret.emplace_back(doneit->second.t, (void *)&buf[doneit->second.ofs]);
    }
  }
  return ret;
}


vector<WithSheet<BoxedPtr>> CellVals::scopedPtrsByCell(CellIndex cell) const
{
  vector<WithSheet<BoxedPtr>> ret;
  for (auto ctx : compiled->contexts) {
    auto doneit = ctx->refByCell.find(cell);
    if (doneit != ctx->refByCell.end()) {
      ret.emplace_back(compiled->sh, BoxedPtr(doneit->second.t, (void *)&buf[doneit->second.ofs]));
    }
  }
  return ret;
}

vector<BoxedPtr> CellVals::ptrsByCellName(string const &name) const
{
  auto cell = sh.getCell(name);
  if (cell == nullCellIndex) return vector<BoxedPtr>();
  return ptrsByCell(cell);
}

string CellVals::sv(string const &name)
{
  auto ptrs = ptrsByCellName(name);
  ostringstream oss;
  if (ptrs.size() > 1) oss << "[";
  bool needSep = false;
  for (auto &p : ptrs) {
    if (needSep) oss << ", ";
    needSep = true;
    oss << WithSheet(sh, p);
  }
  if (ptrs.size() > 1) oss << "]";
  return oss.str();
}

void CellVals::zero()
{
  memset(buf, 0, bufSize); 
}

static void *allocWithBuf(size_t valueDataSize, Arena *pool)
{
  auto size = sizeof(CellVals) + valueDataSize;
  auto const align = alignof(CellVals);
  size = (size + align - 1) & ~(align - 1);
  return (*pool)(align, size);
}

CellVals *CellVals::mk(CompiledSheet *_compiled, Arena *pool)
{
  if (!_compiled) return nullptr;
  auto mem = allocWithBuf(_compiled->valueDataSize, pool);
  return new(mem) CellVals(_compiled);
}

static void *allocWithBuf(size_t valueDataSize)
{
  auto size = sizeof(CellVals) + valueDataSize;
  auto const align = alignof(CellVals);
  size = (size + align - 1) & ~(align - 1);
  return aligned_alloc(align, size);
}

shared_ptr<CellVals> CellVals::mk(CompiledSheet *_compiled)
{
  if (!_compiled) return nullptr;
  auto mem = allocWithBuf(_compiled->valueDataSize);
  auto *obj = new(mem) CellVals(_compiled);
  return shared_ptr<CellVals>(obj);
}

string SheetEmitContext::ctxLabel()
{
  vector<string> oritems;
  auto orstack = this;
  while (orstack) {
    for (auto &[callCell, callPos] : orstack->origins) {
      oritems.push_back(she.sh.nameByCell[callCell] + "@" + repr(callPos.begin));
    }
    orstack = orstack->parent;
  }
  if (!oritems.empty()) {
    vector<string> oritemsrev(rbegin(oritems), rend(oritems));
    return joinChar(oritemsrev, '.');
  }
  return "";
}

ostream & operator <<(ostream &s, CellVals const &a)
{
  s << "{";

  bool firstOuter = true;
  for (auto ctx : a.compiled->contexts) {
    if (firstOuter) {
      firstOuter = false;
    }
    else {
      s << ",\n  ";
    }

    auto label = ctx->ctxLabel();
    if (!label.empty()) {
      s << label << ": ";
    }
    s << "{";
    bool firstInner = true;

    vector<pair<CellIndex, BoxedRef>> refs;
    copy(ctx->refByCell.begin(), ctx->refByCell.end(), back_inserter(refs));
    sort(refs.begin(), refs.end(), [](auto const &a, auto const &b) {
      return a.first < b.first;
    });

    for (auto &[cell, ref] : refs) {
      if (ref.valid()) {
        if (firstInner) {
          firstInner = false;
        }
        else {
          s << ", ";
        }
        s << a.sh.nameByCell[cell] << ": ";
        s << WithSheet(a.sh, ref.getPtr(&a));
      }
    }
    s << "}";
  }
  s << "}";
  return s;
}
