#pragma once

#include "./core.h"
#include "./emit.h"

/*
  See https://github.com/catchorg/Catch2/blob/master/docs/Readme.md#top
  https://github.com/catchorg/Catch2/blob/master/docs/assertions.md#top
 */
//#define CATCH_CONFIG_FAST_COMPILE 1
#define CATCH_CONFIG_ENABLE_PAIR_STRINGMAKER 1
#define CATCH_CONFIG_CPP17_STRING_VIEW
#define CATCH_CONFIG_ENABLE_BENCHMARKING
#include "catch2/catch_all.hpp"

extern ofstream testlog;

bool isMatch(string const &l, string const &re);
bool mkSheetCheck(Sheet &sh, char const *dump, char const *fn = "dummy");
shared_ptr<CellVals> sampleSheetData(Sheet &sh, F time=0.0f);
bool checkSheet(Sheet &sh);
void checkCpu(CpuState &cpu);
void checkError(Sheet &sh, string const &match);

inline string mkSheetSample(char const *dump)
{
  Sheet sh;
  REQUIRE(mkSheetCheck(sh, dump));
  auto ref = sampleSheetData(sh);
  return repr(*ref);
}

#ifndef DEBUGFLAGS
#define DEBUGFLAGS "unknown"
#endif

extern long overridePerfIterCount;

void perfReport(string const &desc, std::function<void()> f, double subIterCount=1.0);
