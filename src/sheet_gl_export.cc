#include "./sheet_ui.h"
#include "../geom/g8.h"
#include <png.h>

#ifdef wgpu_notyet
static int writePng(int width, int height, Blob &data, char const *fn)
{

  FILE *fp = fopen(fn, "wb");
  if (!fp) return -1;

  png_structp png_ptr = png_create_write_struct(
    PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);

  if (!png_ptr) {
    fclose(fp);
    return -1;
  }

  png_infop info_ptr = png_create_info_struct(png_ptr);
  if (!info_ptr) {
    png_destroy_write_struct(&png_ptr, (png_infopp)nullptr);
    fclose(fp);
    return -1;
  }

  if (setjmp(png_jmpbuf(png_ptr))) {
    png_destroy_write_struct(&png_ptr, &info_ptr);
    fclose(fp);
    return -1;
  }

  png_init_io(png_ptr, fp);

  png_set_IHDR(png_ptr, info_ptr,
    width, height,
    8, PNG_COLOR_TYPE_RGB_ALPHA,
    PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, 
    PNG_FILTER_TYPE_DEFAULT);

  png_bytep row_pointers[height];

  for (int i = 0; i < height; i++) {
    row_pointers[i] = data.data() + ((height - 1 - i) * width) * 4;
  }
  png_set_rows(png_ptr, info_ptr, row_pointers);

  png_write_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, nullptr);

  png_write_end(png_ptr, info_ptr);
  png_destroy_write_struct(&png_ptr, &info_ptr);

  fclose(fp);

  return 0;
}
#endif

void SheetUi::exportCells(Trace &tr)
{
#ifdef wgpu_notyet
  int width = 3260, height = 2720;
  auto orig_darkenFg = darkenFg;
  auto orig_lightenBg = lightenBg;

  uint32_t fbo = 0, render_buf = 0;
  glGenFramebuffers(1, &fbo);
  glGenRenderbuffers(1, &render_buf);
  glBindRenderbuffer(GL_RENDERBUFFER, render_buf);
  glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA, width, height);
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo);
  glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, render_buf);
  glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo);

  G8DrawList dl;

  string exportDir;
  if (endsWith(sh.replayFileName, ".replays")) {
    exportDir = sh.replayFileName.substr(0, sh.replayFileName.size() - 8) + ".export";
    mkdir(exportDir.c_str(), 0777);
    W() << "Writing to " << exportDir;
  }
  else {
    W() << "Can't determine export file name from " << sh.replayFileName;
  }

  auto glo = getLayoutGlobal();

  auto cursVals = tr.getVal(glo.cursIndex);

  for (auto exportStyle : {UiDark, UiLight}) {

    char const *styleName = nullptr;
    vec4 clearColor(0.0, 0.0, 0.0, 0.0);

    switch (exportStyle) {
      case UiDark:
        darkenFg = false;
        lightenBg = false;
        styleName = "dark";
        clearColor = vec4(0.0, 0.0, 0.0, 1.0);
        break;
      case UiLight:
        darkenFg = true;
        lightenBg = true;
        styleName = "light";
        clearColor =  vec4(1.0, 1.0, 1.0, 1.0);
        break;
      default:
        throw runtime_error("Unknown exportStyle");
    }

    for (auto cell : sh.liveCells) {
      dl.vpL = dl.glVpL = 0;
      dl.vpR = dl.glVpR = width;
      dl.vpT = 0;
      dl.vpB = height;
      dl.glVpT = height;
      dl.glVpB = 0;

      // auto vscale = width / 5.0f; // pixels per coord unit
      vec3 cellpos = vec3(0, 0, 0);
      dl.lookat = cellpos + vec3(4.5f, -3.5f, 0);
      dl.eyepos = dl.lookat + vec3(0, 0, 20);
      dl.up = vec3(0, 1, 0);
      dl.fov = F(24.0 * M_PI / 180.0);
      dl.setProjView();
      dl.clearPickRay();
      dl.clear();

      auto lo = getLayoutCell(glo, cell, cellpos, false, false, true);
      drawCell(dl, cell, cursVals, lo);
      drawCellData(dl, cell, cursVals, lo, tr);

      auto screen_tl = dl.projectToScreen(cellpos + vec3(-0.5, +0.2, 0));
      auto screen_br = dl.projectToScreen(cellpos + vec3(+0.2 + lo.totWidth, -0.1 - lo.totHeight, 0));

      if (0) L() << "screen " << int(screen_tl.x) << "x" << int(screen_tl.y) << 
        " to " << int(screen_br.x) << "x" << int(screen_br.y);
          
      int pix_left = max(0, int(floor(screen_tl.x)));
      int pix_right = min(width, int(ceil(screen_br.x)));
      int pix_top = max(0, int(floor(screen_tl.y)));
      int pix_bot = min(height, int(ceil(screen_br.y)));

      if (0) L() << "pix trbl " << pix_top << " " << pix_right << " " <<
        pix_bot << " " << pix_left;

      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo);

      glClearColor(clearColor[0], clearColor[1], clearColor[2], clearColor[3]);

      glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

      dl.webgpuRender(false, false);

      Blob data((pix_right - pix_left) * (pix_bot - pix_top) * 4);

      glReadBuffer(GL_COLOR_ATTACHMENT0);
      glReadPixels(pix_left, height - pix_bot, pix_right - pix_left, pix_bot - pix_top, GL_RGBA, GL_UNSIGNED_BYTE, data.data());


      string pngfn = exportDir + "/" + sh.nameByCell[cell] + "." + styleName + ".png";

      writePng(pix_right - pix_left, pix_bot - pix_top, data, pngfn.c_str());
    }
  }
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

  glDeleteFramebuffers(1,&fbo);
  glDeleteRenderbuffers(1,&render_buf);

  darkenFg = orig_darkenFg;
  lightenBg = orig_lightenBg;
#endif
}