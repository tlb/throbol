#include "./celledit.h"

vector<CursorPos> rowColMap(string_view s)
{
  vector<CursorPos> ret(s.size() + 1);
  CursorCoord col = 0;
  CursorCoord row = 0;

  for (auto [c, fofs] : vbParseUtf8(s)) {
    ret[fofs] = CursorPos(row, col);
    if (c == '\n') {
      col = 0;
      row++;
    }
    else {
      col++;
    }
  }
  return ret;
}

U32 findPosAbove(vector<CursorPos> const &rcm, U32 pos)
{
  auto [row, col] = rcm[pos];
  while (pos > 0 && rcm[pos].row == row) pos--;
  while (pos > 0 && rcm[pos-1].row == rcm[pos].row && rcm[pos - 1].col >= col) pos--;
  return pos;
}

U32 findPosBelow(vector<CursorPos> const &rcm, U32 pos)
{
  auto [row, col] = rcm[pos];
  while (pos + 1 < rcm.size() && rcm[pos].row == row) pos++;
  while (pos + 1 < rcm.size() && rcm[pos].row == row + 1 && rcm[pos + 1].col <= col) pos++;
  return pos;
}

U32 findPosEol(vector<CursorPos> const &rcm, U32 pos)
{
  auto [row, col] = rcm[pos];
  while (pos + 1 < rcm.size() && rcm[pos + 1].row == row) pos++;
  return pos;
}

U32 findPosBol(vector<CursorPos> const &rcm, U32 pos)
{
  auto [row, col] = rcm[pos];
  while (pos > 0 && rcm[pos - 1].row == row) pos--;
  return pos;
}

CellEditableContents::CellEditableContents(SheetUi &_ui, Sheet &_sh, CellIndex _cell)
  : ui(_ui), sh(_sh), cell(_cell), p(ui.editCellCursorPos)
{
  if (ui.editItem.t == PickCell && ui.editItem.i == cell) {
    name = ui.editCellName;
  }
  else {
    name = sh.nameByCell[cell];
  }
  formula = sh.formulaByCell[cell];

  setText();
  if (ui.editItem.t == PickCell && ui.editItem.i == cell) {
    p = std::min(p, formulaEnd);
  }
}

void CellEditableContents::setText()
{
  text.clear();

  nameStart = text.size();
  text += name;
  nameEnd = text.size();

  text += " = ";

  formulaStart = text.size();
  text += formula;
  formulaEnd = text.size();
}

void CellEditableContents::applyAnnotations(vector<Annotation> const &annos)
{
  decorations.clear();
  decorations.reserve(text.size());

  auto annop = annos.begin();
  auto annoend = annos.end();

  vector<pair<U32, TextDecoration>> stack;

  for (U32 fofs = 0; fofs < text.size(); fofs++) {

    if (fofs == nameStart) {
      stack.clear();
      stack.emplace_back(nameEnd, TextDecoration(fgcolor(0x4e, 0xc1, 0xff, 0xff)));
    }
    if (fofs == nameEnd) {
      stack.clear();
      stack.emplace_back(formulaStart, TextDecoration(fgcolor(0xff, 0xff, 0xff, 0xff)));
    }
    if (fofs == formulaStart) {
      stack.clear();
      stack.emplace_back(formulaEnd, TextDecoration(fgcolor(0xec, 0xec, 0xca, 0xff)));
    }

    while (annop != annoend && fofs > formulaStart + annop->loc.begin) annop ++;
    while (annop != annoend && fofs == formulaStart + annop->loc.begin) {
      switch (annop->t) {
        case A_cellref:
          stack.emplace_back(formulaStart + annop->loc.end, TextDecoration(fgcolor(0x9b, 0xdb, 0xfe, 0xff)));
          break;

        case A_param:
          if (ui.pickItem.isParam(annop->getParamIndex())) {
            stack.emplace_back(formulaStart + annop->loc.end, TextDecoration(fgcolor(0x88, 0xff, 0x88, 0xff)));
          }
          else {
            stack.emplace_back(formulaStart + annop->loc.end, TextDecoration(fgcolor(0xbb, 0xee, 0xbb, 0xff)));
          }
          break;

        case A_constant:
          stack.emplace_back(formulaStart + annop->loc.end, TextDecoration(fgcolor(0xbf, 0xa2, 0xbb, 0xff)));
          break;

        case A_dt:
        case A_time:
        case A_failCellCount:
        case A_failTime:
          stack.emplace_back(formulaStart + annop->loc.end, TextDecoration(fgcolor(0xbf, 0x82, 0xbb, 0xff)));
          break;

        default:
          break;
      }
      annop++;
    }

    while (stack.size() > 1 && fofs >= stack.back().first) stack.pop_back();
    decorations.push_back(stack.back().second);
  }
  assertlog(decorations.size() == text.size(), decorations.size() << " vs " << text.size());
}


void CellEditableContents::left()
{
  if (p > 0) p--;
}

void CellEditableContents::right()
{
  if (p < formulaEnd) p++;
}

void CellEditableContents::up()
{
  auto rcm = rowColMap(text);
  p = findPosAbove(rcm, p);
}

void CellEditableContents::down()
{
  auto rcm = rowColMap(text);
  p = findPosBelow(rcm, p);
}

void CellEditableContents::eol()
{
  auto rcm = rowColMap(text);
  p = findPosEol(rcm, p);
}

void CellEditableContents::bol()
{
  auto rcm = rowColMap(text);
  auto newp = findPosBol(rcm, p);
  if (newp == 0) {
    if (p > formulaStart) {
      p = formulaStart;
    }
    else if (p == 0) {
      p = formulaStart;
    }
    else {
      p = 0;
    }
  }
  else {
    p = newp;
  }
}

void CellEditableContents::kill()
{
  auto rcm = rowColMap(text);
  auto eol = findPosEol(rcm, p);
  if (!ui.yankBufferAppend) {
    ui.yankBufferAppend = true;
    ui.yankBuffer.clear();
  }
  string cut;
  if (p >= nameStart && p < nameEnd) {
    ui.yankBuffer.append(name.begin() + p - nameStart, name.end());
    name.erase(name.begin() + p - nameStart, name.end());
    nameChanged = true;
  }
  else if (p >= formulaStart && p < formulaEnd) {
    ui.yankBuffer.append(formula.begin() + p - formulaStart, formula.begin() + max(p + 1, eol) - formulaStart);
    formula.erase(formula.begin() + p - formulaStart, formula.begin() + max(p + 1, eol) - formulaStart);
    formulaChanged = true;
  }
}

void CellEditableContents::backspace()
{
  ui.yankBufferAppend = false;
  if (p > nameStart && p <= nameEnd) {
    name.erase(name.begin() + p - 1 - nameStart);
    p --;
    nameChanged = true;
  }
  else if (p > formulaStart && p <= formulaEnd) {
    formula.erase(formula.begin() + p - 1 - formulaStart);
    p --;
    formulaChanged = true;
  }
  else if (p > 0) {
    p --;
  }
}

void CellEditableContents::del()
{
  ui.yankBufferAppend = false;
  if (p >= nameStart && p < nameEnd) {
    name.erase(name.begin() + p - nameStart);
    nameChanged = true;
  }
  else if (p >= formulaStart && p < formulaEnd) {
    formula.erase(formula.begin() + p - formulaStart);
    formulaChanged = true;
  }
}

void CellEditableContents::insert(U32 c)
{
  if (p >= nameStart && p <= nameEnd) {
    if (p == nameStart && name == "new") {
      name.clear();
      name += c;
      p++;
      nameChanged = true;
    }
    else if (c == ' ' || c == '=') {
      p = formulaStart;
      ui.commitCellName();
    }
    else {
      if (c == '-' && p > nameStart && FormulaTokenizer::isIdentCont(name[p - nameStart - 1])) {
        c = '_';
      }
      name.insert(name.begin() + p - nameStart, c);
      p++;
      nameEnd++;
      formulaStart++;
      formulaEnd++;
      nameChanged = true;
    }
  }
  else if (p >= formulaStart && p <= formulaEnd) {
    if (p == formulaStart && (c == ' ' || c == '=')) {
    }
    else {
      if (c == '-' && p > formulaStart && FormulaTokenizer::isIdentCont(formula[p - formulaStart - 1])) {
        c = '_';
      }
      formula.insert(formula.begin() + p - formulaStart, c);
      p++;
      formulaEnd++;
      formulaChanged = true;
    }
  }
  else {
    if (p >= nameEnd && p < formulaStart) {
      p = formulaStart;
      if (c != ' ' && c != '=') {
        formula.insert(formula.begin(), c);
      }
    }
    else {
      p++;
    }
  }
}

void CellEditableContents::insert(string const &s)
{
  for (auto c : s) { // FIXME: unicode
    insert(c);
  }
}


void CellEditableContents::yank()
{
  ui.yankBufferAppend = false;
  for (auto c : ui.yankBuffer) {
    insert(c);
    setText();
  }
}

bool CellEditableContents::cursorInName()
{
  return (p >= nameStart && p <= nameEnd);
}

void CellEditableContents::input(char32_t c)
{
  if (c == '\x01') { // C-a
    bol();
  }
  else if (c == '\x02') { // C-b
    left();
  }
  else if (c == '\x04') { // C-d
    del();
  }
  else if (c == '\x05') { // C-e
    eol();
  }
  else if (c == '\x06') { // C-f
    right();
  }
  else if (c == '\x08') { // C-h
    backspace();
  }
  else if (c == '\x0b') { // C-k
    kill();
  }
  else if (c == '\x0e') { // C-n
    down();
  }
  else if (c == '\x10') { // C-p
    up();
  }
  else if (c == '\x12') { // C-r while hovering
    if (ui.pickItem.t == PickParam) {
      insert(sh.getParamDesc(ui.pickItem.i));
    }
    else if (ui.pickItem.t == PickCell) {
      insert(sh.nameByCell.at(ui.pickItem.i));
    }
  }
  else if (c == '\x19') { // C-y
    yank();
  }
  // make sure to add any new control sequences in ImGui_ImplSDL2_ProcessEvent
  else if (c == ' ' || c == '\n' || c >= 0x20) {
    insert(c);
  }
}
