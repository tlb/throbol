#include "./core.h"

mat Arena::mkMat(Index nr, Index nc)
{
  auto size = nr * nc * sizeof(F);
  auto mem = (*this)(EIGEN_ALIGN, size);
  return mat((float *)mem, nr, nc);
}

mat Arena::mkMat(mat const &other)
{
  auto size = other.rows() * other.cols() * sizeof(F);
  auto mem = (*this)(EIGEN_ALIGN, size);
  memcpy(mem, other.data(), size);
  return mat((float *)mem, other.rows(), other.cols());
}



cmat Arena::mkCmat(Index nr, Index nc)
{
  auto size = nr * nc * sizeof(CF);
  auto mem = (*this)(EIGEN_ALIGN, size);
  return cmat((CF *)mem, nr, nc);
}

cmat Arena::mkCmat(cmat const &other)
{
  auto size = other.rows() * other.cols() * sizeof(CF);
  auto mem = (*this)(EIGEN_ALIGN, size);
  memcpy(mem, other.data(), size);
  return cmat((CF *)mem, other.rows(), other.cols());
}




vec Arena::mkVec(Index nr)
{
  auto size = nr * sizeof(F);
  auto mem = (*this)(EIGEN_ALIGN, size);
  return vec((float *)mem, nr);
}

vec Arena::mkVec(vec const &other)
{
  auto size = other.rows() * sizeof(F);
  auto mem = (*this)(EIGEN_ALIGN, size);
  memcpy(mem, other.data(), size);
  return vec((float *)mem, other.rows());
}

cvec Arena::mkCvec(Index nr)
{
  auto size = nr * sizeof(CF);
  auto mem = (*this)(EIGEN_ALIGN, size);
  return cvec((CF *)mem, nr);
}

cvec Arena::mkCvec(cvec const &other)
{
  auto size = other.rows() * sizeof(CF);
  auto mem = (*this)(EIGEN_ALIGN, size);
  memcpy(mem, other.data(), size);
  return cvec((CF *)mem, other.rows());
}
