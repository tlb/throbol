#include "./trace.h"
#include "./emit.h"
#include "./uipoll.h"
#include <thread>
#include <mutex>
#include <condition_variable>

static int traceUniqueId;

Trace::Trace(
  shared_ptr<CompiledSheet> _compiled,
  bool _doGrads,
  bool _savePads)
  : sh(_compiled->sh),
    compiled(std::move(_compiled)),
    uniqueId(++traceUniqueId),
    doGrads(_doGrads),
    savePads(_savePads),
    startTime(0.0),
    tracelife(make_shared<Arena>()),
    params(sh.params) // makes a copy
{
}

Trace::~Trace()
{
}

void Trace::pushTraceLife(shared_ptr<Arena> _tracelife)
{
  if (!tracelife->empty()) {
    _tracelife->addObject(tracelife);
  }
  tracelife = std::move(_tracelife);
}


void Trace::startLive()
{
  startTime = realtime();
}

void Trace::setupInitial()
{
  // FIXME: this depends on batch size (ugh)
  auto t0Vals = compiled->getT0Vals(&sh.params);
  F _dt = 0.01f;
  if (t0Vals && compiled->dtRef.valid()) {
    _dt = compiled->dtRef.get<F>(t0Vals.get());
  }
  dt = _dt;

  int _maxIndex = 1000;
  if (t0Vals && compiled->tickCountRef.valid()) {
    auto tickCount = compiled->tickCountRef.get<F>(t0Vals.get());
    if (!isnan(tickCount) && !isinf(tickCount)) {
      _maxIndex = int(tickCount);
    }
  }
  maxIndex = min(maxIndex, _maxIndex);
}


void Trace::stepLive(LiveRobotState &lrt)
{
  U32 tIndex = vals.size();
  auto prev = vals.empty() ? CellVals::mk(compiled.get(), tracelife.get()) : vals.back();

  if (tIndex >= U32(maxIndex)) {
    L() << "Trace: exit after " << tIndex;
    lrt.pleaseStopSoft();
    return;
  }

  CellVals *ref = CellVals::mk(compiled.get(), tracelife.get());
  if (tIndex != 0) {
    for (auto const &api : compiled->apis) {
      api->txActuators(prev, lrt);
    }
  }

  for (auto const &api : compiled->apis) {
    api->copyToReplayBuffer(prev, api->lastData, false, true);
  }

  for (auto const &api : compiled->apis) {
    api->rxSensors(lrt);
  }
  for (auto const &api : compiled->apis) {
    api->rxMonitors(lrt);
  }
  for (auto const &api : compiled->apis) {
    api->copyFromReplayBuffer(ref, api->lastData, true, false);
  }
  {
    std::unique_lock lock(replayBuffer->replayMutex);
    for (auto const &api : compiled->apis) {
      replayBuffer->replayByApiInstance[api->apiInstance].push_back(api->lastData);
    }
  }

  CpuState cpu(*compiled, tracelife.get(), 1);
  cpu.curs[0] = ref;
  cpu.prevs[0] = prev;
  cpu.paramss[0] = &params;
  cpu.traces[0] = this;

  cpu.squashSensorWrites = true;
  cpu.timeIndex = int(vals.size());
  cpu.dts[0] = dt;
  cpu.times[0] = cpu.timeIndex * dt;
  cpu.execSheet();

  vals.push_back(ref);
  for (auto failCell : cpu.checkFailures[0]) {
    checkFailuresByCell[failCell] = true;
    failTime += cpu.dts[0];
  }
}

int Trace::findTime(F time)
{
  return min(int(vals.size()), max(0, int(floor(time / dt))));
}


CellVals *Trace::getVal(int i)
{
  if (i >= 0 && i < int(vals.size())) return vals[i];
  return nullptr;
}

Pad *Trace::getPad(int i)
{
  if (i >= 0 && i < int(pads.size())) return pads[i];
  return nullptr;
}



F Trace::getVal_F(int sampleIndex, BoxedRef ref)
{
  if (ref.valid() && ref.ist<F>()) {
    if (auto val = getVal(sampleIndex)) {
      return val->rd_F(ref);
    }
  }
  return numeric_limits<F>::quiet_NaN();
}
  

void Trace::mkReplayBuffer()
{
  if (!replayBuffer) {
    replayBuffer = make_shared<ReplayBuffer>();
    replayBuffer->startTime = startTime;
  }
}

shared_ptr<ReplayBuffer> Trace::extractReplayBuffer()
{
  mkReplayBuffer();
  for (auto const &api : compiled->apis) {
    api->copyToReplayBuffer(*this, *replayBuffer, true);
  }
  return replayBuffer;
}


int verbose = 0;

void simulateGroup(vector<shared_ptr<Trace>> traces)
{
  assertlog(traces.size() <= MAX_BATCH_SIZE, LOGV(traces.size()) << LOGV(MAX_BATCH_SIZE));
  int maxBatchSize = int(traces.size());
  auto &tr0 = *traces[0];
  auto batchCompiled = tr0.compiled;
  auto batchDt = tr0.dt;
  auto batchSavePads = false; // set below
  bool batchHasReplay = !!tr0.replayBuffer;

  auto batchTracelife = make_shared<Arena>();
  for (int batchi = 0; batchi < maxBatchSize; batchi++) {
    auto &tr = *traces[batchi];

    tr.pushTraceLife(batchTracelife);
    assert(tr.tracelife == batchTracelife);

    // it's an error to call this with different compiled code
    assert(tr.compiled == batchCompiled);
    // it's an error to call this with some traces using replay buffers and some not
    assert(!!tr.replayBuffer == batchHasReplay);
    // it's an error to call this with different dts.
    assert(tr.dt == batchDt);
    // save pads if any traces requests saving
    batchSavePads = batchSavePads || tr.savePads;
  }

  auto t0 = clock();

  vector<std::shared_lock<std::shared_mutex>> replayBufferLocks;

  int batchMaxIndex = 0;
  if (batchHasReplay) {
    vector<std::shared_mutex *> replayBufferMutexes;
    for (auto &tr : traces) {
      assert(tr && tr->replayBuffer);
      int mi = tr->maxIndex;
      replayBufferMutexes.push_back(&tr->replayBuffer->replayMutex);
      {
        std::shared_lock lock(tr->replayBuffer->replayMutex);
        for (auto const &api : batchCompiled->apis) {
          auto &blobs = tr->replayBuffer->replayByApiInstance[api->apiInstance];
          mi = min(mi, int(blobs.size()));
        }
      }
      tr->maxIndex = mi;
      batchMaxIndex = max(batchMaxIndex, tr->maxIndex);
      tr->startTime = tr->replayBuffer->startTime;
    }
    // There might be duplicate replayBufferMutexes, and we should lock them in
    // a deterministic order to avoid deadlocking with other threads running this code.
    // The sort just uses the address of the mutex.
    sort(replayBufferMutexes.begin(), replayBufferMutexes.end());
    replayBufferMutexes.erase(std::unique(replayBufferMutexes.begin(), replayBufferMutexes.end()), replayBufferMutexes.end());
    if (0) L() << "Lock " << replayBufferMutexes.size() << " / " << traces.size();
    for (auto *mtx : replayBufferMutexes) {
      replayBufferLocks.emplace_back(*mtx, std::defer_lock);
    }
  }
  else {
    for (auto &tr : traces) {
      batchMaxIndex = max(batchMaxIndex, tr->maxIndex);
      tr->startTime = realtime();
    }
  }
  assert(batchMaxIndex < 999999);

  // sort in descending order of length, so we can reduce realBatchSize as we go
  sort(traces.begin(), traces.end(), [](shared_ptr<Trace> const &a, shared_ptr<Trace> const &b) {
    return b->maxIndex < a->maxIndex;
  });


  for (auto &tr : traces) {
    tr->vals.clear();
    tr->grads.clear();
  }

  int tIndex = 0;
  CpuState cpu(*batchCompiled, batchTracelife.get(), maxBatchSize);
  if (tr0.replayBuffer || batchCompiled->hasSimApi) {
    cpu.squashSensorWrites = true;
  }

  for (int batchi = 0; batchi < maxBatchSize; batchi++) {
    cpu.setLane(batchi, traces[batchi].get(), &traces[batchi]->params);
  }

  Pad *singlePad = nullptr;
  if (!batchSavePads) {
    singlePad = cpu.mkPad();
  }

  while (1) {

    while (cpu.realBatchSize > 0 && (traces[cpu.realBatchSize-1]->maxIndex <= tIndex || traces[cpu.realBatchSize-1]->simFailure)) {
      if (verbose) L() << "lane " << cpu.realBatchSize-1 << " done after " << tIndex;
      cpu.realBatchSize--;
    }
    if (cpu.realBatchSize == 0) break;

    for (int batchi = 0; batchi < MAX_BATCH_SIZE; batchi++) {
      cpu.curs[batchi] = batchi < cpu.realBatchSize ? CellVals::mk(batchCompiled.get(), batchTracelife.get()) : nullptr;
      cpu.grads[batchi] = (batchi < cpu.realBatchSize && traces[batchi]->doGrads) ? CellVals::mk(batchCompiled.get(), batchTracelife.get()) : nullptr;
    }

    if (batchCompiled->hasLiveApi) {
      if (batchHasReplay) {
        for (auto &it : replayBufferLocks) it.lock();
        for (auto const &api : batchCompiled->apis) {
          for (int batchi = 0; batchi < cpu.realBatchSize; batchi++) {
            auto &blobs = traces[batchi]->replayBuffer->replayByApiInstance[api->apiInstance];
            api->copyFromReplayBuffer(cpu.curs[batchi], blobs.at(tIndex), true, false);
          }
        }
        for (auto &it : replayBufferLocks) it.unlock();
      }
    }
    else {
      for (auto const &api : batchCompiled->apis) {
        for (int batchi = 0; batchi < cpu.realBatchSize; batchi++) {
          if (traces[batchi]->simFailure) continue;
          api->simAdvance(cpu.prevs[batchi], cpu.curs[batchi], traces[batchi].get(), batchi);
        }
      }
    }

    cpu.timeIndex = tIndex;
    for (int batchi = 0; batchi < cpu.realBatchSize; batchi++) {
      cpu.times[batchi] = tIndex * cpu.dts[batchi];
    }
    if (batchSavePads) {
      // Same pad for the whole batch. `trace.cpuBatchIndex` tells where to find its values
      Pad *pad = cpu.mkPad();

      for (int batchi = 0; batchi < cpu.realBatchSize; batchi++) {
        traces[batchi]->pads.push_back(pad);
      }
      cpu.execSheet(*pad);
    }
    else {
      cpu.execSheet(*singlePad);
    }

    for (int batchi = 0; batchi < cpu.realBatchSize; batchi++) {
      traces[batchi]->vals.push_back(cpu.curs[batchi]);
      if (traces[batchi]->doGrads) traces[batchi]->grads.push_back(cpu.grads[batchi]);
      cpu.prevs[batchi] = cpu.curs[batchi];
    }
    tIndex++;
    for (int batchi = 0; batchi < cpu.realBatchSize; batchi++) {
      for (auto failCell : cpu.checkFailures[batchi]) {
        traces[batchi]->checkFailuresByCell[failCell] = true;
        traces[batchi]->failTime += cpu.dts[batchi];
      }
    }
  }
  if (1) {
    if (--batchCompiled->perfLogBackoff < 0) {
      auto t1 = clock();
      batchCompiled->perfLogBackoff = verbose ? 1 : 1000;
      if (verbose) L() << "compile: " <<
        repr_clock(batchCompiled->compileTime) <<
        " batch: " << maxBatchSize <<
        " run: " << repr_clockper(t1 - t0, tIndex) << 
        " mem=" << batchTracelife->totalAlloc();
    }
  }
}

void Trace::writeCsv(ostream &s)
{
  s << "time";
  vector<BoxedRef> brs;
  auto ctx = compiled->contexts.at(0);
  for (auto cell : sh.liveCells) {
    if (compiled->typeByCell[cell].ist<F>()) {
      auto ref = ctx->refByCell[cell];
      if (ref.valid()) {
        brs.push_back(ref);
        s << "," << sh.nameByCell[cell];
      }
    }
  }
  s << "\n";
  for (int ti = 0; ti < nVals(); ti++) {
    s << getTimeForIndex(ti);
    auto val = getVal(ti);
    for (auto br : brs) {
      s << "," << val->rd_F(br);
    }
    s << "\n";
  }
}

#if defined(__EMSCRIPTEN__)
static constexpr int BEST_BATCH_SIZE=4;
#elif defined(__linux__)
static constexpr int BEST_BATCH_SIZE=4;
#else
static constexpr int BEST_BATCH_SIZE=8; // seems fastest on MBP M2
#endif

struct SimQueue : UiPollable {

  // 0 is high-priority, 0 is low-priority
  array<deque<tuple<shared_ptr<Trace>, function<void (shared_ptr<Trace>)>>>, 2> pendingQueues;

  std::mutex pendingMutex;
  std::condition_variable pendingReady;
  std::mutex doneMutex;
  std::condition_variable doneCond;
  array<atomic<long>, MAX_BATCH_SIZE + 1> batchSizeHistogram;

  vector<std::thread> threads;
  atomic<bool> pleaseExit = false;

  // must call `setup()` after. We can't call it here becaues it needs `shared_from_this()`
  SimQueue()
  {
    for (int bs = 0; bs <= MAX_BATCH_SIZE; bs++) {
      batchSizeHistogram[bs] = 0;
    }
  }
  
  virtual ~SimQueue()
  {
  }

  void workThreadMain()
  {
    while (1) {
      // batch up to BATCH_SIZE traces together if they are compatible.
      std::unique_lock lock(pendingMutex);
      pendingReady.wait(lock, [this] { return pleaseExit || !pendingQueues[0].empty() || !pendingQueues[1].empty(); });
      if (pleaseExit) return;
      workThreadOne(lock);
    }
  }


  void workThreadOne(std::unique_lock<std::mutex> &lock)
  {
    if (pleaseExit) return;
    if (pendingQueues[0].empty() && pendingQueues[1].empty()) return;
    auto &pendingQueue = !pendingQueues[0].empty() ? pendingQueues[0] : pendingQueues[1];

    vector<tuple<shared_ptr<Trace>, function<void (shared_ptr<Trace>)>>> todo;
    todo.reserve(MAX_BATCH_SIZE);
    todo.push_back(std::move(pendingQueue.front()));
    pendingQueue.pop_front();
    auto reftr = get<0>(todo.front()).get();

    // Batch up compatible traces.
    while (!pendingQueue.empty() && todo.size() < BEST_BATCH_SIZE) {
      auto &[tr, onDone] = pendingQueue.front();
      if (tr->compiled != reftr->compiled) break;
      if (tr->maxIndex != reftr->maxIndex) break; // XXX remove?
      // We can batch together replays with different sensor data, but not with & without sensor data
      if (!!tr->replayBuffer != !!reftr->replayBuffer) break;

      todo.push_back(std::move(pendingQueue.front())); // moving right out of the front here, since we're about to pop it
      pendingQueue.pop_front();
    }
    batchSizeHistogram.at(todo.size()) ++;
    lock.unlock();

    if (0) L() << "workThread todo=" << todo.size();
    vector<shared_ptr<Trace>> traces;
    traces.reserve(MAX_BATCH_SIZE);
    for (auto &it : todo) {
      traces.push_back(get<0>(it));
    }
    simulateGroup(std::move(traces));

    {
      std::lock_guard lock(doneMutex);
      for (auto &[trace1, onDone1] : todo) {
        onDone1(std::move(trace1));
      }
      doneCond.notify_all();
    }
    uiPollLive = true;
  }

  void setup()
  {
#if defined(__EMSCRIPTEN__)
#if defined(__EMSCRIPTEN_PTHREADS__)
    int nWorkerThreads = 4; // wasm-multithread
#else
    int nWorkerThreads = 0; // wasm-singlethread
#endif
#else
    int nWorkerThreads = max(2, int(std::thread::hardware_concurrency()) - 1); // native
#endif
    threads.reserve(nWorkerThreads);
    for (int i = 0; i < nWorkerThreads; i++) {
      threads.emplace_back([this] {
        workThreadMain();
      });
    }
    if (0) L() << "Spawned " << nWorkerThreads << " simulation threads";
  }

  void add(shared_ptr<Trace> &&trace, std::function<void (shared_ptr<Trace>)> &&onDone, int priority)
  {
    std::lock_guard lock(pendingMutex);
    assert(trace->maxIndex < 999999);

    pendingQueues.at(priority).emplace_back(std::move(trace), std::move(onDone));
    pendingReady.notify_one();
  }

  void add(vector<shared_ptr<Trace>> &&traces, std::function<void (shared_ptr<Trace>)> &&onDone, int priority)
  {
    std::lock_guard lock(pendingMutex);
    for (auto &trace : traces) {
      assert(trace->maxIndex < 999999);
      pendingQueues.at(priority).emplace_back(std::move(trace), std::move(onDone));
      trace = nullptr;
    }
    traces.clear();
    pendingReady.notify_one();
  }

  size_t pendingQueueSize()
  {
    std::lock_guard lock(pendingMutex);
    return pendingQueues[0].size() + pendingQueues[1].size();
  }

  void teardown()
  {
    pleaseExit = true;
    pendingReady.notify_all();
    for (auto &it : threads) {
      it.join();
    }
    threads.clear();
    if (0) {
      auto l = L();
      l << "Batch size histogram:";
      for (int bs = 0; bs <= MAX_BATCH_SIZE; bs++) {
        l << " " << batchSizeHistogram[bs];
      }
    }
  }

  void pollWait(function<bool()> cond)
  {
#if defined(__EMSCRIPTEN__) && !defined(__EMSCRIPTEN_PTHREADS__)
    // actually do the work here
    while (1) {
      {
        std::lock_guard lock(doneMutex);
        if (cond()) break;
      }
      {
        std::unique_lock lock(pendingMutex);
        workThreadOne(lock);
      }
    }
#else
    // wait for a thread to do the work
    std::unique_lock lock(doneMutex);
    doneCond.wait(lock, cond);
#endif
  }

  UiPollResult uiPoll() override
  {
    if (pleaseExit) return UiPoll_closeme;
#if defined(__EMSCRIPTEN__) && !defined(__EMSCRIPTEN_PTHREADS__)
    R endTime = realtime() + 0.010;
    while (1) {
      std::unique_lock lock(pendingMutex);
      if (pendingQueues[0].empty() && pendingQueues[1].empty()) break;
      workThreadOne(lock); // releases lock while doing most of the work
      if (realtime() >= endTime) break;
    }
#endif
    return UiPoll_invisible;
  }
};


static shared_ptr<SimQueue> simQueue;

/*
  Run the simulation, batched on a thread (ideally, but maybe less good for emscripten).

  We promise to hold a particular mutex while calling onDone, so only one can be in flight.
  Also, simulatePollWait holds the same mutex while checking the condition.

  simulatePollWait will wait until some more work is done  
*/
void simulateTraceAsync(shared_ptr<Trace> &&trace, std::function<void (shared_ptr<Trace>)> &&onDone, int priority)
{
  simQueue->add(std::move(trace), std::move(onDone), priority);
}

void simulateTracesAsync(vector<shared_ptr<Trace>> &&traces, std::function<void (shared_ptr<Trace>)> onDone, int priority)
{
  simQueue->add(std::move(traces), std::move(onDone), priority);
}


size_t simulateTraceAsyncQueueSize()
{
  if (simQueue) {
    return simQueue->pendingQueueSize();
  }
  return 0;
}

void simulateStartup() // call sometime in main, before threads get involved
{
  if (!simQueue) {
    simQueue = make_shared<SimQueue>();
    simQueue->setup();
#if defined(__EMSCRIPTEN__) && !defined(__EMSCRIPTEN_PTHREADS__)
    startUiPoll(simQueue);
#endif
  }
}

void simulateShutdown() // call late in main
{
  if (simQueue) {
    simQueue->teardown();
    simQueue = nullptr;
  }
}

void simulatePollWait(function<bool()> cond)
{
  if (simQueue) {
    simQueue->pollWait(cond);
  }
}
