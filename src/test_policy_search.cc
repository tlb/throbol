#include "./test_utils.h"
#include "./policy.h"
#include "./vbmainloop.h"

TEST_CASE("Particle search works", "[policy][particleSearch]") {

  for (auto alg : {ParticleSearch_swarm, ParticleSearch_deRand1Bin}) {
    Sheet sh;
    REQUIRE(mkSheetCheck(sh, R"(
  foo: 0.3~1
  bar: -(foo * (1 - foo))
  )", "policy1"));

    auto fooCell = sh.getCell("foo");
    auto barCell = sh.getCell("bar");
    auto fooRefs = sh.refsByCell(fooCell);
    auto barRefs = sh.refsByCell(barCell);
    REQUIRE(fooRefs.size() == 1);
    REQUIRE(barRefs.size() == 1);

    ParticleSearchSpec spec;
    vector<PolicySearchDim> searchDims;
    searchDims.emplace_back(0, 0.2);
    spec.rMetric = barRefs[0];
    spec.populationTarget = 10;
    spec.alg = alg;
    spec.sampleIndex = 2;
    spec.searchDims = std::span<PolicySearchDim>(searchDims.begin(), searchDims.end());

    auto pss = make_shared<ParticleSearchState>(sh, spec);

    pss->start();
    for (int i = 0; i < 1500; i++) {
      pss->poll();
      simulatePollWait([pss]() { return pss->threadsRunning == 0; });
      if (!pss->active) break;
    }
    pss->writeBackBestParams();

    L() << "ParticleSearch alg=" << spec.alg <<
      "  params=" << pss->sh.params.valueByParam_ <<
      "  bar=" << sh.formulaByCell[barCell] <<
      "  foo=" << sh.formulaByCell[fooCell];
    CHECK((sh.params.valueByParam(0) - 0.5) < 0.05);

    auto ref = sampleSheetData(sh);
    CHECK(abs(fooRefs[0].get<F>(ref.get()) - 0.5) < 0.05);
    CHECK(abs(barRefs[0].get<F>(ref.get()) + 0.25) < 0.05);
  }
};




TEST_CASE("Minpack search works", "[policy][minpackSearch]")
{
  Sheet sh;
  REQUIRE(mkSheetCheck(sh, R"(
foo: 0.8~5
bar: (0 - foo) * (1 - foo)
)", "policy1"));

  auto fooCell = sh.getCell("foo");
  auto barCell = sh.getCell("bar");
  auto fooRefs = sh.refsByCell(fooCell);
  auto barRefs = sh.refsByCell(barCell);
  REQUIRE(fooRefs.size() == 1);
  REQUIRE(barRefs.size() == 1);

  MinpackSearchSpec spec;
  vector<PolicySearchDim> searchDims;
  searchDims.emplace_back(0, 0.2);
  spec.rMetric = barRefs[0];
  spec.sampleIndex = 2;
  spec.regularization = 0;
  spec.searchDims = std::span<PolicySearchDim>(searchDims.begin(), searchDims.end());

  auto pss = make_shared<MinpackSearchState>(sh, spec);

  pss->start();
  for (int i = 0; i < 500; i++) {
    pss->poll();
    usleep(1000);
    if (!pss->active) break;
  }
  pss->writeBackBestParams();

  L() << "MinpackSearch alg=" << spec.alg <<
    " params=" << pss->sh.params.valueByParam_ <<
    " bar=" << sh.formulaByCell[barCell] <<
    " foo=" << sh.formulaByCell[fooCell];
  CHECK((sh.params.valueByParam(0) - 0.5) < 0.05);

  auto ref = sampleSheetData(sh);
  CHECK(abs(fooRefs[0].get<F>(ref.get()) - 0.5) < 0.05);
  CHECK(abs(barRefs[0].get<F>(ref.get()) + 0.25) < 0.05);

};


TEST_CASE("Minpack search solves Rosenbrock function", "[policy][minpackSearch]")
{
  Sheet sh;
  REQUIRE(mkSheetCheck(sh, R"(
x0: 0.5~2
x1: -0.5~2
rosenbrock: (1-x0)^2 + (100 * (x1 - x0^2))^2
)", "policy1"));

  auto rosenbrockCell = sh.getCell("rosenbrock");
  auto rosenbrockRefs = sh.refsByCell(rosenbrockCell);
  REQUIRE(rosenbrockRefs.size() == 1);

  REQUIRE(sh.nParams == 2);

  MinpackSearchSpec spec;
  vector<PolicySearchDim> searchDims;
  searchDims.emplace_back(0, 0.5);
  searchDims.emplace_back(1, 0.5);
  spec.rMetric = rosenbrockRefs[0];
  spec.sampleIndex = 1;
  spec.regularization = 0;
  spec.searchDims = std::span<PolicySearchDim>(searchDims.begin(), searchDims.end());

  auto pss = make_shared<MinpackSearchState>(sh, spec);

  pss->start();
  for (int i = 0; i < 1500; i++) {
    pss->poll();
    usleep(1000);
    if (!pss->active) break;
  }
  L() << "Message: " << pss->message << " at x=" << pss->bestx;
  pss->writeBackBestParams();

  L() << "MinpackSearch alg=" << spec.alg <<
    " params=" << pss->sh.params.valueByParam_ <<
    " x0=" << sh.formulaByCell[sh.getCell("x0")] <<
    " x1=" << sh.formulaByCell[sh.getCell("x1")];
  CHECK(abs(sh.params.valueByParam(0) - 1) < 0.05);
  CHECK(abs(sh.params.valueByParam(1) - 1) < 0.05);

  auto ref = sampleSheetData(sh);
  //CHECK(abs(fooRefs[0].get<F>(ref.get()) - 0.5) < 0.05);
  //CHECK(abs(barRefs[0].get<F>(ref.get()) + 0.25) < 0.05);

};




TEST_CASE("Minpack search works for l-v system", "[policy][minpackSearch]")
{
  io_context.restart();

  Sheet sh;
  REQUIRE(mkSheetCheck(sh, R"(
pop.rabbit: |
  max(10, float(pop.rabbit[-dt] + dt * growth.rabbit[-dt]))
pop.fox: |
  initial ? 10 : float(pop.fox[-dt] * dt * growth.fox[-dt])
pop.both: pop.rabbit + pop.fox
growth.rabbit: |
  0.3~1 * pop.rabbit- 0.015~0.1 * pop.rabbit * pop.fox
growth.fox: |
  0.015~0.1 * float(pop.fox) * pop.rabbit - 0.7~1 * pop.fox
goal: |
  1e-4 * (100 - pop.both)^2
)", "policy1"));

  auto goalCell = sh.getCell("goal");
  auto goalRefs = sh.refsByCell(goalCell);
  REQUIRE(goalRefs.size() == 1);

  MinpackSearchSpec spec;
  vector<PolicySearchDim> searchDims;
  spec.alg = Minpack_Lbfgsb;
  for (auto cell : sh.liveCells) {
    for (auto param : sh.paramsByCell[cell]) {
      searchDims.emplace_back(param, 0.2);
    }
  }
  spec.rMetric = goalRefs[0];
  spec.sampleIndex = 500;
  spec.searchDims = std::span<PolicySearchDim>(searchDims.begin(), searchDims.end());

  auto pss = make_shared<MinpackSearchState>(sh, spec);
  int tickCount = 50;

  asio::steady_timer tick(io_context);
  std::function<void(boost::system::error_code)> tickWork = [pss, &tickCount, &tick, &tickWork](boost::system::error_code error) {
    if (--tickCount == 0) {
      pss->writeBackBestParams();
    }
    else {
      pss->poll();
      tick.expires_after(std::chrono::milliseconds(10));
      tick.async_wait(tickWork);
    }
  };
  tick.expires_after(std::chrono::milliseconds(10));
  tick.async_wait(tickWork);


  pss->start();
  pss->poll();
  io_context.run();
  REQUIRE(tickCount == 0);


  L() << "MinpackSearch alg=" << spec.alg << 
    " params=" << pss->sh.params.valueByParam_ <<
    " growth.rabbit=" << sh.formulaByCell[sh.getCell("growth.rabbit")] <<
    " growth.fox=" << sh.formulaByCell[sh.getCell("growth.fox")];
  CHECK((sh.params.valueByParam(0) - 0.5) < 0.05);

};
