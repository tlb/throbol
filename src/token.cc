#include "./core.h"
#include "./token.h"

FormulaTokenizer::FormulaTokenizer(Sheet &_sh, CellIndex _cell, char const *_formula)
  : sh(_sh), cell(_cell), formula(_formula)
{
  tokens.reserve(4096 / sizeof(Token));
  ok = true;
  tokenize();
}

string_view FormulaTokenizer::getSubstr(Token &tok)
{
  return string_view(formula + tok.substr.begin, tok.substr.end - tok.substr.begin);
}

bool FormulaTokenizer::isIdentStart(char c)
{
  switch (c) {
    case 'a' ... 'z':
    case 'A' ... 'Z':
    case '_':
      return true;

    default:
      return false;
  }
}

bool FormulaTokenizer::isIdentCont(char c)
{
  switch (c) {
    case 'a' ... 'z':
    case 'A' ... 'Z':
    case '0' ... '9':
    case '_':
    case '.':
      return true;

    default:
      return false;
  }
}

bool FormulaTokenizer::isNum(char c)
{
  switch (c) {
    case '0' ... '9':
    case '.':
    case '+':
    case '-':
    case 'e':
    case 'E':
      return true;

    default:
      return false;
  }
}

bool FormulaTokenizer::isHex(char c)
{
  switch (c) {
    case '0' ... '9':
    case 'a' ... 'f':
    case 'A' ... 'F':
      return true;

    default:
      return false;
  }
}

bool FormulaTokenizer::isMeta(char c)
{
  switch (c) {
    case 'A' ... 'Z':
      return true;
    default:
      return false;
  }
}

void FormulaTokenizer::setError(string e, char const *p)
{
  error = std::move(e);
  errorLoc = FormulaSubstr(p - formula, p + 1 - formula);
  ok = false;
  if (0) L() << sh.nameByCell[cell] << ": " << error;
}

void FormulaTokenizer::out(TokenType t, char const *start, char const *end)
{
  tokens.emplace_back(t, FormulaSubstr(start - formula, end - formula));
}
void FormulaTokenizer::out(TokenType t, char const *start, char const *end, string_view value)
{
  tokens.emplace_back(t, FormulaSubstr(start - formula, end - formula), value);
}

// sort of like fnmatch with FNM_PERIOD, but simple
static bool simpleMatch(char const *p, char const *n)
{
  while (true) {
    if (*p == '*') {
      while (FormulaTokenizer::isIdentCont(*n)) n++;
      p++;
    }
    else if (*p == *n) {
      if (*p == 0) return true;
      p++;
      n++;
    }
    else {
      return false;
    }
  }
}


// OPT: some kind of prefix tree
// Or maybe cache the whole result in the sheet under pattern,
// and clear the cache whenever we add, delete, or rename cells.
// Also, I dunno how efficient fnmatch is.
vector<CellIndex>
FormulaTokenizer::namesMatching(char const *pattern)
{
  vector<CellIndex> ret;

  for (auto cellit : sh.liveCells) {
    if (cellit == cell) continue;  // We should never match ourself, I think. 
    // Should we issue a warning?

    auto n = sh.nameByCell[cellit].c_str();
    if (simpleMatch(pattern, n)) {
      ret.push_back(cellit);
    }
  }

  return ret;
}


void FormulaTokenizer::tokenize()
{
  char const *p = formula;
  while (*p) {
    while (*p == ' ' || *p == '\t' || *p == '\n') p++;
    if (!*p) break;
    char const *start = p;

    switch (p[0]) {
      case '-':
        if (p[1] == '>') {
          p += 2;
          out(T_PTR, start, p);
        }
        else if (p[1] == '=') {
          p += 2;
          out(T_MINUSEQ, start, p);
        }
        else {
          p += 1;
          out(T_MINUS, start, p);
        }
        break;

      case '+':
        if (p[1] == '=') {
          p += 2;
          out(T_PLUSEQ, start, p);
        }
        else {
          p += 1;
          out(T_PLUS, start, p);
        }
        break;

      case '*':
        p += 1;
        out(T_STAR, start, p);
        break;

      case '@':
        p += 1;
        out(T_AT, start, p);
        break;

      case '/':
        if (p[1] == '/') {
          p += 2;
          while (*p != 0 && *p != '\n') {
            p++;
          }
          continue;
        }
        else if (p[1] == '*') {
          p += 2;
          while (p[0] != 0 && p[1] != 0 && !(p[0] == '*' && p[1] == '/')) {
            p++;
          }
          if (p[0] == '*' && p[1] == '/') {
            p += 2;
            continue;
          }
          else {
            setError("Unclosed comment", start);
          }
        }
        else {
          p += 1;
          out(T_DIV, start, p);
        }
        break;

      case '(':
        p += 1;
        out(T_LPAR, start, p);
        break;

      case ')':
        p += 1;
        out(T_RPAR, start, p);
        break;

      case '[':
        p += 1;
        out(T_LBRK, start, p);
        break;

      case ']':
        p += 1;
        out(T_RBRK, start, p);
        break;

      case '{':
        p += 1;
        out(T_LWING, start, p);
        break;

      case '}':
        p += 1;
        out(T_RWING, start, p);
        break;

      case '^':
        p += 1;
        out(T_HAT, start, p);
        break;

      case '.':
        if (p[1] == '^') {
          p += 2;
          out(T_DOTHAT, start, p);
        }
        else if (p[1] == '*') {
          p += 2;
          out(T_DOTSTAR, start, p);
        }
        else if (p[1] == '/') {
          p += 2;
          out(T_DOTDIV, start, p);
        }
        else if (p[1] == '+') {
          p += 2;
          out(T_DOTPLUS, start, p);
        }
        else if (p[1] == '-') {
          p += 2;
          out(T_DOTMINUS, start, p);
        }
        else if (p[1] == '=' && p[2] == '=') {
          p += 3;
          out(T_DOTEQEQ, start, p);
        }
        else if (p[1] == '!' && p[2] == '=') {
          p += 3;
          out(T_DOTBANGEQ, start, p);
        }
        else if (p[1] == '>' && p[2] == '=') {
          p += 3;
          out(T_DOTGE, start, p);
        }
        else if (p[1] == '>') {
          p += 2;
          out(T_DOTGT, start, p);
        }
        else if (p[1] == '<' && p[2] == '=') {
          p += 3;
          out(T_DOTLE, start, p);
        }
        else if (p[1] == '<') {
          p += 2;
          out(T_DOTLT, start, p);
        }
        else if (p[1] == '.' && p[2] == '.' && !tokens.empty()) { // ...
          p += 3;
          while (*p == ' ' || *p == '\t' || *p == '\n') p++;

          start = p;

          while (isIdentCont(*p) || *p == '*') {
            p++;
          }
          string pattern(start, p);

          auto sep = tokens.back();
          if (sep.t == T_LBRK || sep.t == T_LPAR) {
            bool first = true;
            for (auto cell : namesMatching(pattern.c_str())) {
              if (!first) tokens.emplace_back(T_COMMA, sep.substr);
              first = false;
              tokens.emplace_back(T_WILDCARD, FormulaSubstr(start - formula, p - formula), cell);
            }
          }
          else {
            tokens.pop_back();
            for (auto cell : namesMatching(pattern.c_str())) {
              tokens.push_back(sep);
              tokens.emplace_back(T_WILDCARD, FormulaSubstr(start - formula, p - formula), cell);
            }
          }
        }
        else {
          p += 1;
          out(T_DOT, start, p);
        }
        break;


      case ',':
        p += 1;
        out(T_COMMA, start, p);
        break;

      case ';':
        p += 1;
        out(T_SEMI, start, p);
        break;

      case '&':
        if (p[1] == '&') {
          p += 2;
          out(T_ANDAND, start, p);
        }
        else {
          p += 1;
          out(T_AND, start, p);
        }
        break;

      case '|':
        if (p[1] == '|') {
          p += 2;
          out(T_OROR, start, p);
        }
        else {
          p += 1;
          out(T_OR, start, p);
        }
        break;

      case '~':
        if (p[1] == '~' && p[2] == '~') {
          p += 3;
          out(T_TILTILTIL, start, p);
        }
        else if (p[1] == '~') {
          p += 2;
          out(T_TILTIL, start, p);
        }
        else {
          p += 1;
          out(T_TILDA, start, p);
        }
        break;

      case '=':
        if (p[1] == '=') {
          p += 2;
          out(T_EQEQ, start, p);
        }
        else if (p[1] == '>') {
          p += 2;
          out(T_PTRPTR, start, p);
        }
        else {
          p += 1;
          out(T_EQ, start, p);
        }
        break;

      case '>':
        if (p[1] == '=') {
          p += 2;
          out(T_GE, start, p);
        }
        else if (p[1] == '>') {
          p += 2;
          out(T_RIGHT, start, p);
        }
        else {
          p += 1;
          out(T_GT, start, p);
        }
        break;

      case '<':
        if (p[1] == '=') {
          p += 2;
          out(T_LE, start, p);
        }
        else if (p[1] == '<') {
          p += 2;
          out(T_LEFT, start, p);
        }
        else {
          p += 1;
          out(T_LT, start, p);
        }
        break;

      case '!':
        if (p[1] == '=') {
          p += 2;
          out(T_BANGEQ, start, p);
        }
        else {
          p += 1;
          out(T_BANG, start, p);
        }
        break;
    
      case '?':
        p += 1;
        out(T_QUERY, start, p);
        break;

      case ':':
        p += 1;
        out(T_COLON, start, p);
        break;

      case 'a' ... 'z':
      case 'A' ... 'Z':
      case '_':
      {
        while (isIdentCont(*p)) {
          p++;
        }
        // foo.bar.* becomes T_WILDCARD("foo.bar")
        if (p[-1] == '.' && (p[0] == '*') && !isIdentCont(p[1])) {
          out(T_WILDCARD, start, p-1);
          p++;
        }
        else {
          auto tok = lookupKeyword(start, p);
          out(tok, start, p);
        }
      }
      break;

      case '\'':
        if (p[1] == '\'' && p[2] == '\'') {
          p += 3;
          string val;
          while (p[0] && p[1] && p[2] && !(p[0] == '\'' && p[1] == '\'' && p[2] == '\'')) {
            val += *p++;
          }
          if (p[0] == '\'' && p[1] == '\'' && p[2] == '\'') {
            out(T_STRING, start+3, p, sh.sheetlife.strdup(val));
            p += 3;
          }
          else {
            return setError("EOF in string, expected '''''", p);
          }
        }
        else {
          p++;
          while (isIdentCont(*p)) {
            p++;
          }
          out(T_SYMBOL, start+1, p);
        }
      break;

      case '0' ... '9':
      {
        bool done = false;
        while (!done) {
          switch (*p) {

            case '0' ... '9':
            case '.':
              p++;
              break;

            case 'e':
            case 'E':
              p++;
              if (*p == '+' || *p == '-') p++;
              break;

            default:
              done = true;
              break;
          }
        }
        out(T_NUMBER, start, p);
      }
      break;

      case '#':
      {
        p++;
        int n = 0;
        while (isHex(*p)) {
          p++;
          n++;
        }
        if (n == 6 || n == 8) {
          out(T_COLOR, start, p);
        }
        else {
          return setError("Invalid hex color", p);
        }
      }
      break;

      case '"':
      {
        string val;
        p++;
        while (1) {
          if (p[0] == '\\') {
            if (p[1] == '\\') {
              val += '\\';
              p += 2;
            }
            else if (p[1] == 'b') {
              val += (char)0x08;
              p += 2;
            }
            else if (p[1] == 'f') {
              val += (char)0x0c;
              p += 2;
            }
            else if (p[1] == 'n') {
              val += (char)0x0a;
              p += 2;
            }
            else if (p[1] == 'r') {
              val += (char)0x0d;
              p += 2;
            }
            else if (p[1] == 't') {
              val += (char)0x09;
              p += 2;
            }
            else if (p[1] == 'u' && isHexDigit(p[2]) && isHexDigit(p[3]) && isHexDigit(p[4]) && isHexDigit(p[5])) {
              uint32_t codept = (
                (fromHexDigit(p[2]) << 12) |
                (fromHexDigit(p[3]) << 8) |
                (fromHexDigit(p[4]) << 4) |
                (fromHexDigit(p[5]) << 0));
              p += 6;

              char mb[MB_LEN_MAX];
              int mblen = wctomb(mb, (wchar_t)codept);
              for (int mbi = 0; mbi < mblen; mbi++) {
                val += mb[mbi];
              }
            }
            else {
              val += p[1];
              p += 2;
            }
          }
          else if (p[0] == '"') {
            p += 1;
            out(T_STRING, start, p, sh.sheetlife.strdupview(val));
            break;
          }
          else if (p[0] == 0) {
            return setError("EOF in string", p);
          }
          else if (p[0] == '\r' && p[1] == '\n') {
            return setError("Newline in string", p);
          }
          else if (p[0] == '\n') {
            return setError("Newline in string", p);
          }
          else if (p[0] == '\t') {
            return setError("Tab in string", p);
          }
          else if (p[0] < 0x20) {
            return setError("Control character in string", p);
          }
          else {
            val += p[0];
            p += 1;
          }
        }
      }
      break;

      default:
        return setError("Bad character "s + cppEscape(string_view(p, 1)), p);
    }
  }
}

TokenType FormulaTokenizer::lookupKeyword(char const *start, char const *p)
{
  if (p == start + 2 && !memcmp(start, "im", 2)) {
    return T_IM;
  }
  else if (p == start + 2 && !memcmp(start, "if", 2)) {
    return T_IF;
  }
  else if (p == start + 4 && !memcmp(start, "else", 4)) {
    return T_ELSE;
  }
  else if (p == start + 3 && !memcmp(start, "api", 3)) {
    return T_API;
  }
  else {
    return T_ID;
  }

}

