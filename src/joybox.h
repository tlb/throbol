#pragma once

#include "../geom/gl_headers.h"
#include "./uipoll.h"

struct hid_device_; // defined in hidapi.h

enum JoyboxType {  
  Joybox_TangentWave2,
  // ADD joybox type
};

enum JoyboxTangentWaveButton {

  JBTW_knob_l0 = 0,
  JBTW_knob_l1,
  JBTW_knob_l2,

  JBTW_knob_c0,
  JBTW_knob_c1,
  JBTW_knob_c2,

  JBTW_knob_r0,
  JBTW_knob_r1,
  JBTW_knob_r2,

  JBTW_alt = 9,

  JBTW_lhot0 = 10,
  JBTW_lhot1,
  JBTW_lhot2,
  JBTW_lline,
  JBTW_lfill,

  JBTW_chot0,
  JBTW_chot1,
  JBTW_chot2,
  JBTW_cline,
  JBTW_cfill,

  JBTW_rhot0,
  JBTW_rhot1,
  JBTW_rhot2,
  JBTW_rline,
  JBTW_rfill,

  JBTW_up = 25,
  JBTW_down,

  JBTW_f7,
  JBTW_f8,
  JBTW_f9,
  JBTW_f4,
  JBTW_f5,
  JBTW_f6,
  JBTW_f1,
  JBTW_f2,
  JBTW_f3,
  
  JBTW_prev = 36,
  JBTW_next,
  JBTW_rew,
  JBTW_stop,
  JBTW_fwd,
  JBTW_total,
};


struct Joybox : UiPollable {

	hid_device_ *dev;
  JoyboxType type;
  vector<F> buttons;
  vector<S32> knobs;
  vector<S32> balls;

  static shared_ptr<Joybox> primary;

  Joybox(hid_device_ *_dev, JoyboxType _type);
  virtual ~Joybox();

  static void report();
  static void setup();
  UiPollResult uiPoll() override;
  void uiRedraw() override;

};
