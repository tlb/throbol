#include "./emit.h"
#include "./emit_macros.h"

deftype(particleSearch)
{
  return typetag<DrawOp>();
}

defemit(particleSearch)
{
  if (n->nch() >= 2) {

    // Eval first argument but not any others since they're paramrefs
    evalSpecial(n->ch(0));
    if (!args[0].template ist<F>()) return setError("Type mismatch", n->ch(0));

    auto rMetricRef = mkValue(typetag<F>());
    auto alg = ParticleSearchAlg(n->data<U32>());
    vector<PolicySearchDim> searchDims;

    for (int argi = 1; argi < n->nch(); argi++) {
      auto paramAst = n->ch(argi);
      
      if (paramAst->t == A_cellref) {
        auto srcRef = ce.resolveName(paramAst, true);
        if (!srcRef.valid()) return setError("Unknown param", paramAst);
        for (auto &it : ce.sh.paramsByCell[srcRef.cell]) {
          searchDims.emplace_back(it);
        }
      }
      else if (paramAst->t == A_paramref) {
        auto srcRef = ce.resolveName(paramAst->ch(0), true);
        if (!srcRef.valid()) return setError("Unknown param", paramAst);
        auto whichParam = n->data<ParamIndex>();
        if (!(whichParam < ce.sh.paramsByCell[srcRef.cell].size())) return setError("Unknown param", paramAst);
        searchDims.emplace_back(ce.sh.paramsByCell[srcRef.cell].at(whichParam));
      }
      else {
        goto fail;
      }
    }

    auto searchDimsSpan = she.buildlife.mkSpan(searchDims);
    auto rv = allocResult<DrawOpHandle>();

    she.evalop([rv1=rv, av1=args[0], alg, rMetricRef, searchDimsSpan](CpuState &cpu, Pad &pad, auto batchdef) {
      auto rv = batchdef.getVal(rv1); \
      auto av = batchdef.getVal(av1); \
      for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
        decltype(auto) a = pad.rd_F(av, batchi);
        decltype(auto) r = pad.wr_DrawOpHandle(rv, batchi);
        if (cpu.curs[batchi]) {
          cpu.curs[batchi]->wr_F_unchecked(rMetricRef) = a;
          r = pad.pool->mk<DrawOpParticleSearch>(
            alg,
            rMetricRef,
            searchDimsSpan,
            cpu.paramHashes[batchi],
            cpu.compiled.epoch,
            cpu.timeIndex,
            a);
        }
        else {
          r = nullptr;
        }
      }
    });
    return rv;
  }
fail:
  return setInvalidArgs();
}

defusage(particleSearch, R"(
  minimize.particle(target ∊ F, params...))");



deftype(minpackSearch)
{
  return typetag<DrawOp>();
}

defemit(minpackSearch)
{
  if (n->nch() >= 2) {

    // Eval first argument but not any others since they're paramrefs
    evalSpecial(n->ch(0));
    if (!args[0].template ist<F>()) return setError("Type mismatch", n->ch(0));

    auto rMetricRef = mkValue(typetag<F>());
    auto alg = MinpackAlg(n->data<U32>());
    vector<PolicySearchDim> searchDims;

    for (int argi = 1; argi < n->nch(); argi++) {
      auto paramAst = n->ch(argi);
      
      if (paramAst->t == A_cellref) {
        auto srcRef = ce.resolveName(paramAst, true);
        if (!srcRef.valid()) return setError("Unknown param", paramAst);
        for (auto &it : ce.sh.paramsByCell[srcRef.cell]) {
          searchDims.push_back(PolicySearchDim(it));
        }
      }
      else if (paramAst->t == A_paramref) {
        auto srcRef = ce.resolveName(paramAst->ch(0), true);
        if (!srcRef.valid()) return setError("Unknown param", paramAst);
        auto whichParam = n->data<ParamIndex>();
        if (!(whichParam < ce.sh.paramsByCell[srcRef.cell].size())) return setError("Unknown param", paramAst);
        searchDims.push_back(PolicySearchDim(ce.sh.paramsByCell[srcRef.cell].at(whichParam)));
      }
      else {
        goto fail;
      }
    }

    auto rv = allocResult<DrawOpHandle>();

    auto searchDimsSpan = she.buildlife.mkSpan<PolicySearchDim>(searchDims);

    she.evalop([rv1=rv, av1=args[0], alg, rMetricRef, searchDimsSpan](CpuState &cpu, Pad &pad, auto batchdef) {
      auto rv = batchdef.getVal(rv1); \
      auto av = batchdef.getVal(av1); \
      for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
        decltype(auto) a = pad.rd_F(av, batchi);
        decltype(auto) r = pad.wr_DrawOpHandle(rv, batchi);
        if (cpu.curs[batchi]) {
          cpu.curs[batchi]->wr_F_unchecked(rMetricRef) = a;
          r = pad.pool->mk<DrawOpMinpackSearch>(
            alg,
            rMetricRef,
            searchDimsSpan,
            cpu.paramHashes[batchi],
            cpu.compiled.epoch,
            cpu.timeIndex,
            a);
        } 
        else {
          r = nullptr;
        }
      }
    });
    return rv;
  }
fail:
  return setInvalidArgs();
}

defusage(minpackSearch, R"(
  minimize.ALG(target ∊ F, params...)
  ALG cal be newton, grad, cgrad, bfgs, lbfgs, lbfgsb[recommended])");

