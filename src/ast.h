#pragma once
#include "./core.h"
#include "./token.h"

/*
  Adding a new AST node type in here automagically adds it several places: the enum AstType, the declaration of NodeEmitter::emit_XXX, etc.
*/

#define foreach_ast(X) \
  X(null) \
\
  X(formula) \
  X(assign) \
  X(api) \
\
  X(number) \
  X(im) \
  X(constant) \
  X(name) \
  X(hypobinding) \
  X(assoc) \
  X(with) \
\
  X(param) \
  X(dt) \
  X(time) \
  X(string) \
  X(symbol) \
  X(vector) \
  X(initial) \
  X(failCellCount) \
  X(failTime) \
\
  X(typecast) \
\
  X(cellref) \
  X(paramref) \
\
  X(index) \
\
  X(mul) \
  X(div) \
  X(add) \
  X(sub) \
  X(neg) \
  X(not) \
  X(left) \
  X(right) \
  X(has) \
\
  X(comppow) \
  X(compmul) \
  X(compdiv) \
  X(compadd) \
  X(compsub) \
  X(compeq) \
  X(compneq) \
  X(compge) \
  X(compgt) \
  X(comple) \
  X(complt) \
\
  X(compare) \
  X(lt) \
  X(le) \
  X(gt) \
  X(ge) \
  X(eq) \
  X(neq) \
  X(sim) \
  X(and) \
  X(or) \
  X(implies) \
  X(union) \
  X(intersection) \
  X(switch) \
  X(check) \
  X(checkat) \
  X(checkbetween) \
  X(at) \
  X(mbs) \
\
  X(repr) \
  X(sin) \
  X(cos) \
  X(tan) \
  X(sinh) \
  X(cosh) \
  X(tanh) \
  X(exp) \
  X(log) \
  X(sqrt) \
  X(sqr) \
  X(cbrt) \
  X(cube) \
  X(normangle) \
  X(normangle2) \
  X(abs) \
  X(sign) \
  X(floor) \
  X(ceil) \
  X(round) \
  X(fract) \
  X(step) \
  X(smoothstep) \
  X(easein) \
  X(easeout) \
  X(max) \
  X(min) \
  X(pow) \
  X(normalpdf) \
  X(normallogpdf) \
  X(normalcdf) \
  X(clamp) \
  X(lerp) \
  X(relu) \
  X(random) \
  X(randomNormal) \
\
  X(hysteresis) \
  X(roundhyst) \
  X(vel) \
  X(lpfilter1) \
  X(lpfilter2) \
  X(lookahead) \
  X(clampslew) \
  X(latch) \
  X(duration) \
  X(integrate) \
\
  X(lanczosWindow) \
  X(rectangularWindow) \
  X(tukeyWindow) \
  X(nuttallWindow) \
\
  X(real) \
  X(imag) \
  X(arg) \
  X(conj) \
  X(polar) \
\
  X(rotMat2) \
  X(rotMat3X) \
  X(rotMat3Y) \
  X(rotMat3Z) \
  X(rotMat4X) \
  X(rotMat4Y) \
  X(rotMat4Z) \
  X(quatMat4) \
  X(rotMat4Xderiv) \
  X(rotMat4Yderiv) \
  X(rotMat4Zderiv) \
  X(scaleMat2) \
  X(scaleMat3) \
  X(scaleMat4) \
  X(translateMat4) \
  X(homogeneous) \
  X(hnormalized) \
  X(fourierbasis) \
  X(polybasis) \
  X(chebbasis) \
\
  X(atan2) \
  X(dot) \
  X(length) \
  X(normalize) \
  X(distance) \
\
  X(outerProduct) \
  X(transpose) \
  X(determinant) \
  X(inverse) \
  X(sum) \
  X(prod) \
  X(solve) \
  X(posquat) \
\
  X(line) \
  X(spline) \
  X(arrow) \
  X(arcArrowX) \
  X(arcArrowY) \
  X(arcArrowZ) \
  X(disk) \
  X(tube) \
  X(rect) \
  X(text) \
  X(dial) \
  X(label) \
  X(solid) \
  X(sphere) \
  X(axisArrows) \
  X(transform) \
  X(colorize) \
  X(sweep) \
  X(graphColor) \
\
  X(policyTerrain) \
  X(linePlot) \
  X(bandPlot) \
  X(scatterPlot) \
  X(epicycles) \
\
  X(particleSearch) \
  X(minpackSearch) \
\
  X(cond) \
  X(fallback) \
  X(seq) \
  X(par) \
  X(piggyback) \
  X(isrunning) \
  X(issuccess) \
  X(isfailure) \
  X(run) \
  X(success) \
  X(failure) \
\
  X(head) \
  X(tail) \
  X(segment) \
  X(concat) \
  X(block) \
  X(linspacedMat) \
  X(onesMat) \
  X(zeroMat) \
  X(constantMat) \
  X(identityMat) \
  X(unitMat) \
  X(csvfileMat) \

  // ADD ast


enum AstType : U32 {
#define deftype(T) A_##T,
  foreach_ast(deftype)
#undef deftype
};

ostream & operator <<(ostream &s, AstType t);

struct AstNode {

  // Using variant instead of a union probably wastes 8 bytes :-(
  using Data = variant<monostate, U32, Val, F, char const *, TypeTag, Symbol>;

  AstType const t = A_null;
  int const nChildren = 0;
  FormulaSubstr loc;
  Data data_;
  AstNode *children[0]; // Extensible at alloc time. See FormulaParser::mkAst

  AstNode(AstType _t, vector<AstNode *> const &_children, Data _data)
   : t(_t),
     nChildren(_children.size()),
     data_(_data)
  {
    std::copy(_children.begin(), _children.end(), &children[0]);
  }

  AstNode(AstType _t, initializer_list<AstNode *> _children, Data _data)
   : t(_t),
     nChildren(_children.size()),
     data_(_data)
  {
    std::copy(_children.begin(), _children.end(), &children[0]);
  }

  static AstNode *mk(Arena *pool, AstType _t, vector<AstNode *> const &_children, Data _data)
  {
    return pool->mkExtra<AstNode>(_children.size() * sizeof(AstNode *), _t, _children, _data);
  }

  static AstNode *mk(Arena *pool, AstType _t, initializer_list<AstNode *> _children, Data _data)
  {
    return pool->mkExtra<AstNode>(_children.size() * sizeof(AstNode *), _t, _children, _data);
  }

  template<typename T> T& data() { return get<T>(data_); }

  string_view data_sv() { return string_view(data<char const *>()); }

  // Children accessors. Use these over the chidren[] array
  vector<AstNode *> chvec() const
  {
    return vector<AstNode *>(&children[0], &children[nChildren]);
  }

  inline AstNode *ch(int n) const
  {
    if (n >= 0 && n < nChildren) return children[n];
    return nullptr;
  }

  inline AstNode *chlast() const
  {
    if (nChildren > 0) return children[nChildren - 1];
    return nullptr;
  }

  inline int nch() const
  {
    return nChildren;
  }

};

ostream & operator <<(ostream &s, AstNode *a);
ostream & operator <<(ostream &s, WithSheet<AstNode *> sha);

void updateSubstr(FormulaSubstr &loc,  vector<U32> const &newPosByOldPos);
void updateAstSubstrs(AstNode *n, vector<U32> const &newPosByOldPos);


using AstNodeList = vector<AstNode *>;

optional<F> getConstF(AstNode *a);
optional<int> getConstInt(AstNode *a);
optional<CF> getConstCF(AstNode *a);
optional<string_view> getConstString(Arena *pool, AstNode *a);
optional<tuple<F, F>> getDtMultiple(AstNode *a);

bool isVector(AstNode *a);
bool isCellRef(AstNode *a);


struct AstHelp {

  static string help(AstType t, AstNode *n);
  static string help(AstType t);
  static string help(AstNode *n);

  static optional<string> helpByName(string_view name);
  static optional<string> helpByType(AstType t, AstNode *n);
  #define declhelp(T) static string help_##T(AstNode *n);
  foreach_ast(declhelp);
  #undef declhelp
};

#define defusage(T, S) string AstHelp::help_##T(AstNode *n) { return "Usage:" S; }
