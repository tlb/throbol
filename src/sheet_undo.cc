#include "./core.h"


template<typename T, typename ... Args>
T *Sheet::mkUndo(Args && ...args)
{
  auto it = make_unique<T>(std::forward<Args>(args)...);
  auto ret = it.get();
  undoLog.resize(undoLogPos);
  undoLog.push_back(std::move(it));
  undoLogPos = undoLog.size();
  ret->redo(*this);
  return ret;
}

void Sheet::undo()
{
  if (undoLogPos > 0) {
    undoLogPos --;
    undoLog[undoLogPos]->undo(*this);
  }
}

void Sheet::redo()
{
  if (undoLogPos < undoLog.size()) {
    undoLog[undoLogPos]->redo(*this);
    undoLogPos ++;
  }
}

SheetUndo *Sheet::getCurrentUndo()
{
  if (undoLogPos > 0) {
    if (auto oldu = undoLog[undoLogPos-1].get()) {
      if (!oldu->nomore) return oldu;
    }
  }
  return nullptr;
}

void Sheet::finalizeCurrentUndo()
{
  if (auto oldu = getCurrentUndo()) {
    oldu->nomore = true;
  }
}


struct SheetUndoMkCell : SheetUndo {

  string name;
  CellIndex cell = nullCellIndex;

  SheetUndoMkCell(string _name) : name(_name) {}

  void undo(Sheet &sh)
  {
    sh.killCell(cell);
  }
  void redo(Sheet &sh)
  {
    cell = sh.mkCell(name);
  }
  virtual void print(ostream &s, Sheet &sh)
  {
    s << name << " make";
  }
};

CellIndex Sheet::mkCellUndoable(string const &name)
{
  auto u = mkUndo<SheetUndoMkCell>(name);
  return u->cell;
}


struct SheetUndoSetParamValue : SheetUndo {

  vector<ParamIndex> pis;
  vector<F> prevs;
  vector<F> nexts;

  SheetUndoSetParamValue(vector<ParamIndex> const &_pis, vector<F> const &_nexts) : pis(_pis), nexts(_nexts) {}

  void undo(Sheet &sh) override
  {
    for (size_t i = 0; i < pis.size(); i++) {
      sh.setParamValue(pis[i], prevs[i]);
    }
  }
  void redo(Sheet &sh) override
  {
    prevs.resize(pis.size());
    for (size_t i = 0; i < pis.size(); i++) {
      prevs[i] = sh.params.valueByParam(pis[i]);
      sh.setParamValue(pis[i], nexts[i]);
    }
  }
  bool domoreSetParamValue(Sheet &sh, vector<ParamIndex> const &_pis, vector<F> const &_nexts) override
  {
    if (_pis == pis) {
      nexts = _nexts;
      for (size_t i = 0; i < pis.size(); i++) {
        sh.setParamValue(pis[i], nexts[i]);
      }
      return true;
    }
    return false;
  }
  virtual void print(ostream &s, Sheet &sh) override
  {
    for (size_t i = 0; i < pis.size(); i++) {
      if (i > 0) s << " ";
      s << sh.getParamDesc(pis[i]);
    }
    s << "\n  was " << prevs << "\n  now " << nexts;
  }
};

void Sheet::setParamValueUndoable(ParamIndex pi, F value)
{
  if (auto oldu = getCurrentUndo()) {
    if (oldu->domoreSetParamValue(*this, {pi}, {value})) return;
  }
  mkUndo<SheetUndoSetParamValue>(vector<CellIndex>{pi}, vector<F>{value});
}


void Sheet::setParamValuesUndoable(vector<ParamIndex> const &pis, vector<F> const &values)
{
  assertlog(pis.size() == values.size(), LOGV(pis) << LOGV(values));
  if (auto oldu = getCurrentUndo()) {
    if (oldu->domoreSetParamValue(*this, pis, values)) return;
  }
  mkUndo<SheetUndoSetParamValue>(pis, values);
}


struct SheetUndoKillCell : SheetUndo {

  CellIndex cell = nullCellIndex;
  string cellName;
  string cellFormula;

  SheetUndoKillCell(CellIndex _cell) : cell(_cell) {}

  void undo(Sheet &sh) override
  {
    cell = sh.mkCell(cellName);
    sh.setFormula(cell, cellFormula);
  }
  void redo(Sheet &sh) override
  {
    cellName = sh.nameByCell[cell];
    cellFormula = sh.formulaByCell[cell];
    sh.killCell(cell);
  }
  virtual void print(ostream &s, Sheet &sh) override
  {
    s << cellName << " delete";
  }
};

void Sheet::killCellUndoable(CellIndex cell)
{
  mkUndo<SheetUndoKillCell>(cell);
}

struct SheetUndoSetFormula : SheetUndo {

  CellIndex cell;
  string prev, next;

  SheetUndoSetFormula(CellIndex _cell, string _next) : cell(_cell), next(_next) {}

  void undo(Sheet &sh) override
  {
    sh.setFormula(cell, prev);
  }

  void redo(Sheet &sh) override
  {
    prev = sh.formulaByCell[cell];
    sh.setFormula(cell, next);
  }

  virtual bool domoreSetFormula(Sheet &sh, CellIndex _cell, string const &_formula) override
  {
    if (cell == _cell) {
      next = _formula;
      sh.setFormula(cell, _formula);
      return true;
    }
    return false;
  }

  virtual void print(ostream &s, Sheet &sh) override
  {
    s << sh.nameByCell[cell] << "\n  was " << prev << "\n  now " << next;
  }
};

void Sheet::setFormulaUndoable(CellIndex cell, string const &value)
{
  if (auto oldu = getCurrentUndo()) {
    if (oldu->domoreSetFormula(*this, cell, value)) return;
  }
  mkUndo<SheetUndoSetFormula>(cell, value);
}


struct SheetUndoMoveCells : SheetUndo {

  CellSet cells;
  vector<vec2> origPositions;
  vec2 delta;

  SheetUndoMoveCells(CellSet const &_cells, vec2 _delta) : cells(_cells), delta(_delta) {}

  void undo(Sheet &sh) override
  {
    size_t i = 0;
    for (auto cell : cells) {
      sh.posByCell[cell] = origPositions.at(i);
      i++;
    }
  }

  void redo(Sheet &sh) override
  {
    origPositions.clear();
    for (auto cell : cells) {
      origPositions.push_back(sh.posByCell[cell]);
    }

    for (auto cell : cells) {
      sh.posByCell[cell] += delta;
    }
  }

  virtual bool domoreMoveCells(Sheet &sh, CellSet const &_cells, vec2 _delta) override
  {
    if (cells == _cells) {
      delta += _delta;
      size_t i = 0;
      for (auto cell : cells) {
        sh.posByCell[cell] = origPositions.at(i) + delta;
        i++;
      }
      return true;
    }
    return false;
  }

  virtual void print(ostream &s, Sheet &sh) override
  {
    s << cells.size() << " cells drag " << delta;
  }

};



void Sheet::moveCellsUndoable(CellSet const &cells, vec2 value)
{
  if (auto oldu = getCurrentUndo()) {
    if (oldu->domoreMoveCells(*this, cells, value)) return;
  }

  mkUndo<SheetUndoMoveCells>(cells, value);
}




struct SheetUndoMoveIncludePages : SheetUndo {

  vector<PageIndex> pages;
  vector<vec2> origPositions;
  vec2 delta;
  bool withoutCells;

  SheetUndoMoveIncludePages(vector<PageIndex> const &_pages, vec2 _delta, bool _withoutCells) : pages(_pages), delta(_delta), withoutCells(_withoutCells) {}

  void undo(Sheet &sh) override
  {
    size_t i = 0;

    for (auto page : pages) {
      sh.setIncludePagePos(page, origPositions.at(i), withoutCells);
      i++;
    }
  }

  void redo(Sheet &sh) override
  {
    origPositions.clear();
    for (auto page : pages) {
      origPositions.push_back(sh.includePosByPage[page]);
    }

    size_t i = 0;
    for (auto page : pages) {
      sh.setIncludePagePos(page, origPositions.at(i) + delta, withoutCells);
      i++;
    }
  }


  virtual bool domoreMoveIncludePages(Sheet &sh, vector<PageIndex> const &_pages, vec2 _delta, bool _withoutCells) override
  {
    if (pages == _pages && withoutCells == _withoutCells) {
      delta += _delta;
      size_t i = 0;
      for (auto page : pages) {
        sh.setIncludePagePos(page, origPositions.at(i) + delta, withoutCells);
        i++;
      }
      return true;
    }
    return false;
  }

  virtual void print(ostream &s, Sheet &sh) override
  {
    s << pages.size() << " pages drag " << delta;
  }

};



void Sheet::moveIncludePagesUndoable(vector<PageIndex> pages, vec2 delta, bool withoutCells)
{
  if (auto oldu = getCurrentUndo()) {
    if (oldu->domoreMoveIncludePages(*this, pages, delta, withoutCells)) return;
  }

  mkUndo<SheetUndoMoveIncludePages>(pages, delta, withoutCells);
}
