#include "../src/api.h"


struct SchemaApiEnt {
  bool exists = false;
  string apiVersion;
  string apiType;
  YAML::Node apiSchema;
};

struct SchemaItemEnt {
  bool exists = false;
  string type;
  string dir;
  double ofs = -1.0;
  double version = -1.0;
  double size = -1.0;
};

bool checkSchemaMatch(YAML::Node const &localApiSchemaYaml, YAML::Node const &remoteApiSchemaYaml, ostream &l)
{
  bool ok = true;

  l << "schema comparison:\n";

  array<YAML::Node, 2> schemas {localApiSchemaYaml, remoteApiSchemaYaml};

  map<string, array<SchemaApiEnt, 2>> byInstance;
  for (auto which : {0, 1}) {
    if (schemas[which].IsSequence()) {
      for (auto it : schemas[which]) {
        string k = it["apiInstance"].as<string>("?");
        auto &v = byInstance[k][which];
        v.exists = true;
        v.apiVersion = it["apiVersion"].as<string>("?");
        v.apiType = it["apiType"].as<string>("?");
        v.apiSchema = it["apiSchema"];
      }
    }
    else {
      l << "  schema " << which << " missing\n";
      ok = false;
    }
  }

  for (auto &apiIt : byInstance) {
    l << "  instance " << shellEscape(apiIt.first) << "\n";
    {
      auto &[a, b] = apiIt.second;
      if (a.apiType != b.apiType) {
        l << "    type mismatch: " << shellEscape(a.apiType) << " vs " << shellEscape(b.apiType) << "\n";
        ok = false;
      }
      else {
        l << "    type: " << shellEscape(a.apiType) << "\n";
      }
      if (a.apiVersion != b.apiVersion) {
        l << "    version mismatch: " << shellEscape(a.apiVersion) << " vs " << shellEscape(b.apiVersion) << "\n";
        ok = false;
      }
      else {
        l << "    version: " << shellEscape(a.apiVersion) << "\n";
      }
    }

    map<string, std::array<SchemaItemEnt, 2>> byName;
    for (auto which : {0, 1}) {
      auto &apiSchema = apiIt.second[which].apiSchema;
      if (apiSchema.IsSequence()) {
        for (auto it = apiSchema.begin(); it != apiSchema.end(); it++) {
          string name = (*it)["name"].as<string>();
          auto &v = byName[name][which];

          v.exists = true;
          v.type = (*it)["type"].as<string>("?");
          v.dir = (*it)["dir"].as<string>("?");
          v.ofs = (*it)["ofs"].as<double>(-1.0);
          v.version = (*it)["version"].as<double>(-1.0);
          v.size = (*it)["size"].as<double>(-1.0);
        }
      }
      else {
        l << "    apiSchema " << which << " missing\n";
        ok = false;
      }
    }
    
    int errCount = 0;
    for (auto const &itemIt : byName) {
      auto &[a, b] = itemIt.second;
      if (!a.exists) {
        errCount++;
        if (errCount <= 5) {
          l << "    " << itemIt.first << ": missing remote\n";
        }
        ok = false;
      }
      else if (!b.exists) {
        errCount++;
        if (errCount <= 5) {
          l << "    " << itemIt.first << ": missing local\n";
        }
        ok = false;
      }
      else if (a.dir != b.dir || a.ofs != b.ofs || a.type != b.type || a.version != b.version || a.size != b.size) {
        errCount++;
        if (errCount <= 5) {
          l << "    " << itemIt.first << ": " << 
            a.type << "(dir=" << a.dir << ", ofs=" << a.ofs << ", ver=" << a.version << ", size=" << a.size << ")" <<
            " vs " <<
            b.type << "(dir=" << b.dir << ", ofs=" << b.ofs << ", ver=" << b.version << ", size=" << b.size << ")" <<
            "\n";
        }
        ok = false;
      }
    }
    if (errCount > 5) {
      l << "    (and " << (errCount - 5) << " more)\n";
    }
  }
  
  return ok;
}
