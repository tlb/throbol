#define CATCH_CONFIG_EXTERNAL_INTERFACES
#include "./test_utils.h"
#include <regex>
using std::regex;
#include <sys/resource.h>

long overridePerfIterCount = 0;


bool isMatch(string const &l, string const &re)
{
  std::regex re1(re);
  return regex_search(l, re1);
}

bool mkSheetCheck(Sheet &sh, char const *dump, char const *fn)
{
  if (!sh.loadTest(dump, fn)) return false;
  if (!checkSheet(sh)) return false;
  return true;
}


bool checkSheet(Sheet &sh)
{
  bool ok = true;
  for (auto &it : sh.errors()) {
    FAIL_CHECK(it);
    ok = false;
  }
  if (!sh.compiled) {
    FAIL_CHECK("Compile failed");
  }
  else if (!sh.compiled->orderProblems.empty()) {
    FAIL_CHECK("order problems");
  }
  return ok;
}

void checkCpu(CpuState &cpu)
{
  for (auto cell : cpu.checkFailures[0]) {
    FAIL_CHECK(cpu.sh.nameByCell[cell] + " (returned " + repr(cpu.curs[0]->scopedPtrsByCell(cell)) + ")");
  }
}

void checkError(Sheet &sh, string const &match)
{
  for (auto &err : sh.errors()) {
    if (isMatch(err, match)) return;
  }
  FAIL_CHECK("Expected error " + match + " but got only " + shellEscape(sh.errors()));
}

R usDiff(timeval const &t0, timeval const &t1)
{
  return R(t1.tv_usec - t0.tv_usec) + 1000000.0 * R(t1.tv_sec - t0.tv_sec);
}

void perfReport(string const &desc, std::function<void()> f, double subIterCount)
{
  long iters = overridePerfIterCount ? overridePerfIterCount : (long)1;
  R goalTime = 0.05;
  R bestTime = 0;
  int bestCount = 0;
  while (1) {
    R t0 = realtime();
    for (auto iter = 0; iter < iters; iter++) {
      f();
    }
    R t1 = realtime();
    R elapsed = t1 - t0;
    if (!overridePerfIterCount) {
      if (elapsed < goalTime / 200.0) {
        iters *= 100;
        bestTime = 0;
        bestCount = 0;
        continue;
      }
      else if (elapsed < goalTime / 20.0) {
        iters *= 10;
        bestTime = 0;
        bestCount = 0;
        continue;
      }
      else if (elapsed < goalTime / 2.0) {
        iters *= 2;
        bestTime = 0;
        bestCount = 0;
        continue;
      }
    }
    if (bestTime == 0 || elapsed < bestTime) bestTime = elapsed;
    bestCount ++;
    if (bestCount >= 3 || bestTime > 1.0) {
      ostringstream logline;
      logline << desc << " time: " << repr_time(elapsed);
      if (subIterCount != 1) {
        logline << " (" << repr_time(elapsed/subIterCount) << " per)";
      }
      logline << " avg (n=" + to_string(iters) <<
        ") with " << DEBUGFLAGS << "\n";
      cerr << logline.str();

      char timebuf[256];
      tm tmbuf;
      time_t t = time(NULL);
      strftime(timebuf, sizeof(timebuf), "%Y-%m-%d %H:%M:%S", gmtime_r(&t, &tmbuf));
 
      ofstream of("logs/perflog.txt", std::ios::app);
      of << timebuf << " " << logline.str();
      break;
    }
  }
}


shared_ptr<CellVals> sampleSheetData(Sheet &sh, F time)
{
  auto ref = CellVals::mk(sh.compiled.get());
  auto prev = CellVals::mk(sh.compiled.get());
  CpuState cpu(*sh.compiled, &sh.sheetlife, ref.get(), prev.get());
  cpu.times[0] = time;
  cpu.execSheet();
  checkCpu(cpu);
  return ref;
}
