#include "./emit.h"
#include "./emit_macros.h"
#include "numerical/windowfunc.h"

deftype(lanczosWindow)
{
  return typetag<F>();
}

defemit(lanczosWindow)
{
  defop1(F, F, r = float(lanczosWindow(double(a))));
  return setInvalidArgs();
}

defusage(lanczosWindow, R"(
  lanczosWindow(t ∊ F))");


deftype(rectangularWindow)
{
  return typetag<F>();
}

defemit(rectangularWindow)
{
  defop1(F, F, r = float(rectangularWindow(double(a))));
  return setInvalidArgs();
}

defusage(rectangularWindow, R"(
  rectangularWindow(t ∊ F))");


deftype(tukeyWindow)
{
  return typetag<F>();
}

defemit(tukeyWindow)
{
  defop1(F, F, r = float(tukeyWindow(double(a), 0.5)));
  defop2(F, F, F, r = float(tukeyWindow(double(a), double(b))));
  return setInvalidArgs();
}

defusage(tukeyWindow, R"(
  tukeyWindow(t ∊ F) with alpha = 0.5
  tukeyWindow(t ∊ F, alpha ∊ F))");


deftype(nuttallWindow)
{
  return typetag<F>();
}

defemit(nuttallWindow)
{
  defop1(F, F, r = float(nuttallWindow(double(a))));
  return setInvalidArgs();
}

defusage(nuttallWindow, R"(
  nuttallWindow(t ∊ F))");
