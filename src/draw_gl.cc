#include "./core.h"
#include "../geom/gl_headers.h"
#include "../geom/g8.h"
#include "numerical/numerical.h"
#include "numerical/polyfit.h"
#include "./sheet_ui.h"
#include "./scope_math.h"
#include "./draw_gl.h"
#include <unsupported/Eigen/Splines>

DrawTraceAccum::DrawTraceAccum(G8DrawList &_dl, Sheet &_sh, Trace &_tr, CellIndex _cell, bool _cellPicked)
  : dl(_dl),
    sh(_sh),
    tr(_tr),
    cell(_cell),
    cellPicked(_cellPicked)
{
}

DrawTraceAccum::~DrawTraceAccum()
{
}

static F defaultLayer(DrawOpType op)
{
  switch (op) {
    case DrawOpLine::tid:        return 0.002f;
    case DrawOpLines::tid:       return 0.002f;
    case DrawOpSpline::tid:      return 0.008f;
    case DrawOpArrow::tid:       return 0.1f;
    case DrawOpArcArrow::tid:    return 0.006f;
    case DrawOpDisk::tid:        return 0.005f;
    case DrawOpTube::tid:        return 0.004f;
    case DrawOpRect::tid:        return 0.001f;
    case DrawOpText::tid:        return 0.008f;
    case DrawOpSolid::tid:       return 0.1f;
    case DrawOpSphere::tid:      return 0.1f;
    case DrawOpAxisArrows::tid:  return 0.1f;
    case DrawOpDial::tid:        return 0.009f;
    case DrawOpLinePlot::tid:    return 0.05f;
    case DrawOpBandPlot::tid:    return 0.05f;
    case DrawOpScatterPlot::tid: return 0.05f;
    case DrawOpEpicycles::tid:   return 0.010f;
    case DrawOpMujocoScene::tid: return 0.0f;

    default:                     return 0.0f;
  }
}

void DrawTraceAccum::addRange(BoxedRef br, F alpha, SheetLayoutCell const &lo)
{
  mat4 *posx = dtalife.mk<mat4>(Transform<F, 3, Projective>(Translation3f(lo.origin)).matrix());
  mat4 *colorx = dtalife.mk<mat4>(mat4::Identity());
  (*colorx)(3, 3) *= alpha;

  bool onlyCursTime = true;
  bool firstSample = true;
  for (auto [li, xrel] : lo.samples()) {
    timeIndex = li;
    accumIndex = 0;
    if (auto vals = tr.getVal(timeIndex)) {
      if (auto val = vals->rd_DrawOpHandle(br)) { // can be null if this cell had a compile error
        addTree(
          val,
          posx,
          colorx,
          nullptr,
          (li - lo.g.cursIndex) * tr.dt,
          0.0f,
          lo.thicken,
          onlyCursTime);
      }
    }

    /*
      If the first time we draw we see nothing that draws in any step other than the current one
      (very common for renderings) then only do the current time and end.
    */
    if (firstSample && onlyCursTime) {
      if (auto vals = tr.getVal(lo.g.cursIndex)) {
        if (auto val = vals->rd_DrawOpHandle(br)) {
          addTree(
            val,
            posx,
            colorx,
            nullptr,
            0.0,
            0.0f,
            lo.thicken,
            onlyCursTime);
        }
      }
      break;
    }
    firstSample = false;
  }
}

void DrawTraceAccum::addTree(DrawOpHandle opgen, mat4 const *posx, mat4 const *colorx, Assocp options, F offCursor, F visRange, F thicken, bool &onlyCursTime)
{
  if (!opgen.it) return;

  options = merge(&dtalife, options, opgen.props);

  if (0) L() << "addTree " << opgen.it->op << " " << WithSheet(sh, options);

  switch (opgen.it->op) {

    case DrawOpNull::tid:
      break;

    case DrawOpUnion::tid:
      {
        auto op = opgen.it->as<DrawOpUnion>();
        addTree(op->lhs, posx, colorx, options, offCursor, visRange, thicken, onlyCursTime);
        addTree(op->rhs, posx, colorx, options, offCursor, visRange, thicken, onlyCursTime);
      }
      break;

    case DrawOpTransform::tid:
      {
        auto op = opgen.it->as<DrawOpTransform>();
        mat4 *newPosx = dtalife.mk<mat4>(*posx);
        *newPosx *= op->transform;
        addTree(op->contents, newPosx, colorx, options, offCursor, visRange, thicken, onlyCursTime);
      }
      break;

    case DrawOpColor::tid:
      {
        auto op = opgen.it->as<DrawOpColor>();
        mat4 *newColorx = dtalife.mk<mat4>(*colorx);
        *newColorx *= op->transform;
        addTree(op->contents, posx, newColorx, options, offCursor, visRange, thicken, onlyCursTime);
      }
      break;

    case DrawOpSweep::tid:
      {
        auto op = opgen.it->as<DrawOpSweep>();
        if (op->visRange > 0.0f) {
          onlyCursTime = false;
          mat4 *newPosx = dtalife.mk<mat4>(*posx);
          mat4 *newColorx = dtalife.mk<mat4>(*colorx);
          (*newPosx)(0, 3) += 1.5f * offCursor/op->visRange;
          (*newColorx)(3,3) *= offCursor == 0.0f ? 1.0f : 0.33f;
          addTree(op->contents, 
            newPosx,
            newColorx,
            options, offCursor, op->visRange, thicken, onlyCursTime);
        }
        else {
          addTree(op->contents, posx, colorx, options, offCursor, op->visRange, thicken, onlyCursTime);
        }
      }
      break;

    case DrawOpLinePlot::tid:
      {
        onlyCursTime = false;
        auto op = opgen.it->as<DrawOpLinePlot>();
        plotPts(accumIndex, 0).emplace_back(op->x, op->y, op->z, timeIndex);
      }
      goto add_cmd;

    case DrawOpBandPlot::tid:
      {
        onlyCursTime = false;
        auto op = opgen.it->as<DrawOpBandPlot>();
        plotPts(accumIndex, 0).emplace_back(op->x, op->y0, op->z, timeIndex);
        plotPts(accumIndex, 1).emplace_back(op->x, op->y1, op->z, timeIndex);
      }
      goto add_cmd;

    case DrawOpScatterPlot::tid:
      {
        onlyCursTime = false;
        auto op = opgen.it->as<DrawOpScatterPlot>();
        plotPts(accumIndex, 0).emplace_back(op->x, op->y, op->z, timeIndex);
      }
      goto add_cmd;

    case DrawOpEpicycles::tid:
      {
        onlyCursTime = false;
        auto op = opgen.it->as<DrawOpEpicycles>();
        CF pti(0, 0);
        for (int rowi = 0; rowi < op->v.rows(); rowi++) {
          pti += op->v(rowi, 0);
          plotPts(accumIndex, rowi).emplace_back(pti.real(), pti.imag(), 0.0f, timeIndex);
        }
      }
      goto add_cmd;

    case DrawOpLine::tid:
    case DrawOpLines::tid:
    case DrawOpSpline::tid:
    case DrawOpArrow::tid:
    case DrawOpArcArrow::tid:
    case DrawOpDisk::tid:
    case DrawOpTube::tid:
    case DrawOpRect::tid:
    case DrawOpText::tid:
    case DrawOpSolid::tid:
    case DrawOpSphere::tid:
    case DrawOpAxisArrows::tid:
    case DrawOpDial::tid:
    case DrawOpPolicyTerrain::tid:
    case DrawOpParticleSearch::tid:
    case DrawOpMinpackSearch::tid:
    case DrawOpMujocoScene::tid:
    // ADD DrawOp
    add_cmd:
      if (visRange != 0.0f) onlyCursTime = false;
      if (abs(offCursor) <= visRange) {
        separateCmds.emplace_back(
          posx,
          colorx,
          defaultLayer(opgen.it->op) - abs(offCursor) / max(1.0f, visRange),
          thicken,
          accumIndex,
          options,
          opgen.it);
      }
      accumIndex++;
      break;

    default:
      L() << "Unknown drawop. Did you forget to add it in DrawTraceAccum::addTree?";
  }
}

struct RangeSpec {
  F x0 = 0.0, x1 = 0.0;
  F y0 = 0.0, y1 = 0.0;
  F z0 = 0.0, z1 = 0.0;

  void finalize()
  {
    if (x0 < 0.0f && x1 > 0.0f) {
      x1 = max(x1, -x0);
      x0 = -x1;
    }
    else if (x0 == z1) {
      x1 += 1.0f;
    }
    if (y0 < 0.0f && y1 > 0.0f) {
      y1 = max(y1, -y0);
      y0 = -y1;
    }
    else if (y0 == y1) {
      y1 += 1.0f;
    }
    if (z0 < 0.0f && z1 > 0.0f) {
      z1 = max(z1, -z0);
      z0 = -z1;
    }
    else if (z0 == z1) {
      z0 -= 1.0f;
      z1 += 1.0f;
    }
  }

  F convX(F xRaw) { return ((xRaw - x0) / (x1 - x0)) * 4.0f - 2.0f; }
  F convY(F yRaw) { return ((yRaw - y0) / (y1 - y0)) * 4.0f - 2.0f; }
  F convZ(F zRaw) { return ((zRaw - z0) / (z1 - z0)) * 4.0f - 2.0f; }

  vec3 conv2(F xRaw, F yRaw, F zRaw)
  {
    return vec3(
      ((xRaw - x0) / (x1 - x0)) * 4.0f - 2.0f,
      ((yRaw - y0) / (y1 - y0)) * 4.0f - 2.0f,
      ((zRaw - z0) / (z1 - z0)) * 4.0f - 2.0f
    );
  }
};

RangeSpec autoRange(vector<DrawTraceAccum::PlotPt> const &pts, DrawTraceAccum::Cmd &cmd)
{
  RangeSpec ret;

  for (auto const &pt : pts) {
    ret.x0 = min(ret.x0, pt.x);
    ret.x1 = max(ret.x1, pt.x);

    ret.y0 = min(ret.y0, pt.y);
    ret.y1 = max(ret.y1, pt.y);

    ret.z0 = min(ret.z0, pt.z);
    ret.z1 = max(ret.z1, pt.z);
  }
  ret.finalize();
  
  if (auto it = assoc_range(cmd.options, "range_x")) {
    ret.x0 = (*it)(0);
    ret.x1 = (*it)(1);
  }
  if (auto it = assoc_range(cmd.options, "range_y")) {
    ret.y0 = (*it)(0);
    ret.y1 = (*it)(1);
  }
  if (auto it = assoc_range(cmd.options, "range_z")) {
    ret.z0 = (*it)(0);
    ret.z1 = (*it)(1);
  }

  return ret;
}


void DrawTraceAccum::finish()
{
  timeIndex = -999; // don't use this anymore

  // separateCmds elements are kinda big, so we make an aux array to sort by
  vector<Cmd *> sortOrder;
  for (auto &it : separateCmds) {
    sortOrder.push_back(&it);
  }

  sort(sortOrder.begin(), sortOrder.end(), [](Cmd const *a, Cmd const *b) { 
    // sort first by layer, then by accumIndex
    if (a->layer < b->layer) return true;
    if (b->layer < a->layer) return false;
    return a->index < b->index;
  });

  for (auto it : sortOrder) {
    auto opgen = it->drawOp;
    switch (opgen.it->op) {

      case DrawOpNull::tid:
        break;

      case DrawOpLinePlot::tid:
        drawOpFinal(*it, static_cast<DrawOpLinePlot *>(opgen.it));
        break;

      case DrawOpBandPlot::tid:
        drawOpFinal(*it, static_cast<DrawOpBandPlot *>(opgen.it));
        break;

      case DrawOpScatterPlot::tid:
        drawOpFinal(*it, static_cast<DrawOpScatterPlot *>(opgen.it));
        break;

      case DrawOpPolicyTerrain::tid:
        drawOpFinal(*it, static_cast<DrawOpPolicyTerrain *>(opgen.it));
        break;

      case DrawOpParticleSearch::tid:
        drawOpFinal(*it, static_cast<DrawOpParticleSearch *>(opgen.it));
        break;

      case DrawOpMinpackSearch::tid:
        drawOpFinal(*it, static_cast<DrawOpMinpackSearch *>(opgen.it));
        break;

      case DrawOpSolid::tid:
        drawOpFinal(*it, static_cast<DrawOpSolid *>(opgen.it));
        break;

      case DrawOpSphere::tid:
        drawOpFinal(*it, static_cast<DrawOpSphere *>(opgen.it));
        break;

      case DrawOpAxisArrows::tid:
        drawOpFinal(*it, static_cast<DrawOpAxisArrows *>(opgen.it));
        break;

      case DrawOpLine::tid:
        drawOpFinal(*it, static_cast<DrawOpLine *>(opgen.it));
        break;

      case DrawOpLines::tid:
        drawOpFinal(*it, static_cast<DrawOpLines *>(opgen.it));
        break;

      case DrawOpSpline::tid:
        drawOpFinal(*it, static_cast<DrawOpSpline *>(opgen.it));
        break;

      case DrawOpRect::tid:
        drawOpFinal(*it, static_cast<DrawOpRect *>(opgen.it));
        break;

      case DrawOpDisk::tid:
        drawOpFinal(*it, static_cast<DrawOpDisk *>(opgen.it));
        break;

      case DrawOpTube::tid:
        drawOpFinal(*it, static_cast<DrawOpTube *>(opgen.it));
        break;

      case DrawOpArrow::tid:
        drawOpFinal(*it, static_cast<DrawOpArrow *>(opgen.it));
        break;

      case DrawOpArcArrow::tid:
        drawOpFinal(*it, static_cast<DrawOpArcArrow *>(opgen.it));
        break;

      case DrawOpDial::tid:
        drawOpFinal(*it, static_cast<DrawOpDial *>(opgen.it));
        break;

      case DrawOpText::tid:
        drawOpFinal(*it, static_cast<DrawOpText *>(opgen.it));
        break;

      case DrawOpEpicycles::tid:
        drawOpFinal(*it, static_cast<DrawOpEpicycles *>(opgen.it));
        break;

      case DrawOpMujocoScene::tid:
        drawOpFinal(*it, static_cast<DrawOpMujocoScene *>(opgen.it));
        break;

      // ADD DrawOp

      default:
        L() << "Unknown drawop. Did you forget to add it in DrawTraceAccum::finish?";
    }
  }
}


void DrawTraceAccum::drawOpFinal(Cmd &cmd, DrawOpLine *op)
{
  vector<vec3> pts {
    cmd.tPos(op->p0),
    cmd.tPos(op->p1)
  };
  dl.addThickCurve(pts,
    vec3(0, 0, 0),
    cmd.getColor(),
    cmd.getThickness(0.05),
    0.0f,
    &glowingTextShader);
}


void DrawTraceAccum::drawOpFinal(Cmd &cmd, DrawOpLines *op)
{
  vector<vec3> pts;
  if (op->pts.rows() == 3) {
    for (Index i = 0; i < op->pts.cols(); i++) {
      pts.push_back(cmd.tPos(op->pts.col(i)));
    }
  }
  else if (op->pts.rows() == 2) {
    for (Index i = 0; i < op->pts.cols(); i++) {
      pts.push_back(cmd.tPos(append0(vec2(op->pts.col(i)))));
    }
  }
  else {
    return;
  }
  dl.addThickCurve(pts,
    vec3(0, 0, 0),
    cmd.getColor(),
    cmd.getThickness(0.05),
    cmd.layer,
    &glowingTextShader);
}

void DrawTraceAccum::drawOpFinal(Cmd &cmd, DrawOpSolid *op)
{
  string fullpath;
  if (op->fn.size() >= 1 && op->fn[0] == '/') {
    fullpath = op->fn;
  }
  else {
    fullpath = string(op->pageFileDir) + "/"s + string(op->fn);
  }
  dl.addSolid(
    fullpath,
    *cmd.posx,
    cmd.getColor(),
    cmd.layer);
}

void DrawTraceAccum::drawOpFinal(Cmd &cmd, DrawOpArrow *op)
{
  vec3 finalpos = cmd.tPos(op->origin);
  vec3 finaldir = cmd.tDir(op->dir);

  vec3 orthog1 = finaldir.cross(vec3(0, 0, 1));
  if (orthog1.squaredNorm() < 1e-6f) {
    orthog1 = finaldir.cross(vec3(0, 1, 0));
  }
  if (orthog1.squaredNorm() < 1e-6f) {
    return;
  }
  orthog1 = orthog1.normalized();
  
  vec3 orthog2 = finaldir.cross(orthog1).normalized();
  F fatness = min(0.2f, finaldir.norm()*0.2f) + 0.1f;

  dl.addArrow(
    finalpos,
    finaldir,
    fatness * orthog1,
    fatness * orthog2,
    cmd.getColor(),
    cmd.layer,
    &litSolidShader);
}

void DrawTraceAccum::drawOpFinal(Cmd &cmd, DrawOpSphere *op)
{
  dl.addSphere(cmd.tPos(op->center),
    cmd.tDir(vec3(op->rad, 0, 0)),
    cmd.tDir(vec3(0, op->rad, 0)),
    cmd.tDir(vec3(0, 0, op->rad)),
    cmd.getColor(),
    cmd.layer,
    &litSolidShader);
}

void DrawTraceAccum::drawOpFinal(Cmd &cmd, DrawOpAxisArrows *op)
{
  dl.addArrowTriple(
    *cmd.posx,
    cmd.layer,
    &litSolidShader);
}


void DrawTraceAccum::drawOpFinal(Cmd &cmd, DrawOpSpline *op)
{
  using Spline3d = Eigen::Spline<F, 3, 3>;
  using SplineFitting3d = Eigen::SplineFitting<Spline3d>;
  assert(op->pts.rows() == 3);

  if (0) { // draw control points for debugging
    for (auto ci = 0; ci < op->pts.cols(); ci++) {
      dl.addCircle(
        cmd.tPos(op->pts.col(ci)),
        cmd.tDir(vec3(0.03f * cmd.thicken, 0, 0)),
        cmd.tDir(vec3(0, 0.03f * cmd.thicken, 0)),
        cmd.getColor(),
        cmd.layer,
        &glowingTextShader);
    }
  }

  auto fit = Spline3d(SplineFitting3d::Interpolate(op->pts, 3));
  if (0) L() << "spline pts=" << op->pts;
  vector<vec3> pts;
  pts.reserve(65);
  for (F u = 0.0f; u <= 1.0f; u += 1.0f/64.0f) {
    pts.push_back(cmd.tPos(fit(u)));
  }
  dl.addThickCurve(pts,
    vec3(0, 0, 1),
    cmd.getColor(),
    cmd.getThickness(0.05),
    cmd.layer,
    &glowingTextShader);
}

void DrawTraceAccum::drawOpFinal(Cmd &cmd, DrawOpLinePlot *op)
{
  auto &rawPts = plotPts(cmd.index, 0);

  auto rangeSpec = autoRange(rawPts, cmd);

  vector<vec3> pts;
  for (auto &pt : rawPts) {
    pts.push_back(cmd.tPos(rangeSpec.conv2(pt.x, pt.y, pt.z)));
  }
  for (auto &pt : pts) {
    maxWidth = max(maxWidth, pt.x());
    maxHeight = max(maxHeight, -pt.y());
  }

  dl.addThickCurve(pts, vec3(0, 0, 1), cmd.getPlotColor(), cmd.getThickness(0.04),
    cmd.layer, &glowingTextShader);

  vec3 pt = cmd.tPos(rangeSpec.conv2(op->x, op->y, op->z));

  dl.addCircle(pt, vec3(0.05f * cmd.thicken, 0, 0), vec3(0, 0.05f * cmd.thicken, 0), cmd.getPlotColor(),
    cmd.layer, &glowingTextShader);
  dl.addCircle(pt, vec3(0.02f * cmd.thicken, 0, 0), vec3(0, 0.02f * cmd.thicken, 0), fgcolor(0xff, 0xff, 0xff, 0xff),
    cmd.layer, &glowingTextShader);
  dl.addText(
    pt + vec3(0.1, 0, 0),
    vec3(fontSize, 0, 0),
    vec3(0, -fontSize, 0),
    vec3(0, 0, 1),
    repr_0_3g(op->x) + ", " + repr_0_3g(op->y),
    fgcolor(0xff, 0xff, 0xff, 0xff),
    ysFonts.lrgFont,
    0, 0.5f,
    cmd.layer,
    &glowingTextShader);

  {
    vec4 axcol = fgcolor(0xff, 0xff, 0xff, 0xcc);
    F labelVal = 1.0f;
    vec3 origin = cmd.tPos(rangeSpec.conv2(0, 0, 0));

    if (auto labelX = assoc_string_view(cmd.options, "label_x")) {
      vec3 xTip = cmd.tPos(rangeSpec.conv2(rangeSpec.x1, 0, 0));

      dl.addLine(origin, axcol, xTip, axcol,
        cmd.layer + 0.1f, &rawColorShader);

      dl.addText(
        xTip,
        vec3(fontSize, 0, 0),
        vec3(0, -fontSize, 0),
        vec3(0, 0, 1),
        string(*labelX) + "=" + repr_0_3g(labelVal * rangeSpec.x1),
        fgcolor(vec4(1.0, 1.0, 1.0, 1.0)),
        ysFonts.lrgFont,
        0.0f, 0.5f,
        cmd.layer, &glowingTextShader);
    }
    if (auto labelY = assoc_string_view(cmd.options, "label_y")) {

      vec3 yTip = cmd.tPos(rangeSpec.conv2(0, rangeSpec.y1, 0));
      dl.addLine(origin, axcol, yTip, axcol, 
        cmd.layer + 0.1f, &rawColorShader);

      dl.addText(
        yTip,
        vec3(fontSize, 0, 0),
        vec3(0, -fontSize, 0),
        vec3(0, 0, 1),
        string(*labelY) + "=" + repr_0_3g(labelVal * rangeSpec.y1),
        fgcolor(vec4(1.0, 1.0, 1.0, 1.0)),
        ysFonts.lrgFont,
        0.5f, 1.0f,
        cmd.layer, &glowingTextShader);

    }
  }
}



void DrawTraceAccum::drawOpFinal(Cmd &cmd, DrawOpBandPlot *op)
{
  auto &rawPts0 = plotPts(cmd.index, 0);
  auto &rawPts1 = plotPts(cmd.index, 1);

  auto rangeSpec = autoRange(rawPts1, cmd); // ?

  vector<pair<vec3, vec3>> pts;
  assert(rawPts0.size() == rawPts1.size());
  size_t n = rawPts0.size();
  for (size_t i = 0; i < n; i++) {
    auto &pt0 = rawPts0[i];
    auto &pt1 = rawPts1[i];
    pts.emplace_back(
      cmd.tPos(rangeSpec.conv2(pt0.x, pt0.y, pt0.z)),
      cmd.tPos(rangeSpec.conv2(pt1.x, pt1.y, pt1.z)));
  }
  dl.addBand(pts, vec3(0, 0, 1), cmd.getPlotColor(),
    cmd.layer, &glowingTextShader);


#if 0
  vec3 pt0 = cmd.tPos(rangeSpec.conv2(op->x, op->y0, 0.0));
  vec3 pt1 = cmd.tPos(rangeSpec.conv2(op->x, op->y1, 0.0));

  dl.addCircle(pt0, vec3(0.05 * cmd.thicken, 0, 0), vec3(0, 0.05 * cmd.thicken, 0), getPlotColor(cmd), cmd.layer + 0.05f,
    &glowingTextShader);          
  dl.addCircle(pt0, vec3(0.02 * cmd.thicken, 0, 0), vec3(0, 0.02 * cmd.thicken, 0), fgcolor(0xff, 0xff, 0xff, 0xff), cmd.layer + 0.051f,
    &glowingTextShader);          

  dl.addCircle(pt1, vec3(0.05 * cmd.thicken, 0, 0), vec3(0, 0.05 * cmd.thicken, 0), getPlotColor(cmd), cmd.layer + 0.05f,
    &glowingTextShader);          
  dl.addCircle(pt1, vec3(0.02 * cmd.thicken, 0, 0), vec3(0, 0.02 * cmd.thicken, 0), fgcolor(0xff, 0xff, 0xff, 0xff), cmd.layer + 0.051f,
    &glowingTextShader);          
  dl.addText(
    pt1,
    vec3(fontSize, 0, 0),
    vec3(0, -fontSize, 0),
    vec3(0, 0, 1),
    repr_0_3g(op->x) + ", " + repr_0_3g(op->y0) + " - " + repr_0_3f(op->y1),
    fgcolor(vec4(1.0, 1.0, 1.0, 1.0)),
    ysFonts.lrgFont,
    0, 0.5f,
    cmd.layer, &glowingTextShader);
#endif
}


void DrawTraceAccum::drawOpFinal(Cmd &cmd, DrawOpScatterPlot *op)
{
  auto &rawPts = plotPts(cmd.index, 0);

  auto rangeSpec = autoRange(rawPts, cmd);

  vector<vec3> pts;
  for (auto const &rawPt : rawPts) {
    vec3 pt1 = cmd.tPos(rangeSpec.conv2(rawPt.x, rawPt.y, rawPt.z));
    dl.addCircle(pt1, vec3(0.02f * cmd.thicken, 0, 0), vec3(0, 0.02f * cmd.thicken, 0), cmd.getPlotColor(),
      cmd.layer + 0.01f + 0.001f * cmd.index, &litSolidShader);
  }

  vec3 pt = cmd.tPos(rangeSpec.conv2(op->x, op->y, op->z));
  dl.addCircle(pt, vec3(0.05f * cmd.thicken, 0, 0), vec3(0, 0.05f * cmd.thicken, 0), cmd.getPlotColor(), 
    cmd.layer + 0.05f, &litSolidShader);

  dl.addCircle(pt, vec3(0.02f * cmd.thicken, 0, 0), vec3(0, 0.02f * cmd.thicken, 0), fgcolor(0xff, 0xff, 0xff, 0xff),
    cmd.layer + 0.051f, &litSolidShader);
  dl.addText(
    pt + vec3(0.05, 0, 0),
    vec3(fontSize, 0, 0),
    vec3(0, -fontSize, 0),
    vec3(0, 0, 1),
    repr_0_3g(op->x) + ", " + repr_0_3g(op->y),
    fgcolor(vec4(1.0, 1.0, 1.0, 1.0)),
    ysFonts.lrgFont,
    0.0f, 0.5f,
    cmd.layer);

  if (auto regressionLine = assoc_F(cmd.options, "regression_line")) {
    if (*regressionLine >= 0.5f) {
      size_t n = rawPts.size();
      if (n >= 2) {
        VectorXf xs(n), ys(n);
        size_t pti = 0;
        for (auto &rawPt : rawPts) {
          xs(pti) = rawPt.x;
          ys(pti) = rawPt.y;
          pti++;
        }

        F sxy = xs.dot(ys);
        F sx = xs.sum();
        F sy = ys.sum();
        F sxx = xs.dot(xs);
        //F syy = ys.dot(ys);

        F slope = (sxy - sx*sy/n) / (sxx - sx*sx/n);
        F intercept = sy/n - slope * sx/n;

        vec3 ptn = cmd.tPos(rangeSpec.conv2(rangeSpec.x0, rangeSpec.x0 * slope + intercept, 0));
        vec3 pt0 = cmd.tPos(vec3(0, intercept, 0));
        vec3 ptp = cmd.tPos(rangeSpec.conv2(rangeSpec.x1, rangeSpec.x1 * slope + intercept, 0));
        dl.addLine(ptn, fgcolor(0xff, 0xff, 0xff, 0xcc), ptp, fgcolor(0xff, 0xff, 0xff, 0xcc), 
          cmd.layer + 0.1f, &rawColorShader);

        vec3 textdir = (ptp - ptn).normalized();
        vec3 textvert = textdir.cross(vec3(0, 0, 1));
        dl.addText(
          pt0 - 0.5f * fontSize * textvert,
          textdir * fontSize,
          textvert * fontSize,
          vec3(0, 0, 1),
          "y = " + repr_0_3g(slope) + "x + " + repr_0_3g(intercept),
          fgcolor(vec4(1.0, 1.0, 1.0, 1.0)),
          ysFonts.lrgFont,
          0.5f, 1.0f,
          cmd.layer);
      }
    }
  }
}

void DrawTraceAccum::drawOpFinal(Cmd &cmd, DrawOpRect *op)
{
  auto color = cmd.getColor();
  dl.addQuad(
    cmd.tPos(op->center - op->xdir - op->ydir), color,
    cmd.tPos(op->center + op->xdir - op->ydir), color,
    cmd.tPos(op->center - op->xdir + op->ydir), color,
    cmd.tPos(op->center + op->xdir + op->ydir), color,
    cmd.layer, &litSolidShader);
}

void DrawTraceAccum::drawOpFinal(Cmd &cmd, DrawOpDisk *op)
{
  dl.addCircle(
    cmd.tPos(op->center),
    cmd.tDir(vec3(op->radius, 0, 0)),
    cmd.tDir(vec3(0, op->radius, 0)),
    cmd.getColor(),
    cmd.layer, &glowingTextShader);
}

void DrawTraceAccum::drawOpFinal(Cmd &cmd, DrawOpTube *op)
{
  assert(op->pts.rows() == 3) ;
  for (Index ci = 0; ci + 1 < op->pts.cols(); ci++) {
    dl.addNoodle(
      cmd.tPos(op->pts.col(ci + 0)), cmd.getColor(),
      cmd.tPos(op->pts.col(ci + 1)), cmd.getColor(),
      tubeAxis,
      op->radius,
      cmd.layer, &glowingTextShader);
  }
}

void DrawTraceAccum::drawOpFinal(Cmd &cmd, DrawOpArcArrow *op)
{
  vec3 center = cmd.tPos(op->center);
  vec3 radx = cmd.tDir(op->xDir);
  vec3 rady = cmd.tDir(op->yDir);
  vec3 up = cmd.tDir(op->upDir);
  vec4 col = cmd.getColor();
  F thickness = cmd.getThickness(0.10f);

  auto arrowLenAngle = clamp(0.9f * (op->arcFrom - op->arcTo), -0.15f, +0.15f);
  auto arrowHeadAngle = op->arcTo;
  auto arrowTailAngle = op->arcTo + arrowLenAngle;

  auto arrowHeadRadius = op->radius;
  auto arrowTailRadius1 = op->radius * (1.0f + 1.25f * thickness);
  auto arrowTailRadius2 = op->radius * (1.0f - 1.25f * thickness);

  vec3 arrowHeadDir = cmd.tDir(cos(arrowHeadAngle) * op->xDir + sin(arrowHeadAngle) * op->yDir);
  vec3 arrowTailDir = cmd.tDir(cos(arrowTailAngle) * op->xDir + sin(arrowTailAngle) * op->yDir);

  dl.addAnnulus(center, radx, rady,
    max(0.0f, op->radius * (1.0f + 0.5f * thickness * cmd.thicken)),
    max(0.0f, op->radius * (1.0f - 0.5f * thickness * cmd.thicken)),
    op->arcFrom, op->arcTo + arrowLenAngle*0.75f,
    col, cmd.layer, &litSolidShader);

  dl.addTriangle(
      center + arrowHeadRadius * arrowHeadDir, up, col,
      center + arrowTailRadius1 * arrowTailDir, up, col,
      center + arrowTailRadius2 * arrowTailDir, up, col,
      cmd.layer, &litSolidShader);
}

void DrawTraceAccum::drawOpFinal(Cmd &cmd, DrawOpDial *op)
{
  vec3 dialc = cmd.tPos(op->center);
  vec3 dialx = cmd.tDir(vec3(op->radius, 0, 0));
  vec3 dialy = cmd.tDir(vec3(0, op->radius, 0));
  drawDialStd(dl, dialc, dialx, dialy, *cmd.colorx, op->value, op->range, Dist_linear, nullptr);

#ifdef notyet
  if (auto it = cmd.assoc_mat(cmd.options, "labels", Eigen::Dynamic, 1)) {
    for (Index i = 0; i < it->rows(); i++) {
      F v = (*it)(i, 0);
      drawDialLabel(dl, dialc, dialx, dialy, *cmd.colorx, v, op->range, Dist_linear, repr(v));
    }
  }
#endif
}

void DrawTraceAccum::drawOpFinal(Cmd &cmd, DrawOpText *op)
{
  dl.addText(
    cmd.tPos(op->p0),
    cmd.tDir(vec3(op->size, 0, 0)),
    cmd.tDir(vec3(0, -op->size, 0)),
    cmd.tDir(vec3(0, 0, 1)),
    string(op->text),
    cmd.getColor(),
    ysFonts.lrgFont,
    0, 0);
}

void DrawTraceAccum::drawOpFinal(Cmd &cmd, DrawOpEpicycles *op)
{
  auto &endPts = plotPts(cmd.index, op->v.rows() - 1);
  auto rangeSpec = autoRange(endPts, cmd);
  auto xyScale = 2.0f / max(
    max(abs(rangeSpec.x0),
      abs(rangeSpec.x1)), 
    max(abs(rangeSpec.y0),
      abs(rangeSpec.y1)));

  auto plotCol = cmd.getPlotColor();

  for (Index rowi = op->v.rows()-1; rowi >= 0; rowi--) {
    auto &rawPts = plotPts(cmd.index, rowi);
    bool final = rowi == op->v.rows()-1;
    auto col = final ? plotCol : fgcolor(0xff, 0xff, 0xff, 0x30);

    vector<vec3> pts;
    for (auto const &rawPt : rawPts) {
      pts.push_back(cmd.tPos(vec3(xyScale * rawPt.x, xyScale * rawPt.y, rawPt.z)));
    }
    dl.addThickCurve(pts, 
      vec3(0, 0, 1),
      col,
      cmd.getThickness(final ? 0.04f : 0.0f));
  }

  vector<tuple<vec3, F>> circles;

  vec3 accumPt(0, 0, 0);
  F thinSize = min(0.025f, dl.sizeForPixels(cmd.tPos(accumPt), 3.0f));
  vec3 lastPt = cmd.tPos(accumPt);
  for (Index row = 0; row < op->v.rows(); row++) {
    auto dx = xyScale * op->v(row).real();
    auto dy = xyScale * op->v(row).imag();

    auto radius = hypot(dx, dy);

    circles.emplace_back(accumPt, radius);

    accumPt.x() += dx;
    accumPt.y() += dy;
    vec3 newPt = cmd.tPos(accumPt);

    vec3 longAxis = (newPt - lastPt).normalized();
    vec3 headAxis = -longAxis * min(thinSize * 5.0f, 0.75f * (lastPt-newPt).norm());
    vec3 crossAxis = vec3(0, 0, thinSize).cross(longAxis);
    
    auto col = cmd.getPlotColor(16 + row);
    dl.addQuad(
      lastPt + crossAxis, col,
      lastPt - crossAxis, col,
      newPt + headAxis + crossAxis, col,
      newPt + headAxis - crossAxis, col,
      cmd.layer + 0.2f + 0.001f * row, &rawColorShader);
    dl.addTriangle(
      newPt, col,
      newPt + headAxis + 2.0f*crossAxis, col,
      newPt + headAxis - 2.0f*crossAxis, col,
      cmd.layer + 0.2f + 0.001f * row, &rawColorShader);
    lastPt = newPt;
  }

  {
    vec3 pt = cmd.tPos(accumPt);
    dl.addCircle(pt, 
      vec3(0.05f * cmd.thicken, 0, 0),
      vec3(0, 0.05f * cmd.thicken, 0),
      cmd.getPlotColor(),
      cmd.layer, &glowingTextShader);
    dl.addCircle(pt,
      vec3(0.02f * cmd.thicken, 0, 0),
      vec3(0, 0.02f * cmd.thicken, 0),
      fgcolor(0xff, 0xff, 0xff, 0xff),
      cmd.layer, &glowingTextShader);
    if (0) {
      dl.addText(
        pt + vec3(0.05f, 0, 0),
        vec3(fontSize, 0, 0),
        vec3(0, -fontSize, 0),
        vec3(0, 0, 1),
        repr(op->v),
        fgcolor(vec4(1.0f, 1.0f, 1.0f, 1.0f)),
        ysFonts.lrgFont,
        0, 0.5f,
        cmd.layer, &glowingTextShader);
    }

    if (1) {
      vec4 circcol = fgcolor(0xff, 0xff, 0xff, 0x33);
      for (auto &[center, radius] : circles) {
        auto ncirc = min(128, max(32, int(radius * 40.0f)));
        for (int circi = 0; circi < ncirc; circi++) {
          vec3 pt0 = center + vec3(
              radius * cos((circi + 0) * float(M_2PI) / ncirc), 
              radius * sin((circi + 0) * float(M_2PI) / ncirc),
              0);
          vec3 pt1 = center + vec3(
              radius * cos((circi + 1) * float(M_2PI) / ncirc), 
              radius * sin((circi + 1) * float(M_2PI) / ncirc),
              0);
          dl.addLine(
            cmd.tPos(pt0),
            circcol,
            cmd.tPos(pt1),
            circcol,
            cmd.layer - 0.001f, &rawColorShader);
        }
      }
    }

  }

}

// ------------

static tuple<F, bool> dialAngle(F value, F range, DistType dist)
{
  switch (dist) {

    case Dist_linear:
    case Dist_nonnegative: // we still show negative values even if they shouldn't be there
    default:
      return make_tuple(clamp(value / range, -1.0f, 1.0f), value >= 0.0f);
      break;

    case Dist_exponential:
      return make_tuple(clamp(0.5f * log(value / range), -1.0f, 1.0f), value >= range);
      break;
  }
  return make_tuple(0.0, false);
}


void drawDialStd(G8DrawList &dl, vec3 dialc, vec3 dialx, vec3 dialy, mat4 const &colorx,
  F value, F range, DistType dist,
  PolicyTerrainMap *ptm)
{
  auto [relValue, isPos] = dialAngle(value, range, dist);

  auto shader = &dialShader;
  DialUniforms du = {
    .value = relValue,
    .rangeLo = +1.0f, .rangeHi = -1.0f,
  };

  if (ptm && ptm->spec.xVar.valid()) {
    switch (dist) {

      case Dist_linear:
      case Dist_nonnegative:
      default:
        du.rangeLo = ptm->xParamBase / range - ptm->spec.xVar.relRange;
        du.rangeHi = ptm->xParamBase / range + ptm->spec.xVar.relRange;
        break;

      case Dist_exponential:
        du.rangeLo = log(ptm->xParamBase / range) - ptm->spec.xVar.relRange;
        du.rangeHi = log(ptm->xParamBase / range) + ptm->spec.xVar.relRange;
        break;
    }
  }

  auto restoreDialUniforms = dl.withDialUniforms(du, shader);

  auto &setup = dl.getSetup(shader);
  F layer = 0.1f;
  uint32_t vi = 0;
  if (setup.vt == G8Vtx_pul::vt) {
    G8VtxBlaster<G8Vtx_pul> vb(setup, 4, vi);
    vb(dialc - dialx - dialy, vec2(-1.0, -1.0), layer);
    vb(dialc + dialx - dialy, vec2(+1.0, -1.0), layer);
    vb(dialc - dialx + dialy, vec2(-1.0, +1.0), layer);
    vb(dialc + dialx + dialy, vec2(+1.0, +1.0), layer);
  }
  else {
    return dl.badShader(shader, "drawDialStd (pul)", {G8Vtx_pul::vt});
  }

  G8IdxBlaster ib(setup, 6);
  ib.tripair(vi+0, vi+1, vi+2, vi+3);
  dl.addCmd(ib);

  vec4 negcol = colorx * fgcolor(0xff, 0xa3, 0x2f, 0xff);
  vec4 poscol = colorx * fgcolor(0x3f, 0xf3, 0xff, 0xff);
  vec4 textcol = isPos ? poscol : negcol;

  auto valueText = repr_0_3g(value);
  vec3 up(0, 0, 1);
  vec3 texty = -0.65f * dialy;
  vec3 textx = up.cross(texty);
  dl.addText(dialc, textx, texty, up, valueText, textcol,
    ysFonts.lrgFont,
    0.5, 0.5,
    0.0f, &glowingTextShader);
}

void drawDialLabel(G8DrawList &dl,
  vec3 dialc, vec3 dialx, vec3 dialy, mat4 const &colorx,
  F value, F range, DistType dist, string_view label)
{
  auto ray = [&dialx, &dialy, range, dist](F v, F radius) {
    auto [theta, isPos] = dialAngle(v, range, dist);
    return cosf(theta) * radius * dialx + sinf(theta) * radius * dialy;
  };

  vec4 tickcol = colorx * fgcolor(0x88, 0x88, 0x88, 0xff);
  vec4 textcol = colorx * fgcolor(0xff, 0xff, 0xff, 0xff);

  vec3 tick0 = ray(value, 0.85);
  vec3 tick1 = ray(value, 0.95);
  vec3 textpos = ray(value, 1.2);
  vec3 up(0, 0, 1);
  vec3 texty = vec3(0, -0.12f, 0);
  vec3 textx = up.cross(texty);
  dl.addLine(dialc + tick0, tickcol, dialc + tick1, tickcol);
  dl.addText(dialc + textpos, textx, texty, up, label, textcol,
    ysFonts.lrgFont,
    0.5, 0.5,
    0.0f, &glowingTextShader);
}

