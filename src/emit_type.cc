#include "./emit.h"

CellTypeAnalyzer::CellTypeAnalyzer(CompiledSheet &_she, CellIndex _cell, SheetEmitContext *_hypoCtx)
  : she(_she),
    cell(_cell),
    root(she.sh.astByCell[cell]),
    hypoCtx(_hypoCtx)
{
  ok = true;
  type = eval(root);
  if (type.ist<bool>()) {
    type = typetag<F>();
  }
  if (type.isError()) {
    ok = false;
  }
}

TypeTag CellTypeAnalyzer::setError(string const &e, AstNode *n)
{
  if (ok) {
    error = e;
    ok = false;
    if (n) {
      errorLoc = n->loc;
    }
    else {
      errorLoc = FormulaSubstr(0, 0);
    }
  }
  return T_error;
}

TypeTag CellTypeAnalyzer::setError()
{
  return setError("", nullptr);
}


TypeTag CellTypeAnalyzer::eval(AstNode *n)
{
  if (!n) return T_undef;

  switch (n->t) {
    #define defdispatch(T) case A_##T: return eval_##T(n); break;
    foreach_ast(defdispatch)
    #undef defdispatch
    default:
      return setError("Unknown op", n);
  }

}
