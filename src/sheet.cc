#include "./core.h"
#include "numerical/numerical.h"
#include "./emit.h"

Sheet::Sheet()
{
  doDebug = false;
  verbose = 0;
  auto nilSym = mkSymbol("nil"); // gets id 0
  assert(nilSym.isNil());
}

Sheet::~Sheet()
{
}

ostream & operator <<(ostream &s, Sheet &sh)
{
  return s << "Sheet(" << sh.fileNameByPage << ")";
}


CellIndex Sheet::getCell(string const &name) const
{
  auto slotByName = cellByName.find(name);
  if (slotByName != cellByName.end()) {
    return slotByName->second;
  }
  return nullCellIndex;
}

string Sheet::getCellName(CellIndex cell) const
{
  if (cell == nullCellIndex) return "null";
  return nameByCell.at(cell);
}

bool Sheet::isReservedCellName(string const &name)
{
  /* 
    Reserved names. Note that dt and tickCount aren't -- there should be such cells
    Compare to FormulaParser::resolveName() in parse.cc
    In SheetUi::commitCellName, it assumes that a reserved cell name can be
    cured by adding a suffix starting with .1 (counting up) to it. So be careful
    adding any prefix matches here where that wouldn't be true
  */
  if (name == "time" || name == "initial") return true;
  if (name == "pi" || name == "e" || name == "im" || name == "inf" || name == "nan") return true;
  if (name == "fn" || name == "if" || name == "else") return true;
  if (name == "api") return true;
  if (name == "fail.cellCount" || name == "fail.time") return true;
  return false;
}

CellIndex Sheet::mkCell(string const &name)
{
  auto slotByName = cellByName.find(name);
  if (slotByName != cellByName.end()) {
    return slotByName->second;
  }

  if (isReservedCellName(name)) return nullCellIndex;

  auto cell = nCells++;
  liveCells[cell] = true;
  resizeCellArrays();

  nameByCell[cell] = name;
  cellByName[name] = cell;

  needsEmit = true;
  return cell;
}

void Sheet::resizeCellArrays()
{
  nameByCell.resize(nCells);
  formulaByCell.resize(nCells);
  pageByCell.resize(nCells);
  visualByCell.resize(nCells);
  posByCell.resize(nCells);

  nameAstByCell.resize(nCells);
  annosByCell.resize(nCells);
  astByCell.resize(nCells);
  parseErrorByCell.resize(nCells);
  parseErrorLocByCell.resize(nCells);
  drawInfoByCell.resize(nCells);
  paramsByCell.resize(nCells);

  revDepsByCell.resize(nCells);
  fwdDepsByCell.resize(nCells);
}

void Sheet::killCell(CellIndex cell)
{
  const int verbose = 0;
  if (liveCells[cell]) {
    string name = nameByCell[cell];

    auto cellByNameIt = cellByName.find(name);
    if (cellByNameIt != cellByName.end()) {
      cellByName.erase(cellByNameIt);
    }

    liveCells[cell] = false;
    formulaByCell[cell].clear();
    if (verbose) L() << "Kill cell " << name << " " << cell;

    for (auto dep : fwdDepsByCell[cell]) {
      needParseCells[dep] = true;
    }
    needParseCells[cell] = false;
  }
}

void Sheet::updateFormulaFromParams(CellIndex cell)
{
  if (paramsByCell.empty()) return;

  // A list of (start pos, end pos, new paramvalue) to be stuffed into the formula string
  vector<tuple<U32, U32, F>> todos;
  for (auto pi : paramsByCell[cell]) {
    todos.emplace_back(valueLocByParam[pi].begin, valueLocByParam[pi].end, params.valueByParam(pi));
    todos.emplace_back(rangeLocByParam[pi].begin, rangeLocByParam[pi].end, rangeByParam[pi]);
  }

  sort(todos.begin(), todos.end(), [](auto const &a, auto const &b) {
    return get<0>(a) < get<0>(b);
  });


  size_t todoi = 0;
  auto oldFormula = formulaByCell[cell];
  // Track character movement, so we can update param positions for next time.
  vector<U32> newPosByOldPos(oldFormula.size() + 1, 0);

  string newFormula;
  newFormula.reserve(oldFormula.size() + 50);
  size_t oldPos = 0;
  while (oldPos < oldFormula.size()) {
    if (todoi < todos.size()) {
      auto [p0, p1, newVal] = todos[todoi];
      if (p1 == p0) { // empty, shouldn't really be there
        todoi++;
        continue;
      }
      if (p0 < oldPos) { // Did we skip it? Weird.
        todoi++;
        continue;
      }
      if (p0 == oldPos) {
        newPosByOldPos[oldPos] = newFormula.size();
        auto newStr = repr_0_3g(newVal);
        /* Try to avoid changing length. This doesn't have to be perfect. It just reduces
           visual jiggling when adjusting params by knob. So at least the param was
           going "1.48" -> "1.5" we insert "1.50" */
        if (newStr.size () < p1 - p0) {
          bool hasDot = newStr.find('.') != string::npos;
          bool hasExp = newStr.find('e') != string::npos;
          if (!hasExp) {
            if (!hasDot) {
              newStr += '.';
            }
            while (newStr.size () < p1 - p0) newStr += '0';
          }
          // check that it's not catastrophically wrong
          assertlog(abs(strtof(newStr.c_str(), nullptr) - newVal) < (1.0f + abs(newVal))/100.0f,
            LOGV(newStr) << LOGV(newVal));
        }
        newFormula += newStr;
        oldPos = p1;
        newPosByOldPos[oldPos] = newFormula.size();
        todoi ++;
        continue;
      }
    }

    newPosByOldPos[oldPos] = newFormula.size();
    newFormula += oldFormula[oldPos];
    oldPos ++;
  }
  formulaByCell[cell] = newFormula;

  // Update param positions
  for (auto pi : paramsByCell[cell]) {
    updateSubstr(valueLocByParam[pi], newPosByOldPos);
    updateSubstr(rangeLocByParam[pi], newPosByOldPos);
  }
  for (auto &a : annosByCell[cell]) {
    updateSubstr(a.loc, newPosByOldPos);
  }
  updateSubstr(parseErrorLocByCell[cell], newPosByOldPos);
  if (auto com = compiled.get()) {
    updateSubstr(com->emitErrorLocByCell[cell], newPosByOldPos);
  }

  updateAstSubstrs(astByCell[cell], newPosByOldPos);
}

void Sheet::resizeParamsArrays()
{
  cellByParam.resize(nParams, nullCellIndex);
  rangeByParam.resize(nParams);
  distByParam.resize(nParams);
  valueLocByParam.resize(nParams, FormulaSubstr());
  rangeLocByParam.resize(nParams, FormulaSubstr());
}

ParamIndex Sheet::mkParam(CellIndex cell)
{
  ParamIndex pi = nParams++;
  resizeParamsArrays();

  cellByParam[pi] = cell;
  paramsByCell[cell].push_back(pi);
  if (0) L() << "Cell " << cell << " param " << pi;

  params.mutValueByParam(pi) = 0.0f;

  return pi;
}

string Sheet::getParamLiteral(ParamIndex pi) const
{
  auto cell = cellByParam[pi];
  return formulaByCell[cell].substr(valueLocByParam[pi].begin, valueLocByParam[pi].end - valueLocByParam[pi].begin);
}

void Sheet::setParamValue(ParamIndex pi, F value)
{
  params.mutValueByParam(pi) = value;
  propagateParamUpdate(pi);
}

void Sheet::setParamRange(ParamIndex pi, F range)
{
  rangeByParam[pi] = range;
  propagateParamUpdate(pi);
}

string Sheet::getParamDesc(ParamIndex pi) const
{
  if (pi == nullParamIndex) return "null";
  auto cell = cellByParam[pi];
  auto found = find(paramsByCell[cell].begin(), paramsByCell[cell].end(), pi);
  if (found != paramsByCell[cell].end()) {
    return nameByCell[cell] + "@" + to_string(found - paramsByCell[cell].begin());
  }
  return "param" + to_string(pi);
}


Symbol Sheet::mkSymbol(string const &name)
{
  auto slotByName = symbolByName.find(name);
  if (slotByName != symbolByName.end()) {
    return slotByName->second;
  }

  auto symbol = nSymbols;
  nSymbols.i++;
  nameBySymbol.resize(nSymbols.i);
  nameBySymbol[symbol.i] = name;
  symbolByName[name] = Symbol(symbol);

  return Symbol(symbol);
}


// ---------------

void Sheet::setFormula(CellIndex cell, string const &formula)
{
  if (formula == formulaByCell[cell]) return;

  for (auto dep : fwdDepsByCell[cell]) {
    needParseCells[dep] = true;
  }

  formulaByCell[cell] = formula;
  needParseCells[cell] = true;
}

void Sheet::setCellPos(CellIndex cell, vec2 pos)
{
  posByCell[cell] = pos;
}

/*
  posByCell is the fully resolved position of a cell.
  When we move an includePos, we have to move every cell of that page
  and every page included by it.
*/
void Sheet::setIncludePagePos(PageIndex page, vec2 newPos, bool withoutCells)
{
  if (!withoutCells) {
    vector<bool> moveFlagByPage(nPages, false);
    moveFlagByPage[page] = true;

    while (1) {
      bool didSome = false;
      for (PageIndex child = 0; child < nPages; child++) {
        if (!moveFlagByPage[child] && 
          includeParentByPage[child] != nullPageIndex &&
          moveFlagByPage[includeParentByPage[child]]) {
          moveFlagByPage[child] = true;
          didSome = true;
        }
      }
      if (!didSome) break;
    };

    vec2 delta = newPos - includePosByPage[page];
    for (auto cell : liveCells) {
      if (moveFlagByPage[pageByCell[cell]]) {
        posByCell[cell] += delta;
      }
    }
  }

  includePosByPage[page] = newPos;
}

void Sheet::snapToGrid()
{
  for (auto i : liveCells) {
    posByCell[i] = posByCell[i].array().round();
  }
  for (PageIndex page = 0; page < nPages; page++) {
    includePosByPage[page] = includePosByPage[page].array().round();
  }
}

void Sheet::setCellVisual(CellIndex cell, string visual)
{
  visualByCell[cell] = std::move(visual);
}

void Sheet::setCellDrawInfo(CellIndex cell, DrawInfo drawInfo)
{
  drawInfoByCell[cell] = drawInfo;
}

void Sheet::setCellPage(CellIndex cell, PageIndex page)
{
  pageByCell[cell] = page;
  if (page >= nPages) nPages = page + 1;
}


void Sheet::setDeps(CellIndex cell, vector<CellIndex> newRevDeps)
{
  sort(newRevDeps.begin(), newRevDeps.end());
  newRevDeps.erase(unique(newRevDeps.begin(), newRevDeps.end()), newRevDeps.end());
  auto &oldRevDeps = revDepsByCell[cell];

  vector<CellIndex> added, removed;

  set_difference(
    newRevDeps.begin(), newRevDeps.end(),
    oldRevDeps.begin(), oldRevDeps.end(),
    back_inserter(added));
  set_difference(
    oldRevDeps.begin(), oldRevDeps.end(),
    newRevDeps.begin(), newRevDeps.end(),
    back_inserter(removed));

  if (!removed.empty() || !added.empty()) {
    needsEmit = true;

    vector<CellIndex> needsSort;

    for (auto it : removed) {
      auto &fwds = fwdDepsByCell.at(it);
      fwds.erase(remove(fwds.begin(), fwds.end(), it), fwds.end());
      needsSort.push_back(it);
    }

    for (auto it : added) {
      auto &fwds = fwdDepsByCell.at(it);
      fwds.push_back(cell);
      needsSort.push_back(it);
    }

    for (auto it : needsSort) {
      auto &fwds = fwdDepsByCell.at(it);
      sort(fwds.begin(), fwds.end());
      fwds.erase(unique(fwds.begin(), fwds.end()), fwds.end());
    }
    swap(revDepsByCell[cell], newRevDeps);
    cellTopology = nullptr;
  }

}

CellTopology::CellTopology(CellIndex _nCells)
  : rankByCell(_nCells),
    vindexByCell(_nCells)
{
  totalOrder.reserve(_nCells);
}

void Sheet::updateCellTopology()
{
  auto newTopology = make_shared<CellTopology>(nCells);
  auto &t = *newTopology;
  deque<CellIndex> pending;

  vector<bool> done(nCells);

  auto remainingRevDeps = revDepsByCell; // copy

  for (auto cell : liveCells) {
    if (remainingRevDeps[cell].empty()) {
      pending.push_back(cell);
    }
  }

  while (!pending.empty()) {
    auto n = pending.front();
    assert(n != nullCellIndex);
    if (done[n]) continue;
    done[n] = true;
    pending.pop_front();
    t.totalOrder.push_back(n);

    auto &nOutgoing = fwdDepsByCell[n];

    for (auto m : nOutgoing) {
      assert(liveCells[m]);
      // remove edge from n to m
      auto &mIncoming = remainingRevDeps[m];
      mIncoming.erase(remove(mIncoming.begin(), mIncoming.end(), n));
      if (mIncoming.empty()) { // no more edges into m
        pending.push_back(m);
      }
    }
  }

  vector<int> vindexByRank(nCells);

  int cellRank = 0;
  for (size_t orderi = 0; orderi < t.totalOrder.size(); orderi++) {
    auto cell = t.totalOrder[orderi];
    for (auto prevCell : revDepsByCell[cell]) {
      cellRank = max(cellRank, t.rankByCell[prevCell] + 1);
    }
   t.rankByCell[cell] = cellRank;
   auto &vindexSlot = vindexByRank[cellRank];
   if (compiled && compiled->hasParamByCell[cell] && vindexSlot > 0) vindexSlot++;
   t.vindexByCell[cell] = vindexSlot;
   t.nVindex = max(t.nVindex, vindexSlot+1);
   t.nRank = max(t.nRank, cellRank+1);
   vindexSlot++;
  }

  if (1) {
    auto l = L();
    l << "Topology:\n";
    Eigen::Matrix<int, Dynamic, Dynamic> m = Eigen::Matrix<int, Dynamic, Dynamic>::Constant(t.nVindex, t.nRank, nullCellIndex);
    for (auto &cell : t.totalOrder) {
      auto cellRank = t.rankByCell[cell];
      auto cellVindex = t.vindexByCell[cell];
      assertlog(cellRank < t.nRank, LOGV(cellRank) << LOGV(t.nRank));
      assertlog(cellVindex < t.nVindex, LOGV(cellVindex) << LOGV(t.nVindex));
      m(cellVindex, cellRank) = cell;
    }
    l << m;
  }

  cellTopology = newTopology;
}

shared_ptr<CellTopology> Sheet::getCellTopology()
{
  if (!cellTopology) updateCellTopology();
  return cellTopology;
}


void Sheet::setNeedsParseAll()
{
  needParseCells = liveCells;
}

void Sheet::propagateParamUpdate(ParamIndex pi)
{
  auto cell = cellByParam[pi];
  updateFormulaFromParams(cell);
  needParseCells[cell] = false;
}

CellIndex Sheet::findClosest(F px, F py) const
{
  // OPT: use a quadtree
  CellIndex best = nullCellIndex;
  F bestDistSq = 1.0;
  for (auto cell : liveCells) {
    auto &pos = posByCell[cell];
    F distSq = sqr(0.5f * (pos[0] - px)) + sqr(pos[1] - py);

    if (distSq < bestDistSq) {
      bestDistSq = distSq;
      best = cell;
    }
  }
  return best;
}

// Errors

pair<string_view, FormulaSubstr> Sheet::getCellError(CellIndex cell) const
{
  if (cell != nullCellIndex) {
    if (!parseErrorByCell[cell].empty()) {
      return pair(string_view(parseErrorByCell[cell]), parseErrorLocByCell[cell]);
    }
    if (auto com = compiled.get()) {
      if (!com->emitErrorByCell[cell].empty()) {
        return pair(string_view(com->emitErrorByCell[cell]), com->emitErrorLocByCell[cell]);
      }
    }
  }
  return pair(string_view(""), FormulaSubstr(0, 0));
}


vector<string> Sheet::errors()
{
  vector<string> ret;
  for (auto cell : liveCells) {
    string p1, p2;
    FormulaSubstr loc;
    if (!parseErrorByCell[cell].empty()) {
      p1 = "Parse error: ";
      p2 = parseErrorByCell[cell];
      loc = parseErrorLocByCell[cell];
    }
    else if (auto com = compiled.get()) {
      if (!com->emitErrorByCell[cell].empty()) {
        p1 = "Emit error: ";
        p2 = com->emitErrorByCell[cell];
        loc = com->emitErrorLocByCell[cell];
      }
    }
    if (!p1.empty()) {
      string line;
      line += p1;
      line += nameByCell[cell];
      line += " = ";
      string indent = string(line.size() + loc.begin, ' ');
      line += formulaByCell[cell];
      line += '\n';
      line += indent;
      line += "^ ";
      line += p2;
      ret.push_back(line);
    }
  }
  if (auto com = compiled.get()) {
    for (auto &[desc, order] : com->orderProblems) {
      string line;
      line += "Error in linearize: ";
      bool first = true;
      for (auto &it : order) {
        if (!first) line += ' ';
        first = false;
        line += nameByCell[it];
      }
      line += ": " + desc;
      ret.push_back(line);
    }
  }
  return ret;
}


vector<BoxedRef> Sheet::refsByCell(CellIndex cell) const
{
  vector<BoxedRef> ret;
  if (auto com = compiled.get()) {
    for (auto &ctx : com->contexts) {
      auto found = ctx->refByCell.find(cell);
      if (found != ctx->refByCell.end()) {
        ret.push_back(found->second);
      }
    }
  }
  return ret;
}



TypeTag Sheet::getCellType(CellIndex cell) const
{
  if (auto com = compiled.get()) {
    if (cell != nullCellIndex) {
      return com->typeByCell[cell];
    }
  }
  return T_undef;
}

bool Sheet::getCellIsInput(CellIndex cell) const
{
  if (auto com = compiled.get()) {
    if (cell != nullCellIndex) {
      auto dir = com->apiDirectionByCell[cell];
      return dir == API_SENSOR || dir == API_MONITOR;
    }
  }
  return false;
}

bool Sheet::getCellIsPureParam(CellIndex cell) const
{
  if (auto com = compiled.get()) {
    if (cell != nullCellIndex) {
      return com->isPureParamByCell[cell];
    }
  }
  return false;
}

bool Sheet::getCellIsTimeInvariant(CellIndex cell) const
{
  if (auto com = compiled.get()) {
    if (cell != nullCellIndex) {
      return com->isTimeInvariantByCell[cell];
    }
  }
  return false;
}

bool Sheet::getCellIsApi(CellIndex cell) const
{
  if (auto com = compiled.get()) {
    if (cell != nullCellIndex) {
      return com->isApiByCell[cell];
    }
  }
  return false;
}


void Sheet::compile(EmitMode emitMode)
{
  parseAll();
  flowAll();
  emitAll(emitMode);
  for (auto cell : liveCells) {
    CodeAnnotator anno(*this, cell);
    if (anno.ok) {
      annosByCell[cell] = anno.annos;
    }
  }
}

