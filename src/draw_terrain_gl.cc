#include "./core.h"
#include "./sheet_ui.h"
#include "./scope_math.h"
#include "./draw_gl.h"

vec3 crossnorm(vec3 const &a, vec3 const &b)
{
  return a.cross(b).normalized();
}

using ParamSurface = Eigen::Matrix<vec3, Dynamic, Dynamic>;

ParamSurface mkParamSurface(PolicyTerrainSpec const &spec, MatrixXf const &rs, mat4 const &posx, 
  vector<pair<F, F>> const &xs, vector<pair<F, F>> const &ys,
  F ofsX, F ofsY, F baser, F rScale)
{
  assertlog(size_t(rs.rows()) == ys.size() && size_t(rs.cols()) == xs.size(),
    "rs.rows()=" << rs.rows() << " ys.size()=" << ys.size() <<
    "rs.cols()=" << rs.cols() << " xs.size()=" << xs.size());

  ParamSurface pts(rs.rows()+2, rs.cols()+2);

  for (int yi = 0; yi < rs.rows(); yi++) {
    for (int xi = 0; xi < rs.cols(); xi++) {
      pts(yi+1, xi+1) = (posx * vec4(
        softclamp(xs[xi].first + ofsX) * 2.0f,
        softclamp(ys[yi].first + ofsY) * 2.0f,
        softclamp((rs(yi, xi) - baser) / max(0.01f, rScale)) * 0.75f,
        1.0f)).head<3>();
    }
  }
  for (long yi = 1; yi + 1 < pts.rows(); yi++) {
    pts(yi, 0) = pts(yi, 1);
    pts(yi, pts.cols()-1) = pts(yi, pts.cols()-2);
  }
  for (long xi = 1; xi + 1 < pts.cols(); xi++) {
    pts(0, xi) = pts(1, xi);
    pts(pts.rows()-1, xi) = pts(pts.rows()-2, xi);
  }
  pts(0, 0) = pts(1, 1);
  pts(pts.rows()-1, 0) = pts(pts.rows()-2, 1);
  pts(0, pts.cols()-1) = pts(1, pts.cols()-2);
  pts(pts.rows()-1, pts.cols()-1) = pts(pts.rows()-2, pts.cols()-2);

  return pts;
}

ParamSurface mkParamSurface(Sheet &sh, PolicyTerrainMap *ptm, mat4 const &posx)
{
  assertlog(ptm->spec.xVar.valid() && ptm->spec.yVar.valid(), LOGV(WithSheet(sh, ptm->spec.xVar)) << LOGV(WithSheet(sh, ptm->spec.yVar)));
  auto xs = ptm->spec.xVar.getValues(sh, ptm->xResolution);
  auto ys = ptm->spec.yVar.getValues(sh, ptm->yResolution);

  auto &[mat, baser, rScale] = ptm->rs[0];

  auto ofsX = (sh.params.valueByParam(ptm->spec.xVar.param) - ptm->xParamBase) / (sh.rangeByParam[ptm->spec.xVar.param] * ptm->spec.xVar.relRange);
  auto ofsY = (sh.params.valueByParam(ptm->spec.yVar.param) - ptm->yParamBase) / (sh.rangeByParam[ptm->spec.yVar.param] * ptm->spec.yVar.relRange);

  auto pts = mkParamSurface(ptm->spec, mat, posx, xs, ys, ofsX, ofsY, baser, rScale);
  return pts;
}

void drawParamSurface(G8DrawList &dl, ParamSurface const &pts, vec4 const &col)
{
  for (int yi = 0; yi + 3 < pts.rows(); yi++) {
    for (int xi = 0; xi + 3 < pts.cols(); xi++) {
      dl.addQuad(
        pts(yi+1, xi+1), crossnorm(pts(yi+1, xi+2) - pts(yi+1, xi+0), pts(yi+2, xi+1) - pts(yi+0, xi+1)), col,
        pts(yi+1, xi+2), crossnorm(pts(yi+1, xi+3) - pts(yi+1, xi+1), pts(yi+2, xi+2) - pts(yi+0, xi+2)), col, 
        pts(yi+2, xi+1), crossnorm(pts(yi+2, xi+2) - pts(yi+2, xi+0), pts(yi+3, xi+1) - pts(yi+1, xi+1)), col, 
        pts(yi+2, xi+2), crossnorm(pts(yi+2, xi+3) - pts(yi+2, xi+1), pts(yi+3, xi+2) - pts(yi+1, xi+2)), col, 
        0.0f, &terrainShader);
    }
  }
}

void drawParamGrid(G8DrawList &dl, ParamSurface const &pts, vec4 const &col)
{
  for (long yi = 0; yi + 2 < pts.rows(); yi++) {
    for (long xi = 0; xi + 2 < pts.cols(); xi++) {
      vec3 above(0, 0, 0.005);

      dl.addLine(pts(yi+1, xi+1) + above, col, pts(yi+1, xi+2) + above, col,
        0.1f, &rawColorShader);
      dl.addLine(pts(yi+1, xi+1) + above, col, pts(yi+2, xi+1) + above, col, 
        0.1f, &rawColorShader);
      if (xi + 3 == pts.cols()) {
        dl.addLine(pts(yi+1, xi+2) + above, col, pts(yi+2, xi+2) + above, col,
          0.1f, &rawColorShader);
      }
      if (yi + 3 == pts.rows()) {
        dl.addLine(pts(yi+2, xi+1) + above, col, pts(yi+2, xi+2) + above, col,
          0.1f, &rawColorShader);
      }
    }
  }
}

optional<vec3> hitTestParamSurface(G8DrawList &dl, ParamSurface const &pts)
{
  vec3 bestHit;
  float bestDistance = 9999.0f;
  bool hitOk = false;

  for (int yi = 0; yi + 3 < pts.rows(); yi++) {
    for (int xi = 0; xi + 3 < pts.cols(); xi++) {
      vec3 hit;
      float distance;
      if (dl.hitTestQuad(pts(yi+1, xi+1), pts(yi+1, xi+2), pts(yi+2, xi+1), pts(yi+2, xi+2), hit, distance)) {
        if (distance < bestDistance) {
          bestDistance = distance;
          bestHit = hit;
          hitOk = true;
        }
      }
    }
  }
  if (hitOk) return bestHit;
  return nullopt;
}

static vec3 clickedHitPos;

void DrawTraceAccum::drawOpFinal(Cmd &cmd, DrawOpPolicyTerrain *op)
{
  PolicyTerrainSpec spec;
  spec.rMetrics.push_back(op->rMetricRef);
  spec.baseValues.push_back(op->baseValue);
  spec.xVar = op->xVar;
  spec.yVar = op->yVar;
  spec.paramHash = op->paramHash;
  spec.compiledSheetEpoch = op->compiledSheetEpoch;
  spec.sampleIndex = op->sampleIndex;
  spec.replayBuffer = tr.replayBuffer;
  spec.numSeedsToAverage = numSeedsToAverage;
  auto [ptm, exact] = getPolicyTerrain(sh, spec);
  if (!ptm) return;
  
  auto pts = mkParamSurface(sh, ptm.get(), *cmd.posx);

  dl.addArrowTriple(*cmd.posx);

  drawParamSurface(dl, pts, vec4(1, 1, 1, 1));
  drawParamGrid(dl, pts, vec4(0, 0, 0, 0.533));

  if (!exact) {
    dl.addLoading((*cmd.posx * vec4(0, 0, 0, 1)).head<3>(), 0.2f);
  }

  if (auto hitPos = hitTestParamSurface(dl, pts)) {
    if (IsPopupOpen("paramMenu")) {
      dl.addArrow(clickedHitPos, vec3(0, 0, 0.5f), vec3(0.25f, 0, 0), vec3(0, 0.25f, 0), vec4(0.231, 0.933, 0.675, 0.867));
    }
    else {
      dl.addArrow(*hitPos, vec3(0, 0, 0.5f), vec3(0.25f, 0, 0), vec3(0, 0.25f, 0), vec4(0.231, 0.933, 0.675, 0.867));
    }

    if (IsMouseClicked(0, false)) {
      if (GetIO().KeyCtrl) {
        clickedHitPos = *hitPos;
        OpenPopup("paramMenu");
      }
    }
  }
  if (BeginPopup("paramMenu", ImGuiWindowFlags_AlwaysAutoResize|ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoSavedSettings)) {

    vec4 coordrel = cmd.posx->inverse() * clickedHitPos.homogeneous();
    auto clickX = softunclamp(0.5f * coordrel.x());
    auto clickY = softunclamp(0.5f * coordrel.y());
    auto oldParamX = ptm->spec.xVar.getParamValue(sh, 0.0f);
    auto oldParamY = ptm->spec.yVar.getParamValue(sh, 0.0f);
    auto newParamX = ptm->spec.xVar.getParamValue(sh, clickX);
    auto newParamY = ptm->spec.yVar.getParamValue(sh, clickY);

    if (MenuItem("Set params")) {
      sh.setParamValuesUndoable({
        ptm->spec.xVar.param,
        ptm->spec.yVar.param
      }, {
        newParamX,
        newParamY
      });
    }
    BulletText("Change %s from %s to %s", sh.getParamDesc(ptm->spec.xVar.param).c_str(), repr_0_3g(oldParamX).c_str(), repr_0_3g(newParamX).c_str());
    BulletText("Change %s from %s to %s", sh.getParamDesc(ptm->spec.yVar.param).c_str(), repr_0_3g(oldParamY).c_str(), repr_0_3g(newParamY).c_str());
    EndPopup();
  }
}

VectorXf emphSmall(VectorXf const &x)
{
  VectorXf ret(x);
  for (Index i = 0; i < ret.size(); i++) {
    ret[i] = cbrt(ret[i]);
  }
  return ret;
}

F emphSmall(F x)
{
  return cbrt(x);
}

void drawSquiggle(G8DrawList &dl, Sheet &sh, vec3 const &pos, PolicyTerrainMap *ptm, bool exact, CellVals *rangeByVal)
{
  F halfw = 0.35f;
  F h = 0.2f;
  vec4 bgcol = bgcolor(0xe2, 0xdf, 0x9a, 0xcc);
  vec4 axcol = fgcolor(0x22, 0x22, 0x22, 0xff);


  if (1) dl.addQuad(
    pos + vec3(-halfw, 0, 0), bgcol,
    pos + vec3(halfw, 0, 0), bgcol,
    pos + vec3(-halfw, h, 0), bgcol,
    pos + vec3(halfw, h, 0), bgcol,
    1.0f, &unshinySolidShader);
  
  { // x axis
    vector<vec3> pts {
      pos + vec3(-halfw, 0.5f * h, 0),
      pos + vec3(+halfw, 0.5f * h, 0)
    };
    dl.addThickCurve(pts, vec3(0, 0, 1), axcol, 0.02,
      1.1f, &glowingTextShader);
  }
  { // y axis
    vector<vec3> pts {
      pos + vec3(0, 1.0f * h, 0),
      pos + vec3(0, 0.0f * h, 0)
    };
    dl.addThickCurve(pts, vec3(0, 0, 1), axcol, 0.02f,
      1.1f, &glowingTextShader);
  }

  if (ptm) {
    size_t ri = 0;
    for (auto &[mat, baser, localScale] : ptm->rs) {
      F rScale = rangeByVal->rd_F(ptm->spec.rMetrics.at(ri));
      VectorXf yvals = emphSmall((mat.row(0).array() - baser) / max(0.01f, rScale));
      auto xvals = ptm->spec.xVar.getValues(sh, ptm->xResolution);
      assertlog(xvals.size() == size_t(ptm->xResolution), "xvals=" << xvals << " ptm->xResolution=" << ptm->xResolution);
      vector<vec3> pts(ptm->xResolution);
      for (int i = 0; i < ptm->xResolution; i++) {
        pts[i] = pos + vec3(xvals[i].first * halfw, 0.5f * h + 0.5f * h * yvals[i], 0);
      }
      vec4 linecol = goodGraphColor(ri);
      dl.addThickCurve(pts, vec3(0, 0, 1), linecol, 0.02f, 
        1.2f, &glowingTextShader);
      ri++;
    }
    if (!exact) {
      dl.addLoading(pos + vec3(0, 0.5f * h, 0), 0.2f);
    }
  }
}


void drawBlownupSquiggle(G8DrawList &dl, Sheet &sh, vec3 const &pos, PolicyTerrainMap *ptm, bool exact, CellVals *rangeByVal)
{
  F halfw = 2.5f;
  F h = 1.5f;
  vec4 bgcol = bgcolor(0xe2, 0xdf, 0x9a, 0xcc);
  vec4 axcol = fgcolor(0x22, 0x22, 0x22, 0xff);



  if (1) dl.addQuad(
    pos + vec3(-halfw, 0, 0), bgcol,
    pos + vec3(halfw, 0, 0), bgcol,
    pos + vec3(-halfw, h, 0), bgcol,
    pos + vec3(halfw, h, 0), bgcol,
    1.0f, &unshinySolidShader);
  
  { // x axis
    vector<vec3> pts {
      pos + vec3(-halfw, 0.5f * h, 0),
      pos + vec3(+halfw, 0.5f * h, 0)
    };
    dl.addThickCurve(pts, vec3(0, 0, 1), axcol, 0.02f,
      1.1f, &glowingTextShader);
  }
  { // y axis
    vector<vec3> pts {
      pos + vec3(0, 1.0f * h, 0),
      pos + vec3(0, 0.0f * h, 0)
    };
    dl.addThickCurve(pts, vec3(0, 0, 1), axcol, 0.02f,
      1.1f, &glowingTextShader);
  }

  if (ptm) {
    size_t ri = 0;
    for (auto &[mat, baser, localScale] : ptm->rs) {
      F rScale = rangeByVal ? rangeByVal->rd_F(ptm->spec.rMetrics.at(ri)) : localScale;
      VectorXf yvals = emphSmall((mat.row(0).array() - baser) / max(0.01f, rScale));
      auto xvals = ptm->spec.xVar.getValues(sh, ptm->xResolution);
      assertlog(xvals.size() == size_t(ptm->xResolution), "xvals=" << xvals << " ptm->xResolution=" << ptm->xResolution);
      vector<vec3> pts(ptm->xResolution);
      for (int i = 0; i < ptm->xResolution; i++) {
        pts[i] = pos + vec3(xvals[i].first * halfw, 0.5f * h + 0.5f * h * yvals[i], 0);
      }
      vec4 linecol = goodGraphColor(ri);
      dl.addThickCurve(pts, vec3(0, 0, 1), linecol, 0.02f, 
        1.2f, &glowingTextShader);

      if (ri == 0 && rScale > 1e-6f) {

        vec4 textcol = fgcolor(0xff, 0xff, 0xff, 0x88);
        if (ptm->rs.size() > 1) {
          textcol = linecol;
          textcol[3] *= 0.6f;
        }

        auto rewardDesc = sh.nameByCell.at(ptm->spec.rMetrics.at(ri).cell);

        vector<F> labelVals {
          baser - rScale,
          baser - 0.1f * rScale,
          baser,
          baser + 0.1f * rScale,
          baser + rScale};
        for (auto labelVal : labelVals) {
          vec3 textpos = pos + vec3(-halfw - 0.05f, 
            0.5f * h + 0.5f * h * emphSmall((labelVal - baser) / max(0.01f, rScale)),
            0);
          dl.addThickCurve(vector<vec3> {textpos, textpos + vec3(0.05f, 0, 0)}, vec3(0, 0, 1), 
            textcol,
            0.02f, 
            1.2f, &glowingTextShader);
          dl.addText(textpos,
            vec3(0.15, 0, 0), vec3(0, -0.15, 0), vec3(0, 0, 1), 
            labelVal == 0 ? (rewardDesc + " = " + repr_0_3g(labelVal)) : repr_0_3g(labelVal),
            textcol,
            ysFonts.lrgFont, 
            1.0f, 0.5f, 
            1.3f, &glowingTextShader);
        }


      }

      ri++;
    }
    if (!exact) {
      dl.addLoading(pos + vec3(0, 0.5f * h, 0), 0.2f);
    }
  }
}
