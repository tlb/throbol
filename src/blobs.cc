#include "./defs.h"
#include <atomic>
#include <typeinfo>
#include <sys/random.h>
#include "./blobs.h"

static char localOriginHost[BlobRef::ORIGIN_HOST_SIZE];

static inline U8 *blobAlloc(size_t size)
{
  return (U8 *)aligned_alloc(MAX_ALIGN, (size + MAX_ALIGN - 1) & ~(MAX_ALIGN-1));
}

Blob::Blob(size_t _size) noexcept
  : size_(_size),
    data_(blobAlloc(_size))
{
}

Blob::Blob(U8 *_data, size_t _size) noexcept
  : size_(_size),
    data_(blobAlloc(_size))
{
  memcpy(data_, _data, size_);
}


Blob::Blob() noexcept
  : size_(0),
    data_(nullptr)
{
}

Blob::Blob(Blob &&other) noexcept
  : AllocTrackingMixin<Blob>(std::move(other)),
    size_(other.size_),
    data_(other.data_)
{
  other.size_ = 0;
  other.data_ = nullptr;
}

Blob::Blob(Blob const &other)
  : AllocTrackingMixin<Blob>(other),
    size_(other.size_),
    data_(blobAlloc(other.size_))
{
  memcpy(data_, other.data_, size_);
}

void Blob::zero()
{
  if (size_ > 0) {
    memset(data_, 0, size_);
  }
}


Blob& Blob::operator=(Blob &&other) noexcept
{
  AllocTrackingMixin<Blob>(*this) = std::move(other);
  if (&other != this) {
    if (data_) {
      free(data_);
    }
    data_ = other.data_;
    size_ = other.size_;
    other.data_ = nullptr;
    other.size_ = 0;
  }
  return *this;
}


Blob& Blob::operator=(Blob const &other) noexcept
{
  AllocTrackingMixin<Blob>(*this) = other;
  if (data_) {
    free(data_);
  }

  size_ = other.size_;
  data_ = blobAlloc(size_);
  memcpy(data_, other.data_, size_);

  return *this;
}


Blob::~Blob() noexcept
{
  if (data_) {
    free(data_);
    data_ = nullptr;
  }
  size_ = 0;
}

void Blob::alloc(size_t _size)
{
  if (data_) {
    free(data_);
  }
 
  size_ = _size;
  data_ = blobAlloc(_size);
}


void Blob::swap(Blob &other) noexcept
{
  std::swap(size_, other.size_);
  std::swap(data_, other.data_);
}



U64 parseChunkId(string const &chunkName)
{
  return stoull(chunkName, nullptr, 16);
}

string fmtChunkId(U64 chunkId)
{
  char chunkName[128];
  snprintf(chunkName, sizeof(chunkName), "%016llx", (unsigned long long)chunkId);
  return chunkName;
}

struct BlobFileInfo {

  int fd = -1;
  atomic<off_t> writePos = 0;
  atomic<size_t> lastUsedIndex = 0;

};

static map<U64, BlobFileInfo> blobFiles;
static std::mutex blobFilesMutex;
static size_t blobFilesLastUsed;
static size_t blobFilesOpenCount;

static size_t roundUp(size_t baseSize)
{
  return (baseSize + 7) & ~7;
}


string blobFilename(U64 chunkId)
{
  char ret[128];
  snprintf(ret, sizeof(ret), "blobs/zblob.%016llx.data", (unsigned long long)chunkId);
  return string(ret);
}


string BlobRef::getFilename() const
{
  return blobFilename(chunkId);
}


// Called with blobFilesMutex held
static bool openFile(BlobFileInfo &fileInfo, U64 chunkId)
{
  string fn = blobFilename(chunkId);
  int fd = open(fn.c_str(), O_CREAT | O_RDWR, 0666);
  if (fd < 0) {
    int open_err = errno;
    if (mkdir("blobs", 0777) < 0) {
      int mkdir_err = errno;
      if (errno == EEXIST) {
        L() << fn << ": " << strerror(open_err);
        return false;
      }
      else {
        L() << fn << ": " << strerror(mkdir_err);
        return false;
      }
    }
    fd = open(fn.c_str(), O_CREAT | O_RDWR, 0666);
    if (fd < 0) {
      L() << fn << ": " << strerror(errno);
      return false;
    }
  }
  fileInfo.fd = fd;
  blobFilesOpenCount++;
  struct stat st;
  if (fstat(fd, &st) < 0) {
    L() << fn << ": " << strerror(errno);
    return false;
  }
  fileInfo.writePos = st.st_size;
  return true;
}

// Called with blobFilesMutex held
static void cleanupBlobFiles()
{
  vector<map<U64, BlobFileInfo>::iterator> inorder;
  for (auto it = blobFiles.begin(); it != blobFiles.end(); it++) {
    if (!(it->second.fd < 0)) {
      inorder.push_back(it);
    }
  }
  sort(inorder.begin(), inorder.end(), [](auto a, auto b) {
    return a->second.lastUsedIndex < b->second.lastUsedIndex;
  });
  for (auto &it : inorder) { // oldest first
    assert(!(it->second.fd < 0)); // we checked before adding it to the list
    close(it->second.fd);
    it->second.fd = -1;
    blobFilesOpenCount --;
    blobFiles.erase(it); // depending on this not invalidating iterators
    if (blobFilesOpenCount < 20) return;
  }
}

static BlobFileInfo &getBlobInfo(U64 chunkId, bool pleaseOpen=true)
{
  std::lock_guard lock(blobFilesMutex);
  auto &fileInfo = blobFiles[chunkId];
  if (pleaseOpen && fileInfo.fd < 0) {
    if (blobFilesOpenCount > 100) {
      cleanupBlobFiles();
    }
    openFile(fileInfo, chunkId);
  }
  fileInfo.lastUsedIndex = blobFilesLastUsed ++;
  return fileInfo;
}

struct CachedBlob {

  Blob blob;
  U64 stamp = 0;
  
};

unordered_map<BlobRef, CachedBlob> blobCache;
std::mutex blobCacheMutex;

static U64 globalBlobStamp = 100;


Blob *getBlob(BlobRef const &id)
{
  std::lock_guard lock(blobCacheMutex);
  auto &slot = blobCache[id];
  if (slot.stamp == 0) {
    if (loadBlob(id, slot.blob)) {
      slot.stamp = globalBlobStamp++;
      return &slot.blob;
    }
    else {
      return nullptr;
    }
  }
  return &slot.blob;
}

void stashBlob(BlobRef const &id, Blob const &data)
{
  std::lock_guard lock(blobCacheMutex);
  auto &slot = blobCache[id];
  slot.blob = data;
  slot.stamp = globalBlobStamp++;
}

void stashBlob(BlobRef const &id, Blob &&data)
{
  std::lock_guard lock(blobCacheMutex);
  auto &slot = blobCache[id];
  slot.blob = std::move(data);
  slot.stamp = globalBlobStamp++;
}

void clearBlobCache()
{
  {
    std::lock_guard lock(blobCacheMutex);
    blobCache.clear();
  }
  {
    std::lock_guard lock(blobFilesMutex);
    for (auto &[chunkId, fileInfo] : blobFiles) {
      if (!(fileInfo.fd < 0)) {
        close(fileInfo.fd);
        fileInfo.fd = -1;
        blobFilesOpenCount --;
      }
    }
  }
}

bool loadBlobFile(U64 chunkId, Blob &data)
{
  auto &fileInfo = getBlobInfo(chunkId);
  if (fileInfo.fd < 0) {
    return false;
  }

  struct stat st;
  if (fstat(fileInfo.fd, &st) < 0) {
    return false;
  }
  auto size = st.st_size;

  data.alloc(size);

  auto nr = pread(fileInfo.fd, data.data(), size, 0);

  if (nr < 0) {
    L() << "Loading blobfile " << chunkId << ": " << strerror(errno);
    return false;
  }
  else if (nr == 0) {
    if (0) L() << "No data in blobfile " << chunkId;
    return false;
  }
  else if (nr < size) {
    L() << "Partial load from blobfile " << chunkId <<
      ": " << nr << "/" << size;
    return false;
  }
  return true;
}

bool loadBlob(BlobRef const &id, Blob &data)
{
  if (id.partSize > 0xfffffff) {
    L() << "loadBlob: Silly " << id;
    return false;
  }
  auto &fileInfo = getBlobInfo(id.chunkId);
  if (fileInfo.fd < 0) {
    return false;
  }
  data.alloc(id.partSize);
  auto nr = pread(fileInfo.fd, data.data(), id.partSize, id.partOfs);
  if (nr < 0) {
    L() << "Loading from " << id << ": " << strerror(errno);
    return false;
  }
  else if (nr == 0) {
    if (0) L() << "No data for " << id;
    return false;
  }
  else if (nr < ssize_t(id.partSize)) {
    L() << "Partial load from " << id << ": " << nr << "/" << id.partSize;
    return false;
  }
  else {
    if (0) L() << "Full load from " << id << ": " << nr << "/" << id.partSize;
    return true;
  }
}

static void setLocalOriginHost()
{
  char hn[256];
  if (gethostname(hn, sizeof(hn)) < 0) {
    L() << "gethostname: " << strerror(errno);
    abort();
  }
  hn[sizeof(hn)-1] = 0;
  char *p = hn;
  auto first = strsep(&p, ".");
  if (!*first) {
    L() << "Empty host name (" << p << ")";
    return;
  }
  if (strlen(first) > BlobRef::ORIGIN_HOST_SIZE - 2) {
    L() << "Host name too long\n";
    abort();
  }
  // Yes, I want to zero out the rest of the buffer
  strncpy(localOriginHost, first, BlobRef::ORIGIN_HOST_SIZE);
  if (0) L() << "origin host " << localOriginHost;
}

BlobRef mkBlob(U64 chunkId, U8 const *data, size_t dataSize)
{
  auto &fileInfo = getBlobInfo(chunkId);
  if (fileInfo.fd < 0) {
    return BlobRef();
  }

  if (!localOriginHost[0]) setLocalOriginHost();

  off_t partOfs = fileInfo.writePos.fetch_add(roundUp(dataSize));

  auto rc = pwrite(fileInfo.fd, data, dataSize, partOfs);
  if (rc < 0) {
    L() << "write chunk: " << strerror(errno);
    return BlobRef();
  }
  return BlobRef(chunkId, partOfs, dataSize, localOriginHost);
}

BlobRef mkBlob(U64 chunkId, Blob const &data)
{
  return mkBlob(chunkId, data.data(), data.size());
}

void saveBlob(BlobRef const &id, U8 const *data, size_t dataSize)
{
  if (dataSize != id.partSize) {
    L() << "saveBlob: mismatch dataSize=" << dataSize << " partSize=" << id.partSize;
    return;
  }
  auto &fileInfo = getBlobInfo(id.chunkId);
  auto rc = pwrite(fileInfo.fd, data, dataSize, id.partOfs);
  if (rc < 0) {
    L() << "saveBlob: write chunk: " << strerror(errno);
  }
}

void saveBlob(BlobRef const &id, Blob const &data)
{
  saveBlob(id, data.data(), data.size());
}

bool saveBlobFile(U64 chunkId, Blob const &data)
{
  auto &fileInfo = getBlobInfo(chunkId);
  if (fileInfo.fd < 0) {
    return false;
  }

  auto rc = pwrite(fileInfo.fd, data.data(), data.size(), 0);
  if (rc < 0) {
    L() << "write chunk: " << strerror(errno);
    return false;
  }
  return true;
}

ostream & operator <<(ostream &s, BlobRef const &a)
{
  s << "BlobRef(" << repr_016x(a.chunkId) <<
     ", " << repr(a.partOfs) << 
     ", " << repr(a.partSize) +
     ", " << quoteStringC(string(a.originHost)) + ")";
  return s;
}


U64 mkBlobChunkId()
{
  U64 ret = (U64)time(nullptr) << 32;
  U8 sub[2];
#if defined(__APPLE__) || defined(__EMSCRIPTEN__)
  if (getentropy(sub, sizeof(sub)) < 0) {
    dielog("getentropy: " << strerror(errno));
  }
#else
  if (getrandom(sub, sizeof(sub), 0) < 0) {
    dielog("getrandom: " << strerror(errno));
  }
#endif

  ret += ((U64)sub[0] << 24) + ((U64)sub[1] << 16);

  return ret;
}


void rotateBlobChunkId(U64 &chunkId)
{
  if (chunkId == 0) {
    chunkId = mkBlobChunkId();
  }
  auto &fileInfo = getBlobInfo(chunkId, false);
  if (fileInfo.fd < 0) {
  }
  else {
    if (fileInfo.writePos.load() >= 9*1024*1024) { // keep under 10 mb
      chunkId++;
    }
  }
}
