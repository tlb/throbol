#include "./core.h"
#include "../geom/gl_headers.h"
#include "../geom/g8.h"
#include "numerical/numerical.h"
#include "numerical/polyfit.h"
#include "./sheet_ui.h"
#include "./scope_math.h"
#include "./draw.h"


DrawOpHandle op_union(CpuState &cpu, DrawOpHandle a, DrawOpHandle b)
{
  if (!a) return b;
  if (!b) return a;
  return DrawOpHandle(cpu.pool->mk<DrawOpUnion>(a, b), nullptr);
}

ostream & operator <<(ostream &s, WithSheet<DrawOpHandle> const &sha)
{
  auto &[sh, a] = sha;
  if (!a) return s << "null";

  switch (a.it->op) {

    case DrawOpUnion::tid:
    {
      auto aa = a.it->as<DrawOpUnion>();
      return s << aa->lhs << " | " << aa->rhs;
    }

    case DrawOpTransform::tid:
    {
      auto aa = a.it->as<DrawOpTransform>();
      return s << aa->transform << " * (" << aa->contents << ")";
    }

    case DrawOpColor::tid:
    {
      auto aa = a.it->as<DrawOpColor>();
      return s << aa->transform << " * (" << aa->contents << ")";
    }

    case DrawOpLine::tid:
    {
      auto aa = a.it->as<DrawOpLine>();
      return s << "line(" << aa->p0 << ", " << aa->p1 << ")";
    }

    case DrawOpLines::tid:
    {
      auto aa = a.it->as<DrawOpLines>();
      return s << "line(" << aa->pts << ")";
    }

    case DrawOpDisk::tid:
    {
      auto aa = a.it->as<DrawOpDisk>();
      return s << "disk(" << aa->center << ", " << aa->radius << ")";
    }

    case DrawOpTube::tid:
    {
      auto aa = a.it->as<DrawOpTube>();
      return s << "tube(" << aa->pts << ")";
    }

    case DrawOpText::tid:
    {
      auto aa = a.it->as<DrawOpText>();
      return s << "text(" << aa->p0 << ", " << aa->size << ", " << cppEscape(aa->text) << ")";
    }

    case DrawOpSolid::tid:
    {
      auto aa = a.it->as<DrawOpSolid>();
      return s << "solid(" << cppEscape(aa->fn) << ")";
    }

    case DrawOpAxisArrows::tid:
    {
      return s << "axisArrows()";
    }

    case DrawOpDial::tid:
    {
      auto aa = a.it->as<DrawOpDial>();
      return s << "dial(" << aa->center << ", " << aa->radius << ", " << aa->value << ", " << aa->range << ")";
    }

    case DrawOpLabel::tid:
    {
      auto aa = a.it->as<DrawOpLabel>();
      return s << "label(" << aa->value << ", " << cppEscape(aa->text) << ")";
    }

    case DrawOpPolicyTerrain::tid:
    {
      auto aa = a.it->as<DrawOpPolicyTerrain>();
      // FIXME: we need the sheet here to name the parameters
      s << "policyTerrain(" <<
        ", " << WithSheet(sh, aa->xVar) << 
        ", " << WithSheet(sh, aa->yVar) <<
        ", " << WithSheet(sh, aa->rMetricRef) <<
        ")";
      return s;
    }

    default:
      return s << "?";
  }
}
