#include "./emit.h"
#include "./emit_macros.h"
#include <Eigen/Geometry>


deftype(length)
{
  return typetag<F>();
}

defemit(length)
{
  defop1g(vec2, F, r = a.norm(), ag += rg / max(r, 0.001f) * a);
  defop1g(vec3, F, r = a.norm(), ag += rg / max(r, 0.001f) * a);
  defop1g(vec4, F, r = a.norm(), ag += rg / max(r, 0.001f) * a);
  return setInvalidArgs();
}

defusage(length, R"(
  length(a ∊ vec) -- L2 norm)");


deftype(normalize)
{
  return eval(n->ch(0));
}

defemit(normalize)
{
  defop1(vec2, vec2, r = a.normalized());
  defop1(vec3, vec3, r = a.normalized());
  defop1(vec4, vec4, r = a.normalized());
  return setInvalidArgs();
}

defusage(normalize, R"(
  normalize(a ∊ vec) -- scale so that L2 norm is 1)");


deftype(distance)
{
  return typetag<F>();
}

defemit(distance)
{
  defop2g(vec2, vec2, F, r = (a - b).norm(), (ag += rg / max(0.001f, r) * (a - b), bg += rg / max(0.001f, r) * (b - a)));
  defop2g(vec3, vec3, F, r = (a - b).norm(), (ag += rg / max(0.001f, r) * (a - b), bg += rg / max(0.001f, r) * (b - a)));
  defop2g(vec4, vec4, F, r = (a - b).norm(), (ag += rg / max(0.001f, r) * (a - b), bg += rg / max(0.001f, r) * (b - a)));
  defop2g(mat2, mat2, F, r = (a - b).norm(), (ag += rg / max(0.001f, r) * (a - b), bg += rg / max(0.001f, r) * (b - a)));
  defop2g(mat3, mat3, F, r = (a - b).norm(), (ag += rg / max(0.001f, r) * (a - b), bg += rg / max(0.001f, r) * (b - a)));
  defop2g(mat4, mat4, F, r = (a - b).norm(), (ag += rg / max(0.001f, r) * (a - b), bg += rg / max(0.001f, r) * (b - a)));
  return setInvalidArgs();
}

defusage(distance, R"(
  distance(a ∊ vec, b ∊ vec) -- L2 norm of a-b)");


deftype(outerProduct)
{
  auto at = eval(n->ch(0));
  auto bt = eval(n->ch(1));
  if (at.ist<vec2>() && bt.ist<vec2>()) return typetag<mat2>();
  if (at.ist<vec3>() && bt.ist<vec3>()) return typetag<mat3>();
  if (at.ist<vec4>() && bt.ist<vec4>()) return typetag<mat4>();
  if (at.ist<mat>() && bt.ist<mat>() && at.nc == 1 && bt.nc == 1) {
    return TypeTag{TK_mat, at.nr, bt.nr};
  }
  return setError();  
}

defemit(outerProduct)
{
  defop2g(vec2, vec2, mat2, r = a * b.transpose(), (ag += rg * b, bg += a.transpose() * rg));
  defop2g(vec3, vec3, mat3, r = a * b.transpose(), (ag += rg * b, bg += a.transpose() * rg));
  defop2g(vec4, vec4, mat4, r = a * b.transpose(), (ag += rg * b, bg += a.transpose() * rg));

  defop2rg(mat, mat, mat,
    args[0].t.nc == 1 && args[1].t.nc == 1,
    allocResult({TK_mat, args[0].t.nr, args[1].t.nr}),
    r = a * b.transpose(),
    (ag += rg * b, bg += a.transpose() * rg));

  return setInvalidArgs();
}

defusage(outerProduct, R"(
  outerProduct(a ∊ vec, b ∊ vec))");


deftype(transpose)
{
  auto at = eval(n->ch(0));
  if (at.ist<mat>() || at.ist<cmat>()) {
    return TypeTag(at.kind, at.nc, at.nr);
  }
  return setError();
}

defemit(transpose)
{
  defop1g(mat2, mat2, r = a.transpose(), ag += rg.transpose());
  defop1g(mat3, mat3, r = a.transpose(), ag += rg.transpose());
  defop1g(mat4, mat4, r = a.transpose(), ag += rg.transpose());

  defop1rg(mat, mat,
    true,
    allocResult({TK_mat, args[0].t.nc, args[0].t.nr}),
    r = a.transpose(),
    ag += rg.transpose());

  return setInvalidArgs();
}

defusage(transpose, R"(
  transpose(mat))");


deftype(determinant)
{
  return typetag<F>();
}

defemit(determinant)
{
  defop1(mat2, F, r = a.determinant());
  defop1(mat3, F, r = a.determinant());
  defop1(mat4, F, r = a.determinant());
  defop1(mat, F, r = a.determinant());
  return setInvalidArgs();
}

defusage(determinant, R"(
  determinant(mat))");


deftype(inverse)
{
  auto at = eval(n->ch(0));
  if ((at.ist<mat>() || at.ist<cmat>()) && at.nc == at.nr) {
    return at;
  }
  return setError();
}

defemit(inverse)
{
  defop1(mat2, mat2, r = a.inverse());
  defop1(mat3, mat3, r = a.inverse());
  defop1(mat4, mat4, r = a.inverse());
  return setInvalidArgs();
}

defusage(inverse, R"(
  inverse(mat2|3|4))");


deftype(sum)
{
  auto at = eval(n->ch(0));
  if (at.ist<mat>()) return typetag<F>();
  if (at.ist<cmat>()) return typetag<CF>();
  return setError();
}

defemit(sum)
{
  defop1(mat, F, r = a.sum());
  defop1(cmat, CF, r = a.sum());
  return setInvalidArgs();
}

defusage(sum, R"(
  sum(matNxN)
  sum(cmatNxN))");


deftype(prod)
{
  auto at = eval(n->ch(0));
  if (at.ist<mat>()) return typetag<F>();
  if (at.ist<cmat>()) return typetag<CF>();
  return setError();
}

defemit(prod)
{
  defop1(mat, F, r = a.prod());
  defop1(cmat, CF, r = a.prod());
  return setInvalidArgs();
}

defusage(prod, R"(
  prod(matNxN)
  prod(cmatNxN))");


deftype(homogeneous)
{
  auto at = eval(n->ch(0));
  if (at.ist<vec3>()) return typetag<vec4>();
  if (at.ist<mat3>()) return typetag<mat4>();
  return setError();
}

defemit(homogeneous)
{
  defop1(vec3, vec4, r = a.homogeneous());
  defop1(mat3, mat4, ((r = mat4::Identity()), (r.template topLeftCorner<3, 3>() = a)));
  return setInvalidArgs();
}

defusage(homogeneous, R"(
  homogeneous(vec3) -- add trailing 1
  homogeneous(mat3) -- add row & col)");


deftype(hnormalized)
{
  return typetag<vec3>();
}

defemit(hnormalized)
{
  defop1(vec4, vec3, r = a.hnormalized());
  return setInvalidArgs();
}

defusage(hnormalized, R"(
  hnormalized(vec4) -- returns vec3)");



deftype(head)
{
  if (n->nch() == 2) {
    auto at = eval(n->ch(0));
    if (at.ist<mat>()) {
      if (auto bconst = getConstInt(n->ch(1))) {
        int b_nr = *bconst < 0 ? at.nr + *bconst : *bconst;
        if (b_nr > 0) {
          return TypeTag(TK_mat, b_nr, at.nc);
        }
      }
    }
  }
  return setError();
}

defemit(head)
{
  if (n->nch() == 2) {
    evalSpecial(n->ch(0));
    if (argmatch1(vec)) {
      auto at = args[0].t;
      if (auto bconst = getConstInt(n->ch(1))) {
        int b_nr = *bconst < 0 ? at.nr + *bconst : *bconst;
        if (!(b_nr > 0)) return setError("d must be positive");
        defop1r(vec, vec,
          at.nc == 1 && at.nr >= b_nr && b_nr > 0,
          allocResult(TypeTag(TK_mat, b_nr, at.nc)),
          r = a.head(b_nr));
      }
      else {
        return setError("Dimensions must be constant ints");
      }
    }
  }
  return setInvalidArgs();
}

defusage(head, R"(
  head(vec, len ∊ int))");


deftype(tail)
{
  if (n->nch() == 2) {
    auto at = eval(n->ch(0));
    if (at.ist<mat>()) {
      if (auto bconst = getConstInt(n->ch(1))) {
        int b_nr = *bconst < 0 ? at.nr + *bconst : *bconst;
        if (b_nr > 0) {
          return TypeTag(TK_mat, b_nr, at.nc);
        }
      }
    }
  }
  return setError();
}

defemit(tail)
{
  if (n->nch() == 2) {
    evalSpecial(n->ch(0));
    if (argmatch1(vec)) {
      auto at = args[0].t;
      if (auto bconst = getConstInt(n->ch(1))) {
        int b_nr = *bconst < 0 ? at.nr + *bconst : *bconst;
        if (!(b_nr > 0)) return setError("len must be positive");
        defop1r(vec, vec,
          at.nc == 1 && at.nr >= b_nr && b_nr > 0,
          allocResult({TK_mat, b_nr, at.nc}),
          r = a.tail(b_nr));
      }
      else {
        return setError("Dimensions must be constant ints");
      }
    }
  }
  return setInvalidArgs();
}

defusage(tail, R"(
  tail(vec, len ∊ int))");


deftype(segment)
{
  if (n->nch() == 3) {
    auto at = eval(n->ch(0));
    if (at.ist<mat>()) {
      auto bconst = getConstInt(n->ch(1));
      auto cconst = getConstInt(n->ch(2));
      if (bconst && cconst) {
        int c_nr = *cconst < 0 ? at.nr + *cconst : *cconst;
        if (c_nr > 0) {
          return TypeTag(TK_mat, c_nr, at.nc);
        }
      }
    }
  }
  return setError();
}

defemit(segment)
{
  if (n->nch() == 3) {
    evalSpecial(n->ch(0));
    if (argmatch1(vec)) {
      auto at = args[0].t;
      auto bconst = getConstInt(n->ch(1));
      auto cconst = getConstInt(n->ch(2));
      if (bconst && cconst) {
        int b_pos = *bconst < 0 ? at.nr + *bconst : *bconst;
        int c_nr = *cconst < 0 ? at.nr + *cconst : *cconst;
        if (!(c_nr > 0)) return setError("len must be positive");
        if (!(b_pos >= 0)) return setError("pos must be nonnegative");
        if (!(at.nr >= b_pos + c_nr)) return setError("pos + len must be <= v.size");
        defop1r(vec, vec,
          at.nc == 1 && at.nr >= b_pos + c_nr && c_nr > 0 && b_pos >= 0,
          allocResult({TK_mat, c_nr, at.nc}),
          r = a.segment(b_pos, c_nr));
      }
      else {
        return setError("Dimensions must be constant ints");
      }
    }
  }
  return setInvalidArgs();
}

defusage(segment, R"(
  segment(v ∊ vec, pos ∊ int, len ∊ int))");



deftype(concat)
{
  auto at = eval(n->ch(0));
  auto bt = eval(n->ch(1));
  if (at.ist<vec>() && bt.ist<vec>()) {
    return TypeTag(TK_mat, at.nr + bt.nr, 1);
  }
  return setError();
}

defemit(concat)
{
  defop2r(vec, vec, vec,
    true,
    allocResult({TK_mat, args[0].t.nr + args[1].t.nr, 1}),
    (r.head(av.t.nr) = a, r.tail(bv.t.nr) = b));
  return setInvalidArgs();
}

defusage(concat, R"(
  concat(vec, vec))");



deftype(block)
{
  auto at = eval(n->ch(0));
  if (at.ist<mat>()) {
    auto bconst = getConstInt(n->ch(1));
    auto cconst = getConstInt(n->ch(2));
    auto dconst = getConstInt(n->ch(3));
    auto econst = getConstInt(n->ch(4));
    if (bconst && cconst && dconst && econst) {
      int d_nr = *dconst;
      int e_nc = *econst;
      return TypeTag(TK_mat, d_nr, e_nc);
    }
  }
  return setError();
}

defemit(block)
{
  if (n->nch() == 5) {
    evalSpecial(n->ch(0));
    if (argmatch1(mat)) {
      auto at = args[0].t;
      auto bconst = getConstInt(n->ch(1));
      auto cconst = getConstInt(n->ch(2));
      auto dconst = getConstInt(n->ch(3));
      auto econst = getConstInt(n->ch(4));
      if (bconst && cconst && dconst && econst) {
        int b_pos = *bconst;
        int c_pos = *cconst;
        int d_nr = *dconst < 0 ? at.nr + *dconst : *dconst;
        int e_nc = *econst < 0 ? at.nc + *econst : *econst;
        if (!(d_nr > 0)) return setError("rows must be positive");
        if (!(e_nc > 0)) return setError("cols must be positive");
        defop1r(mat, mat,
          d_nr > 0 && e_nc > 0 && at.nr >= b_pos + d_nr && at.nc >= c_pos + e_nc,
          allocResult(TypeTag(TK_mat, d_nr, e_nc)),
          r = a.block(b_pos, c_pos, d_nr, e_nc));
      }
      else {
        return setError("Dimensions must be constant ints");
      }
    }
  }
  return setInvalidArgs();
}

defusage(block, R"(
  segment(vec, i ∊ int, j ∊ int, rows ∊ int, cols ∊ int))");


deftype(posquat)
{
  return TypeTag(TK_mat, 7, 1);
}

defemit(posquat)
{
  defop1r(mat4, vec,
    true,
    allocResult({TK_mat, 7, 1}),
    (r << a.template topRightCorner<3, 1>(), Eigen::Quaternionf(a.template topLeftCorner<3, 3>()).coeffs()));
  return setInvalidArgs();
}

defusage(posquat, R"(
  posquat(mat4) -- returns vec7 of position (3) and quaternion (4) orientation)");

deftype(solve)
{
  auto at = eval(n->ch(0));
  auto bt = eval(n->ch(1));
  if (at.ist<mat>() && bt.ist<vec>()) {
    return TypeTag(TK_mat, at.nc, 1);
  }
  return setError();
}

defemit(solve)
{
  string_view alg = n->data<char const *>();
  if (alg == "jacobi_svd") {
    defop2r(mat, vec, vec,
      args[0].t.nr == args[1].t.nr,
      allocResult({TK_mat, args[0].t.nc, 1}),
      r = a.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(b));
  }
  else if (alg == "bdc_svd") {
    defop2r(mat, vec, vec,
      args[0].t.nr == args[1].t.nr,
      allocResult({TK_mat, args[0].t.nc, 1}),
      r = a.bdcSvd().solve(b));
  }
  else if (alg == "full_piv_lu") {
    defop2r(mat, vec, vec,
      args[0].t.nr == args[1].t.nr,
      allocResult({TK_mat, args[0].t.nc, 1}),
      r = a.fullPivLu().solve(b));
  }
  else if (alg == "householder_qr") {
    defop2r(mat, vec, vec,
      args[0].t.nr == args[1].t.nr,
      allocResult({TK_mat, args[0].t.nc, 1}),
      r = a.householderQr().solve(b));
  }
  else if (alg == "cod") {
    defop2r(mat, vec, vec,
      args[0].t.nr == args[1].t.nr,
      allocResult({TK_mat, args[0].t.nc, 1}),
      r = a.completeOrthogonalDecomposition().solve(b));
  }
  return setInvalidArgs();
}

defusage(solve, R"(
  solve.svd(A ∊ mat, b ∊ vec) -- return x, the least squares solution to Ax = b)
  solve.jacobi_svd, solve.bdc_svd, solve.full_piv_lu, solve.householder_qr, solve.cod: other algorithms
  )");

deftype(linspacedMat)
{
  return n->data<TypeTag>();
}

defemit(linspacedMat)
{
  if (n->data<TypeTag>().isMat()) defop2r(F, F, vec, true, allocResult(n->data<TypeTag>()), r.setLinSpaced(a, b));
  if (n->data<TypeTag>().isCmat()) defop2r(CF, CF, cvec, true, allocResult(n->data<TypeTag>()), r.setLinSpaced(a, b));
  return setInvalidArgs();
}

defusage(linspacedMat, R"(
  vecN.linspaced(lo ∊ F, hi ∊ F))");


deftype(onesMat)
{
  return n->data<TypeTag>();
}

defemit(onesMat)
{
  if (n->data<TypeTag>().isMat()) defop0r(mat, true, allocResult(n->data<TypeTag>()), r.setOnes());
  if (n->data<TypeTag>().isCmat()) defop0r(cmat, true, allocResult(n->data<TypeTag>()), r.setOnes());
  return setInvalidArgs();
}

defusage(onesMat, R"(
  vecN.ones())");


deftype(zeroMat)
{
  return n->data<TypeTag>();
}

defemit(zeroMat)
{
  if (n->data<TypeTag>().isMat()) defop0r(mat, true, allocResult(n->data<TypeTag>()), r.setZero());
  if (n->data<TypeTag>().isCmat()) defop0r(cmat, true, allocResult(n->data<TypeTag>()), r.setZero());
  return setInvalidArgs();
}

defusage(zeroMat, R"(
  vecN.zero()
  matNxM.zero())");


deftype(constantMat)
{
  return n->data<TypeTag>();
}

defemit(constantMat)
{
  if (n->data<TypeTag>().isMat()) defop1r(F, mat, true, allocResult(n->data<TypeTag>()), r.setConstant(a));
  if (n->data<TypeTag>().isCmat()) defop1r(CF, cmat, true, allocResult(n->data<TypeTag>()), r.setConstant(a));
  return setInvalidArgs();
}

defusage(constantMat, R"(
  vecN.constant(element ∊ F)
  cvecN.constant(element ∊ CF)
  matNxM.constant(element ∊ F)
  cmatNxM.constant(element ∊ CF))");

deftype(identityMat)
{
  return n->data<TypeTag>();
}

defemit(identityMat)
{
  if (n->data<TypeTag>() == typetag<mat2>()) defop0(mat2, r.setIdentity());
  if (n->data<TypeTag>() == typetag<mat3>()) defop0(mat3, r.setIdentity());
  if (n->data<TypeTag>() == typetag<mat4>()) defop0(mat4, r.setIdentity());
  if (n->data<TypeTag>().isSquareMat()) defop0r(mat, true, allocResult(n->data<TypeTag>()), r.setIdentity());
  if (n->data<TypeTag>().isSquareCmat()) defop0r(cmat, true, allocResult(n->data<TypeTag>()), r.setIdentity());
  return setInvalidArgs();
}

defusage(identityMat, R"(
  matN.identity())");


deftype(unitMat)
{
  return n->data<TypeTag>();
}

template <typename scalar_t>
void setUnitMat(CpuState &cpu, Map<Eigen::Matrix<scalar_t, Eigen::Dynamic, Eigen::Dynamic>> &r, F a, F b, int batchi)
{
  r.setZero();
  Index ai = Index(floor(a));
  Index bi = Index(floor(b));
  if (ai >= 0 && ai < r.rows() && bi >= 0 && bi < r.cols()) {
    r(ai, bi) = 1.0f;
  }
}

template <typename scalar_t>
void setUnitVec(CpuState &cpu, Map<Eigen::Matrix<scalar_t, Eigen::Dynamic, 1>> &r, F a, int batchi)
{
  r.setZero();
  Index ai = Index(floor(a));
  if (ai >= 0 && ai < r.rows()) {
    r.setUnit(ai);
  }
}

defemit(unitMat)
{
  // WRITEME: optimize constants like vec4.unit(2)
  defop1r(F, vec, true, allocResult(n->data<TypeTag>()), setUnitVec(cpu, r, a, batchi));
  defop1r(F, cvec, true, allocResult(n->data<TypeTag>()), setUnitVec(cpu, r, a, batchi));
  defop2r(F, F, mat, true, allocResult(n->data<TypeTag>()), setUnitMat(cpu, r, a, b, batchi));
  defop2r(F, F, cmat, true, allocResult(n->data<TypeTag>()), setUnitMat(cpu, r, a, b, batchi));
  return setInvalidArgs();
}

defusage(unitMat, R"(
  vecN.unit(i ∊ int)
  matNxM.unit(i ∊ int, j ∊ int))");


bool readCsv(MatrixXf &m, string const &fn)
{
  ifstream in(fn);
  if (in.is_open()) {

    string line;
    Index row = 0;
    while (getline(in, line)) {
      if (line.empty()) continue;
      if (line[0] == '#') continue;
      Index col = 0;
      for (auto cell : splitChar(line, ',')) {
        if (row < m.rows() && col < m.cols()) m(row, col) = atof(cell.c_str());
        col++;
      }
      row++;
    }
    in.close();
  }
  return true;
}

MatrixXf *CompiledSheet::loadCsv(string name, Index nr, Index nc)
{
  auto &slot = extMatByFilename[name];
  if (!slot) {
    auto m = make_shared<MatrixXf>(nr, nc);
    m->setZero();
    readCsv(*m, name);
    slot = m;
  }
  return slot.get();
}

deftype(csvfileMat)
{
  return n->data<TypeTag>();
}

defemit(csvfileMat)
{
  auto t = n->data<TypeTag>();
  if (t.kind == TK_mat) {
    if (auto name = getConstString(&she.buildlife, n->ch(0))) {
      auto m = she.loadCsv(string(*name), t.nr, t.nc);
      if (m && m->rows() == t.nr && m->cols() == t.nc) {
        defopnr(mat, true, allocResult(t), r = *m);
      }
    }
  }
  // TODO: TK_cmat
  return setInvalidArgs();
}

defusage(csvfileMat, R"(
  matMxN.csvfile(filename ∊ string) -- returns a matrix read from disk)");
