#include "./test_utils.h"
#include "./api.h"

TEST_CASE("Schema comparison works", "[api]") {

  auto s0 = YAML::Load(R"(
- apiType: "test"
  apiInstance: "test0"
  apiVersion: "test3.1"
  apiSchema:
    - name: "foo"
      dir: "sensor"
      ofs: 40
      type: F
      version: 1
      size: 4
    - name: "bar"
      dir: "actuator"
      ofs: 44
      type: F
      version: 1
      size: 4
)");

  {
    ostringstream l;
    REQUIRE(checkSchemaMatch(s0, s0, l));
  }

  auto s1 = YAML::Load(R"(
- apiType: "test"
  apiInstance: "test0"
  apiVersion: "test3.2"
  apiSchema:
    - name: "foo"
      dir: "sensor"
      ofs: 41
      type: F
      version: 1
      size: 4
    - name: "bar"
      dir: "actuator"
      ofs: 44
      type: F
      version: 1
      size: 4
)");

  {
    ostringstream l;
    REQUIRE(!checkSchemaMatch(s0, s1, l));
    if (0) L() << l.str();
    REQUIRE(isMatch(l.str(), "version mismatch: test3\\.1 vs test3\\.2"));
    REQUIRE(isMatch(l.str(), "foo: F\\(dir=sensor, ofs=40, ver=1, size=4\\) vs F\\(dir=sensor, ofs=41, ver=1, size=4\\)"));
  }


  auto s2 = YAML::Load(R"(
- apiType: "test"
  apiInstance: "test0"
  apiVersion: "test3.1"
  apiSchema:
    - name: "foo"
      dir: "sensor"
      ofs: 40
      type: F
      version: 1
      size: 4
    - name: "bar"
      dir: "actuator"
      ofs: 44
      type: F
      version: 1
      size: 4
- apiType: "bogus"
  apiInstance: "bogus0"
  apiVersion: "bogus9.9"
  apiSchema: []
)");

  {
    ostringstream l;
    REQUIRE(!checkSchemaMatch(s0, s2, l));
    if (0) L() << l.str();
  }



}
