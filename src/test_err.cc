#include "./test_utils.h"

#ifdef notyet
TEST_CASE("type err 1", "[err]")
{
  auto shp = make_shared<Sheet>();
  shp->undumpPage(R"(
bar: foo + vec2(1,1)
foo: 3
)", 0);
  REQUIRE(shp);
  auto &sh = *shp;
  sh.verbose--;
  sh.compile();
  auto [err, loc] = sh.getCellError(0);
  CHECK(string(err) == R"(Usage:
  T + T
Called as add(F, vec2)
)");
}

TEST_CASE("parse err 1", "[err]") {

  auto shp = make_shared<Sheet>();
  shp->undumpPage(R"(
bar: foo +
foo: 3
)", 0);
  REQUIRE(shp);
  auto &sh = *shp;
  sh.verbose--;
  sh.compile();

  auto [err, loc] = sh.getCellError(0);
  CHECK(string(err) == "Expected identifier");
}
#endif


TEST_CASE("argument err 1", "[err]")
{
  Sheet sh;
  REQUIRE(sh.loadTest(R"(
bar: easein(1,1)
)"));
  auto [err, loc] = sh.getCellError(0);
  CHECK(string(err) == R"(Usage:
  easein(x ∊ F) -- 0 when x <= 0, 1 when x >= 1, raised cos interp
Called as easein(F, F)
)");
}


TEST_CASE("argument err 2", "[err]")
{
  Sheet sh;
  REQUIRE(sh.loadTest(R"(
bar: lerp(1,1,'foo)
)"));
  auto [err, loc] = sh.getCellError(0);
  CHECK(string(err) == R"(Usage:
  lerp(v0 ∊ F|CF|vec|mat, v1 ∊ same, t ∊ F) -- v0 + t * (v1 - v0)
Called as lerp(F, F, SymbolSet)
)");
}

TEST_CASE("argument err 3", "[err]")
{
  Sheet sh;
  REQUIRE(sh.loadTest(R"(
bar: outerProduct([1,2,3], 7)
)"));
  auto [err, loc] = sh.getCellError(0);
  CHECK(string(err) == R"(Usage:
  outerProduct(a ∊ vec, b ∊ vec)
Called as outerProduct(vec3, F)
)");
  
}
