#include "./emit.h"
#include "./emit_macros.h"


deftype(sub)
{
  auto at = eval(n->ch(0));
  auto bt = eval(n->ch(1));
  if (at == bt) return at;
  if (at.ist<CF>() && bt.convist<F>()) return at;
  if (at.convist<F>() && bt.ist<CF>()) return bt;
  return setError();
}

defemit(sub)
{
  defop2g_simd(F, F, F, r = a - b, (ag += rg, bg -= rg));
  defop2g_simd(F, CF, CF, r = a - b, (ag += rg.real(), bg -= rg));
  defop2g_simd(CF, F, CF, r = a - b, (ag += rg, bg -= rg.real()));
  defop2g_simd(CF, CF, CF, r = a - b, (ag += rg, bg -= rg));

  defop2g(vec2, vec2, vec2, r = a - b, (ag += rg, bg -= rg));
  defop2g(vec3, vec3, vec3, r = a - b, (ag += rg, bg -= rg));
  defop2g(vec4, vec4, vec4, r = a - b, (ag += rg, bg -= rg));
  defop2g(mat2, mat2, mat2, r = a - b, (ag += rg, bg -= rg));
  defop2g(mat3, mat3, mat3, r = a - b, (ag += rg, bg -= rg));
  defop2g(mat4, mat4, mat4, r = a - b, (ag += rg, bg -= rg));

  defop2rg(mat, mat, mat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_mat, args[0].t.nr, args[0].t.nc}),
    r = a - b,
    (ag += rg, bg -= rg));
  defop2rg(cmat, cmat, cmat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_cmat, args[0].t.nr, args[0].t.nc}),
    r = a - b,
    (ag += rg, bg -= rg));

  return setInvalidArgs();
}

defusage(sub, R"(
  T - T)");


deftype(compsub)
{
  auto at = eval(n->ch(0));
  auto bt = eval(n->ch(1));
  if (at == bt) return at;
  return setError();
}

defemit(compsub)
{
  defop2g_simd(F, F, F, r = a - b, (ag += rg, bg -= rg));
  defop2g_simd(F, CF, CF, r = a - b, (ag += rg.real(), bg -= rg));
  defop2g_simd(CF, F, CF, r = a - b, (ag += rg, bg -= rg.real()));
  defop2g_simd(CF, CF, CF, r = a - b, (ag += rg, bg -= rg));

  defop2g(vec2, vec2, vec2, r = a - b, (ag += rg, bg -= rg));
  defop2g(vec3, vec3, vec3, r = a - b, (ag += rg, bg -= rg));
  defop2g(vec4, vec4, vec4, r = a - b, (ag += rg, bg -= rg));
  defop2g(mat2, mat2, mat2, r = a - b, (ag += rg, bg -= rg));
  defop2g(mat3, mat3, mat3, r = a - b, (ag += rg, bg -= rg));
  defop2g(mat4, mat4, mat4, r = a - b, (ag += rg, bg -= rg));

  defop2rg(mat, mat, mat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_mat, args[0].t.nr, args[0].t.nc}),
    r.array() = a.array() - b.array(),
    (ag.array() += rg.array(), bg.array() -= rg.array()));
  defop2rg(cmat, cmat, cmat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_cmat, args[0].t.nr, args[0].t.nc}),
    r.array() = a.array() - b.array(),
    (ag.array() += rg.array(), bg.array() -= rg.array()));

  return setInvalidArgs();
}

defusage(compsub, R"(
  T .- T)");


