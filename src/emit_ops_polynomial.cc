#include "./emit.h"
#include "./emit_macros.h"

deftype(sqrt)
{
  return eval(n->ch(0));
}

defemit(sqrt)
{
  defop1g_simd(F, F, r = sqrt(a), ag += 0.5f * rg / sqrt(a));
  defop1g_simd(CF, CF, r = sqrt(a), ag += 0.5f * rg / sqrt(a));
  defop1r(mat, mat, true, allocResult(args[0].t), r = a.array().sqrt());
  return setInvalidArgs();
}

defusage(sqrt, R"(
  sqrt(F|CF|mat))");


deftype(sqr)
{
  return eval(n->ch(0));
}

defemit(sqr)
{
  defop1g_simd(F, F, r = a * a, ag += rg * 2.0f * a);
  defop1g_simd(CF, CF, r = a * a, ag += rg * 2.0f * a);
  defop1r(mat, mat, true, allocResult(args[0].t), r = a.array().pow(2));
  return setInvalidArgs();
}

defusage(sqr, R"(
  sqr(F|CF|mat))");


deftype(cbrt)
{
  auto at = eval(n->ch(0));
  if (at.ist<CF>()) return at;
  return typetag<F>();
}

defemit(cbrt)
{
  defop1g_simd(F, F, r = a.unaryExpr([](F x) { return std::cbrt(x); }), ag += rg/3.0f * a.abs().pow(-2.0f/3.0f) * a.sign());
  return setInvalidArgs();
}

defusage(cbrt, R"(
  cbrt(F) -- negative numbers work, so cbrt(-8) = -2)");


deftype(cube)
{
  auto at = eval(n->ch(0));
  if (at.ist<CF>()) return at;
  return typetag<F>();
}

defemit(cube)
{
  defop1g_simd(F, F, r = a * a * a, ag += rg * 3 * a * a);
  return setInvalidArgs();
}

defusage(cube, R"(
  cube(F))");


deftype(pow)
{
  return eval(n->ch(0));
}

defemit(pow)
{
  if (n->nch() == 2) {
    if (auto constLhs = getConstF(n->ch(0))) {
      if (*constLhs == F(M_E)) {
        // e^something, convert to exp
        evalSpecial(n->ch(1));
        defop1g(F, F, r = exp(a), ag += rg * r);
        defop1g(CF, CF, r = exp(a), ag += rg * r);
        return setInvalidArgs();
      }
    }
    if (auto constRhs = getConstF(n->ch(1))) {
      if (*constRhs == 2.0f) {
        evalSpecial(n->ch(0));
        defop1g_simd(F, F, r = a * a, ag += rg * 2.0f * a);
        defop1g_simd(CF, CF, r = a * a, ag += rg * 2.0f * a);
        defop1r(mat, mat, true, allocResult(args[0].t), r = a.array().pow(2));
        return setInvalidArgs();
      }
      else if (*constRhs == 0.5f) {
        evalSpecial(n->ch(0));
        defop1g_simd(F, F, r = sqrt(a), ag += 0.5f * rg / sqrt(a));
        defop1g_simd(CF, CF, r = sqrt(a), ag += 0.5f * rg / sqrt(a));
        defop1r(mat, mat, true, allocResult(args[0].t), r = a.array().sqrt());
        return setInvalidArgs();
      }
      else if (*constRhs == 3.0f) {
        evalSpecial(n->ch(0));
        defop1g_simd(F, F, r = a * a * a, ag += rg * 3 * a * a);
        return setInvalidArgs();
      }
      else if (*constRhs == 1.0f/3.0f) {
        evalSpecial(n->ch(0));
        defop1g_simd(F, F, r = a.unaryExpr([](F x) { return std::cbrt(x); }), ag += rg/3.0f * a.abs().pow(-2.0f/3.0f) * a.sign());
        return setInvalidArgs();
      }
      else if (*constRhs == 1.0f) {
        evalSpecial(n->ch(0));
        return args[0];
      }
    }
  }
  defop2g_simd(F, F, F, r = a.pow(b), (ag += rg * b * a.pow(b - 1.0f))); // WRITEME: grad for b
  defop2g_simd(CF, F, CF, r = a.pow(b), (ag += rg * b * a.pow(b - 1.0f))); // WRITEME: grad for b
  defop2g_simd(F, CF, CF, r = a.pow(b), (ag += (rg * b * a.pow(b - 1.0f)).real())); // WRITEME: grad for b
  defop2g_simd(CF, CF, CF, r = a.pow(b), (ag += rg * b * a.pow(b - 1.0f))); // WRITEME: grad for b
  return setInvalidArgs();
}

defusage(pow, R"(
  pow(x, exponent ∊ F)
  (x ∊ F) ^ (exponent ∊ F) -- same as pow. Negative numbers work when exponent=1/3
  (x ∊ CF) ^ (exponent ∊ F)
  (x ∊ F) ^ (exponent ∊ CF)
  (x ∊ CF) ^ (exponent ∊ CF))");


deftype(comppow)
{
  auto at = eval(n->ch(0));
  auto bt = eval(n->ch(1));
  if (at == bt) return at;
  return setError();
}

defemit(comppow)
{
  defop2(vec2, vec2, vec2, r = pow(a.array(), b.array()));
  defop2(vec3, vec3, vec3, r = pow(a.array(), b.array()));
  defop2(vec4, vec4, vec4, r = pow(a.array(), b.array()));
  defop2r(mat, mat, mat, args[0].t == args[1].t, allocResult(args[0].t), r = pow(a.array(), b.array()));
  defop2g(vec2, F, vec2, r = a.array().pow(b), (ag.array() += rg.array() * b * a.array().pow(b-1)));
  defop2g(vec3, F, vec3, r = a.array().pow(b), (ag.array() += rg.array() * b * a.array().pow(b-1)));
  defop2g(vec4, F, vec4, r = a.array().pow(b), (ag.array() += rg.array() * b * a.array().pow(b-1)));
  defop2rg(mat, F, mat, true, allocResult(args[0].t), r = a.array().pow(b), (ag.array() += rg.array() * b * a.array().pow(b-1)));
  return setInvalidArgs();
}

defusage(comppow, R"(
  (x ∊ F) .^ (exponent ∊ F)
  (x ∊ vec) .^ (exponent ∊ F)
  (x ∊ mat) .^ (exponent ∊ F)
  (x ∊ vec) .^ (exponent ∊ vec)
  (x ∊ mat) .^ (exponent ∊ mat))");



deftype(polybasis)
{
  return n->data<TypeTag>();
}

void setPolyBasis(vec &ret, float x)
{
  ret[0] = 1.0f;
  F xPow = 1.0f;
  for (int i = 1; i < ret.rows(); i++) {
    xPow *= x;
    ret[i] = xPow;
  }
}

defemit(polybasis)
{
  auto wantType = n->data<TypeTag>();
  if (argmatch1(F)) {
    if (wantType.kind == TK_mat) {
      auto rv = allocResult(wantType);
      emit_evalop1(F, vec, setPolyBasis(r, a));
      return rv;
    }
  }

  return setInvalidArgs();
}

defusage(polybasis, R"(
  vecN.polybasis(x ∊ F) -- returns [1, x, x^2, x^3, ...])");
