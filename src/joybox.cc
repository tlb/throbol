#include "./core.h"
#include "../geom/gl_headers.h"
#include "./main_window.h"
#include "./uipoll.h"
#include "./joybox.h"
#if !defined(EMSCRIPTEN_NOTYET)
#include "hidapi.h"
#endif

shared_ptr<Joybox> Joybox::primary;

void Joybox::report()
{
#if !defined(EMSCRIPTEN_NOTYET)
  auto devs = hid_enumerate(0, 0);
  for (auto it = devs; it; it = it->next) {
    fprintf(stderr, "HID device type: %04hx %04hx\n  path: %s\n  serial_number: %ls\n", it->vendor_id, it->product_id, it->path, it->serial_number);
    fprintf(stderr, "  Manufacturer: %ls\n", it->manufacturer_string);
    fprintf(stderr, "  Product:      %ls\n", it->product_string);
    fprintf(stderr, "  Release:      %hx\n", it->release_number);
    fprintf(stderr, "  Interface:    %d\n",  it->interface_number);
  }
  hid_free_enumeration(devs);
#endif
}

void Joybox::setup()
{
#if !defined(EMSCRIPTEN_NOTYET)
  if (primary) return;
	if (auto dev = hid_open(0x4d8, 0xfdcf, NULL)) {
	  hid_set_nonblocking(dev, 1);
    primary = make_shared<Joybox>(dev, Joybox_TangentWave2);
    startUiPoll(primary);
  }
#endif
}

Joybox::Joybox(hid_device_ *_dev, JoyboxType _type)
  : dev(_dev),
    type(_type)
{
  switch (type) {
    case Joybox_TangentWave2:
      buttons.resize(JBTW_total);
      fill(buttons.begin(), buttons.end(), -1.0f);
      knobs.resize(13);
      balls.resize(6);
      break;

    // ADD joybox type
  }
}

Joybox::~Joybox()
{
}

UiPollResult Joybox::uiPoll()
{
#if !defined(EMSCRIPTEN_NOTYET)

  if (!dev) return UiPoll_closeme;

  int verbose = 0;
  U8 buf[128];
  F dt = 1.0/60.0; // FIXME
  fill(knobs.begin(), knobs.end(), 0);
  fill(balls.begin(), balls.end(), 0);
  while (dev) {
		auto nr = hid_read(dev, buf, sizeof(buf));
    if (nr < 0) {
      L() << "hid_read error\n";
      hid_close(dev);
      dev = nullptr;
      return UiPoll_closeme;
    }
    if (nr == 0) {
      return UiPoll_invisible;
    }
    if (verbose >= 2) {
      L() << "hid";
      for (auto i = 0; i < nr; i++) {
        L() << " " << repr_02x(buf[i]);
      }
      L() ;
    }
    uiPollLive = true;
    switch (type) {

      case Joybox_TangentWave2:
        if (nr == 26) {
          for (auto i = 0; i < JBTW_total; i++) {
            auto but = (buf[i/8] >> (i%8))&1;
            if (but) {
              if (buttons[i] < 0.0f) {
                if (verbose >= 1) L() << "but[" << i << "] = " << but;
                buttons[i] = 0.0f;
              }
              else {
                buttons[i] += dt;
              }
            }
            else {
              if (buttons[i] >= 0.0f) {
                if (verbose >= 1) L() << "but[" << i << "] = " << but;
                buttons[i] = -1.0f;
              }
            }
          }
          for (auto i = 0; i < 13; i++) {
            auto knob = (S32)(S8)buf[6 + i];
            if (knob) {
              if (verbose >= 1) L() << "knob[" << i << "] = " << knob;
              knobs[i] += knob;
            }
          }
          for (auto i = 0; i < 6; i++) {
            auto ball = (S32)(S8)buf[19 + i];
            if (ball) {
              if (verbose >= 1) L() << "ball[" << i << "] = " << ball;
              balls[i] += ball;
            }
          }
        }
        else {
          L() << "hid_read returned " << nr << ", expected 26\n";
          return UiPoll_invisible;
        }
        break;
    }
  }
  return UiPoll_invisible;
#else
  return UiPoll_closeme;
#endif
}

void Joybox::uiRedraw()
{
}
