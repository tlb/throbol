#include "./test_utils.h"
#include "../geom/geom.h"

TEST_CASE("repr(float) works", "[env]") {
  CHECK(repr(42.5f) == "42.5");
  CHECK(repr(1.0f/3.0f) == "0.3333333");
  CHECK(repr(10000.0f + 1.0f/3.0f) == "10000.33");
  CHECK(repr(1.5e-9) == "1.5e-09");
  CHECK(repr(1.5e12) == "1500000000000");
  CHECK(repr(1.5e30) == "1.5e+30");
}



TEST_CASE("deque blocking", "[env]") {
  /*
    We expect deques to use reasonable-sized allocation chunks.
  */

  deque<pair<U8 *, size_t>> chunks;
  for (size_t i = 0; i < 5000; i++) {
    chunks.emplace_back((U8 *)i, i);
  }
  size_t nshort = 0, nlong = 0;
  size_t last = 0;
  for (auto &it : chunks) {
    auto cur = size_t(&it);
    if (cur - last > 16 || cur < last) {
      nlong++;
    } else {
      nshort++;
    }
    last = size_t(&it);
  }
  if (0) L() << "nlong=" << nlong << " nshort=" << nshort;
  CHECK(nlong > 0);
  CHECK(nlong < 5000/32 + 20);

}

TEST_CASE("Data size", "[env]") {
#define showsize(X) L() << #X << \
  " size=" << sizeof(X) << \
  " align=" << alignof(X)

#define showsize_simd(X) L() << #X << \
  " size=" << sizeof(X) << \
  " align=" << alignof(X) << \
  " x4=" << sizeof(Eigen::Matrix<X, 4, 1>)

  showsize_simd(F);
  showsize_simd(CF);
  showsize_simd(bool);
  showsize(vec2);
  showsize(vec3);
  showsize(vec4);
  showsize(mat2);
  showsize(mat3);
  showsize(mat4);
  showsize(MatrixXf);
  showsize(Symbol);
  showsize(SymbolSet);
  showsize(Beh);
  showsize(AudioFrame);
  showsize(VideoFrame);
  showsize(Check);

  showsize(TypeTag);
  L() << "  unsigned kind:" << TypeTag::KIND_WIDTH << ", nr:" << TypeTag::NR_WIDTH << ", nc:" << TypeTag::NC_WIDTH;
  showsize(Val);
  showsize(AstNode);
  showsize(FormulaSubstr);
  showsize(Annotation);

#undef showsize

  // These really should be this size on any reasonable compiler
  CHECK(sizeof(TypeTag) == 4);
  CHECK(sizeof(Val) == 8);
}

TEST_CASE("Eigen speed", "[.][env][perf]") {
  mat4 result;
  perfReport("Eigen speed 1000x mat4 * mat4", [&result]() {
    mat4 foo = mat4::Identity();
    float theta = 1.0;
    mat4 bar;
    bar << 
      +cos(theta), +sin(theta), 0, 0,
      -sin(theta), +cos(theta), 0, 0,
      0, 0, 1, 0,
      0, 0, 0, 1;
    for (auto i = 0; i < 1000; i++) {
       foo = foo * bar;
    }
    result = foo;
  });
  if (1) L() << result;

  perfReport("Eigen speed 1000x mkrot & mul", [&result]() {
    mat4 foo = mat4::Identity();
    float theta = 0.0;
    for (auto i = 0; i < 1000; i++) {
      mat4 bar;
      theta += 0.0001f;
      bar <<
        +cos(theta), +sin(theta), 0, 0,
        -sin(theta), +cos(theta), 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1;
      foo = foo * bar;
    }
    result = foo;
  });
  if (1) L() << result;  
}

TEST_CASE("intersectRayTriangle", "[env]") {
  vec2 bary;
  F distance;
  CHECK(intersectRayTriangle(
    vec3(0.5, 0.5, 10),
    vec3(0, 0, -1),
    vec3(0, 0, 0),
    vec3(1, 0, 0),
    vec3(0, 1, 0),
    bary, distance));
  CHECK(!intersectRayTriangle(
    vec3(0.5, 0.5, 10),
    vec3(0, 0, +1),
    vec3(0, 0, 0),
    vec3(1, 0, 0),
    vec3(0, 1, 0),
    bary, distance));
}

/*
  Just checking my understanding of how decltype works with references
*/
static int &foo1() { static int x = 5; return x; }
static int foo2() { static int x = 5; return x; }

TEST_CASE("c++ auto", "[env]") {
  decltype(auto) a = foo1();
  auto d = foo1();
  decltype(auto) b = foo2();
  decltype(auto) c = foo1();

  CHECK(a == 5);
  CHECK(b == 5);
  CHECK(c == 5);
  a = 2;
  CHECK(a == 2);
  CHECK(c == 2);
  CHECK(d == 5);

  b = 2;
  CHECK(b == 2);
  b = foo2();
  CHECK(b == 5);

}

TEST_CASE("yaml", "[yaml]") {
  auto doc = YAML::Load(R"(
cis: [[1, 2, 3, 0],  [0, 1, 0, 0],  [0, 0, 1, 0],  [0, 0, 0, 1]]
  )");

  auto m = doc["cis"].as<mat4>();
  CHECK(m(0, 0) == 1);
  CHECK(m(1, 0) == 2);
  CHECK(m(2, 0) == 3);
  CHECK(m(1, 1) == 1);
  CHECK(m(2, 2) == 1);
  CHECK(m(3, 3) == 1);
  doc["trans"] = mat4(m.transpose());

  YAML::Emitter out;
  out << YAML::Flow;
  out << doc["trans"];
  string docs(out.c_str());
  CHECK(docs == "[[1, 0, 0, 0], [2, 1, 0, 0], [3, 0, 1, 0], [0, 0, 0, 1]]");


}


TEST_CASE("yaml-add1", "[yaml]")
{
  YAML::Node all;
  YAML::Node cur;

  cur = YAML::Node();
  all.push_back(cur);
  cur["foo"] = "1";

  cur.reset(); // cur = YAML::Node() doesn't do anything!
  all.push_back(cur);
  cur["bar"] = "2";

  ostringstream s;
  s << all << "\n";
  CHECK(s.str() == "- foo: 1\n- bar: 2\n");
}

template<typename T>
ostream & operator << (ostream &s, list<T> const &a)
{
  s << "[";
  char const *sep = "";
  for (auto it : a) {
    s << sep << it;
    sep = ", ";
  }
  s << "]";
  return s;
}

TEST_CASE("chunky", "[chunky]")
{
  {
    ChunkyAlloc c1;
    c1(500);
    c1(500);
    ChunkyAlloc c2(std::move(c1));
  }
}


TEST_CASE("cbrt", "[cbrt]")
{
  CHECK(cbrt(-8.0) == -2.0);
}


int dummy_ctr;

TEST_CASE("Function capture penalty", "[.][env][perf]")
{
  perfReport("Lambda speed fixed cap", []() {
    vector<function<void()>> fs;

    int a = 17, b = 32;
    for (int i = 0; i < 1000; i++) {
      fs.push_back([a=a, b=b]() {
        dummy_ctr += a;
        dummy_ctr += b;
      });
    }

    for (int j = 0; j < 1000; j++) {
      for (auto f : fs) f();
    }
  });

  perfReport("Lambda speed with =", []() {
    vector<function<void()>> fs;

    int a = 17, b = 32;
    for (int i = 0; i < 1000; i++) {
      fs.push_back([=, a=a, b=b]() {
        dummy_ctr += a;
        dummy_ctr += b;
      });
    }

    for (int j = 0; j < 1000; j++) {
      for (auto f : fs) f();
    }
  });
  
}


TEST_CASE("vector<Blob>", "[mem][new]")
{
  AllocTrackingMixin<Blob>::tracker.clearCounters();
  {
    vector<Blob> foos(23);
    CHECK(AllocTrackingMixin<Blob>::tracker.nAlloc == 23);
    foos.resize(35);
    CHECK(AllocTrackingMixin<Blob>::tracker.nAlloc == 35);
  }
  CHECK(AllocTrackingMixin<Blob>::tracker.nAlloc == 0);
  // This tests whether std::vector uses the move constructor when available
  CHECK(AllocTrackingMixin<Blob>::tracker.nMoveConstructed == 23);
}