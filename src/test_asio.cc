
#include "./test_utils.h"
#include "./vbmainloop.h"


TEST_CASE("asio is reliable with threads", "[env][asio]")
{
  io_context.restart();

  int counter = 0;

  for (int threadi = 0; threadi < 10; threadi++) {
    std::thread([&counter]() {
      
      for (int i = 0; i < 1000; i++) {
        asio::post(io_context, [&counter]() {
          counter++;
        });

      }
    }).detach();
  }

  asio::steady_timer timer(io_context);
  timer.expires_after(std::chrono::milliseconds(100));
  timer.wait();

  io_context.run();

  CHECK(counter == 10000);

}
