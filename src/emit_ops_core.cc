#include "./emit.h"
#include "./emit_macros.h"

deftype(formula)
{
  if (n->nch() == 0) return T_undef;
  for (int i = 0; i + 1 < n->nch(); i++) {
    /*
      Yes, it's necessary to eval the args (semicolon-separated statements)
      even though we only care about the type of the last, because early
      ones may bind variables that determine the type of the last.
      See deftype(assign) and deftype(name) for the information flow.
    */
    eval(n->ch(i));
  }
  auto rt = eval(n->ch(n->nch() - 1));
  if (rt.ist<bool>()) {
    return typetag<F>();
  }
  return rt;
}

defemit(formula)
{
  // progn semantics
  if (evalArgs() > 0) {
    auto rv = args.back();
    if (rv.t.template ist<bool>()) {
      // convert internal bool to external F
      auto newrv = allocPad<F>();
      she.evalop([newrv1=newrv, rv1=rv](CpuState &cpu, Pad &pad, auto batchdef) {
        auto newrv = batchdef.getVal(newrv1);
        auto rv = batchdef.getVal(rv1);
        decltype(auto) newr = pad.wr_F_simd(newrv, batchdef);
        decltype(auto) r = pad.rd_bool_simd(rv, batchdef);
        newr = r.template cast<F>();
      });
      return newrv;
    }
    return rv;
  }
  return ValSuite(T_undef);
}

defusage(formula, R"(
  formula)");


deftype(number)
{
  return typetag<F>();
}

defemit(number)
{
  debugLoc = FormulaSubstr();
  auto imm = n->data<F>();
  defopn_simd(F, r = imm);
  return setError("Invalid number");
}

defusage(number, R"(
  number)");


deftype(constant)
{
  return typetag<F>();
}

defemit(constant)
{
  auto imm = n->data<F>();
  defopn_simd(F, r = imm);
  return setError("Invalid constant");
}

defusage(constant, R"(
  value)");


deftype(param)
{
  return typetag<F>();
}

defemit(param)
{
  ce.hasParam = true;
  auto paramIndex = n->data<ParamIndex>();
  defopn_simd(F, r = cpu.rdparam_simd<batchdef.compiledBatchSize()>(paramIndex));
  return setError("Invalid param");
}

defusage(param, R"(
  value~range)");

deftype(string)
{
  return typetag<string_view>();
}

defemit(string)
{
  auto imm = n->data_sv();
  defopn(string_view, r = imm);
  return setError("Invalid string");
}

defusage(string, R"(
  value -- like "foo\\nbar")");


deftype(symbol)
{
  return typetag<SymbolSet>();
}

defemit(symbol)
{
  auto imm = n->data<Symbol>();
  defopn(SymbolSet, r = SymbolSet(imm));
  return setError("Invalid symbol");
}

defusage(symbol, R"(
  value -- like 'foo)");


deftype(typecast)
{
  return n->data<TypeTag>();
}

defemit(typecast)
{
  auto wantType = n->data<TypeTag>();

  if (matchArgs({wantType})) {
    return args[0];
  }

  if (wantType.ist<DrawOp>()) {
    defop0(DrawOpHandle, r = pad.pool->mk<DrawOpNull>());
  }

  if (wantType.ist<VideoFrame>()) {
    defop0(VideoFrame, r = VideoFrame());
  }

  if (wantType.ist<vec2>()) {
    // Optimization for all constants
    if (auto af = getConstF(n->ch(0)), bf = getConstF(n->ch(1)); af && bf && !n->ch(2)) {
      auto rv = allocResult<vec2>();
      she.evalop([rv1=rv, af=*af, bf=*bf](CpuState &cpu, Pad &pad, auto batchdef) {
        auto rv = batchdef.getVal(rv1);
        for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
          pad.wr_vec2(rv, batchi) = {af, bf};
        }
      });
      return rv;
    }

    defop2(F, F, vec2, r = vec2(a, b))
    defop0(vec2, r = vec2::Zero());
  }

  if (wantType.ist<vec3>()) {
    if (auto af = getConstF(n->ch(0)), bf = getConstF(n->ch(1)), cf = getConstF(n->ch(2)); af && bf && cf && !n->ch(3)) {
      auto rv = allocResult<vec3>();
      she.evalop([rv1=rv, af=*af, bf=*bf, cf=*cf](CpuState &cpu, Pad &pad, auto batchdef) {
        auto rv = batchdef.getVal(rv1);
        for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
          pad.wr_vec3(rv, batchi) = {af, bf, cf};
        }
      });
      return rv;
    }

    defop3(F, F, F, vec3, (r = {a, b, c}));
    defop0(vec3, r = vec3::Zero());
  }

  if (wantType.ist<vec4>()) {
    if (auto af = getConstF(n->ch(0)), bf = getConstF(n->ch(1)), cf = getConstF(n->ch(2)), df = getConstF(n->ch(3)); af && bf && cf && df && !n->ch(4)) {
      auto rv = allocResult<vec4>();
      she.evalop([rv1=rv, af=*af, bf=*bf, cf=*cf, df=*df](CpuState &cpu, Pad &pad, auto batchdef) {
        auto rv = batchdef.getVal(rv1);
        for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
          pad.wr_vec4(rv, batchi) << af, bf, cf, df;
        }
      });
      return rv;
    }

    defop4(F, F, F, F, vec4, (r = {a, b, c, d}));
    defop0(vec4, r = vec4::Zero());
  }

  if (wantType.ist<mat2>()) {
    if (auto af = getConstF(n->ch(0)), bf = getConstF(n->ch(1)), cf = getConstF(n->ch(2)), df = getConstF(n->ch(3)); af && bf && cf && df && !n->ch(4)) {
      auto rv = allocResult<mat2>();
      she.evalop([rv1=rv, af=*af, bf=*bf, cf=*cf, df=*df](CpuState &cpu, Pad &pad, auto batchdef) {
        auto rv = batchdef.getVal(rv1);
        for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
          pad.wr_mat2(rv, batchi) = mat2{{af, cf}, {bf, df}};
        }
      });
      return rv;
    }
    defop4(F, F, F, F, mat2, (r = mat2{{a, c}, {b, d}}));
    defop2(vec2, vec2, mat2, (r.col(0) = a, r.col(1) = b));
    defop1(CF, mat2, (r = mat2{{real(a), -imag(a)}, {imag(a), real(a)}}));
    defop0(mat2, r = mat2::Zero());
  }

  if (wantType.ist<mat3>()) {
    defop3(vec3, vec3, vec3, mat3, (r.col(0) = a, r.col(1) = b, r.col(2) = c));
    defop0(mat3, r = mat3::Zero());
  }

  if (wantType.ist<mat4>()) {
    defop4(vec4, vec4, vec4, vec4, mat4, (r.col(0) = a, r.col(1) = b, r.col(2) = c, r.col(3) = d));
    defop0(mat4, r = mat4::Zero());
  }

  if (wantType.ist<mat>()) {
    if (argmatch0()) {
      auto rv = allocResult(wantType);
      emit_evalop0(vec, setZero(r))
      return rv;
    }
    defop1r(mat, mat, 
      true,
      allocResult(wantType),
      r = a.reshaped(wantType.nr, wantType.nc));
  }

  return setInvalidArgs();
}


string AstHelp::help_typecast(AstNode *n)
{

  auto wantType = n ? n->data<TypeTag>() : TypeTag(TK_undef);

  if (wantType.ist<DrawOp>()) return R"(
    drawop() -- nothing
    drawop(drawop) -- passthru)";
  
  if (wantType.ist<VideoFrame>()) return R"(
    videoframe() -- empty frame
    videoframe(videoframe) -- passthru)";

  if (wantType.ist<vec2>()) return R"(
    vec2(F, F)
    vec2() -- all zeros
    vec3(vec2) -- passthru)";

  if (wantType.ist<vec3>()) return R"(
    vec3(F, F, F)
    vec3() -- all zeros
    vec3(vec3) -- passthru)";

  if (wantType.ist<vec4>()) return R"(
    vec4(F, F, F, F)
    vec4() -- all zeros
    vec4(vec4) -- passthru)";

  if (wantType.ist<mat2>()) return R"(
    mat2(F, F, F, F) -- column major
    mat2(vec2, vec2) -- column major
    mat2(CF) -- corresponding rotation & scale matrix
    mat2(mat2) -- passthru)";

  if (wantType.ist<mat3>()) return R"(
    mat3(vec3, vec3, vec3)
    mat3() -- all zeros
    mat3(mat3) -- passthru)";

  if (wantType.ist<mat4>()) return R"(
    mat4(vec4, vec4, vec4, vec4) -- column major
    mat4() -- all zeros
    mat4(mat4) -- passthru)";

  if (wantType.ist<mat>()) {
    auto tn = typeName(wantType);
    return "\n"
    "  " + tn + "() -- all zeros\n" +
    "  " + tn + "(" + tn + ") -- passthru";
  }

  return R"(
    typecast(othertype)";
}


deftype(assign)
{
  auto lhs = n->ch(0);
  auto bt = eval(n->ch(1));
  if (lhs->t == A_name) {
    locals[lhs->data_sv()] = bt;
  }
  return bt;
}

defemit(assign)
{
  if (n->nch() == 2) {
    auto lhs = n->ch(0);
    auto rhs = n->ch(1);
    if (lhs->t != A_name) {
      return setError("Not a name", lhs);
    }
    auto rhsv = NodeEmitter(*this, rhs).emit();
    ce.locals[lhs->data_sv()] = rhsv;
    return rhsv;
  }
  return setInvalidArgs();
}

defusage(assign, R"(
  A = B)");


deftype(name)
{
  return locals[n->data_sv()];
}

defemit(name)
{
  auto ret = ce.locals[n->data_sv()];
  if (ret.valid()) {
    return ret;
  }

  return setError("Undefined", n);
}

string AstHelp::help_name(AstNode *n)
{
  if (n && n->t == A_name) {
    auto help = AstHelp::helpByName(n->data_sv());
    if (help) return *help;
  }
  return "";
}


deftype(cellref)
{
  // A reference to the local cell is implicitly a float. To make eg `foo = foo + dt` work without
  // a type hint
  if (n->data<CellIndex>() == cell) {
    return typetag<F>();
  }
  else {
    return she.doCell(n->data<CellIndex>(), hypoCtx, true).t;
  }
}

defemit(cellref)
{
  ce.hasRef = true;
  // Also called for A_hypobinding
  BoxedRef srcRef = ce.resolveName(n, false);
  if (!srcRef.valid()) return setError("", n);
  switch (srcRef.t.kind) {

    case TK_undef:
      return ValSuite();

    case TK_error:
      return ValSuite(T_error);

    case TK_F:
      defopng_simd(F, r = cpu.rdcur_F_simd(srcRef, batchdef), cpu.incrgrad_F_simd(srcRef, rg, batchdef));
      break;

    case TK_CF:
      defopng_simd(CF, r = cpu.rdcur_CF_simd(srcRef, batchdef), cpu.incrgrad_CF_simd(srcRef, rg, batchdef));
      break;

    case TK_bool:
      return setError("bool cellref?");

    case TK_mat:
      if (srcRef.ist<vec2>()) {
        defopng(vec2, r = cpu.curs[batchi]->rd_vec2(srcRef), cpu.grads[batchi]->up_vec2(srcRef) += rg);
      }
      else if (srcRef.ist<vec3>()) {
        defopng(vec3, r = cpu.curs[batchi]->rd_vec3(srcRef), cpu.grads[batchi]->up_vec3(srcRef) += rg);
      }
      else if (srcRef.ist<vec4>()) {
        defopng(vec4, r = cpu.curs[batchi]->rd_vec4(srcRef), cpu.grads[batchi]->up_vec4(srcRef) += rg);
      }
      else if (srcRef.ist<mat2>()) {
        defopng(mat2, r = cpu.curs[batchi]->rd_mat2(srcRef), cpu.grads[batchi]->up_mat2(srcRef) += rg);
      }
      else if (srcRef.ist<mat3>()) {
        defopng(mat3, r = cpu.curs[batchi]->rd_mat3(srcRef), cpu.grads[batchi]->up_mat3(srcRef) += rg);
      }
      else if (srcRef.ist<mat4>()) {
        defopng(mat4, r = cpu.curs[batchi]->rd_mat4(srcRef), cpu.grads[batchi]->up_mat4(srcRef) += rg);
      }
      else {
        auto rv = allocResult(srcRef.t);
        she.evalop([rv1=rv, srcRef=srcRef](CpuState &cpu, Pad &pad, auto batchdef) {
          auto rv = batchdef.getVal(rv1);
          for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
            decltype(auto) r = pad.wr_mat(rv, batchi);
            if (batchi < cpu.realBatchSize) {
              r = cpu.curs[batchi]->rd_mat(srcRef);
            }
            else {
              setZero(r);
            }
          }
        });
        she.gradop([rv1=rv, srcRef=srcRef](CpuState &cpu, Pad &pad, Pad &padg, auto batchdef) {
          auto rv = batchdef.getVal(rv1);
          for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
            [[maybe_unused]] mat const &r = pad.wr_mat(rv, batchi);
            if (batchi < cpu.realBatchSize) {
              mat const &rg = padg.rd_mat(rv, batchi);
              cpu.grads[batchi]->up_mat(srcRef) += rg;
            }
          }
        });
        return rv;
      }
      break;

    case TK_cmat:
      if (1) {
        auto rv = allocResult(srcRef.t);
        she.evalop([rv1=rv, srcRef=srcRef](CpuState &cpu, Pad &pad, auto batchdef) {
          auto rv = batchdef.getVal(rv1);
          for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
            decltype(auto) r = pad.wr_cmat(rv, batchi);
            if (batchi < cpu.realBatchSize) {
              r = cpu.curs[batchi]->rd_cmat(srcRef);
            }
            else {
              setZero(r);
            }
          }
        });
        she.gradop([rv1=rv, srcRef=srcRef](CpuState &cpu, Pad &pad, Pad &padg, auto batchdef) {
          auto rv = batchdef.getVal(rv1);
          for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
            [[maybe_unused]] cmat const &r = pad.wr_cmat(rv, batchi);
            decltype(auto) rg = padg.rd_cmat(rv, batchi);
            if (batchi < cpu.realBatchSize && cpu.grads[batchi]) {
              cpu.grads[batchi]->rd_cmat(srcRef) += rg;
            }
          }
        });
        return rv;
      }
      break;
      // WRITEME
      break;

    case TK_str:
      defopn(string_view, r = cpu.curs[batchi]->rd_string_view(srcRef));
      break;

    case TK_DrawOp:
      defopn(DrawOpHandle, r = cpu.curs[batchi]->rd_DrawOpHandle(srcRef));
      break;

    case TK_Check:
      defopn(Checkp, r = cpu.curs[batchi]->rd_Checkp(srcRef));
      break;

    case TK_SymbolSet:
      defopn(SymbolSet, r = cpu.curs[batchi]->rd_SymbolSet(srcRef));
      break;

    case TK_Beh:
      defopn(Beh, r = cpu.curs[batchi]->rd_Beh(srcRef));
      break;

    case TK_VideoFrame:
      defopn(VideoFrame, r = cpu.curs[batchi]->rd_VideoFrame(srcRef));
      break;

    case TK_Assoc:
      defopn(Assocp, r = cpu.curs[batchi]->rd_Assocp(srcRef));
      break;

    case TK_Api:
      defopn(ApiHandle, r = cpu.curs[batchi]->rd_ApiHandle(srcRef));
      break;
    // ADD type
  }
  return setError("Unknown type");
}

defusage(cellref, "");


deftype(hypobinding)
{
  // guaranteed to be same type as underling cellref
  return eval(n->ch(0));
}

defemit(hypobinding)
{
  return emit_cellref();
}

defusage(hypobinding, R"(
  cellname)");


deftype(index)
{
  auto at = eval(n->ch(0));
  if (at.isError()) return typetag<F>(); // TODO: Think through
  return at;
}

defemit(index)
{
  // in lhs[index1], we have to avoid evaluating lhs unless index1 == 0
  if (n->nch() == 2) {
    if (auto lhs = n->ch(0)) {
      if (auto index1 = n->ch(1)) {
        debugLoc.begin = lhs->loc.begin;
        debugLoc.end = index1->loc.end + 1;
        if (auto dtm = getDtMultiple(index1)) {
          if (get<0>(*dtm) == 0.0f && get<1>(*dtm) == 0.0f) {
            return NodeEmitter(*this, lhs).emit();
          }
          if (get<0>(*dtm) == 0.0f && get<1>(*dtm) == -1.0f) {
            // foo[-dt] means the previous value of foo.
            // WRITEME: More generality
            BoxedRef srcRef = ce.resolveName(lhs, true);
            if (srcRef.valid()) {
              ce.hasPrev = true;
              switch (srcRef.t.kind) {
                case TK_undef:
                  return ValSuite();
                case TK_error:
                  return ValSuite(T_error);
                case TK_F:
                  defopn_simd(F, r = cpu.rdprev_F_simd(srcRef, batchdef));
                  break;
                case TK_CF:
                  defopn_simd(CF, r = cpu.rdprev_CF_simd(srcRef, batchdef));
                  break;
                case TK_bool:
                  return setError("bool cellref?");

                case TK_mat:
                  if (srcRef.ist<vec2>()) {
                    defopn(vec2, r = cpu.prevs[batchi] ? cpu.prevs[batchi]->rd_vec2(srcRef) : vec2::Zero());
                  }
                  else if (srcRef.ist<vec3>()) {
                    defopn(vec3, r = cpu.prevs[batchi] ? cpu.prevs[batchi]->rd_vec3(srcRef) : vec3::Zero());
                  }
                  else if (srcRef.ist<vec4>()) {
                    defopn(vec4, r = cpu.prevs[batchi] ? cpu.prevs[batchi]->rd_vec4(srcRef) : vec4::Zero());
                  }
                  else if (srcRef.ist<mat2>()) {
                    defopn(mat2, r = cpu.prevs[batchi] ? cpu.prevs[batchi]->rd_mat2(srcRef) : mat2::Zero());
                  }
                  else if (srcRef.ist<mat3>()) {
                    defopn(mat3, r = cpu.prevs[batchi] ? cpu.prevs[batchi]->rd_mat3(srcRef) : mat3::Zero());
                  }
                  else if (srcRef.ist<mat4>()) {
                    defopn(mat4, r = cpu.prevs[batchi] ? cpu.prevs[batchi]->rd_mat4(srcRef) : mat4::Zero());
                  }
                  else {
                    defopnr(mat, true, allocResult(srcRef.t),
                      if (cpu.prevs[batchi]) { r = cpu.prevs[batchi]->rd_mat(srcRef); } else { setZero(r); });
                  }
                  break;

                case TK_cmat:
                  defopnr(cmat, true, allocResult(srcRef.t),
                    if (cpu.prevs[batchi]) { r = cpu.prevs[batchi]->rd_cmat(srcRef); } else { setZero(r); });
                  break;

                case TK_str:
                  defopn(string_view, r = cpu.prevs[batchi] ? cpu.prevs[batchi]->rd_string_view(srcRef) : string_view("", 0));
                  break;
                case TK_DrawOp:
                  defopn(DrawOpHandle, r = cpu.prevs[batchi] ? cpu.prevs[batchi]->rd_DrawOpHandle(srcRef) : nullptr);
                  break;
                case TK_Check:
                  defopn(Checkp, r = cpu.prevs[batchi] ? cpu.prevs[batchi]->rd_Checkp(srcRef) : nullptr);
                  break;
                case TK_SymbolSet:
                  defopn(SymbolSet, r = cpu.prevs[batchi] ? cpu.prevs[batchi]->rd_SymbolSet(srcRef) : SymbolSet());
                  break;
                case TK_Beh:
                  defopn(Beh, r = cpu.prevs[batchi] ? cpu.prevs[batchi]->rd_Beh(srcRef) : Beh());
                  break;
                case TK_VideoFrame:
                  defopn(VideoFrame, r = cpu.prevs[batchi] ? cpu.prevs[batchi]->rd_VideoFrame(srcRef) : VideoFrame());
                  break;
                case TK_Assoc:
                  defopn(Assocp, r = cpu.prevs[batchi] ? cpu.prevs[batchi]->rd_Assocp(srcRef) : nullptr);
                  break;
                case TK_Api:
                  defopn(ApiHandle, r = cpu.prevs[batchi] ? cpu.prevs[batchi]->rd_ApiHandle(srcRef) : ApiHandle());
                  break;
                // ADD type
              }
              return setError("Unknown type for index");
            }
            else {
              // Avoid evaluating the index argument.
              evalSpecial(lhs);

              defdelay1(F, mkValue(args[0].t), F, F, (r = sprev, snew = a));
              defdelay1(CF, mkValue(args[0].t), CF, CF, (r = sprev, snew = a));
              defdelay1(vec2, mkValue(args[0].t), vec2, vec2, (r = sprev, snew = a));
              defdelay1(vec3, mkValue(args[0].t), vec3, vec3, (r = sprev, snew = a));
              defdelay1(vec4, mkValue(args[0].t), vec4, vec4, (r = sprev, snew = a));
              defdelay1(mat2, mkValue(args[0].t), mat2, mat2, (r = sprev, snew = a));
              defdelay1(mat3, mkValue(args[0].t), mat3, mat3, (r = sprev, snew = a));
              defdelay1(mat4, mkValue(args[0].t), mat4, mat4, (r = sprev, snew = a));
              defdelay1(Beh, mkValue(args[0].t), Beh, Beh, (r = sprev, snew = a));
              defdelay1(mat, mkValue(args[0].t), mat, mat, (r = sprev, snew = a));
              defdelay1(cmat, mkValue(args[0].t), cmat, cmat, (r = sprev, snew = a));
              return setError("Unsupported time index");
            }
          }
        }


        //if (args[0].t != typetag<F>()) {
        //  return setError("Index must be scalar");
        //}

        BoxedRef srcRef = ce.resolveName(lhs, true);
        if (srcRef.valid()) {
          // Avoid evaluating the lhs argument.
          evalSpecial(index1);
          ce.hasPrev = true;
          auto cell = ce.cell; // captured below for valueAtRelTIme
          switch (srcRef.t.kind) {
            case TK_undef:
              return ValSuite();
            case TK_error:
              return ValSuite(T_error);
            case TK_F:
              defop1(F, F, r = cpu.curs[batchi] ? cpu.valueAtRelTime(a, batchi, cell)->rd_F(srcRef) : 0.0f);
              break;
            case TK_CF:
              defop1(F, CF, r = cpu.curs[batchi] ? cpu.valueAtRelTime(a, batchi, cell)->rd_CF(srcRef) : CF(0, 0));
              break;
            case TK_bool:
              return setError("bool cellref?");
            case TK_mat:
              if (srcRef.ist<vec2>()) {
                defop1(F, vec2, r = cpu.curs[batchi] ? cpu.valueAtRelTime(a, batchi, cell)->rd_vec2(srcRef) : vec2::Zero());
              }
              else if (srcRef.ist<vec3>()) {
                defop1(F, vec3, r = cpu.curs[batchi] ? cpu.valueAtRelTime(a, batchi, cell)->rd_vec3(srcRef) : vec3::Zero());
              }
              else if (srcRef.ist<vec4>()) {
                defop1(F, vec4, r = cpu.curs[batchi] ? cpu.valueAtRelTime(a, batchi, cell)->rd_vec4(srcRef) : vec4::Zero());
              }
              else if (srcRef.ist<mat2>()) {
                defop1(F, mat2, r = cpu.curs[batchi] ? cpu.valueAtRelTime(a, batchi, cell)->rd_mat2(srcRef) : mat2::Zero());
              }
              else if (srcRef.ist<mat3>()) {
                defop1(F, mat3, r = cpu.curs[batchi] ? cpu.valueAtRelTime(a, batchi, cell)->rd_mat3(srcRef) : mat3::Zero());
              }
              else if (srcRef.ist<mat4>()) {
                defop1(F, mat4, r = cpu.curs[batchi] ? cpu.valueAtRelTime(a, batchi, cell)->rd_mat4(srcRef) : mat4::Zero());
              }
              else {
                defop1r(F, mat, true, allocResult(srcRef.t), 
                    if (cpu.curs[batchi]) { r = cpu.valueAtRelTime(a, batchi, cell)->rd_mat(srcRef); } else { setZero(r); })
              }
              break;
            case TK_cmat:
              defop1r(F, cmat, true, allocResult(srcRef.t), 
                  if (cpu.curs[batchi]) { r = cpu.valueAtRelTime(a, batchi, cell)->rd_cmat(srcRef); } else { setZero(r); })
              break;
            case TK_str:
              defop1(F, string_view, r = cpu.curs[batchi] ? cpu.valueAtRelTime(a, batchi, cell)->rd_string_view(srcRef) : string_view("", 0));
              break;
            case TK_DrawOp:
              defop1(F, DrawOpHandle, r = cpu.curs[batchi] ? cpu.valueAtRelTime(a, batchi, cell)->rd_DrawOpHandle(srcRef) : nullptr);
              break;
            case TK_Check:
              defop1(F, Checkp, r = cpu.curs[batchi] ? cpu.valueAtRelTime(a, batchi, cell)->rd_Checkp(srcRef) : nullptr);
              break;
            case TK_SymbolSet:
              defop1(F, SymbolSet, r = cpu.curs[batchi] ? cpu.valueAtRelTime(a, batchi, cell)->rd_SymbolSet(srcRef) : SymbolSet());
              break;
            case TK_Beh:
              defop1(F, Beh, r = cpu.curs[batchi] ? cpu.valueAtRelTime(a, batchi, cell)->rd_Beh(srcRef) : Beh());
              break;
            case TK_VideoFrame:
              defop1(F, VideoFrame, r = cpu.curs[batchi] ? cpu.valueAtRelTime(a, batchi, cell)->rd_VideoFrame(srcRef) : VideoFrame());
              break;
            case TK_Assoc:
              defop1(F, Assocp, r = cpu.curs[batchi] ? cpu.valueAtRelTime(a, batchi, cell)->rd_Assocp(srcRef) : nullptr);
              break;
            case TK_Api:
              defop1(F, ApiHandle, r = cpu.curs[batchi] ? cpu.valueAtRelTime(a, batchi, cell)->rd_ApiHandle(srcRef) : ApiHandle());
              break;
            // ADD type
          }
        }
        else {
          evalSpecial(lhs, index1);
          auto cell = ce.cell;

          defold2(F, mkValue(args[0].t), F, F, F, (r = cpu.valueAtRelTime(b, batchi, cell)->rd_F(statev), snew = a));
          defold2(CF, mkValue(args[0].t), CF, F, CF, (r = cpu.valueAtRelTime(b, batchi, cell)->rd_CF(statev), snew = a));
          defold2(vec2, mkValue(args[0].t), vec2, F, vec2, (r = cpu.valueAtRelTime(b, batchi, cell)->rd_vec2(statev), snew = a));
          defold2(vec3, mkValue(args[0].t), vec3, F, vec3, (r = cpu.valueAtRelTime(b, batchi, cell)->rd_vec3(statev), snew = a));
          defold2(vec4, mkValue(args[0].t), vec4, F, vec4, (r = cpu.valueAtRelTime(b, batchi, cell)->rd_vec4(statev), snew = a));
          defold2(mat2, mkValue(args[0].t), mat2, F, mat2, (r = cpu.valueAtRelTime(b, batchi, cell)->rd_mat2(statev), snew = a));
          defold2(mat3, mkValue(args[0].t), mat3, F, mat3, (r = cpu.valueAtRelTime(b, batchi, cell)->rd_mat3(statev), snew = a));
          defold2(mat4, mkValue(args[0].t), mat4, F, mat4, (r = cpu.valueAtRelTime(b, batchi, cell)->rd_mat4(statev), snew = a));
          defold2(Beh, mkValue(args[0].t), Beh, F, Beh, (r = cpu.valueAtRelTime(b, batchi, cell)->rd_Beh(statev), snew = a));
          defold2(mat, mkValue(args[0].t), mat, F, mat, (r = cpu.valueAtRelTime(b, batchi, cell)->rd_mat(statev), snew = a));
          defold2(cmat, mkValue(args[0].t), cmat, F, cmat, (r = cpu.valueAtRelTime(b, batchi, cell)->rd_cmat(statev), snew = a));

        }
      }
    }
  }
  return setError("Unsupported time index");
}

defusage(index, R"(
  value[-dt]
  value[relative time] -- should be negative to respect causality)");


deftype(assoc)
{
  return typetag<Assoc>();
}

defemit(assoc)
{
  string_view optionName = n->data_sv();

  defop2(Assocp, F, Assocp, r = cpu.curs[batchi] ? cpu.pool->mkAssoc(a, optionName, BoxedPtr(cpu.pool->mk<F>(b))) : nullptr);
  defop2(Assocp, string_view, Assocp, r = cpu.curs[batchi] ? cpu.pool->mkAssoc(a, optionName, BoxedPtr(cpu.pool->mk<string_view>(cpu.pool->strdupview(b)))) : nullptr);
  defop2(Assocp, vec2, Assocp, r = cpu.curs[batchi] ? cpu.pool->mkAssoc(a, optionName, BoxedPtr(cpu.pool->mk<vec2>(b))) : nullptr);
  defop2(Assocp, vec4, Assocp, r = cpu.curs[batchi] ? cpu.pool->mkAssoc(a, optionName, BoxedPtr(cpu.pool->mk<vec4>(b))) : nullptr);
  defop2(Assocp, mat4, Assocp, r = cpu.curs[batchi] ? cpu.pool->mkAssoc(a, optionName, BoxedPtr(cpu.pool->mk<mat4>(b))) : nullptr);
  defop2(Assocp, mat, Assocp, r = cpu.curs[batchi] ? cpu.pool->mkAssoc(a, optionName, BoxedPtr(cpu.pool->mk<mat>(cpu.pool->mkMat(b)))): nullptr);
  defop2(Assocp, Assocp, Assocp, r = cpu.curs[batchi] ? cpu.pool->mkAssoc(a, optionName, b) : nullptr);

  defop1(F, Assocp, r = cpu.curs[batchi] ? cpu.pool->mkAssoc(nullptr, optionName, BoxedPtr(cpu.pool->mk<F>(a))) : nullptr);
  defop1(string_view, Assocp, r = cpu.curs[batchi] ? cpu.pool->mkAssoc(nullptr, optionName, BoxedPtr(cpu.pool->mk<string_view>(cpu.pool->strdupview(a)))) : nullptr);
  defop1(vec2, Assocp, r = cpu.curs[batchi] ? cpu.pool->mkAssoc(nullptr, optionName, BoxedPtr(cpu.pool->mk<vec2>(a))) : nullptr);
  defop1(vec4, Assocp, r = cpu.curs[batchi] ? cpu.pool->mkAssoc(nullptr, optionName, BoxedPtr(cpu.pool->mk<vec4>(a))) : nullptr);
  defop1(mat4, Assocp, r = cpu.curs[batchi] ? cpu.pool->mkAssoc(nullptr, optionName, BoxedPtr(cpu.pool->mk<mat4>(a))) : nullptr);
  defop1(mat, Assocp, r = cpu.curs[batchi] ? cpu.pool->mkAssoc(nullptr, optionName, BoxedPtr(cpu.pool->mk<mat>(cpu.pool->mkMat(a)))) : nullptr);
  defop1(Assocp, Assocp, r = cpu.curs[batchi] ? cpu.pool->mkAssoc(nullptr, optionName, a) : nullptr);

  return setInvalidArgs();
}

defusage(assoc, R"(
  {option=value, ...})");

deftype(with)
{
  return eval(n->ch(0));
}

defemit(with)
{
  defop2(DrawOpHandle, Assocp, DrawOpHandle, r = DrawOpHandle(a.it, merge(cpu.pool, a.props, b)));
  defop2(ApiHandle, Assocp, ApiHandle, r = ApiHandle(a.apiType, merge(cpu.pool, a.props, b)));
  return setInvalidArgs();
}

defusage(with, R"(
  expr{option=value, ...})");


deftype(api)
{
  return TypeTag(TK_Api);
}

defemit(api)
{
  string_view apiType = n->data_sv();
  defop0(ApiHandle, r = ApiHandle(apiType, nullptr));
  return setInvalidArgs();
}

defusage(api, R"(
  api.type(){attr=value...})");
