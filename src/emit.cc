#include "./emit.h"
#include "numerical/numerical.h"
#include "./emit_macros.h"

/*
  Turn a formula (previously parsed to an AST) into executable code.

  This generates two kinds of code:
    - A `vector<function<void(CpuState &)>>` of closures that can be called in sequence
    - C++ code as text.
  
  The vector of lambdas is instantly executable. The C++ code is faster once
  compiled, and also human-readable.

  For instance, the C++ output for `plant.waistPtr = plant.waistTorque * plant.th2vel` looks like:

```c++
    // Out of date!
    static void full_2UhzoCtnx1Stzipo8ebd(CpuState &cpu, Pad &pad, auto batchdef) // For plant.tb:plant.waistPwr
    {
      F v0 = s.op_ref<F>(56); // plant.waistTorque
      F v4 = s.op_ref<F>(8); // plant.th2vel
      F v8 = s.op_mul<F, F, F>(v0, v4);
      s.op_set<F, void>(v8, 112); // plant.waistPwr
    }
```
  
  While the vector of lambdas would contain 4 lambdas corresponding to the lines above.

  We use some macros here to allow terse generation of many different operator & type cases.
  The macros also ensure that the C++ code matches the lambda code.

  The constructor sets everything up and does the work. After constructing, check `.ok` to see
  if it worked, and then use either `.evalOps` or `.cppCode`.
  
*/


DefTestInstance *DefTestInstance::head;

NodeEmitter::NodeEmitter(CellEmitter &_ce, AstNode *_n, int _depth)
  : ce(_ce),
    she(ce.she),
    n(_n),
    debugLoc(n->loc),
    depth(_depth),
    evalDone(false),
    evalSpecialDone(false),
    inCheck(false)
{
}

NodeEmitter::NodeEmitter(NodeEmitter &_parent, AstNode *_n)
  : ce(_parent.ce),
    she(ce.she),
    n(_n),
    debugLoc(n->loc),
    depth(_parent.depth+1),
    evalDone(false),
    evalSpecialDone(false),
    inCheck(_parent.inCheck || (n && (n->t == A_check || n->t == A_checkat || n->t == A_checkbetween)))
{  
}

int NodeEmitter::evalArgs()
{
  if (!evalDone) {
    for (int i = 0; i < n->nch(); i++) {
      args.push_back(NodeEmitter(*this, n->ch(i)).emit());
    }
    evalDone = true;
  }
  return int(args.size());
}


bool NodeEmitter::matchArgs(initializer_list<TypeTag> want, bool allowMore)
{
  evalArgs();
  if (allowMore ? args.size() >= want.size() : args.size() == want.size()) {
    size_t i = 0;
    for (auto w : want) {
      if (!args[i].t.convmatch(w)) return false;
      i++;
    }

    i = 0;
    for (auto w : want) {
      auto av = args[i];
      if (!av.t.match(w)) {
        if (w.kind == TK_bool && av.t.kind == TK_F) {
          auto rv = args[i] = allocPad<bool>();
          she.evalop([rv1=rv, av1=av](CpuState &cpu, Pad &pad, auto batchdef) {
            auto rv = batchdef.getVal(rv1);
            auto av = batchdef.getVal(av1);
            decltype(auto) r = pad.wr_bool_simd(rv, batchdef);
            decltype(auto) a = pad.rd_F_simd(av, batchdef);
            r = a >= 0.5f;
          });
        }
        else if (w.kind == TK_F && av.t.kind == TK_bool) {
          auto rv = args[i] = allocPad<F>();
          she.evalop([rv1=rv, av1=av](CpuState &cpu, Pad &pad, auto batchdef) {
            auto rv = batchdef.getVal(rv1);
            auto av = batchdef.getVal(av1);
            decltype(auto) r = pad.wr_F_simd(rv, batchdef);
            decltype(auto) a = pad.rd_bool_simd(av, batchdef);
            r = a.template cast<F>();
          });
        }
        else if (w.kind == TK_bool && av.t.kind == TK_Check) {
          auto rv = args[i] = allocPad<bool>();
          she.evalop([rv1=rv, av1=av](CpuState &cpu, Pad &pad, auto batchdef) {
            auto rv = batchdef.getVal(rv1);
            auto av = batchdef.getVal(av1);
            for (int batchi=0; batchi < batchdef.compiledBatchSize(); batchi++) {
              decltype(auto) r = pad.wr_bool(rv, batchi);
              decltype(auto) a = pad.rd_Checkp(av, batchi);
              r = a && a->status == Check_pass;
            }
          });
        }
        else if (w.kind == TK_bool && av.t.kind == TK_SymbolSet) {
          auto rv = args[i] = allocPad<bool>();
          she.evalop([rv1=rv, av1=av](CpuState &cpu, Pad &pad, auto batchdef) {
            auto rv = batchdef.getVal(rv1);
            auto av = batchdef.getVal(av1);
            for (int batchi=0; batchi < batchdef.compiledBatchSize(); batchi++) {
              decltype(auto) r = pad.wr_bool(rv, batchi);
              decltype(auto) a = pad.rd_SymbolSet(av, batchi);
              r = !a.empty();
            }
          });
        }
        else if (w.kind == TK_bool && av.t.kind == TK_Beh) {
          auto rv = args[i] = allocPad<bool>();
          she.evalop([rv1=rv, av1=av](CpuState &cpu, Pad &pad, auto batchdef) {
            auto rv = batchdef.getVal(rv1);
            auto av = batchdef.getVal(av1);
            for (int batchi=0; batchi < batchdef.compiledBatchSize(); batchi++) {
              decltype(auto) r = pad.wr_bool(rv, batchi);
              decltype(auto) a = pad.rd_Beh(av, batchi);
              r = a.status == B_running || a.status == B_success;
            }
          });
        }
        else if (w.kind == TK_CF && av.t.kind == TK_F) {
          auto rv = args[i] = allocPad<CF>();
          if (0) L() << "matchArgs: " << ce.sh.nameByCell[ce.cell] << ": promote F to CF";
          she.evalop([rv1=rv, av1=av](CpuState &cpu, Pad &pad, auto batchdef) {
            auto rv = batchdef.getVal(rv1);
            auto av = batchdef.getVal(av1);
            decltype(auto) r = pad.wr_CF_simd(rv, batchdef);
            decltype(auto) a = pad.rd_F_simd(av, batchdef);
            r = a;
          });
        }
        else {
          L() << "matchArgs: Failed to convert " << typeName(av.t) << " to " << typeName(w);
          return false;
        }
      }
      i++;
    }
    return true;
  }
  return false;
}



/*
  Emit a cell
*/

CellEmitter::CellEmitter(CompiledSheet &_she, CellIndex _cell, SheetEmitContext *_hypoCtx)
  : she(_she),
    sh(_she.sh),
    cell(_cell),
    root(sh.astByCell[cell]),
    hypoCtx(_hypoCtx)
{
  ok = true;
  if (!root) {
    ok = false;
    return;
  }

  auto ret = emitCell(root);
  if (ret.t.isError()) {
  }
  else if (!she.typeByCell[cell].isUndef() && she.typeByCell[cell] != ret.t) {
    setError("Type confusion");
  }
  else {
    she.typeByCell[cell] = ret.t;
    BoxedRef &dstValue = hypoCtx->refByCell[cell];
    switch (hypoCtx->progress[cell]) {

      case CES_init:
        dielog("Unexpected CES_init" << LOGV(sh.nameByCell[cell]));
        break;

      case CES_placeholder:
        assertlog(dstValue.valid(), LOGV(dstValue));
        break;

      case CES_injected:
        dielog("Unexpected CES_injected" << LOGV(sh.nameByCell[cell]));
        break;

      case CES_working:
        if (!dstValue.valid()) {
          dstValue = she.mkValue(cell, ret.t);
        }
        break;

      case CES_typing:
        dielog("Unexpected CES_typing" << LOGV(sh.nameByCell[cell]));
        break;

      case CES_done:
        dielog("Unexpected CES_done" << LOGV(sh.nameByCell[cell]));
        break;

      case CES_err:
        break;
    }

    if (dstValue.valid()) {
      setValue(dstValue, ret);
    }
    else {
      setError(""); // Should already be an error there from type inference
    }
  }
}

ValSuite CellEmitter::setError(string const &e, AstNode *n)
{
  if (sh.verbose >= 1) {
    L() << sh.nameByCell[cell] + ": " + e + "\n";
    if (sh.verbose >= 2) {
      L() << " AST: " << WithSheet(sh, root);
    }
  }
  if (ok) {
    error = e;
    if (n) {
      errorLoc = n->loc;
    }
    else {
      errorLoc = FormulaSubstr(0, 0);
    }
    ok = false;
  }
  return ValSuite(T_error);
}

BoxedRef CellEmitter::resolveScopedName(CellIndex srcCell, SheetEmitContext *ctx, bool deferrable)
{
  auto ret = she.doCell(srcCell, ctx, deferrable);
  if (!ret.valid()) {
    if (she.sh.verbose >= 1) L() << sh.nameByCell[srcCell] << ": resolveScopedName failed\n";
  }
  return ret;
}
BoxedRef CellEmitter::resolveName(AstNode *name, bool deferrable)
{
  if (!name) {
    return BoxedRef();
  }
  else if (name->t == A_cellref) {
    if (auto srcCell = name->data<CellIndex>(); srcCell != nullCellIndex) {
      return resolveScopedName(srcCell, hypoCtx, deferrable);
    }
  }
  else if (name->t == A_hypobinding) {
    auto cellref = name->ch(0);
    assert(cellref && cellref->t == A_cellref);

    auto newHypoCtx = she.mkContext(hypoCtx);
    newHypoCtx->origins.emplace_back(cell, name->loc);

    for (int chi = 1; chi < name->nch(); chi += 2) {
      auto bindName = name->ch(chi);
      auto bindValue = name->ch(chi+1);
      if (bindName->t == A_name) {
        // Unknown name, possibly for a cell that hasn't been defined yet.
        return BoxedRef();
      }
      assert(bindName->t == A_cellref);
      ValSuite bindVal = NodeEmitter(*this, bindValue, 0).emit();

      if (bindVal.t.isError()) {
        return BoxedRef();
      }

      auto bindValueRef = she.mkValue(cell, bindVal.t);

      setValue(bindValueRef, bindVal);
      newHypoCtx->progress[bindName->data<CellIndex>()] = CES_injected;
      newHypoCtx->refByCell[bindName->data<CellIndex>()] = bindValueRef;
    }

    she.addDeps(newHypoCtx);

    return resolveScopedName(cellref->data<CellIndex>(), newHypoCtx, deferrable);
  }
  return BoxedRef();
}


ParamIndex CellEmitter::getParamIndex(AstNode *n)
{
  if (n && n->t == A_cellref) {
    auto srcRef = resolveName(n, true);
    if (!srcRef.valid()) return nullParamIndex;
    if (sh.paramsByCell[srcRef.cell].empty()) return nullParamIndex;
    return sh.paramsByCell[srcRef.cell].at(0);
  }
  else if (n && n->t == A_paramref) {
    auto srcRef = resolveName(n->ch(0), true);
    if (!srcRef.valid()) return nullParamIndex;
    if (!(n->data<ParamIndex>() < sh.paramsByCell[srcRef.cell].size())) return nullParamIndex;
    return sh.paramsByCell[srcRef.cell].at(n->data<ParamIndex>());
  }
  return nullParamIndex;
}

ValSuite CellEmitter::emitCell(AstNode *n)
{
  switch (n->t) {

    case A_formula:
      isPureParam = n->ch(0) && !n->ch(1) && n->ch(0)->t == A_param;
      return NodeEmitter(*this, n, 0).emit();

    default:
      return setError("Unknown cell type " + repr(n->t));
  }
}

ValSuite NodeEmitter::emit()
{
  if (!n) return ValSuite(T_undef);

  if (auto fval = getConstF(n)) {
    auto imm = *fval;
    defopn_simd(F, r = imm);
  }
  if (auto cfval = getConstCF(n)) {
    auto imm = *cfval;
    defopn_simd(CF, r = imm);
  }
  ValSuite ret;

  switch (n->t) {
    #define defdispatch(T) case A_##T: ret = emit_##T(); break;
    foreach_ast(defdispatch)
    #undef defdispatch
  }

  if (ret.valid()) return ret;
  if (ret.t.kind == T_undef) return ret;

  {
    for (size_t argi = 0; argi < args.size(); argi++) {
      auto &it = args[argi];
      if (it.t.isError()) {
        return setError("ref error", n->ch(argi));
      }
    }
    ostringstream s;
    s << "Invalid " <<  n->t;
    if (evalDone) {
      s << "(";
      bool first = true;
      for (auto &it : args) {
        if (!first) s << ", ";
        first = false;
        s << it.t;
      }
      s << ")";
    }
    return setError(s.str(), n);
  }
}

void NodeEmitter::fmtCalledAs(ostream &s)
{
  if (evalDone && !evalSpecialDone) {
    s << "\nCalled as " << n->t << "(";
    bool first = true;
    for (auto &it : args) {
      if (!first) s << ", ";
      first = false;
      s << it.t;
    }
    s << ")\n";
  }
  else {
    s << "\nCalled as " << n->t << "(";
    for (int i = 0; i < n->nch(); i++) {
      if (i != 0) s << ", ";
      if (auto constval = getConstInt(n->ch(i))) {
        s << *constval;
      }
      else {
        auto fakeVal = NodeEmitter(*this, n->ch(i)).emit();
        s << fakeVal.t;
      }
    }
    s << ")?\n";
  }
}

ValSuite NodeEmitter::setError(string const &e, AstNode *n1)
{
  ostringstream s;
  s << e;
  if (n->t == A_cellref) {
  }
  else if (n->t == A_name) {
  }
  else {
    s << "\n" << AstHelp::help(n);
    fmtCalledAs(s);
  }
  return ce.setError(withoutBlankLines(s.str()), n1 ? n1 : n);
}


BoxedRef NodeEmitter::mkValue(TypeTag t)
{
  return ce.mkValue(t);
}



ValSuite NodeEmitter::allocPad(TypeTag t)
{
  /*
    If we call this with eg typetag<mat>(), we need to know the actual size.
    See if we can find a matching argument with a given size.
  */
  if ((t.isMat() || t.isCmat()) && (t.nr == 0 || t.nc == 0)) {
    for (auto &it : args) {
      if (it.t.kind == t.kind) {
        L() << "Upgraded " << t << " to " << it.t;
        return ce.allocPad(it.t);
      }
    }
  }

  return ce.allocPad(t);
}

ValSuite NodeEmitter::allocResult(TypeTag t)
{
  auto r = allocPad(t);
  if (she.doDebug && r.valid() && debugLoc.end > debugLoc.begin) ce.setDebugVal(n->t, debugLoc, r);
  return r;
}


void CellEmitter::setValue(BoxedRef dst, ValSuite v)
{
  if (!dst.valid() || dst.ofs == nullValueOffset) {
    L() << "CellEmitter::serValue: dst=" << dst;
    return;
  }

  if (dst.t != v.t) {
    setError("Assign " + repr(dst.t) + " = " + repr(v.t) + " failed");
    return;
  }

  switch (dst.t.kind) {
    case TK_undef:
    case TK_error:
      return;

    case TK_F:
      defsetg_simd(F, v, F, dst);
      break;
    
    case TK_CF:
      defsetg_simd(CF, v, CF, dst);
      break;

    case TK_bool:
      setError("Assign " + repr(dst.t) + " = " + repr(v.t) + ": unexpected bool");
      return;
    
    case TK_mat:
      if (0) L() << "setValue(" << dst << ", " << v << ")?\n";
      defsetg(vec2, v, vec2, dst);
      defsetg(vec3, v, vec3, dst);
      defsetg(vec4, v, vec4, dst);
      defsetg(mat2, v, mat2, dst);
      defsetg(mat3, v, mat3, dst);
      defsetg(mat4, v, mat4, dst);
      defsetg(mat, v, mat, dst);
      break;

    case TK_cmat:
      defsetg(cmat, v, cmat, dst);
      break;

    case TK_str:
      defset(string_view, v, string_view, dst);
      break;

    case TK_DrawOp:
      defset(DrawOpHandle, v, DrawOpHandle, dst);
      break;

    case TK_Check:
      defset(Checkp, v, Checkp, dst);
      break;

    case TK_SymbolSet:
      defset(SymbolSet, v, SymbolSet, dst);
      break;

    case TK_Beh:
      defset(Beh, v, Beh, dst);
      break;

    case TK_VideoFrame:
      defset(VideoFrame, v, VideoFrame, dst);
      break;

    case TK_Assoc:
      defset(Assocp, v, Assocp, dst);
      break;

    case TK_Api:
      defset(ApiHandle, v, ApiHandle, dst);
      break;

    // ADD type

  }
  setError("Assign " + repr(dst.t) + " = " + repr(v.t) + " failed");
}

