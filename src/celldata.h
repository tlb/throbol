#pragma once
#include "./defs.h"
#include "./blobs.h"


struct Symbol {
  U32 i = 0;

  Symbol() = default;

  Symbol(U32 _i)
   :i(_i)
  {    
  }
  
  bool isNil() const
  {
    return i == 0;
  }
};

inline bool operator < (Symbol const &a, Symbol const &b)
{
  return a.i < b.i;
}

inline bool operator == (Symbol const &a, Symbol const &b)
{
  return a.i == b.i;
}

struct SymbolSet {
  Symbol leaf;
  SymbolSet const *tail = nullptr;

  SymbolSet() = default;
  
  SymbolSet(Symbol _leaf)
   :leaf(_leaf),
     tail(nullptr)
  {
  }

  SymbolSet(Symbol _leaf, SymbolSet const *_tail)
   :leaf(_leaf),
    tail(_tail)
  {
  }

  SymbolSet(SymbolSet const &other)
   :leaf(other.leaf),
    tail(other.tail)
  {
  }

  SymbolSet & operator =(SymbolSet const &other)
  {
    leaf = other.leaf;
    tail = other.tail;
    return *this;
  }

  void check() const
  {
    assertlog(leaf.i < 10000, LOGV(leaf.i));
    for (auto *p = tail; p != nullptr; p = p->tail) {
      L() << repr_ptr((void *)p);
      assertlog(((size_t)p & 0x07) == 0, LOGV((void *)p));
    }
  }

  void setZero()
  {
    *this = SymbolSet();
  }

  bool empty() const
  {
    return leaf.isNil() && !tail;
  }

};

enum DistType {
  Dist_linear,
  Dist_exponential,
  Dist_nonnegative,
  Dist_complexReal,
  Dist_complexImag,
};

enum DrawStyle {
  Draw_std,
  Draw_custom
};

enum BehStatus {
  B_running,
  B_failure,
  B_success
};

struct Beh {
  BehStatus status = B_failure;
  SymbolSet runset;

  Beh() = default;

  Beh(BehStatus _status, SymbolSet const &_runset)
   :status(_status),
    runset(_runset) 
  {
  }

  Beh(BehStatus _status)
   :status(_status),
    runset()
  {
  }

  Beh(Beh const &other)
   :status(other.status),
    runset(other.runset)
  {
  }

  Beh & operator =(Beh const &other) {
    status = other.status;
    runset = other.runset;
    return *this;
  }

  void check() const
  {
    assertlog(status == B_running || status == B_failure || status == B_success, "status=" << status);
    runset.check();
  }

  void setZero()
  {
    status = B_running;
    runset = SymbolSet();
  }

};

enum VideoFrameFormat : int {
  VFF_NONE,
  VFF_JPEG,
  VFF_RGB,
  VFF_DEPTH16
};


struct alignas(MAX_ALIGN) VideoFrame {
  mat4 orientation = mat4::Zero();
  VideoFrameFormat format = VFF_NONE;
  int width = 0;
  int height = 0;
  BlobRef blobRef;

  VideoFrame() = default;

  VideoFrame(
    mat4 const &_orientation,
    VideoFrameFormat _format,
    int _width, int _height, 
    BlobRef const &_blobRef)
    : orientation(_orientation),
      format(_format),
      width(_width), height(_height),
      blobRef(_blobRef)
  {
  }

  void setZero()
  {
    *this = VideoFrame();
  }

  template<typename Archive> void serialize(Archive &ar)
  {
    ar(orientation);
    ar(format, width, height);
    ar(blobRef);
  }
};

VideoFrame operator * (mat4 const &a, VideoFrame const &b);


enum AudioFrameFormat : int {
  AFF_NONE,
  AFF_S16
};


struct AudioFrame {
  AudioFrameFormat format = AFF_NONE;
  F sampleRate = 0.0f;
  int sampleCount = 0;
  BlobRef blobRef;

  AudioFrame() = default;

  template<typename Archive> void serialize(Archive &ar)
  {
    ar(format, sampleRate, sampleCount, blobRef);
  }
};

/*
  Helper class when we need to carry around a sheet with an item.
  For instance, a Symbol is just an index so when we go to print one
  we need the Sheet to get the symbolic name.
*/
template<typename T>
struct WithSheet {
  Sheet &sh;
  T it;

  WithSheet(Sheet &_sh, T _it)
   :sh(_sh),
    it(_it)
  {
  }

};

ostream & operator << (ostream &s, WithSheet<Beh> const &a);
ostream & operator << (ostream &s, WithSheet<Symbol> const &a);
ostream & operator << (ostream &s, WithSheet<SymbolSet> const &a);
ostream & operator << (ostream &s, WithSheet<DrawOpHandle> const &a);
ostream & operator << (ostream &s, WithSheet<ApiHandle> const &a);

ostream & operator << (ostream &s, VideoFrameFormat const &a);
ostream & operator << (ostream &s, VideoFrame const &a);
ostream & operator << (ostream &s, AudioFrameFormat const &a);
ostream & operator << (ostream &s, AudioFrame const &a);
ostream & operator << (ostream &s, BehStatus const &a);
ostream & operator << (ostream &s, Beh const &a);
ostream & operator << (ostream &s, Symbol const &a);
ostream & operator << (ostream &s, SymbolSet const &a);
ostream & operator << (ostream &s, ApiHandle const &a);



/*
  We need a reliable way of getting a zero for any type.
  Sadly, the Eigen types are uninitialized, so even vec2{} is full of junk.
*/

template<typename T> inline void setZero(T &it) { it.setZero(); }
template<> inline void setZero(F &it) { it = 0.0f; }
template<> inline void setZero(CF &it) { it = 0.0f; }
template<> inline void setZero(bool &it) { it = false; }
template<> inline void setZero(string_view &it) { it = ""; }
template<> inline void setZero(Checkp &it) { it = nullptr; }
template<> inline void setZero(ApiHandle &it) {it.props = nullptr; }
template<> inline void setZero(DrawOpHandle &it) { it.it = nullptr; it.props = nullptr; }
template<> inline void setZero(Assocp &it) { it = nullptr; }
