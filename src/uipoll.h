#pragma once
#include "./defs.h"

extern bool uiPollLive;

enum UiPollResult {
  UiPoll_closeme,
  UiPoll_userVisible,
  UiPoll_invisible,
};

struct UiPollable {

  virtual UiPollResult uiPoll() = 0; // return UiPoll_closeme to end polling
  virtual void uiRedraw();
  virtual void uiAutosave();

};

void startUiPoll(shared_ptr<UiPollable> w);

SyncLogger W();
SyncLogger W(U32 color);
SyncLogger Wred();
SyncLogger Wgreen();
SyncLogger Wyellow();
SyncLogger Wblue();

extern SyncLogger::LineQueue windowLineq;

struct AsyncTimerWrap;

