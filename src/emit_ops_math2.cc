#include "./emit.h"
#include "./emit_macros.h"



deftype(mul)
{
  auto at = eval(n->ch(0));
  auto bt = eval(n->ch(1));
  if (at.convist<F>() && bt.convist<F>()) return typetag<F>();
  if (at.ist<CF>() && bt.ist<CF>()) return typetag<CF>();
  if (at.convist<F>() && bt.ist<CF>()) return typetag<CF>();
  if (at.ist<CF>() && bt.convist<F>()) return typetag<CF>();

  if (at.convist<F>() && bt.ist<mat>()) return bt;
  if (at.ist<mat>() && bt.convist<F>()) return at;

  if (at.ist<vec2>() && bt.ist<vec2>()) return typetag<F>();
  if (at.ist<vec3>() && bt.ist<vec3>()) return typetag<F>();
  if (at.ist<vec4>() && bt.ist<vec4>()) return typetag<F>();

  if (at.ist<vec>() && bt.ist<vec>() && at.nr == bt.nr) return typetag<F>();
  if (at.ist<cvec>() && bt.ist<cvec>() && at.nr == bt.nr) return typetag<CF>();

  if (at.ist<mat>() && bt.ist<mat>()) return TypeTag{TK_mat, at.nr, bt.nc};
  if (at.ist<cmat>() && bt.ist<cmat>()) return TypeTag{TK_cmat, at.nr, bt.nc};

  if (at.ist<mat4>() && (bt.ist<DrawOp>() || bt.ist<VideoFrame>())) return bt;
  return setError();
}

defemit(mul)
{
  defop2g_simd(F, F, F, r = a * b, (ag += rg*b, bg += rg*a));
  defop2g_simd(F, CF, CF, r = a * b, (ag += (rg*b).real(), bg += rg*a));
  defop2g_simd(CF, F, CF, r = a * b, (ag += rg*b, bg += (rg*a).real()));
  defop2g_simd(CF, CF, CF, r = a * b, (ag += rg*b, bg += rg*a));

  defop2g(F, vec2, vec2, r = a * b, (ag += rg.dot(b), bg += rg*a));
  defop2g(F, vec3, vec3, r = a * b, (ag += rg.dot(b), bg += rg*a));
  defop2g(F, vec4, vec4, r = a * b, (ag += rg.dot(b), bg += rg*a));
  defop2g(F, mat2, mat2, r = a * b, (ag += (rg.array() * b.array()).sum(), bg += rg * a));
  defop2g(F, mat3, mat3, r = a * b, (ag += (rg.array() * b.array()).sum(), bg += rg * a));
  defop2g(F, mat4, mat4, r = a * b, (ag += (rg.array() * b.array()).sum(), bg += rg * a));
  if (argmatch2(F, mat)) {
    auto rv = allocResult(args[1].t);
    emit_evalop2(F, mat, mat, r = a * b);
    return rv;
  }

  defop2g(vec2, F, vec2, r = a * b, (ag += rg*b, bg += rg.dot(a)));
  defop2g(vec3, F, vec3, r = a * b, (ag += rg*b, bg += rg.dot(a)));
  defop2g(vec4, F, vec4, r = a * b, (ag += rg*b, bg += rg.dot(a)));
  defop2g(mat2, F, mat2, r = a * b, (ag += rg*b, bg += (rg.array() * a.array()).sum()));
  defop2g(mat3, F, mat3, r = a * b, (ag += rg*b, bg += (rg.array() * a.array()).sum()));
  defop2g(mat4, F, mat4, r = a * b, (ag += rg*b, bg += (rg.array() * a.array()).sum()));
  if (argmatch2(mat, F)) {
    auto rv = allocResult(args[0].t);
    emit_evalop2(mat, F, mat, r = a * b);
    return rv;
  }

  defop2g(vec2, vec2, F, r = a.dot(b), (ag += rg * b, bg += rg * a));
  defop2g(vec3, vec3, F, r = a.dot(b), (ag += rg * b, bg += rg * a));
  defop2g(vec4, vec4, F, r = a.dot(b), (ag += rg * b, bg += rg * a));


  defop2g(mat2, vec2, vec2,
    r.noalias() = a * b,
    (ag += rg * b.transpose(), bg += a.transpose() * rg));
  defop2g(mat2, mat2, mat2,
    r.noalias() = a * b,
    (ag += rg * b.transpose(), bg += a.transpose() * rg));
  defop2g(mat3, vec3, vec3,
    r.noalias() = a * b,
    (ag += rg * b.transpose(), bg += a.transpose() * rg));
  defop2g(mat3, mat3, mat3,
    r.noalias() = a * b,
    (ag += rg * b.transpose(), bg += a.transpose() * rg));
  defop2g(mat4, vec4, vec4,
    r.noalias() = a * b,
    (ag += rg * b.transpose(), bg += a.transpose() * rg));
  defop2g(mat4, mat4, mat4,
    r.noalias() = a * b,
    (ag += rg * b.transpose(), bg += a.transpose() * rg));
    
  defop2(mat4, DrawOpHandle, DrawOpHandle, r = pad.pool->mk<DrawOpTransform>(a, b));
  defop2(mat4, VideoFrame, VideoFrame, r = a * b);

  defop2rg(mat, mat, mat,
    args[0].t.nc == args[1].t.nr,
    allocResult({TK_mat, args[0].t.nr, args[1].t.nc}),
    r.noalias() = a * b,
    (ag.noalias() += rg * b.transpose(), bg.noalias() += a.transpose() * rg));
  defop2rg(cmat, cmat, cmat,
    args[0].t.nc == args[1].t.nr,
    allocResult({TK_cmat, args[0].t.nr, args[1].t.nc}),
    r.noalias() = a * b,
    (ag.noalias() += rg * b.transpose(), bg.noalias() += a.transpose() * rg));
  defop2rg(mat, cmat, cmat,
    args[0].t.nc == args[1].t.nr,
    allocResult({TK_cmat, args[0].t.nr, args[1].t.nc}),
    r.noalias() = a * b,
    (ag.noalias() += (rg * b.transpose()).real(), bg.noalias() += a.transpose() * rg));
  defop2rg(cmat, mat, cmat,
    args[0].t.nc == args[1].t.nr,
    allocResult({TK_cmat, args[0].t.nr, args[1].t.nc}),
    r.noalias() = a * b,
    (ag.noalias() += rg * b.transpose(), bg.noalias() += (a.transpose() * rg).real()));

  // vec dot vec
  defop2r(vec, vec, F, args[0].t.isVec() && args[1].t.isVec() && args[0].t.nr == args[1].t.nr,
    allocResult<F>(),
    r = a.dot(b));
  defop2r(cvec, cvec, CF, args[0].t.isCvec() && args[1].t.isCvec() && args[0].t.nr == args[1].t.nr,
    allocResult<CF>(),
    r = a.dot(b));

  return setInvalidArgs();
}

defusage(mul, R"(
  F|CF * F|CF
  F|CF * vec|cvec|mat|cmat
  vec|cvec|mat|cmat * F|CF
  mat4 * DrawOp -- adds a DrawOpTransform node
  mat4 * VideoFrame -- premultiplies the transformation matrix)");

