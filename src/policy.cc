#include "./policy.h"
#include "./core.h"
#include "./trace.h"

// --- PolicySearchDim

vector<pair<F, F>> PolicySearchDim::getValues(Sheet &sh, int resolution) const
{
  vector<pair<F, F>> ret;
  if (resolution < 2) {
    ret.emplace_back(0.0f, getParamValue(sh, 0.0f));
    return ret;
  }
  for (int i = 0; i < resolution; i++) {
    F tweak = (F(i) / F(resolution - 1) * 2.0f - 1.0f);
    ret.emplace_back(tweak, getParamValue(sh, tweak));
  }
  return ret;
}

VectorXf PolicySearchDim::getRelValues(int resolution) const
{
  assert(resolution > 0);
  VectorXf ret(resolution);
  if (resolution == 1) {
    ret(0) = 0.0f;
  }
  else {
    for (int i = 0; i < resolution; i++) {
      ret(i) = (F(i) / F(resolution - 1) * 2.0f - 1.0f);
    }
  }
  return ret;
}

VectorXf PolicySearchDim::getParamValues(Sheet &sh, int resolution) const
{
  if (param == nullParamIndex) return VectorXf(resolution);
  switch (sh.distByParam[param]) {

    case Dist_linear:
    default:
      return getRelValues(resolution).array() * (relRange * sh.rangeByParam[param]) + getParamBase(sh);

    case Dist_nonnegative:
      return (getRelValues(resolution).array() * (relRange * sh.rangeByParam[param]) + getParamBase(sh)).max(0.0f);

    case Dist_exponential:
      return (getRelValues(resolution).array() * relRange).exp() * getParamBase(sh);
  }

}

F PolicySearchDim::getParamValue(Sheet &sh, F tweak) const
{
  if (param == nullParamIndex) return 0.0f;
  switch (sh.distByParam[param]) {
    case Dist_linear:
    default:
      return tweak * relRange * sh.rangeByParam[param] + getParamBase(sh);

    case Dist_nonnegative:
      return max(0.0f, tweak * relRange * sh.rangeByParam[param] + getParamBase(sh));

    case Dist_exponential:
      return exp(tweak * relRange) * getParamBase(sh);
  }
}

F PolicySearchDim::getParamBase(Sheet &sh) const
{
  if (param == nullParamIndex) return 0.0f;
  return sh.params.valueByParam(param);
}

void PolicySearchDim::setOverride(Trace *trace, F value) const
{
  if (param == nullParamIndex) return;
  trace->params.mutValueByParam(param) = value;
}

bool operator < (PolicySearchDim const &a, PolicySearchDim const &b)
{
  return (a.param < b.param) ? true :
    (b.param < a.param) ? false :
    (a.relRange < b.relRange);
}

bool operator == (PolicySearchDim const &a, PolicySearchDim const &b)
{
  return (a.param == b.param) && (a.relRange == b.relRange);
}


ostream & operator << (ostream &s, WithSheet<PolicySearchDim> const &a)
{
  return s << "dim(" << a.sh.getParamDesc(a.it.param) << "+-" << a.it.relRange << ")";
}


// ---- PolicyTerrainSpec

#define CMPAB(MEMBER) \
    a.MEMBER < b.MEMBER ? true : \
    b.MEMBER < a.MEMBER ? false :

bool operator < (PolicyTerrainSpec const &a, PolicyTerrainSpec const &b)
{
  return
    CMPAB(xVar)
    CMPAB(yVar)
    CMPAB(rMetrics)
    CMPAB(paramHash)
    CMPAB(compiledSheetEpoch)
    CMPAB(sampleIndex)
    CMPAB(baseValues)
    CMPAB(replayBuffer)
    CMPAB(numSeedsToAverage)
    false;
}

template<typename T>
bool operator < (std::span<T> const &a, std::span<T> const &b)
{
  for (size_t i = 0; i < a.size() && i < b.size(); i++) {
    if (a[i] < b[i]) return true;
    if (b[i] < a[i]) return false;
  }
  return a.size() < b.size();
}


bool PolicyTerrainSpec::valid() const
{
  return !rMetrics.empty() > 0 && xVar.valid() && yVar.valid();
}

// ---- ParticleSearchSpec

bool operator < (ParticleSearchSpec const &a, ParticleSearchSpec const &b)
{
  return
    CMPAB(searchDims)
    CMPAB(rMetric)
    CMPAB(alg)
    CMPAB(paramHash)
    CMPAB(compiledSheetEpoch)
    CMPAB(sampleIndex)
    CMPAB(baseValue)
    CMPAB(replayBuffer)
    CMPAB(accelTowardsBest)
    CMPAB(velCoeff)
    CMPAB(initialParamSigma)
    CMPAB(initialVelSigma)
    CMPAB(deFAC)
    CMPAB(deCR)
    CMPAB(populationTarget)
    // ADD ParticleSearchSpec member
    false;
}



bool operator < (MinpackSearchSpec const &a, MinpackSearchSpec const &b)
{
  return
    CMPAB(searchDims)
    CMPAB(rMetric)
    CMPAB(alg)
    CMPAB(paramHash)
    CMPAB(compiledSheetEpoch)
    CMPAB(sampleIndex)
    CMPAB(baseValue)
    CMPAB(replayBuffer)
    CMPAB(regularization)
    // ADD MinpackSearchSpec member
    false;
}


// ---- PolicyTerrainMap

PolicyTerrainMap::PolicyTerrainMap(Sheet &_sh, PolicyTerrainSpec const &_spec, int _xResolution, int _yResolution)
  : sh(_sh),
    spec(_spec),
    xResolution(_xResolution),
    yResolution(_yResolution)
{
  costEst = max(1, xResolution) * max(1, yResolution) * max(1, int(rs.size()));

}

PolicyTerrainMap::~PolicyTerrainMap()
{
  destructed = true;
}

void PolicyTerrainMap::calcStats()
{
  for (auto &[mat, baser, rScale] : rs) {
#if EIGEN_VERSION_AT_LEAST(3,4,0)
    rScale = (mat.array() - baser).abs().maxCoeff<Eigen::PropagateNumbers>();
#else
    rScale = (mat.array() - baser).abs().maxCoeff();
#endif
  }
}

ostream & operator << (ostream &s, ParticleSearchAlg const &a)
{
  switch (a) {
    case ParticleSearch_swarm:
      return s << "swarm";
    case ParticleSearch_deRand1Bin:
      return s << "deRand1Bin";
    case ParticleSearch_deBest2Bin:
      return s << "deBest2Bin";
    // ADD ParticleSearchAlg
  }
  return s << "alg(" << int(a) << ")";
};

ostream & operator << (ostream &s, MinpackAlg const &a)
{
  switch (a) {
    case Minpack_NewtonDescent:
      return s << "NewtonDescent";
    case Minpack_GradientDescent:
      return s << "GradientDescent";
    case Minpack_ConjugatedGradientDescent:
      return s << "ConjugatedGradientDescent";
    case Minpack_Bfgs:
      return s << "Bfgs";
    case Minpack_Lbfgs:
      return s << "Lbfgs";
    case Minpack_Lbfgsb:
      return s << "Lbfgsb";
    // ADD MinpackAlg
  }
  return s << "alg(" << int(a) << ")";
}
