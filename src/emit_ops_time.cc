#include "./emit.h"
#include "./emit_macros.h"


deftype(dt)
{
  return typetag<F>();
}

defemit(dt)
{
  ce.hasDt = true;
  defop0_simd(F, (r = Map<Eigen::Array<F, batchdef.compiledBatchSize(), 1>>(cpu.dts)));
  return setInvalidArgs();
}

defusage(dt, R"(
  dt -- interval between successive calculations)");


deftype(time)
{
  return typetag<F>();
}

defemit(time)
{
  ce.hasTime = true;
  defop0_simd(F, (r = Map<Eigen::Array<F, batchdef.compiledBatchSize(), 1>>(cpu.times)));
  return setInvalidArgs();
}

defusage(time, R"(
  time -- real/simulated elapsed time)");


deftype(initial)
{
  return typetag<F>();
}

defemit(initial)
{
  ce.hasTime = true;
  defop0_simd(F, r = (cpu.isInitial() ? 1.0f : 0.0f));
  return setInvalidArgs();
}

defusage(initial, R"(
  initial -- true for first recalculation)");


template<typename DIST>
auto deterministicRandom(U64 s1, U64 s2, DIST &dist)
{
  std::seed_seq ss{s1, s2};
  std::mt19937_64 gen{ss};
  return dist(gen);
}

// Randomly is not a good way to design a random number generator.
//   -- Knuth
// Here I want repeatabilty based on a seed
void op_random(CpuState &cpu, F &r, U64 hashcode, int batchi)
{
  if (cpu.traces[batchi] && cpu.traces[batchi]->randomSeed != 0) {
    std::uniform_real_distribution<F> dist(0.0f, 1.0f);
    r = deterministicRandom(cpu.traces[batchi]->randomSeed, hashcode, dist);
  }
  else {
    r = 0.5f;
  }
}


deftype(random)
{
  return typetag<F>();
}

defemit(random)
{
  ce.hasRandom = true;
  /*
    Compute a seed from the name of the cell, and the index of the call to random within the cell.
  */
  U64 cellHash = hashcodeU64(she.sh.nameByCell[ce.cell]);
  U64 seed = cellHash + ce.lexIndex++;

  defopn(F, op_random(cpu, r, seed, batchi));
  return setInvalidArgs();
}

defusage(random, R"(
  random() -- in [0, 1))");


void op_randomNormal(CpuState &cpu, F &r, U64 hashcode, int batchi)
{
  if (cpu.traces[batchi] && cpu.traces[batchi]->randomSeed != 0) {
    std::normal_distribution<F> dist(0.0f, 1.0f);
    r = clamp(deterministicRandom(cpu.traces[batchi]->randomSeed, hashcode, dist), -3.0f, 3.0f);
  }
  else {
    r = 0.0f;
  }
}


deftype(randomNormal)
{
  return typetag<F>();
}

defemit(randomNormal)
{
  ce.hasRandom = true;
  U64 cellHash = hashcodeU64(she.sh.nameByCell[ce.cell]);
  U64 seed = cellHash + ce.lexIndex++;

  defopn(F, op_randomNormal(cpu, r, seed, batchi));
  return setInvalidArgs();
}

defusage(randomNormal, R"(
  randomNormal() -- mean 0, variance 1)");
