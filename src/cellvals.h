#pragma once
#include "./types.h"
#include "./boxed.h"

/*
  CellVals represents the values of all the spreadsheet cells at a point in time.
  (We also abuse them to hold gradients)
  A Trace is a vector of CellVals instances.
  Access is through member functions like
  ```
    vec2 const &rd_vec2(BoxedRef ref)
  ```
  although dynamically-sized arrays are returned by value, since the type
  `mat = Eigen::Map<Eigen::Matrix<...>>` contains a pointer to the data and dimensions:
  ```
    mat rd_mat(BoxedRef ref)
  ```
  The rd_X functions also can be called with a raw valueOffset, which we use
  to avoid capturing entire BoxedRef values in the lambdas for code gen.
*/

struct alignas(MAX_ALIGN) CellVals {

  Sheet &sh;
  CompiledSheet *compiled = nullptr;
  size_t bufSize = 0;
  alignas(MAX_ALIGN) U8 buf[0];

  // Don't use this directly, since it has an extensible size. Use
  // CellVals::mk instead.
  explicit CellVals(CompiledSheet *_compiled);
  ~CellVals();

  // Preferred way to construct.
  static CellVals *mk(CompiledSheet *_compiled, Arena *pool);
  static shared_ptr<CellVals> mk(CompiledSheet *_compiled);

  void reportOverflow(BoxedRef const &ref) const;

  /*
    Define accessors named after each type. This doesn't work
    well with templates, so the accessors have the types in the name.

  */

#define memaccessor(T) \
  inline T const &rd_##T(BoxedRef v) const { \
    if (!(v.ofs + sizeof(T) <= bufSize)) reportOverflow(v); \
    return *(T *)(&buf[v.ofs]); \
  } \
  inline T const &rd_##T(BoxedRef v, int batchi) const { \
    if (!(v.ofs + sizeof(T) <= bufSize)) reportOverflow(v); \
    return *(T *)(&buf[v.ofs]); \
  } \
  inline T const &rd_##T##_unchecked(BoxedRef v) const { \
    return *(T *)(&buf[v.ofs]); \
  } \
  inline T &wr_##T(BoxedRef v) { \
    if (!(v.ofs + sizeof(T) <= bufSize)) reportOverflow(v); \
    return *(T *)(&buf[v.ofs]); \
  } \
  inline T &wr_##T##_unchecked(BoxedRef v) { \
    return *(T *)(&buf[v.ofs]); \
  } \
  inline T &up_##T(BoxedRef v) { \
    if (!(v.ofs + sizeof(T) <= bufSize)) reportOverflow(v); \
    return *(T *)(&buf[v.ofs]); \
  } \
  inline T &up_##T##_unchecked(BoxedRef v) { \
    return *(T *)(&buf[v.ofs]); \
  }

  memaccessor(F)
  memaccessor(CF)
  memaccessor(bool)
  memaccessor(vec2)
  memaccessor(vec3)
  memaccessor(vec4)
  memaccessor(mat2)
  memaccessor(mat3)
  memaccessor(mat4)
  memaccessor(string_view)
  memaccessor(DrawOpHandle)
  memaccessor(Checkp)
  memaccessor(SymbolSet)
  memaccessor(Beh)
  memaccessor(VideoFrame);
  memaccessor(Assocp);
  memaccessor(ApiHandle);

  // getting a mat is slightly different, because we return (by value)
  // an object containing the dimensions and a pointer to the contents.
  inline mat rd_mat(BoxedRef v) const {
    if (!(v.ofs + v.t.nr * v.t.nc * sizeof(F) <= bufSize)) reportOverflow(v);
    return mat((F *)&buf[v.ofs], v.t.nr, v.t.nc);
  }
  inline mat rd_mat_unchecked(BoxedRef v) const {
    return mat((F *)&buf[v.ofs], v.t.nr, v.t.nc);
  }
  inline mat rd_mat(BoxedRef v, int batchi) const {
    return rd_mat(v);
  }
  inline mat wr_mat(BoxedRef v) const {
    if (!(v.ofs + v.t.nr * v.t.nc * sizeof(F) <= bufSize)) reportOverflow(v);
    return mat((F *)&buf[v.ofs], v.t.nr, v.t.nc);
  }
  inline mat wr_mat_unchecked(BoxedRef v) const {
    return mat((F *)&buf[v.ofs], v.t.nr, v.t.nc);
  }
  inline mat up_mat(BoxedRef v) const {
    if (!(v.ofs + v.t.nr * v.t.nc * sizeof(F) <= bufSize)) reportOverflow(v);
    return mat((F *)&buf[v.ofs], v.t.nr, v.t.nc);
  }
  inline mat up_mat_unchecked(BoxedRef v) const {
    return mat((F *)&buf[v.ofs], v.t.nr, v.t.nc);
  }

  inline cmat rd_cmat(BoxedRef v) const {
    if (!(v.ofs + v.t.nr * v.t.nc * sizeof(CF) <= bufSize)) reportOverflow(v);
    return cmat((CF *)&buf[v.ofs], v.t.nr, v.t.nc);
  }
  inline cmat rd_cmat_unchecked(BoxedRef v) const {
    return cmat((CF *)&buf[v.ofs], v.t.nr, v.t.nc);
  }
  inline cmat rd_cmat(BoxedRef v, int batchi) const {
    return rd_cmat(v);
  }
  inline cmat wr_cmat(BoxedRef v) const {
    if (!(v.ofs + v.t.nr * v.t.nc * sizeof(CF) <= bufSize)) reportOverflow(v);
    return cmat((CF *)&buf[v.ofs], v.t.nr, v.t.nc);
  }
  inline cmat wr_cmat_unchecked(BoxedRef v) const {
    return cmat((CF *)&buf[v.ofs], v.t.nr, v.t.nc);
  }
  inline cmat up_cmat(BoxedRef v) const {
    if (!(v.ofs + v.t.nr * v.t.nc * sizeof(CF) <= bufSize)) reportOverflow(v);
    return cmat((CF *)&buf[v.ofs], v.t.nr, v.t.nc);
  }
  inline cmat up_cmat_unchecked(BoxedRef v) const {
    return cmat((CF *)&buf[v.ofs], v.t.nr, v.t.nc);
  }

  // ADD type

  vector<BoxedPtr> ptrsByCell(CellIndex cell) const;
  vector<BoxedPtr> ptrsByCellName(string const &name) const;
  vector<WithSheet<BoxedPtr>> scopedPtrsByCell(CellIndex cell) const;
  string sv(string const &name);
  void zero();

#undef memaccessor
};

ostream & operator <<(ostream &s, CellVals const &a);
