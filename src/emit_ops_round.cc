#include "./emit.h"
#include "./emit_macros.h"

deftype(abs)
{
  auto at = eval(n->ch(0));
  if (at == typetag<CF>()) return typetag<F>();
  return at;
}

defemit(abs)
{
  defop1g_simd(F, F, r = abs(a), ag += rg * a.sign());
  defop1g_simd(CF, F, r = abs(a), ag += rg * a / r.max(0.001f));
  defop1g(vec2, vec2, r = a.array().abs(), ag.array() += (a.array() >= 0.0f).select(rg.array(), -rg.array()));
  defop1g(vec3, vec3, r = a.array().abs(), ag.array() += (a.array() >= 0.0f).select(rg.array(), -rg.array()));
  defop1g(vec4, vec4, r = a.array().abs(), ag.array() += (a.array() >= 0.0f).select(rg.array(), -rg.array()));
  defop1r(mat, mat, true, allocResult(args[0].t), r = a.array().abs());
  return setInvalidArgs();
}

defusage(abs, R"(
  abs(F|CF)
  abs(vec|mat) -- componentwise)");


deftype(sign)
{
  return eval(n->ch(0));
}

defemit(sign)
{
  defop1g(F, F, r = copysign(1.0f, a), ag += rg * op_nearzero(cpu, a));
  defop1(vec2, vec2, r = a.unaryExpr([](float x) { return copysign(1.0f, x); }));
  defop1(vec3, vec3, r = a.unaryExpr([](float x) { return copysign(1.0f, x); }));
  defop1(vec4, vec4, r = a.unaryExpr([](float x) { return copysign(1.0f, x); }));
  defop1(mat2, mat2, r = a.unaryExpr([](float x) { return copysign(1.0f, x); }));
  defop1(mat3, mat3, r = a.unaryExpr([](float x) { return copysign(1.0f, x); }));
  defop1(mat4, mat4, r = a.unaryExpr([](float x) { return copysign(1.0f, x); }));
  defop1r(mat, mat, true, allocResult(args[0].t), r = a.unaryExpr([](float x) { return copysign(1.0f, x); }));
  return setInvalidArgs();
}

defusage(sign, R"(
  sign(F)
  sign(vec|mat) -- componentwise)");


deftype(floor)
{
  return eval(n->ch(0));
}

defemit(floor)
{
  defop1_simd(F, F, r = floor(a));
  defop1(vec2, vec2, r = a.array().floor());
  defop1(vec3, vec3, r = a.array().floor());
  defop1(vec4, vec4, r = a.array().floor());
  defop1(mat, mat, r = a.array().floor());
  return setInvalidArgs();
}

defusage(floor, R"(
  floor(F)
  floor(vec|mat) -- componentwise)");


deftype(ceil)
{
  return eval(n->ch(0));
}

defemit(ceil)
{
  defop1_simd(F, F, r = ceil(a));
  defop1(vec2, vec2, r = a.array().ceil());
  defop1(vec3, vec3, r = a.array().ceil());
  defop1(vec4, vec4, r = a.array().ceil());
  defop1r(mat, mat, true, allocResult(args[0].t), r = a.array().ceil());
  return setInvalidArgs();
}

defusage(ceil, R"(
  ceil(F)
  ceil(vec|mat) -- componentwise)");


deftype(round)
{
  return eval(n->ch(0));
}

defemit(round)
{
  defop1_simd(F, F, r = round(a));
  defop1(vec2, vec2, r = a.array().round());
  defop1(vec3, vec3, r = a.array().round());
  defop1(vec4, vec4, r = a.array().round());
  defop1(mat, mat, r = a.array().round());
  return setInvalidArgs();
}

defusage(round, R"(
  round(F)
  round(vec|mat) -- componentwise)");


deftype(fract)
{
  return eval(n->ch(0));
}

defemit(fract)
{
  defop1_simd(F, F, r = a - floor(a));
  defop1(vec2, vec2, r = a.array() - a.array().floor());
  defop1(vec3, vec3, r = a.array() - a.array().floor());
  defop1(vec4, vec4, r = a.array() - a.array().floor());
  defop1r(mat, mat, true, allocResult(args[0].t), r = a.array() - a.array().floor());
  return setInvalidArgs();
}

defusage(fract, R"(
  fract(F)
  fract(vec|mat) -- componentwise)");


deftype(max)
{
  return eval(n->ch(0));
}

defemit(max)
{
  defop2g_simd(F, F, F, r = a.max(b), (ag += (a >= b).select(rg, 0), bg += (b >= a).select(rg, 0) ));
  defop2(vec2, vec2, vec2, r = a.array().max(b.array()));
  defop2(vec3, vec3, vec3, r = a.array().max(b.array()));
  defop2(vec4, vec4, vec4, r = a.array().max(b.array()));
  defop2r(mat, mat, mat, args[0].t == args[1].t, allocResult(args[0].t), r = a.array().max(b.array()));
  return setInvalidArgs();
}

defusage(max, R"(
  max(F, F)
  max(vec|mat, same) -- componentwise)");


deftype(min)
{
  return eval(n->ch(0));
}

defemit(min)
{
  defop2g(F, F, F, r = min(a, b), (ag += a <= b ? rg : 0.0f, bg += b <= a ? rg : 0.0f));
  defop2(vec2, vec2, vec2, r = a.array().min(b.array()));
  defop2(vec3, vec3, vec3, r = a.array().min(b.array()));
  defop2(vec4, vec4, vec4, r = a.array().min(b.array()));
  defop2r(mat, mat, mat, args[0].t == args[1].t, allocResult(args[0].t), r = a.array().min(b.array()));
  return setInvalidArgs();
}

defusage(min, R"(
  min(F, F)
  min(vec, vec) -- componentwise
  min(mat, mat) -- componentwise)");


deftype(clamp)
{
  return eval(n->ch(0));
}

defemit(clamp)
{
  defop3g(F, F, F, F, r = clamp(a, b, c), (ag += (a > b && a < c) ? rg : 0.0f));
  defop3(vec2, vec2, vec2, vec2, r = a.cwiseMax(b).cwiseMin(c));
  defop3(vec3, vec3, vec3, vec3, r = a.cwiseMax(b).cwiseMin(c));
  defop3(vec4, vec4, vec4, vec4, r = a.cwiseMax(b).cwiseMin(c));
  defop3r(mat, F, F, mat, true, allocResult(args[0].t), (r = a.cwiseMax(b).cwiseMin(c)));
  return setInvalidArgs();
}

defusage(clamp, R"(
  clamp(x ∊ F, lo ∊ F, hi ∊ F)
  clamp(x ∊ vec|mat, lo ∊ F, hi ∊ F) -- componentwise)");


deftype(relu)
{
  return eval(n->ch(0));
}

defemit(relu)
{
  defop1g(F, F, r = max(a, 0.0f), ag += (a >= 0) ? rg : 0.0f);
  defop1g(vec2, vec2, r = a.cwiseMax(0.0f), ag.array() += (a.array() >= 0).select(rg.array(), 0.0f));
  defop1g(vec3, vec3, r = a.cwiseMax(0.0f), ag.array() += (a.array() >= 0).select(rg.array(), 0.0f));
  defop1g(vec4, vec4, r = a.cwiseMax(0.0f), ag.array() += (a.array() >= 0).select(rg.array(), 0.0f));
  defop1rg(mat, mat, true, allocResult(args[0].t), r = a.cwiseMax(0.0f), ag.array() += (a.array() >= 0).select(rg.array(), 0.0f));
  return setInvalidArgs();
}

defusage(relu, R"(
  relu(x ∊ F) -- max(0,x)
  relu(x ∊ vec|mat) -- componentwise)");
