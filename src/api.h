#pragma once
#include "./defs.h"
#include "./types.h"
#include "./uipoll.h"
#include "./cellvals.h"
#include "./boxed.h"
#include "./yamlio.h"
#include <mutex>
#include <shared_mutex>

constexpr int API_KBD_NKEYS = 128;

/*
  A SheetApi defines an interface between the spreadsheet and live or simulated hardware.
  You should subclass SheetApi to define an interface a new type of hardware.
  (Maybe it should have been called Sheet-Hardware Interface, or SHI instead of API)

  A subclass of SheetApi contains a bunch of members of type ApiRef<T, DIR>,
  where T is the type (like float) and DIR is an ApiDirection, like API_SENSOR
  or API_ACTUATOR. Each ApiRef has a name, which connects it to a cell in the sheet
  with the same name. So if a subclass of SheetApi has a member declared as:
  ```
    ApiRef<float, API_SENSOR> senseFoo {*this, "sense.foo"}
  ```
  it means that any spreadsheet cell named sense.foo will have its value overwritten
  by the hardware on every step. Inside the SheetApi subclass, there'll be code to
  set it from the hardware, like
  ```
    senseFoo.replayVals(buffer) = ...hardware sensor data received over network...
  ```
  The variable `buffer`, a `Blob`, goes 2 places. First, it's copied in real time
  into the `CellVals` structure for the sheet. Second, it's written to the replay
  database to allow re-running the sheet with the same inputs, typically with different
  parameter values or formulas or some such change you want to see the effects of.

  API_MONITOR is slightly different from API_SENSOR -- it's used for a video
  camera watching the robot. One difference is that API_MONITOR values can arrive
  late without delaying the real-time processing.

  Once an API is bound (which resolves names to cell & value offsets) you can query
  `SheetApi::directionByCell(cell)` to get the ApiDirection (or API_NONE).

  You can have multiple APIs for a sheet, although I haven't actually tested this.
  It might be useful when controlling two robots in coordination.
*/

enum ApiDirection {
  API_NONE,
  API_SENSOR, // an input from the hardware to the sheet
  API_ACTUATOR, // an output from the sheet to the hardware
  API_MONITOR,   // for example, a camera watching the robot that's not used at runtime, just for later analysis
  API_REPLAY, // for example, an integral feedback term in a PID loop. Not implemented yet
  API_SECTION // placeholder for display in the UI
};

ApiDirection apiDirectionByName(string const &name);
string apiDirectionName(ApiDirection a);

ostream & operator <<(ostream &s, ApiDirection const &a);

struct ReplayBuffer {

  R startTime = 0.0;

  map<string, vector<Blob>> replayByApiInstance;

  // Prevent eg adding to the vector<Blob>s while reading them
  std::shared_mutex replayMutex;

  ReplayBuffer();
  ~ReplayBuffer();

  bool empty() const;

};

struct SheetApi;
struct LiveRobotState;
struct LiveRobotTicker;

struct GenericApiRef {

  SheetApi &owner;
  string name;
  string comment;
  string error;
  BoxedRef br;
  ApiDirection direction;
  ValueOffset replayValueOffset;

  GenericApiRef(SheetApi &_owner, string const &_name, TypeTag t, ApiDirection _direction);
  virtual ~GenericApiRef();

  // Not legal to copy or move, because construction adds a pointer to this into *owner
  GenericApiRef(GenericApiRef const &other) = delete;
  GenericApiRef(GenericApiRef &&other) = delete;
  GenericApiRef & operator = (GenericApiRef &other) = delete;

  void bindValue(SheetApi &owner);
  void logError(string const &s);
  virtual void copyToReplayBuffer(CellVals *ref, Blob &buffer) const = 0;
  virtual void copyFromReplayBuffer(CellVals *ref, Blob &buffer) const = 0;
  bool valid();

};

template<typename T, ApiDirection DIR> struct ApiRef;

struct ApiDbSchemaEnt {
  ValueOffset replayValueOffset = nullValueOffset;
  TypeTag type;
  ApiDirection direction{API_NONE};
  int size = 0;
  int version = 0;
  bool used = false;
};

struct ApiSchemaConv {

  SheetApi &api;
  YAML::Node dbSchema;
  string dbVersion;

  vector<function<void(U8 *toMem, U8 const *fromDb)>> fns;
  bool ok = false;
  ostringstream notes;
  map<string, optional<ApiDbSchemaEnt>> dbEntries;

  ApiSchemaConv(SheetApi &_api, YAML::Node const &_dbSchema, string const &_dbVersion);

  Blob run(U8 const *fromDbData, size_t fromDbSize);
  void setup();

};

struct ApiSpec {
  string apiType;
  string apiInstance;
  shared_ptr<AssocValue> apiProps;
  CellIndex cell = nullCellIndex;
};

/*
  Subclass this for each kind of robot hardware
*/
struct SheetApi : enable_shared_from_this<SheetApi>, AllocTrackingMixin<SheetApi> {

  CompiledSheet &compiled;
  Sheet &sh;
  string apiType;
  string apiInstance;
  string label;

  string apiVersion;
  shared_ptr<AssocValue> apiProps;
  CellIndex apiCell;

  double hwTimeBase = 0.0;

  vector<GenericApiRef *> refs;
  vector<shared_ptr<GenericApiRef>> dynamicRefs;
  vector<string> errors;
  ValueOffset replayBufferSize = 0;
  int verbose = 0;
  Blob lastData;  
  shared_ptr<set<BlobRef>> chunks;

  explicit SheetApi(CompiledSheet &_compiled, ApiSpec const &_spec, string _apiVersion);
  virtual ~SheetApi();
  virtual void bindApi();
  void logError(string_view s);
  bool ok();

  SyncLogger ll() {
    return SyncLogger(label);
  }

  // return true to demand more warmup/cooldown
  virtual void txWarmup(LiveRobotState &lrt);
  virtual void txCooldown(LiveRobotState &lrt);

  virtual void txActuators(CellVals *vals, LiveRobotState &lrt);
  virtual void rxSensors(LiveRobotState &lrt);
  virtual void rxMonitors(LiveRobotState &lrt);

  virtual void doInteractive(ReplayBuffer &buffer, vector<int> const &doAdjustParams, int visCursIndex, bool shift);
  virtual bool startLive();
  virtual void stopLive();

  virtual void simAdvance(CellVals *prev, CellVals *next, Trace *tr, int bi);

  YAML::Node getSchema();
  virtual void finalizeConv(ApiSchemaConv &conv);

  virtual void setKbd(std::array<bool, API_KBD_NKEYS> const &_down);

  void copyToReplayBuffer(Trace &tr, ReplayBuffer &replay, bool doSensors=true, bool doActuators=true);
  void copyFromReplayBuffer(Trace &tr, ReplayBuffer &replay, bool doSensors=true, bool doActuators=true);
  void copyToReplayBuffer(CellVals *ref, Blob &buffer, bool doSensors=true, bool doActuators=true);
  void copyFromReplayBuffer(CellVals *ref, Blob const &buffer, bool doSensors=true, bool doActuators=true);

  template<typename T, ApiDirection DIR>
  ApiRef<T, DIR> *mkDynamic(string const &name)
  {
    auto ref = make_shared<ApiRef<T, DIR>>(*this, name);
    dynamicRefs.push_back(ref);
    return ref.get();
  }

};

struct ApiSection : GenericApiRef {

  ApiSection(SheetApi &owner, string const &_name)
    : GenericApiRef(owner, _name, TypeTag(), API_SECTION)
  {
  }
  virtual ~ApiSection()
  {
  }

  void copyToReplayBuffer(CellVals *ref, Blob &buffer) const override {};
  void copyFromReplayBuffer(CellVals *ref, Blob &buffer) const override {};

};

template<typename T, ApiDirection DIR>
struct ApiRef : GenericApiRef {
  
  ApiRef(SheetApi &owner, string const &_name)
    : GenericApiRef(owner, _name, typetag<T>(), DIR)
  {
  }
  virtual ~ApiRef()
  {

  }
  void copyToReplayBuffer(CellVals *ref, Blob &buffer) const override
  {
    replayVal(buffer) = memVal(ref);
  }
  void copyFromReplayBuffer(CellVals *ref, Blob &buffer) const override
  {
    memVal(ref) = replayVal(buffer);
  }

  /*
    Access the associated value in a CellVals or Blob.
    An ApiRef can be invalid, which happens when there's no cell in the
    spreadsheet with the given name. In that case, memVal and replayVal return
    a references to a static variable of the right type, recently zeroed.
    This has some potential for error.
    Somehow I like having memVal & replayVal be separate rather than overloaded.
    Typically you use memVal in a tx function (sending actuator values to hw)
    and replayVal in an rx function (receiving sensor values from hw).
  */
  inline typename up_t<T>::t memVal(CellVals *ref) const
  {
    if (!br.valid()) {
      return getZeroUp<T>(br.t);
    }
    return br.get<T>(ref);
  }

  inline typename rd_t<T>::t memVal(CellVals const *ref) const
  {
    if (!br.valid()) {
      return getZeroRd<T>(br.t);
    }
    return br.get<T>(ref);
  }

  inline T &replayVal(Blob &buffer) const
  {
    assert (replayValueOffset != nullValueOffset);
    return *(T *)&buffer[replayValueOffset];
  }
};


struct ApiRegister {

  char const *type;
  function<shared_ptr<SheetApi> (CompiledSheet &_compiled, ApiSpec const &_spec)> createApi;

  static map<string, ApiRegister *> *registryByType;
  
  ApiRegister(char const *_type, function<shared_ptr<SheetApi>(CompiledSheet &_compiled, ApiSpec const &_spec)> _createApi);

  static shared_ptr<SheetApi> mk(CompiledSheet &_compiled, ApiSpec const &_spec);
};


struct LiveRobotState {
  
  enum State {
    LRS_idle,
    LRS_warmup,
    LRS_running,
    LRS_cooldown,
    LRS_done,
    LRS_emergency,
    LRS_error,
  } state{LRS_idle};

  double hwTimeBase = 0.0, hwTimeVal = 0.0;
  int logcnt = 0; // for periodic logging
  int warmupTicks = 0, cooldownTicks = 0;
  vector<string> needMoreWarmup;
  vector<string> needMoreCooldown;

  void setState(State _state) {
    state = _state;
  }

  void pleaseStopSoft() {
    if (state == LRS_running) {
      setState(LRS_cooldown);
    }
    else if (state == LRS_warmup) {
      setState(LRS_done);
    }
  }

  void pleaseStopHard() {
    if (state == LRS_running || state == LRS_warmup || state == LRS_cooldown) {
      setState(LRS_emergency);
    }
  }

  void pleaseStopFromUi() {
    if (state == LRS_running) {
      setState(LRS_cooldown);
    }
    else if (state == LRS_cooldown) {
      setState(LRS_emergency);
    }
    else if (state == LRS_warmup) {
      setState(LRS_done);
    }
  }

};


ostream & operator <<(ostream &s, LiveRobotState::State const &a);


static constexpr U32 nullApiId = U32(-1);

bool checkSchemaMatch(YAML::Node const &localApiSchemaYaml, YAML::Node const &remoteApiSchemaYaml, ostream &l);



