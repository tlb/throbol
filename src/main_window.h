#pragma once

#include "../geom/gl_headers.h"
#include "../geom/texcache.h"
#include "../geom/g8.h"
#include "./core.h"
#include "./uipoll.h"
#include <stdio.h>

struct MainUi;

enum UiStyle {
  UiLight,
  UiDark,
  UiClassic,
};

struct GLFWwindow;

extern ImGuiContext *imguiContext;

struct MainWindow : UiPollable, enable_shared_from_this<MainWindow> {

  GLFWwindow *glfw_window = nullptr;

  WGPUDevice wgpu_device = nullptr;
  WGPUQueue wgpu_queue = nullptr;
  WGPUInstance wgpu_instance = nullptr;
  WGPUAdapter wgpu_adapter = nullptr;
#if defined(__EMSCRIPTEN__)
#else
  WGPUSurface wgpu_surface = nullptr;
  WGPUSwapChain wgpu_swap_chain = nullptr;
#endif
  WGPUTexture wgpu_depth_tex = nullptr;
  WGPUTexture wgpu_render_tex = nullptr;
  WGPUTextureView wgpu_depth_tex_view = nullptr;
  int wgpu_swap_chain_width = 0;
  int wgpu_swap_chain_height = 0;

  vector<G8ResourcesPerShaderWindow> g8ResourcesPerShaderWindow; // Indexed by shader->shaderIndex
  G8ResourcesPerWindow g8ResourcesPerWindow;

  NavPath path;

  bool tweakForPodcast = false;
  bool showDemoWindow = false;
  bool showMetricsWindow = false;
  bool wantClose = false;
  bool showCompilerOutputWindow = false;

  int logEvents = 0;
  int64_t loopCount = 0;
  double lastLoopTime = 0.0;
  float screenDpi = 150.0;

#ifdef wgpu_notyet
  SDL_AudioSpec audioSpec{};
  int audioDevice = 0;
#endif

  //TexCache imgTextures {*this};

  UiStyle uiStyle = UiDark;
  ImVec4 clearColor;

  MainWindow();
  virtual ~MainWindow();

#ifdef wgpu_notyet
  void handleEvent(SDL_Event &event);
  void logEvent(SDL_Event &event);
#endif
  UiPollResult uiPoll() override;
  void uiRedraw() override;
  void uiRender1();
  void uiRender2();
  void uiAutosave() override;
  void drawWindowLineq();
  void requestClose();
  void teardown();
  void setupAudio();
  void setup();
  void setupWindow();
  void setupGlLoader();
  void setupDisplayScale();
  void setupStyle();
  void setupFonts();
  void setupIcon();
  void setUi(shared_ptr<MainUi> _ui);
  void printWebgpuStats(ostream &s);

  shared_ptr<MainUi> ui;
};

struct MainUi
{
  MainUi(MainWindow &_win);
  virtual ~MainUi();

  MainWindow &win;

  virtual void setupMainUi() = 0;
  virtual void initGlobalDo() = 0;
  virtual void execGlobalDoPost() = 0;
  virtual void drawBackground() = 0;
  virtual void drawMainUi() = 0;
  virtual void renderBackground(WGPURenderPassEncoder wgpu_pass) = 0;
  virtual bool loadDocument(vector<string> fns) = 0;
  virtual void pleaseClose();
  virtual void uiAutosave();
  virtual void prepareDraw();
};

extern shared_ptr<MainWindow> mainWindow0;
