#include "./test_utils.h"

using namespace sqlite;


TEST_CASE("DB Table creation", "[db]")
{
  const char *dbfn = "/tmp/test-trace.db";
  unlink(dbfn);

  Sheet sh;
  REQUIRE(mkSheetCheck(sh, R"(
---
tickCount: 20
sense.pos: 0
bot.force: (targ.pos - bot.pos) * 3~10
targ.pos: sin(time*10)-0.5
bot: |
  api.test()
)", "test_trace_db"));

  sh.openTraceDb(dbfn);

  auto trace = make_shared<Trace>(sh.compiled, false, false);
  trace->setupInitial();
  simulateGroup({trace});

  auto rb = trace->extractReplayBuffer();
}
