#pragma once
#include "./uipoll.h"

#if !defined(__EMSCRIPTEN__)

#define BOOST_ASIO_NO_DEPRECATED 1

#include <boost/asio.hpp>
namespace asio = boost::asio;
extern asio::io_context &io_context;


#endif

