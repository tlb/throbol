#pragma once
#include "./draw.h"
#include "../geom/shaders.h"

struct DrawTraceContext;

struct DrawTraceAccum {

  struct PlotPt {

    F x, y, z;
    U32 timeIndex;

    PlotPt(F _x, F _y, F _z, U32 _timeIndex)
      :x(_x), y(_y), z(_z), timeIndex(_timeIndex)
    {
    }
  };


  struct Cmd {

    mat4 const *posx;
    mat4 const *colorx;
    F layer;
    F thicken;
    U32 index;
    Assocp options;
    DrawOpHandle drawOp;

    Cmd(mat4 const *_posx, mat4 const *_colorx, F _layer, F _thicken, U32 _index, Assocp _options, DrawOp *_drawOp)
      : posx(_posx),
        colorx(_colorx),
        layer(_layer),
        thicken(_thicken),
        index(_index),
        options(_options),
        drawOp(_drawOp)
    {
    }

    vec4 getColor(vec4 dflt = vec4(1,1,1,1))
    {
      if (auto it = assoc_vec4(options, "color")) {
        return *colorx * *it;
      }
      return *colorx * dflt;
    }

    F getThickness(F dflt)
    {
      if (auto it = assoc_F(options, "thickness")) {
        return thicken * *it;
      }
      return thicken * dflt;
    }

    vec4 getPlotColor(int offset = 0)
    {
      if (auto it = assoc_vec4(options, "color")) {
        return *it;
      }
      return goodGraphColor(index + offset);
    }

    vec3 tPos(vec3 const &a)
    {
      return (*posx * a.homogeneous()).hnormalized();
    }

    vec3 tDir(vec3 const &a)
    {
      return posx->topLeftCorner<3, 3>() * a;
    }

    vec4 tColor(vec4 const &a)
    {
      return *colorx * a;
    }

  };

  G8DrawList &dl;
  Sheet &sh;
  Trace &tr;
  int numSeedsToAverage = 1;
  CellIndex cell = nullCellIndex;
  bool cellPicked = false; // We should contribute to the pickItem popup menu
  vec3 tubeAxis{0,0,0};

  // Correct while adding things, but not while running drawOpFinal
  int timeIndex = 0;
  U32 accumIndex = 0;

  F maxWidth = 5.0, maxHeight = 5.0;

  map<tuple<U32, U32>, vector<PlotPt>> plotPts_;

  Arena dtalife;

  vector<Cmd> separateCmds;

  const F figScale = 2.0f;
  const F fontSize = 0.15;


  DrawTraceAccum(G8DrawList &_dl, Sheet &_sh, Trace &_tr, CellIndex _cell, bool _cellPicked);
  ~DrawTraceAccum();

  vector<PlotPt> &plotPts(U32 a, U32 b) {
    return plotPts_[make_tuple(a,b)];
  }

  // Scale up so nominal X and Y sizes are [-2 .. +2]

  void addRange(BoxedRef br, F alpha, SheetLayoutCell const &lo);

  void addTree(DrawOpHandle opgen,
    mat4 const *posx, mat4 const *colorx,
    Assocp options,
    F offCursor, F visRange, F thicken,
    bool &onlyCursTime);

  void finish();

  void drawOpFinal(Cmd &it, DrawOpLinePlot *op);
  void drawOpFinal(Cmd &it, DrawOpBandPlot *op);
  void drawOpFinal(Cmd &it, DrawOpScatterPlot *op);
  void drawOpFinal(Cmd &it, DrawOpSpline *op);
  void drawOpFinal(Cmd &it, DrawOpRect *op);
  void drawOpFinal(Cmd &it, DrawOpDisk *op);
  void drawOpFinal(Cmd &it, DrawOpTube *op);
  void drawOpFinal(Cmd &it, DrawOpArrow *op);
  void drawOpFinal(Cmd &it, DrawOpArcArrow *op);
  void drawOpFinal(Cmd &it, DrawOpSphere *op);
  void drawOpFinal(Cmd &it, DrawOpDial *op);
  void drawOpFinal(Cmd &it, DrawOpText *op);
  void drawOpFinal(Cmd &it, DrawOpEpicycles *op);
  void drawOpFinal(Cmd &it, DrawOpLine *op);
  void drawOpFinal(Cmd &it, DrawOpLines *op);
  void drawOpFinal(Cmd &it, DrawOpSolid *op);
  void drawOpFinal(Cmd &it, DrawOpAxisArrows *op);
  void drawOpFinal(Cmd &it, DrawOpParticleSearch *op);
  void drawOpFinal(Cmd &it, DrawOpMinpackSearch *op);
  void drawOpFinal(Cmd &it, DrawOpPolicyTerrain *op);
  void drawOpFinal(Cmd &it, DrawOpMujocoScene *op);
  // ADD DrawOp

};

void drawDialStd(G8DrawList &dl,
  vec3 dialc, vec3 dialx, vec3 dialy, mat4 const &colorx,
  F value, F range, DistType dist,
  PolicyTerrainMap *ptm);

void drawDialLabel(G8DrawList &dl,
  vec3 dialc, vec3 dialx, vec3 dialy, mat4 const &colorx,
  F value, F range, DistType dist,
  string_view label);

void drawSquiggle(G8DrawList &dl, Sheet &sh,
  vec3 const &pos,
  PolicyTerrainMap *ptm, bool exact, CellVals *rangeByVal);

void drawBlownupSquiggle(G8DrawList &dl, Sheet &sh,
  vec3 const &pos,
  PolicyTerrainMap *ptm, bool exact, CellVals *rangeByVal);

extern struct G8Shader<G8Vtx_pncl> terrainShader;
