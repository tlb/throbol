#include "./core.h"
#include "./sheet_ui.h"
#include "./uipoll.h"
#include "imgui.h"
#include "./config.h"
#include <thread>
#include "GLFW/glfw3.h"
#if defined(__EMSCRIPTEN__)
  #include <emscripten.h>
  #include <emscripten/html5.h>
  #include <emscripten/html5_webgpu.h>
#else
  #error "This file should only be used with Emscripten"
#endif

extern "C"
int tbOpenSheetWin(int argc, char * const *argv)
{
  glfwInit();
  simulateStartup();
  vector<string> pageFileNames;

  for (int i = 0; i < argc; i++) {
    pageFileNames.push_back(argv[i]);
  }

  mainWindow0 = make_shared<MainWindow>();
  mainWindow0->setup();
  panel3dInit();

  auto ui = make_shared<SheetUi>(*mainWindow0);
  if (ui->loadDocument(pageFileNames)) {
    mainWindow0->setUi(ui);
  }

  startUiPoll(mainWindow0);

  return 0;
}

extern "C"
int tbQuit()
{
  if (mainWindow0) {
    mainWindow0->wantClose = true;
    mainWindow0 = nullptr;
  }
  simulateShutdown();
  return 0;
}

extern "C"
int tbStyleChanged()
{
  if (mainWindow0) {
    mainWindow0->setupStyle();
    uiPollLive = true;
  }
  return 0;
}

extern "C"
int main(int argc, char * const *argv)
{
  setupPaths(nullptr);
  glfwInit();
  simulateStartup();
  return 0;
}
