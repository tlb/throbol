#include "./core.h"
#include "./uipoll.h"

#if defined(__EMSCRIPTEN__)
#include <emscripten.h>
#include <emscripten/fetch.h>
#endif

PageIndex Sheet::mkPage(string const &fn, vec2 const &includePos, PageIndex includeParent)
{
  auto page = nPages;
  assert(fileNameByPage.size() == size_t(nPages));
  assert(includeNameByPage.size() == size_t(nPages));
  assert(yamlByPage.size() == size_t(nPages));
  assert(includePosByPage.size() == size_t(nPages));
  assert(includeParentByPage.size() == size_t(nPages));

  fileNameByPage.push_back(fn);
  includeNameByPage.push_back(fn);
  yamlByPage.emplace_back();
  includePosByPage.push_back(includePos);
  includeParentByPage.push_back(includeParent);
  nPages++;
  return page;
}

static bool isUrl(string const &fn)
{
  return startsWith(fn, "http://") || startsWith(fn, "https://");
}

optional<string> readFileOrUrl(string const &fn)
{
  if (isUrl(fn)) {
#if defined(__EMSCRIPTEN__)
    emscripten_fetch_attr_t attr;
    emscripten_fetch_attr_init(&attr);
    strcpy(attr.requestMethod, "GET");
    attr.attributes = EMSCRIPTEN_FETCH_LOAD_TO_MEMORY | EMSCRIPTEN_FETCH_SYNCHRONOUS;
    emscripten_fetch_t *fetch = emscripten_fetch(&attr, fn.c_str());
    if (fetch->status == 200) {
      W() << "Loaded " << fetch->numBytes << " bytes from " << fn;
      string ret(fetch->data, fetch->numBytes);
      emscripten_fetch_close(fetch);
      return ret;
    }
    Wred() << fn << ": failed. http_status=" << fetch->status;
    emscripten_fetch_close(fetch);
    return nullopt;
#else
    Wred() << fn << ": failed: no http";
    return nullopt;
#endif
  }
  else {

    int fd = open(fn.c_str(), O_RDONLY, 0);
    if (fd < 0) {
      Wred() << "loadPage: " << fn << ": " << strerror(errno);
      return nullopt;
    }
    string pageData = readFile(fd, fn);
    if (close(fd) < 0) {
      Wred() << "loadPage: closing " << fn << ": " << strerror(errno);
      return nullopt;
    }
    return pageData;
  }
}

bool writeFileOrUrl(string const &fn, string const &pageData)
{
  if (isUrl(fn)) {
#if defined(__EMSCRIPTEN__)
    emscripten_fetch_attr_t attr;
    emscripten_fetch_attr_init(&attr);
    strcpy(attr.requestMethod, "PUT");
    attr.attributes = EMSCRIPTEN_FETCH_LOAD_TO_MEMORY | EMSCRIPTEN_FETCH_SYNCHRONOUS;
    attr.requestData = pageData.c_str();
    attr.requestDataSize = pageData.size();
    emscripten_fetch_t *fetch = emscripten_fetch(&attr, fn.c_str());
    if (fetch->status == 200) {
      W() << "Saved " << pageData.size() << " bytes to " << fn;
      emscripten_fetch_close(fetch);
      return true;
    }
    Wred() << fn << ": failed. http status=" << fetch->status << "\n" << string(fetch->data, fetch->numBytes);
    emscripten_fetch_close(fetch);
    return false;
#else
    Wred() << fn << ": failed: no http";
    return false;
#endif
  }
  else {
    writeFile(fn, pageData);
    return true;
  }
}

bool Sheet::loadPage(string const &fn, vec2 const &includePos, PageIndex includeParent, deque<Sheet::IncludeRec> &moreFiles)
{
  if (auto pageData = readFileOrUrl(fn)) {
    auto page = mkPage(fn, includePos, includeParent);
    return undumpPage(*pageData, page, moreFiles, fn);
  }
  return false;
}

// For testing
bool Sheet::undumpPage(string const &pageData, PageIndex page)
{
  deque<IncludeRec> moreFiles;
  string fn = "?";
  return undumpPage(pageData, page, moreFiles, fn);
}

static vec2 undumpPos(YAML::Node yaml)
{
  auto posSplit = splitChar(yaml.as<string>(), ' ');
  if (posSplit.size() >= 2) {
    return vec2(stod(posSplit[0]), stod(posSplit[1]));
  }
  return vec2(0, 0);
}

static YAML::Node dumpPos(vec2 a)
{
  return YAML::Node(repr(a.x()) + " " + repr(a.y()));
}

bool Sheet::undumpPage(string const &pageData, PageIndex page, deque<IncludeRec> &moreFiles, string const &fn)
{
  vector<YAML::Node> pageYamls;
  try {
    pageYamls = YAML::LoadAll(pageData);
  }
  catch (YAML::ParserException &e) {
    Wred() << fn << ": " << e.what();
    return false;
  }
  YAML::Node hdrYaml, formulasYaml;
  if (pageYamls.size() >= 1) {
    hdrYaml = pageYamls[0];
    yamlByPage[page] = hdrYaml;
  }
  if (pageYamls.size() >= 2) {
    formulasYaml = pageYamls[1];
    if (!formulasYaml.IsMap()) {
      Wred() << fn << ": formulas should be a map of cell name => formulas";
      return false;
    }
    auto possYaml = hdrYaml["positions"];

    vec2 curPos(0, 0);
    for (auto formulaIt : formulasYaml) {
      auto cellName = formulaIt.first.as<string>();
      if (possYaml.IsMap()) {
        if (auto posYaml = possYaml[cellName]) {
          if (!posYaml.IsScalar()) {
            Wred() << fn << ": " << cellName << ": pos should be a string";
            return false;
          }
          curPos = undumpPos(posYaml);
        }
      }

      auto cellFormulaYaml = formulaIt.second;
      if (!cellFormulaYaml.IsScalar()) {
        Wred() << fn << ": " << cellName << ": formula should be a string";
        return false;
      }
      string cellFormula = cellFormulaYaml.as<string>();
      // For cells written in Literal mode, yaml-cpp seems to add trailing newlines.
      while (!cellFormula.empty() && cellFormula.back() == '\n') {
        cellFormula.pop_back();
      }
      
      CellIndex rxCell = mkCell(cellName);
      if (rxCell == nullCellIndex) {
        Wred() << fn << ": Invalid cell name " << shellEscape(cellName);
      }
      else {
        setCellPage(rxCell, page);
        setFormula(rxCell, cellFormula);
        setCellPos(rxCell, curPos + includePosByPage[page]);
        curPos += vec2(0, -1);
      }
    }
  }

  if (auto includesYaml = hdrYaml["include"]) {
    if (fn != "?") { // used by daemon when loading
      auto fnDir = myDirname(fn);
      for (auto it : includesYaml) {
        vec2 includePos(0, 0);
        string includeFn;
        if (it.IsScalar()) {
          includeFn = it.as<string>();
        }
        else if (it.IsMap()) {
          includeFn = it["fn"].as<string>();
          includePos = undumpPos(it["pos"]);
        }
        string includeFnAbs = includeFn;
        if (!startsWith(includeFn, "/")) {
          includeFnAbs = fnDir + "/" + includeFn;
        }
        includeFnAbs = myRealpath(includeFnAbs);
        if (!any_of(moreFiles.begin(), moreFiles.end(), [&includeFnAbs](auto const &a) {
            return a.includeFnAbs == includeFnAbs;
          })) {
          moreFiles.push_back(IncludeRec{
            includeFnAbs,
            includePos,
            page,
          });
        }
      }
    }
  }

  return true;
}

vector<tuple<CellIndex, vec2>> Sheet::getSortedCells(PageIndex page)
{
  vector<tuple<CellIndex, vec2>> todo;
  for (auto cell : liveCells) {
    if (pageByCell[cell] == page) {
      vec2 relPos = posByCell[cell] - includePosByPage[page];
      todo.emplace_back(cell, relPos);
    }
  }

  sort(todo.begin(), todo.end(), [](auto const &a, auto const &b) {
    if (get<1>(a).x() < get<1>(b).x()) return true; // increasing in X
    if (get<1>(b).x() < get<1>(a).x()) return false;
    if (get<1>(b).y() < get<1>(a).y()) return true; // decreasing in Y
    if (get<1>(a).y() < get<1>(b).y()) return false;
    return get<0>(a) < get<0>(b); // tiebreaker: increasing in cell
  });

  return todo;
}


void Sheet::updatePagesYamlFromSheet()
{
  assert(yamlByPage.size() == nPages);
  for (PageIndex page = 0; page < nPages; page++) {
    auto &pageYaml = yamlByPage[page];

    auto todo = getSortedCells(page);

    YAML::Node possYaml;

    for (auto &[cell, cellPos] : todo) {
      auto &cellName = nameByCell[cell];
      possYaml[cellName] = dumpPos(cellPos); 
    }
    pageYaml["positions"] = possYaml;

    auto fnDir = myDirname(fileNameByPage[page]);
    if (auto includesYaml = pageYaml["include"]) {
      YAML::Node newIncludesYaml;
      for (auto it : includesYaml) {

        vec2 includePos(0, 0);
        string includeFn;
        if (it.IsScalar()) {
          includeFn = it.as<string>();
        }
        else if (it.IsMap()) {
          includeFn = it["fn"].as<string>();
          includePos = undumpPos(it["pos"]);
        }

        string includeFnAbs = includeFn;
        if (!startsWith(includeFn, "/")) {
          includeFnAbs = fnDir + "/" + includeFn;
        }
        includeFnAbs = myRealpath(includeFnAbs);

        // Find the page by matching absolute path name.
        // Not sure this is 100% correct with duplicate includes
        for (PageIndex p = 0; p < nPages; p++) {
          if (fileNameByPage[p] == includeFnAbs) {
            if (0) L() << "Set pos for " << includeFn << " = " << includePosByPage[p];
            includePos = includePosByPage[p];
          }
        }
        YAML::Node ent;
        ent["fn"] = includeFn;
        ent["pos"] = dumpPos(includePos);
        newIncludesYaml.push_back(ent);
      }
      pageYaml["include"] = newIncludesYaml;
    }
  }
}

void Sheet::updateSheetFromPagesYaml()
{
  for (int page = nPages-1; page >= 0; page--) {
    auto pageYaml = yamlByPage[page];
  }
}

static bool shouldUseLiteral(string const &s)
{
  /*
    For ease of hand-editing, choose YAML literal syntax except for 
    short, simple formulas with no funky characters.

      complex.cell: |
        sin(x) + 2*cos(y) +
        tan(z)
      simple.cell: 0.5~1
    
    See https://www.yaml.info/learn/quote.html
  */
  if (s.size() > 30) return true;
  if (s.size() >= 2 && (s[0] == '-' || s[0] == ':' || s[0] == '?') && s[1] == ' ') return true;
  if (s.size() >= 1 && s.back() == ':') return true;

  bool disallowSpace = false;
  for (auto &it : s) {
    if (it == '\n' || it == '"' || it == '\'' || 
        it == '!' || it == '&' || it == '*' ||
        it == '[' || it == ']' ||
        it == '?' || it == ',' || it == ':' ||
        it == '#' || it == '>' || it == '|' ||
        it == '{' || it == '}' ||
        it == '@' || it == '`' ||
        it == '%' ||
        (it == ' ' && disallowSpace)) return true;
    disallowSpace = (it == ':' || it == '-');
  }
  return false;
}

void Sheet::dumpPage(YAML::Emitter &e, PageIndex page)
{
  e << YAML::Comment(R"(Throbol sheet file. See http://throbol.com.
Format is YAML. It's reasonable to edit the front matter, or formulas below the `---` by hand.
Note that YAML `#` comments are lost when loading & saving. Use `//` comments on formulas instead)");

  e << yamlByPage[page];
  auto todo = getSortedCells(page);
  e << YAML::BeginMap;
  e << YAML::Comment("Formulas");
  for (auto &[cell, pos] : todo) {
    auto &cellName = nameByCell[cell];
    auto &cellFormula = formulaByCell[cell];
    e << YAML::Key << cellName;
    e << YAML::Value;
    if (shouldUseLiteral(cellFormula)) {
      e << YAML::Literal;
    }
    e << cellFormula;
  }
  e << YAML::EndMap;
}

void Sheet::saveSheet(SaveSheetMode mode)
{
  updatePagesYamlFromSheet();

  string recoverySuffix;
  if (mode == SaveSheet_precrash) {
    time_t now = time(nullptr);
    struct tm nowtm;
    localtime_r(&now, &nowtm);
    recoverySuffix = ".recovery-" + getTimeTok(&nowtm);
  }

  for (PageIndex page = 0; page < nPages; page++) {
    auto &pageFilename = fileNameByPage[page];

    YAML::Emitter e;
    dumpPage(e, page);

    string pageData(e.c_str(), e.size());
    pageData += '\n';

    switch (mode) {

      case SaveSheet_normal:
        writeFileOrUrl(pageFilename, pageData);
        W() << "Saved " << pageFilename;
#if defined(__EMSCRIPTEN__)
        MAIN_THREAD_EM_ASM({
          if (Module && Module.postSaveFileHook) {
            Module.postSaveFileHook(UTF8ToString($0));
          }
        }, pageFilename.c_str());
#endif
        break;

      case SaveSheet_precrash:
        {
          assert(!recoverySuffix.empty());
          string saveFilename = pageFilename + recoverySuffix;
          writeFileOrUrl(saveFilename, pageData);
          L() << "Saved " << saveFilename << " for crash recovery";
        }
        break;

      case SaveSheet_autosave:
        {
          auto lastSlash = pageFilename.rfind('/');
          if (lastSlash != string::npos) {
            string saveFilename = pageFilename.substr(0, lastSlash) + "/#" + pageFilename.substr(lastSlash+1) + "#";
            if (0) L() << "Autosave " << saveFilename;
            writeFileOrUrl(saveFilename, pageData);
          }
        }
        break;
    }
  }

#if defined(__EMSCRIPTEN__)
  switch (mode) {
    case SaveSheet_normal:
      MAIN_THREAD_EM_ASM(
        if (Module && Module.postSaveHook) {
          Module.postSaveHook();
        }
      );
      break;
    case SaveSheet_precrash:
      break;
    case SaveSheet_autosave:
      break;
  }
#endif

}

bool Sheet::load(vector<string> const &fns)
{
  deque<IncludeRec> moreFiles;
  for (auto &fn : fns) {
    auto fnAbs = myRealpath(fn);
    if (endsWith(fnAbs, ".tb")) {
      moreFiles.push_back(IncludeRec{
        fnAbs,
        vec2(0, 0),
        nullPageIndex,
      });
    }
    else if (endsWith(fnAbs, ".replays")) {
      if (!replayFileName.empty()) {
        Wred() << "Multiple replay files not supported\n";
        goto fail;
      }
      replayFileName = fnAbs;
    }
    else {
      Wred()  << "Unknown file extension (should be .tb or .replays): " << fnAbs;
      goto fail;
    }
  }

  while (!moreFiles.empty()) {
    auto fn = moreFiles.front();
    moreFiles.pop_front();
    if (!loadPage(fn.includeFnAbs, fn.includePos, fn.includeParent, moreFiles)) {
      goto fail;
    }
    W() << "Loaded " << fn.includeFnAbs;
  }

  if (replayFileName.empty() && !fileNameByPage.empty()) {
    auto p0fn = fileNameByPage.at(0);
    if (endsWith(p0fn, ".tb")) {
      replayFileName = p0fn.substr(0, p0fn.size()-3) + ".replays";
    }
  }

  updateSheetFromPagesYaml();
  ok = true;
  return ok;

fail:
  ok = false;
  return ok;
}

bool Sheet::loadTest(char const *dump, char const *fn)
{
  mkPage(fn, vec2(0,0), nullPageIndex);
  string fulldump;
  if (strstr(dump, "---")) {
    fulldump = dump;
  }
  else {
    fulldump = "positions:\n  foo: 1 1\n---\n"s + dump;
  }
  if (!undumpPage(fulldump, 0)) {
    return false;
  }
  doDebug = true;
  compile(Emit_ui);
  return true;
}

sqlite::database &Sheet::getTraceDb()
{
  if (!traceDb) {
    if (replayFileName.empty()) {
      L() << "getTraceDb:: No replay file given";
    }
    openTraceDb(replayFileName);
    if (!traceDb) {
      dielog("openTraceDb(" << replayFileName << "): fail");
    }
  }
  return *traceDb;
}


YAML::Node Sheet::getUiSaveState()
{
  if (!yamlByPage.empty()) {
    return yamlByPage[0]["ui"];
  }
  return YAML::Node();
}
