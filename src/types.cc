#include "./core.h"
#include "./ast.h"


U32 allocAlign(TypeTag t)
{
  switch (t.kind) {
    case TK_undef: return 1;
    case TK_error: return 1;
    case TK_F: return alignof(F);
    case TK_CF: return alignof(CF);
    case TK_bool: return alignof(bool);
    case TK_mat: return EIGEN_ALIGN;
    case TK_cmat: return EIGEN_ALIGN;
    case TK_str: return alignof(string_view);
    case TK_DrawOp: return alignof(DrawOpHandle);
    case TK_Check: return alignof(Checkp);
    case TK_SymbolSet: return alignof(SymbolSet);
    case TK_Beh: return alignof(Beh);
    case TK_VideoFrame: return alignof(VideoFrame);
    case TK_Assoc: return EIGEN_ALIGN;
    case TK_Api: return EIGEN_ALIGN;
    // ADD type
  }
  return sizeof(U64);
}

U32 allocSize(TypeTag t)
{
  switch (t.kind) {
    case TK_undef: return 0;
    case TK_error: return 0;
    case TK_F: return sizeof(F);
    case TK_CF: return sizeof(CF);
    case TK_bool: return sizeof(bool);

    case TK_mat: 
      if (t.nr == 0 || t.nc == 0) {
        L() << "allocSize: incomplete type " << t;
        abort();
      }
      return sizeof(F) * size_t(t.nr) * size_t(t.nc);

    case TK_cmat:
      if (t.nr == 0 || t.nc == 0) {
        L() << "allocSize: incomplete type " << t;
        abort();
      }
      return sizeof(CF) * size_t(t.nr) * size_t(t.nc);

    case TK_str: return sizeof(string_view);
    case TK_DrawOp: return sizeof(DrawOpHandle);
    case TK_Check: return sizeof(Checkp);
    case TK_SymbolSet: return sizeof(SymbolSet);
    case TK_Beh: return sizeof(Beh);
    case TK_VideoFrame: return sizeof(VideoFrame);
    case TK_Assoc: return sizeof(Assocp);
    case TK_Api: return sizeof(ApiHandle);
    // ADD type
  }
  return 0;
}

string typeName(TypeTag t)
{
  switch (t.kind) {
    case TK_undef: return "undef";
    case TK_error: return "error";
    case TK_F: return "F";
    case TK_CF: return "CF";
    case TK_bool: return "bool";
    case TK_mat:
      if (t.ist<vec2>()) return "vec2";
      if (t.ist<vec3>()) return "vec3";
      if (t.ist<vec4>()) return "vec4";
      if (t.ist<mat2>()) return "mat2";
      if (t.ist<mat3>()) return "mat3";
      if (t.ist<mat4>()) return "mat4";
      if (t.nr == 0 && t.nc == 0) {
        return "mat";
      }
      else if (t.nr == 0 && t.nc == 1) {
        return "vec";
      }
      else if (t.nc == 1) {
        return "vec" + to_string(t.nr);
      }
      else if (t.nc == t.nr) {
        return "mat" + to_string(t.nr);
      }
      else {
        return "mat" + to_string(t.nr) + "x" + to_string(t.nc);
      }

    case TK_cmat:
      if (t.nr == 0 && t.nc == 0) {
        return "cmat";
      }
      else if (t.nr == 0 && t.nc == 1) {
        return "cvec";
      }
      else if (t.nc == 1) {
        return "cvec" + to_string(t.nr);
      }
      else if (t.nc == t.nr) {
        return "cmat" + to_string(t.nr);
      }
      else {
        return "cmat" + to_string(t.nr) + "x" + to_string(t.nc);
      }

    case TK_str: return "string";
    case TK_DrawOp: return "DrawOp";
    case TK_Check: return "Checkp";
    case TK_SymbolSet: return "SymbolSet";
    case TK_Beh: return "Beh";
    case TK_VideoFrame: return "VideoFrame";
    case TK_Assoc: return "Assoc";
    case TK_Api: return "Api";
    // ADD type
    default:
      return "type(kind="s + to_string(t.kind) + " nr=" + to_string(t.nr) + " nc=" + to_string(t.nc) + ")";
  }
}

int typeVersion(TypeTag t)
{
  switch (t.kind) {
    // if we need to change format in an incompatible way, bump the version
    // just for that type
    default:
      return 0; 
  }
}

/*
  Parse a single-dimensional size for looking up types like `vec8`.
    "5" => 5
    "26" => 26
    "" => 0
    "foo" => nullopt
*/
optional<tuple<int, string_view>> parseSize(string_view a)
{
  int nr = 0;

  size_t i = 0;
  while (i < a.size()) {
    auto c = a[i];
    if (c >= '0' && c <= '9') {
      nr = nr * 10 + int(c - '0');
      i++;
    }
    else {
      break;
    }
  }
  if (i == 0) {
    return nullopt;
  }
  return make_tuple(nr, a.substr(i));
}

/*
  Parse a 2-dimension size for looking up types like `mat5x7`.
    "5x7" => (5, 7)
    "26x36" => (26, 36)
    "mat4" => (4, 4)
    "" => (0, 0)
    "foo" => nullopt
*/
optional<tuple<int, int, string_view>> parseSize2(string_view a)
{
  bool inrow = true, incol = false;
  int nr = 0, nc = 0;

  size_t i = 0;
  while (i < a.size()) {
    auto c = a[i];
    if (c == 'x' && inrow) {
      inrow = false; incol = true;
      i++;
    }
    else if (c >= '0' && c <= '9') {
      if (inrow) {
        nr = nr * 10 + int(c - '0');
      }
      else if (incol) {
        nc = nc * 10 + int(c - '0');
      }
      else {
        goto fail;
      }
      i++;
    }
    else {
      break;
    }
  }
  if (i == 0) {
    return nullopt;
  }
  if (incol) {
    return make_tuple(nr, nc, a.substr(i));
  }
  if (inrow) {
    return make_tuple(nr, nr, a.substr(i));
  }

fail:
  return nullopt;
}

static bool isDigit(int c)
{
  return c >= '0' && c <= '9';
}

TypeTag typeByName(string_view a)
{
  if (a == "undef") return T_undef;
  if (a == "error") return T_error;

  if (a == "F") return typetag<F>();
  if (a == "float") return typetag<F>();
  if (a == "CF") return typetag<CF>();
  if (a == "complex") return typetag<CF>();
  if (a == "vec2") return typetag<vec2>();
  if (a == "vec4") return typetag<vec4>();
  if (a == "mat2") return typetag<mat2>();
  if (a == "mat4") return typetag<mat4>();
  if (a == "mat") return typetag<mat>();
  if (a == "cmat") return typetag<cmat>();
  if (a == "string") return typetag<string_view>();
  if (a == "DrawOp") return typetag<DrawOp>();
  if (a == "Check") return typetag<Check>();
  if (a == "Checkp") return typetag<Check>();
  if (a == "SymbolSet") return typetag<SymbolSet>();
  if (a == "Beh") return typetag<Beh>();
  if (a == "VideoFrame") return typetag<VideoFrame>();
  // ADD type

  // like vec26
  if (a.size() >= 4 && a[0] == 'v' && a[1] == 'e' && a[2] == 'c' && isDigit(a[3])) {
    if (auto size = parseSize(a.substr(3))) {
      auto [nr, suffix] = *size;
      if (suffix.empty()) {
        return TypeTag(TK_mat, nr, 1);
      }
    }
  }

  if (a.size() >= 5 && a[0] == 'c' && a[1] == 'v' && a[2] == 'e' && a[3] == 'c' && isDigit(a[4])) {
    if (auto size = parseSize(a.substr(4))) {
      auto [nr, suffix] = *size;
      if (suffix.empty()) {
        return TypeTag(TK_cmat, nr, 1);
      }
    }
  }

  // like mat3x5
  if (a.size() >= 4 && a[0] == 'm' && a[1] == 'a' && a[2] == 't' && isDigit(a[3])) {
    if (auto size = parseSize2(a.substr(3))) {
      auto [nr, nc, suffix] = *size;
      if (suffix.empty()) {
        return TypeTag(TK_mat, nr, nc);
      }
    }
  }
  // like cmat3x5
  if (a.size() >= 5 && a[0] == 'c' && a[1] == 'm' && a[2] == 'a' && a[3] == 't' && isDigit(a[4])) {
    if (auto size = parseSize2(a.substr(4))) {
      auto [nr, nc, suffix] = *size;
      if (suffix.empty()) {
        return TypeTag(TK_cmat, nr, nc);
      }
    }
  }

  if (0) L() << "Unknown typeByName " << a;
  return T_error;
}


ostream & operator<< (ostream &s, TypeTag a)
{
  return s << typeName(a);
}

U32 allocIncr(U32 &ofs, TypeTag t, int count)
{
  U32 align = allocAlign(t);
  ofs += (align - ofs) & (align - 1);
  auto ret = ofs;
  ofs += allocSize(t) * count;
  return ret;
}

bool operator < (TypeTag a, TypeTag b)
{
  if (a.kind < b.kind) return true;
  if (b.kind < a.kind) return false;
  if (a.nr < b.nr) return true;
  if (b.nr < a.nr) return false;
  return a.nc < b.nc;
}


ostream & operator <<(ostream &s, Val const &a)
{
  s << a.t << "(ofs=" << a.ofs << ")";
  return s;
}


ostream & operator <<(ostream &s, ValSuite const &a)
{
  s << a.t << "(ofs=" << a.ofs1 << "/" << a.ofs8 << "/" << a.ofs32 << ")";
  return s;
}
