#pragma once
#include "./ast.h"
#include "./cpu.h"
#include "numerical/numerical.h"

struct Annotation {

  AstType t;
  FormulaSubstr loc;
  union {
    U32 u32;
  };

  Annotation(AstType _t, FormulaSubstr _loc) : t(_t), loc(_loc) {}
  Annotation(AstType _t, FormulaSubstr _loc, U32 _data) : t(_t), loc(_loc), u32(_data) {}

  ParamIndex getParamIndex() const { return u32; }
  CellIndex getCellIndex() const { return u32; }

};

struct CodeAnnotator {

  Sheet &sh;
  CellIndex cell;
  AstNode *root;
  bool ok;

  vector<Annotation> annos;

  CodeAnnotator(Sheet &_sh, CellIndex _cell);

  ParamIndex getParamIndex(AstNode *n);
  void emit(AstNode *n, AstNode *parent);
};


ostream & operator <<(ostream &s, DebugTestPoint const &a);
