#pragma once
#include "common/std_headers.h"

template<typename T>
struct IndexSet {

  const T nullIndex = T(-1);

  vector<bool> bitmap;
  vector<T> vec;

  IndexSet() {}
  ~IndexSet() {}
  IndexSet(IndexSet const &other) : bitmap(other.bitmap), vec(other.vec) {}
  IndexSet(IndexSet &&other) noexcept : bitmap(std::move(other.bitmap)), vec(std::move(other.vec)) {}

  IndexSet(initializer_list<T> _vec)
  {
    for (auto &it : _vec) {
      set(it, true);
    }
  }

  void clear()
  {
    vec.clear();
    fill(bitmap.begin(), bitmap.end(), false);
  }

  bool get(T i) const
  {
    if (i == nullIndex) return false;
    if (i < bitmap.size()) return bitmap[i];
    return false;
  }

  T nthIndex(int i) const
  {
    if (i < 0) return nullIndex;
    if (vec.empty()) return nullIndex;
    return vec[size_t(i) % vec.size()];
  }

  void set(T i, bool v)
  {
    if (i == nullIndex) return;
    if (i >= bitmap.size()) {
      if (v) {
        bitmap.resize(std::max(bitmap.size() * 2, size_t(i)+1));
      }
      else {
        return;
      }
    }
    if (v) {
      if (!bitmap[i]) {
        bitmap[i] = true;
        vec.push_back(i);
      }
    }
    else {
      if (bitmap[i]) {
        bitmap[i] = false;
        // alternatively, set a flag saying we neet to filter vec before using.
        // This would help when we clear most of the bits, which is common for todo bits.
        vec.erase(remove(vec.begin(), vec.end(), i), vec.end());
      }
    }
  }

  // Use for iteration if you might change contents inside the loop
  vector<T> dupIndex()
  {
    return vec;
  }

  vector<T> orderBy(vector<T> const &order)
  {
    vector<T> ret;
    for (auto it : order) {
      if (bitmap[it]) ret.push_back(it);
    }
    return ret;
  }

  IndexSet intersect(IndexSet const &other)
  {
    IndexSet ret;
    for (auto it : *this) {
      if (other[it]) ret.set(it, true);
    }
    return ret;
  }

  IndexSet union_(IndexSet const &other)
  {
    IndexSet ret;
    for (auto it : *this) {
      if (other[it]) ret.set(it, true);
    }
    return ret;
  }

  size_t size()
  {
    return vec.size();
  }

  bool empty()
  {
    return vec.empty();
  }

  T front()
  {
    if (vec.empty()) return nullIndex;
    return vec.front();
  }

  T back()
  {
    if (vec.empty()) return nullIndex;
    return vec.back();
  }

  T pop_back()
  {
    if (vec.empty()) throw logic_error("empty");
    auto x = vec.back();
    bitmap[x] = false;
    vec.pop_back();
    return x;
  }

  IndexSet &operator =(IndexSet const &other)
  {
    bitmap = other.bitmap;
    vec = other.vec;
    return *this;
  }

  IndexSet &operator =(IndexSet &&other)
  {
    swap(bitmap, other.bitmap);
    swap(vec, other.vec);
    return *this;
  }

  struct ref {

    IndexSet &owner;
    T i;
    
    ref(IndexSet &_owner, T _i) : owner(_owner), i(_i) {}

    operator bool() const { return owner.get(i); }
    bool operator =(bool v) { owner.set(i, v); return v; }
  };

  struct cref {

    IndexSet const &owner;
    T i;
    
    cref(IndexSet const &_owner, T _i) : owner(_owner), i(_i) {}

    operator bool() const { return owner.get(i); }
  };

  ref operator [](T i)
  {
    return ref(*this, i);
  }

  cref operator [](T i) const
  {
    return cref(*this, i);
  }

  typename vector<T>::const_iterator begin() const { return vec.cbegin(); }
  typename vector<T>::const_iterator end() const { return vec.cend(); }

};

template<typename T>
bool operator == (IndexSet<T> const &a, IndexSet<T> const &b)
{
  return a.vec == b.vec;
}

template<typename T>
T & aref(vector<T> &arr, size_t pos)
{
  if (pos == size_t(U32(-1))) throw logic_error("aref nullIndex?");
  if (pos == size_t(-1)) throw logic_error("aref nullSize?");

  if (pos >= arr.size()) arr.resize(pos + 1);
  return arr[pos];
}

template<typename T>
T & aref(vector<T> &arr, size_t pos, T const &def)
{
  if (pos == size_t(U32(-1))) throw logic_error("aref nullIndex?");
  if (pos == size_t(-1)) throw logic_error("aref nullSize?");

  if (pos >= arr.size()) arr.resize(pos + 1, def);
  return arr[pos];
}
