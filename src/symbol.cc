#include "./celldata.h"
#include "./sheet.h"



SymbolSet op_union(CpuState &cpu, SymbolSet a, SymbolSet b)
{
  if (a.empty()) return b;
  if (b.empty()) return a;

  // This is especially common when building up a set with 'foo|'bar|'buz
  if (!a.tail) return SymbolSet(a.leaf, cpu.pool->mk<SymbolSet>(b));
  if (!b.tail) return SymbolSet(b.leaf, cpu.pool->mk<SymbolSet>(a));
  
  auto av = setToVec(a);
  auto bv = setToVec(b);
  sort(av.begin(), av.end());
  sort(bv.begin(), bv.end());
  vector<Symbol> rc;
  set_union(av.begin(), av.end(), bv.begin(), bv.end(), back_inserter(rc));
  return vecToSet(cpu.pool, rc);
}

SymbolSet op_union(CpuState &cpu, Symbol a, SymbolSet b)
{
  if (a.isNil()) return b;
  if (b.empty()) return SymbolSet(a);
  return SymbolSet(a, cpu.pool->mk<SymbolSet>(b));
}

SymbolSet op_union(CpuState &cpu, SymbolSet a, Symbol b)
{
  if (b.isNil()) return a;
  if (a.empty()) return SymbolSet(b);
  return SymbolSet(b, cpu.pool->mk<SymbolSet>(a));
}

SymbolSet op_union(CpuState &cpu, Symbol a, Symbol b)
{
  if (a.isNil()) {
    if (b.isNil()) {
      return SymbolSet();
    }
    return SymbolSet(b);
  }
  else if (b.isNil()) {
    return SymbolSet(a);
  }
  return SymbolSet(a, cpu.pool->mk<SymbolSet>(b));
}

bool op_has(CpuState &cpu, SymbolSet a, Symbol b)
{
  if (a.leaf == b) return true;
  for (auto p = a.tail; p; p = p->tail) {
    if (p->leaf == b) return true;
  }
  return false;
}

SymbolSet op_intersection(CpuState &cpu, SymbolSet a, SymbolSet b)
{
  if (a.empty() || b.empty()) return SymbolSet();
  auto av = setToVec(a);
  auto bv = setToVec(b);
  sort(av.begin(), av.end());
  sort(bv.begin(), bv.end());
  vector<Symbol> rc;
  set_intersection(av.begin(), av.end(), bv.begin(), bv.end(), back_inserter(rc));
  return vecToSet(cpu.pool, rc);
}

SymbolSet op_intersection(CpuState &cpu, Symbol a, SymbolSet b)
{
  if (a.isNil() || b.empty()) return SymbolSet();
  if (op_has(cpu, b, a)) {
    return SymbolSet(a);
  }
  else {
    return SymbolSet();
  }
}

SymbolSet op_intersection(CpuState &cpu, SymbolSet a, Symbol b)
{
  if (a.empty() || b.isNil()) return SymbolSet();
  if (op_has(cpu, a, b)) {
    return SymbolSet(b);
  }
  else {
    return SymbolSet();
  }
}

SymbolSet op_intersection(CpuState &cpu, Symbol a, Symbol b)
{
  if (!a.isNil() && a == b) {
    return SymbolSet(a);
  }
  else {
    return SymbolSet();
  }
}


bool op_eq(CpuState &cpu, SymbolSet a, SymbolSet b)
{
  auto av = setToVec(a);
  auto bv = setToVec(b);
  sort(av.begin(), av.end());
  sort(bv.begin(), bv.end());
  return av == bv;
}

bool op_eq(CpuState &cpu, Symbol a, SymbolSet b)
{
  if (b.leaf == a && !b.tail) return true;
  return false;
}

bool op_eq(CpuState &cpu, SymbolSet a, Symbol b)
{
  if (a.leaf == b && !a.tail) return true;
  return false;
}

bool op_eq(CpuState &cpu, Symbol a, Symbol b)
{
  return a == b;
}


bool op_hasany(CpuState &cpu, SymbolSet a, SymbolSet b)
{
  if (a.empty() || b.empty()) return false;
  auto av = setToVec(a);
  auto bv = setToVec(b);
  sort(av.begin(), av.end());
  sort(bv.begin(), bv.end());
  vector<Symbol> rc;
  set_intersection(av.begin(), av.end(), bv.begin(), bv.end(), back_inserter(rc));
  return !rc.empty();
}


vector<Symbol> setToVec(SymbolSet a)
{
  vector<Symbol> ret;
  for (auto const *p = &a; p; p = p->tail) {
    if (!p->leaf.isNil()) {
      ret.push_back(p->leaf);
    }
  }
  return ret;
}

SymbolSet vecToSet(Arena *pool, vector<Symbol> const &vec)
{
  if (vec.empty()) return SymbolSet();
  SymbolSet const *tail = nullptr;
  for (auto i = vec.size() - 1; i > 0; i--) {
    tail = pool->mk<SymbolSet>(vec[i], tail);
  }
  return SymbolSet(vec[0], tail);
}

ostream & operator <<(ostream &s, WithSheet<SymbolSet> const &a)
{
  auto av = setToVec(a.it);
  sort(av.begin(), av.end());
  if (av.empty()) {
    return s << "nil";
  }
  bool needSep = false;
  for (auto &it : av) {
    if (needSep) s << "|";
    s << WithSheet(a.sh, it);
    needSep = true;
  }
  return s;
}

ostream &operator << (ostream &s, WithSheet<Symbol> const &a)
{
  return s << '\'' << a.sh.nameBySymbol[a.it.i];
}

ostream &operator << (ostream &s, Symbol const &a)
{
  return s << "\'#" << a.i;
}

ostream & operator <<(ostream &s, SymbolSet const &a)
{
  if (a.empty()) {
    return s << "nil";
  }
  auto av = setToVec(a);
  sort(av.begin(), av.end());
  bool needSep = false;
  for (auto &it : av) {
    if (needSep) s << "|";
    s << it;
    needSep = true;
  }
  return s;
}
