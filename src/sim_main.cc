#include "./core.h"
#include "./vbmainloop.h"
#include "./sheet_ui.h"
#include "./uipoll.h"
#include "./joybox.h"
#include "./emit.h"
#include "../geom/gl_headers.h"
#include "./config.h"
#include "backends/imgui_impl_glfw.h"
#include <getopt.h>
#include "GLFW/glfw3.h"
#include <thread>
#include <filesystem>
extern void panel3dInit();

void usage()
{
  L() << R"(usage: tb [-C dir] [-v] foo.tb ...
  -v: increase verbosity
  -t: write trace as csv file to stdout
  --light: light mode
  Help at http://throbol.com/help/
)";
}


struct SimOptions {

  int startUi = 1;
  int tweakForPodcast = 0;
  int uiPerfTestMode = 0;
  int showUsbDevs = 0;
  int lightMode = 0;
  int writeTraceCsv = 0;
  int verbose = 0;
  
};
static SimOptions options;

int simMain(vector<string> const &pageFileNames, SimOptions &options)
{
  
  if (options.startUi) {

    glfwSetErrorCallback(
      [](int error, const char* description) {
        L() << "GLFW error " << error << ": " << description;
      });
    glfwInit();    

    signal(SIGPIPE, SIG_IGN);



    Joybox::setup();
    

    mainWindow0 = make_shared<MainWindow>();
    mainWindow0->tweakForPodcast = options.tweakForPodcast;
    mainWindow0->uiStyle = options.lightMode ? UiLight : UiDark;
    mainWindow0->setup();
    panel3dInit();
    startUiPoll(mainWindow0);

    auto ui = make_shared<SheetUi>(*mainWindow0);
    if (!ui->loadDocument(pageFileNames)) exit(1);
    if (options.uiPerfTestMode) ui->uiPerfTestMode = true;
    mainWindow0->setUi(ui);

    io_context.run();

    mainWindow0->teardown();

    if (imguiContext) {
      SetCurrentContext(imguiContext);
      ImGui_ImplGlfw_Shutdown();
      ImGui_ImplWGPU_Shutdown();

      DestroyContext(imguiContext);
      imguiContext = nullptr;
    }
    glfwTerminate();    
  }

  return 0;
}

int mainWriteTraceCsv(vector<string> fns)
{
  Sheet sh;
  if (!sh.load(fns)) return 1;
  sh.compile();
  for (auto &it : sh.errors()) {
    L() << it;
  }
  if (!sh.ok || !sh.compiled) {
    return 1;
  }

  auto t = make_shared<Trace>(sh.compiled);
  t->setupInitial();
  simulateGroup({t});
  t->writeCsv(cout);
  return 0;
}

int main(int argc, char * const *argv)
{

  struct option longopts[] = {
    {"podcast", no_argument, &options.tweakForPodcast, 1},
    {"show-usb-devs", no_argument, &options.showUsbDevs, 1},
    {"ui-perf-test-mode", no_argument, &options.uiPerfTestMode, 1},
    {"light-mode", no_argument, &options.lightMode, 1},
    {nullptr, 0, nullptr, 0}
  };

  int ch;
  while ((ch = getopt_long(argc, argv, "C:vt", longopts, nullptr)) != -1) {
    switch (ch) {
      case 'C':
        if (chdir(optarg) < 0) {
          L() << optarg << ": " << strerror(errno);
          return 1;
        }
        break;

      case 'v':
        options.verbose ++;
        break;

      case 't':
        options.writeTraceCsv ++;
        break;

      case 0:
        break;

      default:
        usage();
        return 2;
    }
  }

  setupPaths(argv[0]);

  argc -= optind;
  argv += optind;


  cerr.precision(3);

  if (options.showUsbDevs) {
    Joybox::report();
    return 0;
  }

  vector<string> fns;
  for (int i = 0; i < argc; i++) {
    fns.push_back(argv[i]);
  }

  if (CpuState::execVerbose) {
    CpuState::logFile = make_shared<ofstream>("logs/cpu.log");
  }

  if (options.writeTraceCsv) return mainWriteTraceCsv(fns);

  int exitCode = 0;

  simulateStartup();

  if (!fns.empty()) {
    exitCode = simMain(fns, options);
  }
  else {
    usage();
  }

  simulateShutdown();
  return exitCode;
}

