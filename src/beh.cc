#include "./celldata.h"
#include "./sheet.h"


Beh op_fallback(CpuState &cpu, Beh const &a, Beh const &b)
{
  switch (a.status) {
    case B_running:
      return a;

    case B_failure:
      return Beh(b.status, op_union(cpu, a.runset, b.runset));

    case B_success:
      return a;
  }
  return b;
}

Beh op_seq(CpuState &cpu, Beh const &a, Beh const &b)
{
  switch (a.status) {
    case B_running:
      return a;
    
    case B_failure:
      return a;

    case B_success:
      return Beh(b.status, op_union(cpu, a.runset, b.runset));
  }
}

Beh op_cond(CpuState &cpu, F const &a)
{
  if (a >= 0.5f) {
    return Beh(B_success);
  }
  else {
    return Beh(B_failure);
  }
}


Beh op_cond(CpuState &cpu, bool a)
{
  if (a) {
    return Beh(B_success);
  }
  else {
    return Beh(B_failure);
  }
}


ostream & operator <<(ostream &s, BehStatus const &a)
{
  switch (a) {
    case B_running: return s << "running";
    case B_failure: return s << "fail";
    case B_success: return s << "success";
    default: return s << "?";
  }
}

ostream & operator <<(ostream &s, Beh const &a)
{
  s << a.status << a.runset;
  return s;
}

ostream & operator <<(ostream &s, WithSheet<Beh> const &a)
{
  s << a.it.status << WithSheet(a.sh, a.it.runset);
  return s;
}

