#include "./test_utils.h"
#include "./execlist.h"

TEST_CASE("ExecList", "[core]") {
  ExecList<CpuState &, Pad &, BatchDef<1>> fb;
  int a = 2, b = 3;
  fb.push_back([a, b](CpuState &cpu, Pad &pad, auto batchdef) {
    L() << "a=" << a << " b=" << b;
  });
  b = 5;
  fb.push_back([a, b](CpuState &cpu, Pad &pad, auto batchdef) {
    L() << "a=" << a << " b=" << b;
  });
  int c = 6, d = 7, e = 7;
  fb.push_back([a, b, c, d, e](CpuState &cpu, Pad &pad, auto batchdef) {
    L() << "a=" << a << " b=" << b << " c=" << c << " d=" << d << " e=" << e;
  });

  Arena pool;
  CpuState *cpu = nullptr;
  auto pad = Pad::mk(&pool, 1024);
  fb.run(*cpu, *pad, BatchDef<1>());
}
