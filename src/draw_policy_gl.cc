#include "./core.h"
#include "../geom/gl_headers.h"
#include "../geom/g8.h"
#include "numerical/numerical.h"
#include "numerical/polyfit.h"
#include "./sheet_ui.h"
#include "./scope_math.h"
#include "./draw_gl.h"


struct RenderAxis {

  vec3 dir;
  vec4 color;
  
};

static F axis_nonlin(F x)
{
  return copysign(pow(abs(x), 0.4), x);
}

// We want lower solutions higher, but everything above zero
static F eval_nonlin(F x)
{
  return 0.5f * (1.6f - atan(x));
}

RenderAxis getParticleSearchAxis(Sheet &sh, ParamIndex paramIndex)
{
  auto seed = std::hash<string>{}(sh.getParamDesc(paramIndex));
  std::mt19937 gen(seed);
  std::uniform_real_distribution<float> bidis(0, M_2PI);
  vec4 color = goodGraphColor(seed);
  auto th = bidis(gen);
  vec3 dir = vec3(cos(th) * 0.5f, sin(th) * 0.5f, 0.0);

  return RenderAxis{dir, color};
}

static void showSearchMenu(GenericSearchState *ss)
{
  if (beginPickItem()) {
    bool active = ss->active;
    if (Checkbox("Search", &active)) {
      if (active) {
        ss->start();
      } 
      else {
        ss->stop();
      }
    }
    EndPopup();
  }
}


static void showSetParamMenu(Sheet &sh, ParticleSearchState *pss, ParticleSearchState::Particle *particle)
{
  if (particle && beginPickItem()) {
    if (MenuItem("Set Params")) {
      pss->writeBack(*particle);
      pss->stop();
      pss->clear();
    }
    BulletText("Evaluated to %s",
      //repr(WithSheet(sh, pss->spec.rMetric)).c_str(),
      repr_0_3g(particle->evaluation).c_str());
    auto nParam = int(pss->spec.searchDims.size());
    for (int pi = 0; pi < nParam ; pi++) {
      auto &searchDim = pss->spec.searchDims[pi];
      auto oldParam = searchDim.getParamValue(sh, 0.0);
      auto newParam = searchDim.getParamValue(sh, particle->tweaks[pi]);

      BulletText("Change %s from %s to %s", 
        sh.getParamDesc(searchDim.param).c_str(),
        repr_0_3g(oldParam).c_str(), repr_0_3g(newParam).c_str());
    }
    EndPopup();
  }
}

void DrawTraceAccum::drawOpFinal(Cmd &cmd, DrawOpParticleSearch *op)
{
  ParticleSearchSpec spec;
  spec.alg = op->alg;
  spec.rMetric = op->rMetricRef;
  spec.searchDims = op->searchDims;
  spec.paramHash = op->paramHash;
  spec.compiledSheetEpoch = op->compiledSheetEpoch;
  spec.sampleIndex = op->sampleIndex;
  spec.baseValue = op->baseValue;
  spec.replayBuffer = tr.replayBuffer;

  if (auto it = assoc_F(cmd.options, "accelTowardsBest")) spec.accelTowardsBest = *it;
  if (auto it = assoc_F(cmd.options, "velCoeff")) spec.velCoeff = *it;
  if (auto it = assoc_F(cmd.options, "initialParamSigma")) spec.initialParamSigma = *it;
  if (auto it = assoc_F(cmd.options, "initialVelSigma")) spec.initialVelSigma = *it;
  if (auto it = assoc_F(cmd.options, "deFAC")) spec.deFAC = *it;
  if (auto it = assoc_F(cmd.options, "deCR")) spec.deCR = *it;
  if (auto it = assoc_F(cmd.options, "populationTarget")) spec.populationTarget = int(*it);

  auto pss = ParticleSearchState::lookup(sh, spec);
  if (!pss) return; // ???

  if (pss->active) pss->poll();
  auto particles = pss->particles;
  G8PickList pl(dl);

  if (particles && particles->size()) {
    vector<RenderAxis> axes;
    for (size_t i = 0; i < pss->spec.searchDims.size(); i++) {
      axes.push_back(getParticleSearchAxis(sh, pss->spec.searchDims[i].param));
    }

    F startRad = 0.1;
    dl.addSphere(
      cmd.tPos(vec3(0, 0, 0)),
      vec3(startRad, 0, 0),
      vec3(0, startRad, 0),
      vec3(0, 0, startRad),
      vec4(0, 0, 0, 1),
      0.5f, &litSolidShader);

    int particleIndex = 0;
    for (auto &particle : *particles) {
      if (particle.evaluated) {

        vec3 pt(0, 0, 0);
        vec3 tubeAxis(0, 0, 1);
        for (size_t pi = 0; pi < axes.size(); pi++) {
          auto const &axis = axes[pi];

          auto tweak = particle.tweaks[pi];
          if (tweak != 0.0f) {
            vec3 pt2 = pt + axis_nonlin(tweak) * axis.dir - vec3(0, 0, 0.1f*abs(tweak));

            dl.addNoodle(
              cmd.tPos(pt), axis.color,
              cmd.tPos(pt2), axis.color,
              tubeAxis,
              0.035f);

            if (abs(tweak) >= 1.999f) {
              vec4 endColor(1, 0, 0, 1);
              vec3 endpt = cmd.tPos(pt2);
              vec3 tweakDir = cmd.tPos(pt2) - cmd.tPos(pt);
              vec3 perpDir1 = vec3(0, 0, 1).cross(tweakDir).normalized();
              vec3 perpDir2 = perpDir1.cross(tweakDir).normalized();
              F rectSize = 0.1;

              dl.addCircle(
                endpt,
                rectSize*perpDir1,
                rectSize*perpDir2,
                endColor,
                1.0f, &litSolidShader);
            }            

            pt = pt2;
          }
        }

        vec4 tubeColor(0.6, 0.9, 0.6, 1.0);
        vec4 dotColor(0.5, 0.6, 0.4, 1.0);
        float dotSize = 0.035f; // + 5.0 * (0.02 / (particleIndex + 5.0));

        vec3 pt2 = pt;
        pt2[2] = eval_nonlin(particle.evaluation);
        //vec3 pt2 = pt + vec3(0, 0, 0.2 + 5.0 / (particleIndex + 5.0));
        dl.addNoodle(
          cmd.tPos(pt), tubeColor,
          cmd.tPos(pt2), tubeColor,
          tubeAxis,
          dotSize);
        pt = pt2;

        dl.addSphere(
          cmd.tPos(pt),
          cmd.tDir(vec3(dotSize, 0, 0)),
          cmd.tDir(vec3(0, dotSize, 0)),
          cmd.tDir(vec3(0, 0, dotSize)),
          tubeColor);

        F fontSize = particleIndex == 0 ? 0.20 : 0.15;
        string label = repr_0_3g(particle.evaluation);
        pl.addText(
          cmd.tPos(pt + vec3(0, 0, dotSize)),
          vec3(fontSize, 0, 0),
          vec3(0, -fontSize, 0),
          vec3(0, 0, 1),
          label,
          vec4(1, 1, 1, 1),
          ysFonts.lrgFont,
          0.5, 0.5, 
          1.06 - R(particleIndex) / R(particles->size()),
          [particle, pss](vec3 const &hit) {
            if (IsMouseClicked(0, false) && GetIO().KeyCtrl) {
              pss->clickedBestParticle = make_shared<ParticleSearchState::Particle>(particle);
              OpenPopup("pickItem");
            }
          });
      }
      particleIndex ++;
    }
  }
  if (1) {
    ostringstream labels;
    labels.precision(3);
    if (particles && !particles->empty() && particles->at(0).evaluated) {
      labels << "Optimizing from " << spec.baseValue << " to " << particles->at(0).evaluation;
    }
    else {
      labels << "Optimize from " << spec.baseValue;
    }

    pl.addText(
      cmd.tPos(vec3(0, 2, 0)),
      vec3(fontSize, 0, 0),
      vec3(0, -fontSize, 0),
      vec3(0, 0, 1),
      labels.str(),
      vec4(1, 1, 1, 1),
      ysFonts.lrgFont,
      0.5, 0.5,
      1.06,
      [](vec3 const &hit) {
        if (IsMouseClicked(0, false)) {
          OpenPopup("pickItem");
        }
      });
  }
  pl.finish();
  if (cellPicked) {
    showSearchMenu(pss);    
    if (pss->clickedBestParticle) {
      showSetParamMenu(sh, pss, pss->clickedBestParticle.get());
    }
    else if (particles && !particles->empty()) {
      showSetParamMenu(sh, pss, &particles->at(0));
    }
  }
}


static void showSetParamMenu(Sheet &sh, MinpackSearchState *pss, tuple<VectorXf, F> *soln)
{
  if (soln && beginPickItem()) {
    if (MenuItem("Set Params")) {
      pss->writeBack(get<0>(*soln));
      pss->stop();
      pss->clear();
    }
    BulletText("Evaluated to %s",
      repr_0_3g(get<1>(*soln)).c_str());
    auto nParam = int(pss->spec.searchDims.size());
    for (int pi = 0; pi < nParam ; pi++) {
      auto &searchDim = pss->spec.searchDims[pi];
      F normChange = get<0>(*soln)[pi];
      auto oldParam = searchDim.getParamValue(sh, 0.0);
      auto newParam = searchDim.getParamValue(sh, normChange);

      BulletText("Change %s from %s to %s", 
        sh.getParamDesc(searchDim.param).c_str(),
        repr_0_3g(oldParam).c_str(), repr_0_3g(newParam).c_str());
    }
    EndPopup();
  }
}


void DrawTraceAccum::drawOpFinal(Cmd &cmd, DrawOpMinpackSearch *op)
{
  MinpackSearchSpec spec;
  spec.alg = op->alg;
  spec.rMetric = op->rMetricRef;
  spec.searchDims = op->searchDims;
  spec.paramHash = op->paramHash;
  spec.compiledSheetEpoch = op->compiledSheetEpoch;
  spec.sampleIndex = op->sampleIndex;
  spec.baseValue = op->baseValue;
  spec.replayBuffer = tr.replayBuffer;

  if (auto it = assoc_F(cmd.options, "regularization")) spec.regularization = *it;

  auto pss = MinpackSearchState::lookup(sh, spec);
  if (!pss) return; // ???

  G8PickList pl(dl);

  if (pss->active) pss->poll();
  if (1) {
    vector<RenderAxis> axes;
    for (auto &dim : pss->spec.searchDims) {
      axes.push_back(getParticleSearchAxis(sh, dim.param));
    }

    int solnIndex = 0;
    for (auto &[solnX, solnValue] : pss->xpath) {

      vec3 pt(0, 0, 0);
      vec3 tubeAxis(0, 0.707, 0.707);
      for (size_t pi = 0; pi < axes.size(); pi++) {
        auto const &axis = axes[pi];

        auto tweak = solnX[pi];
        if (tweak != 0.0f) {
          vec3 pt2 = pt + axis_nonlin(tweak) * axis.dir - vec3(0, 0, 0.1f * abs(tweak));

          dl.addNoodle(
            cmd.tPos(pt), axis.color,
            cmd.tPos(pt2), axis.color,
            tubeAxis,
            0.035f);

          if (abs(tweak) >= 1.999f) {
            vec4 endColor(1, 0, 0, 1);
            vec3 endpt = cmd.tPos(pt2);
            vec3 tweakDir = cmd.tPos(pt2) - cmd.tPos(pt);
            vec3 perpDir1 = vec3(0, 0, 1).cross(tweakDir).normalized();
            vec3 perpDir2 = perpDir1.cross(tweakDir).normalized();
            F rectSize = 0.1;

            dl.addCircle(
              endpt,
              rectSize*perpDir1,
              rectSize*perpDir2,
              endColor,
              1.0f, &litSolidShader);
          }            
          pt = pt2;
        }
      }

      vec4 tubeColor(0.6, 0.9, 0.6, 1.0);
      vec4 dotColor(0.5, 0.6, 0.4, 1.0);
      float dotSize = 0.035f;

      vec3 pt2 = pt;
      pt2[2] = eval_nonlin(solnValue);
      dl.addNoodle(
        cmd.tPos(pt), tubeColor,
        cmd.tPos(pt2), tubeColor,
        tubeAxis,
        dotSize);

      pt = pt2;

      dl.addSphere(
        cmd.tPos(pt),
        cmd.tDir(vec3(dotSize, 0, 0)),
        cmd.tDir(vec3(0, dotSize, 0)),
        cmd.tDir(vec3(0, 0, dotSize)),
        tubeColor);

      F fontSize = solnIndex == int(pss->xpath.size() - 1) ? 0.20 : 0.15;
      string label = repr_0_3g(solnValue);
      pl.addText(
        cmd.tPos(pt + vec3(0, 0, dotSize)),
        vec3(fontSize, 0, 0),
        vec3(0, -fontSize, 0),
        vec3(0, 0, 1),
        label,
        vec4(1, 1, 1, 1),
        ysFonts.lrgFont,
        0.5, 0.5,
        0.56 + R(solnIndex) / R(pss->xpath.size()),
        [solnX=solnX, solnValue=solnValue, pss](vec3 const &hit) {
          if (IsMouseClicked(0, false) && GetIO().KeyCtrl) {
            // WRITEME: how to clear when clicked elsewhere?
            pss->clickedBestSoln = make_shared<tuple<VectorXf, F>>(solnX, solnValue);
            OpenPopup("pickItem");
          }
        });
      solnIndex++;
    }
  }
  if (1) {
    ostringstream labels;
    labels.precision(3);
    switch (pss->algState) {
      case Minpack_initial:
        labels << "Optimize from " << spec.baseValue;
        break;

      case Minpack_stepping:
        labels << "Optimizing from " << spec.baseValue << " to " << pss->bestval;
        break;

      case Minpack_done:
        labels << "Optimized from " << spec.baseValue << " to " << pss->bestval;
        break;
    }
    if (!pss->message.empty()) {
      labels << "\n" << pss->message;
    }

    pl.addText(
      cmd.tPos(vec3(0, 2, 0)),
      vec3(fontSize, 0, 0),
      vec3(0, -fontSize, 0),
      vec3(0, 0, 1),
      labels.str(),
      vec4(1, 1, 1, 1),
      ysFonts.lrgFont,
      0.5, 0.5,
      1.06,
      [](vec3 const &hit) {
        if (IsMouseClicked(0, false)) {
          OpenPopup("pickItem");
        }
      });
  }
  pl.finish();
  if (cellPicked) {
    showSearchMenu(pss);    
    if (pss->clickedBestSoln) {
      showSetParamMenu(sh, pss, pss->clickedBestSoln.get());
    }
    else if (!pss->xpath.empty()) {
      showSetParamMenu(sh, pss, &pss->xpath.back());
    }
  }
}
