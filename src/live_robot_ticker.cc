#include "./live_robot_ticker.h"
#if !defined(EMSCRIPTEN_NOTYET)
#include "../src/api.h"
#include "./emit.h"
#include "./trace.h"

static constexpr int verbose = 0;

LiveRobotTicker::LiveRobotTicker(shared_ptr<SimRobotServer> _server)
  : server(std::move(_server)),
    compiled(server->activeSheet->compiled)
{
  assert(compiled->emitMode == Emit_hw);

  label = "lrt";

  trace = make_shared<Trace>(compiled, false, false);
  auto &tr = *trace;
  tr.setupInitial();

  tr.startTime = realtime();
  tr.randomSeed = 0;
  tr.mkReplayBuffer();

}

LiveRobotTicker::~LiveRobotTicker()
{
  if (0) L() << label << ": destructed";
  server = nullptr;
  compiled = nullptr;
  trace = nullptr;
}

bool LiveRobotTicker::startLive()
{
  trace->startLive();
  for (auto const &api : compiled->apis) {
    if (!api->startLive()) {
      L() << label << ": " << api->label << ": startLive failed";
      stopLive();
      lrt.setState(LiveRobotState::LRS_error);
      return false;
    }
  }

  lrt.setState(LiveRobotState::LRS_warmup);
  lrt.hwTimeBase = realtime();

  timer.expires_after(std::chrono::milliseconds(10));
  asyncTick();
  return true;
}

void LiveRobotTicker::asyncTick()
{
  timer.expires_at(timer.expiry() + std::chrono::microseconds(trace ? long(ceil(double(trace->dt) * 1.0e6)) : 10000));
  timer.async_wait(
    [this, keepalive=shared_from_this()](boost::system::error_code error) {
      if (error) {
        if (error == asio::error::operation_aborted) {
          L() << label << ": tick aborted";
          //stopLive();
          //lrt.state = LiveRobotState::S_error;
          return;
        }
        L() << label << ": tick " << error.message();
        stopLive();
        lrt.setState(LiveRobotState::LRS_error);
        return;
      }

      if (lrt.state == LiveRobotState::LRS_warmup) {
        if (verbose >= 2) L() << label << ": warmup";
        lrt.hwTimeVal = 0.0;
        lrt.needMoreWarmup.clear();
        for (auto const &api : compiled->apis) {
          api->txWarmup(lrt);
        }
        if (lrt.state == LiveRobotState::LRS_warmup) {
          if (!lrt.needMoreWarmup.empty()) {
            lrt.warmupTicks++;
            if (verbose >= 2 || lrt.warmupTicks%100==0) {
              L() << label << 
                ": after " << lrt.warmupTicks <<
                " still waiting for " << lrt.needMoreWarmup;
            }
          }
          else {
            if (verbose >= 1) L() << label << ": warmup done";
            lrt.setState(LiveRobotState::LRS_running);
            lrt.hwTimeBase = realtime();
            lrt.logcnt = 0;
          }
        }
      }
      else if (lrt.state == LiveRobotState::LRS_running) {
        if (verbose >= 2) L() << label << ": running";
        lrt.hwTimeVal = realtime() - lrt.hwTimeBase;

        trace->stepLive(lrt);

        if (server) {
          server->txUpdate();
        }
      }
      else if (lrt.state == LiveRobotState::LRS_cooldown) {
        if (verbose >= 2) L() << label << ": cooldown";
        lrt.hwTimeVal = realtime() - lrt.hwTimeBase;
        lrt.needMoreCooldown.clear();
        for (auto const &api : compiled->apis) {
          api->txCooldown(lrt);
        }
        if (lrt.state == LiveRobotState::LRS_cooldown) {
          if (!lrt.needMoreCooldown.empty()) {
            lrt.cooldownTicks++;
            if (verbose >= 2 || lrt.cooldownTicks%100==0) {
              L() << label << 
                ": after " << lrt.cooldownTicks <<
                " still waiting for " << lrt.needMoreCooldown;
            }
          }
          else {
            stopLive();
            lrt.setState(LiveRobotState::LRS_done);
            return; // no more ticks
          }
        }
      }
      else if (lrt.state == LiveRobotState::LRS_emergency) {
        stopLive();
        return;
      }
      else if (lrt.state == LiveRobotState::LRS_done) {
        stopLive();
        return;
      }
      else {
        L() << label << ": tick in state " << lrt.state;
        return;
      }
      lrt.logcnt ++;
      asyncTick();

    });
}

void LiveRobotTicker::stopLive()
{
  L() << label << ": stopLive from " << lrt.state;
  for (auto const &api : compiled->apis) {
    api->stopLive();
  }
  if (server) {
    server->robotHasStopped();
    server = nullptr;
  }
  trace = nullptr;
  compiled = nullptr;

}

#endif

