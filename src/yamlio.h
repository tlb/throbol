#pragma once
#include "./defs.h"
#include <yaml-cpp/yaml.h>

namespace YAML {
template<>
struct convert<vec3> {
  static Node encode(vec3 const &rhs) {
    Node node;
    node.push_back(rhs[0]);
    node.push_back(rhs[1]);
    node.push_back(rhs[2]);
    return node;
  }

  static bool decode(Node const &node, vec3 &rhs) {
    if (!node.IsSequence()) {
      L() << "decode vec3: not a sequence";
      return false;
    }
    if (node.size() != 3) {
      L() << "decode vec3: size=" << node.size();
      return false;
    }

    rhs[0] = node[0].as<double>();
    rhs[1] = node[1].as<double>();
    rhs[2] = node[2].as<double>();
    return true;
  }
};

template<>
struct convert<vec4> {
  static Node encode(vec4 const &rhs) {
    Node node;
    node.push_back(rhs[0]);
    node.push_back(rhs[1]);
    node.push_back(rhs[2]);
    node.push_back(rhs[3]);
    return node;
  }

  static bool decode(Node const &node, vec4 &rhs) {
    if (!node.IsSequence()) {
      L() << "decode vec4: not a sequence";
      return false;
    }
    if (node.size() != 4) {
      L() << "decode vec4: size=" << node.size();
      return false;
    }

    rhs[0] = node[0].as<double>();
    rhs[1] = node[1].as<double>();
    rhs[2] = node[2].as<double>();
    rhs[3] = node[3].as<double>();
    return true;
  }
};

template<>
struct convert<mat4> {
  static Node encode(mat4 const &rhs) {
    Node cols;
    cols.push_back(vec4(rhs.col(0)));
    cols.push_back(vec4(rhs.col(1)));
    cols.push_back(vec4(rhs.col(2)));
    cols.push_back(vec4(rhs.col(3)));
    return cols;
  }

  static bool decode(Node const &node, mat4 &rhs) {
    if (!node.IsSequence()) {
      L() << "decode mat4: not a sequence";
      return false;
    }
    if (node.size() != 4) {
      L() << "decode mat4: size=" << node.size();
      return false;
    }

    rhs.col(0) = node[0].as<vec4>();
    rhs.col(1) = node[1].as<vec4>();
    rhs.col(2) = node[2].as<vec4>();
    rhs.col(3) = node[3].as<vec4>();
    return true;
  }
};


template<typename Scalar, int Size>
struct convert<Eigen::Vector<Scalar, Size>> {
  static Node encode(Eigen::Vector<Scalar, Size> const &rhs) {
    Node node;
    for (Index i = 0; i < Size; i++) {
      node.push_back(rhs[i]);
    }
    return node;
  }

  static bool decode(Node const &node, Eigen::Vector<Scalar, Size> &rhs) {
    if (!node.IsSequence()) {
      L() << "decode vec: not a sequence";
      return false;
    }
    if (node.size() != Size) {
      L() << "decode vec: size=" << node.size() << " expected " << Size;
      return false;
    }

    for (Index i = 0; i < Size; i++) {
      rhs[i] = node[i].as<double>();
    }
    return true;
  }
};


}
