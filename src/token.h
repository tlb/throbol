#pragma once
#include "./defs.h"

enum TokenType {
  T_EOF,

  T_ID,
  T_NUMBER,
  T_STRING,
  T_SYMBOL,
  T_COLOR,
  T_IM,
  T_API,
  T_YAML,
  T_WILDCARD,

  T_IF,
  T_ELSE,
  
  T_LBRK,       // [
  T_RBRK,       // ]
  T_LPAR,       // (
  T_RPAR,       // )
  T_LWING,      // {
  T_RWING,      // }
  T_DOT,        // .
  T_AND,        // &
  T_STAR,       // *
  T_AT,         // @
  T_DOTHAT,     // .^
  T_DOTSTAR,    // .*
  T_DOTDIV,     // ./
  T_DOTPLUS,    // .+
  T_DOTMINUS,   // .-
  T_DOTEQEQ,    // .==
  T_DOTBANGEQ,  // .!=
  T_DOTGE,      // .>=
  T_DOTGT,      // .>
  T_DOTLE,      // .<=
  T_DOTLT,      // .<
  T_PLUS,       // +
  T_MINUS,      // -
  T_TILDA,      // ~
  T_BANG,       // !
  T_DIV,        // /
  T_MOD,        // %
  T_LT,         // <
  T_GT,         // >
  T_LE,         // <=
  T_GE,         // >=
  T_EQEQ,       // ==
  T_BANGEQ,     // !=
  T_PTRPTR,     // =>
  T_HAT,        // ^
  T_OR,         // |
  T_ANDAND,     // &&
  T_OROR,       // ||
  T_QUERY,      // ?
  T_COLON,      // :
  T_SEMI,       // ;
  T_ELLIPSIS,   // ...
  T_EQ,         // =
  T_PLUSEQ,     // +=
  T_MINUSEQ,    // -=
  T_TILTIL,     // ~~
  T_TILTILTIL,  // ~~~
  T_COMMA,      // ,
  T_LEFT,       // <<
  T_RIGHT,      // >>
  T_PTR,        // ->
/*
  T_INC,        // ++
  T_DEC,        // --
  T_STAREQ,     // *=
  T_DIVEQ,      // /=
  T_MODEQ,      // %=
  T_LEFTEQ,     // <<=
  T_RIGHTEQ,    // >>=
  T_ANDEQ,      // &=
  T_HATEQ,      // ^=
  T_OREQ,       // |=
  T_COLONCOLON, // ::
*/

};

struct Token {

  TokenType t;
  FormulaSubstr substr;
  union {
    string_view sv_value;
    CellIndex ci_value;
  };

  Token(TokenType _t, FormulaSubstr _substr) : t(_t), substr(_substr), sv_value() {}
  Token(TokenType _t, FormulaSubstr _substr, string_view _value) : t(_t), substr(_substr), sv_value(_value) {}
  Token(TokenType _t, FormulaSubstr _substr, CellIndex _value) : t(_t), substr(_substr), ci_value(_value) {}

};


struct FormulaTokenizer {

  using Tokit = vector<Token>::iterator;

  Sheet &sh;
  CellIndex cell;
  char const *formula;
  vector<Token> tokens;
  string error;
  FormulaSubstr errorLoc;
  bool ok = false;

  FormulaTokenizer(Sheet &_sh, CellIndex _cell, char const *_formula);

  string_view getSubstr(Token &tok);

  static bool isIdentStart(char c);
  static bool isIdentCont(char c);
  static bool isNum(char c);
  static bool isHex(char c);
  static bool isMeta(char c);

  void setError(string e, char const *p);

  void out(TokenType t, char const *start, char const *end);
  void out(TokenType t, char const *start, char const *end, string_view value);

  vector<CellIndex> namesMatching(char const *pattern);

  void tokenize();
  TokenType lookupKeyword(char const *start, char const *p);
};


