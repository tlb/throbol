#include "./core.h"
#include "../geom/gl_headers.h"
#include "../geom/g8.h"
#include "numerical/numerical.h"
#include "numerical/polyfit.h"
#include "./sheet_ui.h"
#include "./scope_math.h"
#include "./draw_gl.h"
#include "./joybox.h"
#include "./emit.h"
#include "./celledit.h"
#include "./reruns.h"
#include "../remote/sim_robot_client.h"
#include "../remote/sim_fetcher_client.h"
#include "./policy.h"
#include "GLFW/glfw3.h"
#include <regex>
using std::regex;

// An angle slightly under pi/2, to ensure we can rotate back without gimbal lock
const F MAX_ELEVATION = 1.5625;


SheetUi::SheetUi(MainWindow &_win)
  : MainUi(_win),
    sheetView(_win)
{
#if defined(__EMSCRIPTEN__)
  showHelpWindow = true;
#endif

  insets.push_back(make_shared<InsetWindow>(win, "R", CellSet()));
  insets.push_back(make_shared<InsetWindow>(win, "L", CellSet()));
}

SheetUi::~SheetUi()
{
}

void SheetUi::pleaseClose()
{
  SimFetcherClient::closeAll();
}


bool SheetUi::loadDocument(vector<string> fns)
{
  activeTrace = nullptr;
  reruns = nullptr;

  auto ok = sh.load(fns);
  if (!ok) {
    if (fns.size() == 1) {
      sh.mkPage(fns[0], vec2(0, 0), nullPageIndex);
      sh.ok = true;
      W() << "New file " << fns[0];
    }
    else {
      return false;
    }
  }

  if (auto y = sh.getUiSaveState()) {
    rdUiState(y);
  }

  showPagesWindow = sh.nPages > 1;
  updateSheetData();
  setSheetBounds();

  reruns = make_shared<RerunSet>(sh);

  return true;
}

void SheetUi::uiAutosave()
{
  sh.saveSheet(SaveSheet_autosave);
  MainUi::uiAutosave();
}

void SheetUi::wrUiState(YAML::Node &y)
{
  y["cursorPos"] = cursorPos;
  y["visCursIndex"] = visCursIndex;
  y["visMagnify"] = visMagnify;
  y["zoomScale"] = zoomScale;
  y["showRerunsWindow"] = showRerunsWindow;
  YAML::Node newInsets;
  for (auto &it : insets) {
    YAML::Node cellList;
    for (auto cell : it->cells) {
      cellList.push_back(sh.nameByCell[cell]);
    }
    newInsets[it->windowName] = cellList;
  }
  y["insets"] = newInsets;
}

void SheetUi::rdUiState(YAML::Node &y)
{
  if (auto it = y["cursorPos"]) {
    cursorPos = it.as<vec3>();
  }
  if (auto it = y["visCursIndex"]) {
    visCursIndex = int(it.as<double>());
  }
  if (auto it = y["visMagnify"]) {
    visMagnify = max(1, int(it.as<double>()));
  }
  if (auto it = y["zoomScale"]) {
    zoomScale = it.as<double>();
  }
  if (auto it = y["showRerunsWindow"]) {
    showRerunsWindow = it.as<bool>();
  }

  if (auto it = y["insets"]) {
    
    for (auto kv : it) {
      auto windowName = kv.first.as<string>();

      for (auto inset : insets) {
        if (windowName == inset->windowName || (windowName == "Inset 1" && inset->windowName == "R")) {
          for (auto cellit : kv.second) {
            auto cellName = cellit.as<string>();
            inset->cells[sh.getCell(cellName)] = true;
            inset->windowOpen = true;
          }
        }
      }
    }
  }
}


PageIndex SheetUi::getNewCellPage()
{
  PageIndex bestPage = 0;
  F bestDist = 9999;
  for (auto cell : sh.liveCells) {
    auto page = sh.pageByCell[cell];
    if (visByPage[page]) {
      vec2 relpos = sh.posByCell[cell] - mousePickPos.head<2>();
      if (relpos.y() > 0.0f) relpos.y() *= 0.5f;
      F dist = relpos.norm();
      if (dist < bestDist) {
        bestDist = dist;
        bestPage = page;
      }
    }
  }
  return bestPage;
}

vec3 SheetUi::cellDisplayPos(CellIndex cell)
{
  return vec3(sh.posByCell[cell].x(), sh.posByCell[cell].y(), 0.0f);
}


void SheetUi::setSheetBounds()
{
  sheetBoundsLo = vec3(0,0,0);
  sheetBoundsHi = vec3(0,0,0);
  for (auto cell : sh.liveCells) {
    auto &pos = sh.posByCell[cell];
    sheetBoundsHi.x() = max(sheetBoundsHi.x(), pos.x() + totWidthByCell[cell]);
    sheetBoundsHi.y() = max(sheetBoundsHi.y(), pos.y());
    sheetBoundsLo.x() = min(sheetBoundsLo.x(), pos.x());
    sheetBoundsLo.y() = min(sheetBoundsLo.y(), pos.y() - totHeightByCell[cell]);
  }
}


void SheetUi::calcSheet()
{
  sh.doDebug = true;
  sh.compile(Emit_ui);

  bool wasNull = !activeTrace;

  if (liveClient && liveClient->running()) {
    // WRITEME: extend previous trace
    if (!recalcActive) {
      recalcActive = true;
      auto trace = make_shared<Trace>(sh.compiled, false, false);
      trace->setupInitial();
      trace->limitMaxIndex(liveClient->getRunningTick());
      trace->randomSeed = 0; // FIXME
      trace->replayBuffer = liveClient->replayBuffer;
      trace->startTime = liveClient->replayBuffer->startTime;
      simulateTraceAsync(std::move(trace), [this, keepalive = shared_from_this()](shared_ptr<Trace> trace1) {
#if !defined(__EMSCRIPTEN__)
        // We need this on the main thread, though not in Emscripten
        asio::post(io_context, [this, trace2 = std::move(trace1), keepalive]() {
          activeTrace = std::move(trace2);
#else
          activeTrace = std::move(trace1);
#endif
          recalcActive = false;
          uiPollLive = true;
          visCursIndex = max(0, activeTrace->nVals() - 1);
          visCursTime = activeTrace->getTimeForIndex(visCursIndex);
          setVisRange(0, activeTrace->maxIndex);
#if !defined(__EMSCRIPTEN__)
        });
#endif
      });
    }
  }
  if (!liveClient && reruns) {
    activeTrace = reruns->displayTrace();
  }

  if (reruns) {
    reruns->insertFromDb();
    reruns->update();
  }

  if (activeTrace) {
    if (!activeTrace->replayBuffer) {
      activeTrace->extractReplayBuffer();
    }

    int vsize = int(activeTrace->vals.size());
    if (wasNull) {
      setVisRange(0, min(vsize, activeTrace->maxIndex + 1));
    }
    else {
      if (liveClient && liveClient->running()) {
      }
      else {
        setVisRange(min(visBeginIndex, vsize - 1), vsize); // min(visEndIndex, vsize));
      }
    }
  }
}

void SheetUi::startLive()
{
  if (liveClient) {
    liveClient->stop();
  }
  else if (sh.compiled->apis.size()) {

    string daemon_robot_host = "localhost";
    for (auto const &api : sh.compiled->apis) {
      if (auto host = assoc_string_view(api->apiProps, "daemon_robot_host")) {
        daemon_robot_host = *host;
        break;
      }
    }
    W() << "startLive on " << daemon_robot_host;

    liveClient = make_shared<SimRobotClient>(sh.compiled);
    liveClient->startWebsocketClient(daemon_robot_host, "6912", "/throbol-robot");
    if (!liveClient) {
      Wred() << "No live robots configured for this sheet";
    }
  }
}


void SheetUi::setVisRange(int _begin, int _end)
{
  if (!activeTrace) return;
  if (activeTrace->vals.empty()) {
    visBeginIndex = visEndIndex = 0;
    visBeginTime = visEndTime = 0.0f;
  }
  else {
    visBeginIndex = _begin;
    visEndIndex = min(_end, (int)activeTrace->nVals());
    visBeginTime = activeTrace->getTimeForIndex(visBeginIndex);
    visEndTime = activeTrace->getTimeForIndex(visEndIndex);
  }
}

void SheetUi::initGlobalDo()
{

  if (IsModCmd() && IsKeyPressed(ImGuiKey_Comma)) {
    win.showDemoWindow = true;
    win.showMetricsWindow = true;
    showUndoWindow = true;
    showDebugWindow = true;
  }

  if (IsModCmd() && IsKeyPressed(ImGuiKey_U)) { // ⌘U
    showUndoWindow = !showUndoWindow;
  }
  if (IsModCmd() && IsKeyPressed(ImGuiKey_E)) { // ⌘E
    showPagesWindow = !showPagesWindow; 
  }
  if (IsModCtrl() && IsKeyPressed(ImGuiKey_R)) { // ^R
    showRerunsWindow = !showRerunsWindow;
  }
  if (IsModShiftCmd() && IsKeyPressed(ImGuiKey_D)) { // ⇧⌘D
    showDebugWindow = !showDebugWindow;
  }
  if (IsModShiftCmd() && IsKeyPressed(ImGuiKey_A)) { // ⇧⌘A
    showApiWindow = !showApiWindow;
  }
  if (IsModCmd() && IsKeyPressed(ImGuiKey_H)) { // ⌘H
    showHelpWindow = !showHelpWindow;
  }
  if (IsModShiftCmd() && IsKeyPressed(ImGuiKey_M)) { // ⇧⌘M
    win.showMetricsWindow = !win.showMetricsWindow;
  }
  if (IsModOptionShiftCmd() && IsKeyPressed(ImGuiKey_D)) { // ⌘⌥⇧D
    win.showDemoWindow = !win.showDemoWindow;
  }
  if (IsModCtrl() && IsKeyPressed(ImGuiKey_M)) { // ^M
    showMultipleValues = !showMultipleValues;
  }

  if (IsModCmd() && IsKeyPressed(ImGuiKey_S, false)) { // ⌘S
    doSave = true;
  }
  if (IsModShiftCmd() && IsKeyPressed(ImGuiKey_E, false)) { // ⇧⌘S
    doExport = true;
  }
  doReload = IsModShiftCmd() && IsKeyPressed(ImGuiKey_R); // ⇧⌘R

  doNewCell = doCut = doCopy = doPaste = false;
  if (navMode == Nav_Sheet) {
    doNewCell = IsModCtrl() && IsKeyPressed(ImGuiKey_O); // ^O
    doCut = IsModCmd() && IsKeyPressed(ImGuiKey_X, false);
    doCopy = IsModCmd() && IsKeyPressed(ImGuiKey_C, false);
    doPaste = IsModCmd() && IsKeyPressed(ImGuiKey_V, false);
  }

  doUndo = doRedo = false;
  if (navMode == Nav_Sheet) {
    doUndo = IsModCmd() && IsKeyPressed(ImGuiKey_Z); // ⌘Z
    doRedo = IsModShiftCmd() && IsKeyPressed(ImGuiKey_Z); // ⇧⌘Z
  }

  if (navMode == Nav_Sheet || navMode == Nav_Search) {
    doSearch = IsModCtrl() && IsKeyPressed(ImGuiKey_S); // ^S
  }

  doReparse = IsModShiftCmd() && IsKeyPressed(ImGuiKey_L); // ⇧⌘L
  doStartLive = IsModCtrl() && IsKeyPressed(ImGuiKey_L); // ^L

  /*
    Escape exits out of various modes. Proper usage is
      if (inMode() && doEscape) {
        doEscape = false;
        cancelMode();
      }
    so that it cancels one mode per keypress in the somewhat arbitrary priority order
    of which gets checked first
  */
  doEscape = !IsModCmd() && IsKeyPressed(ImGuiKey_Escape);
  if (doEscape) {
    defaultScrollDir = 0.0;
  }

  ctlSetupKnobs();

  if (uiPerfTestMode) {
    if (uiPerfTestCounter == 0) {
      uiPerfTestCenter = cursorPos;
      L() << "uiPerfTest: orbit around " << uiPerfTestCenter;
    }
    uiPerfTestCounter++;
    if (uiPerfTestCounter == 500) {
      win.wantClose = true;
    }
    uiPollLive = true;
    F th = uiPerfTestCounter * 0.05f;
    cursorPos = uiPerfTestCenter + vec3(
      sin(th) * 10.0f, cos(th) * 10.0f, 0);
  }
}

ImVec4 rerunColor(Trace *tr, bool running, bool enabled)
{
  if (!tr) {
    return ImVec4(0.5f, 0.5f, 0.5f, 0.5f);
  }

  auto errors = tr->checkFailuresByCell.size();
  F alpha = running ? 0.5f : 1.0f;
  if (!enabled) {
    return ImVec4(0.5f, 0.5f, 0.5f, alpha);
  }
  else if (errors > 0) {
    return ImVec4(1.0f, 0.0f, 0.0f, alpha);
  }
  else {
    return ImVec4(0.7f, 1.0f, 0.7f, alpha);
  }
}

void SheetUi::prepareDraw()
{
  sheetView.clear();
  for (auto &it : insets) {
    it->dl.clear();
  }
}

void SheetUi::drawMainUi()
{
  ImVec2 wpos(4, 4);
  if (showPagesWindow) {
    SetNextWindowPos(wpos, ImGuiCond_Always);
    if (Begin("Pages", nullptr, ImGuiWindowFlags_AlwaysAutoResize)) {
      drawPagesWindowContents();
    }
    wpos.y += GetWindowSize().y + 4;
    End();
  }
  if (showRerunsWindow && reruns) {
    SetNextWindowPos(wpos, ImGuiCond_Always);
    if (Begin("Reruns", &showRerunsWindow, ImGuiWindowFlags_AlwaysAutoResize)) {
      drawRerunsWindowContents();
    }
    wpos.y += GetWindowSize().y + 4;
    End();
  }
  if (showUndoWindow) {
    SetNextWindowPos(wpos, ImGuiCond_Always);
    SetNextWindowSize(ImVec2(200, 200), ImGuiCond_FirstUseEver);
    if (Begin("Undo Log", &showUndoWindow)) {
      drawUndoWindowContents();
    }
    wpos.y += GetWindowSize().y + 4;
    End();
  }
  if (showHelpWindow) {
    SetNextWindowPos(wpos, ImGuiCond_Always);
    if (Begin("Help", &showHelpWindow, ImGuiWindowFlags_AlwaysAutoResize)) {
      drawHelpWindowContents();
    }
    wpos.y += GetWindowSize().y + 4;
    End();
  }
  if (showDebugWindow) {
    if (Begin("Debug", &showDebugWindow, ImGuiWindowFlags_AlwaysAutoResize)) {
      drawDebugWindowContents();
    }
    End();
  }
  if (showApiWindow && sh.ok) {
    SetNextWindowSize(ImVec2(400, 800), ImGuiCond_FirstUseEver);
    if (Begin("API", &showApiWindow)) {
      drawApiWindowContents();
    }
    End();
  }
  if (sh.ok && activeTrace) {
    drawInsetWindows(*activeTrace);
  }
  if (sh.ok) {
    drawSearchWindow();
  }
}

void SheetUi::drawUndoWindowContents()
{
  auto undoBegin = sh.undoLog.size() - min(sh.undoLog.size(), size_t(10));
  auto undoEnd = sh.undoLog.size();
  auto i = undoEnd;
  while (i > undoBegin) {
    i--;
    auto undo = sh.undoLog[i].get();
    if (!undo) continue;
    ostringstream ss;
    undo->print(ss, sh);
    Text("%s%s", ss.str().c_str(),
      (i == undoEnd - 1 && !undo->nomore) ? "..." : "");
    if (i == sh.undoLogPos) {
      Separator();
    }
  }
  if (undoBegin > 0) {
    // Can I, instead, detect when we're no longer visible?
    Text("...%d more", int(undoBegin));
  }
}

void SheetUi::drawInsetWindows(Trace &tr)
{

  for (auto &it : insets) {
    if (!it->windowOpen) continue;

    SetNextWindowBgAlpha(0.0f);
    auto ds = GetIO().DisplaySize;
    auto winFlags = ImGuiWindowFlags_AlwaysVerticalScrollbar |
      ImGuiWindowFlags_NoNav |
      ImGuiWindowFlags_NoFocusOnAppearing;
    if (it->windowName == "R") {
      SetNextWindowSize(ImVec2(
        max(150.0f, min((ds.y - 20.0f) * 0.33f, ds.x * 0.20f)),
        ds.y - 20.0f),
        ImGuiCond_FirstUseEver);
      // Needed because dragging the right or top edge can result in crazy sizes.
      // This keeps it in bounds.
      SetNextWindowSizeConstraints(ImVec2(
          150.0f,
          150.0f),
        ImVec2(
          ds.x / 2,
          ds.y - 20));
      SetNextWindowPos(ImVec2(
        ds.x - 4.0f,
        0.0f),
        ImGuiCond_Always,
        ImVec2(1.0f, 0.0f));
      winFlags |= ImGuiWindowFlags_NoMove;
    }
    else if (it->windowName == "L") {
      SetNextWindowSize(ImVec2(
        min(500.0f, ds.x * 0.20f),
        ds.y * 0.25f),
        ImGuiCond_FirstUseEver);
      SetNextWindowSizeConstraints(ImVec2(
          150.0f,
          150.0f),
        ImVec2(
          ds.x / 2,
          ds.y - 20));
      SetNextWindowPos(ImVec2(
        4.0f,
        ds.y),
        ImGuiCond_Always,
        ImVec2(0.0f, 1.0f));
      winFlags |= ImGuiWindowFlags_NoMove;
    }

    if (Begin(it->windowName.c_str(), &it->windowOpen, winFlags)) {

      ImGuiWindow* window = GetCurrentWindow();

      auto wr = window->InnerRect;
      it->dl.vpL = wr.Min.x;
      it->dl.vpR = wr.Max.x;
      it->dl.vpT = wr.Min.y;
      it->dl.vpB = wr.Max.y;

      auto vscale = wr.GetWidth() / 5.0f; // pixels per coord unit
      it->dl.lookat = vec3(0.0f, -GetScrollY() / vscale, 0);
      it->dl.eyepos = it->dl.lookat + vec3(0, 0, 20);
      it->dl.up = vec3(0, 1, 0);
      it->dl.fov = F(22.0 * M_PI/180.0);
      it->dl.setProjView();

      if (IsWindowHovered()) {
        it->dl.setPickRay();
      }
      else {
        it->dl.clearPickRay();
      }

      vec4 wipeCol = bgcolor(0x00, 0x00, 0x00, 0xcc);
      it->dl.addQuad(
        vec3(-2, -22, 0), wipeCol,
        vec3(+8, -22, 0), wipeCol,
        vec3(-8, +2, 0), wipeCol,
        vec3(+8, +2, 0), wipeCol,
        -1.0f, &maskShader);

      auto glo = getLayoutGlobal();

      CellVals *cursVals = nullptr;
      if (tr.compiled == sh.compiled) {
        cursVals = tr.getVal(glo.cursIndex);
      }

      vec3 curpos(0, 0, 0);
      for (auto cell : it->cells) {
        auto lo = getLayoutCell(glo, cell, curpos, true, false, false);
        drawCell(it->dl, cell, cursVals, lo);
        drawCellData(it->dl, cell, cursVals, lo, tr);
        curpos += vec3(0, -lo.totHeight, 0);
      }
      SetCursorPosY(-curpos.y() * vscale);
      if (0) {
        L() << "cs=" << window->ContentSize.x << "," << window->ContentSize.y <<
          " scroll=" << window->Scroll.y << " / " << window->ScrollMax.y;
      }
    }
    End();
  }
}

void SheetUi::drawSearchWindow()
{
  if (navMode == Nav_Search) {
    auto ds = GetIO().DisplaySize;
    SetNextWindowPos(ImVec2(0.0f, ds.y - 20.0f), ImGuiCond_Always, ImVec2(0.0f, 1.0f));
    if (Begin("Search", nullptr, ImGuiWindowFlags_NoDecoration|ImGuiWindowFlags_NoInputs|ImGuiWindowFlags_NoMove|ImGuiWindowFlags_AlwaysAutoResize)) {
      if (searchHits.empty() && !searchPrefix.empty()) {
        PushStyleColor(ImGuiCol_Text, IM_COL32(0xff, 0x99, 0x99, 0xff));
      }
      else {
        PushStyleColor(ImGuiCol_Text, IM_COL32(0xff, 0xff, 0xff, 0xff));
      }
      if (searchIndex >= 0 && !searchHits.empty()) {
        Text("Search: %s (%d / %d matches)", searchPrefix.c_str(), (searchIndex % int(searchHits.size())) + 1, int(searchHits.size()));
      }
      else {
        Text("Search: %s (%d matches)", searchPrefix.c_str(), int(searchHits.size()));
      }
      PopStyleColor();
    }
    End();
  }
}

void SheetUi::drawDebugWindowContents()
{
  Textp() << "sheet files: " << sh.fileNameByPage;
  Textp() << "  .compiled: " << sh.compiled.get();
  Textp() << "  .sheetlife.totalAlloc: " << sh.sheetlife.totalAlloc();
  if (sh.compiled) {
    Textp() << "  .compiled.buildlife.totalAlloc: " << sh.compiled->buildlife.totalAlloc();
    Textp() << "  .compiled.padSize: " << sh.compiled->padSize1 << "/" <<  sh.compiled->padSize8 << "/" <<  sh.compiled->padSize32;
    Textp() << "  .compiled.evalFuncs.size: " << sh.compiled->evalFuncs1.size() << " bytes=" << sh.compiled->evalFuncs1.size_bytes();
  }
  Textp() << "activeTrace: " << activeTrace.get();
  if (activeTrace) {
    auto &tr = *activeTrace;
    Textp() << "  .tracelife.totalAlloc: " << tr.tracelife->totalAlloc();
    Textp() << "  .compiled: " << tr.compiled.get();
    Textp() << "  .maxIndex: " << tr.maxIndex;
  }
  Textp() << "vis: " << visBeginIndex << " - " << visEndIndex << " @ " << visCursIndex;
  Textp() << "  visibleCells.size=" << visibleCells.size();
  Textp() << "  zoomScale=" << zoomScale << " screenScale=" << screenScale;
  Textp() << "liveClient: " << liveClient.get();
  if (liveClient) {
    Textp() << "  .label: " << liveClient->label;
    Textp() << "  .state: " << liveClient->state;
    Textp() << "  .replayBuffer: " << liveClient->replayBuffer.get();
  }
  if (reruns) {
    Textp() << "reruns.size=" << reruns->reruns.size();
    Textp() << "selectedRerun=" << reruns->selectedRerun.get() << " seed=" << reruns->selectedSeed << "/" << reruns->numSeeds;
    if (auto sel = reruns->selectedRerun) {
      Textp() << " .traceName=" << sel->traceName;
      Textp() << " .traceBySeed.size=" << sel->traceBySeed_.size();
      Textp() << " .running=" << sel->running << ", enabled=" << sel->enabled;
    }
  }
  {
    Textp t;
    AllocTracker::printStats(t.s);
  }
  {
    Textp t;
    win.printWebgpuStats(t.s);
    t.s << "traceTexCache.size=" << traceTexCache.size() << "\n";
  }
}


void SheetUi::drawPagesWindowContents()
{
  for (PageIndex page = 0; page < sh.nPages; page++) {
    bool vis = visByPage.at(page);
    if (Checkbox(sh.fileNameByPage[page].c_str(), &vis)) {
      visByPage.at(page) = vis;
    }
  }
}

void SheetUi::drawRerunsWindowContents()
{
  if (sh.compiled && sh.compiled->hasLiveApi) {
    if (Button("Live")) {
      reruns->select(nullptr);
      doStartLive = true;
    }
  }
  if (SliderInt("Seeds", &reruns->numSeeds, 1, 20)) {

  }
  if (reruns->numSeeds > 1) {
    Checkbox("Show all seeds", &reruns->showAllSeeds);
  }

  bool selectNextRerun = false;

  for (auto rerunit = reruns->reruns.rbegin(); rerunit != reruns->reruns.rend(); rerunit++) {
    if (auto rerun = *rerunit) {
      if (selectNextRerun) {
        selectNextRerun = false;
        reruns->select(rerun, 0);
      }
      string traceLabel;
      if (rerun->isPure()) {
        traceLabel = "Pure sheet";
      }
      else {
        traceLabel = rerun->traceName;
      }
      if (reruns->numSeeds == 1 || !rerun->isPure()) {
        PushStyleColor(ImGuiCol_Text, rerunColor(rerun->getTraceBySeed(0), rerun->running, rerun->enabled));
        if (Selectable(traceLabel.c_str(), rerun == reruns->selectedRerun && 0 == reruns->selectedSeed)) {
          reruns->select(rerun, 0);
        }
        PopStyleColor();
      }
      else {
        Text("%s", traceLabel.c_str());
        for (int seed = 0; seed < reruns->numSeeds; seed++)  {
          PushStyleColor(ImGuiCol_Text, rerunColor(rerun->getTraceBySeed(seed), rerun->running, rerun->enabled));
          //SameLine();
          if (Selectable(repr(seed).c_str(), rerun == reruns->selectedRerun && seed == reruns->selectedSeed)) {
            reruns->select(rerun, seed);
          }
          PopStyleColor();
        }
      }
      if (IsItemHovered() && IsKeyPressed(ImGuiKey_Backspace)) {
        if (reruns->selectedRerun) selectNextRerun = true;
        reruns->remove(rerun);
      }
    }
  }
}

void SheetUi::drawHelpWindowContents()
{
#if defined(__EMSCRIPTEN__)
  Text("WASD to move, QE to zoom. Or 2-finger drag");
#else
  Text("WASD to move, QE to zoom. Or 2-finger drag & pinch");
#endif
  Text("Left/right arrows to scrub time. Or shift-2-finger-drag");
  Text("Hover over a blue knob and shift-2-finger-drag to modify parameters");
  Text("Click to select a formula. Click again to edit (Emacs keybindings)");
  Text("Control-click for menu");
#if defined(__EMSCRIPTEN__)
  Text("This web version has some limitations. Build the desktop app for more speed & features");
#endif
}

char const *apiDirectionLabel(ApiDirection dir)
{
  switch (dir) {
    case API_NONE:
      return ".";
    case API_SENSOR:
      return ">";
    case API_ACTUATOR:
      return "<";
    case API_MONITOR:
      return ">";
    default:
      return "?";
  }
}


void SheetUi::drawApiWindowContents()
{
  if (sh.compiled) {
    bool firstApi = true;
    for (auto &api : sh.compiled->apis) {
      if (!firstApi) {
        NewLine();
      }
      PushFont(ysFonts.lrgFont);
      PushStyleColor(ImGuiCol_Text, IM_COL32(0xdd, 0xdd, 0xff, 0xff));
      Text("API %s type=%s version=%s", api->apiInstance.c_str(), api->apiType.c_str(), api->apiVersion.c_str());
      PopStyleColor();
      PopFont();
      for (auto &err : api->errors) {
        PushStyleColor(ImGuiCol_Text, IM_COL32(0xff, 0x77, 0x77, 0xff));
        Text("%s", err.c_str());
        PopStyleColor();
      }

      std::array<vector<GenericApiRef *>, 2> cols;
      for (auto ref : api->refs) {
        if (ref) {
          switch (ref->direction) {
            case API_SECTION:
              while (cols[0].size() < cols[1].size()) cols[0].push_back(nullptr);
              while (cols[1].size() < cols[0].size()) cols[1].push_back(nullptr);
              cols[0].push_back(ref);
              cols[1].push_back(nullptr);
              break;
            case API_SENSOR:
            case API_MONITOR:
            default:
              cols[1].push_back(ref);
              break;
            case API_ACTUATOR:
              cols[0].push_back(ref);
              break;
          }
        }
      }
      Indent(30.0);
      Columns(2);
      //Text("Sensors"); NextColumn();
      //Text("Actuators"); NextColumn();
      //Separator();
      for (size_t rowi = 0; ; rowi++) {
        bool rowExists = false;
        for (size_t coli = 0; coli < cols.size(); coli++) {
          if (rowi < cols[coli].size()) {
            rowExists = true;
            if (auto ref = cols[coli][rowi]) {
              if (ref) {
                if (ref->direction == API_SECTION) {
                  Columns(1);
                  Unindent(15.0);
                  PushFont(ysFonts.lrgFont);
                  PushStyleColor(ImGuiCol_Text, IM_COL32(0xcc, 0xcc, 0xff, 0xff));
                  Text("%s", ref->name.c_str());
                  PopStyleColor();
                  PopFont();
                  Indent(15.0);
                  Columns(2);
                }
                else {
                  auto cell = sh.getCell(ref->name);
                  if (cell != nullCellIndex) {
                    // WRITEME: should be able to click to go there
                    PushStyleColor(ImGuiCol_Text, IM_COL32(0xff, 0xff, 0xff, 0xff));
                  }
                  else {
                    PushStyleColor(ImGuiCol_Text, IM_COL32(0xff, 0xdd, 0xaa, 0xff));
                  }
                  Text("%s %s ∊ %s%s%s",
                    apiDirectionLabel(ref->direction),
                    ref->name.c_str(),
                    typeName(ref->br.t).c_str(),
                    ref->comment.empty() ? "" : " // ",
                    ref->comment.c_str());
                  PopStyleColor();
                }
              }
            }
          }
          NextColumn();
        }
        if (!rowExists) break;
      }
      Columns(1);
      Unindent(30.0);
      firstApi = false;
    }
  }
}


void SheetUi::execGlobalDoPost()
{
  computePolicyTerrain();
  if (doSave) {
    doSave = false;
    if (sh.ok) {
      auto y = sh.getUiSaveState();
      wrUiState(y);
      sh.saveSheet();
    }
  }
  if (doExport) {
    doExport = false;
    if (activeTrace) {
      exportCells(*activeTrace);
    }
  }
  if (doReload) {
    doReload = false;
    auto oldFiles = sh.fileNameByPage;
    sh = Sheet();
    loadDocument(oldFiles);
  }
  if (doReparse) {
    doReparse = false;
    sh.setNeedsParseAll();
    if (liveClient) {
      liveClient->stop();
      liveClient = nullptr;
    }
  }
  if (doNewCell) {
    doNewCell = false;
    auto nc = sh.mkCell("new");
    if (nc == nullCellIndex) {
      Wred() << "Can't create new cell";
    }
    else {
      if (0) L() << "New cell " << nc;
      sh.setCellPos(nc, mousePickPos.head<2>().array().round());
      sh.setCellPage(nc, getNewCellPage());
      selectedCells.clear();
      selectedCells[nc] = true;
      setEditItem(PickItem(PickCell, nc));
      editCellCursorPos = 0;
    }
  }
  if (doStartLive) {
    doStartLive = false;
    navMode = Nav_Sheet;
    startLive();
  }
  else if (liveClient) {
    std::array<bool, API_KBD_NKEYS> keysDown = {};
    for (int i = 0; i < API_KBD_NKEYS; i++) {
      keysDown[i] = IsKeyDown(ImGuiKey(ImGuiKey_NamedKey_BEGIN + i));
    }
    liveClient->txUpdateKbd(keysDown.data());
    if (liveClient->done()) {
      W() << "liveClient is done, closing\n";
      for (auto &it : liveClient->chunkList) {
        SimFetcherClient::fetchBlobFile(it);
      }
      SimFetcherClient::fetchBlobFiles();
      if (reruns && liveClient->replayBuffer) {
        auto rb = liveClient->replayBuffer;
        if (rb->startTime != 0.0 && !rb->empty()) {
          reruns->add(rb, liveClient->chunkList);
        }
      }

      liveClient->stop();
      liveClient = nullptr;
    }
    else if (liveClient->error()) {
      W() << "liveClient error, closing\n";
      liveClient->stop();
      liveClient = nullptr;
    }
    else if (doEscape) {
      W() << "Requesting liveClient stop\n";
      doEscape = false;
      liveClient->stopRobot();
    }
  }
}


void SheetUi::setupMainUi()
{
  cursorVel = vec3(0, 0, 0);

  //ctlSheetView(sheetView);
}

/*
  Override from MainUi, this is where we do most of the work
*/
void SheetUi::drawBackground()
{
  if (sheetView.vpR == 0) {
    setProjView(sheetView);
  }

  sheetView.setPickRay();
  if (GetCurrentContext()->HoveredWindow == nullptr) {
    if (sheetView.pickRayActive) {
      ctlSheetView(sheetView);
      ctlSheetContent(sheetView);
    }
  }
  ctlSheetViewMenus1();
  sheetView.setInterval();
  if (0) sheetView.addArrowTriple(mat4::Identity());
  updateSheetData();
  updateVisibleCells();
  updatePolicyTerrain();
  calcSheet();
  drawSheetPane(sheetView);
  ctlSheetViewMenus2();
}

void SheetUi::updateVisibleCells()
{
  visibleCells.clear();

  auto visreg = sheetView.planeVisibleRegion(vec3(0, 0, 0), vec3(0, 0, 1));
  if (0) L() << "visreg=" << visreg;
  if (visreg.empty()) return;
  auto minX = visreg[0].x();
  auto maxX = visreg[0].x();
  auto minY = visreg[0].y();
  auto maxY = visreg[0].y();
  for (auto &pt : visreg) {
    minX = min(minX, pt.x());
    maxX = max(maxX, pt.x());
    minY = min(minY, pt.y());
    maxY = max(maxY, pt.y());
  }
  auto margin = +1.0f;
  minX -= margin;
  maxX += margin;
  minY -= margin;
  maxY += margin;
  if (0) L() << "visreg y=" << minY << " " << maxY;

  for (auto cell : sh.liveCells) {
    if (!visByPage.at(sh.pageByCell.at(cell)) && !selectedCells[cell]) continue;
    auto pos = cellDisplayPos(cell);
    // prune offscreen cells
    if (pos.x() + totWidthByCell[cell] < minX || 
        pos.x() > maxX || 
        pos.y() < minY || 
        pos.y() - totHeightByCell[cell] > maxY) continue;

    visibleCells[cell] = true;
  }

  for (auto &inset : insets) {
    if (inset) {
      for (auto cell : inset->cells) {
        visibleCells[cell] = true;
      }
    }
  }

}


void SheetUi::updateSheetData()
{
  formulaWidthByCell.resize(sh.nCells, 5.0f);
  formulaHeightByCell.resize(sh.nCells, 0.25f);
  totWidthByCell.resize(sh.nCells, 2.0f);
  totHeightByCell.resize(sh.nCells, 1.0f);

  recentVideoTexByCell.resize(sh.nCells);
  knobPosByParam.resize(sh.nParams, vec3(0, 0, 0));
  charPosMapByCell.resize(sh.nCells);
  formulaStartByCell.resize(sh.nCells);
  nameStartByCell.resize(sh.nCells);
  nameEndByCell.resize(sh.nCells);
  visByPage.resize(sh.nPages, true);
}


void SheetUi::ctlSheetViewMenus1()
{
  if (beginPickItem()) {
    if (pickItem.isCell()) {
      CellIndex cell = pickItem.asCellIndex();
      string typeDesc = "?";
      if (sh.compiled) {
        typeDesc = typeName(sh.compiled->typeByCell[cell]);
      }
      if (selectedCells.size() > 1) {
        Text("%d Cells ", int(selectedCells.size()));
        if (BeginMenu("Sheet")) {

          vector<bool> selPages(sh.nPages);
          for (auto sci : selectedCells) {
            selPages.at(sh.pageByCell[sci]) = true;
          }

          for (PageIndex newpagei = 0; newpagei < sh.nPages; newpagei++) {
            if (RadioButton(sh.fileNameByPage[newpagei].c_str(), selPages[newpagei])) {
              for (auto sci : selectedCells) {
                sh.pageByCell[sci] = newpagei;
              }
            }
          }
          EndMenu();
        }
      }
      else {
        Text("Cell %s ∊ %s", 
          sh.nameByCell[cell].c_str(), typeDesc.c_str());
        if (sh.nPages > 1) {
          auto &pagei = sh.pageByCell[cell];
          if (BeginMenu(("Sheet " + sh.fileNameByPage[pagei]).c_str())) {
            for (PageIndex newpagei = 0; newpagei < sh.nPages; newpagei++) {
              if (RadioButton(sh.fileNameByPage[newpagei].c_str(), newpagei == pagei)) {
                pagei = newpagei;
              }
            }
            EndMenu();
          }
        }
        if (BeginMenu("Inset")) {
          for (auto &it : insets) {
            bool inInset = it->cells[cell];
            char const *shortcut = nullptr;
            if (it->windowName == "R") {
              shortcut = "^2";
            }
            else if (it->windowName == "L") {
              shortcut = "^1";
            }
            if (MenuItem(it->windowName.c_str(), shortcut, inInset)) {
              it->toggleCell(cell);
              CloseCurrentPopup();
            }
          }
          EndMenu();
        }
      }
      Separator();
      if (sh.getCellType(cell).ist<F>() && 
          !sh.getCellIsInput(cell) &&
          !sh.getCellIsPureParam(cell)) {
        bool isReward = rewardCells[cell];
        if (Checkbox("Show Reward", &isReward)) {
          rewardCells[cell] = isReward;
          CloseCurrentPopup();
        }
      }

      if (sh.getCellIsInput(cell) && !adjustKnobNames.empty()) {
        Separator();
        if (BeginMenu("Assign to Knob")) {
          auto knobi = 0;
          for (auto &it : adjustKnobNames) {
            if (MenuItem(it)) {
              knobAttachment[knobi] = pickItem;
            }
            knobi++;
          }
          EndMenu();
        }
      }

    }
    else if (pickItem.isParam()) {
      ParamIndex param = pickItem.asParamIndex();
      CellIndex cell = sh.cellByParam[param];
      int ci = find(sh.paramsByCell[cell].begin(), sh.paramsByCell[cell].end(), param) - sh.paramsByCell[cell].begin();
      Text("Param %s@%d %s ~ %s", sh.nameByCell[cell].c_str(), ci, repr_0_3g(sh.params.valueByParam(param)).c_str(), repr_0_1g(sh.rangeByParam[param]).c_str());
      Separator();
      
      if (adjustKnobNames.size() && BeginMenu("Assign to Knob")) {
        auto knobi = 0;
        for (auto &it : adjustKnobNames) {
          if (MenuItem(it)) {
            knobAttachment[knobi] = pickItem;
          }
          knobi++;
        }
        EndMenu();
      }
      if (MenuItem("Increase Range", ">")) {
        cmdIncreaseParamRange(param);
      }
      if (MenuItem("Decrease Range", "<")) {
        cmdDecreaseParamRange(param);
      }
      if (MenuItem("Zero", "0")) {
        cmdSetParam(param, 0.0f);
      }
      if (MenuItem("Quantize", "9")) {
        cmdQuantizeParam(param, 10.0f);
      }
    }
    else if (pickItem.isNone()) {
      if (MenuItem("New Cell", "⌃O")) {
        doNewCell = true;
      }
      if (MenuItem("Undo", "⌘Z")) {
        doUndo = true;
      }
      if (MenuItem("Redo", "⇧⌘Z")) {
        doRedo = true;
      }
      if (MenuItem("Save", "⌘S")) {
        doSave = true;
      }
      if (MenuItem("Export", "⇧⌘E")) {
        doExport = true;
      }

      if (MenuItem("Cut", "⌘X")) {
        doCut = true;
      }
      if (MenuItem("Copy", "⌘C")) {
        doCopy = true;
      }
      if (MenuItem("Paste", "⌘V")) {
        doPaste = true;
      }
      if (MenuItem("Search", "^S")) {
        doSearch = true;
      }
      if (MenuItem("Clear Rewards", nullptr, false, !rewardCells.empty())) {
        doClearRewards = true;
      }

      Separator();
      if (MenuItem("Reload", "⇧⌘R")) {
        doReload = true;
      }
      if (MenuItem("Reparse", "⇧⌘L")) {
        doReparse = true;
      }
      if (MenuItem("Start Live", "^L")) {
        doStartLive = true;
      }
      if (reruns && navMode == Nav_Sheet) {
        Separator();
        if (MenuItem("Prev Rerun", "[")) {
          reruns->selectPrevSeed();
        }
        if (MenuItem("Next Rerun", "]")) {
          reruns->selectNextSeed();
        }
        if (MenuItem("Toggle Show All", "\\")) {
          reruns->toggleShowAllSeeds();
        }
      }
      Separator();
      if (MenuItem("Show Reruns", "^R", showRerunsWindow)) {
        showRerunsWindow = !showRerunsWindow;
      }
      if (MenuItem("Show Debug", "⇧⌘D", showDebugWindow)) {
        showDebugWindow = !showDebugWindow;
      }
      if (MenuItem("Show Help", "⇧⌘H", showHelpWindow)) {
        showHelpWindow = !showHelpWindow;
      }
      if (MenuItem("Show Sheet APIs", "⇧⌘A", showApiWindow)) {
        showApiWindow = !showApiWindow;
      }
#if !defined(EMSCRIPTEN_NOTYET)
      if (MenuItem("Show Metrics", "⇧⌘M", win.showMetricsWindow)) {
        win.showMetricsWindow = !win.showMetricsWindow;
      }
      if (MenuItem("Show Imgui Demo", "⌘⌥⇧D", win.showDemoWindow)) {
        win.showDemoWindow = !win.showDemoWindow;
      }
#endif
      if (MenuItem("Show Multiple Values", "^M", showMultipleValues)) {
        showMultipleValues = !showMultipleValues;
      }
    }
    EndPopup();
  }
}


void SheetUi::ctlSheetViewMenus2()
{
  if (beginPickItem()) {
    if (pickItem.isCell()) {
      CellIndex cell = pickItem.asCellIndex();

      Separator();
      if (MenuItem("Delete cell")) {
        sh.killCell(cell);
      }
    }
    else if (pickItem.isParam()) {
      ParamIndex param = pickItem.asParamIndex();
      [[maybe_unused]] CellIndex cell = sh.cellByParam[param];
    }
    EndPopup();
  }
}


void SheetUi::renderBackground(WGPURenderPassEncoder wgpu_pass)
{
  for (auto it = traceTexCache.begin(); it != traceTexCache.end();) {
    if (it->second.inited && win.loopCount - it->second.mruLoopCount > 20) {
      it = traceTexCache.erase(it);
    }
    else {
      it++;
    }
  }

  auto dd = ImGui::GetDrawData();
  
  sheetView.vpL = 0;
  sheetView.vpR = dd->DisplaySize.x;
  sheetView.vpT = 0;
  sheetView.vpB = dd->DisplaySize.y;
  sheetView.webgpuRender(wgpu_pass);

  for (auto &it : insets) {
    if (!it->windowOpen) continue;
    it->dl.webgpuRender(wgpu_pass);
  }
}

PickItem SheetUi::findItem(vec3 const &targ)
{
  // OPT: use a quadtree
  PickItem best;
  F bestDistSq = 999.0;
  for (auto cell : sh.liveCells) {
    if (!visByPage.at(sh.pageByCell.at(cell))) continue;
    auto pos = cellDisplayPos(cell);
    if (targ[1] >= pos[1] - max(2.0f, totHeightByCell[cell]) && targ[1] <= pos[1] + 0.125f &&
        targ[0] >= pos[0] && targ[0] <= pos[0] + totWidthByCell[cell]) {
      F distSq = sqr(0.5f * (pos[0] - targ[0])) + sqr(pos[1] - 0.5f - targ[1]);
      if (distSq < bestDistSq) {
        bestDistSq = distSq;
        best = PickItem(PickCell, cell);
      }
    }
    for (auto param : sh.paramsByCell[cell]) {
      if (param >= knobPosByParam.size()) continue;
      auto pos = knobPosByParam[param];
      if (targ[1] >= pos[1] - 0.220f && targ[1] <= pos[1] + 0.300f && 
          targ[0] >= pos[0] - 0.375f && targ[0] <= pos[0] + 0.375f) {
        F distSq = sqr(0.5f * (pos[0] - targ[0])) + sqr(pos[1] - targ[1]);
        if (distSq < bestDistSq) {
          bestDistSq = distSq;
          best = PickItem(PickParam, param);
        }
      }
      auto squigglePos = knobPosByParam[param] + vec3(0, 0.26, 0);
      if (targ[1] >= squigglePos[1] - 0.010f && targ[1] <= squigglePos[1] + 0.210f && 
          targ[0] >= squigglePos[0] - 0.375f && targ[0] <= squigglePos[0] + 0.375f) {
        F distSq = sqr(0.5f * (squigglePos[0] - targ[0])) + sqr(squigglePos[1] - targ[1]);
        if (distSq < bestDistSq) {
          bestDistSq = distSq;
          best = PickItem(PickSquiggle, param);
        }
      }
    }
  }

  for (PageIndex page = 1; page < sh.nPages; page++) {
    auto pos = sh.includePosByPage[page];
    if (targ[1] >= pos[1] - 0.5f && targ[1] <= pos[1] + 0.125f &&
        targ[0] >= pos[0] && targ[0] <= pos[0] + 5.0f) {
      F distSq = sqr(0.5f * (pos[0] - targ[0])) + sqr(pos[1] - 0.125f - targ[1]);
      if (distSq < bestDistSq) {
        bestDistSq = distSq;
        best = PickItem(PickIncludePage, page);
      }
    }
  }
  return best;
}

char const * SheetUi::getKnobNameByParam(ParamIndex pi)
{
  size_t knobi = 0;
  for (auto &it : knobAttachment) {
    if (it.isParam(pi) && knobi < adjustKnobNames.size()) {
      return adjustKnobNames[knobi];
    }
    knobi++;
  }
  return nullptr;
}

char const * SheetUi::getKnobNameByCell(CellIndex ci)
{
  size_t knobi = 0;
  for (auto &it : knobAttachment) {
    if (it.isCell(ci) && knobi < adjustKnobNames.size()) {
      return adjustKnobNames[knobi];
    }
    knobi++;
  }
  return nullptr;
}

void SheetUi::toggleParamAttachment(ParamIndex pi)
{
  for (auto &it : knobAttachment) {
    if (it.isParam(pi)) {
      it.clear();
      return;
    }
  }
  for (auto &it : knobAttachment) {
    if (it.t == PickNone) {
      it = PickItem(PickParam, pi);
      return;
    }
  }
}

void SheetUi::clearParamAttachment()
{
  for (auto &it : knobAttachment) {
    it.clear();
  }
}


void SheetUi::commitCellName()
{
  if (editItem.isCell() && editCellName != sh.nameByCell[editItem.asCellIndex()]) {
    if (editCellName.empty()) {
      sh.killCell(editItem.asCellIndex());
      setEditItem(PickItem());
    }
    else {
      auto baseEditCellName = editCellName;
      int cellNameCounter = 1;
      do {
        auto existingCell = sh.getCell(editCellName);
        if (existingCell != nullCellIndex || Sheet::isReservedCellName(editCellName)) {
          editCellName = baseEditCellName + '.' + repr(cellNameCounter++);
          continue;
        }
      } while (0);

      if (editCellName != baseEditCellName) {
        W() << "Cell " << baseEditCellName << " exists, using " << editCellName << " instead";
        editCellCursorPos += editCellName.size() - baseEditCellName.size();
      }
      auto newCell = sh.mkCell(editCellName);
      if (newCell == nullCellIndex) { // shouldn't happen after checks above
        Wred() << "Invalid cell name " << shellEscape(editCellName);
      }
      else {
        CellIndex oldCell = editItem.asCellIndex();
        auto oldCellName = sh.nameByCell[oldCell];
        sh.setFormula(newCell, sh.formulaByCell[oldCell]);
        sh.setCellPos(newCell, sh.posByCell[oldCell]);
        sh.setCellDrawInfo(newCell, sh.drawInfoByCell[oldCell]);
        sh.setCellPage(newCell, sh.pageByCell[oldCell]);
        sh.killCell(oldCell);
        if (selectedCells[oldCell]) {
          selectedCells[oldCell] = false;
          selectedCells[newCell] = true;
        }
        editItem = PickItem(PickCell, newCell);
        sh.setNeedsParseAll();
        if (oldCellName != "new") {
          W() << "Renamed " << oldCellName << " (#" << oldCell << 
            ") to " << editCellName << " (#" << newCell << ")";
        }
      }
    }
  }
}

void SheetUi::ctlSheetView(G8DrawList &dl)
{
  ctlEditMode(dl);
  ctlEditCommands(dl);
  ctlEditCell(dl);
  ctlSearch(dl);
  ctlNavigate(dl);
  ctlSelect(dl);
  ctlScrub(dl);
}


void SheetUi::updateSearch()
{
  searchHits.clear();
  if (searchPrefix.size() > 0) {

    string retext;
    for (auto &c : searchPrefix) {
      if (c == '(' || c == ')' ||
          c == '[' || c == ']' ||
          c == '{' || c == '}' ||
          c == '\\' ||
          c == '.' || c == '*' || c == '+' || c == '?') {
        retext += '\\';
        retext += c;
      }
      else {
        retext += c;
      }
    }

    regex re(searchPrefix, regex::icase);

    for (auto cell : sh.liveCells) {
      auto &name = sh.nameByCell[cell];
      if (regex_search(name, re)) {
        searchHits[cell] = true;
      }
    }
    for (auto cell : sh.liveCells) {
      auto &formula = sh.formulaByCell[cell];
      if (regex_search(formula, re)) {
        searchHits[cell] = true;
      }
    }
    if (searchIndex >= 0 && !searchHits.empty()) {
      auto oneHit = searchHits.nthIndex(searchIndex);
      selectedCells.clear();
      selectedCells[oneHit] = true;
      visByPage[sh.pageByCell[oneHit]] = true;

      if (1) {
        cursorZipTarget = cellDisplayPos(oneHit);
        cursorZipStrength = 1.0f;
      }
      else {
        cursorPos = cellDisplayPos(oneHit);
      }
    }
  }
}

void SheetUi::updateCursorZip()
{
  if (cursorZipStrength > 0.0f) {
    vec3 move = cursorZipTarget - cursorPos;
    F norm = move.norm();
    if (norm > 0.1f) {
      cursorPos += move / min(10.0f, max(1.0f, (norm/3.0f)));
    }
    else {
      cursorZipStrength = 0.0f;
    }
  }
}



void SheetUi::ctlSheetContent(G8DrawList &dl)
{
  auto &io = GetIO();
  for (size_t knobi = 0; knobi < knobAttachment.size(); knobi++) {
    if (doAdjustParams[knobi] != 0) {
      auto const &a = knobAttachment[knobi];
      switch (a.t) {

        case PickNone:
        case PickCustom:
        case PickLive:
        case PickSquiggle:
        case PickIncludePage:
        break;

        case PickParam:
        {
          auto v = sh.params.valueByParam(a.asParamIndex());
          auto oldv = v;
          v += 0.02f * doAdjustParams[knobi] * sh.rangeByParam[a.asParamIndex()];
          if (oldv * v < 0.0f) {
            v = 0.0f;
          }
          if (0 && abs(v) < 0.001f * sh.rangeByParam[a.asParamIndex()]) {
            // get back to zero rather than being off by a few LSBs
            v = 0.0f;
          }
          cmdSetParam(a.asParamIndex(), v);
        }
        break;

        case PickCell:
          if (activeTrace && activeTrace->replayBuffer) {
            for (auto const &api : sh.compiled->apis) {
              api->doInteractive(*activeTrace->replayBuffer, doAdjustParams, visCursIndex, io.KeyShift);
            }
          }
          break;
      }
    }
  }
}

void animateScale(DrawInfo &drawInfo)
{
  F goodScale = tidyScale(drawInfo.autoRangeMax);
  
  F smoothUp = 1.42f; // slighly larger than sqrt(2), so we can handle a 4x jump in 4 steps
  F smoothDn = 1.0f / smoothUp;

  if (!drawInfo.scaleSet) {
    drawInfo.scale = goodScale;
    drawInfo.scaleSet = true;
  }

  if (goodScale != drawInfo.scale) {
    // Never draw with ridiculously off scale, get to within a factor of 4
    if (drawInfo.scale < goodScale * 0.25f) {
      drawInfo.scale = goodScale * 0.25f;
    }
    else if (drawInfo.scale > goodScale * 4.0f) {
      drawInfo.scale = goodScale * 4.0f;
    }
    // Then transition smoothly
    else if (drawInfo.scale * smoothUp < goodScale) {
      drawInfo.scale = drawInfo.scale * smoothUp;
    }
    else if (drawInfo.scale * smoothDn > goodScale) {
      drawInfo.scale = drawInfo.scale * smoothDn;
    }
    else {
      drawInfo.scale = goodScale;
    }
    uiPollLive = true;
  }
}


SheetLayoutGlobal SheetUi::getLayoutGlobal()
{
  SheetLayoutGlobal g;
  visCursIndex = max(0, min(visEndIndex-1, visCursIndex));

  int maxPts = max(512, int(4096 / zoomScale)); // heuristic

  if (visMagnify > 1 && (visEndIndex - visBeginIndex) > 16) {

    int len = visEndIndex - visBeginIndex;
    g.lhsIndex = visCursIndex - max(1, len / (2 * visMagnify));
    g.rhsIndex = visCursIndex + max(1, len / (2 * visMagnify));

    if (g.lhsIndex < 0) {
      g.rhsIndex -= g.lhsIndex;
      g.lhsIndex = 0;
    }
    else if (g.rhsIndex > visEndIndex) {
      g.lhsIndex += (visEndIndex - g.rhsIndex);
      g.rhsIndex = visEndIndex;
    }

    int goalStep = (g.rhsIndex - g.lhsIndex) / maxPts;
    int step = 1;
    while (step * 2 < goalStep) step *= 2;

    g.lhsIndex = step * (g.lhsIndex / step);
    g.rhsIndex = step * (g.rhsIndex / step);

    if (g.rhsIndex > g.lhsIndex) {
      g.cursIndex = step * (visCursIndex / step);
      g.cursXrel = double(g.cursIndex - g.lhsIndex) / double(g.rhsIndex - g.lhsIndex);

      auto startIndex = max(visBeginIndex, min(g.cursIndex, g.lhsIndex - step * ((g.rhsIndex - g.lhsIndex) / (32 * step))));
      auto endIndex = min(visEndIndex, max(g.cursIndex+1, g.rhsIndex + step * ((g.rhsIndex - g.lhsIndex) / (32 * step))));

      for (int li = startIndex; li < endIndex; li += step) {
        g.samples.emplace_back(li, double(li - g.lhsIndex) / double(g.rhsIndex - g.lhsIndex));
      }
      g.samples.emplace_back(endIndex - 1, double(endIndex - g.lhsIndex) / double(g.rhsIndex - g.lhsIndex));
    }
    else {
      g.cursIndex = visCursIndex;
    }

  }
  else {

    int goalStep = (visEndIndex - visBeginIndex) / maxPts;
    int step = 1;
    while (step * 2 < goalStep) step *= 2;

    F indexCoeff = 1.0 / max(1.0, double(visEndIndex - visBeginIndex));
    g.cursIndex = max(0, min(visEndIndex-1, step * (visCursIndex / step)));
    g.cursXrel = indexCoeff * (g.cursIndex - visBeginIndex);
    g.samples.reserve((visEndIndex - visBeginIndex) / step);
    g.lhsIndex = visBeginIndex;
    g.rhsIndex = visEndIndex;
    for (int li = visBeginIndex; li < visEndIndex; li += step) {
      g.samples.emplace_back(li, indexCoeff * (li - visBeginIndex));
    }
    g.samples.emplace_back(visEndIndex - 1, 1.0f);
  }
  return g;
}

SheetLayoutCell SheetUi::getLayoutCell(SheetLayoutGlobal &g, CellIndex cell, vec3 pos, bool forInset, bool forDebug, bool forExport)
{
  SheetLayoutCell lo(g);

  lo.pos = pos;
  lo.vMax = 1.0f;
  lo.vMin = -1.0f;
  lo.vScale = 1.0f;
  lo.graphAxisX = vec3(5, 0, 0);
  lo.cursX = g.cursXrel * lo.graphAxisX;
  lo.graphAxisY = vec3(0, 0.325, 0);
  lo.formulaWidth = 1.0f;
  lo.formulaHeight = 0.25f;
  lo.totHeight = 1.0f;
  lo.totWidth = 1.0f;
  lo.forInset = forInset;
  lo.forDebug = forDebug;
  lo.forExport = forExport;
  lo.drawTimeLabels = !forExport && pickItem.isCell(cell);
  lo.thickenPixels = 1.0f / max(1.0f, zoomScale);
  if (forInset || forExport) {
    lo.thicken = 1.0f;
  }
  else {
    lo.thicken = min(1.0f, max(0.5f, zoomScale));
  }


  return lo;
}


void SheetUi::drawSheetPane(G8DrawList &dl)
{
  auto t0 = clock();
  if (0) drawSheetGrid(dl);
  auto t1 = clock();
  setPickItem(dl);

  drawSheetLinks(dl);
  auto t2 = clock();

  auto glo = getLayoutGlobal();

  CellVals *cursVals = nullptr;
  if (activeTrace) {
    auto &tr = *activeTrace;
    if (tr.compiled == sh.compiled) {
      cursVals = tr.getVal(visCursIndex);
    }
  }

  for (auto cell : visibleCells) {
    if (!visByPage.at(sh.pageByCell.at(cell)) && !selectedCells[cell]) continue;
    auto pos = cellDisplayPos(cell);
    SheetLayoutCell lo = getLayoutCell(glo, cell, pos, false, false, false);

    drawCell(dl, cell, cursVals, lo);

    if (activeTrace) {
      drawCellData(dl, cell, cursVals, lo, *activeTrace);
    }
  }

  for (PageIndex page = 0; page < sh.nPages; page++) {
    vec3 pos(sh.includePosByPage[page].x(), sh.includePosByPage[page].y(), 0.0f);
    drawIncludePage(dl, page, pos);
  }

  for (auto cell : visibleCells) {
    if (activeTrace && pickItem.isCell(cell)) {
      auto pos = cellDisplayPos(cell);
      SheetLayoutCell lo = getLayoutCell(glo, cell, pos, false, true, false);
      lo.origin = lo.pos - lo.graphAxisY;
      drawCellDebug(dl, cell, cursVals, lo, *activeTrace);
    }
  }


  auto t3 = clock();

  if (dragRectActive) {
    vec4 linecol = fgcolor(0x27, 0x70, 0xf9, 0xcc);
    vec3 corner1(clickPickPos.x(), mousePickPos.y(), mousePickPos.z());
    vec3 corner2(mousePickPos.x(), clickPickPos.y(), mousePickPos.z());
    dl.addLine(clickPickPos, linecol, corner1, linecol);
    dl.addLine(corner1, linecol, mousePickPos, linecol);
    dl.addLine(mousePickPos, linecol, corner2, linecol);
    dl.addLine(corner2, linecol, clickPickPos, linecol);
  }
  auto t4 = clock();

  static bool statsOpen = false;
  if (statsOpen) {
    if (Begin("Stats", &statsOpen)) {
      Text("%s", ("grid: " + repr_clock(t1-t0)).c_str());
      Text("%s", ("links: " + repr_clock(t2-t1)).c_str());
      Text("%s", ("cells: " + repr_clock(t3-t2)).c_str());
      Text("%s", ("cursor: " + repr_clock(t4-t4)).c_str());
    }
    End();
  }
}

void SheetUi::drawIncludePage(G8DrawList &dl, PageIndex page, vec3 pos)
{
  vec4 bgcol = bgcolor(0x33, 0x55, 0x77, 0xff);

  auto label = sh.fileNameByPage[page];

  auto font = ysFonts.lrgFont;

  ImVec2 textSize = font->CalcTextSizeA(
    1.0f, FLT_MAX, -1.0f,
    label.data(), label.data() + label.size());

  F rectw = textSize[0];

  dl.addQuad(
    pos + vec3(fontSize * -0.5f, +0.1f * fontSize, 0), bgcol, 
    pos + vec3(fontSize * (rectw + 0.5f), +0.1f * fontSize, 0), bgcol,
    pos + vec3(fontSize * -0.5f, -1.5f * fontSize, 0), bgcol,
    pos + vec3(fontSize * (rectw + 0.5f), -1.5f * fontSize, 0), bgcol,
    1.5f, &unshinySolidShader);
  dl.addText(pos + vec3(0, 0, 0),
    vec3(0.20, 0, 0), vec3(0, -0.20, 0), vec3(0, 0, 1), 
    label,
    fgcolor(0xff, 0xcc, 0x99, 0xff),
    font, 
    0.0f, 0.0f,
    0.5f);

}


void SheetUi::drawCellData(G8DrawList &dl, CellIndex cell, CellVals *ref, SheetLayoutCell &lo, Trace &tr)
{
  auto &drawInfo = sh.drawInfoByCell[cell];

  /*
    Here we address the complications of having an out-of-date trace.
    Usually for just a frame or two after changing the sheet, sh.compiled
    will point to a new compilation, while activeTrace->compiled will be the old one.
    Usually they're extremely similar, so we avoid blinking when the user is typing
    by trying to reuse it. Here we look for the cell having the same name.
  */
  vector<BoxedRef> brs;
  if (auto com = tr.compiled.get()) {
    if (com == sh.compiled.get() || (cell < com->nameByCell.size() && com->nameByCell[cell] == sh.nameByCell[cell])) {
      for (auto &ctx : com->contexts) {
        auto found = ctx->refByCell.find(cell);
        if (found != ctx->refByCell.end()) {
          brs.push_back(found->second);
        }
      }
    }
  }
  vector<Trace *> traces{&tr};
  if (reruns && reruns->selectedRerun && reruns->showAllSeeds) {
    for (auto &[obsoleteSince, tr2] : reruns->selectedRerun->traceBySeed_) {
      if (tr2 && tr2.get() != &tr && tr2->compiled == tr.compiled) {
        traces.push_back(tr2.get());
      }
    }
  }

  auto [error, errorLoc] = sh.getCellError(cell);
  if (error.empty()) {

    autoRange(cell, traces, brs, drawInfo);
    animateScale(drawInfo);

    auto cellType = sh.getCellType(cell);
    if (cellType.template ist<DrawOp>()) {
      lo.origin = lo.pos + vec3(2.5f, -2.375f - lo.formulaHeight, 0);
      drawCellCustom(dl, cell, tr, lo, brs, drawInfo);

      if (reruns && reruns->selectedRerun && reruns->showAllSeeds) {
        auto lo2 = lo;
        lo2.altSeed = true;
        lo2.thicken *= 0.5f;
        lo2.thickenPixels *= 0.5f;
        for (auto &[obsoleteSince, tr2] : reruns->selectedRerun->traceBySeed_) {
          if (tr2 && tr2 != activeTrace && tr2->compiled == tr.compiled) {
            drawCellCustom(dl, cell, *tr2, lo2, brs, drawInfo);
          }
        }
      }
    }
    else if (cellType.template ist<VideoFrame>()) {
      lo.origin = lo.pos + vec3(2.5f, -2.375f - lo.formulaHeight, 0);
      drawCellVideo(dl, cell, tr, lo, brs, drawInfo);
    }
    else if (0 && sh.getCellIsPureParam(cell)) {
      lo.origin = lo.pos + vec3(0.0f, -0.375f - lo.formulaHeight, 0);
      drawCellPureParam(dl, cell, ref, lo, brs, drawInfo);
    }
    else if (0 && sh.getCellIsTimeInvariant(cell)) {
      lo.origin = lo.pos + vec3(0.0f, -0.375f - lo.formulaHeight, 0);
      if (0) drawCellTimeInvariant(dl, cell, ref, lo, brs, drawInfo);
    }
    else if (cellType.isUndef() || cellType.isError()) {
    }
    else if (cellType.template ist<ApiHandle>()) {
    }
    else {
      lo.origin = lo.pos - lo.graphAxisY + vec3(0, -0.025f - lo.formulaHeight, 0);
#if 1  
      drawCellTraceTex(dl, cell, traces, lo, brs, drawInfo);
#else
      if (!brs.empty()) {
        drawCellAxes(dl, cell, tr, lo, brs[0].t, drawInfo);
      }
      drawCellTrace(dl, cell, tr, lo, brs, drawInfo);
      if (reruns && reruns->selectedRerun && reruns->showAllSeeds) {
        auto lo2 = lo;
        lo2.altSeed = true;
        lo2.thicken *= 0.5f;
        lo2.thickenPixels *= 0.5f;
        for (auto &[obsoleteSince, tr2] : reruns->selectedRerun->traceBySeed_) {
          if (tr2 && tr2 != activeTrace && tr2->compiled == tr.compiled) {
            drawCellTrace(dl, cell, *tr2, lo2, brs, drawInfo);
          }
        }
      }
#endif
      drawCellLabel(dl, cell, tr, lo, brs, drawInfo);
      drawCellCursor(dl, cell, tr, lo, drawInfo);
    }
  }
  formulaHeightByCell[cell] = lo.formulaHeight;
  formulaWidthByCell[cell] = lo.formulaWidth;
  totWidthByCell[cell] = lo.totWidth;
  totHeightByCell[cell] = lo.totHeight;
  if (navMode == Nav_Search && searchHits[cell]) {
    vec4 lineCol = fgcolor(0xff, 0x70, 0xf9, 0xcc);
    vec4 fillCol = fgcolor(0xff, 0x70, 0xf9, 0x20);

    float thick = 0.025f;
    if (searchIndex >= 0 && searchHits.nthIndex(searchIndex) == cell) {
      thick = 0.1f;
    }

    vec3 topLeft(lo.pos[0] - thick, lo.pos[1] + thick, lo.pos[2]-0.05f);
    vec3 topRight = topLeft + vec3(lo.totWidth + 2 * thick, 0, 0);
    vec3 botLeft = topLeft + vec3(0, -lo.totHeight - 2 * thick, 0);
    vec3 botRight = topLeft + vec3(lo.totWidth + 2 * thick, -lo.totHeight - 2 * thick, 0);


    dl.addQuad(topLeft, fillCol, topRight, fillCol, botLeft, fillCol, botRight, fillCol);
    dl.addThickCurve({
      topLeft, topRight, botRight, botLeft, topLeft, topRight
    }, vec3(0, 0, 1), lineCol, thick, 0.0f, &glowingTextShader);
  }
}

void SheetUi::drawCellDebug(G8DrawList &dl, CellIndex cell, CellVals *ref, SheetLayoutCell &lo, Trace &tr)
{
  if (sh.compiled && tr.savePads) {

    vector<Trace *> traces{&tr};
    if (reruns && reruns->selectedRerun && reruns->showAllSeeds) {
      for (auto &[obsoleteSince, tr2] : reruns->selectedRerun->traceBySeed_) {
        if (tr2 && tr2.get() != &tr && tr2->compiled == tr.compiled) {
          traces.push_back(tr2.get());
        }
      }
    }

    auto &tps = sh.compiled->debugsByCell[cell];
    auto &charPosMap = charPosMapByCell[cell];

    vector<DebugTestPoint> showTps;
    vec3 hoverRelPos = mousePickPos - lo.pos;
    for (auto &tp : tps) {
      auto ci1 = tp.loc.begin + formulaStartByCell[cell];
      auto ci2 = tp.loc.end + formulaStartByCell[cell];
      if (ci1 < ci2 && ci2 < charPosMap.size()) {

        if (hoverRelPos.x() >= charPosMap[ci1].x() &&
            hoverRelPos.x() <= charPosMap[ci2].x() &&
            hoverRelPos.y() <= charPosMap[ci1].y() &&
            hoverRelPos.y() >= charPosMap[ci1].y() - fontSize) {
          showTps.push_back(tp);
        }
      }
    }

    if (!showTps.empty()) {
      vec3 tp1, tp2;
      bool foundTp = false;
      for (auto &showTp : showTps) {
        auto ci1 = showTp.loc.begin + formulaStartByCell[cell];
        auto ci2 = showTp.loc.end + formulaStartByCell[cell];
        if (ci1 < ci2 && ci2 < charPosMap.size()) {
          tp1 = charPosMap[ci1] + lo.pos + vec3(0, -fontSize, 0);
          tp2 = charPosMap[ci2] + lo.pos + vec3(0, -fontSize, 0);
          foundTp = true;
          break;

          //vec4 ulcol = fgcolor(0xff, 0xff, 0xff, 0xcc);
          //dl.addThickCurve({tp1, tp2}, vec3(0, 0, 1), ulcol, 0.05);
        }
      }

      auto lo2 = lo;
      vec3 ofs(0, +1.3, 0.25);
      lo2.pos += ofs;
      lo2.origin += ofs;
      lo2.graphAxisY *= 2.0f;
      lo2.drawTimeLabels = false;
      vec4 wipecol = bgcolor(0x00, 0x00, 0x00, 0xee);

      vec3 p0 = lo2.origin + 1.25f * lo2.graphAxisY - 0.1f * lo2.graphAxisX;
      vec3 p1 = lo2.origin + 1.25f * lo2.graphAxisY + 1.1f * lo2.graphAxisX;
      vec3 p2 = lo2.origin - 1.15f * lo2.graphAxisY - 0.1f * lo2.graphAxisX;
      vec3 p3 = lo2.origin - 1.15f * lo2.graphAxisY + 1.1f * lo2.graphAxisX;
      dl.addQuad(
        p0, wipecol, 
        p1, wipecol,
        p2, wipecol, 
        p3, wipecol,
        -1.0f, &maskShader);

      if (foundTp) {
        tp1[0] -= 0.01f;
        tp2[0] += 0.01f;
        vec4 bordcol = bgcolor(0x99, 0x99, 0x99, 0xff);
        vec3 tp1base = tp1;
        vec3 tp2base = tp2;
        tp1base[1] = p2[1];
        tp2base[1] = p3[1];
        dl.addThickCurve({p0, p1, p3, tp2base, tp2, tp1, tp1base, p2, p0}, vec3(0, 0, 1), bordcol, 0.03f,
          0.0f, &glowingTextShader);
      }


      DrawInfo drawInfo;
      autoRange(cell, traces, showTps, drawInfo);
      drawInfo.scale = tidyScale(drawInfo.autoRangeMax);

#if 1
      drawCellTraceTex(dl, cell, traces, lo2, showTps, drawInfo);
#else
      drawCellAxes(dl, cell, tr, lo2, showTps[0].t, drawInfo);
      drawCellTrace(dl, cell, tr, lo2, showTps, drawInfo);

      if (reruns && reruns->selectedRerun && reruns->showAllSeeds) {
        auto lo3 = lo2;
        lo3.altSeed = true;
        lo3.thicken *= 0.5f;
        lo3.thickenPixels *= 0.5f;
        for (auto &[obsoleteSince, tr2] : reruns->selectedRerun->traceBySeed_) {
          if (tr2 && tr2 != activeTrace && tr2->compiled == tr.compiled) {
            drawCellTrace(dl, cell, *tr2, lo3, showTps, drawInfo);
          }
        }
      }
#endif
      drawCellLabel(dl, cell, tr, lo2, showTps, drawInfo);
      drawCellCursor(dl, cell, tr, lo2, drawInfo);
    
      string debugLabel = sh.formulaByCell[cell].substr(showTps[0].loc.begin, showTps[0].loc.end - showTps[0].loc.begin);
      debugLabel += " ∊ ";
      debugLabel += typeName(showTps[0].t);
      dl.addText(
        lo2.origin + lo2.graphAxisY,
        vec3(0.15, 0, 0), 
        vec3(0, -0.15, 0), 
        vec3(0, 0, 1), 
        debugLabel, 
        fgcolor(0xff, 0xff, 0xff, 0xff),
        ysFonts.lrgFont, 
        0.0f, 1.0f);

    }
  }

}


static R roundgrid(R x) {
  return round(x/5.0)*5.0;
}

void SheetUi::drawSheetGrid(G8DrawList &dl)
{
  vec4 col = fgcolor(0x20, 0x20, 0x20, 0xff);

  R gridrad = 1.5 * R(dl.eyepos.z()); // OPT: more accuracy

  R xl = roundgrid(R(dl.lookat.x()) - gridrad) - 0.5;
  R xh = roundgrid(R(dl.lookat.x()) + gridrad) + 0.5;
  R yl = roundgrid(R(dl.lookat.y()) - gridrad) - 0.5;
  R yh = roundgrid(R(dl.lookat.y()) + gridrad) + 0.5;
  for (R yi = yl; yi <= yh; yi += 5.0) {
    dl.addLine(vec3(xl, yi, 0), col, vec3(xh, yi, 0), col);
  }

  for (R xi = xl; xi <= xh; xi += 5.0) {
    dl.addLine(vec3(xi, yl, 0), col, vec3(xi, yh, 0), col);
  }

  vec4 targcol = fgcolor(0x40, 0x40, 0x40, 0xff);

  dl.addLine(dl.lookat + vec3(-0.7, 0.0, 0.0), col, dl.lookat + vec3(+0.7, 0.0, 0.0), targcol);
  dl.addLine(dl.lookat + vec3(0.0, -0.7, 0.0), col, dl.lookat + vec3(0.0, +0.7, 0.0), targcol);
}

void SheetUi::setPickItem(G8DrawList &dl)
{
  if (!IsPopupOpen("pickItem")) {
    if (dl.pickRayActive) {
      vec3 hit;
      float distance = 0;
      if (dl.hitTestPlane(vec3(0, 0, 0), vec3(0, 0, 1), hit, distance)) {
        mousePickPos = hit;
        pickItem = findItem(hit);
      }
      else {
        pickItem.clear();
      }
    }
    else {
      pickItem.clear();
    }
  }
}

void SheetUi::setEditItem(PickItem it)
{
  editItem = it;
  if (it.isCell()) {
    editCellName = sh.nameByCell[it.asCellIndex()];
    navMode = Nav_EditCell;
  }
  else if (it.isNone()) {
    editCellName.clear();
    navMode = Nav_Sheet;
  }
  else if (it.isParam()) {
    editCellName.clear();
  }
}

static vector<vec3> calcKnobLayout(vector<vec3> const &paramPoss, SheetLayoutCell &lo)
{
  if (paramPoss.empty()) return vector<vec3>();

  vector<vec3> knobPoss(paramPoss.size());

  vector<size_t> lrSorted(paramPoss.size());
  iota(lrSorted.begin(), lrSorted.end(), 0);
  if (0) {
    // It'd look nicer to sort them, but the order may change while editing
    // and even while adjusting params, which would be messy.
    sort(lrSorted.begin(), lrSorted.end(), [&paramPoss](size_t a, size_t b) {
      return paramPoss[a].x() < paramPoss[b].x();
    });
  }

  for (bool relLayout : {true, false}) {
    vec3 knobCurs = lo.pos + vec3(+lo.knobRad, +lo.knobRad, 0);
    for (auto parami : lrSorted) {
      if (relLayout) knobCurs.x() = max(knobCurs.x(), paramPoss[parami].x());
      knobPoss[parami] = knobCurs;
      knobCurs += vec3(0.72f, 0, 0); // cf drawSquiggle width of 0.70
    }
    if (knobCurs.x() - lo.pos.x() < 5.0f) break;
  }

  return knobPoss;
}

void SheetUi::updatePolicyTerrain()
{
  policyTerrainMapByParam.resize(sh.nParams);
  policyTerrainExactByParam.resize(sh.nParams);
  fill(policyTerrainMapByParam.begin(), policyTerrainMapByParam.end(), nullptr);
  fill(policyTerrainExactByParam.begin(), policyTerrainExactByParam.end(), false);

  policyTerrainRangeByVal = nullptr;

  if (!rewardCells.empty()) {
    policyTerrainRangeByVal = CellVals::mk(sh.compiled.get());
    for (auto cell : visibleCells) {
      for (auto param : sh.paramsByCell[cell]) {

        PolicyTerrainSpec spec;
        for (auto &rc : rewardCells) {
          auto ref = sh.compiled->getValue(rc);
          if (ref.valid()) {
            spec.rMetrics.push_back(ref);
            spec.baseValues.push_back(activeTrace->getVal_F(visCursIndex, ref));
          }
        }
        spec.xVar = PolicySearchDim(param);
        spec.paramHash = sh.params.hash();
        spec.compiledSheetEpoch = sh.compiled->epoch;
        spec.sampleIndex = visCursIndex;
        spec.replayBuffer = activeTrace->replayBuffer;

        auto [ptm, exact] = getPolicyTerrain(sh, spec);
        if (ptm) {
          policyTerrainMapByParam[param] = ptm;
          policyTerrainExactByParam[param] = exact;
          int ri = 0;
          for (auto &rc : rewardCells) {
            auto ref = sh.compiled->getValue(rc);
            if (ref.valid() && ref.ist<F>()) {
              decltype(auto) range = policyTerrainRangeByVal->up_F(ref);
              range = max(range, get<2>(ptm->rs[ri]));
            }
            ri++;
          }
        }
      }
    }
  }
}

tuple<PolicyTerrainMap *, bool> SheetUi::getPolicyTerrainMapByParam(ParamIndex pi)
{
  if (pi == nullParamIndex || pi >= policyTerrainMapByParam.size() || pi >= policyTerrainExactByParam.size()) {
    return make_tuple(nullptr, false);
  }
  return make_tuple(policyTerrainMapByParam[pi].get(), policyTerrainExactByParam[pi]);
}


void SheetUi::drawCell(G8DrawList &dl, CellIndex cell, CellVals *ref, SheetLayoutCell &lo)
{
  bool selected = !lo.forInset && !lo.forExport && selectedCells[cell];

  CellEditableContents cec(*this, sh, cell);
  cec.applyAnnotations(sh.annosByCell[cell]);
  formulaStartByCell[cell] = cec.formulaStart;
  nameStartByCell[cell] = cec.nameStart;
  nameEndByCell[cell] = cec.nameEnd;

  bool errneeded = false;
  auto [errline, errloc] = sh.getCellError(cell);
  if (!errline.empty()) {
     errneeded = true;
  }

  auto font = ysFonts.lrgFont;
  F lineHeight = 1.35f * fontSize;

  vector<vec3> fakeCharPosMap;
  auto &charPosMap = (lo.forInset || lo.forExport) ? fakeCharPosMap : charPosMapByCell[cell];
  charPosMap.clear();
  charPosMap.resize(cec.text.size() + 1);
  vector<tuple<vec3, ImFontGlyph const *, vec4>> todoGlyphs;
  vec3 errpos = lo.pos + vec3(0, -fontSize, 0); // in case we don't find the character pos
  if (1) {
    F fscale = 1.0f / font->FontSize;

    vec3 linepos = lo.pos + vec3(0, -0.02f, 0);
    vec3 textpos = linepos;
    linepos += vec3(0.3f, 0, 0); // indent subsequent lines
    for (auto [c, fofs] : vbParseUtf8(cec.text)) {

      charPosMap[fofs] = textpos - lo.pos;
      if (c == '\n') {
        linepos += vec3(0, -lineHeight, 0);
        textpos = linepos;
      }

      if (errneeded && fofs == errloc.begin + cec.formulaStart) {
        errpos = textpos + vec3(-0.0f * fontSize, -1.3f * fontSize, 0);
        errneeded = false;
      }

      if (fofs == cec.nameStart) {
        if (sh.compiled) {
          switch (sh.compiled->apiDirectionByCell[cell]) {
            case API_NONE:
              break;

            case API_ACTUATOR:
              if (auto glyph = font->FindGlyph('<')) { // FIXME: icon
                todoGlyphs.emplace_back(textpos - vec3(0.7f * fontSize, 0, 0), glyph, fgcolor(0xff, 0x88, 0x88, 0xff));
              }
              break;

            case API_SENSOR:
            case API_MONITOR:
              if (auto glyph = font->FindGlyph('>')) {
                todoGlyphs.emplace_back(textpos - vec3(0.7f * fontSize, 0, 0), glyph, fgcolor(0xff, 0x88, 0x88, 0xff));
              }
              break;

            default:
              break;
          }
        }
      }
      
      if (c == '\n' || c == 0) continue;

      if (auto glyph = font->FindGlyph((ImWchar)c)) {
        if (c == '(' || c == ')') textpos[0] += 0.07f * fontSize;
        todoGlyphs.emplace_back(textpos, glyph, cec.decorations[fofs].color);
        textpos += glyph->AdvanceX * vec3(fontSize * fscale, 0, 0);
        if (c == '(' || c == ')') textpos[0] += 0.07f * fontSize;
      }
    }
    charPosMap[cec.text.size()] = textpos - lo.pos;

    // Set cursor by click
    if (editItem.isCell(cell)) {
      U32 bestFofs = -1;
      if (!GetIO().WantCaptureMouse && IsMouseClicked(0, false)) {
        F bestClickDistanceX = 5.0f;
        F bestClickDistanceY = 1.0f * fontSize;
        for (size_t fofs = 0; fofs < charPosMap.size(); fofs++) {
          vec3 finalpos = charPosMap[fofs] + lo.pos + vec3(0, -0.5f * fontSize, 0);
          auto dx = abs(finalpos.x() - mousePickPos.x());
          auto dy = abs(finalpos.y() - mousePickPos.y());
          if (dx < bestClickDistanceX && dy < bestClickDistanceY) {
            bestClickDistanceX = dx;
            bestClickDistanceY = dy + 0.25f * fontSize;
            bestFofs = fofs;
          }
        }
      }
      if (bestFofs != U32(-1)) {
        editCellCursorPos = bestFofs;
      }
    }

  }

  for (auto &[pos, glyph, col] : todoGlyphs) {
    lo.growFormula(pos[0] - lo.pos[0] + fontSize, lo.pos[1] - pos[1] + lineHeight);
  }

  if (auto cellKnob = getKnobNameByCell(cell)) {
    dl.addText(
      lo.pos + charPosMap.back() + vec3(0.1f, 0, 0),
      vec3(0.20f, 0, 0), vec3(0, -0.20f, 0),
      vec3(0, 0, 1),
      string(cellKnob),
      fgcolor(0xff, 0xff, 0xff, 0xff),
      ysFonts.hugeFont,
      0.0f, 0.0f,
      0.0f, &glowingTextShader);
  }
  
  if (lo.forInset || lo.forExport) {
  }
  else if (editItem.isCell(cell)) {
    F borderh = fontSize * 0.2f;
    F borderv = fontSize * 0.0f;
    const vec4 editBgCol = bgcolor(0x27, 0x70, 0xf9, 0x30);
    dl.addQuad(
      lo.pos + vec3(-borderh, borderv, 0), editBgCol,
      lo.pos + vec3(borderh + lo.formulaWidth, borderv, 0), editBgCol,
      lo.pos + vec3(-borderh, -borderv - lo.formulaHeight, 0), editBgCol,
      lo.pos + vec3(borderh + lo.formulaWidth, -borderv - lo.formulaHeight, 0), editBgCol,
      0.0f, &unshinySolidShader);
  }
  else if (selected) {
    F borderh = fontSize * 0.2f;
    F borderv = fontSize * 0.0f;
    const vec4 selectBgCol = bgcolor(0x27, 0x70, 0xf9, 0x30); 
    dl.addQuad(
      lo.pos + vec3(-borderh, borderv, 0), selectBgCol,
      lo.pos + vec3(borderh + lo.formulaWidth, borderv, 0), selectBgCol,
      lo.pos + vec3(-borderh, -borderv - lo.formulaHeight, 0), selectBgCol,
      lo.pos + vec3(borderh + lo.formulaWidth, -borderv - lo.formulaHeight, 0), selectBgCol,
      0.0f, &unshinySolidShader);
  }
  else if (pickItem.isCell(cell)) {
    F borderh = fontSize * 0.2f;
    F borderv = fontSize * 0.0f;
    const vec4 pickBgCol = bgcolor(0xff, 0xff, 0xff, 0x40); 
    dl.addQuad(
      lo.pos + vec3(-borderh, borderv, 0), pickBgCol,
      lo.pos + vec3(borderh + lo.formulaWidth, borderv, 0), pickBgCol,
      lo.pos + vec3(-borderh, -borderv - lo.formulaHeight, 0), pickBgCol,
      lo.pos + vec3(borderh + lo.formulaWidth, -borderv - lo.formulaHeight, 0), pickBgCol,
      0.0f, &unshinySolidShader);
  }

  // Draw cursor
  if (!lo.forInset && !lo.forExport&& editItem.isCell(cell)) {
    if (editCellCursorPos < charPosMap.size()) {
      auto pos = charPosMap[editCellCursorPos] + lo.pos;
      vec3 curswid(fontSize * 0.05f, 0, 0);
      vec3 cursheight(0, -fontSize, 0);
      vec4 curscol = fgcolor(0xff, 0xff, 0xff, 0xff);
      auto whiteuv = GetFontTexUvWhitePixel();
      auto whiteuv2 = vec2(whiteuv[0], whiteuv[1]);
      dl.addQuad(
        pos - curswid, whiteuv2, curscol,
        pos + curswid, whiteuv2, curscol,
        pos - curswid + cursheight, whiteuv2, curscol,
        pos + curswid + cursheight, whiteuv2, curscol,
        0.0f, &glowingTextShader);
    }
  }

  if (!lo.forInset && !lo.forExport) {
    auto const &params = sh.paramsByCell[cell];

    vector<vec3> paramPoss(params.size());

    for (size_t parami = 0; parami < params.size(); parami++) {
      auto param = params[parami];
      paramPoss[parami] = lo.pos + 0.5f * charPosMap[cec.formulaStart + sh.valueLocByParam[param].begin] + 0.5f * charPosMap[cec.formulaStart + sh.valueLocByParam[param].end];
    }

    auto knobPoss = calcKnobLayout(paramPoss, lo);

    for (size_t parami = 0; parami < params.size(); parami++) {
      auto param = params[parami];
      if (sh.valueLocByParam[param].empty()) continue;
      auto &knobPos = knobPoss[parami];
      knobPosByParam.resize(sh.nParams);
      knobPosByParam[param] = knobPos;
      if (0 && sh.distByParam[param] == Dist_complexReal && parami+1 < params.size() && sh.distByParam[params[parami+1]] == Dist_complexImag) {
        // it's a complex pair

        parami++; // skip the imag part
      }
      else {
        if (pickItem.isParam(param) || pickItem.isSquiggle(param)) {
          dl.addAnnulus(
              knobPos + vec3(0, 0, -0.01), vec3(lo.knobRad, 0, 0), vec3(0, lo.knobRad, 0),
              0.95f, 1.05f,
              0, M_2PI,
              bgcolor(0xff, 0xff, 0xff, 0x60),
              0.3f, &unshinySolidShader);
        }

        auto [ptm, exact] = getPolicyTerrainMapByParam(param);
        if (ptm) {
          drawSquiggle(dl, sh, knobPos + vec3(0, 0.26, 0), ptm, exact, policyTerrainRangeByVal.get());
          if (pickItem.isSquiggle(param)) {
            drawBlownupSquiggle(dl, sh, knobPos + vec3(0, 0.51, 0), ptm, exact, IsModOption() ? nullptr : policyTerrainRangeByVal.get());
          }
        }

        if (auto knob = getKnobNameByParam(param)) {
          dl.addText(
            knobPos + vec3(0, 0.33, 0),
            vec3(0.20, 0, 0), vec3(0, -0.20, 0),
            vec3(0, 0, 1),
            string(knob),
            fgcolor(0xff, 0xff, 0xff, 0xff),
            ysFonts.hugeFont,
            0.5f, 1.0f,
            0.0f, &glowingTextShader);
        }

        drawDialStd(dl, knobPos, vec3(lo.knobRad, 0, 0), vec3(0, lo.knobRad, 0), mat4::Identity(),
          sh.params.valueByParam(param), 
          sh.rangeByParam[param],
          sh.distByParam[param],
          ptm);

        dl.addCatenary(
          paramPoss[parami] + vec3(0, -0.08f, -0.01f), fgcolor(0x66, 0x66, 0xaa, 0xee),
          knobPos + vec3(0, 0, -0.01f), fgcolor(0x66, 0x77, 0xaa, 0xee),
          vec3(0, 0, 1),
          0.25f,
          1.5f,
          0.30f);

      }
    }
  }

  for (auto &[pos, glyph, textcol] : todoGlyphs) {
    F fscale = 1.0f / font->FontSize;
    vec3 textpos = pos;
    dl.addGlyph(textpos, // updates textpos
      vec3(fontSize*fscale, 0, 0),
      vec3(0, -fontSize*fscale, 0),
      vec3(0, 0, 1),
      textcol,
      glyph,
      0.0f, &glowingTextShader);
  }

  if (!errline.empty()) {
    // errpos was hopefully set to below the exact character pos,
    // otherwise just the upper left corner
    vec3 caratpos = errpos;
    errpos.x() = lo.pos.x();
    errpos.y() -= fontSize;

    ImVec2 textSize = font->CalcTextSizeA(
      1.0f, FLT_MAX, -1.0f,
      errline.data(), errline.data() + errline.size());

    F rectw = max(textSize[0], (caratpos.x() - errpos.x()) / fontSize);
    F recth = textSize[1];

    const vec4 errorBgCol = bgcolor(0x00, 0x00, 0x00, 0x99);
    dl.addQuad(
      errpos + vec3(fontSize * -0.5f, fontSize * 0.1f, 0), errorBgCol, 
      errpos + vec3(fontSize * (rectw + 0.5f), fontSize * 0.1f, 0), errorBgCol,
      errpos + vec3(fontSize * -0.5f, -fontSize * (recth + 0.5f), 0), errorBgCol,
      errpos + vec3(fontSize * (rectw + 0.5f), -fontSize * (recth + 0.5f), 0), errorBgCol,
      1.5f, &unshinySolidShader);

    const vec4 errorTextCol = fgcolor(0xff, 0x00, 0x00, 0xff);
    dl.addText(
      caratpos,
      vec3(fontSize, 0, 0),
      vec3(0, -fontSize, 0),
      vec3(0, 0, 1),
      "^",
      errorTextCol,
      font,
      0, 0,
      0.0f, &glowingTextShader);

    dl.addText(
      errpos,
      vec3(fontSize, 0, 0),
      vec3(0, -fontSize, 0),
      vec3(0, 0, 1),
      string(errline),
      errorTextCol,
      font,
      0, 0,
      0.0f, &glowingTextShader);
  }
}

static vec3 getTokenPos(vector<vec3> const &charPosMap, U32 startLoc, U32 endLoc)
{
  if (charPosMap.empty()) return vec3(0, 0, 0);
  auto startPos = charPosMap[min(size_t(startLoc), charPosMap.size()-1)];
  auto endPos = charPosMap[min(size_t(endLoc), charPosMap.size()-1)];
  return 0.5f * startPos + 0.5f * endPos;
}

void SheetUi::drawSheetLinks(G8DrawList &dl)
{
  for (auto dstCell : sh.liveCells) {
    if (!visByPage.at(sh.pageByCell.at(dstCell))) continue;
    for (auto &anno : sh.annosByCell[dstCell]) {
      if (anno.t == A_cellref) {
        auto srcCell = anno.getCellIndex();
        if (!visByPage.at(sh.pageByCell.at(dstCell))) continue;

        F alpha;
        if (editItem.isCell(dstCell) || rubberBandedCells[dstCell] ||
          pickItem.isCell(dstCell) || pickItem.isCell(srcCell)) {
          alpha = 0.75f;
        }
        else {
          alpha = min(0.75f, 0.0f + (10.0f * (MAX_ELEVATION - renderElevation)));
        }
        if (alpha > 0.05f) {

          if (!visByPage.at(sh.pageByCell.at(srcCell))) continue;
          bool picked = editItem.isCell(dstCell) || 
            pickItem.isCell(srcCell) || pickItem.isCell(dstCell) ||
            rubberBandedCells[srcCell] || rubberBandedCells[dstCell];

          auto &srcCharPos = charPosMapByCell[srcCell];
          auto &dstCharPos = charPosMapByCell[dstCell];
          if (srcCharPos.empty() || dstCharPos.empty()) continue;

          vec3 srcp = cellDisplayPos(srcCell) + vec3(0, 0, -0.1f) +
            getTokenPos(srcCharPos, nameStartByCell[srcCell], nameEndByCell[srcCell]);
          vec3 dstp = cellDisplayPos(dstCell) + vec3(0, 0, -0.1f) +
            getTokenPos(dstCharPos, formulaStartByCell[dstCell] + anno.loc.begin, formulaStartByCell[dstCell] + anno.loc.end);

#if 0
          if (srcp.y() - fontSize > dstp.y()) {
            srcp.y() -= fontSize;
          }
          else if (dstp.y() - fontSize > srcp.y()) {
            dstp.y() -= fontSize;
          }
#endif

          drawLink(dl, 
            srcp, picked ? vec4(0.4, 0.4, 0.7, alpha) : vec4(0.2, 0.2, 0.5, alpha),
            dstp, picked ? vec4(0.7, 0.4, 0.4, alpha) : vec4(0.5, 0.2, 0.2, alpha),
            picked ? 0.20f : 0.10f);
        }
      }
      else if (anno.t == A_paramref) {
        auto srcParam = anno.getParamIndex();
        if (srcParam == nullParamIndex) continue;
        auto srcCell = sh.cellByParam[srcParam];
        if (!visByPage.at(sh.pageByCell.at(srcCell))) continue;

        F alpha;
        if (editItem.isCell(dstCell) || rubberBandedCells[dstCell] ||
          pickItem.isCell(dstCell) || pickItem.isCell(srcCell)) {
          alpha = 0.75f;
        }
        else {
          alpha = min(0.75f, 0.0f + (10.0f * (MAX_ELEVATION - renderElevation)));
        }
        if (alpha > 0.05f) {

          bool picked = editItem.isCell(dstCell) || 
            pickItem.isCell(srcCell) || pickItem.isCell(dstCell) ||
            rubberBandedCells[srcCell] || rubberBandedCells[dstCell];

          auto &dstCharPos = charPosMapByCell[dstCell];
          if (dstCharPos.empty()) continue;

          vec3 dstp = cellDisplayPos(dstCell) + vec3(0, 0, -0.1f) +
            getTokenPos(dstCharPos, formulaStartByCell[dstCell] + anno.loc.begin, formulaStartByCell[dstCell] + anno.loc.end);

          vec3 srcp = knobPosByParam[srcParam];

          drawLink(dl,
            srcp, picked ? vec4(0.6, 0.6, 1.0, alpha) : vec4(0.4, 0.4, 0.7, alpha),
            dstp, picked ? vec4(1.0, 0.7, 0.7, alpha) : vec4(0.6, 0.4, 0.4, alpha),
            picked ? 0.20f : 0.10f);
        }
      }
    }
  }

  if (auto com = sh.compiled.get()) {
    int limProblems = 5;
    for (auto &[probType, vertices] : com->orderProblems) {
      if (--limProblems < 0) break;
      vec4 errcol = fgcolor(0xff, 0x00, 0x00, 0xff);
      for (size_t vi = 0; vi < vertices.size(); vi++) {
        auto src = vertices[vi];
        auto dst = vertices[(vi + 1) % vertices.size()];


        vec3 srcp = cellDisplayPos(src) + vec3(0, -0.08f, -0.1f);
        vec3 dstp = cellDisplayPos(dst) + vec3(0, -0.08f, -0.1f);

        dl.addCatenary(
          srcp, errcol,
          dstp, errcol,
          vec3(0, 0, 1),
          0.5f,
          1.5f,
          0.20f);

      }
    }
  }

}

void SheetUi::drawLink(G8DrawList &dl, vec3 const &srcp, vec4 const &srccol, vec3 const &dstp, vec4 const &dstcol, F rad)
{
  F dist = (srcp - dstp).norm();

  dl.addCatenary(
    srcp, srccol,
    dstp, dstcol,
    vec3(0, 0, 1),
    sqrt(dist + 0.5f) * 2.0f,
    1.5f,
    rad);
}


void SheetUi::drawCellCustom(G8DrawList &dl, CellIndex cell, Trace &tr, SheetLayoutCell &lo, vector<BoxedRef> const &brs, DrawInfo &drawInfo)
{
  if (!brs.empty() && brs[0].ist<DrawOpHandle>()) {
    int bri = 0;
    for (auto &br : brs) {
      DrawTraceAccum accum(dl, sh, tr, cell, pickItem.isCell(cell));
      if (reruns) {
        accum.numSeedsToAverage = reruns->showAllSeeds ? reruns->numSeeds : 1;
      }
      accum.addRange(br, bri > 0 ? 0.5f : 1.0f, lo);
      accum.finish();
      lo.growTot(accum.maxWidth, lo.formulaHeight + accum.maxHeight);
      bri++;
      if (!showMultipleValues) break;
    }
    lo.growTot(5.0f, lo.formulaHeight + 5.0f); // overestimate
  }
}

static bool decodeImage(TexCache &texCache, int texIndex, Blob &buffer, VideoFrame const &f)
{
  if (f.format == VFF_JPEG) return texCache.setJpeg(texIndex, buffer);
  //if (f.format == VFF_RGB) return texCache.setRgb(texIndex, f.width, f.height, buffer);
  return false;
}

void SheetUi::drawCellVideo(G8DrawList &dl, CellIndex cell, Trace &tr, SheetLayoutCell &lo, vector<BoxedRef> const &brs, DrawInfo &drawInfo)
{
  int verbose = 0;
  if (!brs.empty() && brs[0].ist<VideoFrame>()) {
    auto &br = brs[0];

    if (!recentVideoTexByCell[cell]) {
      recentVideoTexByCell[cell] = make_shared<CellVideoSitchy>(win);
    }
    auto videoSitchy = recentVideoTexByCell[cell].get();

    bool good = false;
    bool addLoadingFlag = false;
    R now = realtime();
    if (auto vals = tr.getVal(visCursIndex)) {
      decltype(auto) val = vals->rd_VideoFrame(br);
      if (val.blobRef.valid()) {
        int bestIndex = -1;
        if (verbose >= 1) L() << "Display " << val << " from " << br;
        if (videoSitchy->texCache.getIndexFor(val.blobRef, bestIndex)) {
          good = true;
          videoSitchy->bestIndex = bestIndex;
          videoSitchy->lastUsed = now;
          if (verbose >= 1) L() << "  Found in tex cache " << bestIndex;
        }
        if (!good) {
          if (Blob *b = getBlob(val.blobRef)) {
            if (decodeImage(videoSitchy->texCache, bestIndex, *b, val)) {
              videoSitchy->texCache.setIndexFor(val.blobRef, bestIndex);
              videoSitchy->bestIndex = bestIndex;
              videoSitchy->lastUsed = now;
              good = true;
              if (verbose >= 1) L() << "  Decoded to tex cache " << bestIndex;
            }
          }
        }
        if (!good) {
          SimFetcherClient::fetchBlob(val.blobRef, &videoSitchy->mostRecentFetch); // does nothing if a fetch has already been sent
        }
        if (1) {
          if (!good) {
            if (videoSitchy->texCache.getIndexFor(videoSitchy->mostRecentFetch, bestIndex)) {
              good = true;
              if (!liveClient) addLoadingFlag = true;
              videoSitchy->bestIndex = bestIndex;
              videoSitchy->lastUsed = now;
              if (verbose >= 1) L() << "  Found mostRecentFetch in tex cache " << bestIndex;
            }
          }
          if (!good) {
            if (Blob *b = getBlob(videoSitchy->mostRecentFetch)) {
              if (decodeImage(videoSitchy->texCache, bestIndex, *b, val)) {
                videoSitchy->texCache.setIndexFor(videoSitchy->mostRecentFetch, bestIndex);
                videoSitchy->bestIndex = bestIndex;
                videoSitchy->lastUsed = now;
                good = true;
                if (!liveClient) addLoadingFlag = true;
                if (verbose >= 1) L() << "  Decoded mostRecentFetch to tex cache " << bestIndex;
              }
            }
          }
        }
        if (good) {
          auto size = videoSitchy->texCache.sizes[bestIndex];

          mat4 posTransform = Transform<float, 3, Projective>(Translation3f(lo.origin)).matrix();

          F aspect = size.y() / size.x();
          mat4 corr; corr <<
            1, 0, 0, 0,
            0, aspect, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1;
          
          // Make it as big as we can within the bounding box
          vec4 nw = val.orientation * corr * vec4(1, 1, 0, 0);
          vec4 ne = val.orientation * corr * vec4(-1, 1, 0, 0);
          vec4 sw = val.orientation * corr * vec4(1, -1, 0, 0);
          vec4 se = val.orientation * corr * vec4(-1, -1, 0, 0);
          F maxx = max(max(abs(nw.x()), abs(ne.x())), max(abs(sw.x()), abs(se.x())));
          F maxy = max(max(abs(nw.y()), abs(ne.y())), max(abs(sw.y()), abs(se.y())));

          F scale = min(2.5f/maxx, 2.375f/maxy);

          if (0) L() << "scale maxx=" << maxx << " maxy=" << maxy << " scale=" << scale;

          mat4 rescale; rescale <<
            scale, 0, 0, 0,
            0, scale * aspect, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1;

          dl.addVideo(posTransform * val.orientation * rescale, videoSitchy->texCache.textures[bestIndex]);
          if (addLoadingFlag) {
            dl.addLoading(lo.origin, 0.2f);
          }

          //vec4 scrubCenter = posTransform * rescale * vec4(0, 1, 0, 0);
          // WRITEME: draw scrubber

        }
      }
    }
  }
  lo.growTot(5.0f, lo.formulaHeight + 5.0f);
}

template<typename RefType>
void addHash(vector<U64> &hashsrc, RefType ref);

template<> void addHash(vector<U64> &hashsrc, BoxedRef br)
{
  hashsrc.push_back(0);
  hashsrc.push_back(br.t.kind);
  hashsrc.push_back(br.t.nr);
  hashsrc.push_back(br.t.nc);
  hashsrc.push_back(U64(br.ofs));
}

template<> void addHash(vector<U64> &hashsrc, DebugTestPoint tp)
{
  hashsrc.push_back(1);
  hashsrc.push_back(tp.t.kind);
  hashsrc.push_back(tp.t.nr);
  hashsrc.push_back(tp.t.nc);
  hashsrc.push_back(U64(tp.ofs1));
  hashsrc.push_back(U64(tp.ofs8));
  hashsrc.push_back(U64(tp.ofs32));
}

template<typename RefType>
void SheetUi::autoRange(CellIndex cell, vector<Trace *> const &traces, vector<RefType> const &brs, DrawInfo &drawInfo)
{
  vector<U64> hashsrc;
  for (auto trace : traces) {
    hashsrc.push_back(trace->uniqueId);
  }
  for (auto &br : brs) {
    addHash(hashsrc, br);
  }
  auto hashcode = hashcodeU64(hashsrc);
  if (drawInfo.autoRangeSignature == hashcode) {
    return;
  }
  if (0) L() << "Redo autorange for " << cell << "\n";
  drawInfo.autoRangeSignature = hashcode;

  F m = 0.0f;
  for (auto trace : traces) {
    auto &tr = *trace;
    int batchi = tr.cpuBatchIndex;
    int endIndex = tr.nVals();
    for (auto &brSuite : brs) {
      if (!brSuite.valid()) break;
      auto br = brSuite.getRefForCompiledBatchSize(trace->compiledBatchSize);
      if (br.template ist<F>()) {
        for (int li = 0; li < endIndex; li++) {
          if (auto vals = getVals(tr, li, br)) {
            auto val = vals->rd_F(br, batchi);
            m = max(m, abs(val));
          }
        }
      }
      else if (br.template ist<CF>()) {
        for (U32 row = 0; row < 2; row++) {
          for (int li = 0; li < endIndex; li++) {
            if (auto vals = getVals(tr, li, br)) {
              auto val = vals->rd_CF(br, batchi);
              m = max(m, max(abs(val.real()), abs(val.imag())));
            }
          }
        }
      }
      else if (br.template ist<bool>()) {
        m = 1.0f;
      }
      else if (br.template ist<vec2>()) {
        for (U32 row = 0; row < 2; row++) {
          for (int li = 0; li < endIndex; li++) {
            if (auto vals = getVals(tr, li, br)) {
              auto &val = vals->rd_vec2(br, batchi);
              m = max(m, abs(val[row]));
            }
          }
        }
      }
      else if (br.template ist<vec3>()) {
        for (U32 row = 0; row < 3; row++) {
          for (int li = 0; li < endIndex; li++) {
            if (auto vals = getVals(tr, li, br)) {
              auto &val = vals->rd_vec3(br, batchi);
              m = max(m, abs(val[row]));
            }
          }
        }
      }
      else if (br.template ist<vec4>()) {
        for (int li = 0; li < endIndex; li++) {
          if (auto vals = getVals(tr, li, br)) {
            auto &val = vals->rd_vec4(br, batchi);
            m = max(m, val.cwiseAbs().maxCoeff());
          }
        }
      }
      else if (br.template ist<mat2>()) {
        for (int li = 0; li < endIndex; li++) {
          if (auto vals = getVals(tr, li, br)) {
            auto &val = vals->rd_mat2(br, batchi);
            m = max(m, val.cwiseAbs().maxCoeff());
          }
        }
      }
      else if (br.template ist<mat3>()) {
        for (int li = 0; li < endIndex; li++) {
          if (auto vals = getVals(tr, li, br)) {
            auto &val = vals->rd_mat3(br, batchi);
            m = max(m, val.cwiseAbs().maxCoeff());
          }
        }
      }
      else if (br.template ist<mat4>()) {
        for (int li = 0; li < endIndex; li++) {
          if (auto vals = getVals(tr, li, br)) {
            auto &val = vals->rd_mat4(br, batchi);
            m = max(m, val.cwiseAbs().maxCoeff());
          }
        }
      }
      else if (br.template ist<mat>()) {
        for (int li = 0; li < endIndex; li++) {
          if (auto vals = getVals(tr, li, br)) {
            auto val = vals->rd_mat(br, batchi);
            m = max(m, val.cwiseAbs().maxCoeff());
          }
        }
      }
      else if (br.template ist<cmat>()) {
        for (int li = 0; li < endIndex; li++) {
          if (auto vals = getVals(tr, li, br)) {
            auto val = vals->rd_cmat(br, batchi);
            m = max(m, val.cwiseAbs().maxCoeff());
          }
        }
      }
      else if (br.template ist<Checkp>()) {
        for (int li = 0; li < endIndex; li++) {
          if (auto vals = getVals(tr, li, br)) {
            auto val = vals->rd_Checkp(br, batchi);
            if (auto range = findRange(val)) {
              auto [lo, realval, hi] = *range;
              m = max(m, max(abs(lo)*1.1f, max(abs(hi)*1.1f, abs(realval))));
            }
          }
        }
      }
      else if (br.template ist<Beh>()) {
        m = 1.0;
        vector<U32> all;
        vector<bool> done(sh.nSymbols.i, false);
        for (int li = 0; li < endIndex; li++) {
          if (auto vals = getVals(tr, li, br)) {
            decltype(auto) val = vals->rd_Beh(br, batchi);
            for (auto const *p = &val.runset; p; p = p->tail) {
              if (p->leaf.i != 0) {
                assert(p->leaf.i < sh.nSymbols); // ensured by autorange
                if (!done[p->leaf.i]) {
                  done[p->leaf.i] = true;
                  all.push_back(p->leaf.i);
                }
              }
            }
          }
        }
        drawInfo.autoRangeSymbols = all;
      }
      // ADD type
    }
  }
  if (m == 0.0f) m = 0.0008f;

  drawInfo.autoRangeMax = m;
}



static Pad *getVals(Trace &tr, int li, Val const &dummy)
{
  return tr.getPad(li);
}

static CellVals *getVals(Trace &tr, int li, BoxedRef const &dummy)
{
  return tr.getVal(li);
}

static void addHash(vector<U64> &dst, BoxedRef const &br)
{
  dst.push_back(101);
  dst.push_back(br.t.kind);
  dst.push_back(br.t.nr);
  dst.push_back(br.t.nc);
  dst.push_back(br.cell);
  dst.push_back(br.ofs);
}

static void addHash(vector<U64> &dst, Val const &br)
{
  dst.push_back(102);
  dst.push_back(br.t.kind);
  dst.push_back(br.t.nr);
  dst.push_back(br.t.nc);
  dst.push_back(br.ofs);
}

ostream & operator << (ostream &s, TraceVisInfo const &a)
{
  s << "TraceVisInfo(type=" << a.traceType <<
    ", color=" << a.colorIndex <<
    ", dataIndex=" << a.dataIndex <<
    ", dataCount=" << a.dataCount <<
    ", stride=" << a.dataStride <<
    ", alpha=" << a.alpha <<
    ", thick=" << a.thick <<
    ")";
  return s;
}

struct TraceVisBuilder {
  SheetUi &ui;
  vector<TraceVisInfo> visInfos;
  Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> data;
  size_t colorIndex = 0;
  size_t dataIndex = 0; // increases as we go
  size_t nSamples = 0; // stable after init


  TraceVisBuilder(SheetUi &_ui, size_t _nSamples)
    : ui(_ui), nSamples(_nSamples)
  {
  }
  
  pair<float *, size_t> addVisInfo(size_t nTraces, uint32_t type, size_t dataCount, float alpha, float thick)
  {
    size_t dataIndex0 = dataIndex;

    for (size_t i = 0; i < nTraces; i++) {
      TraceVisInfo vi;
      vi.traceType = type;
      vi.dataIndex = dataIndex;
      vi.colorIndex = colorIndex;
      vi.dataCount = dataCount;
      vi.dataStride = uint32_t(-1); // unknown, so fill in during finish()
      vi.alpha = alpha;
      vi.thick = thick;
      dataIndex += dataCount;
      colorIndex += 1;

      visInfos.push_back(vi);
    }

    if (size_t(data.rows()) < dataIndex) {
      Index newRows = max(Index(dataIndex), data.rows() * 2);
      if (0 && data.rows() != 0) {
        L() << "Resize " << data.rows() << "x" << data.cols() << " to " << newRows << "x" << nSamples;
      }
      data.conservativeResize(newRows, nSamples);
      data.bottomRows(data.rows() - dataIndex0).setZero();
      assert(data(dataIndex0, 0) == 0.0f);
    }

    return make_pair(&data(dataIndex0, 0), size_t(data.rows()));
  }

  void finish(WGPUDevice dev, WGPUQueue queue, TraceTexResources &bufs)
  {
    bufs.inited = true;

    if (visInfos.empty()) {
      bufs.empty = true;
      return;
    }
    bufs.empty = false;

    for (auto &vi : visInfos) {
      vi.dataStride = dataIndex;
    }
    data.conservativeResize(dataIndex, nSamples);
    assert(data.rows() > 0 && data.cols() > 0);
    if (nSamples > 1) assert(&data(0, 1) - &data(0, 0) == ssize_t(dataIndex));

    if (0 && data.rows() != 1) L() << "TraceVisBuilder visInfos=" << visInfos <<
      " data.size=" << data.rows() << "x" << data.cols();
    

    WGPUBufferDescriptor desc0 = {
      .usage = WGPUBufferUsage_CopyDst | WGPUBufferUsage_Storage,
      .size = visInfos.size() * sizeof(TraceVisInfo),
    };
    bufs.traceVisInfoBuf = wgpuDeviceCreateBuffer(dev, &desc0);
    wgpuQueueWriteBuffer(queue, bufs.traceVisInfoBuf, 0, visInfos.data(), desc0.size);
  
    WGPUBufferDescriptor desc1 = {
      .usage = WGPUBufferUsage_CopyDst | WGPUBufferUsage_Storage,
      .size = uint32_t(data.size() * sizeof(float)),
    };
    bufs.traceDataBuf = wgpuDeviceCreateBuffer(dev, &desc1);
    wgpuQueueWriteBuffer(queue, bufs.traceDataBuf, 0, data.data(), desc1.size);

  }

  template<typename RefType>
  void addTrace(CellIndex cell, Trace &tr, F alpha, F thick, RefType const &br, DrawInfo &drawInfo)
  {
    int batchi = tr.cpuBatchIndex;

    if (br.template ist<F>()) {
      auto [p, stride] = addVisInfo(1, TVIT_line, 1, alpha, thick);
      for (size_t li = 0; li < nSamples; li++, p += stride) {
        if (auto vals = getVals(tr, li, br)) {
          auto val = vals->rd_F(br, batchi);
          p[0] = val;
        }
      }
    }
    else if (br.template ist<CF>()) {
      auto [p, stride] = addVisInfo(2, TVIT_line, 1, alpha, thick);

      for (size_t li = 0; li < nSamples; li++, p += stride) {
        if (auto vals = getVals(tr, li, br)) {
          auto val = vals->rd_CF(br, batchi);
          p[0] = val.real();
          p[1] = val.imag();
        }
      }
    }

    else if (br.template ist<bool>()) {
      auto [p, stride] = addVisInfo(1, TVIT_line, 1, alpha, thick);
      for (size_t li = 0; li < nSamples; li++, p += stride) {
        if (auto vals = getVals(tr, li, br)) {
          auto val = vals->rd_bool(br, batchi);
          p[0] = val ? 1.0f : 0.0f;
        }
      }
    }
    else if (br.template ist<mat>()) {
      size_t nr = br.t.nr, nc = br.t.nc;
      auto [p, stride] = addVisInfo(nr * nc, TVIT_line, 1, alpha, thick);
      for (size_t li = 0; li < nSamples; li++, p += stride) {
        if (auto vals = getVals(tr, li, br)) {
          auto val = vals->rd_mat(br, batchi);
          for (size_t ci = 0; ci < nc; ci++) {
            for (size_t ri = 0; ri < nr; ri++) {
              p[ci * nr + ri] = val(ri, ci);
            }
          }
        }
      }
    }
    else if (br.template ist<cmat>()) {
      size_t nr = br.t.nr, nc = br.t.nc;
      auto [p, stride] = addVisInfo(nr * nc * 2, TVIT_line, 1, alpha, thick);
      for (size_t li = 0; li < nSamples; li++, p += stride) {
        if (auto vals = getVals(tr, li, br)) {
          auto val = vals->rd_cmat(br, batchi);
          for (size_t ci = 0; ci < nc; ci++) {
            for (size_t ri = 0; ri < nr; ri++) {
              p[(ci * nr + ri) * 2 + 0] = val(ri, ci).real();
              p[(ci * nr + ri) * 2 + 1] = val(ri, ci).imag();
            }
          }
        }
      }
    }
    else if (br.template ist<Beh>()) {
      auto &symbols = drawInfo.autoRangeSymbols;
      auto [p, stride] = addVisInfo(1, TVIT_beh, symbols.size() + 1, alpha, thick);

      for (size_t li = 0; li < nSamples; li++, p += stride) {
        memset(p, 0, symbols.size() * sizeof(float));
        if (auto vals = getVals(tr, li, br)) {
          auto val = vals->rd_Beh(br, batchi);
          for (auto const *q = &val.runset; q; q = q->tail) {
            auto found = find(symbols.begin(), symbols.end(), q->leaf.i);
            if (found != symbols.end()) {
              size_t si = found - symbols.begin();
              p[si] = 1.0f;
            }
          }
          p[symbols.size()] = 
              val.status == B_failure ? -1.0f :
              val.status == B_success ? 1.0f :
              0.0f; // running
        }
      }
    }
    else if (br.template ist<Checkp>()) {

      auto [p, stride] = addVisInfo(1, TVIT_check, 4, alpha, thick);

      for (size_t li = 0; li < nSamples; li++, p += stride) {
        if (auto vals = getVals(tr, li, br)) {
          auto val = vals->rd_Checkp(br, batchi);
          if (auto range = findRange(val)) {
            auto [lo, realval, hi] = *range;
            p[0] = realval;
            p[1] = lo;
            p[2] = hi;
          }
          else {
            // shouldn't happen?
            p[0] = 0.0f;
            p[1] = 0.0f;
            p[2] = 0.0f;
          }
          p[3] = 
            val->status == Check_notnow ? 0.0f :
            val->status == Check_pass ? 1.0f :
            val->status == Check_fail ? -1.0f :
            -2.0f;
        }
      }
    }
  }

  template<typename RefType>
  void addTraces(CellIndex cell, vector<Trace *> const &traces, float alpha, float thick, vector<RefType> const &brs, DrawInfo &drawInfo)
  {
    int tri = 0;
    for (auto trp : traces) {
      int bri = 0;
      for (auto &brSuite : brs) {
        if (!brSuite.valid()) continue;
        if (dataIndex >= 1000) break; // FIXME do I need this?

        auto br = brSuite.getRefForCompiledBatchSize(trp->compiledBatchSize);
        addTrace(cell, *trp, 
          tri > 0 ? alpha * 0.5f : alpha, 
          bri > 0 ? thick * 0.5f : thick,
          br, drawInfo);
        bri ++;
        if (!ui.showMultipleValues) break;
      }
    }
    tri ++;
  }

};


TraceTexResources::~TraceTexResources()
{
  if (traceVisInfoBuf) {
    wgpuBufferDestroy(traceVisInfoBuf);
    wgpuBufferRelease(traceVisInfoBuf);
    traceVisInfoBuf = nullptr;
  }
  if (traceDataBuf) {
    wgpuBufferDestroy(traceDataBuf);
    wgpuBufferRelease(traceDataBuf);
    traceDataBuf = nullptr;
  }
  inited = false;
  empty = false;
}


template<typename RefType>
TraceTexResources &SheetUi::mkTraceBuf(CellIndex cell, vector<Trace *> const &traces, vector<RefType> const &brs, DrawInfo &drawInfo)
{
  auto &tr = *traces[0];
  size_t nSamples = tr.vals.size();
  if (nSamples == 0) {
    static TraceTexResources bogus;
    return bogus;
  }

  vector<U64> identity;
  identity.reserve(8u + 8u * brs.size());
  for (auto trp : traces) {
    identity.push_back(trp->uniqueId);
  }
  identity.push_back(nSamples);
  for (auto &brSuite : brs) {
    auto br = brSuite.getRefForCompiledBatchSize(tr.compiledBatchSize);
    addHash(identity, br);
    if (br.template ist<Beh>()) {
      copy(drawInfo.autoRangeSymbols.begin(), drawInfo.autoRangeSymbols.end(), back_inserter(identity));
    }
    if (!showMultipleValues) break;
  }

  auto &bufSlot = traceTexCache[ObjectKey(&tr, hashcodeU64(identity))];
  if (!bufSlot.inited) {

    TraceVisBuilder b(*this, nSamples);
    b.addTraces(cell, traces, 1.0f, 1.0f, brs, drawInfo);

    b.finish(win.wgpu_device, win.wgpu_queue, bufSlot);
  }
  bufSlot.mruLoopCount = win.loopCount;
  return bufSlot;
}

template<typename RefType>
void SheetUi::drawCellTraceTex(G8DrawList &dl, CellIndex cell, vector<Trace *> const &traces, SheetLayoutCell &lo, vector<RefType> const &brs, DrawInfo &drawInfo)
{
  if (brs.empty()) return;

  auto type = brs[0].t;
  auto &tr = *traces[0];

  auto &traceBufs = mkTraceBuf(cell, traces, brs, drawInfo);
  if (!traceBufs.inited || traceBufs.empty) return;

  TraceUniforms traceUniforms = {
    .scalePos = +drawInfo.scale,
    .scaleNeg = -drawInfo.scale,
    .range = max(0.001f, drawInfo.scale),
    .nSamples = uint32_t(tr.vals.size()),
    .isCheckFailure = tr.checkFailuresByCell[cell],
    .isRewardCell = rewardCells[cell],
  };

  auto restoreTraceVisInfo = dl.withTraceVisInfo(traceBufs.traceVisInfoBuf, &traceShader);
  auto restoreTraceData = dl.withTraceData(traceBufs.traceDataBuf, &traceShader);
  auto restoreTraceUniforms = dl.withTraceUniforms(traceUniforms, &traceShader);
  auto overscanY = 1.05f;
  dl.addQuad(
    lo.origin - overscanY * lo.graphAxisY, vec2(lo.g.lhsIndex, -overscanY),
    lo.origin + lo.graphAxisX - overscanY * lo.graphAxisY, vec2(lo.g.rhsIndex, -overscanY),
    lo.origin + overscanY * lo.graphAxisY, vec2(lo.g.lhsIndex, +overscanY),
    lo.origin + lo.graphAxisX + overscanY * lo.graphAxisY, vec2(lo.g.rhsIndex, +overscanY),
    0.0f,
    &traceShader);


  if (type.template ist<F>() || type.template ist<CF>() || 
      type.template ist<vec2>() || type.template ist<vec3>() || type.template ist<vec4>() ||
      type.template ist<mat2>() || type.template ist<mat3>() || type.template ist<mat4>() ||
      type.template ist<mat>() || type.template ist<cmat>() ||
      type.template ist<Checkp>()) {

    auto tickColor = fgcolor(0xff, 0xff, 0xff, 0x88);
    for (auto labelVal : {-1.0f, 0.0f, +1.0f}) {
      vec3 textpos = lo.origin + labelVal * lo.graphAxisY + vec3(-0.05f, 0, 0);
      dl.addQuad(
        textpos + vec3(0, 0.01f, 0), tickColor,
        textpos + vec3(0.05f, 0.01f, 0), tickColor,
        textpos + vec3(0, -0.01f, 0), tickColor,
        textpos + vec3(0.05f, -0.01f, 0), tickColor,
        0.0f);
      dl.addText(textpos,
        vec3(0.15, 0, 0), vec3(0, -0.15, 0), vec3(0, 0, 1), 
        repr_0_2g(labelVal * drawInfo.scale), 
        tickColor,
        ysFonts.lrgFont, 
        1.0f, 0.5f,
        0.0f);
    }

    lo.growTot(5.0f, lo.formulaHeight + 0.75f);
    
    if (lo.drawTimeLabels) {
      for (auto [ptIndex, ptX, alignH, ok] : {
        tuple(lo.g.lhsIndex, 0.0f, 0.0f, lo.g.cursXrel > 0.03f),
        tuple(lo.g.cursIndex, lo.g.cursXrel, 0.0f, true),
        tuple(lo.g.rhsIndex, 1.0f, 1.0f, lo.g.cursXrel < 0.92f)}) {
        if (!ok) continue;
        vec3 textpos = lo.origin - lo.graphAxisY + ptX * lo.graphAxisX + vec3(0, +0.0f, 0);
        dl.addText(textpos,
          vec3(0.15f, 0, 0), vec3(0, -0.15f, 0), vec3(0, 0, 1.0f),
          repr_0_3g(tr.getTimeForIndex(ptIndex)), 
          tickColor,
          ysFonts.lrgFont, 
          alignH, 1.0f,
          0.0f);
      }
    }
  }

  if (brs[0].template ist<Beh>()) {
    auto &symbols = drawInfo.autoRangeSymbols;

    vec3 line0 = lo.origin - 1.0f * lo.graphAxisY;
    vec3 lineSep = lo.graphAxisY * 2.0f / max(size_t(4), symbols.size() + 1u);

    int linei = 0;
    if (!lo.altSeed) {
      for (auto symbol : symbols) {
        dl.addText(
          line0 + (linei + 0.5) * lineSep + vec3(-0.03, 0, 0),
          vec3(0.15, 0, 0), vec3(0, -0.15, 0), vec3(0, 0, 1), 
          sh.nameBySymbol[symbol],
          goodGraphColor(linei),
          ysFonts.lrgFont, 
          1.0f, 0.5f,
          0.0f, &glowingTextShader);

        linei++;
      }
    }

    dl.addText(
      line0 + (symbols.size() + 0.5) * lineSep + vec3(-0.03, +0.05, 0), 
      vec3(0.15, 0, 0), vec3(0, -0.15, 0), vec3(0, 0, 1), 
      "status",
      vec4(1, 1, 1, 1),
      ysFonts.lrgFont, 
      1.0f, 0.5f,
      0.0f, &glowingTextShader);
  }
}



template<typename RefType>
void SheetUi::drawCellLabel(G8DrawList &dl, CellIndex cell, Trace &tr, SheetLayoutCell &lo, vector<RefType> const &brs, DrawInfo &drawInfo)
{
  if (brs.size() > 0) {
    auto &brSuite = brs[0];
    if (!brSuite.valid()) return;
    auto br = brSuite.getRefForCompiledBatchSize(tr.compiledBatchSize);
    int batchi = tr.cpuBatchIndex;

    vec3 vCoeff = (1.0f / drawInfo.scale) * lo.graphAxisY;

    if (br.template ist<F>()) {
      if (auto cursvals = getVals(tr, lo.g.cursIndex, br)) {
        auto val = cursvals->rd_F(br, batchi);
        auto label = repr_0_3g(val);
        dl.addText(
          lo.origin + vec3(0.03, 0, 0) + lo.cursX + val * vCoeff,
          vec3(0.15, 0, 0), 
          vec3(0, -0.15, 0), 
          vec3(0, 0, 1), 
          label, 
          fgcolor(0xff, 0xff, 0xff, 0xff),
          ysFonts.lrgFont, 
          0.0f, 0.5f);
      }
    }
    else if (br.template ist<CF>()) {
      if (auto cursvals = getVals(tr, lo.g.cursIndex, br)) {
        auto val = cursvals->rd_CF(br, batchi);
        auto label = repr_0_3g(real(val)) + "+" + repr_0_3g(imag(val)) + "i";
        dl.addText(
          lo.origin + vec3(0.03, 0, 0) + lo.cursX + real(val) * vCoeff,
          vec3(0.15, 0, 0), 
          vec3(0, -0.15, 0), 
          vec3(0, 0, 1), 
          label, 
          fgcolor(0xff, 0xff, 0xff, 0xff),
          ysFonts.lrgFont, 
          0.0f, 0.5f);
      }
    }
    else if (br.template ist<string_view>()) {
      if (auto cursvals = getVals(tr, lo.g.cursIndex, br)) {
        auto label = cursvals->rd_string_view(br, batchi);
        dl.addText(
          lo.origin + vec3(0.03, 0, 0) + lo.cursX,
          vec3(0.15, 0, 0), 
          vec3(0, -0.15, 0), 
          vec3(0, 0, 1), 
          string(label), 
          fgcolor(0xff, 0xff, 0xff, 0xff),
          ysFonts.lrgFont, 
          0.0f, 0.5f);
      }
    }
    else if (br.template ist<mat>()) {
      if (auto cursvals = getVals(tr, lo.g.cursIndex, br)) {
        if (br.t.nc == 0 || br.t.nr == 0) {
        }
        else if (br.t.nc == 1) {
          decltype(auto) val = cursvals->rd_mat(br, batchi);
          char txt[64];
          Index showrows = min(Index(10), Index(br.t.nr));
          vec3 cursorPos = lo.origin + vec3(0.03, 0, 0) + lo.cursX;
          for (int rowi = 0; rowi < showrows; rowi ++) {
            snprintf(txt, sizeof(txt), "%+0.3g", double(val(rowi, 0)));
            dl.addTextAdvance(
              cursorPos,
              vec3(0.15f, 0, 0), 
              vec3(0, -0.15f, 0), 
              vec3(0, 0, 1.0f), 
              txt,
              lerp(goodGraphColor(rowi), vec4(1, 1, 1, 1), 0.3f),
              ysFonts.lrgFont);
            cursorPos += vec3(0.1f, 0, 0);
          }
        }
        else {
          decltype(auto) val = cursvals->rd_mat(br, batchi);
          auto showrows = min(Index(br.t.nr), Index(5));
          auto showcols = min(Index(br.t.nc), Index(5));
          auto rowIncr = vec3(0, -0.15f, 0); // lo.graphAxisY * (-1.5f / nr);
          auto rowStart = -(showrows * 0.5f - 0.25f) * rowIncr;
          size_t colorIndex = 0;
          for (Index rowi = 0; rowi < showrows; rowi ++) {
            vec3 cursorPos = lo.origin + vec3(0.03, 0, 0) + lo.cursX + rowStart + rowi * rowIncr;
            char txt[64];
            for (Index coli = 0; coli < showcols; coli ++) {
              snprintf(txt, sizeof(txt), "%+0.3g", double(val(rowi, coli)));
              dl.addTextAdvance(
                cursorPos,
                vec3(0.15, 0, 0), 
                vec3(0, -0.15, 0), 
                vec3(0, 0, 1), 
                txt,
                lerp(goodGraphColor(colorIndex), vec4(1, 1, 1, 1), 0.3f),
                ysFonts.lrgFont);
              cursorPos += vec3(0.1, 0, 0);
              colorIndex++;
            }
          }
        }
      }
    }
    else if (br.template ist<cmat>()) {
      if (auto cursvals = getVals(tr, lo.g.cursIndex, br)) {
        auto val = cursvals->rd_cmat(br, batchi);
        if (br.t.nc == 0 || br.t.nr == 0) {
        }
        else if (br.t.nc == 1) {
          string label;
          Index showrows = min(Index(10), Index(br.t.nr));
          for (int rowi = 0; rowi < showrows; rowi ++) {
            if (!label.empty()) label += ' ';
            label += repr_0_3g(real(val(rowi, 0))) + "+" + repr_0_3g(imag(val(rowi, 0))) + "i";
          }
          dl.addText(
            lo.origin + vec3(0.03, 0, 0) + lo.cursX,
            vec3(0.15, 0, 0), 
            vec3(0, -0.15, 0), 
            vec3(0, 0, 1), 
            label, 
            fgcolor(0xff, 0xff, 0xff, 0xff),
            ysFonts.lrgFont, 
            0.0f, 0.5f);
        }
        else {
          auto showrows = min(val.rows(), Index(5));
          auto showcols = min(val.cols(), Index(5));
          auto rowIncr = vec3(0, -0.15, 0); // lo.graphAxisY * (-1.5f / nr);
          auto rowStart = -(showrows * 0.5f - 0.25f) * rowIncr;
          char labelBuf[256];
          size_t colorIndex = 0;
          for (Index rowi = 0; rowi < showrows; rowi ++) {
            vec3 cursorPos = lo.origin + vec3(0.03, 0, 0) + lo.cursX + rowStart + rowi * rowIncr;
            for (Index coli = 0; coli < showcols; coli ++) {
              snprintf(labelBuf, sizeof(labelBuf), "%+0.3g+%0.3gi", double(real(val(rowi, coli))), double(imag(val(rowi, coli))));
              dl.addTextAdvance(
                cursorPos,
                vec3(0.15f, 0, 0), 
                vec3(0, -0.15f, 0), 
                vec3(0, 0, 1.0f), 
                labelBuf,
                lerp(goodGraphColor(colorIndex), vec4(1, 1, 1, 1), 0.3f),
                ysFonts.lrgFont);
              cursorPos += vec3(0.1, 0, 0);
              colorIndex++;
            }
          }
        }
      }
    }    
    else if (br.template ist<Checkp>()) {
#if 0
      if (auto cursvals = getVals(tr, lo.g.cursIndex, br)) {
        auto val = cursvals->rd_Checkp(br, batchi);
        auto label = repr_0_3g(val.val);
        dl.addText(
          lo.origin + vec3(0.03f, 0, 0) + lo.cursX + val.val * vCoeff,
          vec3(0.15f, 0, 0),
          vec3(0, -0.15f, 0),
          vec3(0, 0, 1),
          label,
          fgcolor(0xff, 0xff, 0xff, 0xff),
          ysFonts.lrgFont, 
          0.0f, 0.5f);
      }
#endif
    }
    // ADD type

  }
}


void SheetUi::drawCellCursor(G8DrawList &dl, CellIndex cell, Trace &tr, SheetLayoutCell &lo, DrawInfo &drawInfo)
{
  vec3 visOfs = lo.cursX;

  vec4 curscol = fgcolor(0xff, 0xcc, 0x99, 0xbb);
  {
    vector<vec3> pts {
      lo.origin + visOfs - lo.graphAxisY,
      lo.origin + visOfs + lo.graphAxisY
    };
    dl.addThickCurve(pts, vec3(0, 0, 1), curscol, 0.02f * zoomScale);
  }

}

void SheetUi::drawCellPureParam(G8DrawList &dl, CellIndex cell, CellVals *ref, SheetLayoutCell &lo, vector<BoxedRef> const &brs, DrawInfo &drawInfo)
{
}

void SheetUi::drawCellTimeInvariant(G8DrawList &dl, CellIndex cell, CellVals *ref, SheetLayoutCell &lo, vector<BoxedRef> const &brs, DrawInfo &drawInfo)
{
  if (ref) {
    vec3 dialc = lo.origin + vec3(0.5, 0, 0);
    vec3 dialx = vec3(0.375, 0, 0);
    vec3 dialy = vec3(0, 0.375, 0);
    mat4 colorx = mat4::Identity();

    auto brs = sh.refsByCell(cell);
    for (auto br : brs) {
      if (br.ist<F>()) {
        drawDialStd(dl, dialc, dialx, dialy, colorx, ref->rd_F(br), 1.0, Dist_linear, nullptr);
        lo.totHeight = max(lo.totHeight, lo.formulaHeight + 0.75f);
      }
    }
  }
}

void SheetUi::cmdSetParam(ParamIndex param, F value)
{
  sh.setParamValueUndoable(param, value);
  sh.propagateParamUpdate(param);
  if (liveClient && liveClient->running()) {
    liveClient->txUpdateCellParams(sh.cellByParam[param]);
  }
}

void SheetUi::cmdQuantizeParam(ParamIndex param, F steps)
{
  auto v = sh.params.valueByParam(param);
  auto range = sh.rangeByParam[param];
  auto incr = range / steps;
  auto newv = round(v / incr) * incr;
  cmdSetParam(param, newv);
}


void SheetUi::cmdIncreaseParamRange(ParamIndex param)
{
  sh.setParamRange(param, quantizeIntervalUp(sh.rangeByParam[param] * 1.9f));
}

void SheetUi::cmdDecreaseParamRange(ParamIndex param)
{
  sh.setParamRange(param, quantizeIntervalDn(sh.rangeByParam[param] / 1.9f));
}

void SheetUi::setProjView(G8DrawList &dl)
{
#if defined(__EMSCRIPTEN__)
  // FIXME
  int width = 800, height = 600;
  screenScale = max(5.0f, 0.030f * min(width, height));
#else
  if (win.glfw_window) {
    int width = -1, height = -1;
    glfwGetFramebufferSize(win.glfw_window, &width, &height);
    screenScale = max(5.0f, 0.030f * min(width, height));
  }
#endif

  F renderDistance = zoomScale * screenScale;

  dl.lookat = vec3(cursorPos.x(), cursorPos.y(), 0.0);
  vec3 deye = vec3(0, -renderDistance, 0);
  deye = AngleAxis<F>(renderYaw, vec3::UnitZ()) * AngleAxis<F>(-renderElevation, vec3::UnitX()) * deye;
  dl.eyepos = dl.lookat + deye;
  dl.up = vec3(0, 0.1f * sin(renderElevation), 1).normalized();
  dl.fov = F(22.0 * M_PI/180.0);

  dl.nearZ = 0.25f * renderDistance;
  dl.farZ = 8.0f * renderDistance;

  dl.camCheckSanity();
  dl.setProjView();
}


void SheetUi::ctlSearch(G8DrawList &dl)
{
  auto &io = GetIO();
  if (navMode == Nav_Search) {
    if (io.InputQueueCharacters.Size > 0) {
      if (IsModText()) {
        for (int n = 0; n < io.InputQueueCharacters.Size; n++) {
          U32 c = (U32)io.InputQueueCharacters[n];
          if (c == '\x08') {
            if (searchPrefix.empty()) {
              navMode = Nav_Sheet;
            }
            else {
              searchPrefix.pop_back();
            }
          }
          else if (c == '\n') {
            navMode = Nav_Sheet;
          }
          else {
            searchPrefix.push_back((U8)c);
          }
        }
        searchIndex = -1;
        updateSearch();
      }
      io.InputQueueCharacters.resize(0);
    }
  }
}


void SheetUi::ctlEditCell(G8DrawList &dl)
{
  auto &io = GetIO();

  if (navMode == Nav_EditCell && (!editItem.isCell() || !selectedCells[editItem.asCellIndex()]) && !selectedCells.empty()) {
    editItem = PickItem(PickCell, selectedCells.front());
  }
  if (navMode == Nav_EditCell && editItem.isCell()) {
    /*
      editCellCursorPos is the offset of the cursor within the string made up of
      name + sep + formula. So we may need to modify name or formula depending on
      the cursor position.
      Note we take a copy of name and formula, set changed flags, and handle write-back at the end.
    */

    CellEditableContents cec(*this, sh, editItem.asCellIndex());

    bool wasInName = cec.cursorInName();

    if ((IsModNone() && IsKeyPressed(ImGuiKey_LeftArrow, true))) {
      cec.left();
    }
    if ((IsModNone() && IsKeyPressed(ImGuiKey_RightArrow, true))) {
      cec.right();
    }
    if ((IsModNone() && IsKeyPressed(ImGuiKey_UpArrow, true))) {
      cec.up();
    }
    if ((IsModNone() && IsKeyPressed(ImGuiKey_DownArrow, true))) {
      cec.down();
    }
    if (IsModNone() && (IsKeyPressed(ImGuiKey_Tab, false) || IsKeyPressed(ImGuiKey_Escape, false))) {
      setEditItem(PickItem());
      selectedCells.clear();
    }

    if (IsModNone()) {
      if (IsKeyPressed(ImGuiKey_Backspace, true)) cec.input(0x08);
      if (IsKeyPressed(ImGuiKey_Enter, true)) cec.input(0x0a);
    }
    if (IsModCtrl()) {
      if (IsKeyPressed(ImGuiKey_A, true)) cec.input(0x01);
      if (IsKeyPressed(ImGuiKey_B, true)) cec.input(0x02);
      if (IsKeyPressed(ImGuiKey_D, true)) cec.input(0x04);
      if (IsKeyPressed(ImGuiKey_E, true)) cec.input(0x05);
      if (IsKeyPressed(ImGuiKey_F, true)) cec.input(0x06);
      if (IsKeyPressed(ImGuiKey_K, true)) cec.input(0x0b);
      if (IsKeyPressed(ImGuiKey_N, true)) cec.input(0x0e);
      if (IsKeyPressed(ImGuiKey_P, true)) cec.input(0x10);
      if (IsKeyPressed(ImGuiKey_R, true)) cec.input(0x12);
      if (IsKeyPressed(ImGuiKey_Y, true)) cec.input(0x19);
    }

    if (io.InputQueueCharacters.Size > 0) {
      if (!IsModCmd()) {
        for (int n = 0; n < io.InputQueueCharacters.Size; n++) {
          U32 c = (U32)io.InputQueueCharacters[n];
          cec.input(c);
        }
      }
      // Consume characters
      io.InputQueueCharacters.resize(0);
    }
    if (cec.formulaChanged) {
      sh.setFormulaUndoable(editItem.asCellIndex(), cec.formula);
    }
    if (cec.nameChanged) {
      editCellName = cec.name;
    }
    if (!wasInName) {
      commitCellName();
    }
  }
}

void SheetUi::ctlEditMode(G8DrawList &dl)
{

  switch (navMode) {

    case Nav_Sheet:
      if (doSearch) {
        doSearch = false;
        navMode = Nav_Search;
        searchIndex = -1;
        searchPrefix.clear();
        updateSearch();
      }
      if (doEscape && !selectedCells.empty()) {
        doEscape = false;
        selectedCells.clear();
      }
      if (IsModCmd() && IsKeyPressed(ImGuiKey_F, false)) {
        navMode = Nav_Fly;
        dl.homeLookat = dl.lookat;
        dl.homeEyepos = dl.eyepos;
        dl.homeFov = dl.fov;
        dl.homeUp = dl.up;
      }
      break;

    case Nav_Fly:
      if (doEscape) {
        doEscape = false;
        navMode = Nav_Sheet;
        editItem.clear();
      }
      if (IsModCmd() && IsKeyPressed(ImGuiKey_F, false)) {
        navMode = Nav_Sheet;
      }
      break;

    case Nav_EditCell:
      if (doEscape) {
        doEscape = false;
        commitCellName();
        navMode = Nav_Sheet;
        editItem.clear();
      }
      if (IsModNone() && IsKeyPressed(ImGuiKey_Tab, false)) {
        commitCellName();
        navMode = Nav_Sheet;
        editItem.clear();
      }
      break;

    case Nav_Search:
      if (doSearch) {
        searchIndex++;
        updateSearch();
      }
      if (doEscape) {
        doEscape = false;
        navMode = Nav_Sheet;
      }
      break;
  }
}

void SheetUi::ctlEditCommands(G8DrawList &dl)
{

  if (doClearRewards) {
    doClearRewards = false;
    rewardCells.clear();
  }

  if (doCopy) {
    doCopy = false;
    if (navMode == Nav_Sheet) {
      pasteBuffer.clear();
      for (auto cell : selectedCells) {
        pasteBuffer.emplace_back(sh.nameByCell[cell], sh.formulaByCell[cell], (sh.posByCell[cell] - mousePickPos.head<2>()).array().round());
      }
      selectedCells.clear();
    }
    else if (navMode == Nav_EditCell) {
      // WRITEME SetClipboardText(...)
    }
  }

  if (doCut) {
    doCut = false;
    if (navMode == Nav_Sheet) {
      pasteBuffer.clear();
      for (auto cell : selectedCells) {
        pasteBuffer.emplace_back(sh.nameByCell[cell], sh.formulaByCell[cell], (sh.posByCell[cell] - mousePickPos.head<2>()).array().round());
      }
      for (auto cell : selectedCells) {
        sh.killCell(cell);
      }
      selectedCells.clear();
      sh.setNeedsParseAll();
    }
    else if (navMode == Nav_EditCell) {
      // WRITEME SetClipboardText(...)
    }
  }

  if (doPaste) {
    doPaste = false;
    if (navMode == Nav_Sheet) {
      for (auto [name, formula, relpos] : pasteBuffer) {
        string newname = name;
        int trynum = 0;
        while (1) {
          auto existing = sh.getCell(newname);
          if (existing == nullCellIndex || trynum > 1000) break;
          newname = name + to_string(++trynum);
        }
        auto cell = sh.mkCell(newname);
        if (cell != nullCellIndex) {
          sh.setFormula(cell, formula);
          sh.setCellPos(cell, cursorPos.head<2>().array().round() + relpos.array());
          sh.setCellPage(cell, getNewCellPage());
        }
      }
      sh.setNeedsParseAll();
    }
    else if (navMode == Nav_EditCell) {
      // WRITEME GetClipboardText()
    }
  }

}

void SheetUi::ctlNavigate(G8DrawList &dl)
{
  auto &io = GetIO();  

  if (navMode == Nav_Fly) {
    dl.hoverControls();
  }
  else {
    updateCursorZip();

    if (io.MouseWheelH != 0.0f || io.MouseWheel != 0.0f) {
      uiPollLive = true;
      if (IsModCmd()) {
        renderYaw = normangle(renderYaw - 0.1f * io.MouseWheelH);
        renderElevation = clamp(renderElevation - 0.1f * io.MouseWheel, -MAX_ELEVATION, +MAX_ELEVATION);
        if (renderElevation == MAX_ELEVATION && io.MouseWheel < 0) {
          if (renderYaw > 0) {
            renderYaw = max(renderYaw + 0.1f * io.MouseWheel, 0.0f);
          }
          else {
            renderYaw = min(renderYaw - 0.1f * io.MouseWheel, 0.0f);
          }
        }
      }
      else if (IsModShift()) {
        // shift hwheel with a param picked means to adjust that param
        if (pickItem.isParam() || pickItem.isSquiggle()) {
          auto param = pickItem.asParamIndex();
          auto v = sh.params.valueByParam(param);

          switch (sh.distByParam[param]) {

            case Dist_linear:
              v += -0.02f * io.MouseWheelH * sh.rangeByParam[param];
              break;

            case Dist_nonnegative:
              v = max(0.0f, v - 0.02f * io.MouseWheelH * sh.rangeByParam[param]);
              break;

            case Dist_exponential:
              v *= 1.0f - 0.05f * io.MouseWheelH;
              break;

            case Dist_complexReal:
            case Dist_complexImag:
              // WRITEME: should do cartesian thing with mouse wheel
              v += -0.02f * io.MouseWheelH * sh.rangeByParam[param];
              break;
          }

          if (abs(v) < 0.001f * sh.rangeByParam[param]) {
            // get back to zero rather than being off by a few LSBs
            v = 0.0f;
          }
          sh.setParamValueUndoable(param, v);
          sh.propagateParamUpdate(param);
          if (liveClient && liveClient->running()) {
            liveClient->txUpdateCellParams(sh.cellByParam[param]);
          }
        }
        else {
          // shift hwheel with no pick item means to drag the time cursor
          visCursIndex += int(round((visEndIndex - visBeginIndex) * -0.1f * io.MouseWheelH));
        }
      }
      else if (IsModOption()) {
        // option hwheel drags the time cursor 10x slower
        visCursIndex += int(round((visEndIndex - visBeginIndex) * -0.01f * io.MouseWheelH));
      }
      else if (!IsAnyPopupOpen()) {
        if (0) L() << "scroll " << io.MouseWheelH << " " << io.MouseWheel;
        //zoomScale = max(0.1f, zoomScale - 10.0f * clamp(io.MousePinch, -0.02f, 0.02f) * zoomScale);
        cursorPos.x() += zoomScale * clamp(-io.MouseWheelH, -2.0f, 2.0f);
        cursorPos.y() += zoomScale * clamp(io.MouseWheel, -2.0f, 2.0f);
        //io.MousePinch = 0.0f;
      }
    }
    if (abs(cursorPos.x()) > 1e8f || abs(cursorPos.y()) > 1e8f) {
      L() << "Crazy cursorPos " << cursorPos << ". Setting to zero";
      cursorPos = vec3(0, 0, 0);
    }

    if (navMode == Nav_Sheet) {
      if (IsModNone() && IsKeyDown(ImGuiKey_E)) {
        uiPollLive = true;
        zoomScale /= 1.05f;
      }
      if (IsModNone() && IsKeyDown(ImGuiKey_Q)) {
        uiPollLive = true;
        zoomScale *= 1.05f;
        zoomScale = min(zoomScale, 5.0f * (sheetBoundsHi - sheetBoundsLo).norm() / screenScale);
      }
      const F moveAccel = 0.25f;
      if (IsModNone() && IsKeyDown(ImGuiKey_W)) {
        uiPollLive = true;
        cursorVel.y() = clamp(cursorVel.y() + moveAccel, 0.0f, 1.0f);
      }
      else if (IsModNone() && IsKeyDown(ImGuiKey_S)) {
        uiPollLive = true;
        cursorVel.y() = clamp(cursorVel.y() - moveAccel, -1.0f, 0.0f);
      }
      else {
        cursorVel.y() = 0.0f;
      }
      if (IsModNone() && IsKeyDown(ImGuiKey_D)) {
        uiPollLive = true;
        cursorVel.x() = clamp(cursorVel.x() + moveAccel, 0.0f, 1.0f);
      }
      else if (IsModNone() && IsKeyDown(ImGuiKey_A)) {
        uiPollLive = true;
        cursorVel.x() = clamp(cursorVel.x() - moveAccel, -1.0f, 0.0f);
      }
      else {
        cursorVel.x() = 0.0f;
      }
      cursorPos += 0.75f * zoomScale * cursorVel;
      cursorPos = cursorPos
        .cwiseMax(sheetBoundsLo - zoomScale * screenScale * vec3(0.4f, 0.2f, 0))
        .cwiseMin(sheetBoundsHi + zoomScale * screenScale * vec3(0.4f, 0.2f, 0));
      if (0) L() << LOGV(cursorPos) << LOGV(cursorVel) << LOGV(zoomScale) << LOGV(screenScale);
    }

    if (navMode == Nav_Sheet && pickItem.isParam()) {
      if (IsModNone() && IsKeyPressed(ImGuiKey_0)) {
        cmdSetParam(pickItem.asParamIndex(), 0.0f);
      }
      if (IsModNone() && IsKeyPressed(ImGuiKey_9)) {
        cmdQuantizeParam(pickItem.asParamIndex(), 10.0f);
      }
      if (IsModShift() && IsKeyPressed(ImGuiKey_Period)) {
        cmdIncreaseParamRange(pickItem.asParamIndex());
      }
      if (IsModShift() && IsKeyPressed(ImGuiKey_Comma)) {
        cmdDecreaseParamRange(pickItem.asParamIndex());
      }
    }

    if (navMode == Nav_Sheet && pickItem.isCell()) {
      if (IsModCtrl() && IsKeyPressed(ImGuiKey_1)) {
        insets.at(1)->toggleCell(pickItem.asCellIndex());
      }
      if (IsModCtrl() && IsKeyPressed(ImGuiKey_2)) {
        insets.at(0)->toggleCell(pickItem.asCellIndex());
      }
    }


    setProjView(dl);

    if (doUndo) {
      sh.undo();
    }
    if (doRedo) {
      sh.redo();
    }
  }
}


void SheetUi::ctlSelect(G8DrawList &dl)
{
  if (!GetIO().WantCaptureMouse && IsMouseClicked(0, false)) {
    if (IsModCtrl()) {
      OpenPopup("pickItem");
    }
    else {
      clickPickPos = mousePickPos;
      dragLastPickPos = mousePickPos;

      if (pickItem.isCell()) {
        // clicked on a cell.
        if (selectedCells[pickItem.asCellIndex()]) {
          if (selectedCells.size() == 1) {
            setEditItem(pickItem);
          }
          else {
            setEditItem(PickItem());
          }
        }
        else {
          if (!IsModShift()) {
            selectedCells.clear();
            editItem.clear();
          }
          selectedCells[pickItem.asCellIndex()] = true;
          navMode = Nav_Sheet;
        }
      }
      else if (pickItem.isParam()) {
        // shift-click on param to only select one
        if (IsModShift()) {
          clearParamAttachment();
        }
        toggleParamAttachment(pickItem.asParamIndex());
      }
      else if (pickItem.isIncludePage()) {
        selectedIncludePages.clear();
        selectedIncludePages.push_back(pickItem.asPageIndex());
      }
      else {
        if (!IsModShift()) {
          selectedCells.clear();
          selectedIncludePages.clear();
          setEditItem(PickItem());
        }
      }
    }
  }
  else if (!GetIO().WantCaptureMouse && IsMouseDragging(0)) {
    vec3 diff = mousePickPos - dragLastPickPos;
    dragLastPickPos = mousePickPos;

    if (dragCellsActive || (!dragRectActive && !selectedCells.empty())) {
      dragCellsActive = true;
      sh.moveCellsUndoable(selectedCells, diff.head<2>());
    }
    else if (dragIncludePageActive || (!dragRectActive && !selectedIncludePages.empty())) {
      dragIncludePageActive = true;
      sh.moveIncludePagesUndoable(selectedIncludePages, diff.head<2>(), IsModOption());
    }
    else {
      dragRectActive = true;
      rubberBandedCells.clear();
      for (auto i : sh.liveCells) {
        if (!visByPage.at(sh.pageByCell.at(i))) continue;
        auto p = cellDisplayPos(i);
        if (p.x() >= min(mousePickPos.x(), clickPickPos.x()) &&
            p.x() <= max(mousePickPos.x(), clickPickPos.x()) &&
            p.y() >= min(mousePickPos.y(), clickPickPos.y()) &&
            p.y() <= max(mousePickPos.y(), clickPickPos.y())) {
          rubberBandedCells[i] = true;
        }
      }
    }
  }
  else {
    if (dragRectActive) {
      dragRectActive = false;
      for (auto i : rubberBandedCells) {
        selectedCells[i] = true;
      }
      rubberBandedCells.clear();
    }
    else if (dragCellsActive) {
      dragCellsActive = false;
      sh.snapToGrid();
    }
    else if (dragIncludePageActive) {
      dragIncludePageActive = false;
      sh.snapToGrid();
    }
  }
}

void SheetUi::ctlScrub(G8DrawList &dl)
{
  if (auto j = Joybox::primary) {
    switch (j->type) {

      case Joybox_TangentWave2:
        if (activeTrace) {
          visCursIndex += j->knobs.at(12);
          if (j->buttons.at(JBTW_rew) >= 0.0f) { // left arrow
            visCursIndex -= 1;
            uiPollLive = true;
          }
          if (j->buttons.at(JBTW_fwd) >= 0.0f) { // right arrow
            visCursIndex += 1;
            uiPollLive = true;
          }
          if (j->buttons.at(JBTW_prev) == 0.0f) { // rew
            visCursIndex = 0;
            uiPollLive = true;
          }
          if (j->buttons.at(JBTW_next) == 0.0f) { // ff
            visCursIndex = visEndIndex - 1;
            uiPollLive = true;
          }
        }
        break;

      default:
        break;
    }
  }

  if (navMode == Nav_Sheet && activeTrace) {
    R oldScrollDir = scrollDir;
    if (IsModNone() && IsKeyDown(ImGuiKey_LeftArrow)) {
      scrollDir = -1.0;
      defaultScrollDir = 0.0;
    }
    else if (IsModOption() && IsKeyDown(ImGuiKey_LeftArrow)) {
      scrollDir = -0.1;
      defaultScrollDir = 0.0;
    }
    else if (IsModNone() && IsKeyDown(ImGuiKey_RightArrow)) {
      scrollDir = +1.0;
      defaultScrollDir = 0.0;
    }
    else if (IsModOption() && IsKeyDown(ImGuiKey_RightArrow)) {
      scrollDir = +0.1;
      defaultScrollDir = 0.0;
    }
    else {
      scrollDir = defaultScrollDir;
    }
    if (scrollDir != oldScrollDir) {
      scrollStartTime = realtime() - 0.016;
      scrollStartTick = visCursIndex;
    }
    if (scrollDir != 0.0) {
      visCursIndex = scrollStartTick + int(floor(scrollDir * (realtime() - scrollStartTime) / R(activeTrace->dt) / visMagnify));
      if (visCursIndex < visBeginIndex || visCursIndex >= visEndIndex) defaultScrollDir = 0.0;
      uiPollLive = true;
    }
    if (IsModShift() && IsKeyDown(ImGuiKey_LeftArrow)) {
      visCursIndex = 0;
      defaultScrollDir = 0.0;
    }
    if (IsModShift() && IsKeyDown(ImGuiKey_RightArrow)) {
      visCursIndex = visEndIndex - 1;
      defaultScrollDir = 0.0;
    }
  
    auto oldVisMagnify = visMagnify;
    if (IsModNone() && IsKeyPressed(ImGuiKey_DownArrow, true)) {
      if (startsWith(repr(visMagnify), "5")) {
        visMagnify = 2 * (visMagnify / 5);
      }
      else {
        visMagnify = max(1, visMagnify / 2);
      }
    }
    else if (IsModNone() && IsKeyPressed(ImGuiKey_UpArrow, false)) {
      if (startsWith(repr(visMagnify), "2")) {
        visMagnify = 5 * (visMagnify / 2);
      }
      else {
        visMagnify = visMagnify * 2;
      }
    }
    if (visMagnify != oldVisMagnify) {
      Wblue() << "Time scale x" << visMagnify;
    }
  }

  if (navMode == Nav_Sheet && reruns) {
    if (IsModNone() && IsKeyPressed(ImGuiKey_LeftBracket, true)) {
      reruns->selectPrevSeed();
    }
    if (IsModNone() && IsKeyPressed(ImGuiKey_RightBracket, true)) {
      reruns->selectNextSeed();
    }
    if (IsModNone() && IsKeyPressed(ImGuiKey_Backslash, false)) {
      reruns->toggleShowAllSeeds();
    }
    if (IsModNone() && IsKeyPressed(ImGuiKey_P, false)) {
      reruns->selectPureSheet();
    }
  }

}


void SheetUi::ctlSetupKnobs()
{
  doAdjustParams.clear();
  adjustKnobNames.clear();
  if (auto j = Joybox::primary) {
    switch (j->type) {

      case Joybox_TangentWave2:
        adjustKnobNames = vector<char const *> {
          "\ue001", "\ue002", "\ue003",
          "\ue00b", "\ue00c", "\ue00d",
          "\ue00e", "\ue00f", "\ue010",
          "\ue011", "\ue012", "\ue013",
        };
        doAdjustParams = vector<S32> {
          j->knobs[9],
          j->knobs[10],
          j->knobs[11],
          j->knobs[0],
          j->knobs[1],
          j->knobs[2],
          j->knobs[3],
          j->knobs[4],
          j->knobs[5],
          j->knobs[6],
          j->knobs[7],
          j->knobs[8]
        };
        break;
    }
  }
  knobAttachment.resize(doAdjustParams.size());
}



InsetWindow::InsetWindow(MainWindow &_win, string _windowName, CellSet const &_cells)
  : windowName(_windowName),
    cells(_cells),
    dl(_win)
{
  dl.calcProjView = [](G8DrawList &dl) {
    float aspect = float(dl.vpR - dl.vpL) / float(dl.vpB - dl.vpT);
    F dist = 30.0f;
    F xScale = dist / 2.7f;
    F yScale = xScale * aspect;
    dl.view << 
      1, 0, 0, 0.2,
      0, 1, 0, -dl.lookat.y(),
      0, 0, 1, -dist,
      0, 0, 0, 1;
    dl.proj <<
      xScale, 0, 0, -dist,
      0, yScale, 0, +dist,
      0, 0, -1, -1,
      0, 0, -1, 1;
    if (0) L() << "fov=" << dl.fov << " proj=" << dl.proj << " view=" << dl.view;
  };

}

void InsetWindow::toggleCell(CellIndex cell)
{
  if (!cells[cell]) {
    windowOpen = true;
    cells[cell] = true;
    pleaseScrollToBottom = true;
  }
  else {
    if (!windowOpen) {
      windowOpen = true;
    }
    else {
      cells[cell] = false;
      if (cells.empty()) {
        windowOpen = false;
      }
    }
  }
}


bool beginPickItem()
{
  return BeginPopup("pickItem", ImGuiWindowFlags_AlwaysAutoResize|ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoSavedSettings);
}
