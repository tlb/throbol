#include "./emit.h"
#include "./emit_macros.h"


deftype(left)
{
  auto at = eval(n->ch(0));
  return at;
}

defemit(left)
{
  return setError("Unhandled operator <<");
}

defusage(left, R"(
  <<)");


deftype(right)
{
  auto at = eval(n->ch(0));
  return at;
}

defemit(right)
{
  return setError("Unhandled operator >>");
}

defusage(right, R"(
  >>)");


deftype(null)
{
  L() << "What is " << n->t << " doing in a final AST?\n";
  return setError("Internal error");
}

defemit(null)
{
  return setError("Unhandled operator null");
}

defusage(null, R"(
  null)");



deftype(paramref)
{
  return typetag<F>();
}

defemit(paramref)
{
  ce.hasParam = true;

  auto srcRef = ce.resolveName(n->ch(0), true);
  if (!srcRef.valid()) return setError("Not a param", n->ch(0));
  if (!(n->data<ParamIndex>() < ce.sh.paramsByCell[srcRef.cell].size())) return setError("No such param in cell", n->ch(1));
  auto paramIndex = ce.sh.paramsByCell[srcRef.cell].at(n->data<ParamIndex>());

  defopn(F, r = cpu.paramss[batchi]->valueByParam(paramIndex));
  return setInvalidArgs();
}

defusage(paramref, R"(
  cellname@index -- eg foo@0)");
