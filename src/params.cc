#include "./params.h"
#include <xxhash.h>

void Paramset::updateHash()
{
  paramHash_ = XXH64((void const *)valueByParam_.data(), valueByParam_.size() * sizeof(valueByParam_[0]), 0);
  hashValid = true;
}

