#pragma once
#include "./main_window.h"
#include "./policy.h"

struct RerunSet;
struct SimRobotClient;
struct InsetWindow;

struct SheetLayoutGlobal {

  SheetLayoutGlobal() = default;
  ~SheetLayoutGlobal() = default;

  int cursIndex = 0;
  F cursXrel = 0.0f;
  int lhsIndex = 0, rhsIndex = 0;

  vector<pair<int, F>> samples;

};

struct SheetLayoutCell {

  SheetLayoutGlobal &g;
  vec3 pos;
  vec3 origin;
  F tScale = 0, vScale = 0;
  F vMax = 0, vMin = 0;
  vec3 graphAxisX, graphAxisY;
  vec3 cursX;
  F formulaWidth = 0, formulaHeight = 0, totWidth = 0, totHeight = 0;
  bool altSeed = false;
  bool forInset = false;
  bool forExport = false;
  bool forDebug = false;
  bool drawTimeLabels = false;
  F thicken = 1.0f;
  F thickenPixels = 1.0f;
  F knobRad = 0.25f;
  
  SheetLayoutCell(SheetLayoutGlobal &_g)
    :g(_g)
  {
  }

  ~SheetLayoutCell() = default;

  auto const &samples() const { return g.samples; }

  void growTot(F x, F y) {
    totWidth = max(totWidth, x);
    totHeight = max(totHeight, y);
  }
  void growFormula(F x, F y) {
    formulaWidth = max(formulaWidth, x);
    formulaHeight = max(formulaHeight, y);
    totWidth = max(totWidth, x);
  }

};

enum PickType {
  PickNone,
  PickCell,
  PickIncludePage,
  PickParam,
  PickLive,
  PickCustom,
  PickSquiggle,
};

struct PickItem {

  PickType t;
  U32 i;

  PickItem()
   :t(PickNone),
    i(U32(-1)) 
  {
  }

  PickItem(PickType _t, U32 _i)
   :t(_t),
    i(_i)
  {
    assert(t==PickNone || i != U32(-1));
  }

  void clear()
  {
    t = PickNone;
    i = U32(-1);
  }

  bool isCell() const
  {
    return t == PickCell;
  }

  bool isCell(CellIndex cell) const
  {
    return t == PickCell && i == cell;
  }

  bool isIncludePage() const
  {
    return t == PickIncludePage;
  }

  bool isIncludePage(PageIndex page) const
  {
    return t == PickIncludePage && i == page;
  }


  bool isParam() const
  {
    return t == PickParam;
  }

  bool isParam(ParamIndex param) const
  {
    return t == PickParam && i == param;
  }

  bool isSquiggle() const
  {
    return t == PickSquiggle;
  }

  bool isSquiggle(ParamIndex param) const
  {
    return t == PickSquiggle && i == param;
  }

  bool isNone() const
  {
    return t == PickNone;
  }

  ParamIndex asParamIndex() const
  {
    assert(isParam() || isSquiggle());
    return ParamIndex(i);
  }

  CellIndex asCellIndex() const
  {
    assert(isCell());
    return CellIndex(i);
  }

  CellIndex asPageIndex() const
  {
    assert(isIncludePage());
    return PageIndex(i);
  }

};

struct DrawWire {

  CellIndex srcCell;
  TextIndex dstLoc;

};

enum NavMode {
  Nav_Sheet,
  Nav_Fly,
  Nav_EditCell,
  Nav_Search
};

struct TraceTexResources {
  bool inited = false;
  bool empty = false;
  WGPUBuffer traceVisInfoBuf = nullptr;
  WGPUBuffer traceDataBuf = nullptr;
  int64_t mruLoopCount = 0;

  TraceTexResources() = default;
  TraceTexResources(TraceTexResources const &other) = delete;
  TraceTexResources(TraceTexResources &&other) = delete;
  ~TraceTexResources();
};

struct SheetUi : MainUi, enable_shared_from_this<SheetUi> {

  G8DrawList sheetView;
  Sheet sh;
  shared_ptr<Trace> activeTrace;
  vector<bool> visByPage;

  shared_ptr<SimRobotClient> liveClient;
  atomic<bool> recalcActive = false;
  shared_ptr<ParticleSearchState> policySearchState;

  int visCursIndex = 0;
  int visBeginIndex = 0, visEndIndex = 0;
  int visMagnify = 1;
  R scrollStartTime = 0.0;
  R scrollDir = 0.0, defaultScrollDir = 1.0;
  int scrollStartTick = 0;
  F visCursTime = 0.0f, visBeginTime = 0.0f, visEndTime = 0.0f; // must be in sync with visBeginIndex, visEndIndex, activeTrace

  PickItem pickItem;
  CellSet selectedCells;
  CellSet rubberBandedCells;
  CellSet visibleCells;
  vector<PageIndex> selectedIncludePages;

  vector<PickItem> knobAttachment;
  vector<F> formulaWidthByCell;
  vector<F> formulaHeightByCell;
  vector<F> totWidthByCell;
  vector<F> totHeightByCell;
  vector<vector<vec3>> charPosMapByCell;
  vector<TextIndex> formulaStartByCell;
  vector<TextIndex> nameEndByCell;
  vector<TextIndex> nameStartByCell;
  vector<shared_ptr<PolicyTerrainMap>> policyTerrainMapByParam;
  vector<bool> policyTerrainExactByParam;
  shared_ptr<CellVals> policyTerrainRangeByVal;

  struct CellVideoSitchy {
    CellVideoSitchy(MainWindow &_win)
      :texCache(_win)
    {
    }

    int bestIndex = -1;
    R lastUsed = 0.0;
    BlobRef mostRecentFetch;
    TexCache texCache;

  };
  vector<shared_ptr<CellVideoSitchy>> recentVideoTexByCell;

  vector<vec3> knobPosByParam;
  PickItem editItem;
  string editCellName;
  CellSet rewardCells;
  U32 editCellCursorPos = 0;
  string searchPrefix;
  int searchIndex = 0;
  CellSet searchHits;

  shared_ptr<RerunSet> reruns;

  bool showUndoWindow = false;
  bool showPagesWindow = false;
  bool showRerunsWindow = false;
  bool showDebugWindow = false;
  bool showApiWindow = false;
  bool showHelpWindow = false;

  bool uiPerfTestMode = false;
  int uiPerfTestCounter = 0;
  vec3 uiPerfTestCenter;

  NavMode navMode{Nav_Sheet};

  vec3 cursorPos;
  vec3 cursorVel;
  vec3 cursorZipTarget;
  F screenScale = 20.0;
  F cursorZipStrength = 0.0;
  vec3 mousePickPos;
  vec3 clickPickPos;
  vec3 dragLastPickPos;
  vec3 pivotGroundPos;
  vec3 sheetBoundsLo, sheetBoundsHi;
  F zoomScale = 1.0f;
  F renderElevation = 1.5625;
  F renderYaw = 0.0;
  F fontSize = 0.202;
  bool dragCellsActive = false;
  bool dragRectActive = false;
  bool dragIncludePageActive = false;

  // Set initially on the page by initGlobalDo().
  // May be set further while drawing.
  // Acted on and cleared by execGlobalDoPost()
  bool doSave = false, doReload = false;
  bool doExport = false;
  bool doUndo = false, doRedo = false;
  bool doCut = false, doCopy = false, doPaste = false;
  bool doSearch = false;
  bool doReparse = false;
  bool doNewCell = false;
  bool doStartLive = false;
  bool doEscape = false;
  bool doClearRewards = false;

  bool showMultipleValues = false;

  vector<int> doAdjustParams; // how many clicks of the knob to move by.
  vector<char const *> adjustKnobNames;

  vector<tuple<string, string, vec2>> pasteBuffer;
  string yankBuffer;
  bool yankBufferAppend = false;

  vector<shared_ptr<InsetWindow>> insets;

  unordered_map<ObjectKey, TraceTexResources> traceTexCache;

  SheetUi(MainWindow &_win);
  ~SheetUi();

  void setupMainUi() override;
  void initGlobalDo() override;
  void execGlobalDoPost() override;
  void drawBackground() override;
  void drawMainUi() override;
  void renderBackground(WGPURenderPassEncoder wgpu_pass) override;
  bool loadDocument(vector<string> fns) override;
  void uiAutosave() override;
  void prepareDraw() override;
  void pleaseClose() override;
  void startLive();
  void exportCells(Trace &tr);

  PageIndex getNewCellPage();
  void calcSheet();
  void setVisRange(int _begin, int _end);

  void ctlSheetView(G8DrawList &dl);
  void ctlSheetContent(G8DrawList &dl);
  void ctlSheetViewMenus1();
  void ctlSheetViewMenus2();
  void ctlSetupKnobs();
  void ctlEditCell(G8DrawList &dl);
  void ctlSearch(G8DrawList &dl);
  void ctlEditMode(G8DrawList &dl);
  void ctlEditCommands(G8DrawList &dl);
  void ctlNavigate(G8DrawList &dl);
  void ctlSelect(G8DrawList &dl);
  void ctlScrub(G8DrawList &dl);

  template<typename RefType>
  void autoRange(CellIndex cell, vector<Trace *> const &traces, vector<RefType> const &brs, DrawInfo &drawInfo);

  vec3 cellDisplayPos(CellIndex cell);
  SheetLayoutGlobal getLayoutGlobal();
  SheetLayoutCell getLayoutCell(SheetLayoutGlobal &slg, CellIndex cell, vec3 pos, bool forInset, bool forDebug, bool forExport);

  void wrUiState(YAML::Node &y);
  void rdUiState(YAML::Node &y);

  void drawInsetWindows(Trace &tr);
  void drawSearchWindow();

  void drawPagesWindowContents();
  void drawRerunsWindowContents();
  void drawDebugWindowContents();
  void drawUndoWindowContents();
  void drawApiWindowContents();
  void drawHelpWindowContents();

  void drawSheetLinks(G8DrawList &dl);
  void drawSheetGrid(G8DrawList &dl);

  void drawSheetPane(G8DrawList &dl);
  void drawCell(G8DrawList &dl, CellIndex cell, CellVals *ref, SheetLayoutCell &lo);
  void drawCellData(G8DrawList &dl, CellIndex cell, CellVals *ref, SheetLayoutCell &lo, Trace &tr);
  void drawCellDebug(G8DrawList &dl, CellIndex cell, CellVals *ref, SheetLayoutCell &lo, Trace &tr);
  void drawCellCustom(G8DrawList &dl, CellIndex cell, Trace &tr, SheetLayoutCell &lo, vector<BoxedRef> const &br, DrawInfo &drawInfo);
  void drawCellVideo(G8DrawList &dl, CellIndex cell, Trace &tr, SheetLayoutCell &lo, vector<BoxedRef> const &br, DrawInfo &drawInfo);

  template<typename RefType>
  void drawCellLabel(G8DrawList &dl, CellIndex cell, Trace &tr, SheetLayoutCell &lo, vector<RefType> const &br, DrawInfo &drawInfo);

  void drawIncludePage(G8DrawList &dl, PageIndex page, vec3 pos);

  template<typename RefType>
  TraceTexResources &mkTraceBuf(CellIndex cell, vector<Trace *> const &trs, vector<RefType> const &brs, DrawInfo &drawInfo);

  template<typename RefType>
  void drawCellTraceTex(G8DrawList &dl, CellIndex cell, vector<Trace *> const &trs, SheetLayoutCell &lo, vector<RefType> const &brs, DrawInfo &drawInfo);

  void drawCellCursor(G8DrawList &dl, CellIndex cell, Trace &tr, SheetLayoutCell &lo, DrawInfo &drawInfo);
  void drawCellPureParam(G8DrawList &dl, CellIndex cell, CellVals *ref, SheetLayoutCell &lo, vector<BoxedRef> const &br, DrawInfo &drawInfo);
  void drawCellTimeInvariant(G8DrawList &dl, CellIndex cell, CellVals *ref, SheetLayoutCell &lo, vector<BoxedRef> const &br, DrawInfo &drawInfo);

  void drawLink(G8DrawList &dl, vec3 const &srcp, vec4 const &srccol, vec3 const &dstp, vec4 const &dstcol, F rad);

  void setPickItem(G8DrawList &dl);
  PickItem findItem(vec3 const &targ);
  void setEditItem(PickItem it);
  void commitCellName();
  void updateSearch();
  void updateCursorZip();
  void updateSheetData();
  void updateVisibleCells();
  void updatePolicyTerrain();

  void setProjView(G8DrawList &dl);
  void setSheetBounds();

  char const *getKnobNameByParam(ParamIndex pi);
  char const *getKnobNameByCell(CellIndex ci);
  void toggleParamAttachment(ParamIndex pi);
  void clearParamAttachment();
  tuple<PolicyTerrainMap *, bool> getPolicyTerrainMapByParam(ParamIndex pi);

  void cmdSetParam(ParamIndex param, F value);
  void cmdQuantizeParam(ParamIndex param, F steps);
  void cmdIncreaseParamRange(ParamIndex param);
  void cmdDecreaseParamRange(ParamIndex param);

};

struct InsetWindow {

  string windowName;
  CellSet cells;
  G8DrawList dl;
  bool windowOpen = false;
  bool pleaseScrollToBottom = false;

  InsetWindow(MainWindow &_win, string _windowName, CellSet const &_cells);
  InsetWindow() = delete;

  void toggleCell(CellIndex cell);
};

bool beginPickItem();
