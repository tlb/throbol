#pragma once
#include "./defs.h"

/*
  I would like to use `vector<U8>` but alignment rules get in the way
  This is allocated to align on MAX_ALIGN-byte boundaries,
  depending on what Eigen things is suitable for the
  instruction set it's using.
  Note that the constructor doesn't zero the memory.
  And `.alloc` doesn't preserve contents or init to 0.
  Call `.zero()` if you need that.
*/
struct Blob : AllocTrackingMixin<Blob> {

  size_t size_;
  U8 *data_;

  Blob() noexcept;
  explicit Blob(size_t _size) noexcept;
  Blob(U8 *_data, size_t _size) noexcept;
  Blob(Blob &&other) noexcept;
  Blob(Blob const &other);
  ~Blob() noexcept;

  Blob& operator=(Blob const &other) noexcept;
  Blob& operator=(Blob &&other) noexcept;

  void alloc(size_t _size);
  U8 *data() const { return data_; }
  size_t size() const { return size_; }
  bool empty() const { return size_ == 0; }
  void swap(Blob &other) noexcept;
  U8 & operator [] (size_t ofs)
  {
#if !defined(NDEBUG)
    if (data_ == nullptr) throw runtime_error("No blob");
    if (ofs > size_ ) throw runtime_error("Indexing past blob");
#endif
    return data_[ofs];
  }
  void zero();

};

/*
  A reference to a blob stored outside the database.
  This has to be copyable as POD, so the hostname string is fixed-length. Sorry.
*/
struct BlobRef {

  static constexpr size_t ORIGIN_HOST_SIZE = 16;

  U64 chunkId = 0;
  U32 partOfs = 0;
  U32 partSize = 0;
  char originHost[ORIGIN_HOST_SIZE];

  BlobRef()
    : chunkId(0),
      partOfs(0),
      partSize(0)
  {
  }
  
  BlobRef(
    U64 _chunkId,
    U32 _partOfs,
    U32 _partSize,
    char _originHost[ORIGIN_HOST_SIZE])
    : chunkId(_chunkId),
      partOfs(_partOfs),
      partSize(_partSize)
  {
    memcpy(originHost, _originHost, ORIGIN_HOST_SIZE);
  }

  BlobRef chunkRef()
  {
    return BlobRef(chunkId, 0, 0, originHost);
  }

  string getFilename() const;
  bool valid() const { return chunkId != 0; }

  template<typename Archive> void serialize(Archive &ar)
  {
    ar(chunkId, partOfs, partSize, originHost);
  }
};

ostream & operator <<(ostream &s, BlobRef const &a);

inline bool operator <(BlobRef const &a, BlobRef const &b)
{
  if (a.chunkId < b.chunkId) return true;
  if (b.chunkId < a.chunkId) return false;
  if (a.partOfs < b.partOfs) return true;
  if (b.partOfs < a.partOfs) return false;
  if (a.partSize < b.partSize) return true;
  if (b.partSize < a.partSize) return false;
  return false;
}

inline bool operator == (BlobRef const &a, BlobRef const &b)
{
  return a.chunkId == b.chunkId && a.partOfs == b.partOfs && a.partSize == b.partSize;
}

// Make unordered_map<BlobRef, T> work
namespace std
{
  template<> struct hash<BlobRef>
  {
    std::size_t operator()(BlobRef const &s) const noexcept
    {
      std::size_t h1 = std::hash<U64>{}(s.chunkId);
      std::size_t h2 = std::hash<U32>{}(s.partOfs);
      std::size_t h3 = std::hash<U32>{}(s.partSize);
      return h1 ^ ((h2 ^ (h3 << 1)) << 1);
    }
  };
}


Blob *getBlob(BlobRef const &id);
void stashBlob(BlobRef const &id, Blob const &data);
void stashBlob(BlobRef const &id, Blob &&data);
void stashBlob(BlobRef const &id, U8 *data, size_t size);

void clearBlobCache();

bool loadBlob(BlobRef const &id, Blob &data);

BlobRef mkBlob(U64 chunkId, U8 const *data, size_t dataSize);
BlobRef mkBlob(U64 chunkId, Blob const &data);

void saveBlob(BlobRef const &id, U8 const *data, size_t dataSize);
void saveBlob(BlobRef const &id, Blob const &data);
bool saveBlobFile(U64 chunkId, Blob const &data);

U64 mkBlobChunkId();
void rotateBlobChunkId(U64 &chunkId);

U64 parseChunkId(string const &chunkName);
string fmtChunkId(U64 chunkId);

bool loadBlobFile(U64 chunkId, Blob &data);

string blobFilename(U64 chunkId);
