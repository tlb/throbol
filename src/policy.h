#pragma once
#include "./defs.h"
#include "./boxed.h"
#include <thread>
#include <span>

/*
  This adapter class gives us a consistent way to read & modify parameter values,
  normalized for the parameter's scale.
  For instance, getParamValue returns something like:
    value = sh.params.valueByParam(param) + tweak * relRange * sh.rangeByParam[param];
            ^ base value                    ^ normalized       ^ parameter real range
  When we support parameters that aren't linear (say, that are multiplicative) we should handle it here.
*/
struct PolicySearchDim {

  ParamIndex param;
  F relRange;

  PolicySearchDim(ParamIndex _param = nullParamIndex, F _relRange = 0.2f)
    :param(_param), relRange(_relRange)
  {
  }

  bool valid() const
  {
    return param != nullParamIndex;
  }

  VectorXf getRelValues(int resolution) const;
  VectorXf getParamValues(Sheet &sh, int resolution) const;
  F getParamValue(Sheet &sh, F tweak) const;
  vector<pair<F, F>> getValues(Sheet &sh, int resolution) const; // vector of pair(relative value, actual param value)
  void setOverride(Trace *trace, F value) const;
  F getParamBase(Sheet &sh) const;

};

bool operator < (PolicySearchDim const &a, PolicySearchDim const &b);
bool operator == (PolicySearchDim const &a, PolicySearchDim const &b);
ostream & operator << (ostream &s, WithSheet<PolicySearchDim> const &a);

/*
  The complete spec for a policy terrain run.
  Make sure to increment policyTerrainEpoch when you change anything in here
  so that ScopeModel::updatePolicyTerrain will know to re-run the policy terrain
  calculation.
*/
struct PolicyTerrainSpec {

  PolicySearchDim xVar, yVar;
  vector<BoxedRef> rMetrics;
  U64 paramHash = 0;
  U64 compiledSheetEpoch = 0;
  int sampleIndex = 0;
  vector<F> baseValues;
  shared_ptr<ReplayBuffer> replayBuffer;
  int numSeedsToAverage = 1;

  PolicyTerrainSpec() = default;
  ~PolicyTerrainSpec() = default;

  bool valid() const;
};

bool operator < (PolicyTerrainSpec const &a, PolicyTerrainSpec const &b);


/*
  A completed policy terrain run. We capture the spec it was based off,
  and we can compare epochs to know if it's out of date.
  This map is between 2 independent variables (X and Y) and one dependent one (R).
*/
struct PolicyTerrainMap : AllocTrackingMixin<PolicyTerrainMap> {

  Sheet &sh;
  PolicyTerrainSpec spec;

  F xParamBase = 0.0, yParamBase = 0.0;
  int xResolution = 9, yResolution = 9;
  int costEst = 0;
  bool started = false;
  bool finished = false;
  bool destructed = false;

  // tuple is matrix, baser, rScale
  // Matric layout is (y 0..resolution, x 0..resolution)
  vector<tuple<MatrixXf, F, F>> rs;
  
  PolicyTerrainMap(Sheet &_sh, PolicyTerrainSpec const &_spec, int _xResolution, int _yResolution);
  ~PolicyTerrainMap();

  void calcStats();

};

pair<shared_ptr<PolicyTerrainMap>, bool> getPolicyTerrain(Sheet &sh, PolicyTerrainSpec const &desiredSpec, int boost=0);

void computePolicyTerrain();

enum ParticleSearchAlg {
  ParticleSearch_swarm,
  ParticleSearch_deRand1Bin,
  ParticleSearch_deBest2Bin,
  // ADD ParticleSearchAlg
};

ostream & operator << (ostream &s, ParticleSearchAlg const &a);

/*

*/
struct ParticleSearchSpec {

  std::span<PolicySearchDim> searchDims;
  BoxedRef rMetric;
  ParticleSearchAlg alg{ParticleSearch_deRand1Bin};

  U64 paramHash = 0;
  U64 compiledSheetEpoch = 0;
  int sampleIndex = 0;
  F baseValue = 0.0f;
  shared_ptr<ReplayBuffer> replayBuffer;

  // for swarm
  F accelTowardsBest = 0.01;
  F velCoeff = 0.925;
  F initialParamSigma = 1.5;
  F initialVelSigma = 0.05;

  // for differential evolution
  F deFAC = 0.9f;
  F deCR = 0.1;

  int populationTarget = 50;
  // ADD ParticleSearchSpec member (search for this when adding)
};

F score(ParticleSearchSpec const &spec, Trace *t);

bool operator < (ParticleSearchSpec const &a, ParticleSearchSpec const &b);

struct GenericSearchState {

  Sheet &sh;
  bool active = false;

  GenericSearchState(Sheet &_sh)
    :sh(_sh)
  {
  }
  virtual ~GenericSearchState() = default;

  virtual void start() = 0;
  virtual void stop() = 0;
  virtual void clear() = 0;
};


struct ParticleSearchState : GenericSearchState, enable_shared_from_this<ParticleSearchState>, AllocTrackingMixin<ParticleSearchState> {

  struct Particle {
    Particle(size_t dim);
    Particle(Particle const &other) = default;
    Particle(VectorXf const &_tweaks, VectorXf const &_tweakVels);
    /*
      range is always ~1, 
      indexed not by ParamIndex but by an index into activeParams
    */
    size_t nParams() const { return tweaks.size(); }
    VectorXf tweaks, tweakVels;

    bool evaluated;
    F evaluation;
  };

  ParticleSearchSpec spec;

  int iterCount = 0;

  atomic<int> threadsRunning = 0;
  shared_ptr<vector<Particle>> particles;

  shared_ptr<Particle> clickedBestParticle;

  ParticleSearchState(Sheet &_sh, ParticleSearchSpec const &_spec);
  ~ParticleSearchState();

  void start();
  void stop();
  void clear();
  void evalParticles();
  void doSwarm(vector<Particle> &newps, vector<Particle> const &oldps);
  void doDeRand1Bin(vector<Particle> &newps, vector<Particle> const &oldps);
  void doDeBest2Bin(vector<Particle> &newps, vector<Particle> const &oldps);
  void setNewGeneration(vector<Particle> const &newps);
  void poll();
  shared_ptr<Trace> mkAltTraceForParticle(Particle &newp);

  bool isInBounds(Particle const &p);
  void mkRandomParticle(Particle &newp);
  void writeBackBestParams();
  void writeBack(Particle &bestParticle);


  static ParticleSearchState *lookup(Sheet &sh, ParticleSearchSpec const &desiredSpec);

};


ostream & operator <<(ostream &s, ParticleSearchState::Particle const &a);



/*
  Optimizers based on various minpack algorithms, as implemented by deps/CppNumericalSolvers
*/

enum MinpackAlg {
  Minpack_NewtonDescent,
  Minpack_GradientDescent,
  Minpack_ConjugatedGradientDescent,
  Minpack_Bfgs,
  Minpack_Lbfgs,
  Minpack_Lbfgsb,
  // ADD MinpackAlg
};

ostream & operator << (ostream &s, MinpackAlg const &a);

enum MinpackAlgState {
  Minpack_initial,
  Minpack_stepping,
  Minpack_done,
};

struct MinpackSearchSpec {
  
  std::span<PolicySearchDim> searchDims;
  BoxedRef rMetric;
  MinpackAlg alg{Minpack_Lbfgsb};

  U64 paramHash = 0;
  U64 compiledSheetEpoch = 0;
  int sampleIndex = 0;
  F baseValue = 0.0f;
  shared_ptr<ReplayBuffer> replayBuffer;

  F regularization = 0.1;
  // ADD MinpackSearchSpec member (search for this when adding)

  MinpackSearchSpec() = default;
  ~MinpackSearchSpec() = default;

};

bool operator < (MinpackSearchSpec const &a, MinpackSearchSpec const &b);

F score(MinpackSearchSpec const &spec, Trace *t);

struct MinpackSearchState;

struct MinpackSearchDeets {

  MinpackSearchDeets() = default;
  virtual ~MinpackSearchDeets() = default;

  virtual void step(MinpackSearchState *owner) = 0;
};

struct MinpackSearchState : GenericSearchState, AllocTrackingMixin<MinpackSearchState> {

  MinpackSearchSpec spec;
  MinpackAlgState algState{Minpack_initial};

  vector<tuple<VectorXf, F>> xpath;
  VectorXf bestx;
  F bestval = numeric_limits<float>::infinity();
  string message;

  shared_ptr<MinpackSearchDeets> deets;

  shared_ptr<tuple<VectorXf, F>> clickedBestSoln;

  MinpackSearchState(Sheet &_sh, MinpackSearchSpec const &_spec);
  ~MinpackSearchState();

  void start();
  void stop();
  void clear();
  void poll();
  void writeBackBestParams();
  void writeBack(VectorXf const &x);

  static MinpackSearchState *lookup(Sheet &sh, MinpackSearchSpec const &desiredSpec);
};

