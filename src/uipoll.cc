#include "./uipoll.h"
#include "./vbmainloop.h"
#include "./main_window.h"
#if defined(__EMSCRIPTEN__)
#include <emscripten.h>
#include <emscripten/html5.h>
#endif

bool uiPollLive;

/*
  This is a big compromise between asio, OpenGL and imgui.
  We arrange a repeating asio timer event every 16 mS and check
  whether something requires a redraw.
  We could do better if we could get an event for the vertical
  retrace, but as far as I can tell we can only get that by
  calling SDL_GL_SwapWindow, which we can only do on the
  main thread (at least on Mac).

  There's a global variable uiPollLive visible everywhere (externed in uipoll.h)
  which can be set anywhere to trigger a redraw. We check this when the 16 mS timer
  triggers to see if we have to run the UI.
*/

static vector<shared_ptr<UiPollable>> pollable;

static int uiActiveReservoir = 1;


#if !defined(__EMSCRIPTEN__)

constexpr int verbose = 0;

// we use this single context for everything, including the UI drawing.
asio::io_context &io_context = *new asio::io_context(1); // singlethread

asio::steady_timer *uiPollTimer;
asio::steady_timer *autosavePollTimer;

void asyncUiPoll()
{
  if (!uiPollTimer) uiPollTimer = new asio::steady_timer(io_context);
  uiPollTimer->expires_at(max(
    std::chrono::steady_clock::now() + std::chrono::milliseconds(5), // avoid hogging the main loop
    uiPollTimer->expiry() + std::chrono::milliseconds(16))); // but hit 60 Hz when we can

  uiPollTimer->async_wait(
    [](boost::system::error_code error) {
      if (error) {
        L() << "asyncUiPoll: " << error.message();
        return;
      }
      int userVisibleCount = 0;

      for (auto &w : pollable) {
        if (w) {
          userVisibleCount++;
          auto result = w->uiPoll();
          if (result == UiPoll_closeme) {
            w = nullptr;
            // This timer would otherwise keep alive the io_context loop, so trigger it now
            if (autosavePollTimer) autosavePollTimer->cancel();
          }
          else if (result == UiPoll_userVisible) {
            userVisibleCount++;
          }
        }
      }

      if (uiPollLive) {
        uiPollLive = false;
        uiActiveReservoir = 3;
      }
      if (verbose >= 2) L() << "uiPollLive=" << uiPollLive << " uiActiveReservoir=" << uiActiveReservoir;

      if (uiActiveReservoir > 0) {
        uiActiveReservoir--;
        for (auto &w : pollable) {
          if (w) {
            if (verbose >= 2) L() << "Redraw\n";
            w->uiRedraw();
          }
        }
      }

      if (userVisibleCount > 0) {
        asyncUiPoll();
      }
      else {
        if (verbose) L() << "asyncUiPoll: done";
      }
    });
}

void asyncUiAutosave()
{
  if (!autosavePollTimer) autosavePollTimer = new asio::steady_timer(io_context);
  autosavePollTimer->expires_after(std::chrono::milliseconds(15000));
  autosavePollTimer->async_wait(
    [](boost::system::error_code error) {
      if (error) {
        if (error == asio::error::operation_aborted) {
          if (verbose) L() << "asyncUiAutosave: aborted";
          return;
        }
        L() << "asyncUiAutosave: " << error.message();
        return;
      }
      bool didSome = false;
      for (auto &w : pollable) {
        if (w) {
          w->uiAutosave();
          didSome = true;
        }
      }
      if (didSome) {
        asyncUiAutosave();
      }
      else {
        if (verbose) L() << "asyncUiAutosave: done";
      }
    });
}  

void startUiPoll(shared_ptr<UiPollable> w)
{
  pollable.erase(remove(pollable.begin(), pollable.end(), nullptr), pollable.end());
  pollable.push_back(w);

  if (pollable.size() == 1) {
    asyncUiPoll();
    asyncUiAutosave();
  }

}

#else

void emscriptenIter() {
  int userVisibleCount = 0;
  for (auto &w : pollable) {
    if (w) {
      userVisibleCount++;
      auto result = w->uiPoll();
      if (result == UiPoll_closeme) {
        w = nullptr;
      }
      else if (result == UiPoll_userVisible) {
        userVisibleCount++;
      }
    }
  }

  if (uiPollLive) {
    uiPollLive = false;
    uiActiveReservoir = 3;
  }

  if (uiActiveReservoir > 0) {
    uiActiveReservoir--;
    for (auto &w : pollable) {
      if (w) {
        w->uiRedraw();
      }
    }
  }

  if (userVisibleCount == 0) {
    L() << "No windows, calling emscripten_cancel_main_loop";
    emscripten_cancel_main_loop();
  }
}

void startUiPoll(shared_ptr<UiPollable> w)
{
  static bool mainLoopDone = false;

  if (w) {
    pollable.push_back(w);
  }
  if (!mainLoopDone) {
    mainLoopDone = true;
    emscripten_set_main_loop(emscriptenIter, 0, 0);
  }
}

#endif


void UiPollable::uiAutosave()
{
}

void UiPollable::uiRedraw()
{
}

SyncLogger::LineQueue windowLineq;

SyncLogger W(U32 color)
{
  uiPollLive = true;
  return SyncLogger(cerr, windowLineq, color);
}

SyncLogger W()
{
  return W(IM_COL32(0xff,0xff,0xff,0xff));
}

SyncLogger Wred()
{
  return W(IM_COL32(0xff,0x88,0x88,0xff));
}

SyncLogger Wgreen()
{
  return W(IM_COL32(0x88,0xff,0x88,0xff));
}

SyncLogger Wyellow()
{
  return W(IM_COL32(0xff,0xff,0x00,0xff));
}

SyncLogger Wblue()
{
  return W(IM_COL32(0x88,0x88,0xff,0xff));
}
