#include "./policy.h"
#include "./core.h"
#include "./emit.h"
#include "common/asyncjoy.h"



static int ptmCostPending;

void calcPolicyTerrainMap(shared_ptr<PolicyTerrainMap> newMap)
{
  static const int verbose = 0;
  assert(!newMap->started);
  newMap->started = true;
  ptmCostPending += newMap->costEst;
  if (verbose >= 1) L() << "Start  ptm " << newMap->costEst << " for " << newMap.get();

  auto t0 = clock();
  auto aj = make_shared<AsyncJoy>([newMap, t0](string const &err) {
    newMap->calcStats();
    newMap->finished = true;
    ptmCostPending -= newMap->costEst;
    uiPollLive = true;
    auto t1 = clock();
    if (verbose >= 1) L() << "Finish ptm " << newMap->costEst << " for " << newMap.get() << " in " << repr_clock(t1-t0);
  });

  auto &sh = newMap->sh;

  for (auto bv : newMap->spec.baseValues) {
    newMap->rs.emplace_back(
      MatrixXf::Zero(newMap->yResolution, newMap->xResolution),
      bv,
      0.0f);
  }

  newMap->xParamBase = newMap->spec.xVar.getParamBase(sh);
  newMap->yParamBase = newMap->spec.yVar.getParamBase(sh);

  auto xParamValues = newMap->spec.xVar.getParamValues(sh, newMap->xResolution);
  auto yParamValues = newMap->spec.yVar.getParamValues(sh, newMap->yResolution);

  for (long yi = 0; yi < yParamValues.size(); yi++) {
    for (long xi = 0; xi < xParamValues.size(); xi++) {
      for (int seed = 0; seed < newMap->spec.numSeedsToAverage; seed++) {
        auto newTrace = make_shared<Trace>(sh.compiled);
        newTrace->setupInitial();
        newTrace->replayBuffer = newMap->spec.replayBuffer;
        newTrace->limitMaxIndex(newMap->spec.sampleIndex + 1);
        newMap->spec.xVar.setOverride(newTrace.get(), xParamValues[xi]);
        newMap->spec.yVar.setOverride(newTrace.get(), yParamValues[yi]);

        aj->start();

        simulateTraceAsync(std::move(newTrace), [newMap, aj, xi, yi](shared_ptr<Trace> newTrace2) {
          for (size_t ri = 0; ri < newMap->spec.rMetrics.size(); ri++) {
            auto &rMetric = newMap->spec.rMetrics[ri];
            F rMetricValue = newTrace2->getVal_F(newMap->spec.sampleIndex, rMetric);
            get<0>(newMap->rs.at(ri))(yi, xi) += rMetricValue;
          }
          aj->end();
        });
      }
    }
  }
  aj->end();
}

static map<PolicyTerrainSpec, shared_ptr<PolicyTerrainMap>> stateCacheBySpec;
static deque<shared_ptr<PolicyTerrainMap>> stateLru;
static vector<shared_ptr<PolicyTerrainMap>> statePending;

size_t refSeqCounter;

void computePolicyTerrain()
{
  const int MAX_PENDING = 50;
  if (!statePending.empty()) {
    if (ptmCostPending < MAX_PENDING) {
      sort(statePending.begin(), statePending.end(), [](auto const &a, auto const &b) {
        return a->costEst < b->costEst;
      });

      for (auto &newMap : statePending) {
        calcPolicyTerrainMap(newMap);
        if (ptmCostPending >= MAX_PENDING) break;
      }
    }
    statePending.clear();
  }
}

pair<shared_ptr<PolicyTerrainMap>, bool> getPolicyTerrain(Sheet &sh, PolicyTerrainSpec const &desiredSpec, int boost)
{
  assert(desiredSpec.rMetrics.size() == desiredSpec.baseValues.size());

  auto &slot = stateCacheBySpec[desiredSpec];
  if (slot && slot->finished) {
    return pair(slot, true);
  }

  // Perhaps caller will settle for a slightly out of date one?
  F bestDistancesq = 999;
  shared_ptr<PolicyTerrainMap> bestMap = nullptr;
  for (auto &it : stateLru) {
    if (it &&
      it->finished &&
      it->spec.xVar.param == desiredSpec.xVar.param &&
      it->spec.yVar.param == desiredSpec.yVar.param &&
      it->spec.rMetrics == desiredSpec.rMetrics) {

      auto distancesq = 0.0f;
      if (it->spec.paramHash != desiredSpec.paramHash) {
        distancesq += 1.0f;
      }
      if (it->spec.compiledSheetEpoch != desiredSpec.compiledSheetEpoch) {
        distancesq += 5.0f;
      }
      distancesq += sqr(F(int(it->spec.sampleIndex) - int(desiredSpec.sampleIndex)) * 0.1f);

      if (desiredSpec.xVar.valid()) {
        distancesq += sqr((it->xParamBase - sh.params.valueByParam(desiredSpec.xVar.param)) / sh.rangeByParam[desiredSpec.xVar.param]);
      }
      if (desiredSpec.yVar.valid()) {
        distancesq += sqr((it->yParamBase - sh.params.valueByParam(desiredSpec.yVar.param)) / sh.rangeByParam[desiredSpec.yVar.param]);
      }

      if (distancesq < bestDistancesq) {
        bestDistancesq = distancesq;
        bestMap = it;
      }
    }
  }

  if (!slot) {
    auto xResolution = desiredSpec.xVar.valid() ? 9 : 1;
    auto yResolution = desiredSpec.yVar.valid() ? 9 : 1;
    if (yResolution == 1) xResolution *= 3;

    auto newMap = make_shared<PolicyTerrainMap>(sh, desiredSpec, xResolution, yResolution);
    slot = newMap;
    stateLru.push_front(newMap);
    while (stateLru.size() > 50) {

      auto oldest = stateLru.back();
      if (oldest.get() == bestMap.get()) break;
      assert(stateCacheBySpec[oldest->spec] == oldest);
      stateCacheBySpec.erase(oldest->spec);
      stateLru.pop_back();
    }
  }

  if (slot && !slot->started) {
    statePending.push_back(slot);
  }

  return pair(bestMap, false);
}
