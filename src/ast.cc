#include "./core.h"
#include "./ast.h"


void updateSubstr(FormulaSubstr &loc,  vector<U32> const &newPosByOldPos)
{
  loc.begin = newPosByOldPos[std::min(loc.begin, U32(newPosByOldPos.size()-1))];
  loc.end = newPosByOldPos[std::min(loc.end, U32(newPosByOldPos.size()-1))];
}

/*
  Recursively update all formula positions according to the old-new map.
*/
void updateAstSubstrs(AstNode *n, vector<U32> const &newPosByOldPos)
{
  if (!n) return;
  updateSubstr(n->loc, newPosByOldPos);
  for (int i = 0; i < n->nch(); i++) {
    updateAstSubstrs(n->ch(i), newPosByOldPos);
  }
}

ostream & operator << (ostream &s, AstType t)
{
  switch (t) {
    #define defrepr(T) case A_##T: return s << #T;
    foreach_ast(defrepr)
    #undef defrepr
  }
  return s << "?";
};

ostream & operator <<(ostream &s, AstNode *a)
{
  if (!a) return s << "null";
  s << a->t;
  switch (a->t) {

    case A_number:
    case A_constant:
      s << ".f=" << a->data<F>();
      break;

    case A_param:
      s << ".i=" << a->data<ParamIndex>();
      break;

    case A_cellref:
      s << ".i=" << a->data<CellIndex>();
      break;

    case A_string:
    case A_name:
      s << ".s=" << a->data_sv();
      break;

    case A_typecast:
      s << ".tt=" << a->data<TypeTag>();
      break;

    default:
      break;
  }
  bool first = true;
  for (int i = 0; i < a->nch(); i++) {
    if (first) {
      s << "[";
      first = false;
    }
    else {
       s << ", ";
    }
    s << a->ch(i);
  }
  if (!first) s << "]";
  return s;
}


ostream & operator <<(ostream &s, WithSheet<AstNode *> sha)
{
  auto &[sh, a] = sha;

  if (!a) return s << "null";
  s << repr(a->t);
  switch (a->t) {

    case A_number:
    case A_constant:
      s << ".f=" << a->data<F>();
      break;

    case A_param:
      s << ".i" << a->data<ParamIndex>();
      break;

    case A_cellref:
      s << ".i=" << a->data<CellIndex>();
      if (a->data<CellIndex>() != nullCellIndex) {
        s << "=" << sh.nameByCell[a->data<CellIndex>()];
      }
      break;

    case A_string:
    case A_name:
      s << ".s=" << a->data_sv();
      break;

    default:
      break;
  }
  bool first = true;
  for (int i = 0; i < a->nch(); i++) {
    if (first) {
      s << "[";
      first = false;
    }
    else {
       s << ", ";
    }
    s << WithSheet(sh, a->ch(i));
  }
  if (!first) s << "]";
  return s;
}



optional<F> getConstF(AstNode *n)
{
  if (!n) return nullopt;
  switch (n->t) {
    case A_number:
      return n->data<F>();

    case A_constant:
      return n->data<F>();

    case A_neg:
      if (n->nch() == 1) {
        if (auto af = getConstF(n->ch(0))) {
          return - *af;
        }
      }
      break;

    case A_add:
      if (n->nch() == 2) {
        if (auto af = getConstF(n->ch(0))) {
          if (auto bf = getConstF(n->ch(1))) {
            return *af + *bf;
          }
        }
      }
      break;

    case A_sub:
      if (n->nch() == 2) {
        if (auto af = getConstF(n->ch(0))) {
          if (auto bf = getConstF(n->ch(1))) {
            return *af - *bf;
          }
        }
      }
      break;

    case A_mul:
      if (n->nch() == 2) {
        if (auto af = getConstF(n->ch(0))) {
          if (auto bf = getConstF(n->ch(1))) {
            return *af * *bf;
          }
        }
      }
      break;

    case A_div:
      if (n->nch() == 2) {
        if (auto af = getConstF(n->ch(0))) {
          if (auto bf = getConstF(n->ch(1))) {
            return *af / *bf;
          }
        }
      }
      break;

    default:
      break;
  }

  return nullopt;
}

optional<int> getConstInt(AstNode *a)
{
  if (!a) return nullopt;

  if (auto af = getConstF(a)) {
    auto roundAf = round(*af);
    if (*af == roundAf) {
      return int(roundAf);
    }
  }
  return nullopt;
}


optional<CF> getConstCF(AstNode *n)
{
  if (!n) return nullopt;

  if (auto nf = getConstF(n)) {
    return CF(*nf, 0);
  }

  switch (n->t) {
    case A_im:
      return CF(0, 1);
    
    case A_neg:
      if (n->nch() == 1) {
        if (auto af = getConstCF(n->ch(0))) {
          return - *af;
        }
      }
      break;

    case A_add:
      if (n->nch() == 2) {
        if (auto acf = getConstCF(n->ch(0))) {
          if (auto bcf = getConstCF(n->ch(1))) {
            return *acf + *bcf;
          }
        }
      }
      break;

    case A_sub:
      if (n->nch() == 2) {
        if (auto acf = getConstCF(n->ch(0))) {
          if (auto bcf = getConstCF(n->ch(1))) {
            return *acf - *bcf;
          }
        }
      }
      break;

    case A_mul:
      if (n->nch() == 2) {
        if (auto acf = getConstCF(n->ch(0))) {
          if (auto bcf = getConstCF(n->ch(1))) {
            return *acf * *bcf;
          }
        }
      }
      break;

    case A_div:
      if (n->nch() == 2) {
        if (auto acf = getConstCF(n->ch(0))) {
          if (auto bcf = getConstCF(n->ch(1))) {
            return *acf / *bcf;
          }
        }
      }
      break;

    default:
    break;
  }

  return nullopt;
}


optional<string_view> getConstString(Arena *pool, AstNode *n)
{
  if (!n) return nullopt;
  switch (n->t) {

    case A_string:
      return n->data_sv();
      break;

    case A_repr:
      if (n->nch() == 1) {
        if (auto af = getConstF(n->ch(0))) {
          return pool->strdupview(repr(*af));
        }
      }
      break;

    case A_add:
      if (n->nch() == 2) {
        if (auto af = getConstString(pool, n->ch(0))) {
          if (auto bf = getConstString(pool, n->ch(1))) {
            return pool->strdupview(string(*af) + string(*bf));
          }
        }
      }
      break;

    default:
      break;
  }

  return nullopt;
}


// If the node resolves to a first-degree polynomial in dt, return
// tuple(a, b) where the polynomial is a + b*dt;
optional<tuple<F, F>> getDtMultiple(AstNode *a)
{
  if (!a) return nullopt;

  switch (a->t) {

    case A_dt:
      return make_tuple(0.0f, 1.0f);

    case A_neg:
      if (auto arg = getDtMultiple(a->ch(0))) {
        return make_tuple(-get<0>(*arg), -get<1>(*arg));
      }
      break;

    case A_mul:
      if (auto arg0 = getDtMultiple(a->ch(0))) {
        if (auto arg1 = getDtMultiple(a->ch(1))) {
          if (get<1>(*arg0) == 0 || get<1>(*arg1) == 0) {
            return make_tuple(get<0>(*arg0) * get<0>(*arg1), get<0>(*arg0) * get<1>(*arg1) + get<1>(*arg0) * get<0>(*arg1));
          }
        }
      }
      break;

    case A_div:
      if (auto arg0 = getDtMultiple(a->ch(0))) {
        if (auto arg1 = getConstF(a->ch(1))) {
          return make_tuple(get<0>(*arg0) / *arg1, get<1>(*arg0) / *arg1);
        }
      }
      break;

    case A_add:
      if (auto arg0 = getDtMultiple(a->ch(0))) {
        if (auto arg1 = getDtMultiple(a->ch(1))) {
          return make_tuple(get<0>(*arg0) + get<0>(*arg1), get<1>(*arg0) + get<1>(*arg1));
        }
      }
      break;
    
    case A_sub:
      if (auto arg0 = getDtMultiple(a->ch(0))) {
        if (auto arg1 = getDtMultiple(a->ch(1))) {
          return make_tuple(get<0>(*arg0) - get<0>(*arg1), get<1>(*arg0) - get<1>(*arg1));
        }
      }
      break;

    case A_number:
      return make_tuple(a->data<F>(), 0.0f);

    case A_constant:
      return make_tuple(a->data<F>(), 0.0f);

    default:
      break;
  }
  return nullopt;
}

bool isVector(AstNode *a)
{
  return a && a->t == A_vector;
}

bool isCellRef(AstNode *a)
{
  return a && a->t == A_cellref;
}



optional<string> AstHelp::helpByType(AstType t, AstNode *n)
{
  switch (t) {
    #define defdispatch(T) case A_##T: return help_##T(n); break;
    foreach_ast(defdispatch)
    #undef defdispatch
  }
  return nullopt;
}

optional<string> AstHelp::helpByName(string_view name)
{
  #define defdispatch(NAME) if (name == #NAME) return help(A_##NAME, nullptr);
  foreach_ast(defdispatch)
  #undef defdispatch
  return nullopt;
}

string AstHelp::help(AstType t, AstNode *n)
{
  auto options = helpByType(t, n);
  if (!options) return "?";
  return *options;
}

string AstHelp::help(AstType t)
{
  return help(t, nullptr);
}

string AstHelp::help(AstNode *n)
{
  return help(n->t, n);
}

