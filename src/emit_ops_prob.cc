#include "./emit.h"
#include "./emit_macros.h"

deftype(normalpdf)
{
  return typetag<F>();
}

defemit(normalpdf)
{
  defop3(F, F, F, F, (r = (1.0f / (c * F(sqrt(M_2PI)))) * exp(-0.5f * sqr((a - b) / c))));
  return setInvalidArgs();
}

defusage(normalpdf, R"(
  normalpdf(x ∊ F, mean ∊ F, std ∊ F))");


deftype(normallogpdf)
{
  return typetag<F>();
}

defemit(normallogpdf)
{
  defop3(F, F, F, F, (r = -log(c * F(sqrt(M_2PI))) + -0.5f * sqr((a - b) / c)));
  return setInvalidArgs();
}

defusage(normallogpdf, R"(
  normallogpdf(x ∊ F, mean ∊ F, std ∊ F))");


deftype(normalcdf)
{
  return typetag<F>();
}

defemit(normalcdf)
{
  defop3(F, F, F, F, (r = (0.5f * (1.0f + erf((a - b) / (c * sqrt(2.0f)))))));
  return setInvalidArgs();
}

defusage(normalcdf, R"(
  normalcdf(x ∊ F, mean ∊ F, std ∊ F))");
