#include "./core.h"
#include "./emit.h"

shared_ptr<ofstream> CpuState::logFile;
std::mutex CpuState::logFileMutex;

int compiledBatchSizeForBatchSize(int batchSize)
{
  /*
    We only compile for a few discrete batch sizes, and for whatever batch size we're given we have to use
    code compiled for one at least as large
  */
  if (batchSize <= 1) return 1;
  if (batchSize <= 8) return 8;
  if (batchSize <= 32) return 32;
  // ADD batch size
  throw runtime_error("Bad batch");
}


CpuState::CpuState(CompiledSheet &_compiled, Arena *_pool, int _realBatchSize)
  : sh(_compiled.sh),
    compiled(_compiled),
    pool(_pool),
    realBatchSize(_realBatchSize),
    compiledBatchSize(compiledBatchSizeForBatchSize(realBatchSize))
{
  for (int batchi = 0; batchi < MAX_BATCH_SIZE; batchi++) {
    dts[batchi] = 0.0f;
    curs[batchi] = nullptr;
    prevs[batchi] = nullptr;
    grads[batchi] = nullptr;
    paramss[batchi] = &sh.params;
    paramHashes[batchi] = paramss[batchi]->hash();
    traces[batchi] = nullptr;
  }
}


CpuState::CpuState(CompiledSheet &_compiled, Arena *_pool, CellVals *ref, CellVals *prev)
  : sh(_compiled.sh),
    compiled(_compiled),
    pool(_pool),
    realBatchSize(1),
    compiledBatchSize(compiledBatchSizeForBatchSize(realBatchSize))
{
  for (int batchi = 0; batchi < MAX_BATCH_SIZE; batchi++) {
    dts[batchi] = 0.0f;
    curs[batchi] = nullptr;
    prevs[batchi] = nullptr;
    grads[batchi] = nullptr;
    paramss[batchi] = &sh.params;
    paramHashes[batchi] = paramss[batchi]->hash();
    traces[batchi] = nullptr;
  }
  curs[0] = ref;
  prevs[0] = prev;
}


CpuState::~CpuState()
{
}


void CpuState::setLane(int batchi, Trace *tr, Paramset *params)
{
  assert(batchi >= 0 && batchi < realBatchSize);
  tr->cpuBatchIndex = batchi;
  tr->compiledBatchSize = compiledBatchSize;
  traces[batchi] = tr;
  dts[batchi] = tr->dt;
  paramss[batchi] = params;
  paramHashes[batchi] = params->hash();
  prevs[batchi] = CellVals::mk(&compiled, pool);
}

Pad *CpuState::mkPad()
{
  if (compiledBatchSize == 1) {
    return Pad::mk(pool, compiled.padSize1);
  }
  else if (compiledBatchSize == 8) {
    return Pad::mk(pool, compiled.padSize8);
  }
  else if (compiledBatchSize == 32) {
    return Pad::mk(pool, compiled.padSize32);
  }
  // ADD batch size
  else {
    throw runtime_error("Bad batch");
  }
}


void CpuState::execSheet()
{
  auto pad = mkPad();
  execSheet(*pad);
}

void CpuState::execSheet(Pad &pad)
{
  if (compiledBatchSize == 1) {
    compiled.evalFuncs1.run(*this, pad, BatchDef<1>());
  }
  else if (compiledBatchSize == 8) {
    compiled.evalFuncs8.run(*this, pad, BatchDef<8>());
  }
  else if (compiledBatchSize == 32) {
    compiled.evalFuncs32.run(*this, pad, BatchDef<32>());
  }
  // ADD batch size
}


CellVals *CpuState::valueAtRelTime(F delta, int batchi, CellIndex cell)
{
  if (!traces[batchi]) {
    err = ERR_ref;
    checkFailures[batchi].push_back(cell);
    return compiled.zeroVals.get();
  }
  auto &tr = *traces[batchi];
  long ti = long(timeIndex) + long(floor(delta / dts[batchi]));

  if (ti < 0) {
    return compiled.zeroVals.get(); // this is the intended semantics for references before the beginning of time
  }
  auto ret = tr.getVal(ti);
  if (!ret) {
    if (tr.nVals() > 0) {
      err = ERR_ref;
      checkFailures[batchi].push_back(cell);
    }
    return compiled.zeroVals.get();
  }
  return ret;
}




Pad *Pad::mk(Arena *_pool, U32 _padSize)
{
  auto size = sizeof(Pad) + _padSize;
  auto const align = alignof(Pad);
  size = (size + align - 1) & ~(align - 1);
  auto mem = (*_pool)(align, size);
  return new(mem) Pad(_pool, _padSize);
}


