#pragma once

/*
  A slightly faster replacement for std:vector<std::function<void(Args...)>>
  The 
*/

template<typename ...Args>
struct ExecList {

  struct ExecBase {

    ExecBase() = default;
    virtual ~ExecBase() = default;
    virtual ExecBase *next() = 0;
    virtual ExecBase *invoke(Args &&...) = 0;
    virtual void move_to(void *mem) = 0;

  };

  struct ExecEnd : ExecBase {

    ExecEnd() = default;
    ~ExecEnd() = default;
    ExecBase * next() override
    {
      return nullptr;
    }
    ExecBase * invoke(Args &&...args) override
    {
      return next();
    }
    void move_to(void *mem) override
    {
      new(mem) ExecEnd();
    }
    
  };
  
  template<typename Functor>
  struct ExecOp : ExecBase {

    Functor functor;

    explicit ExecOp(Functor _functor)
      : ExecBase(),
        functor(_functor)
    {
    }
    ~ExecOp() = default;
    ExecOp(ExecOp const &other) = delete;
    ExecOp(ExecOp &&other) = delete;
    ExecOp & operator =(ExecOp const &other) = delete;
    ExecOp & operator =(ExecOp &&other) = delete;

    ExecBase *next() override
    {
      return this + 1;
    }

    ExecBase * invoke(Args &&...args) override
    {
      // L() << "invoke " << (void *)this << " T=" << typeid(*this).name() << " size=" << sizeof(*this);
      functor(std::forward<Args>(args)...);
      return this + 1;
    }

    void move_to(void *mem) override
    {
      new(mem) ExecOp<Functor>(std::move(functor));
    }

  };

  size_t alloc_ = 0;
  size_t size_ = 0;
  char *data_ = nullptr;
  size_t nfuncs_ = 0;

  ExecList(ExecList const &other) = delete;
  ExecList(ExecList &&other) = delete;
  ExecList & operator =(ExecList const &other) = delete;
  ExecList & operator =(ExecList &&other) = delete;

  ExecList()
  {
  }

  ~ExecList()
  {
    ExecBase *ip = (ExecBase *)data_;
    while (ip) {
      auto nextip = ip->next();
      ip->~ExecBase();
      ip = nextip;
    }
    
    free(data_);
    data_ = nullptr;
  }

  void need(size_t s)
  {
    if (s + size_ > alloc_) {
      alloc_ = max(s * 2, max(size_t(4096), alloc_ * 2));
      auto newData = (char *)malloc(alloc_);
      auto newDataPtr = newData;

      ExecBase *ip = (ExecBase *)data_;
      while (ip) {
        auto nextip = ip->next();
        ip->move_to(newDataPtr);
        ip->~ExecBase();
        newDataPtr += (char *)nextip - (char *)ip;
        ip = nextip;
      }
      free(data_);
      data_ = newData;
    }
  }

  template<typename Functor>
  void push_back(Functor functor)
  {
    need(sizeof(ExecOp<Functor>) + sizeof(ExecEnd));
    new(data_ + size_) ExecOp<Functor>(functor);
    size_ += sizeof(ExecOp<Functor>);
    new(data_ + size_) ExecEnd();
    nfuncs_++;
  }

  void run(Args... args)
  {
    ExecBase *ip = (ExecBase *)data_;
    while (ip) {
      ip = ip->invoke(std::forward<Args>(args)...);
    }
  }

  size_t size()
  {
    return nfuncs_;
  }

  size_t size_bytes()
  {
    return size_;
  }

};
