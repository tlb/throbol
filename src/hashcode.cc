#include "common/std_headers.h"
#include "common/str_utils.h"
#include <xxhash.h>

U64 hashcodeU64(string_view src)
{
  return XXH64(src.data(), src.size() * sizeof(char), 0);
}

U64 hashcodeU64(vector<U64> const &src)
{
  return XXH64(src.data(), src.size() * sizeof(U64), 0);
}

string hashcodeStr(string_view src, size_t strLen)
{
  auto result = XXH64(src.data(), src.size(), 0);
  return base58Hash((U8 *)&result, sizeof(result), strLen);
}

