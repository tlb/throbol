#include "./policy.h"
#include "common/asyncjoy.h"
#include "./sheet.h"
#include "./trace.h"

ParticleSearchState::Particle::Particle(size_t dim)
  : tweaks(dim),
    tweakVels(dim),
    evaluated(false),
    evaluation(0.0)
{
}

F score(ParticleSearchSpec const &spec, Trace *t)
{
  return t->getVal_F(spec.sampleIndex, spec.rMetric);
}


ParticleSearchState::Particle::Particle(VectorXf const &_tweaks, VectorXf const &_tweakVels)
  : tweaks(_tweaks),
    tweakVels(_tweakVels),
    evaluated(false),
    evaluation(0.0)

{
  assertlog(tweaks.size() == tweakVels.size(), LOGV(tweaks) << LOGV(tweakVels));
}


ostream & operator <<(ostream &s, ParticleSearchState::Particle const &a)
{
  s << a.tweaks;
  if (a.evaluated) {
    s << "=>" << a.evaluation;
  }
  return s;
}

ParticleSearchState::ParticleSearchState(Sheet &_sh, ParticleSearchSpec const &_spec)
  : GenericSearchState(_sh),
    spec(_spec)
{
}

ParticleSearchState::~ParticleSearchState()
{
}


void ParticleSearchState::start()
{
  active = true;
}

void ParticleSearchState::stop()
{
  active = false;
}

void ParticleSearchState::clear()
{
  particles = nullptr;
}

bool ParticleSearchState::isInBounds(Particle const &p)
{
  for (Index i = 0; i < p.tweaks.size(); i++) {
    if (p.tweaks[i] < -2.0f || p.tweaks[i] > 2.0f) return false;
  }
  return true;
}

static uint32_t seed;

void ParticleSearchState::mkRandomParticle(ParticleSearchState::Particle &newp)
{
  size_t dim = newp.nParams();

  std::mt19937 gen(seed++);
  std::lognormal_distribution<R> varDist(0.0, 1.0);
  R variance = varDist(gen);
  std::normal_distribution<R> paramDist(0.0, variance * double(spec.initialParamSigma));
  std::normal_distribution<R> velDist(0.0, variance * double(spec.initialVelSigma));

  for (size_t i = 0; i < dim; i++) {
    newp.tweaks[i] = clamp(paramDist(gen), -2.0, 2.0);
    switch(spec.alg) {
      case ParticleSearch_swarm:
        newp.tweakVels[i] = velDist(gen);
        break;
      default:
        newp.tweakVels[i] = 0;
    }
  }
}

void ParticleSearchState::writeBackBestParams()
{
  auto ps = particles;
  if (!ps || ps->empty()) return;
  auto &bestParticle = ps->at(0);

  writeBack(bestParticle);
}

void ParticleSearchState::writeBack(Particle &bestParticle)
{
  vector<ParamIndex> pis;
  vector<F> values;

  for (size_t i = 0; i < spec.searchDims.size(); i++) {
    auto &param = spec.searchDims[i];
    auto v = param.getParamValue(sh, bestParticle.tweaks[i]);
    pis.push_back(param.param);
    values.push_back(v);
  }

  sh.setParamValuesUndoable(pis, values);
}


shared_ptr<Trace> ParticleSearchState::mkAltTraceForParticle(Particle &newp)
{
  auto ret = make_shared<Trace>(sh.compiled);
  ret->setupInitial();
  ret->limitMaxIndex(spec.sampleIndex + 1);

  for (size_t i = 0; i < spec.searchDims.size() && i < newp.nParams(); i++) {
    auto &param = spec.searchDims[i];
    auto v = param.getParamValue(sh, newp.tweaks[i]);
    if (isnan(v)) {
      L() << "nan from particle search: " << newp.tweaks;
      return nullptr;
    }
    ret->params.mutValueByParam(param.param) = v;
  }

  return ret;
}

void ParticleSearchState::evalParticles()
{
  auto newps = make_shared<vector<Particle>>();
  auto oldps = particles;
  if (oldps && !oldps->empty()) {
    switch (spec.alg) {
      case ParticleSearch_swarm:
        doSwarm(*newps, *oldps);
        break;
      case ParticleSearch_deRand1Bin:
        doDeRand1Bin(*newps, *oldps);
        break;
      case ParticleSearch_deBest2Bin:
        doDeBest2Bin(*newps, *oldps);
        break;
    }
  }
  while (int(newps->size()) < spec.populationTarget) {
    newps->emplace_back(spec.searchDims.size());
    mkRandomParticle(newps->back());
  }
  iterCount ++;

  threadsRunning ++;
  auto t0 = clock();
  auto aj = make_shared<AsyncJoy>([newps=newps, t0, this, this1=shared_from_this()](string const &err) {
    auto t1 = clock();
    if (0) L() << "Calculated " << newps->size() << " particle evals points in " << repr_clock(t1-t0);

    setNewGeneration(*newps);
    if (0) L() << particles;
    uiPollLive = true;
    threadsRunning --;
  });

  int evalCount = 0;
  for (auto &newp : *newps) {
    if (newp.evaluated) continue;
    evalCount ++;
    auto newTrace = mkAltTraceForParticle(newp);
    if (!newTrace) continue;

    aj->start();

    simulateTraceAsync(std::move(newTrace), [this, aj, &newp](shared_ptr<Trace> newTrace1) {
      newp.evaluation = score(spec, newTrace1.get());
      newp.evaluated = true;
      aj->end();
    });
  }
  if (evalCount == 0) {
    active = false;
  }

  aj->end();
}

void ParticleSearchState::doSwarm(vector<Particle> &newps, vector<Particle> const &oldps)
{
  auto &bestParticle = oldps[0];
  auto evalTol = 0.01f * (abs(bestParticle.evaluation) + 1.0f);
  for (auto &oldp : oldps) {
    // Early on (in the first 100 iterations), add fewer mutated particles to allow
    // more new random particles
    if (int(newps.size()) >= spec.populationTarget - max(0, 100 - iterCount)/10) break;

    size_t nParams = oldp.nParams();

    if (oldp.evaluation < bestParticle.evaluation + evalTol) {
      Particle newp(oldp);
      newp.tweakVels.setZero();
      newps.push_back(newp);
    }
    else {
      Particle newp(nParams);

      VectorXf newVel = spec.velCoeff * oldp.tweakVels +
        spec.accelTowardsBest * (bestParticle.tweaks - oldp.tweaks);
      
      VectorXf newPos = (oldp.tweaks + newVel).array().max(-2.0f).min(2.0f);
      
      newVel = newVel.array().max(-2.0f - newPos.array()).min(2.0f - newPos.array());

      newps.emplace_back(newPos, newVel);
    }
  }
}

void ParticleSearchState::doDeRand1Bin(vector<Particle> &newps, vector<Particle> const &oldps)
{
  /*
     See https://groups.csail.mit.edu/EVO-DesignOpt/pb/uploads/Site/differentialEv.pdf
     This is the rand/1/bin algorithm with clamping
  */
  int ND = int(spec.searchDims.size());
  int NP = int(oldps.size());
  assert(NP >= 4);
  std::mt19937 gen(seed++);
  std::uniform_int_distribution<> chooseElement(0,NP - 1);
  std::uniform_int_distribution<> chooseDimension(0, ND - 1);
  std::uniform_real_distribution<float> chooseProb(0.0f, 1.0f);
  for (int ni = 0; ni < NP; ni++) {
    int ai, bi, ci;
    do {
      ai = chooseElement(gen);
    } while (ai == ni);
    do {
      bi = chooseElement(gen);
    } while (bi == ni || bi == ai);
    do {
      ci = chooseElement(gen);
    } while (ci == ni || ci == ai || ci == bi);

    Particle const &ap = oldps[ai];
    Particle const &bp = oldps[bi];
    Particle const &cp = oldps[ci];
    Particle const &op = oldps[ni];
    Particle newp(ND);
    newp.tweaks.setZero();
    newp.tweakVels.setZero();

    /*
      The magic is right here. There are 4 points: A,B,C,O from which we make
      a new point N to replace O.

      For some dimensions, we take
        N_k = C_k + FAC * (A_k - B_k)
      ie, from point C, move a litle way in the direction A to B.

      For other dimensions, we preserve the old point:
        N_k = O_k

      We always mutate one of the dimensions, and other ones with probability CR.
      
      The original paper doesn't include clamping, but it seems pretty essential.
      We can do it here because the parameter range is established by the ParamRef.
      An alternative might be to add a regularization term to the evaluation function.
    */
    int dimi = chooseDimension(gen); // random starting place
    assert(dimi >= 0 && dimi < ND);

    for (int ki = 0; ki < ND; ki++) {
      if (ki == ND - 1 || chooseProb(gen) < spec.deCR) {
        newp.tweaks[dimi] = clamp(
          cp.tweaks[dimi] + spec.deFAC * (ap.tweaks[dimi] - bp.tweaks[dimi]),
          -2.0f, 2.0f);
      }
      else {
        newp.tweaks[dimi] = op.tweaks[dimi];
      }
      dimi = (dimi + 1) % ND;
    }
    newps.push_back(newp);
  }

}

void ParticleSearchState::doDeBest2Bin(vector<Particle> &newps, vector<Particle> const &oldps)
{
  auto bestParticle = oldps[0];
  // WRITEME
}

static void sortParticles(vector<ParticleSearchState::Particle> &ps)
{
  sort(ps.begin(), ps.end(), [](ParticleSearchState::Particle const &a, ParticleSearchState::Particle const &b) {
    assert(a.evaluated && b.evaluated);
    return a.evaluation < b.evaluation; // lowest evaluation first
  });
}

void ParticleSearchState::setNewGeneration(vector<Particle> const &newps)
{
  switch (spec.alg) {
    case ParticleSearch_swarm:
      {
        auto sorted = make_shared<vector<Particle>>(newps);
        sortParticles(*sorted);
        particles = sorted;
      }
      break;

    case ParticleSearch_deRand1Bin:
    case ParticleSearch_deBest2Bin:
      auto oldps = particles;
      auto newps2 = make_shared<vector<Particle>>();
      for (size_t i = 0; i < newps.size(); i++) {
        auto &newp = newps[i];
        if (oldps && i < oldps->size()) {
          auto &oldp = oldps->at(i);
          assert(oldp.evaluated && newp.evaluated);
          if (newp.evaluation >= oldp.evaluation) {
            newps2->push_back(oldp);
          }
          else {
            newps2->push_back(newp);
          }
        }
        else {
          newps2->push_back(newp);
        }
      }
      sortParticles(*newps2);
      particles = newps2;
      break;
  }
}

void ParticleSearchState::poll()
{
  if (threadsRunning == 0) {
    evalParticles();
  }
}


static map<ParticleSearchSpec, shared_ptr<ParticleSearchState>> stateCacheBySpec;
static deque<shared_ptr<ParticleSearchState>> stateLru;

ParticleSearchState * ParticleSearchState::lookup(Sheet &sh, ParticleSearchSpec const &desiredSpec)
{
  auto &slot = stateCacheBySpec[desiredSpec];
  if (!slot) {
    slot = make_shared<ParticleSearchState>(sh, desiredSpec);

    stateLru.push_front(slot);
    while (stateLru.size() > 50) {
      auto oldest = stateLru.back();
      assert(stateCacheBySpec[oldest->spec] == oldest);
      stateCacheBySpec.erase(oldest->spec);
      stateLru.pop_back();
    }

  }
  return slot.get();
}

