#include "./vbmainloop.h"
#include "../remote/sim_robot_server.h"

#if !defined(EMSCRIPTEN_NOTYET)

struct LiveRobotTicker : enable_shared_from_this<LiveRobotTicker>, AllocTrackingMixin<LiveRobotTicker> {

  string label;
  shared_ptr<SimRobotServer> server;
  shared_ptr<CompiledSheet> compiled;
  shared_ptr<Trace> trace;

  LiveRobotState lrt;

  asio::steady_timer::time_point baseTime;
  asio::steady_timer timer{io_context};

  LiveRobotTicker(shared_ptr<SimRobotServer> _server);
  ~LiveRobotTicker();

  bool startLive();
  void stopLive();

  void asyncTick();

};

#endif
