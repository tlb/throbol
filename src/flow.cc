#include "./core.h"
#include "./ast.h"

struct FlowAnalyzer
{
  FlowAnalyzer(Sheet &_sh, CellIndex _cell, AstNode *_root)
    : sh(_sh),
      cell(_cell),
      root(_root)
  {
    ok = true;
    scan(root);

    sort(deps.begin(), deps.end());
    deps.erase(unique(deps.begin(), deps.end()), deps.end());
    if (0) L() << sh.nameByCell[cell] << " flow: deps=" << deps;
  }

  Sheet &sh;
  CellIndex cell;
  AstNode *root;

  vector<CellIndex> deps;
  bool ok;
  string error;

  void setError(string const &err)
  {
    if (ok) {
      error = err;
      ok = false;
    }
  }

  void scan(AstNode *n)
  {
    if (!n) return;
    switch (n->t) {

      case A_cellref:
      {
        deps.push_back(n->data<CellIndex>());
        break;
      }

      case A_hypobinding:
      {
        scan(n->ch(0));
        for (int i = 1; i < n->nch(); i += 2) {
          auto key = n->ch(i);
          auto value = n->ch(i + 1);
          assert(key->t == A_cellref || key->t == A_name);
          assert(value);
          // Don't scan key
          scan(value);
        }
        break;
      }

      case A_formula:
      {
        for (int i = 0; i < n->nch(); i += 2) {
          scan(n->ch(i));
        }
        break;
      }

      case A_index:
      {
        if (n->ch(0) && n->ch(1) && !n->ch(2)) {
          if (auto dtm = getDtMultiple(n->ch(1))) {
            if (get<0>(*dtm) == 0 && get<1>(*dtm) == 0.0f) {
              scan(n->ch(0));
            }
            else if (get<0>(*dtm) == 0 && get<1>(*dtm) < 0.0f) { // previous, no dep
            }
            else {
              setError("Bad index");
              return;
            }
            break;
          }
        }
        break;
      }

      case A_policyTerrain:
      {
        // args 0, 2, 4 are special
        scan(n->ch(1));
        scan(n->ch(3));
        break;
      }

      case A_paramref:
      {
        break;
      }

      case A_null:
      case A_name:
        return;

      default:
      {
        for (int i = 0; i < n->nch(); i++) {
          scan(n->ch(i));
        }
        break;
      }
    }
  }

};


void Sheet::flowAll()
{
  CellSet redo;
  for (auto cell : needFlowCells) {
    if (astByCell[cell]) {
      FlowAnalyzer flow(*this, cell, astByCell[cell]);
      if (flow.ok) {
        setDeps(cell, flow.deps);
      }
      else {
        redo[cell] = true;
      }
    }
    else {
      redo[cell] = true;
    }
  }
  needFlowCells = std::move(redo);
}
