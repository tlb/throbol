
#pragma once

#include <iostream>

// Allow large draw operations
#define ImDrawIdx unsigned int

#define IMGUI_DISABLE_OBSOLETE_KEYIO 1

extern bool uiPollLive;