#include "./ast.h"
#include "./annotation.h"

CodeAnnotator::CodeAnnotator(Sheet &_sh, CellIndex _cell)
  : sh(_sh),
    cell(_cell),
    root(sh.astByCell[cell])
{
  ok = true;
  emit(root, nullptr);
  sort(annos.begin(), annos.end(), [](Annotation const &a, Annotation const &b) {
    return a.loc.begin < b.loc.begin;
  });
}

ParamIndex CodeAnnotator::getParamIndex(AstNode *n)
{
  if (n && n->t == A_paramref && n->ch(0) && n->ch(0)->t == A_cellref) {
    auto srcCell = n->ch(0)->data<CellIndex>();
    if (!(n->data<ParamIndex>() < sh.paramsByCell[srcCell].size())) return nullParamIndex;
    return sh.paramsByCell[srcCell].at(n->data<ParamIndex>());
  }
  return nullParamIndex;
}

void CodeAnnotator::emit(AstNode *n, AstNode *parent)
{
  if (!n) return;
  switch (n->t) {

    case A_param:
      annos.push_back(Annotation(A_param, n->loc, n->data<ParamIndex>()));
      break;

    case A_dt:
    case A_time:
    case A_constant:
    case A_failCellCount:
    case A_failTime:
      annos.push_back(Annotation(n->t, n->loc));
      break;

    case A_cellref:
      annos.push_back(Annotation(A_cellref, n->loc, n->data<CellIndex>()));
      break;

    case A_paramref:
      annos.push_back(Annotation(A_paramref, n->loc, getParamIndex(n)));
      goto do_children;
    
    default:
    do_children:
      for (int i = 0; i < n->nch(); i++) {
        emit(n->ch(i), n);
      }
      break;

  }
}


ostream & operator <<(ostream &s, DebugTestPoint const &a)
{
  s << a.astt << "@" << a.loc << "=" << (Val const &)a;
  return s;
}
