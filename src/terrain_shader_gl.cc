#include "./core.h"
#include "../geom/g8.h"

/*
  The following shader interpolates code (the colormap* functions)
  from https://github.com/kbinani/colormap-shaders.git
  in the file shaders/glsl/IDL_Blue-Pastel-Red.frag.
  licensed thusly:

  The MIT License (MIT)

  Copyright (c) 2015 kbinani

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

*/

G8Shader<G8Vtx_pncl> terrainShader("terrain", {
  "vtx:", R"(

    @group(0) @binding(0) var<uniform> projViewUniforms: ProjViewUniforms;

    @vertex
    fn main(in: G8Vtx_pncl) -> Frag_spnlcr
    {
      var out: Frag_spnlcr;
      out.color = in.color;
      out.normal = (projViewUniforms.view * vec4<f32>(in.normal, 0.0)).xyz;
      out.rawpos = in.pos;
      out.pos = (projViewUniforms.view * vec4<f32>(in.pos, 1.0)).xyz;
      out.lightpos = (projViewUniforms.view * vec4<f32>(1.0, -1.0, 5.0, 1.0)).xyz;
      out.screenpos = projViewUniforms.proj * projViewUniforms.view * vec4<f32>(in.pos, 1.0) + vec4<f32>(0.0, 0.0, -in.layer, 0.0);
      return out;
    }
  )",
  "frg:", R"(

    fn colormap_red(x: f32) -> f32 {
      if (x < 0.1131206452846527) {
        return (-9.40943766883858E+02 * x - 1.84146720562529E+02) * x + 3.28713709677420E+01;
      } else if (x < 0.5116005837917328) {
        return 0.0;
      } else if (x < 0.5705677568912506) {
        return (-2.22507913165263E+03 * x + 2.76053354341733E+03) * x - 8.29909138655453E+02;
      } else if (x < 0.622047244) {
        return (-1.84774532967032E+04 * x + 2.30647002747253E+04) * x - 7.12389120879120E+03;
      } else if (x < 0.7922459542751312) {
        return ((((1.29456468589020E+06 * x - 4.64095889653844E+06) * x + 6.62951004830418E+06) * x - 4.71587036142377E+06) * x + 1.67048886368434E+06) * x - 2.35682532934682E+05;
      } else {
        return 3.34889230769210E+02 * x - 1.41006123680226E+02;
      }
    }

    fn colormap_green(x: f32) -> f32 {
      if (x < 0.114394336938858) {
        return 0.0;
      } else if (x < 0.4417250454425812) {
        return (9.43393359191585E+02 * x + 1.86774918014536E+02) * x - 3.37113020096108E+01;
      } else if (x < 0.4964917968308496) {
        return 3.11150000000070E+02 * x + 9.54249999999731E+01;
      } else if (x < 0.6259051214039278) {
        return -1.03272635599706E+03 * x + 7.62648586707481E+02;
      } else if (x < 0.8049814403057098) {
        return -2.92799028677160E+02 * x + 2.99524283071235E+02;
      } else {
        return (1.34145201311283E+03 * x - 2.75066701126586E+03) * x + 1.40880802982723E+03;
      }
    }

    fn colormap_blue(x: f32) -> f32 {
      if (x < 0.4424893036638088) {
        return 3.09636968527514E+02 * x + 9.62203074056821E+01;
      } else if (x < 0.5) {
        return -4.59921428571535E+02 * x + 4.36741666666678E+02;
      } else if (x < 0.5691165986930345) {
        return -1.81364912280674E+03 * x + 1.05392982456125E+03;
      } else if (x < 0.6279306709766388) {
        return 1.83776470588197E+02 * x - 8.28382352940910E+01;
      } else {
        return ((-1.14087926835422E+04 * x + 2.47091243363548E+04) * x - 1.80428756181930E+04) * x + 4.44421976986281E+03;
      }
    }

    fn colormap(x: f32) -> vec4<f32> {
      var r = clamp(colormap_red(x) / 255.0, 0.0, 1.0);
      var g = clamp(colormap_green(x) / 255.0, 0.0, 1.0);
      var b = clamp(colormap_blue(x) / 255.0, 0.0, 1.0);
      return vec4<f32>(r, g, b, 1.0);
    }

    @group(0) @binding(0) var<uniform> projViewUniforms: ProjViewUniforms;

    @fragment
    fn main(in: Frag_spnlcr) -> @location(0) vec4<f32> {

      var norm = normalize(in.normal);
      var lightDir = normalize(in.lightpos - in.pos);
      var viewDir = normalize(-in.pos);
      var reflectDir = reflect(-lightDir, norm);

      /*
        Contour lines, 12 per z=1.
        fwidth is GLSL magic that returns the partial derivative of alt WRT X and Y pixel coords.
        So contourDistancePixels is 1 per pixel of (abs(dX) + abs(dY)) distance from the center of the contour line
      */
      var alt = in.rawpos.z * 12.0 - 0.001;
      var fwAlt = fwidth(alt);
      var contourDistancePixels : f32;
      if (fwAlt > 0.0001) {
        contourDistancePixels = abs(fract(alt+0.5) - 0.5) / fwAlt;
      } else {
        contourDistancePixels = 99.0;
      }

      /*
        Add an underwater effect where we cut the red and green parts of the light
        more than the blue.
      */
      var underwater : f32;
      if (alt < 0.0) {
        underwater = 0.1 - 0.05 * alt;
      } else {
        underwater = 0.0;
      }

      var contour = 0.5 * clamp(1.0 - pow(1.0 * contourDistancePixels, 2.0), 0.0, 1.0);

      var lightCol = colormap((alt+12.0)/24.0);
      var glowCol = vec3(contour, contour, contour);
      var specCol = pow(clamp(dot(viewDir, reflectDir) - 3.0*underwater, 0.0, 1.0), 16.0) * vec3(0.85, 0.85, 0.90);

      return vec4<f32>(specCol + glowCol + lightCol.rgb * in.color.rgb, in.color.a);
    }
  )"
});
