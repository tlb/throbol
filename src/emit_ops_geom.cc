#include "./emit.h"
#include "./emit_macros.h"
#include <Eigen/Geometry>

deftype(vector)
{
  auto at = eval(n->ch(0));

  if (at.ist<F>()) {
    bool all_f = true;
    int len = 1;
    for (auto chi = 1; chi < n->nch(); chi++) {
      auto nt = eval(n->ch(chi));
      len++;
      if (!nt.ist<F>()) all_f = false;
    }
    if (all_f) {
      return TypeTag(TK_mat, len, 1);
    }
  }

  if (at.ist<CF>()) {
    bool all_cf = true;
    int len = 1;
    for (auto chi = 1; chi < n->nch(); chi++) {
      auto nt = eval(n->ch(chi));
      len++;
      if (!nt.ist<CF>()) all_cf = false;
    }
    if (all_cf) {
      return TypeTag(TK_cmat, len, 1);
    }
  }

  if (at.ist<mat>() && at.nc == 1) {
    bool all_same = true;
    int len = 1;
    for (auto chi = 1; chi < n->nch(); chi++) {
      auto nt = eval(n->ch(chi));
      len++;
      if (!(nt.ist<mat>() && nt.nr == at.nr)) all_same = false;
    }
    if (all_same) {
      return TypeTag(TK_mat, at.nr, len);
    }
  }
  if (at.ist<cmat>() && at.nc == 1) {
    bool all_same = true;
    int len = 1;
    for (auto chi = 1; chi < n->nch(); chi++) {
      auto nt = eval(n->ch(chi));
      len++;
      if (!(nt.ist<cmat>() && nt.nr == at.nr)) all_same = false;
    }
    if (all_same) {
      return TypeTag(TK_cmat, at.nr, len);
    }
  }
  return setError();
}

defemit(vector)
{
  // Optimization for [const, const]
  if (auto af = getConstF(n->ch(0)), bf = getConstF(n->ch(1)); af && bf && !n->ch(2)) {
    auto rv = allocResult<vec2>();
    she.evalop([rv1=rv, af=*af, bf=*bf](CpuState &cpu, Pad &pad, auto batchdef) {
      auto rv = batchdef.getVal(rv1);
      for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
        pad.wr_vec2(rv, batchi) = {af, bf};
      }
    });
    return rv;
  }
  if (auto af = getConstF(n->ch(0)), bf = getConstF(n->ch(1)), cf = getConstF(n->ch(2)); af && bf && cf && !n->ch(3)) {
    auto rv = allocResult<vec3>();
    she.evalop([rv1=rv, af=*af, bf=*bf, cf=*cf](CpuState &cpu, Pad &pad, auto batchdef) {
      auto rv = batchdef.getVal(rv1);
      for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
        pad.wr_vec3(rv, batchi) = {af, bf, cf};
      }
    });
    return rv;
  }
  if (auto af = getConstF(n->ch(0)), bf = getConstF(n->ch(1)), cf = getConstF(n->ch(2)), df = getConstF(n->ch(3)); af && bf && cf && df && !n->ch(4)) {
    auto rv = allocResult<vec4>();
    she.evalop([rv1=rv, af=*af, bf=*bf, cf=*cf, df=*df](CpuState &cpu, Pad &pad, auto batchdef) {
      auto rv = batchdef.getVal(rv1);
      for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
        pad.wr_vec4(rv, batchi) = {af, bf, cf, df};
      }
    });
    return rv;
  }
  defop2(F, F, vec2, (r = {a, b}));
  defop3(F, F, F, vec3, (r = {a, b, c}));
  defop4(F, F, F, F, vec4, (r = {a, b, c, d}));
  defop2(vec2, vec2, mat2, (r.col(0) = a, r.col(1) = b));
  defop3(vec3, vec3, vec3, mat3, (r.col(0) = a, r.col(1) = b, r.col(2) = c));
  defop4(vec4, vec4, vec4, vec4, mat4, (r.col(0) = a, r.col(1) = b, r.col(2) = c, r.col(3) = d));

  // Any number of floats
  if (matchArgs({typetag<F>()}, true) && all_of(args.begin(), args.end(), [](ValSuite const &it) {
      return it.ist<F>();
    })) {
    auto rt = TypeTag(TK_mat, int(args.size()), 1);
    auto rv = allocResult(rt);
    if (0) L() << "Making " << rt;
    for (int i = 0; i < int(args.size()); i++) {
      she.evalop([rv1=rv, av1=args[i], i](CpuState &cpu, Pad &pad, auto batchdef) {
        auto rv = batchdef.getVal(rv1);
        auto av = batchdef.getVal(av1);
        for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
          pad.up_mat(rv, batchi)(i) = pad.rd_F(av, batchi);
        }
      });
    }
    return rv;
  }

  // Any number of complex numbers
  if (matchArgs({typetag<CF>()}, true) && all_of(args.begin(), args.end(), [](ValSuite const &it) {
      return it.ist<CF>(); 
    })) {
    auto rt = TypeTag(TK_cmat, int(args.size()), 1);
    auto rv = allocResult(rt);
    if (0) L() << "Making " << rt;
    for (int i = 0; i < int(args.size()); i++) {
      she.evalop([rv1=rv, av1=args[i], i](CpuState &cpu, Pad &pad, auto batchdef) {
        auto rv = batchdef.getVal(rv1);
        auto av = batchdef.getVal(av1);
        for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
          pad.up_cmat(rv, batchi)(i) = pad.rd_CF(av, batchi);
        }
      });
    }
    return rv;
  }

  // Any number of real column vectors
  if (matchArgs({typetag<vec>()}, true)) {
    auto colType = args[0].t;
    if (all_of(args.begin(), args.end(), 
        [colType](ValSuite const &it) { return it.t == colType; } )) {

      auto rt = TypeTag(TK_mat, colType.nr, int(args.size()));
      auto rv = allocResult(rt);
      if (0) L() << "Making " << rt;
      for (int i = 0; i < int(args.size()); i++) {
        she.evalop([rv1=rv, av1=args[i], i](CpuState &cpu, Pad &pad, auto batchdef) {
          auto rv = batchdef.getVal(rv1);
          auto av = batchdef.getVal(av1);
          for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
            pad.up_mat(rv, batchi).col(i) = pad.rd_mat(av, batchi);
          }
        });
      }
      return rv;
    }
  }

  // Any number of complex column vectors
  if (matchArgs({typetag<cvec>()}, true)) {
    auto colType = args[0].t;
    if (all_of(args.begin(), args.end(),
        [colType](ValSuite const &it) { return it.t == colType; } )) {

      auto rt = TypeTag(TK_cmat, colType.nr, int(args.size()));
      auto rv = allocResult(rt);
      if (0) L() << "Making " << rt;
      for (int i = 0; i < int(args.size()); i++) {
        she.evalop([rv1=rv, av1=args[i], i](CpuState &cpu, Pad &pad, auto batchdef) {
          auto rv = batchdef.getVal(rv1);
          auto av = batchdef.getVal(av1);
          for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
            pad.up_cmat(rv, batchi).col(i) = pad.rd_cmat(av, batchi);
          }
        });
      }
      return rv;
    }
  }

  return setInvalidArgs();
}

defusage(vector, R"(
  [F..]
  [vec...])");


deftype(rotMat2)
{
  return typetag<mat2>();
}

defemit(rotMat2)
{
  defop1(F, mat2, r = Eigen::Rotation2D<F>(a).toRotationMatrix());
  return setInvalidArgs();
}

defusage(rotMat2, R"(
  mat2.rot(angle ∊ F))");


deftype(scaleMat2)
{
  return typetag<mat2>();
}

defemit(scaleMat2)
{
  defop1(F, mat2, (r = mat2{
    {a, 0},
    {0, a}}));
  return setInvalidArgs();
}

defusage(scaleMat2, R"(
  mat2.scale(scale ∊ F))");


deftype(rotMat3X)
{
  return typetag<mat3>();
}

defemit(rotMat3X)
{
  defop1g(F, mat3, (r = mat3{
      {1, 0, 0},
      {0, cos(a), -sin(a)},
      {0, sin(a), cos(a)}}),
    (ag += rg(1, 1) * -sin(a) + rg(1, 2) * -cos(a) + rg(2, 1)*cos(a) + rg(2 ,2) * - sin(a)));
  return setInvalidArgs();
}

defusage(rotMat3X, R"(
  mat3.rotx(angle ∊ F))");


deftype(rotMat3Y)
{
  return typetag<mat3>();
}

defemit(rotMat3Y)
{
  defop1g(F, mat3, (r = mat3{
      {cos(a), 0, sin(a)},
      {0, 1, 0},
      {-sin(a), 0, cos(a)}}),
    (ag += rg(0, 0) * -sin(a) + rg(0, 2) * cos(a) + rg(2, 0) * -cos(a) + rg(2, 2) * -sin(a)));
  return setInvalidArgs();
}

defusage(rotMat3Y, R"(
  mat3.roty(angle ∊ F))");


deftype(rotMat3Z)
{
  return typetag<mat3>();
}

defemit(rotMat3Z)
{
  defop1g(F, mat3, (r = mat3{
      {cos(a), -sin(a), 0},
      {sin(a), cos(a), 0},
      {0, 0, 1}}),
    (ag += rg(0, 0) * -sin(a) + rg(0, 1) * -cos(a) + rg(1, 0) * cos(a) + rg(1, 1) * -sin(a)));
  return setInvalidArgs();
}

defusage(rotMat3Z, R"(
  mat3.rotz(angle ∊ F))");


deftype(rotMat4X)
{
  return typetag<mat4>();
}

defemit(rotMat4X)
{
  defop1g(F, mat4, (r = mat4{
      {1, 0, 0, 0},
      {0, cos(a), -sin(a), 0},
      {0, sin(a), cos(a), 0},
      {0, 0, 0, 1}}),
    (ag += rg(1, 1) * -sin(a) + rg(1, 2) * -cos(a) + rg(2, 1)*cos(a) + rg(2 ,2) * - sin(a)));
  return setInvalidArgs();
}

defusage(rotMat4X, R"(
  mat4.rotx(angle ∊ F))");



deftype(quatMat4)
{
  return typetag<mat4>();
}

defemit(quatMat4)
{
  defop1(vec4, mat4, (r = mat4::Identity(), r.template topLeftCorner<3,3>() = Eigen::Quaternionf(a[0], a[1], a[2], a[3]).normalized().toRotationMatrix()));
  return setInvalidArgs();
}

defusage(quatMat4, R"(
  mat4.quat([x,y,z,w]))");


deftype(rotMat4Y)
{
  return typetag<mat4>();
}

defemit(rotMat4Y)
{
  defop1g(F, mat4, (r = mat4{
      {cos(a), 0, sin(a), 0},
      {0, 1, 0, 0},
      {-sin(a), 0, cos(a), 0},
      {0, 0, 0, 1}}),
    (ag += rg(0, 0) * -sin(a) + rg(0, 2) * cos(a) + rg(2, 0) * -cos(a) + rg(2, 2) * -sin(a)));
  return setInvalidArgs();
}

defusage(rotMat4Y, R"(
  mat4.roty(angle ∊ F))");


deftype(rotMat4Z)
{
  return typetag<mat4>();
}

defemit(rotMat4Z)
{
  defop1g(F, mat4, (r = mat4{
      {cos(a), -sin(a), 0, 0},
      {sin(a), cos(a), 0, 0},
      {0, 0, 1, 0},
      {0, 0, 0, 1}}),
    (ag += rg(0, 0) * -sin(a) + rg(0, 1) * -cos(a) + rg(1, 0) * cos(a) + rg(1, 1) * -sin(a)));
  return setInvalidArgs();
}

defusage(rotMat4Z, R"(
  mat4.rotz(angle ∊ F))");


deftype(rotMat4Xderiv)
{
  return typetag<mat4>();
}

defemit(rotMat4Xderiv)
{
  defop1(F, mat4, (r = mat4{
    {0, 0, 0, 0},
    {0, -sin(a), -cos(a), 0},
    {0, cos(a), -sin(a), 0},
    {0, 0, 0, 0}}));
  return setInvalidArgs();
}

defusage(rotMat4Xderiv, R"(
  mat4.rotxderiv(angle ∊ F))");


deftype(rotMat4Yderiv)
{
  return typetag<mat4>();
}

defemit(rotMat4Yderiv)
{
  defop1(F, mat4, (r = mat4{
    {-sin(a), 0, cos(a), 0},
    {0, 0, 0, 0},
    {-cos(a), 0, -sin(a), 0},
    {0, 0, 0, 0}}));
  return setInvalidArgs();
}

defusage(rotMat4Yderiv, R"(
  mat4.rotyderiv(angle ∊ F))");


deftype(rotMat4Zderiv)
{
  return typetag<mat4>();
}

defemit(rotMat4Zderiv)
{
  defop1(F, mat4, (r = mat4{
    {-sin(a), -cos(a), 0, 0},
    {cos(a), -sin(a), 0, 0},
    {0, 0, 0, 0},
    {0, 0, 0, 0}}));
  return setInvalidArgs();
}

defusage(rotMat4Zderiv, R"(
  mat4.rotzderiv(angle ∊ F))");


deftype(scaleMat3)
{
  return typetag<mat3>();
}

defemit(scaleMat3)
{
  defop3g(F, F, F, mat3, (r = mat3{
      {a, 0, 0},
      {0, b, 0},
      {0, 0, c}}),
    (ag += rg(0, 0), bg += rg(1, 1), cg += rg(2, 2)));
  defop1g(F, mat3, (r = mat3{
      {a, 0, 0},
      {0, a, 0},
      {0, 0, a}}),
    (ag += rg(0, 0) + rg(1, 1) + rg(2, 2)));
  return setInvalidArgs();
}

defusage(scaleMat3, R"(
  mat3.scale(scale ∊ F)
  mat3.scale(xScale ∊ F, yScale ∊ F, zScale ∊ F))");


deftype(translateMat4)
{
  return typetag<mat4>();
}

defemit(translateMat4)
{
  defop3g(F, F, F, mat4, (r = mat4{
      {1, 0, 0, a},
      {0, 1, 0, b},
      {0, 0, 1, c},
      {0, 0, 0, 1}}),
    (ag += rg(0, 3), bg += rg(1, 3), cg += rg(2, 3)));
  defop1g(vec3, mat4, (r = mat4{
      {1, 0, 0, a[0]},
      {0, 1, 0, a[1]},
      {0, 0, 1, a[2]},
      {0, 0, 0, 1}}),
    (ag += vec3(rg(0, 3), rg(1, 3), rg(2, 3))));
  defop1g(vec4, mat4, (r = mat4{
      {1, 0, 0, a[0]},
      {0, 1, 0, a[1]},
      {0, 0, 1, a[2]},
      {0, 0, 0, 1}}),
    (ag += vec4(rg(0, 3), rg(1, 3), rg(2, 3), 0)));
  return setInvalidArgs();
}

defusage(translateMat4, R"(
  mat4.translate(x ∊ F, y ∊ F, z ∊ F)
  mat4.translate(vec3)
  mat4.translate(vec4))");


deftype(scaleMat4)
{
  return typetag<mat4>();
}

defemit(scaleMat4)
{
  defop3g(F, F, F, mat4, (r = mat4{
      {a, 0, 0, 0},
      {0, b, 0, 0},
      {0, 0, c, 0},
      {0, 0, 0, 1}}),
    (ag += rg(0, 0), bg += rg(1, 1), cg += rg(2, 2)));
  defop1g(F, mat4, (r = mat4{
      {a, 0, 0, 0},
      {0, a, 0, 0},
      {0, 0, a, 0},
      {0, 0, 0, 1}}),
    (ag += rg(0, 0) + rg(1, 1) + rg(2, 2)));
  return setInvalidArgs();
}

defusage(scaleMat4, R"(
  mat4.scale(x ∊ F, y ∊ F, z ∊ F)
  mat4.scale(F))");


deftype(dot)
{
  return typetag<F>();
}

defemit(dot)
{
  defop2g(vec2, vec2, F, r = a.dot(b), (ag += rg * b, bg += rg * a));
  defop2g(vec3, vec3, F, r = a.dot(b), (ag += rg * b, bg += rg * a));
  defop2g(vec4, vec4, F, r = a.dot(b), (ag += rg * b, bg += rg * a));
  defop2g(vec, vec, F, r = a.dot(b), (ag += rg * b, bg += rg * a));
  defop2g(cvec, cvec, CF, r = a.dot(b), (ag += rg * b, bg += rg * a));
  return setInvalidArgs();
}

defusage(dot, R"(
  dot(a ∊ vec, b ∊ vec))");

