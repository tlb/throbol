#include "./policy.h"
#include "./sheet.h"
#include "./trace.h"
#include "cppoptlib/function.h"
#include "cppoptlib/solver/bfgs.h"
#include "cppoptlib/solver/lbfgsb.h"
#include "cppoptlib/solver/conjugated_gradient_descent.h"
#include "cppoptlib/solver/gradient_descent.h"
#include "cppoptlib/solver/lbfgs.h"
#include "cppoptlib/solver/newton_descent.h"

F score(MinpackSearchSpec const &spec, Trace *t)
{
  return t->getVal_F(spec.sampleIndex, spec.rMetric);
}

MinpackSearchState::MinpackSearchState(Sheet &_sh, MinpackSearchSpec const &_spec)
  : GenericSearchState(_sh),
    spec(_spec),
    bestx(VectorXf::Zero(spec.searchDims.size()))
{
}

MinpackSearchState::~MinpackSearchState()
{
}

using FunctionX = cppoptlib::function::Function<float>;

struct PolicyRewardFunc : FunctionX {

  Sheet &sh;
  MinpackSearchSpec spec;
  Index nDim = 0;
  int verbose = 0;

  PolicyRewardFunc(Sheet &_sh, MinpackSearchSpec const &_spec)
    : sh(_sh),
      spec(_spec),
      nDim(spec.searchDims.size())
  {
  }

  shared_ptr<Trace> mkAltTrace(vector_t const &x) const
  {
    auto ret = make_shared<Trace>(sh.compiled);
    ret->setupInitial();
    ret->limitMaxIndex(spec.sampleIndex + 1);
    assertlog(x.size() == nDim, LOGV(x) << LOGV(nDim));

    for (size_t i = 0; i < spec.searchDims.size(); i++) {
      auto &dim = spec.searchDims[i];
      auto v = dim.getParamValue(sh, x[i]);
      if (isnan(v)) {
        L() << "nan from minpack: " << x;
        return nullptr;
      }
      ret->params.mutValueByParam(dim.param) = v;
    }

    return ret;
  }

  scalar_t operator()(vector_t const &x) const override
  {
    assertlog(nDim == x.size(), LOGV(nDim) << LOGV(x));
    auto valueTrace = mkAltTrace(x);
    scalar_t valueMetric = 0.0;
    VectorXf diffMetrics = VectorXf::Zero(nDim);

    atomic<int> outstanding = 1;
    simulateTraceAsync(std::move(valueTrace), [this, &valueMetric, &outstanding](shared_ptr<Trace> newTrace1) {
      valueMetric = score(spec, newTrace1.get());
      outstanding--;
    });

    simulatePollWait([&outstanding]() { return outstanding == 0; });

    if (verbose >= 2) L() << " f(" << x << ") = " << valueMetric;
    return valueMetric + spec.regularization * x.squaredNorm();
  }

  virtual void Gradient(vector_t const &x, vector_t *gradient) const override
  {
    assertlog(nDim == x.size(), LOGV(nDim) << LOGV(x));
    assertlog(nDim == gradient->size(), LOGV(nDim) << LOGV(*gradient));

    scalar_t eps = 0.001; // WRITEME: maybe be more clever here

    VectorXf posVals = VectorXf::Zero(nDim);
    VectorXf negVals = VectorXf::Zero(nDim);

    atomic<int> outstanding = 2 * nDim;

    for (Index dimi = 0; dimi < nDim; dimi++) {
      auto xPos = x;
      xPos[dimi] += eps/2;
      auto posTrace = mkAltTrace(xPos);

      auto xNeg = x;
      xNeg[dimi] -= eps/2;
      auto negTrace = mkAltTrace(xNeg);

      simulateTraceAsync(std::move(posTrace), [this, &posVals, &outstanding, dimi](shared_ptr<Trace> newTrace1) {
        posVals[dimi] = score(spec, newTrace1.get());
        outstanding--;
      });

      simulateTraceAsync(std::move(negTrace), [this, &negVals, &outstanding, dimi](shared_ptr<Trace> newTrace1) {
        negVals[dimi] = score(spec, newTrace1.get());
        outstanding--;
      });
    }

    simulatePollWait([&outstanding]() { return outstanding == 0; });

    *gradient = (posVals - negVals) / eps + (spec.regularization * 2.0f) * x;

    if (verbose >= 2) L() << " f(" << x << ") grad = " << *gradient <<
      " from " << posVals << "-" << negVals;
  }

};


using vector_t = PolicyRewardFunc::vector_t;

template<typename solver_t>
struct MinpackSearchDeetsImpl : MinpackSearchDeets {
  
  using solver_base_t = cppoptlib::solver::Solver<PolicyRewardFunc>;

  PolicyRewardFunc prf;
  solver_t solver;
  solver_base_t::state_t solver_state;
  PolicyRewardFunc::state_t initial_func_state;
  PolicyRewardFunc::state_t func_state;
  solver_base_t::state_t stopping_state;

  template <typename ...Args>
  MinpackSearchDeetsImpl(
    Sheet &_sh,
    MinpackSearchSpec const &_spec,
    Args && ...args)
    : prf(_sh, _spec),
      solver(std::forward<Args>(args)...),
      initial_func_state(prf.Eval(vector_t::Zero(_spec.searchDims.size()), 1)),
      func_state(initial_func_state),
      stopping_state(cppoptlib::solver::DefaultStoppingSolverState<PolicyRewardFunc::scalar_t>())
  {
    solver.InitializeSolver(initial_func_state);

    stopping_state.x_delta = 1e-9;
    stopping_state.x_delta_violations = 100;
    stopping_state.num_iterations = 1000;
  }

  ~MinpackSearchDeetsImpl()
  {
  }

  virtual void step(MinpackSearchState *owner) override
  {
    if (0) L() << "minpack step " << func_state.value << " " << func_state.x;

    owner->xpath.emplace_back(func_state.x.template cast<float>(), func_state.value);
    uiPollLive = true;

    PolicyRewardFunc::state_t previous_func_state(func_state);

    func_state = solver.OptimizationStep(
      prf,
      previous_func_state,
      solver_state);

    // We can't use stopping_state_ in the solver, because it's protected.
    // So we keep our own. Encapsulation FTW!
    solver_state.Update(previous_func_state, func_state, stopping_state);

    if (func_state.value < owner->bestval) {
      owner->bestx = func_state.x.cast<float>();
      owner->bestval = func_state.value;
    }

    if (solver_state.status != cppoptlib::solver::Status::Continue) {
      owner->algState = Minpack_done;
      owner->active = false;
      owner->message = "Stopped: " + repr(solver_state.status);
    }
  }

};


void MinpackSearchState::start()
{
  active = true;
  message = "";
}

void MinpackSearchState::stop()
{
  active = false;
}

void MinpackSearchState::clear()
{
}

void MinpackSearchState::poll()
{

  switch (algState) {

    case Minpack_initial:
      if (active) {

        switch (spec.alg) {

          case Minpack_NewtonDescent:
            deets = make_shared<MinpackSearchDeetsImpl<cppoptlib::solver::NewtonDescent<PolicyRewardFunc>>>(
              sh, spec);
          break;

          case Minpack_GradientDescent:
            deets = make_shared<MinpackSearchDeetsImpl<cppoptlib::solver::GradientDescent<PolicyRewardFunc>>>(
              sh, spec);
          break;

          case Minpack_ConjugatedGradientDescent:
            deets = make_shared<MinpackSearchDeetsImpl<cppoptlib::solver::ConjugatedGradientDescent<PolicyRewardFunc>>>(
              sh, spec);
          break;

          case Minpack_Bfgs:
            deets = make_shared<MinpackSearchDeetsImpl<cppoptlib::solver::Bfgs<PolicyRewardFunc>>>(
              sh, spec);
          break;

          case Minpack_Lbfgs:
            deets = make_shared<MinpackSearchDeetsImpl<cppoptlib::solver::Lbfgs<PolicyRewardFunc>>>(
              sh, spec);
          break;

          case Minpack_Lbfgsb:
            deets = make_shared<MinpackSearchDeetsImpl<cppoptlib::solver::Lbfgsb<PolicyRewardFunc>>>(
              sh, spec,
              vector_t::Constant(spec.searchDims.size(), -2),
              vector_t::Constant(spec.searchDims.size(), +2));
          break;

          default:
            break;
        }
        algState = Minpack_stepping;
        uiPollLive = true;

      }
      break;

    case Minpack_stepping:
      if (active && deets) {
        deets->step(this);
      }
      else {
        deets = nullptr;
        algState = Minpack_initial;
      }
      break;

    case Minpack_done:
      if (active) {
        algState = Minpack_initial;
      }
      break;
  }
}



void MinpackSearchState::writeBackBestParams()
{
  writeBack(bestx);
}

void MinpackSearchState::writeBack(VectorXf const &x)
{
  vector<ParamIndex> pis;
  vector<F> values;

  Index nDim = spec.searchDims.size();
  assertlog(x.size() == nDim, LOGV(x) << LOGV(nDim));
  for (Index i = 0; i < nDim; i++) {
    auto &dim = spec.searchDims[i];
    auto v = dim.getParamValue(sh, x[i]);
    pis.push_back(dim.param);
    values.push_back(v);
  }

  sh.setParamValuesUndoable(pis, values);
}


// Not as performant as a unordered_map, but saves having to write a hash function
static map<MinpackSearchSpec, shared_ptr<MinpackSearchState>> stateCacheBySpec;
static deque<shared_ptr<MinpackSearchState>> stateLru;

MinpackSearchState * MinpackSearchState::lookup(Sheet &sh, MinpackSearchSpec const &desiredSpec)
{
  auto &slot = stateCacheBySpec[desiredSpec];
  if (!slot) {
    slot = make_shared<MinpackSearchState>(sh, desiredSpec);
    stateLru.push_front(slot);
    while (stateLru.size() > 50) {
      auto oldest = stateLru.back();
      assert(stateCacheBySpec[oldest->spec] == oldest);
      stateCacheBySpec.erase(oldest->spec);
      stateLru.pop_back();
    }
  }
  return slot.get();
}

