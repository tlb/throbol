#include "./emit.h"
#include "./emit_macros.h"


deftype(step)
{
  return eval(n->ch(0));
}

defemit(step)
{
  defop2(F, F, F, r = (a > b) ? 0.0f : 1.0f);
  //defop2(op_step, vec2, vec2, vec2);
  //defop2(op_step, vec4, vec4, vec4);
  return setInvalidArgs();
}

defusage(step, R"(
  step(threshold ∊ F, value ∊ F) -- 1 when value>=threshold, 0 otherwise)");


deftype(smoothstep)
{
  return eval(n->ch(0));
}

defemit(smoothstep)
{
  defop3(F, F, F, F, ({ auto t = clamp01((c - a) / (b - a)); r = t * t * (3.0f - 2.0f * t); }));
  //defop3(op_smoothstep, vec2, vec2, vec2, vec2);
  //defop3(op_smoothstep, vec4, vec4, vec4, vec4);
  return setInvalidArgs();
}

defusage(smoothstep, R"(
  smoothstep(threshold0 ∊ F, threshold1 ∊ F, value ∊ F) -- 1 when value >= threshold1, 0 when value <= threshold0, cubic interp)");


deftype(lerp)
{
  return eval(n->ch(0));
}

defemit(lerp)
{
  defop3(F, F, F, F, r = lerp(a, b, c));
  defop3(CF, CF, F, CF, r = lerp(a, b, c));

  defop3(vec2, vec2, F, vec2, r = lerp(a, b, c));
  defop3(vec3, vec3, F, vec3, r = lerp(a, b, c));
  defop3(vec4, vec4, F, vec4, r = lerp(a, b, c));
  defop3(mat2, mat2, F, mat2, r = lerp(a, b, c));
  defop3(mat3, mat3, F, mat3, r = lerp(a, b, c));
  defop3(mat4, mat4, F, mat4, r = lerp(a, b, c));
  
  // FIXME defop3(mat, mat, F, mat, if (cpu.checktrue(a.rows() ==  b.rows() && a.cols() == b.cols())) r = lerp(a, b, c));
  return setInvalidArgs();
}

defusage(lerp, R"(
  lerp(v0 ∊ F|CF|vec|mat, v1 ∊ same, t ∊ F) -- v0 + t * (v1 - v0))");


deftype(easein)
{
  return typetag<F>();
}

defemit(easein)
{
  defop1(F, F, r = easeInRaisedCos(a));
  return setInvalidArgs();
}

defusage(easein, R"(
  easein(x ∊ F) -- 0 when x <= 0, 1 when x >= 1, raised cos interp)");


deftype(easeout)
{
  return typetag<F>();
}

defemit(easeout)
{
  defop1(F, F, r = 1.0f - easeInRaisedCos(a));
  return setInvalidArgs();
}

defusage(easeout, R"(
  easeout(x ∊ F) -- 1 when t <= 0, 0 when t >= 1, raised cos interp)");

