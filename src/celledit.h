#pragma once
#include "../geom/geom.h"
#include "../geom/vbtext.h"
#include "./sheet_ui.h"

using CursorCoord = U32;

struct CursorPos {

  CursorCoord row, col;

  CursorPos() : row(0), col(0) {}
  CursorPos(U32 _row, U32 _col) : row(_row), col(_col) {}
};

// returns a vector mapping char offset to (row, col)
// Includes a fake terminating null
vector<CursorPos> rowColMap(string_view s);

U32 findPosAbove(vector<CursorPos> const &rcm, TextIndex pos);
U32 findPosBelow(vector<CursorPos> const &rcm, TextIndex pos);
U32 findPosEol(vector<CursorPos> const &rcm, TextIndex pos);
U32 findPosBol(vector<CursorPos> const &rcm, TextIndex pos);

struct TextDecoration {

  vec4 color;

  TextDecoration(vec4 const &_color) : color(_color) {}

};

struct CellEditableContents {

  SheetUi &ui;
  Sheet &sh;
  CellIndex cell;
  U32 &p;
  U32 nameStart, nameEnd, formulaStart, formulaEnd;
  string name, formula;
  string text;
  vector<TextDecoration> decorations;
  bool nameChanged = false, formulaChanged = false;

  CellEditableContents(SheetUi &_ui, Sheet &_sh, CellIndex _cell);

  void input(char32_t c);

  void applyAnnotations(vector<Annotation> const &annos);

  void setText();
  void left();
  void right();
  void up();
  void down();
  void eol();
  void bol();
  void kill();
  void backspace();
  void del();
  void insert(U32 c);
  void insert(string const &s);
  void yank();

  bool cursorInName();

};
