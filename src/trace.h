#pragma once
#include "./defs.h"
#include "./params.h"

struct LiveRobotState;

struct Trace : AllocTrackingMixin<Trace> {

  Sheet &sh;
  shared_ptr<CompiledSheet> compiled;
  int uniqueId = 0;
  int maxIndex = 999999;
  F dt = 0.0f;
  F failTime = 0.0f;
  bool doGrads = false;
  bool savePads = false;
  bool simFailure = false;

  // If executed in a batch, the batch number. We need this
  // to extract intermediate values from a Pad.
  int cpuBatchIndex = -1;
  int compiledBatchSize = -1; // useful for getting the right DebugTestPoint
  R startTime = 0.0;
  U64 randomSeed = 0;
  vector<CellVals *> vals;
  vector<CellVals *> grads;
  vector<Pad *> pads;
  CellSet checkFailuresByCell;
  shared_ptr<Arena> tracelife;

  Paramset params;
  shared_ptr<ReplayBuffer> replayBuffer;
  shared_ptr<void> simState;

  Trace(
    shared_ptr<CompiledSheet> _compiled,
    bool _doGrads = false,
    bool _savePads = false);
  ~Trace();
  void startLive();
  void stepLive(LiveRobotState &lrt);
  void mkReplayBuffer();
  void setupInitial();

  void pushTraceLife(shared_ptr<Arena> _tracelife);

  int findTime(F time);

  F getTimeForIndex(int i)
  {
    if (i < 0) return 0.0f;
    return float(i) * dt;
  }

  void limitMaxIndex(int a)
  {
    maxIndex = min(maxIndex, a);
  }

  int nVals()
  {
    return int(vals.size());
  }
  
  CellVals *getVal(int i);
  Pad *getPad(int i);
  F getVal_F(int sampleIndex, BoxedRef ref);

  void writeCsv(ostream &s);

  shared_ptr<ReplayBuffer> extractReplayBuffer();

};

void simulateGroup(vector<shared_ptr<Trace>> traces);

void simulateTraceAsync(shared_ptr<Trace> &&trace, std::function<void (shared_ptr<Trace>)> &&onDone, int priority=1);
void simulateTracesAsync(vector<shared_ptr<Trace>> &&traces, std::function<void (shared_ptr<Trace>)> &&onDone, int priority=1);

size_t simulateTraceAsyncQueueSize();
void simulateStartup();
void simulateShutdown();
void simulatePollWait(function<bool()> cond);
