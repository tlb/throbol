#pragma once
#include "./defs.h"
#include "./types.h"

struct BoxedPtr {

  TypeTag t;
  void *p;

  BoxedPtr(TypeTag _t, void *_p)
   :t(_t),
    p(_p)
  {
  }

  template<typename T>
  BoxedPtr(T *_p)
   :t(typetag<T>()),
    p(_p)
  {
  }

  template<typename T> bool ist() const
  {
    return t.ist<T>();
  }

  template<typename T> T &get() const
  {
    assert(ist<T>());
    return *(T *)p;
  }

  mat get_mat() const
  {
    return mat((F *)p, t.nr, t.nc);
  }

  cmat get_cmat() const
  {
    return cmat((CF *)p, t.nr, t.nc);
  }

  void assign(BoxedPtr const &rhs, int compiledBatchSize);
};



struct BoxedValue {

  TypeTag t;
  void *p = nullptr;

  BoxedValue();
  BoxedValue(BoxedPtr ptr);
  ~BoxedValue();

  BoxedValue(BoxedValue const &other);
  BoxedValue(BoxedValue &&other) noexcept;
  BoxedValue & operator = (BoxedValue const &other);

  template<typename T> bool ist() const
  {
    return t.ist<T>();
  }

  template<typename T> T const &get() const
  {
    return *(T *)p;
  }

  string_view get_string_view() const
  {
    return *(string *)p;
  }

  mat get_mat() const
  {
    return mat((F *)p, t.nr, t.nc);
  }

  cmat get_cmat() const
  {
    return cmat((CF *)p, t.nr, t.nc);
  }
  
  BoxedPtr asPtr() const
  {
    return BoxedPtr(t, p);
  }

  string asString() const
  {
    return repr(asPtr());
  }

  void setp(BoxedPtr const &ptr);
  void setp(BoxedValue const &val);
  void freep();
};


ostream & operator <<(ostream &s, WithSheet<BoxedPtr> const &sa);
ostream & operator <<(ostream &s, BoxedPtr const &a);

struct BoxedRef {

  TypeTag t;
  CellIndex cell;
  ValueOffset ofs;

  BoxedRef()
    : t(T_error),
      cell(nullCellIndex),
      ofs(-1)
  {
  }

  BoxedRef(TypeTag _t, CellIndex _cell, ValueOffset _v)
    : t(_t),
      cell(_cell),
      ofs(_v)
  {
  }

  bool valid() const
  {
    return cell != nullCellIndex && ofs != nullValueOffset;
  }

  BoxedRef getRefForCompiledBatchSize(int compiledBatchSize) const
  {
    return *this; // unlike a ValSuite, this is independent of batch size
  }

  template<typename T> bool ist() const
  {
    return t.match(typetag<T>());
  }
  template<typename T> typename up_t<T>::t get(CellVals *ref) const;
  template<typename T> typename rd_t<T>::t get(CellVals const *ref) const;

  BoxedPtr getPtr(CellVals const *ref) const;
};

ostream & operator <<(ostream &s, BoxedRef const &a);
ostream & operator <<(ostream &s, WithSheet<BoxedRef> const &sha);
bool operator < (BoxedRef const &a, BoxedRef const &b);
bool operator == (BoxedRef const &a, BoxedRef const &b);

string repr(BoxedRef const &a, CellVals const *ref);


/*
  An Assoc is the key-value pair for internal use by the interpreter.
  No destructor, so it should be allocated inside an Arena.
*/
struct Assoc {

  Assocp next;
  string_view key;
  BoxedPtr val;

  Assoc(Assocp _next, string_view _key, BoxedPtr _val)
    : next(_next), key(_key), val(_val)
  {
  }
};

Assocp merge(Arena *pool, Assocp a, Assocp b);

// Lookups
Assocp assoc(Assocp alist, string_view name);
optional<string_view> assoc_string_view(Assocp alist, string_view name);
string_view assoc_string_view(Assocp alist, string_view name, string_view dflt);
optional<F> assoc_F(Assocp alist, string_view name);
F assoc_F(Assocp alist, string_view name, F dflt);
Assocp assoc_assoc(Assocp alist, string_view name);
optional<bool> assoc_bool(Assocp alist, string_view name);
bool assoc_bool(Assocp alist, string_view name, bool dflt);
optional<vec2> assoc_vec2(Assocp alist, string_view name);
optional<vec2> assoc_range(Assocp alist, string_view name);
optional<vec4> assoc_vec4(Assocp alist, string_view name);
optional<mat4> assoc_mat4(Assocp alist, string_view name);


/*
  An AssocValue is the externally visible key-value pair,
  using shared_ptr &c.
*/
struct AssocValue {

  shared_ptr<AssocValue> next;
  string key;
  BoxedValue val;
  
  AssocValue(shared_ptr<AssocValue> _next, string_view _key, BoxedPtr _val)
    : next(_next), key(_key), val(_val)
  {
  }
  AssocValue()
    : next(nullptr), key(""), val()
  {
  }

};

shared_ptr<AssocValue> toShared(Assocp it);

AssocValue *assoc(shared_ptr<AssocValue> alist, string_view name);
optional<string_view> assoc_string_view(shared_ptr<AssocValue> alist, string_view name);
string_view assoc_string_view(shared_ptr<AssocValue> alist, string_view name, string_view dflt);
optional<F> assoc_F(shared_ptr<AssocValue> alist, string_view name);
F assoc_F(shared_ptr<AssocValue> alist, string_view name, F dflt);
shared_ptr<AssocValue> assoc_assoc(shared_ptr<AssocValue> alist, string_view name);
optional<bool> assoc_bool(shared_ptr<AssocValue> alist, string_view name);
bool assoc_bool(shared_ptr<AssocValue> alist, string_view name, bool dflt);
optional<vec2> assoc_vec2(shared_ptr<AssocValue> alist, string_view name);
optional<vec2> assoc_range(shared_ptr<AssocValue> alist, string_view name);
optional<vec4> assoc_vec4(shared_ptr<AssocValue> alist, string_view name);
optional<mat4> assoc_mat4(shared_ptr<AssocValue> alist, string_view name);

ostream & operator <<(ostream &s, WithSheet<Assocp> const &sha);
ostream & operator <<(ostream &s, WithSheet<shared_ptr<AssocValue>> const &sha);
