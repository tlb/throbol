#include "./test_utils.h"

TEST_CASE("CellSet", "[core]") {
  CellSet s;
  s[10] = true;
  CHECK(s[10] == true);
  CHECK(s[9] == false);
  CHECK(s[1000] == false);
  CHECK(s.size() == 1);
  s[10] = true;
  CHECK(s.size() == 1);
  s[11] = true;
  CHECK(s.size() == 2);
}

TEST_CASE("Types", "[core]") {
  CHECK(alignof(F) == 4);
  CHECK(alignof(CellVals) == MAX_ALIGN);
};



TEST_CASE("Type tags", "[core]") {
  CHECK(typetag<mat4>().kind == TK_mat);
  CHECK(typetag<mat4>().nr == 4);
  CHECK(typetag<mat4>().nc == 4);

  CHECK(typetag<mat>().kind == TK_mat);
  CHECK(typetag<mat>().nr == 0);
  CHECK(typetag<mat>().nc == 0);

  CHECK(typetag<F>().kind == TK_F);
  CHECK(typetag<F>().nr == 0);
  CHECK(typetag<F>().nc == 0);

  CHECK(Val(typetag<F>()).ist<F>());
  CHECK(!Val(typetag<mat4>()).ist<F>());

  // mat should match mat4, but not vice versa. See Val::ist() in types.h
  CHECK(Val(typetag<mat4>()).ist<mat4>());
  CHECK(Val(typetag<mat4>()).ist<mat>());

  CHECK(!Val(typetag<mat>()).ist<mat4>());
  CHECK(Val(typetag<mat>()).ist<mat>());

  CHECK(typeName(typetag<F>()) == "F");
  CHECK(typeName(typetag<CF>()) == "CF");
  CHECK(typeName(typetag<mat>()) == "mat");
  CHECK(typeName(typetag<mat4>()) == "mat4");

  CHECK(typeByName("F").ist<F>());
  CHECK(typeByName("float").ist<F>());
  CHECK(typeByName("complex").ist<CF>());
  CHECK(typeByName("mat4").ist<mat4>());
  CHECK(typeByName("mat23") == TypeTag(TK_mat, 23, 23));
  CHECK(typeByName("mat").ist<mat>());
  CHECK(typeByName("mat3x5") == TypeTag(TK_mat, 3, 5));
  CHECK(typeByName("mat1x1") == TypeTag(TK_mat, 1, 1));
  CHECK(typeByName("mat1") == TypeTag(TK_mat, 1, 1));
  CHECK(typeByName("vec7") == TypeTag(TK_mat, 7, 1));
  CHECK(typeByName("vec26") == TypeTag(TK_mat, 26, 1));
  CHECK(typeByName("mat25x73") == TypeTag(TK_mat, 25, 73));
  CHECK(typeByName("mat25x73y") == TypeTag(TK_error));
  CHECK(typeByName("mat25xx73") == TypeTag(TK_error));
};

