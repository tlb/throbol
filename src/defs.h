#pragma once

#if defined(__EMSCRIPTEN__)
// Use this to ifdef out features that probably ought to work in the web version, but aren't implemented yet.
#define EMSCRIPTEN_NOTYET
#endif

#include "common/std_headers.h"
/*
  MAX_ALIGN: memory multiple to align several data structures to.
  Plausible choices are 16 and 32.
  I'd like to use EIGEN_IDEAL_MAX_ALIGN_BYTES, except this differs between x64 and arm64
  and depending on whether we compile with -mavx2.
  MAX_ALIGN finds its way into the client-server ABI and and the database (.replays) format
  so changing it requires testing against old databases or being willing to throw them away.
  x64 - arm64 interop is common, like if you're controlling a Linux/x64 robot from a Mac laptop.
*/
#define MAX_ALIGN 16
#define EIGEN_ALIGN 16

using CF = complex<float>;

#include "../geom/geom.h"

/*
  Insist that Eigen isn't hoping for more alignment than we're actually going to deliver.
  This restriction could be changed, if needed, with some testing. Right now this will
  cause a failure if you add -mavx2, which is good unless we can guarantee everything
  we link against gets built with compatible compiler flags.

  If you compile parts of the code with and without -mavx2, shit gets weird. You might see mysterious
  crashes when freeing some `Eigen::Matrix` objects because it used its custom aligned memory allocator
  to allocate it and the regular free() to free it.

*/
static_assert(MAX_ALIGN >= EIGEN_IDEAL_MAX_ALIGN_BYTES);
static_assert(MAX_ALIGN >= EIGEN_ALIGN);
static_assert(EIGEN_ALIGN <= EIGEN_IDEAL_MAX_ALIGN_BYTES);
static_assert(EIGEN_ALIGN >= EIGEN_MIN_ALIGN_BYTES);


#include "common/str_utils.h"
#include "common/chunky.h"
#include "numerical/numerical.h"
#include "./index_set.h"
#include "./blobs.h"
#include <variant>


struct Sheet;
struct G8DrawList;
struct PrintTarget;
struct CellDepsTarget;
struct CpuState;
struct AstNode;
struct BoxedPtr;
struct BoxedRef;
struct Trace;
struct CellVals;
struct Annotation;
struct DrawOp;
struct Pad;
struct CompiledSheet;
struct CompiledSheetFat;
struct Val;
struct Check;
struct GenericApiRef;
struct SheetApi;
struct ReplayBuffer;
struct VideoFrame;
struct AudioFrame;
struct Symbol;
struct SymbolSet;
struct SimCameraClient;
struct TypeTag;
struct ParticleSearchState;
struct Assoc;
using Assocp = Assoc *;

enum AstType : U32;

using CellIndex = U32;
const CellIndex nullCellIndex = -1;
using ParamIndex = U32;
const ParamIndex nullParamIndex = -1;
using ValueOffset = U32;
const ValueOffset nullValueOffset = -1;
using PageIndex = U32;
const PageIndex nullPageIndex = -1;
using HypoIndex = U32;
const HypoIndex nullHypoIndex = -1;
using TextIndex = U32;

const int MAX_BATCH_SIZE = 32;

struct DrawOpHandle {

  DrawOp *it;
  Assocp props;

  DrawOpHandle(DrawOp *_it, Assocp _props)
    : it(_it), props(_props)
  {
  }
  DrawOpHandle(DrawOp *_it)
    : it(_it), props(nullptr)
  {
  }
  DrawOpHandle()
    : it(nullptr), props(nullptr)
  {
  }

  operator bool() const { return it != nullptr; };
};

struct ApiHandle {

  string_view apiType;
  Assocp props = nullptr;

  ApiHandle(string_view _apiType, Assocp _props)
    : apiType(_apiType), props(_props)
  {
  }
  ApiHandle()
  {
  }

  operator bool() const { return !apiType.empty(); };
};


enum EmitMode {
  Emit_standalone,
  Emit_hw,
  Emit_ui,
};

#define foreach_batchi for (int batchi = 0; batchi < BATCH_SIZE; batchi++)

using CellSet = IndexSet<CellIndex>;

using CellOrder = vector<CellIndex>;
using CellEdges = vector<vector<CellIndex>>;
using CellProblems = vector<tuple<string, CellOrder>>;
using CellWire = tuple<CellIndex, CellIndex, U32, U32, vec2, vec2>; // src, dst, srcIndex, dstIndex, srcRel, dstRel

// XXX using EvalFunc = function<void(CpuState &cpu, Pad &pad, auto batchdef)>;
// XXX using GradFunc = function<void(CpuState &cpu, Pad &pad, Pad &padg)>;

struct FormulaSubstr {

  TextIndex begin, end;

  FormulaSubstr(TextIndex _begin, TextIndex _end) : begin(_begin), end(_end) {}
  FormulaSubstr() : begin(0), end(0) {}

  bool empty() const { return begin == 0 && end == 0; }

};

using Checkp = Check *;

ostream & operator << (ostream &s, FormulaSubstr const &a);

struct Arena : ChunkyAlloc {

  mat mkMat(Index nr, Index nc);
  cmat mkCmat(Index nr, Index nc);
  mat mkMat(mat const &other);
  cmat mkCmat(cmat const &other);

  vec mkVec(Index nr);
  cvec mkCvec(Index nr);
  vec mkVec(vec const &other);
  cvec mkCvec(cvec const &other);

  Assocp mkAssoc(Assocp _next, string_view _key, BoxedPtr _val);
};

#include "./celldata.h"

// See uipoll.cc
extern bool uiPollLive;

extern string installDir;
