#include "./emit.h"
#include "./emit_macros.h"
#include "../geom/colors.h"


static mat mkpts(Arena *pool, vec3 const &p0, vec3 const &p1)
{
  auto ret = pool->mkMat(3, 2);
  ret.col(0) = p0;
  ret.col(1) = p1;
  return ret;
}

deftype(line)
{
  return typetag<DrawOp>();
}

defemit(line)
{
  defop2(vec2, vec2, DrawOpHandle, r = cpu.pool->mk<DrawOpLine>(append0(a), append0(b)));
  defop2(vec3, vec3, DrawOpHandle, r = cpu.pool->mk<DrawOpLine>(a, b));
  defop2(vec4, vec4, DrawOpHandle, r = cpu.pool->mk<DrawOpLine>(a.hnormalized(), b.hnormalized()));
  if (args[0].t.nr == 2 || args[0].t.nr == 3) {
    defop1(mat, DrawOpHandle, r = cpu.pool->mk<DrawOpLines>(cpu.pool->mkMat(a)));
  }
  return setInvalidArgs();
}

defusage(line, R"(
  line(p0 ∊ vec2|3|4, p1 ∊ same)
  line(pts ∊ mat3xN))");


deftype(spline)
{
  return typetag<DrawOp>();
}

defemit(spline)
{
  if (matchArgs({TypeTag(TK_mat, 3, 0)})) {
    defop1(mat, DrawOpHandle, r = cpu.pool->mk<DrawOpSpline>(cpu.pool->mkMat(a)));
  }
  return setInvalidArgs();
}

defusage(spline, R"(
  spline(controlPoints ∊ mat3xN))")


deftype(arcArrowX)
{
  return typetag<DrawOp>();
}

defemit(arcArrowX)
{
  defop4(vec3, F, F, F, DrawOpHandle, r = cpu.pool->mk<DrawOpArcArrow>(a, vec3(0, 1, 0), vec3(0, 0, 1), vec3(1, 0, 0), b, c, d));
  return setInvalidArgs();
}

defusage(arcArrowX, R"(
  arcArrowX(center ∊ vec2|3, radius ∊ F, arcFrom ∊ F, arcTo ∊ F))");


deftype(arcArrowY)
{
  return typetag<DrawOp>();
}

defemit(arcArrowY)
{
  defop4(vec3, F, F, F, DrawOpHandle, r = cpu.pool->mk<DrawOpArcArrow>(a, vec3(1, 0, 0), vec3(0, 0, 1), vec3(0, 1, 0), b, c, d));
  return setInvalidArgs();
}

defusage(arcArrowY, R"(
  arcArrowY(center ∊ vec2, radius ∊ F, arcFrom ∊ F, arcTo ∊ F))");


deftype(arcArrowZ)
{
  return typetag<DrawOp>();
}

defemit(arcArrowZ)
{
  defop4(vec2, F, F, F, DrawOpHandle, r = cpu.pool->mk<DrawOpArcArrow>(append0(a), vec3(1, 0, 0), vec3(0, 1, 0), vec3(0, 0, 1), b, c, d));
  defop4(vec3, F, F, F, DrawOpHandle, r = cpu.pool->mk<DrawOpArcArrow>(a, vec3(1, 0, 0), vec3(0, 1, 0), vec3(0, 0, 1), b, c, d));
  return setInvalidArgs();
}

defusage(arcArrowZ, R"(
  arcArrowZ(center ∊ vec2|3, radius ∊ F, arcFrom ∊ F, arcTo ∊ F))");



deftype(arrow)
{
  return typetag<DrawOp>();
}

defemit(arrow)
{
  defop2(vec3, vec3, DrawOpHandle, r = cpu.pool->mk<DrawOpArrow>(a, b));
  return setInvalidArgs();
}

defusage(arrow, R"(
  arrow(origin ∊ vec3, dir ∊ vec3)");



deftype(sphere)
{
  return typetag<DrawOp>();
}

defemit(sphere)
{
  defop2(vec3, F, DrawOpHandle, r = cpu.pool->mk<DrawOpSphere>(a, b));
  return setInvalidArgs();
}

defusage(sphere, R"(
  sphere(center ∊ vec3, radius ∊ F)");



deftype(disk)
{
  return typetag<DrawOp>();
}

defemit(disk)
{
  defop2(vec2, F, DrawOpHandle, r = cpu.pool->mk<DrawOpDisk>(append0(a), b));
  defop2(vec3, F, DrawOpHandle, r = cpu.pool->mk<DrawOpDisk>(a, b));
  return setInvalidArgs();
}

defusage(disk, R"(
  disk(center ∊ vec2|3, radius ∊ F))");


deftype(tube)
{
  return typetag<DrawOp>();
}

defemit(tube)
{
  defop3(vec2, vec2, F, DrawOpHandle, r = cpu.pool->mk<DrawOpTube>(mkpts(cpu.pool, append0(a), append0(b)), c));
  defop3(vec3, vec3, F, DrawOpHandle, r = cpu.pool->mk<DrawOpTube>(mkpts(cpu.pool, a, b), c));
  defop2(vec2, vec2, DrawOpHandle, r = cpu.pool->mk<DrawOpTube>(mkpts(cpu.pool, append0(a), append0(b)), 0.05));
  defop2(vec3, vec3, DrawOpHandle, r = cpu.pool->mk<DrawOpTube>(mkpts(cpu.pool, a, b), 0.05));

  if (args[0].t.nr == 3) {
    defop2(mat, F, DrawOpHandle, r = cpu.pool->mk<DrawOpTube>(cpu.pool->mkMat(a), b));
    defop1(mat, DrawOpHandle, r = cpu.pool->mk<DrawOpTube>(cpu.pool->mkMat(a), 0.05));
  }
  return setInvalidArgs();
}

defusage(tube, R"(
  tube(p0 ∊ vec2|3, p1 ∊ same, radius=0.05 ∊ F)
  tube(pts ∊ mat3xN, radius=0.05 ∊ F)");


deftype(rect)
{
  return typetag<DrawOp>();
}

defemit(rect)
{
  defop3(vec2, vec2, vec2, DrawOpHandle, r = cpu.pool->mk<DrawOpRect>(append0(a), append0(b), append0(c)));
  defop3(vec3, vec3, vec3, DrawOpHandle, r = cpu.pool->mk<DrawOpRect>(a, b, c));
  return setInvalidArgs();
}

defusage(rect, R"(
  rect(center ∊ vec2|3, xdir ∊ vec2|3, ydir ∊ vec2|3))");



deftype(text)
{
  return typetag<DrawOp>();
}

defemit(text)
{
  defop3(vec2, F, string_view, DrawOpHandle, r = cpu.pool->mk<DrawOpText>(append0(a), b, c));
  defop3(vec3, F, string_view, DrawOpHandle, r = cpu.pool->mk<DrawOpText>(a, b, c));
  return setInvalidArgs();
}

defusage(text, R"(
  text(pos ∊ vec2|3, size ∊ F, text ∊ string))");


deftype(solid)
{
  return typetag<DrawOp>();
}

defemit(solid)
{
  auto page = ce.sh.pageByCell[ce.cell];
  auto pageFileName = ce.sh.fileNameByPage[page];
  auto pageFileDir = myDirname(pageFileName);
  string_view pageFileDirView = ce.she.buildlife.strdupview(pageFileDir);
  defop1(string_view, DrawOpHandle, r = cpu.pool->mk<DrawOpSolid>(a, pageFileDirView));
  return setInvalidArgs();
}

defusage(solid, R"(
  solid(filename ∊ string))");



deftype(axisArrows)
{
  return typetag<DrawOp>();
}

defemit(axisArrows)
{
  defop0(DrawOpHandle, r = cpu.pool->mk<DrawOpAxisArrows>());
  return setInvalidArgs();
}

defusage(axisArrows, R"(
  axisArrows())");



deftype(dial)
{
  return typetag<DrawOp>();
}

defemit(dial)
{
  defop2(F, F, DrawOpHandle, r = cpu.pool->mk<DrawOpDial>(vec3(0.5, 0, 0), 0.5, a, b));
  defop4(vec2, F, F, F, DrawOpHandle, r = cpu.pool->mk<DrawOpDial>(append0(a), b, c, d));
  defop4(vec3, F, F, F, DrawOpHandle, r = cpu.pool->mk<DrawOpDial>(a, b, c, d));
  return setInvalidArgs();
}

defusage(dial, R"(
  dial(value ∊ F, range ∊ F): default center=[0.5, 0. 0], radius=0.5
  dial(center ∊ vec2|3, radius ∊ F, value ∊ F, range ∊ F))");



deftype(label)
{
  return typetag<DrawOp>();
}

defemit(label)
{
  defop2(F, string_view, DrawOpHandle, r = cpu.pool->mk<DrawOpLabel>(a, b));
  return setInvalidArgs();
}

defusage(label, R"(
  label(value ∊ F, text ∊ string))");



deftype(graphColor)
{
  return typetag<DrawOp>();
}

defemit(graphColor)
{
  defop1(F, vec4, r = goodGraphColor(size_t(a)));
  return setInvalidArgs();
}

defusage(graphColor, R"(
  graphColor(index ∊ F))");



/*
  policyTerrain(rMetric, xParam, xRange, yParam, yRange)
  eg:
    policyTerrain(log(total.spend), install.rate@0, 0.1, cost.over.quality@0, 0.1)
*/
deftype(policyTerrain)
{
  return typetag<DrawOp>();
}

defemit(policyTerrain)
{
  if (n->nch() == 5) {
    evalSpecial(n->ch(0));
    if (argmatch1(F)) {
      auto xParam = ce.getParamIndex(n->ch(1));
      if (xParam == nullParamIndex) return setError("Bad param", n->ch(1));
      auto yParam = ce.getParamIndex(n->ch(3));
      if (yParam == nullParamIndex) return setError("Bad param", n->ch(3));

      auto xRange = getConstF(n->ch(2));
      if (!xRange) return setError("Range must be constant", n->ch(2));
      auto yRange = getConstF(n->ch(4));
      if (!yRange) return setError("Range must be constant", n->ch(4));

      // Allocate a place for the policy search system to find the metric
      auto rMetricRef = mkValue(typetag<F>());

      auto xVar = PolicySearchDim(xParam, *xRange);
      auto yVar = PolicySearchDim(yParam, *yRange);

      auto rv = allocResult<DrawOpHandle>();
      she.evalop([rv1=rv, av1=args[0], rMetricRef, xVar, yVar](CpuState &cpu, Pad &pad, auto batchdef) {
        auto rv = batchdef.getVal(rv1);
        auto av = batchdef.getVal(av1);
        for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
          if (cpu.traces[batchi] && cpu.traces[batchi]->randomSeed == 0 && cpu.curs[batchi]) {
            decltype(auto) a = pad.rd_F(av, batchi);
            cpu.curs[batchi]->wr_F(rMetricRef) = a;
            pad.wr_DrawOpHandle(rv, batchi) = cpu.pool->mk<DrawOpPolicyTerrain>(
              rMetricRef,
              xVar,
              yVar,
              cpu.paramHashes[batchi],
              cpu.compiled.epoch,
              cpu.timeIndex,
              a);
          } else {
            pad.wr_DrawOpHandle(rv, batchi) = nullptr;
          }
        }
      });
      return rv;
    }
  }
  else if (n->nch() == 3) {
    evalSpecial(n->ch(0));
    if (argmatch1(F)) {
      auto xParam = ce.getParamIndex(n->ch(1));
      if (xParam == nullParamIndex) return setError("Bad param", n->ch(1));
      auto yParam = ce.getParamIndex(n->ch(2));
      if (yParam == nullParamIndex) return setError("Bad param", n->ch(2));

      // Allocate a place for the policy search system to find the metric
      auto rMetricRef = mkValue(typetag<F>());

      auto xVar = PolicySearchDim(xParam, 0.25);
      auto yVar = PolicySearchDim(yParam, 0.25);
      
      auto rv = allocResult<DrawOpHandle>();
      she.evalop([rv1=rv, av1=args[0], rMetricRef, xVar, yVar](CpuState &cpu, Pad &pad, auto batchdef) {
        auto rv = batchdef.getVal(rv1);
        auto av = batchdef.getVal(av1);
        for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
          if (cpu.traces[batchi] && cpu.traces[batchi]->randomSeed == 0 && cpu.curs[batchi]) {
            decltype(auto) a = pad.rd_F(av, batchi);
            cpu.curs[batchi]->wr_F(rMetricRef) = a;
            pad.wr_DrawOpHandle(rv, batchi) = cpu.pool->mk<DrawOpPolicyTerrain>(
              rMetricRef,
              xVar,
              yVar,              
              cpu.paramHashes[batchi],
              cpu.compiled.epoch,
              cpu.timeIndex,
              a);
          }
          else {
            pad.wr_DrawOpHandle(rv, batchi) = nullptr;
          }
        }
      });
      return rv;
    }
  }
  return setInvalidArgs();
}

defusage(policyTerrain, R"(
  policyTerrain(rMetric ∊ F, xParam ∊ param, yParam ∊ param)
  policyTerrain(rMetric ∊ F, xParam ∊ param, xRange ∊ F, yParam ∊ param, yRange ∊ F))");



deftype(linePlot)
{
  return typetag<DrawOp>();
}

defemit(linePlot)
{
  defop2(F, F, DrawOpHandle, r = cpu.pool->mk<DrawOpLinePlot>(a, b, 0.0f));
  defop3(F, F, F, DrawOpHandle, r = cpu.pool->mk<DrawOpLinePlot>(a, b, c));
  defop1(CF, DrawOpHandle, r = cpu.pool->mk<DrawOpLinePlot>(a.real(), a.imag(), 0.0f));
  defop1(vec2, DrawOpHandle, r = cpu.pool->mk<DrawOpLinePlot>(a.x(), a.y(), 0.0f));
  defop1(vec3, DrawOpHandle, r = cpu.pool->mk<DrawOpLinePlot>(a.x(), a.y(), a.z()));
  return setInvalidArgs();
}

defusage(linePlot, R"(
  linePlot(x ∊ F, y ∊ F)
  linePlot(x ∊ F, y ∊ F, z ∊ F)
  linePlot(pt ∊ CF|vec2|3))");



deftype(bandPlot)
{
  return typetag<DrawOp>();
}

defemit(bandPlot)
{
  defop3(F, F, F, DrawOpHandle, r = cpu.pool->mk<DrawOpBandPlot>(a, b, c, 0.0f));
  defop4(F, F, F, F, DrawOpHandle, r = cpu.pool->mk<DrawOpBandPlot>(a, b, c, d));
  return setInvalidArgs();
}

defusage(bandPlot, R"(
  bandPlot(x ∊ F, y0 ∊ F, y1 ∊ F)
  bandPlot(x ∊ F, y0 ∊ F, y1 ∊ F, z ∊ F))");



deftype(scatterPlot)
{
  return typetag<DrawOp>();
}

defemit(scatterPlot)
{
  defop2(F, F, DrawOpHandle, r = cpu.pool->mk<DrawOpScatterPlot>(a, b, 0.0f));
  defop3(F, F, F, DrawOpHandle, r = cpu.pool->mk<DrawOpScatterPlot>(a, b, c));
  defop1(vec2, DrawOpHandle, r = cpu.pool->mk<DrawOpScatterPlot>(a.x(), a.y(), 0.0f));
  defop1(vec3, DrawOpHandle, r = cpu.pool->mk<DrawOpScatterPlot>(a.x(), a.y(), a.z()));
  return setInvalidArgs();
}

defusage(scatterPlot, R"(
  scatterPlot(x ∊ F, y ∊ F)
  scatterPlot(x ∊ F, y ∊ F, z ∊ F)
  scatterPlot(pt ∊ vec2|3)");




deftype(epicycles)
{
  return typetag<DrawOp>();
}

defemit(epicycles)
{
  defop1(cvec, DrawOpHandle, r = cpu.pool->mk<DrawOpEpicycles>(cpu.pool->mkCvec(a), 0.001f, 0.001f));
  defop3(cvec, F, F, DrawOpHandle, r = cpu.pool->mk<DrawOpEpicycles>(cpu.pool->mkCvec(a), b, c));
  return setInvalidArgs();
}

defusage(epicycles, R"(
  epicycles(pts ∊ cvec3xN, realRange=0.001 ∊ F, imagRange=0.001 ∊ F))");


// Or left-multiply the DrawOp by a mat4
deftype(transform)
{
  return typetag<DrawOp>();
}

defemit(transform)
{
  defop2(mat4, DrawOpHandle, DrawOpHandle, r = cpu.pool->mk<DrawOpTransform>(a, b));
  return setInvalidArgs();
}

defusage(transform, R"(
  transform(m ∊ mat4, content ∊ DrawOp))");



deftype(colorize)
{
  return typetag<DrawOp>();
}

defemit(colorize)
{
  defop2(mat4, DrawOpHandle, DrawOpHandle, r = cpu.pool->mk<DrawOpColor>(a, b));
  defop2(vec4, DrawOpHandle, DrawOpHandle, r = cpu.pool->mk<DrawOpColor>(a.asDiagonal(), b));
  return setInvalidArgs();
}

defusage(colorize, R"(
  colorize(v ∊ vec4, content ∊ DrawOp) -- transform white to v
  colorize(m ∊ mat4, content ∊ DrawOp) -- linear color transform by m)");



deftype(sweep)
{
  return typetag<DrawOp>();
}

defemit(sweep)
{
  defop2(F, DrawOpHandle, DrawOpHandle, r = cpu.pool->mk<DrawOpSweep>(a, b));
  return setInvalidArgs();
}

defusage(sweep, R"(
  sweep(range ∊ F, content ∊ DrawOp))");



deftype(repr)
{
  return typetag<string_view>();
}

defemit(repr)
{
  defop1(F, string_view, r = cpu.pool->strdupview(repr(a)));
  defop1(CF, string_view, r = cpu.pool->strdupview(repr(a)));
  defop1(vec2, string_view, r = cpu.pool->strdupview(repr(a)));
  defop1(vec3, string_view, r = cpu.pool->strdupview(repr(a)));
  defop1(vec4, string_view, r = cpu.pool->strdupview(repr(a)));
  defop1(mat2, string_view, r = cpu.pool->strdupview(repr(a)));
  defop1(mat3, string_view, r = cpu.pool->strdupview(repr(a)));
  defop1(mat4, string_view, r = cpu.pool->strdupview(repr(a)));
  defop1(mat, string_view, r = cpu.pool->strdupview(repr(a)));
  defop1(cmat, string_view, r = cpu.pool->strdupview(repr(a)));
  if (argmatch1(string_view)) {
    return args[0];
  }
  return setInvalidArgs();
}

defusage(repr, R"(
  repr(any) -- string representation)");

