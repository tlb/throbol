#include "./emit.h"
#include "./emit_macros.h"

deftype(run)
{
  return typetag<Beh>();
}

defemit(run)
{
  defop1(SymbolSet, Beh, r = Beh(B_running, a));
  return setInvalidArgs();
}

defusage(run, R"(
  run(symbols))");


deftype(isrunning)
{
  return typetag<bool>();
}

defemit(isrunning)
{
  defop1(Beh, bool, r = (a.status == B_running));
  defop2(Beh, SymbolSet, bool, r = (a.status == B_running && op_hasany(cpu, a.runset, b)));
  defdelay3(F, mkValue(typetag<F>()), Beh, SymbolSet, F, F, (snew = (a.status == B_running && op_hasany(cpu, a.runset, b)) ? sprev + cpu.dts[batchi] : 0.0f, r = snew >= c));
  return setInvalidArgs();
}

defusage(isrunning, R"(
  isrunning(Beh)
  isrunning(Beh, SymbolSet)
  isrunning(Beh, SymbolSet, duration ∊ F)");


deftype(success)
{
  return typetag<Beh>();
}

defemit(success)
{
  defop0(Beh, r = Beh(B_success));
  return setInvalidArgs();
}

defusage(success, R"(
  success())");


deftype(issuccess)
{
  return typetag<F>();
}

defemit(issuccess)
{
  defop1(Beh, F, r = (a.status == B_success));
  return setInvalidArgs();
}

defusage(issuccess, R"(
  issuccess(Beh))");


deftype(failure)
{
  return typetag<Beh>();
}

defemit(failure)
{
  defop0(Beh, r = Beh(B_failure));
  return setInvalidArgs();
}

defusage(failure, R"(
  failure())");


deftype(isfailure)
{
  return typetag<F>();
}

defemit(isfailure)
{
  defop1(Beh, F, r = (a.status == B_failure));
  return setInvalidArgs();
}

defusage(isfailure, R"(
  isfailure(Beh))");


deftype(cond)
{
  return typetag<Beh>();
}

defemit(cond)
{
  defop1(bool, Beh, r = a ? Beh(B_success) : Beh(B_failure));
  return setInvalidArgs();
}

defusage(cond, R"(
  cond(F))");


deftype(fallback)
{
  return typetag<Beh>();
}

defemit(fallback)
{
  evalArgs();
  ValSuite retv = allocResult<Beh>();
  ValSuite livev = allocPad<bool>();
  she.evalop([retv1=retv, livev1=livev](CpuState &cpu, Pad &pad, auto batchdef) {
    auto retv = batchdef.getVal(retv1);
    auto livev = batchdef.getVal(livev1);
    for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
      Beh &ret = pad.wr_Beh(retv, batchi);
      F &live = pad.wr_F(livev, batchi);
      ret = Beh(B_failure);
      live = true;
    }
  });

  for (size_t argi=0; argi < args.size(); argi++) {

    auto chvalv = args[argi];
    if (chvalv.t != typetag<Beh>()) goto fail;

    she.evalop([retv1=retv, chvalv1=chvalv, livev1=livev](CpuState &cpu, Pad &pad, auto batchdef) {
      auto retv = batchdef.getVal(retv1);
      auto chvalv = batchdef.getVal(chvalv1);
      auto livev = batchdef.getVal(livev1);
      for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
        Beh &ret = pad.up_Beh(retv, batchi);
        Beh const &chval = pad.rd_Beh(chvalv, batchi);
        bool &live = pad.up_bool(livev, batchi);
        if (live) {
          if (chval.status == B_running) {
            ret = Beh(B_running, op_union(cpu, ret.runset, chval.runset));
            live = false;
          }
          else if (chval.status == B_success) {
            ret = Beh(B_success, op_union(cpu, ret.runset, chval.runset));
            live = false;
          }
        }
      }
    });
  }
  return retv;
fail:
  return setInvalidArgs();
}

defusage(fallback, R"(
  fallback(Beh...) -- tries arguments until one runs or succeeds)");


deftype(par)
{
  return typetag<Beh>();
}

defemit(par)
{
  evalArgs();
  if (matchArgs({typetag<F>()}, true)) {
    auto successThreshv = args[0];
    auto nChildren = args.size() - 1;

    ValSuite retv = allocResult<Beh>();
    ValSuite successCountv = allocPad<F>();
    ValSuite failCountv = allocPad<F>();

    she.evalop([retv1=retv, successCountv1=successCountv, failCountv1=failCountv](CpuState &cpu, Pad &pad, auto batchdef) {
      auto retv = batchdef.getVal(retv1);
      auto successCountv = batchdef.getVal(successCountv1);
      auto failCountv = batchdef.getVal(failCountv1);
      for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
        Beh &ret = pad.wr_Beh(retv, batchi);
        F &successCount = pad.wr_F(successCountv, batchi);
        F &failCount = pad.wr_F(failCountv, batchi);
        ret = Beh(B_running);
        successCount = 0;
        failCount = 0;
      }
    });

    for (size_t argi = 1; argi < args.size(); argi++) {
      auto chvalv = args[argi];
      if (chvalv.t != typetag<Beh>()) goto fail;

      she.evalop([retv1=retv, chvalv1=chvalv, successCountv1=successCountv, failCountv1=failCountv](CpuState &cpu, Pad &pad, auto batchdef) {
        auto retv = batchdef.getVal(retv1);
        auto chvalv = batchdef.getVal(chvalv1);
        auto successCountv = batchdef.getVal(successCountv1);
        auto failCountv = batchdef.getVal(failCountv1);
        for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
          Beh &ret = pad.wr_Beh(retv, batchi);
          Beh const &chval = pad.rd_Beh(chvalv, batchi);
          F &successCount = pad.up_F(successCountv, batchi);
          F &failCount = pad.up_F(failCountv, batchi);

          ret = Beh(B_running, op_union(cpu, ret.runset, chval.runset));
          if (chval.status == B_success) {
            successCount += 1.0f;
          }
          else if (chval.status == B_failure) {
            failCount += 1.0f;
          }
        }
      });
    }

    she.evalop([retv1=retv, successCountv1=successCountv, failCountv1=failCountv, successThreshv1=successThreshv, nChildren](CpuState &cpu, Pad &pad, auto batchdef) {
      auto retv = batchdef.getVal(retv1);
      auto successCountv = batchdef.getVal(successCountv1);
      auto failCountv = batchdef.getVal(failCountv1);
      auto successThreshv = batchdef.getVal(successThreshv1);
      for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
        Beh &ret = pad.wr_Beh(retv, batchi);
        F const &successCount = pad.rd_F(successCountv, batchi);
        F const &failCount = pad.rd_F(failCountv, batchi);
        F const &successThresh = pad.rd_F(successThreshv, batchi);
        if (successCount >= successThresh) {
          ret.status = B_success;
        }
        else if (failCount > nChildren - successThresh) {
          ret.status = B_failure;
        }
      }
    });
    return retv;
  }
fail:
  return setInvalidArgs();
}

defusage(par, R"(
  par(thresh ∊ F, Beh...) -- runs all behaviors, returns success if at least thresh succeed)");


deftype(seq)
{
  return typetag<Beh>();
}

defemit(seq)
{
  evalArgs();
  ValSuite retv = allocResult<Beh>();
  ValSuite livev = allocPad<bool>();
  she.evalop([retv1=retv, livev1=livev](CpuState &cpu, Pad &pad, auto batchdef) {
    auto retv = batchdef.getVal(retv1);
    auto livev = batchdef.getVal(livev1);
    for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
      decltype(auto) ret = pad.wr_Beh(retv, batchi);
      decltype(auto) live = pad.wr_bool(livev, batchi);
      ret = Beh(B_success);
      live = 1.0f;
    }
  });

  for (size_t argi = 0; argi < args.size(); argi++) {

    auto chvalv = args[argi];
    if (chvalv.t != typetag<Beh>()) goto fail;

    she.evalop([retv1=retv, chvalv1=chvalv, livev1=livev](CpuState &cpu, Pad &pad, auto batchdef) {
      auto retv = batchdef.getVal(retv1);
      auto chvalv = batchdef.getVal(chvalv1);
      auto livev = batchdef.getVal(livev1);
      for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
        Beh &ret = pad.up_Beh(retv, batchi);
        Beh const &chval = pad.rd_Beh(chvalv, batchi);
        decltype(auto) live = pad.up_bool(livev, batchi);
        if (live) {
          if (chval.status == B_running) {
            ret = Beh(B_running, op_union(cpu, ret.runset, chval.runset));
            live = false;
          }
          else if (chval.status == B_failure) {
            ret = Beh(B_failure, op_union(cpu, ret.runset, chval.runset));
            live = false;
          }
        }
      }
    });
  }
  return retv;
fail:
  return setInvalidArgs();
}

defusage(seq, R"(
  seq(Beh...) -- tries arguments until one runs or fails)");


deftype(piggyback)
{
  return typetag<Beh>();
}

defemit(piggyback)
{
  evalArgs();
  if (matchArgs({typetag<Beh>()}, true)) {

    ValSuite retv = allocResult<Beh>();
    she.evalop([retv1=retv, av1=args[0]](CpuState &cpu, Pad &pad, auto batchdef) {
      auto retv = batchdef.getVal(retv1);
      auto av = batchdef.getVal(av1);
      for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
        Beh &ret = pad.wr_Beh(retv, batchi);
        Beh const &a = pad.rd_Beh(av, batchi);
        ret = a;
      }
    });

    for (size_t argi = 1; argi < args.size(); argi++) {

      auto chvalv = args[argi];
      if (chvalv.t != typetag<Beh>()) goto fail;

      she.evalop([retv1=retv, chvalv1=chvalv](CpuState &cpu, Pad &pad, auto batchdef) {
        auto retv = batchdef.getVal(retv1);
        auto chvalv = batchdef.getVal(chvalv1);
        for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
          Beh &ret = pad.up_Beh(retv, batchi);
          Beh const &chval = pad.rd_Beh(chvalv, batchi);
          if (ret.status == B_running) {
            ret = Beh(B_running, op_union(cpu, ret.runset, chval.runset));
          }
        }
      });
    }
    return retv;
  }
fail:
  return setInvalidArgs();
}

defusage(piggyback, R"(
  piggyback(main ∊ Beh, Beh...) -- if main succeeds, also run the other args)");
