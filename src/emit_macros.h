
/*
  These macros save a lot of boilerplate, but be careful. Note that all of them return
  a Val from the enclosing function if they succeed, or proceed if they don't match.

  Read the "Emitting Code" section of ARCHITECTURE.md to understand this
*/

#define argmatch0() matchArgs({})
#define argmatch1(AT) matchArgs({typetag<AT>()})
#define argmatch2(AT, BT) matchArgs({typetag<AT>(), typetag<BT>()})
#define argmatch3(AT, BT, CT) matchArgs({typetag<AT>(), typetag<BT>(), typetag<CT>()})
#define argmatch4(AT, BT, CT, DT) matchArgs({typetag<AT>(), typetag<BT>(), typetag<CT>(), typetag<DT>()})
#define argmatch5(AT, BT, CT, DT, ET) matchArgs({typetag<AT>(), typetag<BT>(), typetag<CT>(), typetag<DT>(), typetag<ET>()})
#define argmatch6(AT, BT, CT, DT, ET, FT) matchArgs({typetag<AT>(), typetag<BT>(), typetag<CT>(), typetag<DT>(), typetag<ET>(), typetag<FT>()})

#define emitlog(INFO) \
  if constexpr (CpuState::execVerbose) { \
    if (CpuState::logFile) { \
      std::lock_guard lock(CpuState::logFileMutex); \
      *CpuState::logFile << __FILE__ << ":" << __LINE__ << " [" << batchi << "] " << INFO << "\n"; \
    } \
  }

#define emitlog_simd(INFO) \
  if constexpr (CpuState::execVerbose) { \
    if (CpuState::logFile) { \
      std::lock_guard lock(CpuState::logFileMutex); \
      *CpuState::logFile << __FILE__ << ":" << __LINE__ << " [simd] " << INFO << "\n"; \
    } \
  }

/*
  defset(AT, V, DSTT, DST): 
  If the value V has the type AT, emit code to write it to the spreadsheet (`*cpu.curs`) at location DST
  and return from the enclosing scope.
    AT is a type known to the TypeTag system
    V is a `Val`
    DST is a BoxedRef or ValueOffset

  Squash the write if it's going to a sensor cell and `cpu.squashSensorWrites` is set. (This is how we turn a
  self-contained spreadsheet into a live interactive one.) As an optimization, we only include the
  runtime check on `cpu.squashSensorWrites` if `isSensor` is true (for the current cell) at code gen time.
*/
#define defset(AT, AV, DSTT, DSTV) \
  if (AV.template ist<AT>() && DSTV.template ist<DSTT>()) { \
    she.evalop([av1=AV, dstv=DSTV, cell=cell](CpuState &cpu, Pad &pad, auto batchdef) { \
      auto av = batchdef.getVal(av1); \
      if (cpu.squashSensorWrites && ( \
        cpu.compiled.apiDirectionByCell[cell] == API_SENSOR || \
        cpu.compiled.apiDirectionByCell[cell] == API_MONITOR)) { \
      } \
      else { \
        for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
          if (batchi < cpu.realBatchSize && cpu.curs[batchi]) { \
            decltype(auto) a = pad.rd_##AT(av, batchi); \
            cpu.curs[batchi]->wr_##DSTT##_unchecked(dstv) = a; \
            emitlog("mem." << WithSheet(cpu.sh, dstv) << " = " << av << "=" << a) \
          } \
        } \
      } \
    }); \
    return; \
  }

#define defset_simd(AT, AV, DSTT, DSTV) \
  if (AV.template ist<AT>() && DSTV.template ist<DSTT>()) { \
    she.evalop([av=AV, dstv=DSTV, cell=cell](CpuState &cpu, Pad &pad, auto batchdef) { \
      auto rv = batchdef.getVal(rv1); \
      if (cpu.squashSensorWrites && ( \
        cpu.compiled.apiDirectionByCell[cell] == API_SENSOR || \
        cpu.compiled.apiDirectionByCell[cell] == API_MONITOR)) { \
      } \
      else { \
        decltype(auto) a = pad.rd_##AT##_simd(av, batchdef); \
        cpu.wrref_##DSTT##_simd( dstv, a, batchdef); \
        emitlog_simd("mem." << WithSheet(cpu.sh, dstv) << " = " << av << "=" << a) \
      }); \
    return; \
  }

// Same as above but do gradient too. Only works for types with a + operator
#define defsetg(AT, AV, DSTT, DSTV) \
  if (AV.template ist<AT>() && DSTV.template ist<DSTT>()) { \
    she.evalop([av1=AV, dstv=DSTV, cell=cell](CpuState &cpu, Pad &pad, auto batchdef) { \
      auto av = batchdef.getVal(av1); \
      if (cpu.squashSensorWrites && ( \
        cpu.compiled.apiDirectionByCell[cell] == API_SENSOR || \
        cpu.compiled.apiDirectionByCell[cell] == API_MONITOR)) { \
      } \
      else { \
        for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
          if (batchi < cpu.realBatchSize && cpu.curs[batchi]) { \
            decltype(auto) a = pad.rd_##AT(av, batchi); \
            cpu.curs[batchi]->wr_##DSTT##_unchecked(dstv) = a; \
            emitlog("mem." << WithSheet(cpu.sh, dstv) << " = " << av << "=" << a); \
          } \
        } \
      } \
    }); \
    she.gradop([av1=AV, dstv=DSTV](CpuState &cpu, Pad &pad, Pad &padg, auto batchdef) { \
      auto av = batchdef.getVal(av1); \
      for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
        if (batchi < cpu.realBatchSize && cpu.grads[batchi]) { \
          decltype(auto) ag = padg.up_##AT(av, batchi); \
          ag += cpu.grads[batchi]->rd_##DSTT##_unchecked(dstv); \
        } \
      } \
    }); \
    return; \
  }

#define defsetg_simd(AT, AV, DSTT, DSTV) \
  if (AV.template ist<AT>() && DSTV.template ist<DSTT>()) { \
    she.evalop([av1=AV, dstv=DSTV, cell=cell](CpuState &cpu, Pad &pad, auto batchdef) { \
      auto av = batchdef.getVal(av1); \
      if (cpu.squashSensorWrites && ( \
        cpu.compiled.apiDirectionByCell[cell] == API_SENSOR || \
        cpu.compiled.apiDirectionByCell[cell] == API_MONITOR)) { \
      } \
      else { \
        decltype(auto) a = pad.rd_##AT##_simd(av, batchdef); \
        cpu.wrcur_##DSTT##_simd(dstv, a, batchdef); \
        emitlog_simd("mem." << WithSheet(cpu.sh, dstv) << " = " << av << "=" << a) \
      } \
    }); \
    she.gradop([av1=AV, dstv=DSTV](CpuState &cpu, Pad &pad, Pad &padg, auto batchdef) { \
      auto av = batchdef.getVal(av1); \
      decltype(auto) ag = padg.up_##AT##_simd(av, batchdef); \
      ag += cpu.rdgrad_##DSTT##_simd(dstv, batchdef); \
    }); \
    return; \
  }

/*
  Code emitting macros with 0 operands (Again: read ARCHITECTURE.md first).

  Call emit_evalop0(RT, CODE) to emit code that evaluates CODE with a variable `r` (of type `RT`) in scope
  where the `CODE` should write the result to `r`.
  For example, when emitting code for the time function:
  ```
    emit_evalop0(F, r = cpu.time)
  ```
  In the RHS, `r` is lexically captured from inside this macro. It's of type `float &` in this case.
  (It's usually `RT &`, except dynamic matrices are special.)
  `cpu` is also captured -- it's the `CpuState` structure used during execution.

  A C++ note: we use `decltype(auto)` to bind variables. This is different from `auto` because
  it preserves reference semantics exactly as returned by functions like pad.wr_T(...) (T replaced with a type).

  `pad.wr_T` returns a `T &`, `pad.rd_T` returns a `T const &`, except with variable-sized matrices
  they return a Eigen::Map which contains a pointer to the matrix values and also the sizes.
  So we use decltype(auto) to preserve all this.
*/

/*
  0 args
*/

/* 
  Capturing rv as rv1 and then assigning to a local rv improves speed because
  otherwise Clang can't prove that the captured variable hasn't changed between
  loop iterations.
*/
#define emit_evalop0(RT, CODE) \
  she.evalop([=, rv1=rv](CpuState &cpu, Pad &pad, auto batchdef) { \
    auto rv = batchdef.getVal(rv1); \
    for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
      decltype(auto) r = pad.wr_##RT(rv, batchi); \
      if (batchi < cpu.realBatchSize) { \
        CODE; \
        emitlog(rv << "=" << r << " " << #CODE); \
      } \
      else { \
        setZero(r); \
      } \
    } \
  });

#define emit_evalop0_simd(RT, CODE) \
  she.evalop([=, rv1=rv](CpuState &cpu, Pad &pad, auto batchdef) { \
    auto rv = batchdef.getVal(rv1); \
    decltype(auto) r = pad.wr_##RT##_simd(rv, batchdef); \
    CODE; \
    emitlog_simd(rv << "=" << r << " " << #CODE); \
  }); \

#define emit_gradop0(RT, REVCODE) \
  she.gradop([=, rv1=rv](CpuState &cpu, Pad &pad, Pad &padg, auto batchdef) { \
    auto rv = batchdef.getVal(rv1); \
    for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
      if (batchi < cpu.realBatchSize) { \
        [[maybe_unused]] decltype(auto) r = pad.rd_##RT(rv, batchi); \
        [[maybe_unused]] decltype(auto) rg = padg.rd_##RT(rv, batchi); \
        REVCODE; \
      } \
    } \
  });

#define emit_gradop0_simd(RT, REVCODE) \
  she.gradop([=, rv1=rv](CpuState &cpu, Pad &pad, Pad &padg, auto batchdef) { \
    auto rv = batchdef.getVal(rv1); \
    [[maybe_unused]] decltype(auto) r = pad.rd_##RT##_simd(rv, batchdef); \
    [[maybe_unused]] decltype(auto) rg = padg.rd_##RT##_simd(rv, batchdef); \
    REVCODE; \
  });

#define defop0(RT, CODE) \
  if (argmatch0()) { \
    auto rv = allocResult<RT>(); \
    emit_evalop0(RT, CODE) \
    return rv; \
  }

#define defop0_simd(RT, CODE) \
  if (argmatch0()) { \
    auto rv = allocResult<RT>(); \
    emit_evalop0_simd(RT, CODE) \
    return rv; \
  }

#define defop0r(RT, TEST, ALLOC, CODE) \
  if (argmatch0() && TEST) { \
    auto rv = ALLOC; \
    emit_evalop0(RT, CODE) \
    return rv; \
  }

/*
  n args
*/

#define defopn(RT, CODE) \
  if (1) { \
    auto rv = allocResult<RT>(); \
    emit_evalop0(RT, CODE) \
    return rv; \
  }

#define defopn_simd(RT, CODE) \
  if (1) { \
    auto rv = allocResult<RT>(); \
    emit_evalop0_simd(RT, CODE) \
    return rv; \
  }

#define defopng(RT, CODE, REVCODE) \
  if (1) { \
    auto rv = allocResult<RT>(); \
    emit_evalop0(RT, CODE) \
    emit_gradop0(RT, REVCODE) \
    return rv; \
  }

#define defopng_simd(RT, CODE, REVCODE) \
  if (1) { \
    auto rv = allocResult<RT>(); \
    emit_evalop0_simd(RT, CODE) \
    emit_gradop0_simd(RT, REVCODE) \
    return rv; \
  }

#define defopnr(RT, TEST, ALLOC, CODE) \
  if (TEST) { \
    auto rv = ALLOC; \
    emit_evalop0(RT, CODE) \
    return rv; \
  }

/*
  1 arg
*/

#define emit_evalop1(AT, RT, CODE) \
  she.evalop([=, rv1=rv, av1=args[0]](CpuState &cpu, Pad &pad, auto batchdef) { \
    auto rv = batchdef.getVal(rv1); \
    auto av = batchdef.getVal(av1); \
    for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
      decltype(auto) r = pad.wr_##RT(rv, batchi); \
      if (batchi < cpu.realBatchSize) { \
        decltype(auto) a = pad.rd_##AT(av, batchi); \
        CODE; \
        emitlog(rv << "=" << r << " " #CODE << \
          " a=" << av  << "=" << a); \
      } else { \
        setZero(r); \
      } \
    } \
  });

#define emit_evalop1_simd(AT, RT, CODE) \
  she.evalop([=, rv1=rv, av1=args[0]](CpuState &cpu, Pad &pad, auto batchdef) { \
    auto rv = batchdef.getVal(rv1); \
    auto av = batchdef.getVal(av1); \
    decltype(auto) r = pad.wr_##RT##_simd(rv, batchdef); \
    decltype(auto) a = pad.rd_##AT##_simd(av, batchdef); \
    CODE; \
    emitlog_simd(rv << "=" << r << " " #CODE << \
      " a=" << av << "=" << a); \
  });

#define emit_gradop1(AT, RT, REVCODE) \
  she.gradop([=, rv1=rv, av1=args[0]](CpuState &cpu, Pad &pad, Pad &padg, auto batchdef) { \
    auto rv = batchdef.getVal(rv1); \
    auto av = batchdef.getVal(av1); \
    for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
      if (batchi < cpu.realBatchSize) { \
        [[maybe_unused]] decltype(auto) r = pad.wr_##RT(rv, batchi); \
        [[maybe_unused]] decltype(auto) rg = padg.rd_##RT(rv, batchi); \
        [[maybe_unused]] decltype(auto) a = pad.rd_##AT(av, batchi); \
        [[maybe_unused]] decltype(auto) ag = padg.up_##AT(av, batchi); \
        REVCODE; \
      } \
    } \
  });

#define emit_gradop1_simd(AT, RT, REVCODE) \
  she.gradop([=, rv1=rv, av1=args[0]](CpuState &cpu, Pad &pad, Pad &padg, auto batchdef) { \
    auto rv = batchdef.getVal(rv1); \
    auto av = batchdef.getVal(av1); \
    [[maybe_unused]] decltype(auto) r = pad.wr_##RT##_simd(rv, batchdef); \
    [[maybe_unused]] decltype(auto) rg = padg.rd_##RT##_simd(rv, batchdef); \
    [[maybe_unused]] decltype(auto) a = pad.rd_##AT##_simd(av, batchdef); \
    [[maybe_unused]] decltype(auto) ag = padg.up_##AT##_simd(av, batchdef); \
    REVCODE; \
  });

#define defop1(AT, RT, CODE) \
  if (argmatch1(AT)) { \
    auto rv = allocResult<RT>(); \
    emit_evalop1(AT, RT, CODE) \
    return rv; \
  }

#define defop1_simd(AT, RT, CODE) \
  if (argmatch1(AT)) { \
    auto rv = allocResult<RT>(); \
    emit_evalop1_simd(AT, RT, CODE) \
    return rv; \
  }

#define defop1g(AT, RT, CODE, REVCODE) \
  if (argmatch1(AT)) { \
    auto rv = allocResult<RT>(); \
    emit_evalop1(AT, RT, CODE) \
    emit_gradop1(AT, RT, REVCODE) \
    return rv; \
  }

#define defop1g_simd(AT, RT, CODE, REVCODE) \
  if (argmatch1(AT)) { \
    auto rv = allocResult<RT>(); \
    emit_evalop1_simd(AT, RT, CODE) \
    emit_gradop1_simd(AT, RT, REVCODE) \
    return rv; \
  }

#define defop1r(AT, RT, CHECK, ALLOC, CODE) \
  if (argmatch1(AT) && CHECK) { \
    auto rv = ALLOC; \
    emit_evalop1(AT, RT, CODE) \
    return rv; \
  }

#define defop1rg(AT, RT, CHECK, ALLOC, CODE, REVCODE) \
  if (argmatch1(AT) && CHECK) { \
    auto rv = ALLOC; \
    emit_evalop1(AT, RT, CODE) \
    emit_gradop1(AT, RT, REVCODE) \
    return rv; \
  }

#define defdelay1(STATET, STATEV, AT, RT, CODE) \
  if (argmatch1(AT)) { \
    auto rv = allocResult<RT>(); \
    auto statev = STATEV; \
    ce.hasPrev = true; \
    she.evalop([=, rv1=rv, av1=args[0], statev=statev](CpuState &cpu, Pad &pad, auto batchdef) { \
      auto rv = batchdef.getVal(rv1); \
      auto av = batchdef.getVal(av1); \
      for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
        decltype(auto) r = pad.wr_##RT(rv, batchi); \
        if (batchi < cpu.realBatchSize && cpu.curs[batchi] && cpu.prevs[batchi]) { \
          decltype(auto) sprev = cpu.prevs[batchi]->rd_##STATET##_unchecked(statev); \
          decltype(auto) snew = cpu.curs[batchi]->wr_##STATET##_unchecked(statev); \
          decltype(auto) a = pad.rd_##AT(av, batchi); \
          CODE; \
          emitlog(rv << "=" << r << " " #CODE << \
            " a=" << av << "=" << a << \
            " statev=" << statev << \
            " sprev=" << sprev << \
            " snew=" << snew); \
        } \
        else { \
          setZero(r); \
        } \
      } \
    }); \
    return rv; \
  }

#define defdelay1r(STATET, STATEV, AT, RT, CHECK, ALLOC, CODE) \
  if (argmatch1(AT) && CHECK) { \
    auto rv = ALLOC; \
    auto statev = STATEV; \
    ce.hasPrev = true; \
    she.evalop([=, rv1=rv, av1=args[0], statev=statev](CpuState &cpu, Pad &pad, auto batchdef) { \
      auto rv = batchdef.getVal(rv1); \
      auto av = batchdef.getVal(av1); \
      for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
        decltype(auto) r = pad.wr_##RT(rv, batchi); \
        if (batchi < cpu.realBatchSize && cpu.curs[batchi] && cpu.prevs[batchi]) { \
          decltype(auto) sprev = cpu.prevs[batchi]->rd_##STATET##_unchecked(statev); \
          decltype(auto) snew = cpu.curs[batchi]->wr_##STATET##_unchecked(statev); \
          decltype(auto) a = pad.rd_##AT(av, batchi); \
          CODE; \
          emitlog(rv << "=" << r << " " #CODE << \
            " a=" << av << "=" << a << \
            " sprev=" << statev << "=" << sprev << \
            " snew=" << snew); \
        } \
        else { \
          setZero(r); \
        } \
      } \
    }); \
    return rv; \
  }

/*
  2 args
*/

#define defold2(STATET, STATEV, AT, BT, RT, CODE) \
  if (argmatch2(AT, BT)) { \
    auto rv = allocResult<RT>(); \
    auto statev = STATEV; \
    ce.hasPrev = true; \
    she.evalop([=, rv1=rv, av1=args[0], bv1=args[1], statev=statev](CpuState &cpu, Pad &pad, auto batchdef) { \
      auto rv = batchdef.getVal(rv1); \
      auto av = batchdef.getVal(av1); \
      auto bv = batchdef.getVal(bv1); \
      for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
        decltype(auto) r = pad.wr_##RT(rv, batchi); \
        if (batchi < cpu.realBatchSize && cpu.curs[batchi] && cpu.prevs[batchi]) { \
          decltype(auto) snew = cpu.curs[batchi]->wr_##STATET##_unchecked(statev); \
          decltype(auto) a = pad.rd_##AT(av, batchi); \
          decltype(auto) b = pad.rd_##BT(bv, batchi); \
          CODE; \
          emitlog(rv << "=" << r << " " #CODE << \
            " a=" << av << "=" << a << \
            " snew=" << statev << "=" << snew); \
        } \
        else { \
          setZero(r); \
        } \
      } \
    }); \
    return rv; \
  }

#define defdelay2(STATET, STATEV, AT, BT, RT, CODE) \
  if (argmatch2(AT, BT)) { \
    auto rv = allocResult<RT>(); \
    auto statev = STATEV; \
    ce.hasPrev = true; \
    she.evalop([=, rv1=rv, av1=args[0], bv1=args[1], statev=statev](CpuState &cpu, Pad &pad, auto batchdef) { \
      auto rv = batchdef.getVal(rv1); \
      auto av = batchdef.getVal(av1); \
      auto bv = batchdef.getVal(bv1); \
      for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
        decltype(auto) r = pad.wr_##RT(rv, batchi); \
        if (batchi < cpu.realBatchSize && cpu.curs[batchi] && cpu.prevs[batchi]) { \
          decltype(auto) sprev = cpu.prevs[batchi]->rd_##STATET##_unchecked(statev); \
          decltype(auto) snew = cpu.curs[batchi]->wr_##STATET##_unchecked(statev); \
          decltype(auto) a = pad.rd_##AT(av, batchi); \
          decltype(auto) b = pad.rd_##BT(bv, batchi); \
          CODE; \
          emitlog(rv << "=" << r << " " #CODE << \
            " a=" << av << "=" << a << \
            " b=" << bv << "=" << b << \
            " sprev=" << statev << "=" << sprev << \
            " snew=" << snew); \
        } \
        else { \
          setZero(r); \
        } \
      } \
    }); \
    return rv; \
  }

#define defdelay2r(STATET, STATEV, AT, BT, RT, CHECK, ALLOC, CODE) \
  if (argmatch2(AT, BT) && CHECK) { \
    auto rv = ALLOC; \
    auto statev = STATEV; \
    ce.hasPrev = true; \
    she.evalop([=, rv1=rv, av1=args[0], bv1=args[1], statev=statev](CpuState &cpu, Pad &pad, auto batchdef) { \
      auto rv = batchdef.getVal(rv1); \
      auto av = batchdef.getVal(av1); \
      auto bv = batchdef.getVal(bv1); \
      for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
        decltype(auto) r = pad.wr_##RT(rv, batchi); \
        if (batchi < cpu.realBatchSize && cpu.curs[batchi] && cpu.prevs[batchi]) { \
          decltype(auto) sprev = cpu.prevs[batchi]->rd_##STATET##_unchecked(statev); \
          decltype(auto) snew = cpu.curs[batchi]->wr_##STATET##_unchecked(statev); \
          decltype(auto) a = pad.rd_##AT(av, batchi); \
          decltype(auto) b = pad.rd_##BT(bv, batchi); \
          CODE; \
          emitlog(rv << "=" << r << " " #CODE << \
            " a=" << av << "=" << a << \
            " b=" << bv << "=" << b << \
            " sprev=" << statev << "=" << sprev << \
            " snew=" << snew); \
        } \
        else { \
          setZero(r); \
        } \
      } \
    }); \
    return rv; \
  }

#define emit_evalop2(AT, BT, RT, CODE) \
  she.evalop([=, rv1=rv, av1=args[0], bv1=args[1]](CpuState &cpu, Pad &pad, auto batchdef) { \
    auto rv = batchdef.getVal(rv1); \
    auto av = batchdef.getVal(av1); \
    auto bv = batchdef.getVal(bv1); \
    for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
      decltype(auto) r = pad.wr_##RT(rv, batchi); \
      if (batchi < cpu.realBatchSize) { \
        decltype(auto) a = pad.rd_##AT(av, batchi); \
        decltype(auto) b = pad.rd_##BT(bv, batchi); \
        CODE; \
        emitlog(rv << "=" << r << " " #CODE << \
          " a=" << av << "=" << a << \
          " b=" << bv << "=" << b); \
      } else { \
        setZero(r); \
      } \
    } \
  });


#define emit_evalop2_simd(AT, BT, RT, CODE) \
  she.evalop([=, rv1=rv, av1=args[0], bv1=args[1]](CpuState &cpu, Pad &pad, auto batchdef) { \
    auto rv = batchdef.getVal(rv1); \
    auto av = batchdef.getVal(av1); \
    auto bv = batchdef.getVal(bv1); \
    decltype(auto) r = pad.wr_##RT##_simd(rv, batchdef); \
    decltype(auto) a = pad.rd_##AT##_simd(av, batchdef); \
    decltype(auto) b = pad.rd_##BT##_simd(bv, batchdef); \
    CODE; \
    emitlog_simd(rv << "=" << r << " " #CODE << \
      " a=" << batchdef.getVal(av) << "=" << a << \
      " b=" << batchdef.getVal(bv) << "=" << b); \
  });

#define emit_gradop2(AT, BT, RT, REVCODE) \
  she.gradop([rv1=rv, av1=args[0], bv1=args[1]](CpuState &cpu, Pad &pad, Pad &padg, auto batchdef) { \
    auto rv = batchdef.getVal(rv1); \
    auto av = batchdef.getVal(av1); \
    auto bv = batchdef.getVal(bv1); \
    for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
      if (batchi < cpu.realBatchSize) { \
        [[maybe_unused]] decltype(auto) r = pad.rd_##RT(rv, batchi); \
        [[maybe_unused]] decltype(auto) rg = padg.rd_##RT(rv, batchi); \
        [[maybe_unused]] decltype(auto) a = pad.rd_##AT(av, batchi); \
        [[maybe_unused]] decltype(auto) ag = padg.up_##AT(av, batchi); \
        [[maybe_unused]] decltype(auto) b = pad.rd_##BT(bv, batchi); \
        [[maybe_unused]] decltype(auto) bg = padg.up_##BT(bv, batchi); \
        REVCODE; \
      } \
    } \
  });

#define emit_gradop2_simd(AT, BT, RT, REVCODE) \
  she.gradop([rv1=rv, av1=args[0], bv1=args[1]](CpuState &cpu, Pad &pad, Pad &padg, auto batchdef) { \
    auto rv = batchdef.getVal(rv1); \
    auto av = batchdef.getVal(av1); \
    auto bv = batchdef.getVal(bv1); \
    [[maybe_unused]] decltype(auto) r = pad.rd_##RT##_simd(rv, batchdef); \
    [[maybe_unused]] decltype(auto) rg = padg.rd_##RT##_simd(rv, batchdef); \
    [[maybe_unused]] decltype(auto) a = pad.rd_##AT##_simd(av, batchdef); \
    [[maybe_unused]] decltype(auto) ag = padg.up_##AT##_simd(av, batchdef); \
    [[maybe_unused]] decltype(auto) b = pad.rd_##BT##_simd(bv, batchdef); \
    [[maybe_unused]] decltype(auto) bg = padg.up_##BT##_simd(bv, batchdef); \
    REVCODE; \
  });

#define defop2(AT, BT, RT, CODE) \
  if (argmatch2(AT, BT)) { \
    auto rv = allocResult<RT>(); \
    emit_evalop2(AT, BT, RT, CODE) \
    return rv; \
  }

#define defop2_simd(AT, BT, RT, CODE) \
  if (argmatch2(AT, BT)) { \
    auto rv = allocResult<RT>(); \
    emit_evalop2_simd(AT, BT, RT, CODE) \
    return rv; \
  }

#define defop2r(AT, BT, RT, CHECK, ALLOC, CODE) \
  if (argmatch2(AT, BT) && CHECK) { \
    auto rv = ALLOC; \
    emit_evalop2(AT, BT, RT, CODE) \
    return rv; \
  }

#define defop2rg(AT, BT, RT, CHECK, ALLOC, CODE, REVCODE) \
  if (argmatch2(AT, BT) && CHECK) { \
    auto rv = ALLOC; \
    emit_evalop2(AT, BT, RT, CODE) \
    emit_gradop2(AT, BT, RT, REVCODE) \
    return rv; \
  }

// Apply an op to 2 arguments, but return the first arg
#define defop2a(AT, BT, CODE) \
  if (argmatch2(AT, BT)) { \
    she.evalop([=, av1=args[0], bv1=args[1]](CpuState &cpu, Pad &pad, auto batchdef) { \
      auto av = batchdef.getVal(av1); \
      auto bv = batchdef.getVal(bv1); \
      for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
        decltype(auto) a = pad.rd_##AT(av, batchi); \
        if (batchi < cpu.realBatchSize) { \
          decltype(auto) b = pad.rd_##BT(bv, batchi); \
          CODE; \
        } \
      } \
    }); \
    return args[0]; \
  }

#define defop2g(AT, BT, RT, CODE, REVCODE) \
  if (argmatch2(AT, BT)) { \
    auto rv = allocResult<RT>(); \
    emit_evalop2(AT, BT, RT, CODE) \
    emit_gradop2(AT, BT, RT, REVCODE) \
    return rv; \
  }

#define defop2g_simd(AT, BT, RT, CODE, REVCODE) \
  if (argmatch2(AT, BT)) { \
    auto rv = allocResult<RT>(); \
    emit_evalop2_simd(AT, BT, RT, CODE) \
    emit_gradop2_simd(AT, BT, RT, REVCODE) \
    return rv; \
  }

#define defsubop2(AT, AV, BT, BV, RT, RV, CODE) \
  if (AV.template ist<AT>() && BV.template ist<BT>()) { \
    RV = allocPad<RT>(); \
    she.evalop([rv1=RV, av1=AV, bv1=BV](CpuState &cpu, Pad &pad, auto batchdef) { \
      auto rv = batchdef.getVal(rv1); \
      auto av = batchdef.getVal(av1); \
      auto bv = batchdef.getVal(bv1); \
      for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
        decltype(auto) r = pad.wr_##RT(rv, batchi); \
        if (batchi < cpu.realBatchSize) { \
          decltype(auto) a = pad.rd_##AT(av, batchi); \
          decltype(auto) b = pad.rd_##BT(bv, batchi); \
          CODE; \
        } else { \
          setZero(r); \
        } \
      } \
    }); \
    break; \
  }

/*
  3 args
*/

#define emit_evalop3(AT, BT, CT, RT, CODE) \
  she.evalop([=, rv1=rv, av1=args[0], bv1=args[1], cv1=args[2]](CpuState &cpu, Pad &pad, auto batchdef) { \
    auto rv = batchdef.getVal(rv1); \
    auto av = batchdef.getVal(av1); \
    auto bv = batchdef.getVal(bv1); \
    auto cv = batchdef.getVal(cv1); \
    for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
      decltype(auto) r = pad.wr_##RT(rv, batchi); \
      if (batchi < cpu.realBatchSize) { \
        decltype(auto) a = pad.rd_##AT(av, batchi); \
        decltype(auto) b = pad.rd_##BT(bv, batchi); \
        decltype(auto) c = pad.rd_##CT(cv, batchi); \
        CODE; \
      } else { \
        setZero(r); \
      } \
    } \
  });

#define emit_evalop3_simd(AT, BT, CT, RT, CODE) \
  she.evalop([=, rv1=rv, av1=args[0], bv1=args[1], cv1=args[2]](CpuState &cpu, Pad &pad, auto batchdef) { \
    auto rv = batchdef.getVal(rv1); \
    auto av = batchdef.getVal(av1); \
    auto bv = batchdef.getVal(bv1); \
    auto cv = batchdef.getVal(cv1); \
    decltype(auto) r = pad.wr_##RT##_simd(rv, batchdef); \
    decltype(auto) a = pad.rd_##AT##_simd(av, batchdef); \
    decltype(auto) b = pad.rd_##BT##_simd(bv, batchdef); \
    decltype(auto) c = pad.rd_##CT##_simd(cv, batchdef); \
    CODE; \
  });

#define emit_gradop3(AT, BT, CT, RT, REVCODE) \
  she.gradop([=, rv1=rv, av1=args[0], bv1=args[1], cv1=args[2]](CpuState &cpu, Pad &pad, Pad &padg, auto batchdef) { \
    auto rv = batchdef.getVal(rv1); \
    auto av = batchdef.getVal(av1); \
    auto bv = batchdef.getVal(bv1); \
    auto cv = batchdef.getVal(cv1); \
    for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
      if (batchi < cpu.realBatchSize) { \
        [[maybe_unused]] decltype(auto) r = pad.rd_##RT(rv, batchi); \
        [[maybe_unused]] decltype(auto) rg = padg.rd_##RT(rv, batchi); \
        [[maybe_unused]] decltype(auto) a = pad.rd_##AT(av, batchi); \
        [[maybe_unused]] decltype(auto) ag = padg.up_##AT(av, batchi); \
        [[maybe_unused]] decltype(auto) b = pad.rd_##BT(bv, batchi); \
        [[maybe_unused]] decltype(auto) bg = padg.up_##BT(bv, batchi); \
        [[maybe_unused]] decltype(auto) c = pad.rd_##CT(cv, batchi); \
        [[maybe_unused]] decltype(auto) cg = padg.up_##CT(cv, batchi); \
        REVCODE; \
      } \
    } \
  });

#define defop3(AT, BT, CT, RT, CODE) \
  if (argmatch3(AT, BT, CT)) { \
    auto rv = allocResult<RT>(); \
    emit_evalop3(AT, BT, CT, RT, CODE) \
    return rv; \
  }

#define defop3_simd(AT, BT, CT, RT, CODE) \
  if (argmatch3(AT, BT, CT)) { \
    auto rv = allocResult<RT>(); \
    emit_evalop3_simd(AT, BT, CT, RT, CODE) \
    return rv; \
  }

#define defop3rg(AT, BT, CT, RT, CHECK, ALLOC, CODE, REVCODE) \
  if (argmatch3(AT, BT, CT) && CHECK) { \
    auto rv = ALLOC; \
    emit_evalop3(AT, BT, CT, RT, CODE) \
    emit_gradop3(AT, BT, CT, RT, REVCODE) \
    return rv; \
  }

#define defop3r(AT, BT, CT, RT, CHECK, ALLOC, CODE) \
  if (argmatch3(AT, BT, CT) && CHECK) { \
    auto rv = ALLOC; \
    emit_evalop3(AT, BT, CT, RT, CODE) \
    return rv; \
  }

#define defop3g(AT, BT, CT, RT, CODE, REVCODE) \
  if (argmatch3(AT, BT, CT)) { \
    auto rv = allocResult<RT>(); \
    emit_evalop3(AT, BT, CT, RT, CODE) \
    emit_gradop3(AT, BT, CT, RT, REVCODE) \
    return rv; \
  }

#define defdelay3(STATET, STATEV, AT, BT, CT, RT, CODE) \
  if (argmatch3(AT, BT, CT)) { \
    auto rv = allocResult<RT>(); \
    auto statev = STATEV; \
    ce.hasPrev = true; \
    she.evalop([=, rv1=rv, av1=args[0], bv1=args[1], cv1=args[2], statev=statev](CpuState &cpu, Pad &pad, auto batchdef) { \
      auto rv = batchdef.getVal(rv1); \
      auto av = batchdef.getVal(av1); \
      auto bv = batchdef.getVal(bv1); \
      auto cv = batchdef.getVal(cv1); \
      for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
        decltype(auto) r = pad.wr_##RT(rv, batchi); \
        if (batchi < cpu.realBatchSize && cpu.curs[batchi] && cpu.prevs[batchi]) { \
          decltype(auto) sprev = cpu.prevs[batchi]->rd_##STATET##_unchecked(statev); \
          decltype(auto) snew = cpu.curs[batchi]->wr_##STATET##_unchecked(statev); \
          decltype(auto) a = pad.rd_##AT(av, batchi); \
          decltype(auto) b = pad.rd_##BT(bv, batchi); \
          decltype(auto) c = pad.rd_##CT(cv, batchi); \
          CODE; \
        } \
        else { \
          setZero(r); \
        } \
      } \
    }); \
    return rv; \
  }

/*
  4 args
*/

#define emit_evalop4(AT, BT, CT, DT, RT, CODE) \
  she.evalop([=, rv1=rv, av1=args[0], bv1=args[1], cv1=args[2], dv1=args[3]](CpuState &cpu, Pad &pad, auto batchdef) { \
    auto rv = batchdef.getVal(rv1); \
    auto av = batchdef.getVal(av1); \
    auto bv = batchdef.getVal(bv1); \
    auto cv = batchdef.getVal(cv1); \
    auto dv = batchdef.getVal(dv1); \
    for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
      decltype(auto) r = pad.wr_##RT(rv, batchi); \
      if (batchi < cpu.realBatchSize) { \
        decltype(auto) a = pad.rd_##AT(av, batchi); \
        decltype(auto) b = pad.rd_##BT(bv, batchi); \
        decltype(auto) c = pad.rd_##CT(cv, batchi); \
        decltype(auto) d = pad.rd_##DT(dv, batchi); \
        CODE; \
      } else { \
        setZero(r); \
      } \
    } \
  });

#define defop4(AT, BT, CT, DT, RT, CODE) \
  if (argmatch4(AT, BT, CT, DT)) { \
    auto rv = allocResult<RT>(); \
    emit_evalop4(AT, BT, CT, DT, RT, CODE) \
    return rv; \
  }

#define defdelay4(STATET, STATEV, AT, BT, CT, DT, RT, CODE) \
  if (argmatch4(AT, BT, CT, DT)) { \
    auto rv = allocResult<RT>(); \
    auto statev = STATEV; \
    ce.hasPrev = true; \
    she.evalop([=, rv1=rv, av1=args[0], bv1=args[1], cv1=args[2], dv1=args[3], statev=statev](CpuState &cpu, Pad &pad, auto batchdef) { \
      auto rv = batchdef.getVal(rv1); \
      auto av = batchdef.getVal(av1); \
      auto bv = batchdef.getVal(bv1); \
      auto cv = batchdef.getVal(cv1); \
      auto dv = batchdef.getVal(dv1); \
      for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
        decltype(auto) r = pad.wr_##RT(rv, batchi); \
        if (batchi < cpu.realBatchSize && cpu.curs[batchi] && cpu.prevs[batchi]) { \
          decltype(auto) sprev = cpu.prevs[batchi]->rd_##STATET##_unchecked(statev); \
          decltype(auto) snew = cpu.curs[batchi]->wr_##STATET##_unchecked(statev); \
          decltype(auto) a = pad.rd_##AT(av, batchi); \
          decltype(auto) b = pad.rd_##BT(bv, batchi); \
          decltype(auto) c = pad.rd_##CT(cv, batchi); \
          decltype(auto) d = pad.rd_##DT(dv, batchi); \
          CODE; \
          emitlog(rv << "=" << r << " " #CODE << \
            " a=" << av << "=" << a << \
            " b=" << bv << "=" << b << \
            " c=" << cv << "=" << c << \
            " d=" << dv << "=" << d << \
            " sprev=" << statev << "=" << sprev << \
            " snew=" << snew); \
        } \
        else { \
          setZero(r); \
        } \
      } \
    }); \
    return rv; \
  }

/*
  5 args
*/

#define emit_evalop5(AT, BT, CT, DT, ET, RT, CODE) \
  she.evalop([=, rv1=rv, av1=args[0], bv1=args[1], cv1=args[2], dv1=args[3], ev1=args[4]](CpuState &cpu, Pad &pad, auto batchdef) { \
    auto rv = batchdef.getVal(rv1); \
    auto av = batchdef.getVal(av1); \
    auto bv = batchdef.getVal(bv1); \
    auto cv = batchdef.getVal(cv1); \
    auto dv = batchdef.getVal(dv1); \
    auto ev = batchdef.getVal(ev1); \
    for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
      decltype(auto) r = pad.wr_##RT(rv, batchi); \
      if (batchi < cpu.realBatchSize) { \
        decltype(auto) a = pad.rd_##AT(av, batchi); \
        decltype(auto) b = pad.rd_##BT(bv, batchi); \
        decltype(auto) c = pad.rd_##CT(cv, batchi); \
        decltype(auto) d = pad.rd_##DT(dv, batchi); \
        decltype(auto) e = pad.rd_##ET(ev, batchi); \
        CODE; \
      } else { \
        setZero(r); \
      } \
    } \
  });

#define defop5(AT, BT, CT, DT, ET, RT, CODE) \
  if (argmatch5(AT, BT, CT, DT, ET)) { \
    auto rv = allocResult<RT>(); \
    emit_evalop5(AT, BT, CT, DT, ET, RT, CODE) \
    return rv; \
  }

/*
  6 args
*/

#define emit_evalop6(AT, BT, CT, DT, ET, FT, RT, CODE) \
  she.evalop([=, rv1=rv, av1=args[0], bv1=args[1], cv1=args[2], dv1=args[3], ev1=args[4], fv1=args[5]](CpuState &cpu, Pad &pad, auto batchdef) { \
    auto rv = batchdef.getVal(rv1); \
    auto av = batchdef.getVal(av1); \
    auto bv = batchdef.getVal(bv1); \
    auto cv = batchdef.getVal(cv1); \
    auto dv = batchdef.getVal(dv1); \
    auto ev = batchdef.getVal(ev1); \
    auto fv = batchdef.getVal(fv1); \
    for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
      decltype(auto) r = pad.wr_##RT(rv, batchi); \
      if (batchi < cpu.realBatchSize) { \
        decltype(auto) a = pad.rd_##AT(av, batchi); \
        decltype(auto) b = pad.rd_##BT(bv, batchi); \
        decltype(auto) c = pad.rd_##CT(cv, batchi); \
        decltype(auto) d = pad.rd_##DT(dv, batchi); \
        decltype(auto) e = pad.rd_##ET(ev, batchi); \
        decltype(auto) f = pad.rd_##FT(fv, batchi); \
        CODE; \
      } else { \
        setZero(r); \
      } \
    } \
  });

#define defop6(AT, BT, CT, DT, ET, FT, RT, CODE) \
  if (argmatch6(AT, BT, CT, DT, ET, FT)) { \
    auto rv = allocResult<RT>(); \
    emit_evalop6(AT, BT, CT, DT, ET, FT, RT, CODE) \
    return rv; \
  }
