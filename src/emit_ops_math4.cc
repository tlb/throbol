#include "./emit.h"
#include "./emit_macros.h"


deftype(compeq)
{
  auto at = eval(n->ch(0));
  auto bt = eval(n->ch(1));
  if (at == bt) return at;
  return setError();
}

defemit(compeq)
{
  defop2_simd(F, F, F, r = (a.array() == b.array()).template cast<F>());
  defop2_simd(CF, CF, CF, r = (a.array() == b.array()).template cast<F>());

  defop2r(mat, mat, mat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_mat, args[0].t.nr, args[0].t.nc}),
    r.array() = (a.array() == b.array()).template cast<F>());
  defop2r(cmat, cmat, cmat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_cmat, args[0].t.nr, args[0].t.nc}),
    r.array() = (a.array() == b.array()).template cast<F>());

  return setInvalidArgs();
}

defusage(compeq, R"(
  T .== T)");



deftype(compneq)
{
  auto at = eval(n->ch(0));
  auto bt = eval(n->ch(1));
  if (at == bt) return at;
  return setError();
}

defemit(compneq)
{
  defop2_simd(F, F, F, r.array() = (a.array() != b.array()).template cast<F>());
  defop2_simd(CF, CF, CF, r.array() = (a.array() != b.array()).template cast<F>());

  defop2r(mat, mat, mat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_mat, args[0].t.nr, args[0].t.nc}),
    r.array() = (a.array() != b.array()).template cast<F>());
  defop2r(cmat, cmat, cmat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_cmat, args[0].t.nr, args[0].t.nc}),
    r.array() = (a.array() != b.array()).template cast<F>());

  return setInvalidArgs();
}

defusage(compneq, R"(
  T .!= T)");



deftype(compge)
{
  auto at = eval(n->ch(0));
  auto bt = eval(n->ch(1));
  if (at == bt) return at;
  return setError();
}

defemit(compge)
{
  defop2_simd(F, F, F, r = (a.array() >= b.array()).template cast<F>());

  defop2r(mat, mat, mat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_mat, args[0].t.nr, args[0].t.nc}),
    r.array() = (a.array() >= b.array()).template cast<F>());

  return setInvalidArgs();
}

defusage(compge, R"(
  T .>= T)");



deftype(compgt)
{
  auto at = eval(n->ch(0));
  auto bt = eval(n->ch(1));
  if (at == bt) return at;
  return setError();
}

defemit(compgt)
{
  defop2_simd(F, F, F, r = (a.array() > b.array()).template cast<F>());

  defop2r(mat, mat, mat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_mat, args[0].t.nr, args[0].t.nc}),
    r.array() = (a.array() > b.array()).template cast<F>());

  return setInvalidArgs();
}

defusage(compgt, R"(
  T .> T)");


deftype(comple)
{
  auto at = eval(n->ch(0));
  auto bt = eval(n->ch(1));
  if (at == bt) return at;
  return setError();
}

defemit(comple)
{
  defop2_simd(F, F, F, r = (a.array() <= b.array()).template cast<F>());

  defop2r(mat, mat, mat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_mat, args[0].t.nr, args[0].t.nc}),
    r.array() = (a.array() <= b.array()).template cast<F>());
  return setInvalidArgs();
}

defusage(comple, R"(
  T .<= T)");


deftype(complt)
{
  auto at = eval(n->ch(0));
  auto bt = eval(n->ch(1));
  if (at == bt) return at;
  return setError();
}

defemit(complt)
{
  defop2_simd(F, F, F, r = (a.array() < b.array()).template cast<F>());

  defop2r(mat, mat, mat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_mat, args[0].t.nr, args[0].t.nc}),
    r.array() = (a.array() < b.array()).template cast<F>());

  return setInvalidArgs();
}

defusage(complt, R"(
  T .< T)");


