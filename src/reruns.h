#pragma once
#include "./defs.h"

string getNewTraceName(R startTime, U64 randomSeed = 0);

struct RerunResult {

  string traceName;
  shared_ptr<ReplayBuffer> replayBuffer;
  vector<pair<R, shared_ptr<Trace>>> traceBySeed_;

  atomic<int> running = 0;
  bool enabled = false, failed = false;
  string failMsg;
  string convNotes;

  RerunResult(string const &_traceName) : traceName(_traceName) {}

  Trace *getTraceBySeed(int i) const {
    if (i >= 0 && i < int(traceBySeed_.size())) return traceBySeed_[i].second.get();
    return nullptr;
  }

  bool isPure() const {
    return traceName.empty();
  }
};

struct RerunSet {

  Sheet &sh;
  bool needInsertFromDb = true;
  vector<shared_ptr<RerunResult>> reruns;
  shared_ptr<RerunResult> selectedRerun;
  shared_ptr<RerunResult> pureRerun;
  int selectedSeed = 0;
  bool showAllSeeds = false, showPure = false;
  int numSeeds = 1;

  RerunSet(Sheet &_sh);
  ~RerunSet() = default;

  void select(shared_ptr<RerunResult> rerun, int seed = 0);
  void selectPrevSeed();
  void selectNextSeed();
  void toggleShowAllSeeds();
  void selectPureSheet();

  shared_ptr<Trace> displayTrace();
  char const *traceObsolete(Trace &trace);
  void update();
  void insertFromDb();
  void remove(shared_ptr<RerunResult> rerun);
  void add(shared_ptr<ReplayBuffer> replay, vector<BlobRef> const &chunkList);
  shared_ptr<RerunResult> byName(string const &name);
  void load(RerunResult &rerun);
  void save(string const &traceName, ReplayBuffer &replayBuffer, vector<BlobRef> const &chunkList);

};

