#include "./emit.h"
#include "./emit_macros.h"

deftype(compadd)
{
  auto at = eval(n->ch(0));
  auto bt = eval(n->ch(1));
  if (at == bt) return at;
  return setError();
}

defemit(compadd)
{
  defop2g_simd(F, F, F, r = a + b, (ag += rg, bg += rg));
  defop2g_simd(F, CF, CF, r = a + b, (ag += rg.real(), bg += rg));
  defop2g_simd(CF, F, CF, r = a + b, (ag += rg, bg += rg.real()));
  defop2g_simd(CF, CF, CF, r = a + b, (ag += rg, bg += rg));

  defop2g(vec2, vec2, vec2, r = a + b, (ag += rg, bg += rg));
  defop2g(vec3, vec3, vec3, r = a + b, (ag += rg, bg += rg));
  defop2g(vec4, vec4, vec4, r = a + b, (ag += rg, bg += rg));
  defop2g(mat2, mat2, mat2, r = a + b, (ag += rg, bg += rg));
  defop2g(mat3, mat3, mat3, r = a + b, (ag += rg, bg += rg));
  defop2g(mat4, mat4, mat4, r = a + b, (ag += rg, bg += rg));

  defop2rg(mat, mat, mat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_mat, args[0].t.nr, args[0].t.nc}),
    r.array() = a.array() + b.array(),
    (ag.array() += rg.array(), bg.array() += rg.array()));
  defop2rg(cmat, cmat, cmat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_cmat, args[0].t.nr, args[0].t.nc}),
    r.array() = a.array() + b.array(),
    (ag.array() += rg.array(), bg.array() += rg.array()));

  return setInvalidArgs();
}

defusage(compadd, R"(
  T .+ T)");


deftype(compmul)
{
  auto at = eval(n->ch(0));
  auto bt = eval(n->ch(1));
  if (at == bt) return at;
  if (at.ist<F>() && bt.ist<CF>()) return bt;
  if (at.ist<CF>() && bt.ist<F>()) return at;
  if (at.ist<mat>() && bt.ist<cmat>()) return bt;
  if (at.ist<cmat>() && bt.ist<mat>()) return at;
  return setError();
}

defemit(compmul)
{
  defop2_simd(F, F, F, r = a * b);
  defop2_simd(F, CF, CF, r = a * b);
  defop2_simd(CF, F, CF, r = a * b);
  defop2_simd(CF, CF, CF, r = a * b);

  defop2(vec2, vec2, vec2, r = a.array() * b.array());
  defop2(vec3, vec3, vec3, r = a.array() * b.array());
  defop2(vec4, vec4, vec4, r = a.array() * b.array());
  defop2(mat2, mat2, mat2, r = a.array() * b.array());
  defop2(mat3, mat3, mat3, r = a.array() * b.array());
  defop2(mat4, mat4, mat4, r = a.array() * b.array());
  
  defop2rg(mat, mat, mat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_mat, args[0].t.nr, args[0].t.nc}),
    r = a.array() * b.array(),
    (ag.array() += rg.array() * b.array(), bg.array() += a.array() * rg.array()));
  defop2rg(cmat, cmat, cmat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_cmat, args[0].t.nr, args[0].t.nc}),
    r = a.array() * b.array(),
    (ag.array() += rg.array() * b.array(), bg.array() += a.array() * rg.array()));
  defop2rg(mat, cmat, cmat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_cmat, args[0].t.nr, args[0].t.nc}),
    r = a.array() * b.array(),
    (ag.array() += (rg.array() * b.array()).real(), bg.array() += a.array() * rg.array()));
  defop2rg(cmat, mat, cmat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_cmat, args[0].t.nr, args[0].t.nc}),
    r = a.array() * b.array(),
    (ag.array() += (rg.array() * b.array()), bg.array() += (a.array() * rg.array()).real()));

  return setInvalidArgs();
}

defusage(compmul, R"(
  T .* T)");

