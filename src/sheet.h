#pragma once
#include "./defs.h"
#include "./yamlio.h"
#include "./params.h"
#include "./annotation.h"

struct SheetUndo;
struct DrawInfo;
struct SheetApi;
struct CellTopology;
struct CompiledSheetFat;
namespace sqlite { class database; }

enum SaveSheetMode {
  SaveSheet_normal,
  SaveSheet_precrash,
  SaveSheet_autosave
};

struct Sheet : AllocTrackingMixin<Sheet> {

  vector<string> fileNameByPage;
  vector<string> includeNameByPage;
  vector<YAML::Node> yamlByPage;
  vector<vec2> includePosByPage;
  vector<PageIndex> includeParentByPage;
  string replayFileName;
  /*
    When loading foo.tb, this is opened from foo.replays
  */
  shared_ptr<sqlite::database> traceDb;

  CellIndex nCells = 0;
  ParamIndex nParams = 0;
  PageIndex nPages = 0;
  Symbol nSymbols = 0;

  bool doDebug = false;
  bool ok = true;
  int verbose = 0;
  
  Arena sheetlife;

  CellSet liveCells;
  // Everything we know about cells, in a struct-of-arrays style indexed by a CellIndex (a U32)
  vector<string> nameByCell;
  vector<string> formulaByCell;
  vector<string> visualByCell;
  vector<PageIndex> pageByCell;
  vector<vec2> posByCell;
  vector<AstNode *> nameAstByCell;
  vector<vector<Annotation>> annosByCell;
  unordered_map<string, CellIndex> cellByName;
  vector<vector<ParamIndex>> paramsByCell;

  CellSet needParseCells;
  CellSet needFlowCells;
  bool needsEmit = false;

  // set by compile
  vector<AstNode *> astByCell;
  shared_ptr<CompiledSheet> compiled;

  vector<string> parseErrorByCell;
  vector<FormulaSubstr> parseErrorLocByCell;

  vector<DrawInfo> drawInfoByCell;

  vector<string> nameBySymbol;
  unordered_map<string, Symbol> symbolByName;

  // Params
  //CellSet liveParams;
  vector<CellIndex> cellByParam;
  Paramset params;
  vector<F> rangeByParam;
  vector<DistType> distByParam;
  vector<FormulaSubstr> valueLocByParam; // byte location within the cell at cellByParam
  vector<FormulaSubstr> rangeLocByParam;

  // Updated by compile > setDeps
  vector<vector<CellIndex>> revDepsByCell;
  vector<vector<CellIndex>> fwdDepsByCell;

  shared_ptr<CellTopology> cellTopology;

  size_t undoLogPos = 0;
  vector<shared_ptr<SheetUndo>> undoLog;

  Sheet(); // in sheet.cc
  ~Sheet();

  Sheet(Sheet const &other) = delete;
  Sheet(Sheet &&other) noexcept = default;
  Sheet & operator= (Sheet &&other) = default;

  bool load(vector<string> const &fn);
  bool loadTest(char const *dump, char const *fn = "dummy");

  struct IncludeRec {
    string includeFnAbs;
    vec2 includePos;
    PageIndex includeParent;
  };
  bool loadPage(string const &fn, vec2 const &includePos, PageIndex includeParent, deque<IncludeRec> &moreFiles);
  bool undumpPage(string const &pageData, PageIndex page, deque<IncludeRec> &moreFiles, string const &fn);
  bool undumpPage(string const &pageData, PageIndex page);
  void dumpPage(YAML::Emitter &e, PageIndex page);
  PageIndex mkPage(string const &fn, vec2 const &includePos, PageIndex includeParent);
  vector<tuple<CellIndex, vec2>> getSortedCells(PageIndex page);
  void updatePagesYamlFromSheet();
  void updateSheetFromPagesYaml();

  void updateCellTopology();
  shared_ptr<CellTopology> getCellTopology();

  void saveSheet(SaveSheetMode mode = SaveSheet_normal);

  void openTraceDb(string const &fn);
  sqlite::database &getTraceDb();

  void draw(G8DrawList &dl);

  vector<BoxedRef> refsByCell(CellIndex cell) const;

  string getCellName(CellIndex cell) const;
  CellIndex getCell(string const &name) const;
  static bool isReservedCellName(string const &name);
  CellIndex mkCell(string const &name);
  CellIndex mkCellUndoable(string const &name);
  void killCell(CellIndex cell);
  void killCellUndoable(CellIndex cell);
  void resizeCellArrays();
  void resizeParamsArrays();

  Symbol mkSymbol(string const &name);

  TypeTag getCellType(CellIndex cell) const;
  bool getCellIsInput(CellIndex cell) const;
  bool getCellIsPureParam(CellIndex cell) const;
  bool getCellIsTimeInvariant(CellIndex cell) const;
  bool getCellIsApi(CellIndex cell) const;
  pair<string_view, FormulaSubstr> getCellError(CellIndex cell) const;

  ParamIndex mkParam(CellIndex cell);
  string getParamLiteral(ParamIndex pi) const;
  string getParamDesc(ParamIndex pi) const;
  void updateFormulaFromParams(CellIndex cell);
  void propagateParamUpdate(ParamIndex pi);

  CellIndex findClosest(F px, F py) const;
  void setFormula(CellIndex cell, string const &formula);
  void setFormulaUndoable(CellIndex cell, string const &value);
  void setCellPos(CellIndex cell, vec2 pos);
  void setCellPage(CellIndex cell, PageIndex page);
  void setIncludePagePos(PageIndex page, vec2 newPos, bool withoutCels);
  void snapToGrid();
  void moveCellsUndoable(CellSet const &cells, vec2 delta);
  void moveIncludePagesUndoable(vector<PageIndex> pages, vec2 delta, bool withoutCells);
  void setCellVisual(CellIndex cell, string visual);
  void setCellDrawInfo(CellIndex cell, DrawInfo drawInfo);
  void setDeps(CellIndex cell, vector<CellIndex> newRevDeps);
  void setParamValue(ParamIndex pi, F value);
  void setParamRange(ParamIndex pi, F value);
  void setParamValueUndoable(ParamIndex pi, F value);
  void setParamValuesUndoable(vector<ParamIndex> const &pis, vector<F> const &values);
  void setNeedsParseAll();


  vector<string> errors();
  void compile(EmitMode emitMode = Emit_standalone);
  void parseAll();
  void flowAll();
  void linearizeAll();
  void typeAll();
  void emitAll(EmitMode emitMode);

  YAML::Node getUiSaveState();

  void pushUndo(shared_ptr<SheetUndo> &&it);
  template<typename T, typename ... Args> T *mkUndo(Args && ...args);
  void undo();
  void redo();
  SheetUndo *getCurrentUndo();
  void finalizeCurrentUndo();

};

ostream & operator <<(ostream &s, Sheet &sh);

struct SheetUndo {

  bool nomore = false;

  SheetUndo() {}
  virtual ~SheetUndo() {}

  virtual void undo(Sheet &sh) {}
  virtual void redo(Sheet &sh) {}
  virtual void print(ostream &s, Sheet &sh) {}
  virtual bool domoreSetParamValue(Sheet &, vector<ParamIndex> const &_pis, vector<F> const &_nexts) { return false; }
  virtual bool domoreMoveCells(Sheet &sh, CellSet const &_cells, vec2 _delta) { return false; }
  virtual bool domoreMoveIncludePages(Sheet &sh, vector<PageIndex> const &_pages, vec2 _delta, bool _withoutCells) { return false; }
  virtual bool domoreSetFormula(Sheet &sh, CellIndex cell, string const &formula) { return false; }
};

struct CellTopology {

  vector<CellIndex> totalOrder;
  vector<int> rankByCell;
  vector<int> vindexByCell;
  int nRank = 0, nVindex = 0;

  CellTopology(CellIndex _nCells); // in sheet.cc
  ~CellTopology() = default;

};
