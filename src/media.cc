#include "./celldata.h"
#include "./sheet.h"

ostream & operator << (ostream &s, AudioFrame const &a)
{
  return s << 
    "AudioFrame(format=" << a.format <<
    " sampleRate=" << a.sampleRate <<
    " sampleCount=" << a.sampleCount << " blob=" << a.blobRef << ")";
}

VideoFrame operator * (mat4 const &a, VideoFrame const &b)
{
  return VideoFrame(a * b.orientation, b.format, b.width, b.height, b.blobRef);
}


ostream & operator << (ostream &s, VideoFrame const &a)
{
  return s << 
    "VideoFrame(format=" << a.format <<
    " size=" << a.width << "x" << a.height <<
    " orientation=" << a.orientation << " blob=" << a.blobRef << ")";
}

ostream & operator << (ostream &s, VideoFrameFormat const &a)
{
  switch (a) {
    case VFF_NONE: return s << "none";
    case VFF_JPEG: return s << "jpeg";
    case VFF_RGB: return s << "rgb";
    case VFF_DEPTH16: return s << "depth16";
    default: return s << "?";
  }
}

ostream & operator << (ostream &s, AudioFrameFormat const &a)
{
  switch (a) {
    case AFF_S16: return s << "s16";
    default: return s << "?";
  }
}
