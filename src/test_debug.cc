#include "./test_utils.h"

TEST_CASE("Debug data correct", "[sheet]")
{
  Sheet sh;
  REQUIRE(mkSheetCheck(sh, R"(
bar: foo - 1
foo: scalar(bar[-dt])
)", "test1"));

  auto foo = sh.getCell("foo");
  auto bar = sh.getCell("bar");

  L() << sh.compiled->debugsByCell;
  auto batchSize = 1; // always?

  CHECK(repr(sh.astByCell[bar]) == "formula[sub[cellref.i=1, number.f=1]]");
  CHECK(repr(sh.astByCell[foo]) == "formula[typecast.tt=F[index[cellref.i=0, neg[dt]]]]");

  CHECK(repr(sh.compiled->debugsByCell[bar]) == 
    "[cellref@0:3=F(ofs=" + 
    repr(4 * batchSize) + ") sub@4:5=F(ofs=" + repr(12 * batchSize) + 
    ") number@6:7=F(ofs=" + repr(8 * batchSize) + ")]");
  CHECK(repr(sh.compiled->debugsByCell[foo]) == "[index@7:13=F(ofs=0)]");
}
