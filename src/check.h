#pragma once
#include "./defs.h"

enum FailureCountType {
  FailureCount_numCells,
  FailureCount_time,
};

ostream &operator <<(ostream &s, FailureCountType const &a);


enum CheckStatus {
  Check_notnow,
  Check_pass,
  Check_fail,
};

ostream &operator <<(ostream &s, CheckStatus const &a);

enum CheckType {
  CheckType_range,
  CheckType_compare,
  CheckType_and,
  CheckType_or,
  CheckType_not,
  CheckType_at,
  CheckType_between,
  CheckType_bool
};

ostream &operator <<(ostream &s, CheckType const &a);


struct Check {

  CheckStatus status;
  CheckType t;

  Check(CheckStatus _status, CheckType _t) : status(_status), t(_t) {}

  template<typename T> bool ist() {
    return t == T::tag;
  }
  template<typename T> T * ast() {
    if (t == T::tag) return (T *)this;
    return nullptr;
  }
};

struct CheckRange : Check {

  static constexpr CheckType tag = CheckType_range;

  F val, lo, hi;

  CheckRange(CheckStatus _status, F _val, F _lo, F _hi) : Check(_status, tag), val(_val), lo(_lo), hi(_hi) {}
};

struct CheckCompare : Check {

  static constexpr CheckType tag = CheckType_compare;

  AstType pred;
  F lhs, rhs;

  CheckCompare(CheckStatus _status, AstType _pred, F _lhs, F _rhs) : Check(_status, tag), pred(_pred), lhs(_lhs), rhs(_rhs) {}
};

struct CheckAnd : Check {

  static constexpr CheckType tag = CheckType_and;

  Checkp a, b;

  CheckAnd(CheckStatus _status, Checkp _a, Checkp _b) : Check(_status, tag), a(_a), b(_b) {}
};

struct CheckOr : Check {

  static constexpr CheckType tag = CheckType_or;

  Checkp a, b;

  CheckOr(CheckStatus _status, Checkp _a, Checkp _b) : Check(_status, tag), a(_a), b(_b) {}
};

struct CheckNot : Check {

  static constexpr CheckType tag = CheckType_not;

  Checkp a;

  CheckNot(CheckStatus _status, Checkp _a) : Check(_status, tag), a(_a) {}
};

struct CheckAt : Check {

  static constexpr CheckType tag = CheckType_at;

  F checkTime;
  Checkp a;

  CheckAt(CheckStatus _status, F _checkTime, Checkp _a) : Check(_status, tag), checkTime(_checkTime), a(_a) {}
};

struct CheckBetween : Check {

  static constexpr CheckType tag = CheckType_between;

  F checkT0, checkT1;
  Checkp a;

  CheckBetween(CheckStatus _status, F _checkT0, F _checkT1, Checkp _a) : Check(_status, tag), checkT0(_checkT0), checkT1(_checkT1), a(_a) {}
};


struct CheckBool : Check {

  static constexpr CheckType tag = CheckType_bool;

  F a;

  CheckBool(CheckStatus _status, F _a) : Check(_status, tag), a(_a) {}
};

optional<tuple<F, F, F>> findRange(Checkp it);


ostream & operator<< (ostream &s, Checkp const &a);
