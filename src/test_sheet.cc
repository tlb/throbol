#include "./test_utils.h"

TEST_CASE("Create simple sheet", "[sheet]")
{
  Sheet sh;
  REQUIRE(mkSheetCheck(sh, R"(
bar: foo - 1
foo: 3 * 5
)", "test1"));
  auto foo = sh.getCell("foo");
  auto bar = sh.getCell("bar");

  CHECK(repr(sh.astByCell[bar]) == "formula[sub[cellref.i=1, number.f=1]]");
  CHECK(repr(sh.astByCell[foo]) == "formula[mul[number.f=3, number.f=5]]");

  CHECK(repr(sh.revDepsByCell) == "[[1] []]");
  CHECK(repr(sh.fwdDepsByCell) == "[[] [0]]");

  auto ref = sampleSheetData(sh);

  L() << "Values "s << *ref;

  CHECK(repr(*ref) == "{{bar: 14, foo: 15}}");

};

TEST_CASE("Perf for compile plantTest", "[.][sheet][perf]")
{
  Sheet sh;
  REQUIRE(sh.load({"src/test-plant.tb"}));
  sh.compile();
  checkSheet(sh);

  perfReport("compile plantTest", [&sh]() {
    sh.setNeedsParseAll();
    sh.compile();
  });
}

TEST_CASE("Perf for execute plantTest", "[.][sheet][perf]")
{
  Sheet sh;
  REQUIRE(sh.load({"src/test-plant.tb"}));
  sh.compile();
  checkSheet(sh);

  auto ref = CellVals::mk(sh.compiled.get());
  auto prev = CellVals::mk(sh.compiled.get());
  CpuState cpu(*sh.compiled, &sh.sheetlife, ref.get(), prev.get());
  cpu.dts[0] = 0.1f;
  perfReport("exec plantTest", [&cpu] {
    cpu.execSheet();
  });
}

TEST_CASE("Execute single plantTest", "[sheet]")
{
  Sheet sh;
  REQUIRE(sh.load({"src/test-plant.tb"}));
  sh.compile();
  checkSheet(sh);

  auto ref = CellVals::mk(sh.compiled.get());
  auto prev = CellVals::mk(sh.compiled.get());
  CpuState cpu(*sh.compiled, &sh.sheetlife, ref.get(), prev.get());
  cpu.dts[0] = 0.1f;
  cpu.execSheet();
}



TEST_CASE("Perf for async trace execution on test-plant", "[.][simperf][simperf-plant]")
{
  Sheet sh;
  REQUIRE(sh.load({"src/test-plant.tb"}));
  sh.compile();
  checkSheet(sh);

  L() << "test-plant: evalops=" << sh.compiled->evalFuncs1.size();

  for (auto [nTraces, nTicks] : {pair(1000, 1000), pair(5000, 10000)}) {

    perfReport("exec test-plant " + repr(nTraces) + " traces * " + repr(nTicks) + " ticks batch=" + repr(MAX_BATCH_SIZE),
      [&sh, nTraces=nTraces, nTicks=nTicks] {

      atomic<size_t> outstanding = 1;
      for (int runi = 0; runi < nTraces; runi++) {
        auto newTrace = make_shared<Trace>(sh.compiled);
        newTrace->setupInitial();
        newTrace->limitMaxIndex(nTicks);
        outstanding++;
        simulateTraceAsync(std::move(newTrace), [&outstanding](shared_ptr<Trace> tr) {
          outstanding--;
        });
        simulatePollWait([&outstanding]() {
          return outstanding < 200;
        });
      }
      outstanding--;
      simulatePollWait([&outstanding]() {
        return outstanding == 0;
      });
    }, nTraces * nTicks);
  }
}

TEST_CASE("Perf for async trace execution on humanoid_standing", "[.][simperf][simperf-humanoid]")
{
  Sheet sh;
  REQUIRE(sh.load({"examples/humanoid-standing.tb"}));
  sh.compile();
  checkSheet(sh);

  int nTraces = 100;
  int nTicks = 1000;

  perfReport("exec humanoid-standing " + repr(nTraces) + " traces * " + repr(nTicks) + " ticks batch=" + repr(MAX_BATCH_SIZE),
    [&sh, nTraces, nTicks] {

    atomic<int> outstanding = 0;
    for (int runi = 0; runi < nTraces; runi++) {
      outstanding ++;
      auto newTrace = make_shared<Trace>(sh.compiled);
      newTrace->setupInitial();
      newTrace->limitMaxIndex(nTicks);
      
      simulateTraceAsync(std::move(newTrace), [&outstanding](shared_ptr<Trace> newTrace2) {
        outstanding--;
      });
    }
    simulatePollWait([&outstanding]() { return outstanding == 0; });
  }, nTraces * nTicks);

}

void dumpParams(Sheet &sh)
{
  for (ParamIndex i = 0; i < sh.nParams; i++) {
    auto cell = sh.cellByParam[i];
    L() << "param[" << i << "] in cell " << cell <<
      " = " << sh.params.valueByParam(i) << " at " << sh.valueLocByParam[i] <<
      "  \"" << sh.getParamLiteral(i) << "\"\n";
  }
}

TEST_CASE("Sheet Params 1", "[sheet]")
{
  Sheet sh;
  REQUIRE(mkSheetCheck(sh, R"(
bar: foo - 1.7~2
foo: (3 * 5)~5
)", "params1"));

  CHECK(sh.nParams == 3);
  dumpParams(sh);

  CHECK(sh.params.valueByParam(0) == 1.7f);
  sh.updateFormulaFromParams(0);
  CHECK(sh.formulaByCell[0] == "foo - 1.7~2");
  sh.params.mutValueByParam(0) = 5.9;
  sh.updateFormulaFromParams(0);
  CHECK(sh.formulaByCell[0] == "foo - 5.9~2");

  sh.params.mutValueByParam(1) = 7.5;
  sh.params.mutValueByParam(2) = 8.5;
  sh.updateFormulaFromParams(1);
  CHECK(sh.formulaByCell[1] == "(7.5 * 8.5)~5");

  sh.compile();
  CHECK(sh.nParams == 3);
  CHECK(sh.params.valueByParam(1) == 7.5);
  CHECK(sh.params.valueByParam(2) == 8.5);

}



TEST_CASE("Params 3", "[sheet]")
{
  Sheet sh;
  REQUIRE(mkSheetCheck(sh, R"(
joints.rht: 0~1
)", "params3"));

  CHECK(sh.nParams == 1);
  dumpParams(sh);

  CHECK(sh.params.valueByParam(0) == 0.0f);
  sh.updateFormulaFromParams(0);
  CHECK(sh.formulaByCell[0] == "0~1");
  sh.params.mutValueByParam(0) = 2.5;
  sh.updateFormulaFromParams(0);
  CHECK(sh.formulaByCell[0] == "2.5~1");
}


TEST_CASE("Negative Params 1", "[sheet]")
{
  /*
    A unary minus should get folded into a negative param
  */

  Sheet sh;
  REQUIRE(mkSheetCheck(sh, R"(
foo: 1
bar: foo * -1.7~2
)", "params2"));

  CHECK(sh.nParams == 1);
  dumpParams(sh);

  CHECK(sh.params.valueByParam(0) == -1.7f);
  sh.updateFormulaFromParams(1);
  CHECK(sh.formulaByCell[1] == "foo * -1.7~2");
  sh.params.mutValueByParam(0) = 5.9;
  sh.updateFormulaFromParams(1);
  CHECK(sh.formulaByCell[1] == "foo * 5.90~2");
}



TEST_CASE("Sheet strings 1", "[sheet]")
{
  Sheet sh;
  REQUIRE(mkSheetCheck(sh, R"(
bar: |
  "blub"
)", "strings1"));

  auto ref = CellVals::mk(sh.compiled.get());
  auto prev = CellVals::mk(sh.compiled.get());
  CpuState cpu(*sh.compiled, &sh.sheetlife, ref.get(), prev.get());
  cpu.dts[0] = 0.1f;
  cpu.execSheet();

  L() << "Values " << *ref;

  checkCpu(cpu);
}


TEST_CASE("Sheet Functions 1", "[sheet]")
{
  Sheet sh;
  REQUIRE(sh.load({"src/test-functions.tb"}));
  sh.compile();
  checkSheet(sh);

  auto ref = CellVals::mk(sh.compiled.get());
  auto prev = CellVals::mk(sh.compiled.get());
  CpuState cpu(*sh.compiled, &sh.sheetlife, ref.get(), prev.get());
  cpu.dts[0] = 0.1f;
  cpu.execSheet();

  L() << "test-functions.tb values " << *ref;

  checkCpu(cpu);
}


TEST_CASE("Sheet Locals 1", "[sheet]")
{
  Sheet sh;
  REQUIRE(sh.load({"src/test-locals.tb"}));
  sh.compile();
  checkSheet(sh);

  auto ref = CellVals::mk(sh.compiled.get());
  auto prev = CellVals::mk(sh.compiled.get());
  CpuState cpu(*sh.compiled, &sh.sheetlife, ref.get(), prev.get());
  cpu.execSheet();

  L() << "test-locals.tb values " << *ref;

  checkCpu(cpu);
}




TEST_CASE("Sheet arrays 1", "[sheet]")
{
  Sheet sh;
  REQUIRE(sh.load({"src/test-arr.tb"}));
  sh.compile();
  checkSheet(sh);

  auto ref = CellVals::mk(sh.compiled.get());
  auto prev = CellVals::mk(sh.compiled.get());
  CpuState cpu(*sh.compiled, &sh.sheetlife, ref.get(), prev.get());
  cpu.dts[0] = 0.1f;
  cpu.execSheet();

  L() << "test-arr.tb values " << *ref;

  checkCpu(cpu);
}

TEST_CASE("Sheet delays 1", "[sheet]")
{
  Sheet sh;
  REQUIRE(mkSheetCheck(sh, R"(
t.const: 5
t.delay2: t.const[-dt]
t.delay3: (t.const + 3)[-dt]
  )", "t-delay"));

  auto v0 = CellVals::mk(sh.compiled.get());
  auto v1 = CellVals::mk(sh.compiled.get());
  auto v2 = CellVals::mk(sh.compiled.get());
  {
    CpuState cpu(*sh.compiled, &sh.sheetlife, v1.get(), v0.get());
    cpu.dts[0] = 0.1f;
    cpu.execSheet();
    checkCpu(cpu);
  }
  {
    CpuState cpu(*sh.compiled, &sh.sheetlife, v2.get(), v1.get());
    cpu.dts[0] = 0.1f;
    cpu.execSheet();
    checkCpu(cpu);
  }

  L() << "test-delay.tb values 0 " << *v0 << "\n" <<
    "test-delay.tb values 1 " << *v1 << "\n" <<
    "test-delay.tb values 2 " << *v2 << "\n";

  CHECK(v0->sv("t.const") == "0");
  CHECK(v1->sv("t.const") == "5");
  CHECK(v2->sv("t.const") == "5");

  CHECK(v0->sv("t.delay2") == "0");
  CHECK(v1->sv("t.delay2") == "0");
  CHECK(v2->sv("t.delay2") == "5");

  CHECK(v0->sv("t.delay3") == "0");
  CHECK(v1->sv("t.delay3") == "0");
  CHECK(v2->sv("t.delay3") == "8");

}

TEST_CASE("Sheet recurrence", "[sheet]")
{

  /*
    This tests the logic behind recursively evaluating cells in postorder regardless of the nominal order
  */
  auto testOrder = [](char const *dump) {
    Sheet sh;
    REQUIRE(mkSheetCheck(sh, dump, "t-recurrence"));

    auto v0 = CellVals::mk(sh.compiled.get());
    auto v1 = CellVals::mk(sh.compiled.get());
    auto v2 = CellVals::mk(sh.compiled.get());
    {
      CpuState cpu(*sh.compiled, &sh.sheetlife, v1.get(), v0.get());
      cpu.dts[0] = 0.01;
      cpu.execSheet();
      checkCpu(cpu);
    }
    {
      CpuState cpu(*sh.compiled, &sh.sheetlife, v2.get(), v1.get());
      cpu.dts[0] = 0.01;
      cpu.execSheet();
      checkCpu(cpu);
    }

    if (0) {
      L() << "t-recurrence.tb values 0 " << *v0 << "\n" <<
        "t-recurrence.tb values 1 " << *v1 << "\n" <<
        "t-recurrence.tb values 2 " << *v2 << "\n";
    }

    CHECK(v0->sv("t.phase") == "0");
    CHECK(v1->sv("t.phase") == "0.01");
    CHECK(v2->sv("t.phase") == "0.02");
  };

  testOrder(R"(
foo: t.phase+1
bar: t.phase+2
t.phase: float(t.phase[-dt]) + dt
)");


  testOrder(R"(
t.phase: float(t.phase[-dt]) + dt
)");

  testOrder(R"(
t.phase2: float(t.phase[-dt]) + dt
t.phase: float(t.phase2) * 1.0;
)");

  testOrder(R"(
t.phase: float(t.phase2) * 1.0;
t.phase2: float(t.phase[-dt]) + dt
)");

  testOrder(R"(
t.phase2: t.phase[-dt] * 1.0
t.phase: float(t.phase3) + dt;
t.phase3: t.phase2 * 1.0
)");

  testOrder(R"(
t.phase: float(t.phase3) + dt;
t.phase2: t.phase[-dt] * 1.0
t.phase3: t.phase2 * 1.0
)");

  testOrder(R"(
t.phase3: t.phase2 * 1.0
t.phase2: t.phase[-dt] * 1.0
t.phase: float(t.phase3) + dt;
)");

  testOrder(R"(
t.phase2: t.phase[-dt] * 1.0
t.phase3: t.phase2 * 1.0
t.phase: float(t.phase3) + dt;
)");

  testOrder(R"(
t.phase: float(t.phase3) + dt;
t.phase3: t.phase2 * 1.0
t.phase2: t.phase[-dt] * 1.0
)");

  testOrder(R"(
t.phase3: t.phase2 * 1.0
t.phase: float(t.phase3) + dt;
t.phase2: t.phase[-dt] * 1.0
)");

}


TEST_CASE("Sheet syntax 1", "[sheet]")
{
  Sheet sh;
  REQUIRE(mkSheetCheck(sh, R"(
t.num: check(2e5+3e5 == 5e5)
t.num2: check(!(5 == 6))
t.num3: check(3. == 3); check(5e-1 == 0.5); check(5e-01 == 0.5) 
t.num4: check(3.==3); check(5e-1-1==-0.5); check(5e-01+1 == 1.5);

t.str: check("foo" == "foo")

)", "syntax1"));

  auto ref = sampleSheetData(sh);
  L() << "Values " << *ref;
}



TEST_CASE("Sheet indexing", "[sheet]")
{
  Sheet sh;
  REQUIRE(mkSheetCheck(sh, R"(
t.num: 2
t.num2: t.num[-dt] + 1
t.num3: t.num[-1 * dt] + 1
t.num4: t.num[dt * -1] + 1
)", "indexing1"));

  auto v0 = CellVals::mk(sh.compiled.get());
  auto v1 = CellVals::mk(sh.compiled.get());
  auto v2 = CellVals::mk(sh.compiled.get());

  {
    CpuState cpu(*sh.compiled, &sh.sheetlife, v1.get(), v0.get());
    cpu.dts[0] = 0.1f;
    cpu.execSheet();
    L() << "Values " << *v1;
    checkCpu(cpu);
  }
  {
    CpuState cpu(*sh.compiled, &sh.sheetlife, v2.get(), v1.get());
    cpu.dts[0] = 0.1f;
    cpu.execSheet();
    L() << "Values " << *v2;
    CHECK(repr(*v2) == "{{t.num: 2, t.num2: 3, t.num3: 3, t.num4: 3}}");
    checkCpu(cpu);
  }

}


TEST_CASE("Complex Params 1", "[sheet][param]")
{
  Sheet sh;
  REQUIRE(mkSheetCheck(sh, R"(
bar: (1 + 3im)~5
)", "complexparams3"));

  CHECK(sh.nParams == 2);
  dumpParams(sh);

  CHECK(sh.distByParam[0] == Dist_complexReal);
  CHECK(sh.distByParam[1] == Dist_complexImag);

  sh.params.mutValueByParam(0) = 2;
  sh.updateFormulaFromParams(0);
  CHECK(sh.formulaByCell[0] == "(2 + 3im)~5");

  sh.params.mutValueByParam(1) = 4;
  sh.updateFormulaFromParams(0);
  CHECK(sh.formulaByCell[0] == "(2 + 4im)~5");
}



TEST_CASE("Load test-calling", "[sheet]")
{
  Sheet sh;
  REQUIRE(sh.load({"src/test-calling.tb"}));

  sh.compile();
  REQUIRE(checkSheet(sh));

  auto ref = sampleSheetData(sh);
  L() << "Values " << *ref;
};

TEST_CASE("Load lotka-volterra", "[sheet]")
{
  Sheet sh;
  REQUIRE(sh.load({"examples/lotka-volterra.tb"}));

  sh.compile();
  REQUIRE(checkSheet(sh));

  auto ref = sampleSheetData(sh);
  L() << "Values " << *ref;
};


TEST_CASE("Load test-fib", "[sheet]")
{
  Sheet sh;
  REQUIRE(sh.load({"src/test-fib.tb"}));

  sh.compile();
  REQUIRE(checkSheet(sh));

  auto ref = sampleSheetData(sh);
  L() << "Values " << *ref;
};


TEST_CASE("Load test-cell-loop", "[sheet]")
{
  Sheet sh;
  REQUIRE(sh.load({"src/test-cell-loop.tb"}));

  sh.compile();
  checkError(sh, "Circular type dependency");

  auto ref = sampleSheetData(sh);
  L() << "Values " << *ref;
};


TEST_CASE("Load test-check", "[sheet]")
{
  Sheet sh;
  REQUIRE(sh.load({"src/test-check.tb"}));
  sh.doDebug = true;

  sh.compile();
  REQUIRE(checkSheet(sh));

  auto ref = sampleSheetData(sh);
  L() << "Values " << *ref;
};


TEST_CASE("Load test_behaviortree", "[sheet][behaviortree]")
{
  Sheet sh;
  REQUIRE(sh.load({"examples/behaviortree.tb"}));
  sh.doDebug = true;

  sh.compile();
  REQUIRE(checkSheet(sh));

  auto ref = sampleSheetData(sh);
  L() << "Values " << *ref;
};



TEST_CASE("Load test-recursion", "[sheet]")
{
  Sheet sh;
  REQUIRE(sh.load({"src/test-recursion.tb"}));
  sh.doDebug = true;

  sh.compile();
  REQUIRE(checkSheet(sh));

  auto ref = sampleSheetData(sh);
  L() << "Values " << *ref;
};



TEST_CASE("Load test-wildcard", "[sheet]")
{
  Sheet sh;
  REQUIRE(sh.load({"src/test-wildcard.tb"}));
  sh.doDebug = true;

  sh.compile();
  REQUIRE(checkSheet(sh));

  auto ref = sampleSheetData(sh);
  L() << "Values " << *ref;
};


TEST_CASE("vec .* cvec", "[sheet][complex][compmul]")
{
  Sheet sh;
  sh.verbose = 2;
  REQUIRE(mkSheetCheck(sh, R"(
bar: |
  [1,2,3,4,5]
foo: |
  [1+1im, 2+2im, 3+3im, 4+4im, 5+5im]
buz: |
  bar .* foo
)", "vec_compmul_cvec"));

}



TEST_CASE("Load test-cycle", "[sheet]")
{
  Sheet sh;
  sh.verbose = 0;
  REQUIRE(sh.load({"src/test-cycle.tb"}));
  sh.doDebug = true;

  sh.compile();

  CellProblems expect;
  expect.emplace_back("Circular type dependency", vector<CellIndex>{
    sh.getCell("x1"),
    sh.getCell("x3"),
    sh.getCell("x2"),
    sh.getCell("x1"),
  });

  expect.emplace_back("Circular value dependency", vector<CellIndex>{
    sh.getCell("y1"),
    sh.getCell("y3"),
    sh.getCell("y2"),
    sh.getCell("y1"),
  });

  CHECK(repr(sh.compiled->orderProblems) == repr(expect));

  //REQUIRE(checkSheet(sh));

};


TEST_CASE("Parsing & associativity", "[sheet][parsing]")
{
  Sheet sh;
  REQUIRE(sh.load({"src/test-parsing.tb"}));
  sh.compile();
  checkSheet(sh);

  auto ref = CellVals::mk(sh.compiled.get());
  auto prev = CellVals::mk(sh.compiled.get());
  CpuState cpu(*sh.compiled, &sh.sheetlife, ref.get(), prev.get());
  cpu.execSheet();

  L() << "test-parsing.tb values " << *ref;

  checkCpu(cpu);
}


TEST_CASE("Deftests", "[sheet][deftest]")
{
  vector<DefTestInstance *> all;
  for (auto p = DefTestInstance::head; p; p = p->prev) {
    all.push_back(p);
  }
  for (auto it = rbegin(all); it != rend(all); it++) {
    auto p = *it;
    SECTION(p->name) {
      Sheet sh;
      REQUIRE(mkSheetCheck(sh, p->dump));
      auto ref = sampleSheetData(sh);
      if (ref) {
        L() << p->name << ": " << ref;
      }
    }
  }
}
