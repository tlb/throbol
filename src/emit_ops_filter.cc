#include "./emit.h"
#include "./emit_macros.h"


template <typename ELT>
void lpfilter1(
  CpuState &cpu,
  ELT &r, F period, ELT const &input,
  ELT &y0New, ELT const &y0Prev, int batchi)
{
  if (cpu.isInitial()) {
    r = y0New = input;
    return;
  }
  F coeff = 1.0f / (1.0f + max(0.0f, period) / max(cpu.dts[batchi], 0.00001f));
  r = y0New = (coeff * input) + (1.0f - coeff) * y0Prev;
}


deftype(lpfilter1)
{
  return eval(n->ch(1));
}

defemit(lpfilter1)
{
  // filtered = lpfilter1(period, input)
  defdelay2(F, mkValue(args[1].t), F, F, F, lpfilter1(cpu, r, a, b, snew, sprev, batchi));
  defdelay2(CF, mkValue(args[1].t), F, CF, CF, lpfilter1(cpu, r, a, b, snew, sprev, batchi));
  defdelay2(vec2, mkValue(args[1].t), F, vec2, vec2, lpfilter1(cpu, r, a, b, snew, sprev, batchi));
  defdelay2(vec3, mkValue(args[1].t), F, vec3, vec3, lpfilter1(cpu, r, a, b, snew, sprev, batchi));
  defdelay2(vec4, mkValue(args[1].t), F, vec4, vec4, lpfilter1(cpu, r, a, b, snew, sprev, batchi));
  defdelay2(mat2, mkValue(args[1].t), F, mat2, mat2, lpfilter1(cpu, r, a, b, snew, sprev, batchi));
  defdelay2(mat3, mkValue(args[1].t), F, mat3, mat3, lpfilter1(cpu, r, a, b, snew, sprev, batchi));
  defdelay2(mat4, mkValue(args[1].t), F, mat4, mat4, lpfilter1(cpu, r, a, b, snew, sprev, batchi));
  defdelay2r(mat, mkValue(args[1].t), F, mat, mat, true, allocResult(args[1].t), lpfilter1(cpu, r, a, b, snew, sprev, batchi));
  defdelay2r(cmat, mkValue(args[1].t), F, cmat, cmat, true, allocResult(args[1].t), lpfilter1(cpu, r, a, b, snew, sprev, batchi));

  return setInvalidArgs();
}

defusage(lpfilter1, R"(
  lpfilter1(period ∊ F, input ∊ F|CF)
  lpfilter1(period ∊ F, input ∊ vec2|3|4)
  lpfilter1(period ∊ F, input ∊ mat2|3|4))");



template <typename ELT>
void clampslew(
  CpuState &cpu,
  ELT &r, ELT const &input, F ratelo, F ratehi,
  ELT &y0New, ELT const &y0Prev, int batchi)
{
  if (cpu.isInitial()) {
    r = y0New = input;
  }
  else {
    r = y0New = std::clamp(input, y0Prev + ratelo * cpu.dts[batchi], y0Prev + ratehi * cpu.dts[batchi]);
  }
}


deftype(clampslew)
{
  return eval(n->ch(0));
}

defemit(clampslew)
{
  // filtered = clampslew(input, ratelo, ratehi)
  if (argmatch3(F, F, F)) {
    auto y0v = mkValue(args[0].t);
    auto rv = allocResult<F>();
    she.evalop([rv1=rv, inputv1=args[0], ratelov1=args[1], ratehiv1=args[2], y0v=y0v](CpuState &cpu, Pad &pad, auto batchdef) {
      auto rv = batchdef.getVal(rv1);
      auto inputv = batchdef.getVal(inputv1);
      auto ratelov = batchdef.getVal(ratelov1);
      auto ratehiv = batchdef.getVal(ratehiv1);
      for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) {
        F &r = pad.wr_F(rv, batchi);
        if (cpu.curs[batchi] && cpu.prevs[batchi]) {
          F const &y0Prev = cpu.prevs[batchi]->rd_F(y0v);
          F &y0New = cpu.curs[batchi]->wr_F(y0v);
          F const &input = pad.rd_F(inputv, batchi);
          F const &ratelo = pad.rd_F(ratelov, batchi);
          F const &ratehi = pad.rd_F(ratehiv, batchi);
          clampslew(cpu, r, input, ratelo, ratehi, y0New, y0Prev, batchi);
        } else {
          setZero(r);
        }
      }
    });
    ce.hasPrev = true;
    return rv;
  }

  return setInvalidArgs();
}

defusage(clampslew, R"(
  clampslew(input ∊ F, ratelo ∊ F, ratehi ∊ F))");


template<typename ELT>
void lpfilter2(
  CpuState &cpu,
  ELT &r, ELT const &input, F period, F q,
  ELT &y1New, ELT const &y1Prev,
  ELT &y2New, ELT const &y2Prev,
  ELT &x1New, ELT const &x1Prev,
  ELT &x2New, ELT const &x2Prev,
  int batchi)
{
  if (cpu.isInitial()) {
    r = y1New = y2New = x1New = x2New = input;
    return;
  }
  // See http://shepazu.github.io/Audio-EQ-Cookbook/audio-eq-cookbook.html
  F w0 = cpu.dts[batchi] / max(cpu.dts[batchi], period);
  F cosw0 = cos(w0);
  F sinw0 = sin(w0);
  F alpha = sinw0 / (2.0f * max(0.01f, q));

  F scale = 1 / (1 + alpha);

  F a1 = (-2 * cosw0) * scale;
  F a0 = (1 - alpha) * scale;
  F b2 = ((1 - cosw0) / 2) * scale;
  F b1 = (1 - cosw0) * scale;
  F b0 = ((1 - cosw0) / 2) * scale;

  x1New = x2Prev;
  x2New = input;
  y1New = y2Prev;
  
  r = y2New = (
    x1Prev * b0 +
    x2Prev * b1 +
    input * b2 -
    y1Prev * a0 -
    y2Prev * a1);
}


deftype(lpfilter2)
{
  return eval(n->ch(2));
}

defemit(lpfilter2)
{
  // filtered = lpfilter2(period, q, input)
  // q = 0.707 gives critical damping
#define fortype(T) \
  if (argmatch3(F, F, T)) { \
    auto y1Ref = mkValue(args[2].t); \
    auto y2Ref = mkValue(args[2].t); \
    auto x1Ref = mkValue(args[2].t); \
    auto x2Ref = mkValue(args[2].t); \
    auto rv = allocResult(args[2].t); \
    she.evalop([rv1=rv, periodv1=args[0], qv1=args[1], inputv1=args[2], \
      y1v=y1Ref, y2v=y2Ref, \
      x1v=x1Ref, x2v=x2Ref] \
      (CpuState &cpu, Pad &pad, auto batchdef) { \
      auto rv = batchdef.getVal(rv1); \
      auto periodv = batchdef.getVal(periodv1); \
      auto qv = batchdef.getVal(qv1); \
      auto inputv = batchdef.getVal(inputv1); \
      for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
        decltype(auto) r = pad.wr_##T(rv, batchi); \
        if (cpu.curs[batchi] && cpu.prevs[batchi]) { \
\
          decltype(auto) y1Prev = cpu.prevs[batchi]->rd_##T(y1v); \
          decltype(auto) y1New = cpu.curs[batchi]->wr_##T(y1v); \
\
          decltype(auto) y2Prev = cpu.prevs[batchi]->rd_##T(y2v); \
          decltype(auto) y2New = cpu.curs[batchi]->wr_##T(y2v); \
\
          decltype(auto) x1Prev = cpu.prevs[batchi]->rd_##T(x1v); \
          decltype(auto) x1New = cpu.curs[batchi]->wr_##T(x1v); \
\
          decltype(auto) x2Prev = cpu.prevs[batchi]->rd_##T(x2v); \
          decltype(auto) x2New = cpu.curs[batchi]->wr_##T(x2v); \
\
          decltype(auto) period = pad.rd_F(periodv, batchi); \
          decltype(auto) q = pad.rd_F(qv, batchi); \
          decltype(auto) input = pad.rd_##T(inputv, batchi); \
\
          lpfilter2(cpu, r, input, period, q, \
            y1New, y1Prev, \
            y2New, y2Prev, \
            x1New, x1Prev, \
            x2New, x2Prev, batchi); \
        } else { \
          setZero(r); \
        } \
      } \
    }); \
    return rv; \
  }

  fortype(F);
  fortype(CF);
  fortype(vec2);
  fortype(vec3);
  fortype(vec4);
  fortype(mat2);
  fortype(mat3);
  fortype(mat4);
  fortype(mat);

#undef fortype

  return setInvalidArgs();
}

defusage(lpfilter2, R"(
  lpfilter2(period ∊ F, q ∊ F, input ∊ F|CF|vec|mat))");



deftype(hysteresis)
{
  return typetag<F>();
}

defemit(hysteresis)
{
  defdelay1(F, mkValue(typetag<F>()), F, F, (r = snew = (a + 0.5f * sprev >= 0.0f) ? 1.0f : -1.0f));
  return setInvalidArgs();
}

defusage(hysteresis, R"(
  hysteresis(F))");


deftype(roundhyst)
{
  return typetag<F>();
}

defemit(roundhyst)
{
  defdelay2(F, mkValue(typetag<F>()), F, F, F, (r = snew = op_roundhyst(cpu, a, b, sprev)));
  defdelay1(F, mkValue(typetag<F>()), F, F, (r = snew = op_roundhyst(cpu, a, 0.0, sprev)));
  return setInvalidArgs();
}

defusage(roundhyst, R"(
  roundhyst(input ∊ F)
  roundhyst(input ∊ F, thresh ∊ F))");


deftype(latch)
{
  return typetag<bool>();
}

defemit(latch)
{
  defdelay1(bool, mkValue(typetag<bool>()), bool, bool, (r = snew = (a || sprev)));
  defdelay2(bool, mkValue(typetag<bool>()), bool, bool, bool, (r = snew = !b && (a || sprev)));
  return setInvalidArgs();
}

defusage(latch, R"(
  latch(set ∊ bool)
  latch(set ∊ bool, reset ∊ bool)");


deftype(integrate)
{
  return eval(n->ch(0));
}

defemit(integrate)
{
  defdelay1(F, mkValue(typetag<F>()), F, F, (r = snew = sprev + a * cpu.dts[batchi]));
  defdelay2(F, mkValue(typetag<F>()), F, bool, F, (r = snew = b ? 0.0f : (sprev + a * cpu.dts[batchi])));
  defdelay4(F, mkValue(typetag<F>()), F, bool, F, F, F, (r = snew = clamp(b ? 0.0f : (sprev + a * cpu.dts[batchi]), c, d)));
  return setInvalidArgs();
}

defusage(integrate, R"(
  integrate(x ∊ F)
  integrate(x ∊ F, reset ∊ F) -- set to 0 if reset
  integrate(x ∊ F, reset ∊ F, lo ∊ F, hi ∊ F) -- add windup protection between lo and hi)");



deftype(duration)
{
  return typetag<F>();
}

defemit(duration)
{
  defdelay1(F, mkValue(typetag<F>()), bool, F, (r = snew = a ? sprev + cpu.dts[batchi] : 0.0f));
  return setInvalidArgs();
}

defusage(duration, R"(
  duration(a ∊ bool) -- how long a has been continuously true for)");


template <typename ELT>
void vel(
  CpuState &cpu,
  ELT &r, ELT const &input,
  ELT &sNew, ELT const &sPrev, int batchi)
{
  if (cpu.isInitial()) {
    setZero(r);
    sNew = input;
    return;
  }
  r = (input - sPrev) / cpu.dts[batchi];
  sNew = input;
}

deftype(vel)
{
  return eval(n->ch(0));
}

defemit(vel)
{
  defdelay1(F, mkValue(args[0].t), F, F, vel(cpu, r, a, snew, sprev, batchi));
  defdelay1(CF, mkValue(args[0].t), CF, CF, vel(cpu, r, a, snew, sprev, batchi));
  defdelay1(vec2, mkValue(args[0].t), vec2, vec2, vel(cpu, r, a, snew, sprev, batchi));
  defdelay1(vec3, mkValue(args[0].t), vec3, vec3, vel(cpu, r, a, snew, sprev, batchi));
  defdelay1(vec4, mkValue(args[0].t), vec4, vec4, vel(cpu, r, a, snew, sprev, batchi));
  defdelay1(mat2, mkValue(args[0].t), mat2, mat2, vel(cpu, r, a, snew, sprev, batchi));
  defdelay1(mat3, mkValue(args[0].t), mat3, mat3, vel(cpu, r, a, snew, sprev, batchi));
  defdelay1(mat4, mkValue(args[0].t), mat4, mat4, vel(cpu, r, a, snew, sprev, batchi));
  defdelay1r(mat, mkValue(args[0].t), mat, mat, true, allocResult(args[0].t), vel(cpu, r, a, snew, sprev, batchi));
  defdelay1r(cmat, mkValue(args[0].t), cmat, cmat, true, allocResult(args[0].t), vel(cpu, r, a, snew, sprev, batchi));

  return setInvalidArgs();
}

defusage(vel, R"(
  vel(F|CF|vec|mat))");

template <typename ELT>
void lookahead(
  CpuState &cpu,
  ELT &r, ELT const &input, F period,
  ELT &sNew, ELT const &sPrev, int batchi)
{
  if (cpu.isInitial()) {
    r = input;
    sNew = input;
    return;
  }
  r = input + (input - sPrev) * period / cpu.dts[batchi];
  sNew = input;
}


deftype(lookahead)
{
  return eval(n->ch(1));
}

defemit(lookahead)
{
  #define fortype(T) \
  if (argmatch2(F, T)) { \
    defdelay2(T, mkValue(args[1].t), F, T, T, lookahead(cpu, r, b, a, snew, sprev, batchi)); \
  }
  fortype(F);
  fortype(CF);
  fortype(vec2);
  fortype(vec3);
  fortype(vec4);
  fortype(mat2);
  fortype(mat3);
  fortype(mat4);
  fortype(mat);
  #undef fortype
  return setInvalidArgs();
}

defusage(lookahead, R"(
  lookahead(period ∊ F, input ∊ F|CF|vecN|matN))");

