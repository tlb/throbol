

static vec4 checkColor(CheckStatus t)
{
  switch (t) {
    case Check_notnow: return bgcolor(0x5d, 0xa5, 0xda, 0xcc);
    case Check_pass: return bgcolor(0x55, 0xff, 0x55, 0xff);
    case Check_fail: return bgcolor(0xff, 0x33, 0x33, 0xff);
    default:
      L() << "Unexpected CheckStatus " << t;
      return bgcolor(0xff, 0x00, 0x00, 0xff);
  }
}


struct BehVis {

  struct SegmentEnt {

    F startXrel, endXrel;
    int lineIndex;
    vec4 color;

  };

  Sheet &sh;
  SheetLayoutCell &lo;
  vector<SegmentEnt> segments;

  static constexpr F XREL_NONE = -1.0f;
  F failStartXrel = XREL_NONE;
  F successStartXrel = XREL_NONE;
  F runningStartXrel = XREL_NONE;

  vec4 failCol{1, 0.5, 0.5, 1};
  vec4 successCol{0.5, 1, 0.5, 1};
  vec4 runningCol{1, 1, 1, 1};
  int nLines = 0;
  F thicken = 0.0f;

  map<U32, vector<bool>> runmap;

  BehVis(Sheet &_sh, SheetLayoutCell &_lo)
    : sh(_sh),
      lo(_lo)
  {
  }

  ~BehVis() = default;

  void start(DrawInfo &drawInfo, F _thicken)
  {
    thicken = _thicken;
    for (auto &it : drawInfo.autoRangeSymbols) {
      runmap[it].resize(lo.samples().size());
    }
  }

  void addSymbol(size_t samplei, Symbol symbol)
  {
    if (symbol.i != 0) {
      auto &runslot = runmap[symbol.i];
      if (runslot.empty()) {
        // surprise value, not found by autorange
        runslot.resize(lo.samples().size());
      }
      if (samplei < runslot.size()) {
        runslot[samplei] = true;
      }
    }
  }

  void add(size_t samplei, F xrel, Beh val)
  {
    for (auto const *p = &val.runset; p; p = p->tail) {
      addSymbol(samplei, p->leaf.i);
    }
    addOverall(xrel, val);
  }

  void addOverall(F xrel, Beh val)
  {
    if (val.status == B_failure && failStartXrel == XREL_NONE) {
      failStartXrel = xrel;
    }
    else if (val.status != B_failure && failStartXrel != XREL_NONE) {
      segments.push_back(SegmentEnt {
        failStartXrel,
        xrel,
        -1,
        failCol
      });
      failStartXrel = XREL_NONE;  
    }

    if (val.status == B_success && successStartXrel == XREL_NONE) {
      successStartXrel = xrel;
    }
    else if (val.status != B_success && successStartXrel != XREL_NONE) {
      segments.push_back(SegmentEnt {
        successStartXrel,
        xrel,
        -1,
        successCol
      });
      successStartXrel = XREL_NONE;
    }

    if (val.status == B_running && runningStartXrel == XREL_NONE) {
      runningStartXrel = xrel;
    }
    else if (val.status != B_running && runningStartXrel != XREL_NONE) {
      segments.push_back(SegmentEnt {
        runningStartXrel,
        xrel,
        -1,
        runningCol
      });
      runningStartXrel = XREL_NONE;
    }
  }

  void finish()
  {
    if (failStartXrel != XREL_NONE) segments.push_back(SegmentEnt {
      failStartXrel, 
      lo.samples().back().second,
      -1,
      failCol
    });
    if (successStartXrel != XREL_NONE) segments.push_back(SegmentEnt {
      successStartXrel,
      lo.samples().back().second,
      -1,
      successCol
    });
    if (runningStartXrel != XREL_NONE) segments.push_back(SegmentEnt {
      runningStartXrel,
      lo.samples().back().second,
      -1,
      runningCol
    });


    nLines = runmap.size();

    int linei = 0;
    for (auto &[symbol, runslot] : runmap) {
      F lineStartXrel = XREL_NONE;
      size_t samplei = 0;
      for (auto &[li, xrel] : lo.samples()) {
        auto curval = runslot[samplei];
        if (curval && lineStartXrel == XREL_NONE) {
          lineStartXrel = xrel;
        }
        else if (!curval && lineStartXrel != XREL_NONE) {
          segments.push_back(SegmentEnt {
            lineStartXrel,
            xrel,
            linei,
            goodGraphColor(linei)
          });
          lineStartXrel = XREL_NONE;
        }
        samplei ++;
      }
      if (lineStartXrel != XREL_NONE) {
        segments.push_back(SegmentEnt {
          lineStartXrel,
          lo.samples().back().second,
          linei,
          goodGraphColor(linei)
        });
      }
      linei++;
    }

  }

  void render(G8DrawList &dl)
  {

    vec3 lineSep = lo.graphAxisY * -1.8f / F(min(4, nLines + 1));
    vec3 lineTop = lo.origin + 0.95f * lo.graphAxisY + lineSep;
    vec3 lineMid = lineTop - 0.1f * lo.graphAxisY;
    vec3 lineBot = lineTop - 0.2f * thicken * lo.graphAxisY;

    int linei = 0;
    if (!lo.altSeed) {
      for (auto &[symbol, runslot] : runmap) {
        dl.addText(
          lineMid + linei * lineSep + vec3(-0.03, 0, 0), 
          vec3(0.15, 0, 0), vec3(0, -0.15, 0), vec3(0, 0, 1), 
          sh.nameBySymbol[symbol],
          goodGraphColor(linei),
          ysFonts.lrgFont, 
          1.0f, 0.5f,
          0.0f, &glowingTextShader);

        linei++;
      }
    }

    dl.addText(
      lineMid + -1 * lineSep + vec3(-0.03, 0, 0), 
      vec3(0.15, 0, 0), vec3(0, -0.15, 0), vec3(0, 0, 1), 
      "status",
      vec4(1, 1, 1, 1),
      ysFonts.lrgFont, 
      1.0f, 0.5f,
      0.0f, &glowingTextShader);

    for (auto &it : segments) {
      dl.addQuad(
        lineTop + it.startXrel * lo.graphAxisX + it.lineIndex * lineSep, it.color,
        lineTop + it.endXrel * lo.graphAxisX + it.lineIndex * lineSep, it.color,
        lineBot + it.startXrel * lo.graphAxisX + it.lineIndex * lineSep, it.color,
        lineBot + it.endXrel * lo.graphAxisX + it.lineIndex * lineSep, it.color,
        0.0f, &glowingTextShader);
    }
  }

};

struct CheckVis {

  Sheet &sh;
  SheetLayoutCell &lo;

  vec3 vCoeff;
  F thicken = 0.0f;

  vector<vector<pair<vec3, vec3>>> bands;
  vector<pair<CheckStatus, vec3>> valueLine;

  CheckVis(Sheet &_sh, SheetLayoutCell &_lo)
    : sh(_sh),
      lo(_lo)
  {
    bands.emplace_back();
  }

  ~CheckVis() = default;

  void start(DrawInfo &drawInfo, F _thicken)
  {
    thicken = _thicken;
    vCoeff = (1.0f / drawInfo.scale) * lo.graphAxisY;
  }

  void add(F xrel, Checkp val)
  {
    if (auto range = findRange(val)) {
      auto [low, v, hi] = *range;
      if (val->status == Check_notnow) {
        if (bands.size() % 2 != 1) {
          bands.back().emplace_back(
            lo.origin + xrel * lo.graphAxisX + low * vCoeff,
            lo.origin + xrel * lo.graphAxisX + hi * vCoeff);
          bands.emplace_back(); // notnows go in odd bands
        }
        bands.back().emplace_back(
          lo.origin + xrel * lo.graphAxisX + 1.05 * lo.graphAxisY,
          lo.origin + xrel * lo.graphAxisX - 1.05 * lo.graphAxisY);
      }
      else {
        if (bands.size() % 2 != 0) {
          bands.back().emplace_back(
            lo.origin + xrel * lo.graphAxisX + low * vCoeff,
            lo.origin + xrel * lo.graphAxisX + hi * vCoeff);
          bands.emplace_back(); // actual checks go in even bands
        }
        bands.back().emplace_back(
          lo.origin + xrel * lo.graphAxisX + low * vCoeff,
          lo.origin + xrel * lo.graphAxisX + hi * vCoeff);
      }
      valueLine.emplace_back(
        val->status,
        lo.origin + xrel * lo.graphAxisX + v * vCoeff);
    }
    else {
      valueLine.emplace_back(
        val->status,
        lo.origin + xrel * lo.graphAxisX + vCoeff * (val->status==Check_fail ? 0.0f : 1.0f));
      // ??? drawInfo.autoRangeMax = 1.0;
    }
  }

  void finish()
  {
  }

  void render(G8DrawList &dl)
  {
    vec3 up(0, 0, 1);
    for (size_t bi = 0; bi < bands.size(); bi++) {
      dl.addBand(bands[bi], up, fgcolor(0xcc, 0xcc, 0xcc, bi%2 ? 0x88 : 0x88));
    }

    CheckStatus prevStatus = Check_notnow;
    if (!valueLine.empty()) {
      vector<vec3> pts;
      for (auto &[status, pt] : valueLine) {
        pts.push_back(pt);
        if (status != prevStatus && !pts.empty()) {
          dl.addThickCurve(pts, up, checkColor(prevStatus), 0.04);
          auto ptsEnd = pts.back();
          pts.clear();
          pts.push_back(ptsEnd);
          prevStatus = status;
        }
      }
      dl.addThickCurve(pts, up, checkColor(prevStatus), 0.04f * thicken);
    }
  }

};



template<typename RefType>
void SheetUi::drawCellTrace(G8DrawList &dl, CellIndex cell, Trace &tr, SheetLayoutCell &lo, vector<RefType> const &brs, DrawInfo &drawInfo)
{
  if (visEndIndex <= visBeginIndex) return;
  int colorIndex = 0;
  if (!lo.forDebug && rewardCells[cell]) {
    colorIndex = find(rewardCells.begin(), rewardCells.end(), cell) - rewardCells.begin();
  }
  int batchi = tr.cpuBatchIndex;

  int bri = 0;
  for (auto &brSuite : brs) {
    if (!brSuite.valid()) continue;
    auto br = brSuite.getRefForCompiledBatchSize(tr.compiledBatchSize);
    vector<vec3> pts;
    vec3 vCoeff = (1.0f / drawInfo.scale) * lo.graphAxisY;
    
    auto pt = [&pts, &lo, vCoeff](F xrel, F val) {
      pts.push_back(lo.origin + xrel * lo.graphAxisX + val * vCoeff);
    };

    F thicken = lo.thicken * (bri > 0 ? 0.5f : 1.0f);
    if (zoomScale > 3.0f) {
      thicken = 0.0f;
    }

    pts.reserve(lo.samples().size());
    vec3 up(0, 0, 1);
    if (br.template ist<F>()) {
      for (auto &[li, xrel] : lo.samples()) {
        if (auto vals = getVals(tr, li, br)) {
          auto val = vals->rd_F(br, batchi);
          pt(xrel, val);
        }
      }
      dl.addThickCurve(pts, up, goodGraphColor(colorIndex), 0.04f * thicken);
    }
    else if (br.template ist<CF>()) {
      for (U32 row = 0; row < 2; row++) {
        pts.clear();
        for (auto &[li, xrel] : lo.samples()) {
          if (auto vals = getVals(tr, li, br)) {
            auto val = vals->rd_CF(br, batchi);
            pt(xrel, row ? val.imag() : val.real());
          }
        }
        dl.addThickCurve(pts, up, goodGraphColor(row), 0.04f * thicken);
      }
    }
    else if (br.template ist<bool>()) {
      for (auto &[li, xrel] : lo.samples()) {
        if (auto vals = getVals(tr, li, br)) {
          auto val = vals->rd_bool(br, batchi);
          pt(xrel, val ? 1.0f : 0.0f);
        }
      }
      dl.addThickCurve(pts, up, goodGraphColor(colorIndex), 0.04f * thicken);
    }

    else if (br.template ist<mat>()) {
      // This should be configurable
      Index showrows = min(Index(32), Index(br.t.nr));
      Index showcols = min(Index(5), Index(br.t.nc));

      vector<vector<vec3>> allpts(showrows * showcols);

      for (auto &[li, xrel] : lo.samples()) {
        if (auto vals = getVals(tr, li, br)) {
          decltype(auto) val = vals->rd_mat(br, batchi);
          for (Index col = 0; col < showcols; col++) {
            for (Index row = 0; row < showrows; row++) {
              allpts[col * showrows + row].push_back(lo.origin + 
                xrel * lo.graphAxisX +
                val(row, col) * vCoeff);
            }
          }
        }
      }
      for (Index col = 0; col < showcols; col++) {
        for (Index row = 0; row < showrows; row++) {
          dl.addThickCurve(allpts[col * showrows + row], up, goodGraphColor(col*showrows + row), 0.03f * thicken);
        }
      }
    }
    else if (br.template ist<cmat>()) {
      // This should be configurable
      Index showrows = min(Index(32), Index(br.t.nr));
      Index showcols = min(Index(5), Index(br.t.nc));

      vector<vector<vec3>> allpts(showrows * showcols * 2);

      for (auto &[li, xrel] : lo.samples()) {
        if (auto vals = getVals(tr, li, br)) {
          decltype(auto) val = vals->rd_cmat(br, batchi);
          for (Index col = 0; col < showcols; col++) {
            for (Index row = 0; row < showrows; row++) {
              allpts[(col * showrows + row) * 2 + 0].push_back(lo.origin + 
                xrel * lo.graphAxisX +
                real(val(row, col)) * vCoeff);

              allpts[(col * showrows + row) * 2 + 1].push_back(lo.origin + 
                xrel * lo.graphAxisX +
                imag(val(row, col)) * vCoeff);
            }
          }
        }
      }
      for (Index col = 0; col < showcols; col++) {
        for (Index row = 0; row < showrows; row++) {
          dl.addThickCurve(allpts[(col * showrows + row) * 2 + 0], up, goodGraphColor((col * showrows + row) * 2 + 0), 0.03f * thicken);
          dl.addThickCurve(allpts[(col * showrows + row) * 2 + 1], up, goodGraphColor((col * showrows + row) * 2 + 1), 0.03f * thicken);
        }
      }
    }
    else if (br.template ist<Checkp>()) {
      CheckVis vis(sh, lo);
      vis.start(drawInfo, thicken);

      for (auto &[li, xrel] : lo.samples()) {
        auto vals = getVals(tr, li, br);
        if (auto val = vals->rd_Checkp(br, batchi)) {
          vis.add(xrel, val);
        }
      }
      vis.finish();
      vis.render(dl);
    }
    else if (br.template ist<Beh>()) {

      BehVis vis(sh, lo);
      vis.start(drawInfo, thicken);

      size_t samplei = 0;
      for (auto &[li, xrel] : lo.samples()) {
        if (auto vals = getVals(tr, li, br)) {
          decltype(auto) val = vals->rd_Beh(br, batchi);
          vis.add(samplei, xrel, val);
        }
        samplei ++;
      }
      vis.finish();
      vis.render(dl);
    }
    // ADD type
    bri++;
    if (!showMultipleValues) break;
  }
}


void SheetUi::drawCellAxes(G8DrawList &dl, CellIndex cell, Trace &tr, SheetLayoutCell &lo, TypeTag type, DrawInfo &drawInfo)
{
#if 0
  vec4 axcol = fgcolor(0x22, 0x22, 0x22, 0xff);
  vec4 bgcol;
  if (tr.checkFailuresByCell[cell]) {
    bgcol = bgcolor(0xcc, 0x55, 0x55, 0x99);
  }
  else if (rewardCells[cell]) {
    bgcol = bgcolor(0xe2, 0xdf, 0x9a, 0x99);
  }
  else {
    bgcol = bgcolor(0x55, 0x55, 0x55, 0x99);
  }
#endif
  F baseLayer = 0.0f;

  if (type.template ist<F>() || type.template ist<CF>() || 
      type.template ist<vec2>() || type.template ist<vec3>() || type.template ist<vec4>() ||
      type.template ist<mat2>() || type.template ist<mat3>() || type.template ist<mat4>() ||
      type.template ist<mat>() || type.template ist<cmat>() ||
      type.template ist<Checkp>()) {
    
#if 0
    float yRange = 1.05;
    dl.addQuad(
      lo.origin - yRange * lo.graphAxisY, bgcol, 
      lo.origin - yRange * lo.graphAxisY + lo.graphAxisX, bgcol,
      lo.origin + yRange * lo.graphAxisY, bgcol, 
      lo.origin + yRange * lo.graphAxisY + lo.graphAxisX, bgcol,
      baseLayer-0.01f, &unshinySolidShader);
    {
      vector<vec3> pts {
        lo.origin,
        lo.origin + lo.graphAxisX
      };
      dl.addThickCurve(pts, vec3(0, 0, 1), axcol, 0.02,
        baseLayer-0.02, &glowingTextShader);
    }
#endif

    auto tickColor = fgcolor(0xff, 0xff, 0xff, 0x88);
    for (auto labelVal : {-1.0f, 0.0f, +1.0f}) {
      vec3 textpos = lo.origin + labelVal * lo.graphAxisY + vec3(-0.05f, 0, 0);
      dl.addQuad(
        textpos + vec3(0, 0.01f, 0), tickColor,
        textpos + vec3(0.05f, 0.01f, 0), tickColor,
        textpos + vec3(0, -0.01f, 0), tickColor,
        textpos + vec3(0.05f, -0.01f, 0), tickColor,
        baseLayer);
      dl.addText(textpos,
        vec3(0.15, 0, 0), vec3(0, -0.15, 0), vec3(0, 0, 1), 
        repr_0_2g(labelVal * drawInfo.scale), 
        tickColor,
        ysFonts.lrgFont, 
        1.0f, 0.5f,
        baseLayer);
    }

    lo.growTot(5.0f, lo.formulaHeight + 0.75f);
    
    if (lo.drawTimeLabels) {
      for (auto [ptIndex, ptX, alignH, ok] : {
        tuple(lo.g.lhsIndex, 0.0f, 0.0f, lo.g.cursXrel > 0.03f),
        tuple(lo.g.cursIndex, lo.g.cursXrel, 0.0f, true),
        tuple(lo.g.rhsIndex, 1.0f, 1.0f, lo.g.cursXrel < 0.92f)}) {
        if (!ok) continue;
        vec3 textpos = lo.origin - lo.graphAxisY + ptX * lo.graphAxisX + vec3(0, +0.0f, 0);
        dl.addText(textpos,
          vec3(0.15f, 0, 0), vec3(0, -0.15f, 0), vec3(0, 0, 1.0f),
          repr_0_3g(tr.getTimeForIndex(ptIndex)), 
          tickColor,
          ysFonts.lrgFont, 
          alignH, 1.0f,
          baseLayer);
      }
    }

  }

}
