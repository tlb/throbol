#include "./core.h"
#include "./token.h"
#include "./ast.h"

/*
  Turn a formula as text into an AST.

  The constructor sets everything up and does the work. After constructing,
  check `.ok` to see if it worked, and then use `.ast`.
  If not `.ok`, then `.error` is a string with a parse error.

  This is a straightforward recursive descent parser.

*/

struct FormulaParser {

  Sheet &sh;
  CellIndex cell;

  FormulaTokenizer tokenizer;
  FormulaTokenizer::Tokit p;
  FormulaTokenizer::Tokit end;
  FormulaTokenizer::Tokit lastConsumedToken;
  vector<ParamIndex> paramsForReuse;

  AstNode *ast = nullptr;
  string error;
  FormulaSubstr errorLoc;
  bool ok = false;
  bool lastConsumedTokenValid = false;

  FormulaParser(Sheet &_sh, CellIndex _cell)
    : sh(_sh),
      cell(_cell),
      tokenizer(sh, cell, sh.formulaByCell[cell].c_str())
  {
    if (!tokenizer.ok) {
      setError(tokenizer.error);
      errorLoc = tokenizer.errorLoc;
      return;
    }
    // We pop paramIndexes off the back of paramsForReuse, so add them in reverse order
    for (auto paramit = sh.paramsByCell[cell].rbegin(); paramit != sh.paramsByCell[cell].rend(); paramit++) {
      paramsForReuse.push_back(*paramit);
    }
    parse();
    for (auto paramIndex : paramsForReuse) { // left-over because removed?
      sh.valueLocByParam[paramIndex] = FormulaSubstr();
      sh.rangeLocByParam[paramIndex] = FormulaSubstr();
    }
  }

  void parse()
  {
    p = tokenizer.tokens.begin();
    end = tokenizer.tokens.end();
    ok = true;
    ast = parseCell();
    if (!ast) {
      ok = false;
    }
    else {
      if (0) L() << "parse " << sh.nameByCell[cell] << ": " << ast;
    }
  }

  nullptr_t setError(string e, AstNode *n=nullptr, AstNode *helpfor=nullptr)
  {
    error = std::move(e);
    if (helpfor) {
      error += '\n';
      error += AstHelp::help(helpfor);
    }
    if (n) {
      errorLoc = n->loc;
    }
    else if (lastConsumedTokenValid) {
      errorLoc = lastConsumedToken->substr;
    }
    else {
      errorLoc = FormulaSubstr();
    }

    ok = false;
    if (sh.verbose >= 1) L() << sh.nameByCell[cell] << ": " << error;
    return nullptr;
  }

  inline TokenType peek()
  {
    if (p == end) return T_EOF;
    return p->t;
  }

  inline bool consume(TokenType t)
  {
    if (peek() == t) {
      lastConsumedToken = p;
      lastConsumedTokenValid = true;
      p++;
      return true;
    }
    return false;
  }

  inline bool consume(TokenType t, string_view &str)
  {
    if (peek() == t) {
      lastConsumedToken = p;
      lastConsumedTokenValid = true;
      str = tokenizer.getSubstr(*p);
      p++;
      return true;
    }
    return false;
  }

  AstNode *mkNumber(F value)
  {
    return mkAst(A_number, {}, AstNode::Data(value));
  }

  float parseLiteralNumber(string_view s)
  {
    return strtof(string(s).c_str(), nullptr);
  }

  u_long parseLiteralHex(string_view s)
  {
    return strtoul(string(s).c_str(), nullptr, 16);
  }

  FormulaSubstr lastloc()
  {
    if (lastConsumedTokenValid) {
      return lastConsumedToken->substr;
    }
    return FormulaSubstr(0, 0);
  }

  AstNode *setloc(AstNode *n)
  {
    if (n && lastConsumedTokenValid) {
      n->loc = lastConsumedToken->substr;
    }
    return n;
  }

  AstNode *setloc(AstNode *n, AstNode *from)
  {
    if (n && from) {
      n->loc = from->loc;
    }
    return n;
  }

  AstNode *setloc(AstNode *n, FormulaSubstr substr)
  {
    if (n) {
      n->loc = substr;
    }
    return n;
  }

  /*
    AST nodes go are held in the sheetlife pool: the lifetime of the sheet.
    They're used for a few purposes beyond code gen, like in the debug system.
  */
  AstNode *mkAst(AstType _t, initializer_list<AstNode *> _children, AstNode::Data _data=AstNode::Data{})
  {
    return setloc(AstNode::mk(&sh.sheetlife, _t, _children, _data));
  }
  AstNode *mkAst(AstType _t, vector<AstNode *> const &_children, AstNode::Data _data=AstNode::Data{})
  {
    return setloc(AstNode::mk(&sh.sheetlife, _t, _children, _data));
  }
  

  AstNode *parseBody()
  {
    return parseExpr();
  }

  AstNode *parseCellName()
  {
    string_view str;
    if (consume(T_ID, str)) {
      return mkAst(A_name, {}, sh.sheetlife.strdup(str));
    }
    else {
      return setError("Expected cell name");
    }
  }

  AstNode *parseAtom()
  {
    string_view str;
    if (consume(T_LPAR)) {
      auto ret = parseExpr();
      if (!ret) return nullptr;
      if (!consume(T_RPAR)) return setError("Expected closing ')'");
      return ret;
    }
    else if (consume(T_LBRK)) { // vector
      auto oploc = lastloc();
      AstNodeList args;
      while (1) {
        if (consume(T_RBRK)) break;
        auto arg = parseSwitch();
        if (!arg) return nullptr;
        args.push_back(arg);
        if (consume(T_RBRK)) break;
        if (!consume(T_COMMA)) return setError("Expected ',' or ']'");
      }
      return setloc(mkAst(A_vector, args), oploc);
    }
    else if (consume(T_NUMBER, str)) {
      auto value = parseLiteralNumber(str);
      auto num = mkNumber(value);
      // 2.7im gets parsed as mul(number(2.7), im())
      if (peek() == T_IM || peek() == T_ID) {
        auto x = parseAtom();
        if (!x) return nullptr;
        return mkAst(A_mul, {num, resolveName(x)});
      }
      return num;
    }
    else if (consume(T_ID, str)) {

      auto ret = mkAst(A_name, {}, sh.sheetlife.strdup(str));

      if (consume(T_LWING)) {
        auto wingLoc = lastConsumedToken->substr;
        auto retCell = resolveName(ret);
        if (!retCell || retCell->t != A_cellref) return setError("Expected cellref");
        AstNodeList subs{retCell};

        while (1) {
          if (peek() == T_RWING) break;
          auto name = parseCellName();
          if (!name) return nullptr; // error will have been set by parseCellName

          AstType augMode = A_null;
          if (consume(T_EQ)) {
            augMode = A_null;
          }
          else if (consume(T_PLUSEQ)) {
            augMode = A_add;
          }
          else if (consume(T_MINUSEQ)) {
            augMode = A_sub;
          }
          else {
            return setError("Expected '=', '+=', '-='");;
          }

          auto expr = parseExpr();
          if (!expr) return nullptr; // error will have been set by parseExpr
          auto nameCell = resolveName(name);
          if (!nameCell || nameCell->t != A_cellref) {
            // WRITEME
          }
          if (augMode != A_null) {
            expr = mkAst(augMode, {nameCell, expr});
          }

          subs.push_back(nameCell);
          subs.push_back(expr);

          if (peek() == T_RWING) break;
          if (!consume(T_COMMA)) return setError("Expected ',' or '}'");
        }
        if (!consume(T_RWING)) return setError("Expected '}'");
        ret = setloc(mkAst(A_hypobinding, subs), wingLoc);
      }

      if (consume(T_AT)) {
        if (!consume(T_NUMBER, str)) return setError("Expected number");
        auto value = parseLiteralNumber(str);
        return mkAst(A_paramref, {resolveName(ret)}, ParamIndex(value));
      }

      return ret;
    }
    else if (consume(T_COLOR, str)) {
      if (str.size() == 9 && str[0] == '#') {
        AstNodeList args;
        for (auto ofs = 1; ofs < 9; ofs += 2) {
          args.push_back(mkAst(A_constant, {}, clamp(parseLiteralHex(str.substr(ofs, 2))/255.0f, 0.0f, 1.0f)));
        }
        return mkAst(A_vector, args);
      }
      else if (str.size() == 7 && str[0] == '#') {
        AstNodeList args;
        for (auto ofs = 1; ofs < 7; ofs += 2) {
          args.push_back(mkAst(A_constant, {}, clamp(parseLiteralHex(str.substr(ofs, 2))/255.0f, 0.0f, 1.0f)));
        }
        args.push_back(mkAst(A_constant, {}, 1.0f));
        return mkAst(A_vector, args);
      }
      else {
        return setError("Invalid color: must be hex #RRGGBBAA");
      }
    }
    else if (consume(T_STRING, str)) {
      return mkAst(A_string, {}, lastConsumedToken->sv_value.begin());
    }
    else if (consume(T_IM)) {
      return mkAst(A_im, {});
    }
    else if (consume(T_IF)) {
      if (!consume(T_LPAR)) return setError("Expected '('");

      auto test = parseExpr();
      if (!test) return nullptr;
      if (!consume(T_RPAR)) return setError("Expected closing ')'");

      AstNode *ifTrue = nullptr;
      AstNode *ifFalse = nullptr;
      if (consume(T_LWING)) {
        ifTrue = parseFormula();
        if (!ifTrue) return nullptr;
        if (!consume(T_RWING)) {
          return setError("Expected '}'");
        }
      }
      else {
        ifTrue = parseExpr();
        if (!ifTrue) return nullptr;
      }
      if (consume(T_ELSE)) {
        if (consume(T_LWING)) {
          ifFalse = parseFormula();
          if (!ifFalse) return nullptr;
          if (!consume(T_RWING)) {
            return setError("Expected '}'");
          }
        }
        else {
          ifFalse = parseExpr();
          if (!ifFalse) return nullptr;
        }
      }
      if (ifFalse) {
        return mkAst(A_switch, {test, ifTrue, ifFalse});
      }
      else {
        return mkAst(A_switch, {test, ifTrue});
      }

    }
    else if (consume(T_SYMBOL, str)) {
      string symName(str);
      Symbol sym = sh.mkSymbol(symName);
      return mkAst(A_symbol, {}, sym);
    }
    else if (consume(T_WILDCARD)) {
      return mkAst(A_cellref, {}, lastConsumedToken->ci_value);
    }
    else if (peek() == T_LWING) {
      return parseAssoc();
    }
    else {
      return setError("Expected identifier");
    }
  }

  AstNode *resolveName(AstNode *a)
  {
    if (a->t == A_name) {
      string_view name = a->data_sv();
      if (name.size() > 0) {
        switch (name[0]) {

          case 'd':
            if (name == "dt") return setloc(mkAst(A_dt, {}), a);
            break;

          case 'e':
            if (name == "e") return setloc(mkAst(A_constant, {}, F(M_E)), a);
            break;

          case 'f':
            if (name == "fail.cellCount") return setloc(mkAst(A_failCellCount, {}), a);
            if (name == "fail.time") return setloc(mkAst(A_failTime, {}), a);
            break;

          case 'i':
            if (name == "initial") return setloc(mkAst(A_initial, {}), a);
            if (name == "inf") return setloc(mkAst(A_constant, {}, numeric_limits<F>::infinity()), a);
            break;

          case 'n':
            if (name == "nan") return setloc(mkAst(A_constant, {}, numeric_limits<F>::quiet_NaN()), a);
            break;

          case 'p':
            if (name == "pi") return setloc(mkAst(A_constant, {}, F(M_PI)), a);
            break;

          case 't':
            if (name == "time") return setloc(mkAst(A_time, {}), a);
            break;

          default:
            break;
        }
      }
    
      auto srcCell = sh.getCell(string(name));
      if (srcCell != nullCellIndex) {
        return setloc(mkAst(A_cellref, a->chvec(), srcCell), a);
      }
      return a;
    }
    return a;
  }

  AstNode *parseSuffixed()
  {
    auto lhs = parseAtom();
    if (!lhs) return nullptr;
    while (1) {
      if (lhs->t == A_name && consume(T_LPAR)) { // function call
        AstNodeList args;
        while (1) {
          if (consume(T_RPAR)) break;
          if (peek() == T_EOF) return setError("Expected value", nullptr, lhs);
          auto arg = parseSwitch();
          if (!arg) return nullptr;
          args.push_back(arg);
          if (consume(T_RPAR)) break;
          if (!consume(T_COMMA)) return setError("Expected ',' or ')'", nullptr, lhs);
        }
        lhs = setloc(mkCall(lhs, args), lhs);
        if (!lhs) return nullptr;
      }
      else if (consume(T_LBRK)) {
        auto oploc = lastloc();
        auto lhsName = resolveName(lhs);
        if (!lhsName) return nullptr;
        AstNodeList args{lhsName};
        while (1) {
          if (consume(T_RBRK)) break;
          auto arg = parseSwitch();
          if (!arg) return nullptr;
          args.push_back(arg);
          if (consume(T_RBRK)) break;
          if (!consume(T_COMMA)) return setError("Expected ',' or ']'");
        }
        lhs = setloc(mkAst(A_index, args), oploc);
      }
      else if (peek() == T_LWING) {
        AstNode *assocs = parseAssoc();
        lhs = mkAst(A_with, {lhs, assocs});
      }
      else {
        break;
      }
    }
    return resolveName(lhs);
  }

  AstNode *parseAssoc()
  {
    if (!consume(T_LWING)) return setError("Expected '{'");
    AstNode *assocs = nullptr;
    while (1) {
      if (consume(T_RWING)) break;
      string_view name;
      if (!consume(T_ID, name)) return setError("Expected name = value");
      if (!consume(T_EQ)) return setError("Expected '='");
      auto value = parseSwitch();
      if (!value) return nullptr;
      if (assocs) {
        assocs = mkAst(A_assoc, {assocs, value}, sh.sheetlife.strdup(name));
      }
      else {
        assocs = mkAst(A_assoc, {value}, sh.sheetlife.strdup(name));
      }
      if (consume(T_RWING)) break;
      if (!consume(T_COMMA)) return setError("Expected ',' or '}'");
    }
    if (!assocs) {
      assocs = mkAst(A_assoc, {}, "");
    }
    return assocs;
  }

  AstNode *parsePrefixed()
  {
    if (consume(T_MINUS)) {
      auto oploc = lastloc();
      auto arg = parsePrefixed();
      if (!arg) return nullptr;
      if (arg->t == A_number) {
        auto ret = mkNumber(- arg->data<F>());
        ret->loc = FormulaSubstr(oploc.begin, arg->loc.end);
        return ret;
      }
      else {
        return setloc(mkAst(A_neg, {arg}), oploc);
      }
    }
    else if (consume(T_PLUS)) {
      auto oploc = lastloc();
      auto arg = parsePrefixed();
      if (!arg) return nullptr;
      if (arg->t == A_number) {
        auto ret = mkNumber(arg->data<F>());
        ret->loc = FormulaSubstr(oploc.begin, arg->loc.end);
        return ret;
      }
      else {
        return arg;
      }
    }
    else if (consume(T_BANG)) {
      auto oploc = lastloc();
      auto arg = parsePrefixed();
      if (!arg) return nullptr;
      return setloc(mkAst(A_not, {arg}), oploc);
    }
    else {
      return parseSuffixed();
    }
  }

  AstNode *mkCall(AstNode *lhs, AstNodeList const &args)
  {
    if (lhs->t == A_name) {
      string_view name = lhs->data_sv();

      #define defname(NAME) if (name == string_view(#NAME)) return mkAst(A_##NAME, args)
      #define defalias(TOKEN, NAME) if (name == string_view(#TOKEN)) return mkAst(A_##NAME, args)
      #define defcast(NAME, TYPE) if (name == string_view(#NAME)) return mkAst(A_typecast, args, TYPE)
      switch (name[0]) {

       case 'a':
          defname(abs);
          defname(arg);
          defname(atan2);
          defname(arcArrowX);
          defname(arcArrowY);
          defname(arcArrowZ);
          defalias(arcArrow, arcArrowZ);
          defname(axisArrows);
          defname(arrow);
          defname(at);
          if (name[0] == 'a' && name[1] == 'p' && name[2] == 'i' && name[3] == '.') {
            return mkAst(A_api, args, sh.sheetlife.strdup(name.substr(4)));
          }

          break;

        case 'b':
          defcast(bool, typetag<bool>());
          defcast(beh, typetag<Beh>());
          defname(bandPlot);
          defname(block);
          break;

        case 'c':
          defname(check);
          defname(checkat);
          defname(checkbetween);
          defname(cos);
          defname(cosh);
          defname(ceil);
          defname(clamp);
          defname(cbrt);
          defname(cube);
          defname(colorize);
          defname(conj);
          defname(cond);
          defname(clampslew);
          defname(concat);
          defcast(complex, typetag<CF>());

          if (name.size() >= 4 && name[0] == 'c' && name[1] == 'v' && name[2] == 'e' && name[3] == 'c') {
            if (auto size = parseSize(name.substr(4))) {
              auto [nr, suffix] = *size;
              if (nr > 0) {
                if (suffix.empty()) {
                  return mkAst(A_typecast, args, TypeTag(TK_cmat, nr, 1));
                }
                else if (suffix == ".fourierbasis") {
                  return mkAst(A_fourierbasis, args, TypeTag(TK_cmat, nr, 1));
                }
                else if (suffix == ".linspaced") {
                  return mkAst(A_linspacedMat, args, TypeTag(TK_cmat, nr, 1));
                }
                else if (suffix == ".ones") {
                  return mkAst(A_onesMat, args, TypeTag(TK_cmat, nr, 1));
                }
                else if (suffix == ".zero") {
                  return mkAst(A_zeroMat, args, TypeTag(TK_cmat, nr, 1));
                }
                else if (suffix == ".constant") {
                  return mkAst(A_constantMat, args, TypeTag(TK_cmat, nr, 1));
                }
                else if (suffix == ".unit") {
                  return mkAst(A_unitMat, args, TypeTag(TK_cmat, nr, 1));
                }
              }
            }
          }

          if (name.size() >= 4 && name[0] == 'c' && name[1] == 'm' && name[2] == 'a' && name[3] == 't') {
            if (auto size = parseSize2(name.substr(4))) {
              auto [nr, nc, suffix] = *size;
              if (nr > 0 && nc > 0) {
                if (suffix.empty()) {
                  return mkAst(A_typecast, args, TypeTag(TK_cmat, nr, nc));
                }
                else if (suffix == ".zero") {
                  return mkAst(A_zeroMat, args, TypeTag(TK_cmat, nr, nc));
                }
                else if (suffix == ".identity") {
                  return mkAst(A_identityMat, args, TypeTag(TK_cmat, nr, nc));
                }
                else if (suffix == ".unit") {
                  return mkAst(A_unitMat, args, TypeTag(TK_cmat, nr, nc));
                }
              }
            }
          }
          break;

        case 'd':
          defname(dot);
          defname(distance);
          defname(determinant);
          defname(disk);
          defname(dial);
          defname(duration);
          defcast(drawop, typetag<DrawOp>());
          break;

        case 'e':
          defname(exp);
          defname(epicycles);
          defname(easein);
          defname(easeout);
          break;

        case 'f':
          defcast(float, typetag<F>());
          defname(floor);
          defname(fract);
          defname(fallback);
          defname(failure);
          break;

        case 'g':
          defname(graphColor);
          break;

        case 'h':
          defname(has);
          defname(hysteresis);
          defname(homogeneous);
          defname(hnormalized);
          defname(head);
          break;

        case 'i':
          defname(inverse);
          defname(imag);
          defname(isrunning);
          defname(issuccess);
          defname(isfailure);
          defname(integrate);
          break;

        case 'l':
          defname(log);
          defname(length);
          defname(line);
          defname(label);
          defname(lerp);
          defname(linePlot);
          defname(lpfilter1);
          defname(lpfilter2);
          defname(lookahead);
          defname(lanczosWindow);
          defname(latch);
          break;

        case 'm':
          defname(max);
          defname(min);
          defname(mbs);
          if (name == "minimize.newton") return mkAst(A_minpackSearch, args, U32(Minpack_NewtonDescent));
          if (name == "minimize.grad") return mkAst(A_minpackSearch, args, U32(Minpack_GradientDescent));
          if (name == "minimize.cgrad") return mkAst(A_minpackSearch, args, U32(Minpack_ConjugatedGradientDescent));
          if (name == "minimize.bfgs") return mkAst(A_minpackSearch, args, U32(Minpack_Bfgs));
          if (name == "minimize.lbfgs") return mkAst(A_minpackSearch, args, U32(Minpack_Lbfgs));
          if (name == "minimize.lbfgsb") return mkAst(A_minpackSearch, args, U32(Minpack_Lbfgsb));
          if (name == "minimize.particle") return mkAst(A_particleSearch, args, U32(ParticleSearch_swarm));
          if (name == "minimize.derand1bin") return mkAst(A_particleSearch, args, U32(ParticleSearch_deRand1Bin));
          if (name == "minimize.debest2bin") return mkAst(A_particleSearch, args, U32(ParticleSearch_deBest2Bin));

          defalias(mat2.rot, rotMat2);
          defalias(mat2.scale, scaleMat2);
          defalias(mat3.rotx, rotMat3X);
          defalias(mat3.roty, rotMat3Y);
          defalias(mat3.rotz, rotMat3Z);
          defalias(mat3.scale, scaleMat3);
          defalias(mat4.rotx, rotMat4X);
          defalias(mat4.roty, rotMat4Y);
          defalias(mat4.rotz, rotMat4Z);
          defalias(mat4.quat, quatMat4);
          defalias(mat4.rotxderiv, rotMat4Xderiv);
          defalias(mat4.rotyderiv, rotMat4Yderiv);
          defalias(mat4.rotzderiv, rotMat4Zderiv);
          defalias(mat4.translate, translateMat4);
          defalias(mat4.scale, scaleMat4);

          if (name.size() >= 3 && name[0] == 'm' && name[1] == 'a' && name[2] == 't') {
            if (auto size = parseSize2(name.substr(3))) {
              auto [nr, nc, suffix] = *size;
              if (nr > 0 && nc > 0) {
                if (suffix.empty()) {
                  return mkAst(A_typecast, args, TypeTag(TK_mat, nr, nc));
                }
                else if (suffix == ".identity") {
                  return mkAst(A_identityMat, args, TypeTag(TK_mat, nr, nc));
                }
                else if (suffix == ".zero") {
                  return mkAst(A_zeroMat, args, TypeTag(TK_mat, nr, nc));
                }
                else if (suffix == ".unit") {
                  return mkAst(A_unitMat, args, TypeTag(TK_mat, nr, nc));
                }
                else if (suffix == ".constant") {
                  return mkAst(A_constantMat, args, TypeTag(TK_mat, nr, nc));
                }
                else if (suffix == ".csvfile") {
                  return mkAst(A_csvfileMat, args, TypeTag(TK_mat, nr, nc));
                }
              }
            }
          }
          break;

        case 'n':
          defname(normangle);
          defname(normangle2);
          defname(normalize);
          defname(normalpdf);
          defname(normallogpdf);
          defname(normalcdf);
          defname(nuttallWindow);
          break;

        case 'o':
          defname(outerProduct);
          break;

        case 'p':
          defname(pow);
          defname(policyTerrain);
          defname(polar);
          defname(par);
          defname(piggyback);
          defname(particleSearch);
          defname(prod);
          defname(posquat);
          break;

        case 'r':
          defname(round);
          defname(rotMat2);
          defname(rotMat3X);
          defname(rotMat3Y);
          defname(rotMat3Z);
          defname(rotMat4X);
          defname(rotMat4Y);
          defname(rotMat4Z);
          defname(rotMat4Xderiv);
          defname(rotMat4Yderiv);
          defname(rotMat4Zderiv);
          defname(repr);
          defname(rect);
          defname(relu);
          defname(real);
          defname(rectangularWindow);
          defname(run);
          defname(random);
          defname(randomNormal);
          defname(roundhyst);
          break;

        case 's':
          defcast(scalar, typetag<F>());
          defcast(string, typetag<string_view>());
          defname(sin);
          defname(sinh);
          defname(sqrt);
          defname(sqr);
          defname(sign);
          defname(step);
          defname(smoothstep);
          defname(scaleMat3);
          defname(scaleMat4);
          defname(solid);
          defname(scatterPlot);
          defname(sweep);
          defname(spline);
          defname(seq);
          defname(success);
          defname(sum);
          defname(sphere);
          defname(segment);
          if (name == "solve.svd") return mkAst(A_solve, args, "jacobi_svd");
          if (name == "solve.jacobi_svd") return mkAst(A_solve, args, "jacobi_svd");
          if (name == "solve.bdc_svd") return mkAst(A_solve, args, "bdc_svd");
          if (name == "solve.full_piv_lu") return mkAst(A_solve, args, "full_piv_lu");
          if (name == "solve.householder_qr") return mkAst(A_solve, args, "householder_qr");
          if (name == "solve.cod") return mkAst(A_solve, args, "cod");
          break;

        case 't':
          defname(tan);
          defname(tanh);
          defname(transpose);
          defname(translateMat4);
          defname(text);
          defname(transform);
          defname(tukeyWindow);
          defname(tail);
          defname(tube);
          break;

        case 'v':
          defcast(videoframe, typetag<VideoFrame>());
          defname(vel);
          if (name.size() >= 3 && name[0] == 'v' && name[1] == 'e' && name[2] == 'c') {
            if (auto size = parseSize(name.substr(3))) {
              auto [nr, suffix] = *size;
              if (nr > 0) {
                if (suffix.empty()) {
                  return mkAst(A_typecast, args, TypeTag(TK_mat, nr, 1));
                }
                else if (suffix == ".linspaced") {
                  return mkAst(A_linspacedMat, args, TypeTag(TK_mat, nr, 1));
                }
                else if (suffix == ".ones") {
                  return mkAst(A_onesMat, args, TypeTag(TK_mat, nr, 1));
                }
                else if (suffix == ".zero") {
                  return mkAst(A_zeroMat, args, TypeTag(TK_mat, nr, 1));
                }
                else if (suffix == ".constant") {
                  return mkAst(A_constantMat, args, TypeTag(TK_mat, nr, 1));
                }
                else if (suffix == ".unit") {
                  return mkAst(A_unitMat, args, TypeTag(TK_mat, nr, 1));
                }
                else if (suffix == ".csvfile") {
                  return mkAst(A_csvfileMat, args, TypeTag(TK_mat, nr, 1));
                }
                else if (suffix == ".polybasis") {
                  return mkAst(A_polybasis, args, TypeTag(TK_mat, nr, 1));
                }
                else if (suffix == ".chebbasis") {
                  return mkAst(A_chebbasis, args, TypeTag(TK_mat, nr, 1));
                }
              }
            }
          }
          break;

        // ADD ast under the appropriate first letter

        default:
          break;
      }
      #undef defname
      #undef defcast
      #undef defalias

      return setError("Unknown function "s + string(name) + "(" + to_string(args.size()) + ")", lhs);
    }
    else {
      return setError("Function not a name", lhs);
    }
  }

  AstNode *parseParam()
  {
    auto lhs = parsePrefixed();
    if (!lhs) return nullptr;
    if (consume(T_TILDA)) {
      DistType dist = Dist_linear;
      if (consume(T_PLUS)) {
        dist = Dist_nonnegative;
      }
      else if (consume(T_STAR)) {
        dist = Dist_exponential;
      }
      string_view str;
      if (consume(T_NUMBER, str)) {
        auto range = parseLiteralNumber(str);
        FormulaSubstr rangeLoc = lastloc();
        return withParams(lhs, range, dist, rangeLoc);
      }
      else {
        return setError("Expected range {+, *}<number>");
      }
    }
    else {
      return lhs;
    }
  }

  optional<tuple<AstNode *, AstNode *>> isStdComplex(AstNode *n)
  {
    if (!n) return nullopt;
    if (n->t == A_add && n->nch() == 2) {
      auto a = n->ch(0), b = n->ch(1);
      if (a->t == A_number && b->t == A_mul && b->nch() == 2) {
        auto c = b->ch(0), d = n->ch(1);
        if (c->t == A_number && d->t == A_im) {
          return make_tuple(a, b);
        }
      }
    }
    return nullopt;
  }

  ParamIndex mkParam()
  {
    ParamIndex paramIndex;
    if (!paramsForReuse.empty()) {
      paramIndex = paramsForReuse.back();
      paramsForReuse.pop_back();
      assert(sh.cellByParam[paramIndex] == cell);
    }
    else {
      paramIndex = sh.mkParam(cell);
    }
    return paramIndex;
  }

  AstNode *withParams(AstNode *n, float range, DistType dist, FormulaSubstr rangeLoc)
  {
    if (!n) {
      return nullptr;
    }
    else if (n->t == A_number) {
      ParamIndex paramIndex = mkParam();
      sh.params.mutValueByParam(paramIndex) = n->data<F>();
      sh.rangeByParam[paramIndex] = range;
      sh.distByParam[paramIndex] = dist;
      sh.valueLocByParam[paramIndex] = n->loc;
      sh.rangeLocByParam[paramIndex] = rangeLoc;
      auto ret = mkAst(A_param, {}, paramIndex);
      ret->loc = n->loc;
      return ret;
    }
    else if (n->t == A_constant) {
      return n;
    }
    else if (n->t == A_param) {
      return n;
    }
    else if (n->t == A_add && n->nch() == 2 &&
             n->ch(0)->t == A_number && n->ch(1)->t == A_mul && n->ch(1)->nch() == 2 &&
             n->ch(1)->ch(0)->t == A_number && n->ch(1)->ch(1)->t == A_im) {
      
      auto realParam = withParams(n->ch(0), range, Dist_complexReal, rangeLoc);
      auto imagParam = withParams(n->ch(1)->ch(0), range, Dist_complexImag, rangeLoc);

      auto imagVal = setloc(mkAst(A_mul, {imagParam, n->ch(1)->ch(1)}), n->ch(1));

      return setloc(mkAst(A_add, {realParam, imagVal}), n);
    }
    else if (n->nch() > 0) {
      AstNodeList newArgs;

      bool changeFlag = false;
      for (int i = 0; i < n->nch(); i++) {
        auto arg = n->ch(i);
        auto newArg = withParams(arg, range, dist, rangeLoc);
        if (newArg != arg) changeFlag = true;
        newArgs.push_back(newArg);
      }
      if (changeFlag) {
        return setloc(mkAst(n->t, newArgs, n->data_), n);
      }
    }

    return n;
  }

  AstNode *parsePower()
  {
    if (auto lhs = parseParam()) {
      // Exponentiation isn't associative. Use parens if you want this.
      if (consume(T_HAT)) {
        auto oploc = lastloc();
        if (auto rhs = parseParam()) {
          return setloc(mkAst(A_pow, {lhs, rhs}), oploc);
        }
      }
      else if (consume(T_DOTHAT)) {
        auto oploc = lastloc();
        if (auto rhs = parseParam()) {
          return setloc(mkAst(A_comppow, {lhs, rhs}), oploc);
        }
      }
      return lhs;
    }
    return nullptr;
  }

  AstNode *parseProduct()
  {
    if (auto lhs = parsePower()) {
      while (1) {
        if (consume(T_STAR)) {
          auto oploc = lastloc();
          if (auto rhs = parsePower()) {
            lhs = setloc(mkAst(A_mul, {lhs, rhs}), oploc);
            continue;
          }
          return nullptr;
        }
        else if (consume(T_DIV)) {
          auto oploc = lastloc();
          if (auto rhs = parsePower()) {
            lhs = setloc(mkAst(A_div, {lhs, rhs}), oploc);
            continue;
          }
          return nullptr;
        }
        else if (consume(T_DOTSTAR)) {
          auto oploc = lastloc();
          if (auto rhs = parsePower()) {
            lhs = setloc(mkAst(A_compmul, {lhs, rhs}), oploc);
            continue;
          }
          return nullptr;
        }
        else if (consume(T_DOTDIV)) {
          auto oploc = lastloc();
          if (auto rhs = parsePower()) {
            lhs = setloc(mkAst(A_compdiv, {lhs, rhs}), oploc);
            continue;
          }
          return nullptr;
        }
        else {
          return lhs;
        }
      }
    }
    return nullptr;
  }

  AstNode *parseSum()
  {
    if (auto lhs = parseProduct()) {
      while (1) {
        if (consume(T_PLUS)) {
          auto oploc = lastloc();
          if (auto rhs = parseProduct()) {
            lhs = setloc(mkAst(A_add, {lhs, rhs}), oploc);
            continue;
          }
          return nullptr;
        }
        else if (consume(T_MINUS)) {
          auto oploc = lastloc();
          if (auto rhs = parseProduct()) {
            lhs = setloc(mkAst(A_sub, {lhs, rhs}), oploc);
            continue;
          }
          return nullptr;
        }
        else if (consume(T_DOTPLUS)) {
          auto oploc = lastloc();
          if (auto rhs = parseProduct()) {
            lhs = setloc(mkAst(A_compadd, {lhs, rhs}), oploc);
            continue;
          }
          return nullptr;
        }
        else if (consume(T_DOTMINUS)) {
          auto oploc = lastloc();
          if (auto rhs = parseProduct()) {
            lhs = setloc(mkAst(A_compsub, {lhs, rhs}), oploc);
            continue;
          }
          return nullptr;
        }
        else {
          return lhs;
        }
      }
    }
    return nullptr;
  }

  AstNode *parseShift()
  {
    if (auto lhs = parseSum()) {
      while (1) {
        if (consume(T_LEFT)) {
          auto oploc = lastloc();
          if (auto rhs = parseSum()) {
            lhs = setloc(mkAst(A_left, {lhs, rhs}), oploc);
            continue;
          }
          return nullptr;
        }
        else if (consume(T_RIGHT)) {
          auto oploc = lastloc();
          if (auto rhs = parseSum()) {
            lhs = setloc(mkAst(A_right, {lhs, rhs}), oploc);
            continue;
          }
          return nullptr;
        }
        else {
          return lhs;
        }
      }
    }
    return nullptr;
  }

  AstNode *parseDotCompare()
  {
    if (auto lhs = parseShift()) {
      while (1) {
        if (consume(T_DOTEQEQ)) {
          auto oploc = lastloc();
          if (auto rhs = parseShift()) {
            lhs = setloc(mkAst(A_compeq, {lhs, rhs}), oploc);
            continue;
          }
          return nullptr;          
        }
        else if (consume(T_DOTBANGEQ)) {
          auto oploc = lastloc();
          if (auto rhs = parseShift()) {
            lhs = setloc(mkAst(A_compneq, {lhs, rhs}), oploc);
            continue;
          }
          return nullptr;          
        }
        else if (consume(T_DOTGE)) {
          auto oploc = lastloc();
          if (auto rhs = parseShift()) {
            lhs = setloc(mkAst(A_compge, {lhs, rhs}), oploc);
            continue;
          }
          return nullptr;          
        }
        else if (consume(T_DOTGT)) {
          auto oploc = lastloc();
          if (auto rhs = parseShift()) {
            lhs = setloc(mkAst(A_compgt, {lhs, rhs}), oploc);
            continue;
          }
          return nullptr;          
        }
        else if (consume(T_DOTGE)) {
          auto oploc = lastloc();
          if (auto rhs = parseShift()) {
            lhs = setloc(mkAst(A_compge, {lhs, rhs}), oploc);
            continue;
          }
          return nullptr;          
        }
        else if (consume(T_DOTLE)) {
          auto oploc = lastloc();
          if (auto rhs = parseShift()) {
            lhs = setloc(mkAst(A_comple, {lhs, rhs}), oploc);
            continue;
          }
          return nullptr;          
        }
        else if (consume(T_DOTLT)) {
          auto oploc = lastloc();
          if (auto rhs = parseShift()) {
            lhs = setloc(mkAst(A_complt, {lhs, rhs}), oploc);
            continue;
          }
          return nullptr;          
        }
        else {
          return lhs;
        }
      }
    }
    return nullptr;
  }

  AstNode *parseCompare()
  {
    auto lhs = parseDotCompare();
    if (!lhs) return nullptr;

    if (peek() == T_LE || peek() == T_LT || peek() == T_GE || peek() == T_GT || peek() == T_TILTIL) {
      AstNodeList args{lhs};
      FormulaSubstr firstOpLoc;
      bool firstOpLocSet = false;
      FormulaSubstr lastOpLoc;
      
      while (1) {
        if (consume(T_LE)) {
          auto oploc = lastloc();
          lastOpLoc = oploc;
          if (!firstOpLocSet) firstOpLoc = oploc;
          firstOpLocSet = true;
          auto rhs = parseDotCompare();
          args.push_back(setloc(mkAst(A_le, {rhs}), oploc));
        }
        else if (consume(T_LT)) {
          auto oploc = lastloc();
          lastOpLoc = oploc;
          if (!firstOpLocSet) firstOpLoc = oploc;
          firstOpLocSet = true;
          auto rhs = parseDotCompare();
          args.push_back(setloc(mkAst(A_lt, {rhs}), oploc));
        }
        else if (consume(T_GE)) {
          auto oploc = lastloc();
          lastOpLoc = oploc;
          if (!firstOpLocSet) firstOpLoc = oploc;
          firstOpLocSet = true;
          auto rhs = parseDotCompare();
          args.push_back(setloc(mkAst(A_ge, {rhs}), oploc));
        }
        else if (consume(T_GT)) {
          auto oploc = lastloc();
          lastOpLoc = oploc;
          if (!firstOpLocSet) firstOpLoc = oploc;
          firstOpLocSet = true;
          auto rhs = parseDotCompare();
          args.push_back(setloc(mkAst(A_gt, {rhs}), oploc));
        }
        else if (consume(T_TILTIL)) {
          auto oploc = lastloc();
          lastOpLoc = oploc;
          if (!firstOpLocSet) firstOpLoc = oploc;
          firstOpLocSet = true;
          auto rhs = parseDotCompare();
          args.push_back(setloc(mkAst(A_sim, {rhs}), oploc));
        }
        else {
          break;
        }
      }
      return setloc(mkAst(A_compare, args), FormulaSubstr(firstOpLoc.begin, lastOpLoc.end));
    }
    return lhs;
  }

  AstNode *parseSimilar()
  {
    auto lhs = parseCompare();
    if (!lhs) return nullptr;
    if (consume(T_TILTILTIL)) {
      auto oploc = lastloc();
      auto rhs = parseCompare();
      if (!rhs) return nullptr;
      lhs = setloc(mkAst(A_mbs, {lhs, rhs}), oploc);
    }
    return lhs;
  }

  AstNode *parseEquals()
  {
    auto lhs = parseSimilar();
    if (!lhs) return nullptr;
    if (consume(T_EQEQ)) {
      auto oploc = lastloc();
      auto rhs = parseSimilar();
      if (!rhs) return nullptr;
      lhs = setloc(mkAst(A_eq, {lhs, rhs}), oploc);
    }
    else if (consume(T_BANGEQ)) {
      auto oploc = lastloc();
      auto rhs = parseSimilar();
      if (!rhs) return nullptr;
      lhs = setloc(mkAst(A_neq, {lhs, rhs}), oploc);
    }
    return lhs;
  }

  AstNode *parseIntersection()
  {
    auto lhs = parseEquals();
    if (!lhs) return nullptr;
    while (consume(T_AND)) {
      auto oploc = lastloc();
      auto rhs = parseEquals();
      if (!rhs) return nullptr;
      lhs = setloc(mkAst(A_intersection, {lhs, rhs}), oploc);
    }
    return lhs;
  }

  AstNode *parseUnion()
  {
    auto lhs = parseIntersection();
    if (!lhs) return nullptr;
    while (consume(T_OR)) {
      auto oploc = lastloc();
      auto rhs = parseIntersection();
      if (!rhs) return nullptr;
      lhs = setloc(mkAst(A_union, {lhs, rhs}), oploc);
    }
    return lhs;
  }

  AstNode *parseAnd()
  {
    auto lhs = parseUnion();
    if (!lhs) return nullptr;
    while (consume(T_ANDAND)) {
      auto oploc = lastloc();
      auto rhs = parseUnion();
      if (!rhs) return nullptr;
      lhs = setloc(mkAst(A_and, {lhs, rhs}), oploc);
    }
    return lhs;
  }

  AstNode *parseOr()
  {
    auto lhs = parseAnd();
    if (!lhs) return nullptr;
    while (consume(T_OROR)) {
      auto oploc = lastloc();
      auto rhs = parseAnd();
      if (!rhs) return nullptr;
      lhs = setloc(mkAst(A_or, {lhs, rhs}), oploc);
    }
    return lhs;
  }

  AstNode *parseImplies()
  {
    auto lhs = parseOr();
    if (!lhs) return nullptr;
    while (consume(T_PTR)) {
      auto oploc = lastloc();
      auto rhs = parseOr();
      if (!rhs) return nullptr;
      lhs = setloc(mkAst(A_implies, {lhs, rhs}), oploc);
    }
    return lhs;
  }

  AstNode *parseSwitch()
  {
    auto lhs = parseImplies();
    if (!lhs) return nullptr;
    if (consume(T_QUERY)) {
      auto oploc = lastloc();
      auto ifTrue = parseImplies();
      if (!ifTrue) return nullptr;
      if (!consume(T_COLON)) return setError("Expected ':' to match '?'");
      auto ifFalse = parseSwitch();  // right-to-left associative
      if (!ifFalse) return nullptr;
      return setloc(mkAst(A_switch, {lhs, ifTrue, ifFalse}), oploc);
    }
    return lhs;
  }

  AstNode *parseExpr()
  {
    auto lhs = parseSwitch();
    if (!lhs) return nullptr;
    if (consume(T_EQ)) {
      auto oploc = lastloc();
      auto rhs = parseExpr(); // right-to-left associative
      if (!rhs) return nullptr;
      return setloc(mkAst(A_assign, {lhs, rhs}), oploc);
    }
    return lhs;
  }

  AstNode *parseSymbol()
  {
    string_view str;
    if (consume(T_SYMBOL, str)) {
      string symName(str);
      Symbol sym = sh.mkSymbol(symName);
      return mkAst(A_symbol, {}, sym); 
    }
    return setError("Expected symbol");
  }

  AstNode *parseFormula()
  {
    AstNodeList exprs;
    while (1) {
      consume(T_SEMI); // ignore
      if (consume(T_EOF)) break;
      auto e = parseExpr();
      if (!e) return nullptr;
      exprs.push_back(e);
      if (consume(T_SEMI)) {
        continue;
      }
      else if (consume(T_EOF)) {
        break;
      }
      else if (peek() == T_RWING) {
        break;
      }
      else {
        return setError("Expected ';', '}', or eof");
      }
    }
    return mkAst(A_formula, exprs);
  }

  AstNode *parseCell()
  {
    return parseFormula();
  }
};


void Sheet::parseAll()
{
  CellSet redoParse;
  for (auto cell : needParseCells) {
    FormulaParser parser(*this, cell);
    if (parser.ok) {
      astByCell[cell] = parser.ast;
      parseErrorByCell[cell].clear();
      parseErrorLocByCell[cell] = FormulaSubstr(0, 0);
      needFlowCells[cell] = true;
      needsEmit = true;
    }
    else {
      astByCell[cell] = nullptr;
      parseErrorByCell[cell] = parser.error;
      parseErrorLocByCell[cell] = parser.errorLoc;
      redoParse[cell] = true;
    }
  }
  needParseCells = std::move(redoParse);
}


ostream & operator << (ostream &s, FormulaSubstr const &a)
{
  return s << a.begin << ":" << a.end;
}
