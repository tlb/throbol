#include "./core.h"
#include "./vbmainloop.h"
#include "./emit.h"

SheetApi::SheetApi(CompiledSheet &_compiled, ApiSpec const &_spec, string _apiVersion)
  : compiled(_compiled),
    sh(compiled.sh),
    apiType(_spec.apiType),
    apiInstance(_spec.apiInstance),
    apiVersion(std::move(_apiVersion)),
    apiProps(_spec.apiProps),
    apiCell(_spec.cell)
{
  label = apiInstance;

  if (auto it = assoc_F(apiProps, "verbose")) {
    verbose = int(*it);
  }
}

SheetApi::~SheetApi()
{
  if (verbose >= 2) L() << label << ": destructed";
  label += " (destructed)";
}

void SheetApi::logError(string_view s)
{
  if (verbose >= 0) L() << label << ": " << s;
  errors.push_back(string(s));
}

void SheetApi::txActuators(CellVals *vals, LiveRobotState &lrt)
{
}

void SheetApi::txWarmup(LiveRobotState &lrt)
{
}

void SheetApi::txCooldown(LiveRobotState &lrt)
{
}


void SheetApi::rxSensors(LiveRobotState &lrt)
{
}

void SheetApi::rxMonitors(LiveRobotState &lrt)
{
}

void SheetApi::doInteractive(ReplayBuffer &buffer, vector<int> const &doAdjustParams, int visCursIndex, bool shift)
{
}

bool SheetApi::startLive()
{
  lastData.alloc(replayBufferSize);
  lastData.zero();
  return true;
}


void SheetApi::bindApi()
{
  for (auto it : refs) {
    it->bindValue(*this);
  }
}

YAML::Node SheetApi::getSchema()
{
  YAML::Node ret;
  for (auto it : refs) {
    if (it->direction == API_NONE) continue;
    if (it->direction == API_SECTION) continue;
    YAML::Node ent;
    ent["name"] = it->name;
    ent["dir"] = apiDirectionName(it->direction);
    ent["ofs"] = it->replayValueOffset;
    ent["type"] = typeName(it->br.t);
    ent["version"] = typeVersion(it->br.t);
    ent["size"] = allocSize(it->br.t); // sanity check
    // Don't include it->br.v, since this isn't part of the on-disk schema but just where
    // it goes in a CellVals structure.
    ret.push_back(ent);
  }
  return ret;
}

YAML::Node CompiledSheet::getApiSchema()
{
  YAML::Node ret;
  for (auto const &api : apis) {
    YAML::Node ent;

    ent["apiType"] = api->apiType;
    ent["apiInstance"] = api->apiInstance;
    ent["apiVersion"] = api->apiVersion;
    ent["apiSchema"] = api->getSchema();
    ret.push_back(ent);
  }
  return ret;
}

ApiDirection apiDirectionByName(string const &name)
{
  if (name == "none") return API_NONE;
  if (name == "sensor") return API_SENSOR;
  if (name == "actuator") return API_ACTUATOR;
  if (name == "monitor") return API_MONITOR;
  if (name == "replay") return API_REPLAY;
  if (name == "section") return API_SECTION;
  return API_NONE;
}

string apiDirectionName(ApiDirection a)
{
  switch (a) {
    case API_NONE: return "none";
    case API_SENSOR: return "sensor";
    case API_ACTUATOR: return "actuator";
    case API_MONITOR: return "monitor";
    case API_REPLAY: return "replay";
    case API_SECTION: return "section";
    default: return "ApiDirection(" + repr(int(a)) + ")";
  }
}

ApiSchemaConv::ApiSchemaConv(SheetApi &_api, YAML::Node const &_dbSchema, string const &_dbVersion)
  : api(_api),
    dbSchema(_dbSchema),
    dbVersion(_dbVersion)
{
  setup();
}

void ApiSchemaConv::setup()
{
  ok = true; // set to false if enough goes wrong
  for (auto it : dbSchema) {
    auto name = it["name"].as<string>();
    auto ofs = ValueOffset(it["ofs"].as<double>());
    auto type = typeByName(it["type"].as<string>());
    auto dir = apiDirectionByName(it["dir"].as<string>());
    auto version = it["version"].as<int>(0);
    auto size = it["size"].as<int>(-1);
    dbEntries[name] = ApiDbSchemaEnt{ofs, type, dir, size, version, false};
  }

  notes << "Conv " << dbVersion << " -> " <<
    api.apiType << ":" << api.apiVersion << "\n";

  for (auto it : api.refs) {
    if (it->direction == API_SENSOR ||
        it->direction == API_ACTUATOR ||
        it->direction == API_MONITOR ||
        it->direction == API_REPLAY) {
      // this will barf if it's not a concrete type

      auto &ent = dbEntries[it->name];
      if (!ent) {
        notes << "  API expects " << it->name << 
          " of type " << it->br.t <<
          " but DB has nothing with that name.\n";
        // give subclasses a chance to do something else?
        continue;
      }
      if (ent->used) {
        notes << "  API expects " << it->name << 
          " of type " << it->br.t <<
          " which has already been used once. Using it again anyway.\n";
      }
      if (ent->type != it->br.t) {
        // Handle straightforward type conversion here, like
        // if we add a bool type to replace some usages of float
        notes << "  API expects " << it->name << 
          " to have type " << it->br.t <<
          " but DB has type " << ent->type << " instead.\n";
        // give subclasses a chance to do something else?
        ok = false;
        continue;
      }

      if (ent->version != typeVersion(it->br.t)) {
        // Add conversion between versions of types here,
        // like if we add a field to VideoFrame
        notes << "  API expects " << it->name << 
          " of type " << it->br.t <<
          " to have version " << typeVersion(it->br.t) <<
          " but DB has version " << ent->version << " instead.\n";
        ok = false;
        continue;
      }
      if (ent->size != -1 && ent->size != int(allocSize(it->br.t))) {
        notes << "  API expects " << it->name << 
          " of type " << it->br.t <<
          " to have size " << allocSize(it->br.t) <<
          " but DB has size " << ent->size << " instead.\n";
        ok = false;
        continue;
      }
      ent->used = true;

      auto memOfs = it->replayValueOffset;
      auto memSize = allocSize(it->br.t);
      assert(memOfs + memSize <= api.replayBufferSize);
      auto dbOfs = ent->replayValueOffset;
      
      if (it->br.t.ist<F>()) {
        // specialized code for float, but not a big advantage
        notes << "  Copy " << it->name << ": *(F *)&mem[" << memOfs << 
          "] = *(F *)&db[" << dbOfs << "]\n";
        fns.push_back([memOfs, dbOfs](U8 *toMem, U8 const *fromDb) {
          *(F *)&toMem[memOfs] = *(F *)&fromDb[dbOfs];
        });
      }
      // Handle any types here that can't be simply memcpyd
      else {
        notes << "  Copy " << it->name << ": memcpy(&mem[" << memOfs << 
          "], &db[" << dbOfs << "], " << memSize << ")\n";
        fns.push_back([memOfs, dbOfs, memSize](U8 *toMem, U8 const *fromDb) {
          memcpy(&toMem[memOfs], &fromDb[dbOfs], memSize);
        });
      }
    }
  }

  for (auto [itName, itVal] : dbEntries) {
    if (itVal && !itVal->used) {
      notes << "  Ignored DB entry " << itName << 
        " of type " << itVal->type << 
        " and direction " << itVal->direction << "\n";
    }
  }
  api.finalizeConv(*this);
}

Blob ApiSchemaConv::run(U8 const *fromDbData, size_t fromDbSize)
{
  Blob toMem(api.replayBufferSize);
  toMem.zero();
  for (auto &f : fns) {
    f(toMem.data(), fromDbData);
  }
  return toMem;
}

void SheetApi::finalizeConv(ApiSchemaConv &conv)
{
  // override if you need to do something
}


void SheetApi::copyToReplayBuffer(Trace &tr, ReplayBuffer &replay, bool doSensors, bool doActuators)
{
  auto &blobs = replay.replayByApiInstance[apiInstance];
  blobs.resize(tr.nVals());
  for (int li = 0; li < tr.nVals(); li++) {
    copyToReplayBuffer(tr.getVal(li), blobs[li], doSensors, doActuators);
  }
}

void SheetApi::copyFromReplayBuffer(Trace &tr, ReplayBuffer &replay,  bool doSensors, bool doActuators)
{
  auto &blobs = replay.replayByApiInstance[apiInstance];
  for (size_t li = 0; li < blobs.size(); li++) {
    while (li >= tr.vals.size()) {
      tr.vals.push_back(CellVals::mk(tr.compiled.get(), tr.tracelife.get()));
    }
    copyFromReplayBuffer(tr.vals[li], blobs[li], doSensors, doActuators);
  }
}


void SheetApi::copyToReplayBuffer(CellVals *ref, Blob &buffer, bool doSensors, bool doActuators)
{
  if (buffer.empty()) {
    buffer.alloc(replayBufferSize);
    buffer.zero();
  }

  assertlog(buffer.size() >= replayBufferSize, LOGV(buffer.size()) << LOGV(replayBufferSize));
  for (auto it : refs) {
    if (!it->valid()) continue;
    if (it->direction == API_SENSOR && !doSensors) continue;
    if (it->direction == API_ACTUATOR && !doActuators) continue;
    it->copyToReplayBuffer(ref, buffer);
  }
}

void SheetApi::copyFromReplayBuffer(CellVals *ref, Blob const &buffer, bool doSensors, bool doActuators)
{
  assertlog(buffer.size() == replayBufferSize, LOGV(buffer.size()) << 
    LOGV(replayBufferSize) << " schema:\n" << getSchema());
  for (auto it : refs) {
    if (!it->valid()) continue;
    if (it->direction == API_SENSOR && !doSensors) continue;
    if (it->direction == API_ACTUATOR && !doActuators) continue;
    it->copyFromReplayBuffer(ref, const_cast<Blob &>(buffer));
  }
}

void SheetApi::setKbd(std::array<bool, API_KBD_NKEYS> const &_down)
{
}



GenericApiRef::GenericApiRef(SheetApi &_owner, string const &_name, TypeTag t, ApiDirection _direction)
  : owner(_owner),
    name(dotJoin(owner.apiInstance, _name)),
    direction(_direction),
    replayValueOffset(nullValueOffset)
{
  // only bind type and cell now, we'll figure out the offset later in bindValue
  br.t = t;
  br.cell = owner.sh.getCell(name);
  if (br.cell != nullCellIndex && owner.compiled.apiDirectionByCell[br.cell] != API_NONE) {
    logError(string(name) + " multiply defined");
  }
  owner.refs.push_back(this);
  if (br.cell != nullCellIndex) {
    owner.compiled.apiDirectionByCell[br.cell] = direction;
    owner.compiled.isTimeInvariantByCell[br.cell] = false;
  }
  if (!br.t.isUndef()) {
    replayValueOffset = allocIncr(owner.replayBufferSize, br.t, 1);
  }
}

GenericApiRef::~GenericApiRef()
{
}


bool GenericApiRef::valid()
{
  return br.valid();
}

void GenericApiRef::logError(string const &s)
{
  if (!error.empty()) error += '\n';
  error += s;
  owner.logError(owner.sh.getCellName(br.cell) + ": " + s);
}

void GenericApiRef::bindValue(SheetApi &owner)
{
  if (br.cell != nullCellIndex) {
    // Get the full ref from the partial ref
    auto fullbr = owner.compiled.getValue(br.cell);
    if (fullbr.valid()) {
      if (fullbr.t == br.t) {
        br = fullbr;
      }
      else {
        ostringstream s;
        s << "type mismatch. " <<
          " Got " << fullbr.t <<
          " but " << owner.apiType << "@" + owner.apiVersion <<
          " expected " << br.t;
        logError(s.str());
      }
    }
    else {
      logError("no value");
    }
  }
}

bool SheetApi::ok()
{
  return errors.empty();
}

void SheetApi::stopLive()
{
}

void SheetApi::simAdvance(CellVals *prev, CellVals *next, Trace *tr, int bi)
{
  // nothing by default
}




ostream & operator <<(ostream &s, ApiDirection const &a)
{
  return s << apiDirectionName(a);
}


map<string, ApiRegister *> *ApiRegister::registryByType;

ApiRegister::ApiRegister(
    char const *_type,
    function<shared_ptr<SheetApi>(CompiledSheet &_compiled, ApiSpec const &_spec)> _createApi)
  : type(_type),
    createApi(_createApi)
{
  if (!registryByType) registryByType = new map<string, ApiRegister *>();
  (*registryByType)[_type] = this;
}

shared_ptr<SheetApi> ApiRegister::mk(CompiledSheet &compiled, ApiSpec const &_spec)
{
  if (registryByType) {
    auto reg = (*registryByType)[_spec.apiType];
    if (reg) {
      return reg->createApi(compiled, _spec);
    }
  }
  return nullptr;
}

map<string, string> CompiledSheet::getExpectedApiVersions()
{
  map<string, string> ret;
  for (auto const &api : apis) {
    ret[api->apiType] = api->apiVersion;
  }
  return ret;
}

bool CompiledSheet::checkExpectedApiVersions(
  map<string, string> const &expectedApiVersions,
  function<void(string const &)> onErr)
{
  bool ok = true;
  for (auto const &[apiType, expectedVersion] : expectedApiVersions) {
    bool found = false;
    for (auto const &api : apis) {
      if (api->apiType == apiType) {
        found = true;
        if (api->apiVersion != expectedVersion) {
          onErr("API version mismatch: client expected " + expectedVersion + " but I have " + api->apiVersion);
          ok = false;
        }
      }
    }
    if (!found) {
      onErr("No API " + apiType);
      ok = false;
    }
  }
  return ok;
}

ostream & operator <<(ostream &s, LiveRobotState::State const &a)
{
  switch (a) {
    case LiveRobotState::LRS_idle: return s << "idle";
    case LiveRobotState::LRS_warmup: return s << "warmup";
    case LiveRobotState::LRS_running: return s << "running";
    case LiveRobotState::LRS_cooldown: return s << "cooldown";
    case LiveRobotState::LRS_done: return s << "done";
    case LiveRobotState::LRS_emergency: return s << "emergency";
    case LiveRobotState::LRS_error: return s << "error";
    default: return s << "LiveRobotState::State(" << int(a) << ")";
  }
}



bool ReplayBuffer::empty() const
{
  for (auto &[apiInstance, replay] : replayByApiInstance) {
    if (!replay.empty()) return false;
  }
  return true;
}


ostream & operator <<(ostream &s, WithSheet<ApiHandle> const &sha)
{
  auto &[sh, a] = sha;
  s << "api." << a.apiType << "()" << WithSheet(sh, a.props);
  return s;
}

