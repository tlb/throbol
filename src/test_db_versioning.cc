#include "./test_utils.h"
#include <sqlite3.h>
#include <sqlite_modern_cpp.h>

using namespace sqlite;

/*
  Should a deleted cell be represented as a cell with an empty formula?
*/

void updateCell(database &db, string const &sheetName, string const &cellName, string const &formula)
{
  static long timestamp;

  timestamp++;

  db << "BEGIN TRANSACTION;";

  if (1) {
    db << R"(
      INSERT OR REPLACE INTO sheetVersion (sheetName, version)
        VALUES (?, ?);)"
      << sheetName << timestamp;
  }

  if (1) {
    db << R"(
      INSERT INTO sheetFormulaVersions (sheetName, cellName, version, formula)
        VALUES (?, ?, ?, ?);)"
      << sheetName << cellName << timestamp << formula;
  }

  if (1) {
    db << R"(
      INSERT OR REPLACE INTO sheetFormulas (sheetName, cellName, formula)
        VALUES (?, ?, ?);)"
      << sheetName << cellName << formula;
  }

  db << "COMMIT;";

}

TEST_CASE("DB versioning", "[.][perf][db]") {
  const char *dbfn = "/tmp/test-versioning.db";
  unlink(dbfn);
  database db(dbfn);

  db << "PRAGMA synchronous = 0;";

  db << R"(
    CREATE TABLE sheetFormulas (
      sheetName TEXT,
      cellName TEXT,
      formula TEXT,
      PRIMARY KEY (sheetName, cellName)
    );)";

  /*
      comment TEXT,
      posX FLOAT4,
      posY FLOAT4
  */

  db << R"(
    CREATE TABLE sheetFormulaVersions (
      sheetName TEXT,
      cellName TEXT,
      version INTEGER,
      formula TEXT,
      PRIMARY KEY (sheetName, cellName, version)
    );)";

  db << R"(
    CREATE TABLE sheetVersion (
      sheetName TEXT,
      version INTEGER,
      PRIMARY KEY(sheetName)
    );)";


  perfReport("updateCell", [&]() {
    for (auto i = 0; i < 1000; i++) {
      updateCell(db, "foo", "bar", "v"s+repr(i));
    }
  }, 1000);
  
}
