#include "./core.h"
#include "../geom/gl_headers.h"
#include "../geom/g8.h"
#include "numerical/numerical.h"
#include "numerical/polyfit.h"
#include "./sheet_ui.h"
#include "./scope_math.h"
#include "./draw_gl.h"
#include "./mjutil.h"
#include "mujoco/mjvisualize.h"

static int isBehind(const float* headpos, const float* pos, const float* mat) 
{
  return ((headpos[0]-pos[0])*mat[2] +
          (headpos[1]-pos[1])*mat[5] +
          (headpos[2]-pos[2])*mat[8] < 0.0f);
}

static int isReflective(const mjvGeom &geom)
{
  return ((geom.type==mjGEOM_PLANE || geom.type==mjGEOM_BOX) &&
          !geom.transparent &&
          (geom.reflectance>0));
}

static float mjvRbound(const mjvGeom &g)
{
  if (g.objtype == mjOBJ_GEOM) {
    return g.modelrbound;
  }

  // compute rbound according to type
  const float *s = g.size;
  switch (g.type) {
    case mjGEOM_SPHERE:
      return s[0];

    case mjGEOM_CAPSULE:
      return (s[0]+s[2]);

    case mjGEOM_CYLINDER:
      return sqrtf(s[0]*s[0] + s[2]*s[2]);

    case mjGEOM_BOX:
      return sqrtf(s[0]*s[0] + s[1]*s[1] + s[2]*s[2]);
      break;

    default:  // not accurate for arrows, but they are not transparent
      return mjMAX(s[0], mjMAX(s[1], s[2]));
  }
}

static void addMjMesh(G8DrawList &dl, mjModel *m, mat4 pos, int meshid, int texid, F layer)
{
  int texcoordadr = texid < 0 ? -1 : m->mesh_texcoordadr[meshid];

  auto const *shader = texcoordadr>=0 ? (G8ShaderGeneric *)&litTexSolidShader : (G8ShaderGeneric *)&litSolidShader;
  auto restoreViBuf = dl.withViBuf(1, shader);
  auto &setup = dl.getSetup(shader);

  assert(meshid >= 0 && meshid < m->nmesh);
  auto &permcacheSlot = setup.perSW->geomCache[ObjectKey(&m->mesh_vertadr[meshid], 0)];
  if (!permcacheSlot) {

    int vertadr = m->mesh_vertadr[meshid];

    int faceBegin = m->mesh_faceadr[meshid];
    int faceEnd = faceBegin + m->mesh_facenum[meshid];


    G8IdxBlaster ib(setup, 3 * (faceEnd - faceBegin));
    U32 vi = 0;
    if (setup.vt == G8Vtx_pnul::vt) {
      G8VtxBlaster<G8Vtx_pnul> vb(setup, 3 * (faceEnd - faceBegin), vi);

      for (int face = faceBegin; face < faceEnd; face++) {
        vb(*(vec3 *)&m->mesh_vert[3 * (vertadr + m->mesh_face[3*face + 0])],
          *(vec3 *)&m->mesh_normal[3 * (vertadr + m->mesh_face[3*face + 0])],
          *(vec2 *)&m->mesh_texcoord[2 * (texcoordadr + m->mesh_face[3*face + 0])],
          layer);
        vb(*(vec3 *)&m->mesh_vert[3 * (vertadr + m->mesh_face[3*face + 1])],
          *(vec3 *)&m->mesh_normal[3 * (vertadr + m->mesh_face[3*face + 1])],
          *(vec2 *)&m->mesh_texcoord[2 * (texcoordadr + m->mesh_face[3*face + 1])],
          layer);
        vb(*(vec3 *)&m->mesh_vert[3 * (vertadr + m->mesh_face[3*face + 2])],
          *(vec3 *)&m->mesh_normal[3 * (vertadr + m->mesh_face[3*face + 2])],
          *(vec2 *)&m->mesh_texcoord[2 * (texcoordadr + m->mesh_face[3*face + 2])],
          layer);

        ib.tri(vi+0, vi+1, vi+2);
        vi += 3;
      }
    }
    else if (setup.vt == G8Vtx_pnl::vt) {
      G8VtxBlaster<G8Vtx_pnl> vb(setup, 3 * (faceEnd - faceBegin), vi);

      for (int face = faceBegin; face < faceEnd; face++) {
        vb(*(vec3 *)&m->mesh_vert[3 * (vertadr + m->mesh_face[3*face + 0])],
          *(vec3 *)&m->mesh_normal[3 * (vertadr + m->mesh_face[3*face + 0])],
          layer);
        vb(*(vec3 *)&m->mesh_vert[3 * (vertadr + m->mesh_face[3*face + 1])],
          *(vec3 *)&m->mesh_normal[3 * (vertadr + m->mesh_face[3*face + 1])],
          layer);
        vb(*(vec3 *)&m->mesh_vert[3 * (vertadr + m->mesh_face[3*face + 2])],
          *(vec3 *)&m->mesh_normal[3 * (vertadr + m->mesh_face[3*face + 2])],
          layer);

        ib.tri(vi+0, vi+1, vi+2);
        vi += 3;

      }
    }
    else if (setup.vt == G8Vtx_pncl::vt) {
      G8VtxBlaster<G8Vtx_pncl> vb(setup, 3 * (faceEnd - faceBegin), vi);

      for (int face = faceBegin; face < faceEnd; face++) {
        vb(*(vec3 *)&m->mesh_vert[3 * (vertadr + m->mesh_face[3*face + 0])],
          *(vec3 *)&m->mesh_normal[3 * (vertadr + m->mesh_face[3*face + 0])],
          vec4(1,1,1,1),
          layer);
        vb(*(vec3 *)&m->mesh_vert[3 * (vertadr + m->mesh_face[3*face + 1])],
          *(vec3 *)&m->mesh_normal[3 * (vertadr + m->mesh_face[3*face + 1])],
          vec4(1,1,1,1),
          layer);
        vb(*(vec3 *)&m->mesh_vert[3 * (vertadr + m->mesh_face[3*face + 2])],
          *(vec3 *)&m->mesh_normal[3 * (vertadr + m->mesh_face[3*face + 2])],
          vec4(1,1,1,1),
          layer);

        ib.tri(vi+0, vi+1, vi+2);
        vi += 3;

      }
    }
    else {
      return dl.badShader(shader, "addMjMesh", {G8Vtx_pnul::vt, G8Vtx_pnl::vt, G8Vtx_pncl::vt});
    }

    permcacheSlot = ib;
  }

  if (setup.vt == G8Vtx_pnul::vt && texid >= 0) {

    auto &texSlot = setup.perW->textureCache[ObjectKey(&m->tex_adr[texid], 0)];
    if (!texSlot) {

      uint32_t width = m->tex_width[texid];
      uint32_t height = m->tex_height[texid];

      L() << "addMjMesh: load texture " << width << "x" << height <<
        " shader=" << shader->name;

      WGPUTextureDescriptor tex_desc = {
        .label = "addMjMesh",
        .usage = WGPUTextureUsage_CopyDst | WGPUTextureUsage_TextureBinding,
        .dimension = WGPUTextureDimension_2D,
        .size = {
          .width = width,
          .height = height,
          .depthOrArrayLayers = 1,
        },
        .format = WGPUTextureFormat_BGRA8Unorm,
        .mipLevelCount = 1,
        .sampleCount = 1,
      };

      texSlot = wgpuDeviceCreateTexture(dl.win.wgpu_device, &tex_desc);

      WGPUImageCopyTexture dst_view = {
        .texture = *texSlot,
        .mipLevel = 0,
        .origin = { .x = 0, .y = 0, .z = 0 },
        .aspect = WGPUTextureAspect_All
      };
      WGPUTextureDataLayout layout = {
        .offset = 0,
        .bytesPerRow = width * 4,
        .rowsPerImage = height
      };
      WGPUExtent3D size = { 
        .width = width,
        .height = height,
        .depthOrArrayLayers = 1
      };

      // Oof size = large
      auto *rgb = &m->tex_rgb[m->tex_adr[texid]];
      auto *bgra = new uint8_t [width * 4 * height];
      auto src = rgb;
      auto dst = bgra;
      for (size_t i = 0; i < width * height; i++) {
        dst[0] = src[2];
        dst[1] = src[1];
        dst[2] = src[0];
        dst[3] = 0xff;
        dst += 4;
        src += 3;
      }

      wgpuQueueWriteTexture(dl.win.wgpu_queue, &dst_view, bgra, (uint32_t)(width * 4 * height), &layout, &size);

      delete [] bgra;
    }
    auto restoreModelUniforms = dl.withModelUniforms(ModelUniforms(pos), shader);
    auto restoreTexture = dl.withImgTex(*texSlot, shader);
    dl.addCmd(setup, *permcacheSlot);
  }
  else if (setup.vt == G8Vtx_pnl::vt) {
    auto restoreModelUniforms = dl.withModelUniforms(ModelUniforms(pos), shader);
    dl.addCmd(setup, *permcacheSlot);
  }
  else if (setup.vt == G8Vtx_pncl::vt) {
    auto restoreModelUniforms = dl.withModelUniforms(ModelUniforms(pos), shader);
    dl.addCmd(setup, *permcacheSlot);
  }
  else {
    return dl.badShader(shader, "addSolid", {G8Vtx_pnul::vt, G8Vtx_pnl::vt, G8Vtx_pncl::vt});
  }

}


static void renderGeom(G8DrawList &dl, DrawTraceAccum::Cmd &cmd, mjModel *m, mjvGeom &geom, float headpos[3])
{
  // make transformation matrix
  mat4 tpos = *cmd.posx * mjMat4FromOrientPos(geom.mat, geom.pos);

  if (0) dl.addArrowTriple(tpos);

  int behind = isBehind(headpos, geom.pos, geom.mat);

  vec4 col(geom.rgba[0], geom.rgba[1], geom.rgba[2], geom.rgba[3]);

  if (geom.type == mjGEOM_PLANE && behind) {
    col[3] *= 0.3;
  }

  switch (geom.type) {
    case mjGEOM_PLANE:
    {
      const int NSX = 4, NSY = 4;
      const F XS = 0.25f, YS = 0.25f;
      auto restoreModel = dl.withModelUniforms(ModelUniforms(tpos), &litSolidShader);
      for (int xi = -NSX; xi < NSX; xi++) {
        for (int yi = -NSY; yi < NSY; yi++) {
          if ((xi + yi + 1024) % 2) continue;
          vec4 tilecol(geom.rgba[0], geom.rgba[1], geom.rgba[2], geom.rgba[3] * 0.3f);

          dl.addQuad(
            vec3(XS * (xi + 0), YS * (yi + 0), 0),
            vec3(0, 0, 1),
            tilecol,
            vec3(XS * (xi + 0), YS * (yi + 1), 0),
            vec3(0, 0, 1),
            tilecol,
            vec3(XS * (xi + 1), YS * (yi + 0), 0),
            vec3(0, 0, 1),
            tilecol,
            vec3(XS * (xi + 1), YS * (yi + 1), 0),
            vec3(0, 0, 1),
            tilecol,
            cmd.layer, &litSolidShader);
        }
      }
    }
    break;

    case mjGEOM_SPHERE:
    {
      auto restoreModel = dl.withModelUniforms(ModelUniforms(tpos), &litSolidShader);
      const int NTH = 16, NDM = 8;
      F r = geom.size[0];
      for (int thi = 0; thi < NTH; thi ++) {
        F th0 = (thi + 0) * (M_2PI/NTH);
        F th1 = (thi + 1) * (M_2PI/NTH);
        F sth0 = sin(th0), cth0 = cos(th0), sth1 = sin(th1), cth1 = cos(th1);
        for (int dmi = 0; dmi < NDM; dmi ++) {
          F dm0 = (dmi + 0) * (M_PI/NDM);
          F dm1 = (dmi + 1) * (M_PI/NDM);
          F sdm0 = sin(dm0), cdm0 = cos(dm0), sdm1 = sin(dm1), cdm1 = cos(dm1);

          dl.addQuad(
            vec3(sth0 * r * sdm0, cth0 * r * sdm0, r * cdm0),
            vec3(sth0 * r * sdm0, cth0 * r * sdm0, r * cdm0),
            col,
            vec3(sth1 * r * sdm0, cth1 * r * sdm0, r * cdm0),
            vec3(sth1 * r * sdm0, cth1 * r * sdm0, r * cdm0),
            col,
            vec3(sth0 * r * sdm1, cth0 * r * sdm1, r * cdm1),
            vec3(sth0 * r * sdm1, cth0 * r * sdm1, r * cdm1),
            col,
            vec3(sth1 * r * sdm1, cth1 * r * sdm1, r * cdm1),
            vec3(sth1 * r * sdm1, cth1 * r * sdm1, r * cdm1),
            col,
            cmd.layer, &litSolidShader);
        }
      }
    }
    break;

    case mjGEOM_CAPSULE:
    {
      dl.addCapsule(tpos, col, vec3(geom.size[0], geom.size[1], geom.size[2]),
        cmd.layer, &litSolidShader);
    }
    break;

    case mjGEOM_MESH:
    {
      int meshid = geom.dataid/2;
      addMjMesh(dl, m, tpos, meshid, geom.texid, cmd.layer);
    }
    break;

    case mjGEOM_BOX:
    {
      auto restoreModel = dl.withModelUniforms(ModelUniforms(tpos), &litSolidShader);
      dl.addBox(tpos, col, vec3(geom.size[0], geom.size[1], geom.size[2]),
        cmd.layer, &litSolidShader);
    }
    break;

    default:
      L() << "Unknown geom type " << geom.type;
    break;
  }
}

void DrawTraceAccum::drawOpFinal(Cmd &cmd, DrawOpMujocoScene *op)
{
  auto &scn = *op->scene;
  
  mjtNum hpos[3], hfwd[3];
  float headpos[3], forward[3];
  //float camProject[16], camView[16], lightProject[16], lightView[16];
  //double clipplane[4];

  mjv_cameraInModel(hpos, hfwd, NULL, &scn);
  mju_n2f(headpos, hpos, 3);
  mju_n2f(forward, hfwd, 3);

  // No: initLights(&scn, headpos, forward);

  int nt = 0;
  for (int i = 0; i < scn.ngeom; i++) {
    // get geom pointer
    auto &g = scn.geoms[i];

    if (g.rgba[3]<0.995f || (g.type==mjGEOM_PLANE &&
                                    isBehind(headpos, g.pos, g.mat))) {
      // include index in list
      scn.geomorder[nt++] = i;
      g.transparent = 1;

      // compute distance to camera
      g.camdist = sqrtf((g.pos[0]-headpos[0])*(g.pos[0]-headpos[0]) +
                        (g.pos[1]-headpos[1])*(g.pos[1]-headpos[1]) +
                        (g.pos[2]-headpos[2])*(g.pos[2]-headpos[2]));

      // correct for rbound
      g.camdist -= mjvRbound(g);

      // plane always far away
      if (g.type == mjGEOM_PLANE) {
        g.camdist = 1e+10f;
      }
    }
    else {
      g.transparent = 0;
    }
  }

  sort(&scn.geomorder[0], &scn.geomorder[nt],
    [&scn](int const &el1, int const &el2) {
      float d1 = scn.geoms[el1].camdist;
      float d2 = scn.geoms[el2].camdist;

      return d1 < d2;
    });

  // allow only one reflective geom
  int j = 0;
  for (int i = 0; i < scn.ngeom; i++) {
    if (j) {
      scn.geoms[i].reflectance = 0;
    }
    else if (isReflective(scn.geoms[i])) {
      j = 1;
    }
  }

  for (int i = 0; i < scn.ngeom; i++) {
    if (!scn.geoms[i].transparent && !isReflective(scn.geoms[i])) {
      renderGeom(dl, cmd, op->m, scn.geoms[i], headpos);
    }
  }

  for (int i = 0; i < scn.ngeom; i++) {
    if (isReflective(scn.geoms[i])) {
      renderGeom(dl, cmd, op->m, scn.geoms[i], headpos);
    }
  }  
}
