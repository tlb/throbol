#include "./emit.h"
#include "./emit_macros.h"

deftype(exp)
{
  auto at = eval(n->ch(0));
  if (at.ist<CF>()) return at;
  return typetag<F>();
}

defemit(exp)
{
  defop1g_simd(F, F, r = exp(a), ag += rg * r);
  defop1g_simd(CF, CF, r = exp(a), ag += rg * r);
  return setInvalidArgs();
}

defusage(exp, R"(
  exp(F))");


deftype(log)
{
  return typetag<F>();
}

defemit(log)
{
  defop1g_simd(F, F, r = log(a), ag += rg / a);
  return setInvalidArgs();
}

defusage(log, R"(
  log(F))");

