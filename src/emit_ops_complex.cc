#include "./emit.h"
#include "./emit_macros.h"

deftype(real)
{
  return typetag<F>();
}

defemit(real)
{
  defop1g(F, F, r = a, ag += rg);
  defop1g(CF, F, r = a.real(), ag += rg);
  return setInvalidArgs();
}

defusage(real, R"(
  real(F|CF))");


deftype(imag)
{
  return typetag<F>();
}

defemit(imag)
{
  defop1g(F, F, r = a*0.0f, ag += 0);
  defop1g(CF, F, r = a.imag(), ag += CF(0, rg));
  return setInvalidArgs();
}

defusage(imag, R"(
  imag(F|CF))");


deftype(arg)
{
  return typetag<F>();
}

defemit(arg)
{
  defop1_simd(F, F, r = a*0.0f);
  defop1_simd(CF, F, r = a.arg());
  return setInvalidArgs();
}

defusage(arg, R"(
  arg(F|CF))");


deftype(conj)
{
  return typetag<CF>();
}

defemit(conj)
{
  defop1(F, CF, r = CF(a, 0.0f));
  defop1(CF, CF, r = conj(a));
  return setInvalidArgs();
}

defusage(conj, R"(
  conj(F|CF))");


deftype(polar)
{
  return typetag<CF>();
}

defemit(polar)
{
  defop2(F, F, CF, r = std::polar(a, b));
  return setInvalidArgs();
}

defusage(polar, R"(
  polar(real ∊ F, imag ∊ F))");


deftype(im)
{
  return typetag<CF>();
}

defemit(im)
{
  defop0(CF, r = CF(0.0f, 1.0f));
  return setInvalidArgs();
}

defusage(im, R"(
  im -- the unit imaginary number)");

