#include "./emit.h"

static const int compileVerbose = 0;
static U64 epochCounter = 1000000;

SheetEmitContext::SheetEmitContext(CompiledSheet &_she, SheetEmitContext *_parent)
  : she(_she),
    parent(_parent),
    overlay(she.sh.nCells),
    progress(she.sh.nCells, CES_init)
{
  if (parent) {
    for (CellIndex cell = 0; cell < she.sh.nCells; cell++) {
      switch (parent->progress[cell]) {

        case CES_injected:
          refByCell[cell] = parent->refByCell[cell];
          progress[cell] = CES_injected;
          break;

        default:
          break;
      }
    }
  }
}

CompiledSheet::CompiledSheet(Sheet &_sh, EmitMode _emitMode)
  : sh(_sh),
    doDebug(sh.doDebug),
    emitMode(_emitMode),
    apiDefByCell(sh.nCells),
    apiDirectionByCell(sh.nCells, API_NONE),
    debugsByCell(sh.nCells),
    nameByCell(sh.nameByCell),
    emitErrorByCell(sh.nCells),
    emitErrorLocByCell(sh.nCells),
    typeByCell(sh.nCells, T_undef),
    hasParamByCell(sh.nCells),
    isPureParamByCell(sh.nCells),
    hasRefByCell(sh.nCells),
    hasPrevByCell(sh.nCells),
    hasDtByCell(sh.nCells),
    hasTimeByCell(sh.nCells),
    isTimeInvariantByCell(sh.nCells),
    isApiByCell(sh.nCells),
    epoch(epochCounter++)
{
  ok = true;
  doSheet();

  if (ok && doDebug) {
    for (auto &debugs : debugsByCell) {
      sort(debugs.begin(), debugs.end(), [](DebugTestPoint const &a, DebugTestPoint const &b) {
        if (a.loc.begin < b.loc.begin) return true;
        if (b.loc.begin < a.loc.begin) return false;
        if (b.loc.end < a.loc.end) return true;
        return false;
      });
    }
  }
}

CompiledSheet::~CompiledSheet()
{
}



ValueOffset CompiledSheet::allocValue(TypeTag t)
{
  return allocIncr(valueDataSize, t, 1);
}

BoxedRef CompiledSheet::mkValue(CellIndex cell, TypeTag t)
{
  auto vofs = allocValue(t);

  return BoxedRef(t, cell, vofs);
}

BoxedRef CompiledSheet::getValue(CellIndex cell)
{
  return getValue(cell, contexts.at(0).get());
}

BoxedRef CompiledSheet::getValue(CellIndex cell, SheetEmitContext *ctx)
{
  auto found = ctx->refByCell.find(cell);
  if (found != ctx->refByCell.end()) {
    return found->second;
  }
  return BoxedRef();
}


SheetEmitContext * CompiledSheet::mkContext(SheetEmitContext *parent)
{
  auto ctx = contexts.emplace_back(make_shared<SheetEmitContext>(*this, parent)).get();
  return ctx;
}

void CompiledSheet::addDeps(SheetEmitContext *ctx, CellIndex cell)
{
  if (!ctx->overlay[cell]) {
    ctx->overlay[cell] = true;
    for (auto dep : sh.fwdDepsByCell[cell]) {
      addDeps(ctx, dep);
    }
  }
}

void CompiledSheet::addDeps(SheetEmitContext *ctx)
{
  for (auto &[srcCell, ref] : ctx->refByCell) {
    if (ctx->progress[srcCell] == CES_injected) {
      addDeps(ctx, srcCell);
    }
  }
  if (0) {
    auto s = L();
    s << "Deps:";
    for (auto &[srcCell, ref] : ctx->refByCell) {
      if (ctx->progress[srcCell] == CES_injected) {
        s << " " << sh.nameByCell[srcCell];
      }
    }
    s << " =>";
    for (CellIndex cell = 0; cell < sh.nCells; cell++) {
      if (ctx->overlay[cell]) {
        s << " " << sh.nameByCell[cell];
      }
    }
  }
}

shared_ptr<CellVals> CompiledSheet::getT0Vals(Paramset *params)
{
  if (!params) params = &sh.params;
  // This should be cacheable based on params->hash();
  auto &slot = t0ValCacheByParamHash[params->hash()];
  if (!slot) {
    slot = CellVals::mk(this);
    CpuState cpu(*this, &buildlife, 1);
    cpu.curs[0] = slot.get();
    cpu.prevs[0] = zeroVals.get();
    cpu.paramss[0] = params;
    cpu.paramHashes[0] = params->hash();
    cpu.traces[0] = nullptr;
    cpu.execSheet();
  }
  return slot;
}

void CompiledSheet::doSheet()
{
  assert(contexts.empty());
  auto ctx = mkContext(nullptr);
  if (compileVerbose >= 1) L() << "Type pass:\n";
  for (auto cell : sh.liveCells) {
    doCell(cell, ctx, true);
  }
  if (compileVerbose >= 1) L() << "Emit pass:\n";
  for (auto cell : sh.liveCells) {
    doCell(cell, ctx, false);
  }

  auto baseCtx = contexts.at(0).get();
  zeroVals = CellVals::mk(this);
  auto t0Vals = getT0Vals(&sh.params);

  for (auto cell : sh.liveCells) {
    if (compileVerbose >= 2) L() << nameByCell[cell] << " type=" << baseCtx->refByCell[cell].t;
    if (baseCtx->refByCell[cell].t.ist<ApiHandle>()) {
      auto br = getValue(cell, baseCtx);
      if (auto ah = br.get<ApiHandle>(t0Vals.get())) {
        apiDefByCell[cell] = ah;
        ApiSpec spec;
        spec.apiType = ah.apiType;
        spec.apiInstance = sh.nameByCell[cell];
        spec.apiProps = toShared(ah.props);
        spec.cell = cell;

        // deep inside the api constructor it sets elements of this->apiDirectionByCell
        auto api = ApiRegister::mk(*this, spec);
        if (!api) {
          setError(cell, "No such API " + shellEscape(spec.apiType));
          continue;
        }
        if (!api->ok()) {
          setError(cell, api->errors[0]);
          continue;
        }
        api->bindApi();
        if (!api->ok()) {
          setError(cell, api->errors[0]);
          continue;
        }
        apis.push_back(api);
        isApiByCell[cell] = true;
      }
    }
  }
  if (compileVerbose >= 2) L() << "t0Vals=" << *t0Vals;

  auto dtCell = sh.getCell("dt");
  if (dtCell != nullCellIndex) {
    if (!isTimeInvariantByCell[dtCell]) {
      setError(dtCell, "dt must be constant");
    }
    auto br = getValue(dtCell, baseCtx);
    if (!br.ist<F>()) {
      setError(dtCell, "dt must be scalar");
    }
    else {
      dtRef = br;
    }
  }

  auto tickCountCell = sh.getCell("tick_count");
  if (tickCountCell != nullCellIndex) {
    if (0 && !isTimeInvariantByCell[tickCountCell]) {
      setError(tickCountCell, "tick_count must be constant");
    }
    auto br = getValue(tickCountCell, baseCtx);
    if (!br.ist<F>()) {
      setError(tickCountCell, "tick_count must be scalar");
    }
    else {
      tickCountRef = br;
    }
  }


  if (!orderProblems.empty()) ok = false;

}

ostream & operator << (ostream &s, CellEmitState const &a)
{
  switch (a) {
    case CES_init: return s << "init";
    case CES_injected: return s << "injected";
    case CES_working: return s << "working";
    case CES_typing: return s << "typing";
    case CES_placeholder: return s << "placeholder";
    case CES_done: return s << "done";
    case CES_err: return s << "err";
  }
  return s << "???";
}

void SheetEmitContext::setProgress(CellIndex cell, CellEmitState newState)
{
  if constexpr (compileVerbose >= 1) {
    auto s = L();
    s << "  " << she.sh.nameByCell[cell] << ": " << progress[cell] << " -> " << newState;
    if (refByCell[cell].valid()) {
      s << " " << refByCell[cell];
    }
  }
  progress[cell] = newState;
}

BoxedRef CompiledSheet::emitCell(CellIndex cell, SheetEmitContext *hypoCtx)
{
  CellEmitter ce(*this, cell, hypoCtx);
  if (!ce.ok) goto fail;

  if (hypoCtx->refByCell[cell].t.isError()) {
    L() << "Cell " << sh.nameByCell[cell] << " is " << hypoCtx->refByCell[cell] << " despite ce.ok";
    goto fail;
  }
  hasParamByCell[cell] = ce.hasParam;
  hasRefByCell[cell] = ce.hasRef;
  hasPrevByCell[cell] = ce.hasPrev;
  hasDtByCell[cell] = ce.hasDt;
  hasTimeByCell[cell] = ce.hasTime;
  isPureParamByCell[cell] = ce.isPureParam;
  // Could get overwritten later by an Api.
  isTimeInvariantByCell[cell] = (
    !ce.hasRef && 
    !ce.hasPrev &&
    !ce.hasDt &&
    !ce.hasTime &&
    !ce.hasRandom);
  return hypoCtx->refByCell[cell];

fail:
  ok = false;
  if (!ce.error.empty()) {
    emitErrorByCell[cell] = ce.error;
    emitErrorLocByCell[cell] = ce.errorLoc;
  }
  else if (!emitErrorByCell[cell].empty()) {
    // leave it
  }
  else {
    emitErrorByCell[cell] = "Compile error"s;
  }
  return BoxedRef();
}

BoxedRef CompiledSheet::typeCell(CellIndex cell, SheetEmitContext *hypoCtx)
{
  CellTypeAnalyzer cta(*this, cell, hypoCtx);
  if (cta.ok) {
    hypoCtx->refByCell[cell] = mkValue(cell, cta.type);
    assertlog(hypoCtx->refByCell[cell].valid(), "cell=" << sh.nameByCell[cell] << LOGV(hypoCtx->refByCell[cell]));
    return hypoCtx->refByCell[cell];
  }
  else {
    ok = false;
    hypoCtx->setProgress(cell, CES_err);

    if (!cta.error.empty()) {
      emitErrorByCell[cell] = cta.error;
      emitErrorLocByCell[cell] = cta.errorLoc;
    }
    else if (!emitErrorByCell[cell].empty()) {
      // leave it
    }
    else {
      emitErrorByCell[cell] = "Type inference error"s;
    }
    return BoxedRef();
  }
}

template<typename T>
struct OnStack {

  vector<T> &stack;
  T item;

  OnStack(vector<T> &_stack, T _item)
    :stack(_stack), item(_item)
  {
    stack.push_back(item);
  }
  ~OnStack()
  {
    assert(!stack.empty());
    assert(stack.back() == item);
    stack.pop_back();
  }

};

void CompiledSheet::reportCycle(string const &err)
{
  orderProblems.emplace_back(err, curCellStack);
  for (auto &it : curCellStack) {
    if (emitErrorByCell[it].empty()) {
      emitErrorByCell[it] = err;
    }
  }
}

BoxedRef CompiledSheet::doCell(CellIndex cell, SheetEmitContext *hypoCtx, bool deferrable)
{
  if (curCellStack.size() > 500) {
    reportCycle("Recursion limit exceeded");
    return BoxedRef();
  }
  OnStack onStack(curCellStack, cell);
  while (hypoCtx->parent && !hypoCtx->overlay[cell]) {
    hypoCtx = hypoCtx->parent;
  }

  switch (hypoCtx->progress[cell]) {

    case CES_init:
      if (deferrable) {
        hypoCtx->setProgress(cell, CES_typing);
        auto ret = typeCell(cell, hypoCtx);
        if (ret.valid()) {
          hypoCtx->setProgress(cell, CES_placeholder);
          return ret;
        }
        else {
          if (sh.verbose >= 1) L() << sh.nameByCell[cell] << ": typing failed at " << curCellStack << "\n";
          ok = false;
          hypoCtx->setProgress(cell, CES_err);
          // get better error message
          emitCell(cell, hypoCtx);
          return ret;
        }
      }
      else {
        hypoCtx->setProgress(cell, CES_working);

        BoxedRef ret;
        ret = emitCell(cell, hypoCtx);
        if (ret.valid()) {
          hypoCtx->setProgress(cell, CES_done);
          return ret;
        }
        else {
          if (sh.verbose >= 1) L() << sh.nameByCell[cell] << ": emit failed\n";
          ok = false;
          hypoCtx->setProgress(cell, CES_err);
          return ret;
        }
      }

    case CES_injected:
      if constexpr (compileVerbose >= 1) L() << "  " << sh.nameByCell[cell] << ": injected " << hypoCtx->refByCell[cell];
      assertlog(hypoCtx->refByCell[cell].valid(), LOGV(sh.nameByCell[cell]) << LOGV(hypoCtx->refByCell[cell]));
      return hypoCtx->refByCell[cell];

    case CES_working:
      if (deferrable) {
        if (hypoCtx->refByCell[cell].valid()) {
          return hypoCtx->refByCell[cell];
        }
        else {
          hypoCtx->setProgress(cell, CES_typing);
          auto ret = typeCell(cell, hypoCtx);
          if (ret.valid()) {
            hypoCtx->setProgress(cell, CES_placeholder);
            return ret;
          }
          else {
            if (sh.verbose >= 1) L() << sh.nameByCell[cell] << ": cycle detected (3) at " << curCellStack << "\n";
            ok = false;
            hypoCtx->setProgress(cell, CES_err);
            return ret;
          }
        }
      }
      else {
        if (sh.verbose >= 1) L() << sh.nameByCell[cell] << ": cycle detected (4) at " << curCellStack << "\n";
        reportCycle("Circular value dependency");
        return BoxedRef();
      }

    case CES_typing:
    {
      if (emitErrorByCell[cell].empty()) {
        emitErrorByCell[cell] = "Circular type dependency";
      }        
      ok = false;
      if (sh.verbose >= 1) L() << sh.nameByCell[cell] << ": cycle detected (7) at " << curCellStack << "\n";
      reportCycle("Circular type dependency");
      hypoCtx->setProgress(cell, CES_err);
      return BoxedRef();
    }

    case CES_placeholder:
      if (deferrable) {
        if constexpr (compileVerbose >= 1) L() << "  " << sh.nameByCell[cell] << ": placeholder " << hypoCtx->refByCell[cell];
        assertlog(hypoCtx->refByCell[cell].valid(), LOGV(sh.nameByCell[cell]) << LOGV(hypoCtx->refByCell[cell]));
        return hypoCtx->refByCell[cell];
      }
      else {
        hypoCtx->setProgress(cell, CES_working);

        BoxedRef ret;
        ret = emitCell(cell, hypoCtx);
        if (ret.valid()) {
          hypoCtx->setProgress(cell, CES_done);
          return ret;
        }
        else {
          if (sh.verbose >= 1) L() << sh.nameByCell[cell] << ": error (5) at " << curCellStack << "\n";
          ok = false;
          hypoCtx->setProgress(cell, CES_err);
          return ret;
        }
      }
      return BoxedRef();

    case CES_done:
      if constexpr (compileVerbose >= 1) L() << "  " << sh.nameByCell[cell] << ": done\n";
      return hypoCtx->refByCell[cell];

    case CES_err:
      if constexpr (compileVerbose >= 1) L() << "  " << sh.nameByCell[cell] << ": err\n";
      return BoxedRef();
  }
}

void Sheet::emitAll(EmitMode emitMode)
{
  if (needsEmit || !compiled) {
    needsEmit = false;
    auto t0 = clock();

    // FIXME: multiple batch sizes
    auto emit = make_shared<CompiledSheet>(*this, emitMode);

    emit->compileTime = clock() - t0;
    compiled = emit;
  }
}

#if 0
void debugCellInfo(ostream &s, CellIndex cell, Pad &pad, shared_ptr<CompiledSheet> &compiled)
{
  auto &sh = compiled->sh;
  auto &debugs = compiled->debugsByCell[cell];
  s << sh.nameByCell[cell] << " = " << sh.formulaByCell[cell] << "\n";
  auto indent = sh.nameByCell[cell].size() + 3;
  for (auto &it : debugs) {
    auto bp = WithSheet(compiled->sh, BoxedPtr(it.t, &pad.buf[it.ofs]));
    s << string(indent + it.loc.begin, ' ') << string(it.loc.end - it.loc.begin, '^') << " ";
    s << it.t << " " << bp << "\n";
  }
}
#endif

void CompiledSheet::setError(CellIndex cell, string const &e, AstNode *n)
{
  if (emitErrorByCell[cell].empty()) {
    emitErrorByCell[cell] = e;
    if (n) emitErrorLocByCell[cell] = n->loc;
  }
}

