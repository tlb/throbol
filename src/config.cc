#include "./config.h"

#if defined(__APPLE__)
#include <mach-o/dyld.h>
#include <libgen.h>
#elif defined(__linux__)
#include <libgen.h>
#endif


string installDir = ".";

void setupPaths(char const *argv0)
{

#if defined(__APPLE__)
  char pathbuf[PATH_MAX+1];
  uint32_t pathsize = sizeof(pathbuf);
  if (_NSGetExecutablePath(pathbuf, &pathsize) == 0) {
    auto tmp = realpath((string(dirname(pathbuf)) + "/..").c_str(), nullptr);
    installDir = tmp;
    free(tmp);
  }
#elif defined(__linux__)

  char pathbuf[PATH_MAX+1];
  size_t pathsize = PATH_MAX;
  ssize_t nr = readlink("/proc/self/exe", pathbuf, pathsize);
  if (nr >= 0) {
    pathbuf[nr] = 0;
    if (0) L() << "readlink " << nr << " = " << pathbuf;
    char const *rp = realpath((string(dirname(pathbuf)) + "/..").c_str(), nullptr);
    if (!rp) {
      throw runtime_error("realpath failed");
    }
    installDir = rp;
  }

#endif

  if (0) L() << "installDir=" << installDir;
}