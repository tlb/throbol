#pragma once
#include "./defs.h"
#include <type_traits>
#include <mutex>

enum CpuStateError : U32 {
  ERR_none,
  ERR_ref,
  ERR_dim
};

struct DebugTestPoint : ValSuite {

  AstType astt;
  FormulaSubstr loc;

  DebugTestPoint(ValSuite _vals, AstType _astt, FormulaSubstr _loc)
    : ValSuite(_vals),
      astt(_astt),
      loc(_loc)
  {    
  }

};

/*
  A Pad holds the working memory when executing a compiled sheet. During compilation, every value
  is assigned an offset and the code read and writes the values with calls like `pad.rd_vec2(offset)`.
  Some types (strings and dynamically sized matrices) also require external allocation from an `Arena`
  pointed to here by `.pool`.
*/
struct Pad {

  Arena *pool;
  U32 padSize;
  alignas(MAX_ALIGN) U8 buf[0]; // extensible

  Pad(Arena *_pool, U32 _padSize)
    : pool(_pool),
      padSize(_padSize)
  {
  }

  ~Pad() noexcept
  {
  }

  Pad(const Pad &other) = delete;
  Pad(Pad &&other) = delete;

  static Pad *mk(Arena *_pool, U32 _padSize);

  /*
    In defense of this preprocessor hack, I'll point out that it saves a boatload of compile time over using templates.
    Also, I couldn't figure out a good way to handle the dynamic matrix types, for which the access functions
    need to return a Map<Matrix<...>>  by value, though everything else is by reference.

    Anyway, there's no case where we want to do argument-dependent lookup here. We always know the type of
    what we're accessing, so there's no problem including it in the function name rather than as a template arg.
  */
#define padaccessor(T) \
  inline T const &rd_##T(Val v, int batchi) const { return (*(T *)(&buf[v.ofs + batchi * sizeof(T)])); } \
  inline T &wr_##T(Val v, int batchi) const { return (*(T *)(&buf[v.ofs + batchi * sizeof(T)])); } \
  inline T &up_##T(Val v, int batchi) const { return (*(T *)(&buf[v.ofs + batchi * sizeof(T)])); }

  padaccessor(F)
  padaccessor(CF)
  padaccessor(bool)
  padaccessor(vec2)
  padaccessor(vec3)
  padaccessor(vec4)
  padaccessor(mat2)
  padaccessor(mat3)
  padaccessor(mat4)
  padaccessor(string_view)
  padaccessor(DrawOpHandle)
  padaccessor(Checkp)
  padaccessor(SymbolSet)
  padaccessor(Beh)
  padaccessor(VideoFrame);
  padaccessor(AudioFrame);
  padaccessor(Assocp);
  padaccessor(ApiHandle);
  // ADD type
#undef padaccessor

  inline mat const rd_mat(Val v, int batchi) const { return mat(((F *)(buf + v.ofs + batchi * v.t.nr * v.t.nc * sizeof(F))), v.t.nr, v.t.nc); }
  inline mat wr_mat(Val v, int batchi) const { return mat(((F *)(buf + v.ofs + batchi * v.t.nr * v.t.nc * sizeof(F))), v.t.nr, v.t.nc); }
  inline mat up_mat(Val v, int batchi) const { return mat(((F *)(buf + v.ofs + batchi * v.t.nr * v.t.nc * sizeof(F))), v.t.nr, v.t.nc); }

  inline vec const rd_vec(Val v, int batchi) const { return vec(((F *)(buf + v.ofs + batchi * v.t.nr * sizeof(F))), v.t.nr); }
  inline vec wr_vec(Val v, int batchi) const { return vec(((F *)(buf + v.ofs + batchi * v.t.nr * sizeof(F))), v.t.nr); }
  inline vec up_vec(Val v, int batchi) const { return vec(((F *)(buf + v.ofs + batchi * v.t.nr * sizeof(F))), v.t.nr); }

  inline cmat const rd_cmat(Val v, int batchi) const { return cmat(((CF *)(buf + v.ofs + batchi * v.t.nr * v.t.nc * sizeof(CF))), v.t.nr, v.t.nc); }
  inline cmat wr_cmat(Val v, int batchi) const { return cmat(((CF *)(buf + v.ofs + batchi * v.t.nr * v.t.nc * sizeof(CF))), v.t.nr, v.t.nc); }
  inline cmat up_cmat(Val v, int batchi) const { return cmat(((CF *)(buf + v.ofs + batchi * v.t.nr * v.t.nc * sizeof(CF))), v.t.nr, v.t.nc); }

  inline cvec const rd_cvec(Val v, int batchi) const { return cvec(((CF *)(buf + v.ofs + batchi * v.t.nr * sizeof(CF))), v.t.nr); }
  inline cvec wr_cvec(Val v, int batchi) const { return cvec(((CF *)(buf + v.ofs + batchi * v.t.nr * sizeof(CF))), v.t.nr); }
  inline cvec up_cvec(Val v, int batchi) const { return cvec(((CF *)(buf + v.ofs + batchi * v.t.nr * sizeof(CF))), v.t.nr); }


  /*
    The return value of these is some unholy Eigen template, so be sure to use decltype(auto) to bind a name to it
  */
#define padaccessor_simd(T) \
  inline auto rd_##T##_simd(Val v, auto batchdef) const { \
    return Map<Eigen::Array<T, batchdef.compiledBatchSize(), 1>>((T *)(buf + v.ofs)); \
  } \
  inline auto wr_##T##_simd(Val v, auto batchdef) const { \
    return Map<Eigen::Array<T, batchdef.compiledBatchSize(), 1>>((T *)(buf + v.ofs)); \
  } \
  inline auto up_##T##_simd(Val v, auto batchdef) const { \
    return Map<Eigen::Array<T, batchdef.compiledBatchSize(), 1>>((T *)(buf + v.ofs)); \
  }

  padaccessor_simd(F)
  padaccessor_simd(CF)
  padaccessor_simd(bool)

#undef padaccessor_simd

};


struct CpuState {

  Sheet &sh;
  CompiledSheet &compiled;
  Arena *pool = nullptr;

  int timeIndex = 0;
  int realBatchSize = 0;
  int compiledBatchSize = 0;
  CpuStateError err = ERR_none;
  bool squashSensorWrites = false;

  alignas(MAX_ALIGN) F dts[MAX_BATCH_SIZE];
  alignas(MAX_ALIGN) F times[MAX_BATCH_SIZE];
  CellVals *curs[MAX_BATCH_SIZE];
  CellVals *prevs[MAX_BATCH_SIZE];
  CellVals *grads[MAX_BATCH_SIZE];
  vector<CellIndex> checkFailures[MAX_BATCH_SIZE];
  Paramset *paramss[MAX_BATCH_SIZE];
  U64 paramHashes[MAX_BATCH_SIZE];
  Trace *traces[MAX_BATCH_SIZE];

  static constexpr int execVerbose = 0; // 1 to enable logging
  static shared_ptr<ofstream> logFile;
  static std::mutex logFileMutex;

  CpuState(CompiledSheet &_compiled, Arena *_pool, int _realBatchSize);
  CpuState(CompiledSheet &_compiled, Arena *_pool, CellVals *ref, CellVals *prev);
  ~CpuState();

  Arena * operator ()() const { return pool; }

  CellVals *valueAtRelTime(F delta, int batchi, CellIndex cell);

  void setLane(int batchi, Trace *tr, Paramset *params);

  void execSheet();
  void execSheet(Pad &pad);
  bool isInitial() const { return timeIndex == 0; }

  Pad *mkPad();

  void zero();

  bool checktrue(F a, int batchi, CellIndex cell) {
    if (a < 0.5f) {
      checkFailures[batchi].push_back(cell);
      return false;
    }
    return true;
  }

  bool checktrue(bool a, int batchi, CellIndex cell) {
    if (!a) {
      checkFailures[batchi].push_back(cell);
      return false;
    }
    return true;
  }

#define defmem_simd(T) \
  inline auto rdcur_##T##_simd(BoxedRef ref, auto batchdef) const { \
    Eigen::Array<T, batchdef.compiledBatchSize(), 1> ret; \
    for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
      ret[batchi] = batchi < realBatchSize ? curs[batchi]->rd_##T##_unchecked(ref) : T(0); \
    } \
    return ret; \
  } \
  inline auto rdgrad_##T##_simd(BoxedRef ref, auto batchdef) const { \
    Eigen::Array<T, batchdef.compiledBatchSize(), 1> ret; \
    for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
      ret[batchi] = (batchi < realBatchSize && grads[batchi]) ? grads[batchi]->rd_##T##_unchecked(ref) : T(0); \
    } \
    return ret; \
  } \
  inline void incrgrad_##T##_simd(BoxedRef ref, auto const &v, auto batchdef) const { \
    for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
      if (batchi < realBatchSize && grads[batchi]) { \
        grads[batchi]->up_##T##_unchecked(ref) += v[batchi]; \
      } \
    } \
  } \
  inline auto rdprev_##T##_simd(BoxedRef ref, auto batchdef) const { \
    Eigen::Array<T, batchdef.compiledBatchSize(), 1> ret; \
    for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
      ret[batchi] = (batchi < realBatchSize) ? prevs[batchi]->rd_##T##_unchecked(ref) : T(0); \
    } \
    return ret; \
  } \
  inline void wrcur_##T##_simd(BoxedRef ref, auto const &v, auto batchdef) const { \
    for (int batchi = 0; batchi < batchdef.compiledBatchSize(); batchi++) { \
      if (batchi < realBatchSize) { \
        curs[batchi]->wr_##T##_unchecked(ref) = v[batchi]; \
      } \
    } \
  }

  defmem_simd(F)
  defmem_simd(CF)

  template<int BATCH_SIZE>
  inline auto rdparam_simd(ParamIndex param) const {
    Eigen::Array<F, BATCH_SIZE, 1> ret;
    foreach_batchi {
      ret[batchi] = paramss[batchi] ? paramss[batchi]->valueByParam(param) : 0.0f;
    }
    return ret;
  }
};

bool op_has(CpuState &cpu, SymbolSet a, Symbol b);
bool op_hasany(CpuState &cpu, SymbolSet a, SymbolSet b);

vector<Symbol> setToVec(SymbolSet a);
SymbolSet vecToSet(Arena *pool, vector<Symbol> const &vec);

SymbolSet op_union(CpuState &cpu, SymbolSet a, SymbolSet b);
SymbolSet op_union(CpuState &cpu, Symbol a, SymbolSet b);
SymbolSet op_union(CpuState &cpu, SymbolSet a, Symbol b);
SymbolSet op_union(CpuState &cpu, Symbol a, Symbol b);

SymbolSet op_intersection(CpuState &cpu, SymbolSet a, SymbolSet b);
SymbolSet op_intersection(CpuState &cpu, Symbol a, SymbolSet b);
SymbolSet op_intersection(CpuState &cpu, SymbolSet a, Symbol b);
SymbolSet op_intersection(CpuState &cpu, Symbol a, Symbol b);

bool op_eq(CpuState &cpu, SymbolSet a, SymbolSet b);
bool op_eq(CpuState &cpu, Symbol a, SymbolSet b);
bool op_eq(CpuState &cpu, SymbolSet a, Symbol b);
bool op_eq(CpuState &cpu, Symbol a, Symbol b);

DrawOpHandle op_union(CpuState &cpu, DrawOpHandle a, DrawOpHandle b);

Beh op_fallback(CpuState &cpu, Beh const &a, Beh const &b);

Beh op_seq(CpuState &cpu, Beh const &a, Beh const &b);

Beh op_cond(CpuState &cpu, F const &a);

Checkp op_and(CpuState &cpu, Checkp a, Checkp b);

Checkp op_or(CpuState &cpu, Checkp a, Checkp b);

bool op_at(CpuState &cpu, F time, int batchi);

Checkp op_checkat(CpuState &cpu, F time, Checkp arg, int batchi, CellIndex cell);
Checkp op_checkbetween(CpuState &cpu, F t0, F t1, Checkp arg, int batchi, CellIndex cell);
Checkp op_checkalways(CpuState &cpu, Checkp arg, int batchi, CellIndex cell);
Checkp op_check_bool(CpuState &cpu, F condition);
Checkp op_not(CpuState &cpu, Checkp a);


static inline F op_bool(CpuState &cpu, bool a)
{
  return a ? 1.0f : 0.0f;
}

static inline F op_not(CpuState &cpu, F const &a)
{
  return clamp01(1.0f - a);
}

static inline F op_nearzero(CpuState &cpu, F const &a)
{
  return (abs(a) < 0.001f) ? 1.0f : 0.0f;
}

static inline F op_and(CpuState &cpu, F const &a, F const &b)
{
  return clamp01(a) * clamp01(b);
}

static inline F op_or(CpuState &cpu, F const &a, F const &b)
{
  return 1.0f - clamp01(1.0f - a) * clamp01(1.0f - b);
}

static inline F op_roundhyst(CpuState &cpu, F const &a, F const &b, F const &prev)
{
  if (a > prev + 0.5f + b) return prev + 1.0f;
  if (a < prev - 0.5f - b) return prev - 1.0f;
  return prev;
}
