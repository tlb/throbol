#include "./scope_math.h"


// Quantizes to the next smaller interval
double quantizeIntervalDn(double v)
{
  double scale = 1.0;
  if (isnan(v) || v <= 0.0) return 1.0;

  while (v < scale) {
    scale /= 10.0;
  }
  while (v >= 10 * scale) {
    scale *= 10.0;
  }
  if (v >= 5.0 * scale) return 5.0 * scale;
  if (v >= 2.0 * scale) return 2.0 * scale;
  return scale;
}


double quantizeIntervalUp(double v)
{
  double scale = 1.0;
  if (isnan(v) || v <= 0.0) return 1.0;

  while (scale > v) {
    scale /= 10.0;
  }
  while (scale * 10 < v) {
    scale *= 10.0;
  }
  while (1) {
    if (v <= 2.0 * scale) return 2.0 * scale;
    if (v <= 5.0 * scale) return 5.0 * scale;
    if (v <= 10.0 * scale) return 10.0 * scale;
    scale *= 10.0;
  }
}



double quantizeSubsample(double v)
{
  if (isnan(v) || v <= 1.0) return 1.0;

  double scale = 1.0;
  while (1) {
    if (v <= 2.0 * scale) return 2.0 * scale;
    if (v <= 5.0 * scale) return 5.0 * scale;
    if (v <= 10.0 * scale) return 10.0 * scale;
    scale *= 10.0;
  }
}


/*
  Return largest nice number >= v.
  Nice means [1, 1.5, 2, 3, 5, 7] * 10^n. 6 steps per decade
*/

double tidyScale(double v)
{
  auto vl = log10(v);
  auto vlp = pow(10, floor(vl));
  auto r = v / vlp;
  if (r <= 1.0) return 1.0 * vlp;
  if (r <= 1.5) return 1.5 * vlp;
  if (r <= 2.0) return 2.0 * vlp;
  if (r <= 3.0) return 3.0 * vlp;
  if (r <= 5.0) return 5.0 * vlp;
  if (r <= 7.0) return 7.0 * vlp;
  return 10.0 * vlp;
}

double tidyScaleUp(double v)
{
  return tidyScale(v * 1.24);
}

double tidyScaleDn(double v)
{
  return tidyScale(v * 0.60);
}


double compressGrad(double g)
{
  return clamp(0.4 * copysign(pow(abs(g)+0.001, 1.0/3.0), g), -1.0, 1.0);
}

double expandGrad(double g)
{
  return copysign(pow(abs(g * 2.5), 3.0), g);
}

double compressPower(double v, double power)
{
  return copysign(pow(abs(v), 1.0/power), v);
}

double expandPower(double v, double power)
{
  return copysign(pow(abs(v), power), v);
}

string fmteng(double v, int prec)
{
  if (v == 0.0) return "0";
  bool neg = v < 0.0;
  double vabs = abs(v);
  double mag = log10(vabs);
  double kilos = floor((mag - 1.0) / 3.0);
  if (kilos < 0.0) kilos += 1.0;
  size_t digits = (size_t)max(0, (int)floor(prec - (mag - kilos*3.0)));

  double vmant = vabs;
  if (kilos > 0.0) {
    for (double i = 0.0; i < kilos; i += 1.0) {
      vmant *= 0.001;
    }
  }
  if (kilos < 0.0) {
    for (double i = 0.0; i > kilos; i -= 1.0) {
      vmant *= 1000.0;
    }
  }

#if 0
  char vmantstr_buf[32];
  auto vmantstr_r = to_chars(vmantstr_buf, vmantstr_buf+sizeof(vmantstr_buf), vmant, chars_format::fixed, int(digits));
  string vmantstr(vmantstr_buf, vmantstr_r.ptr)
#else
  string vmantstr = to_string(vmant);

  auto fracpos = vmantstr.find('.');
  if (fracpos == string::npos) return "<err>";
  fracpos += 1;

  if (0) {
    while (vmantstr.size() < fracpos + digits) {
      vmantstr += '0';
    }
  }
  if (vmantstr.size() > fracpos + digits) {
    vmantstr = vmantstr.substr(0, fracpos + digits);
  }
  if (vmantstr.back() == '.') vmantstr.pop_back();
#endif

  return (
    (neg ? "-"s : "+"s) + 
    vmantstr +
    (kilos == 0.0 ? "" : "e" + to_string(int(kilos*3.0))));
}

string fmta2(R v)
{
  if (v == 0.0) return "0";

  bool neg = v < 0.0;
  double vabs = abs(v);
  double mag = log10(vabs);
  size_t digits = (size_t)max(0, (int)floor(2.0 - mag));

  string vabsstr = to_string(vabs);
  auto fracpos = vabsstr.find('.');
  if (fracpos == string::npos) return "<err>";
  fracpos += 1;
  if (vabsstr.size() > fracpos + digits) {
    vabsstr = vabsstr.substr(0, fracpos + digits);
  }
  if (vabsstr.back() == '.') vabsstr.pop_back();

  return (neg ? "-"s : "") + vabsstr;
}


string fmta3(R v)
{
  if (v == 0.0) return "0";

  bool neg = v < 0.0;
  double vabs = abs(v);
  double mag = log10(vabs);
  size_t digits = (size_t)max(0, (int)floor(3.0 - mag));

  string vabsstr = to_string(vabs);
  auto fracpos = vabsstr.find('.');
  if (fracpos == string::npos) return "<err>";
  fracpos += 1;
  if (vabsstr.size() > fracpos + digits) {
    vabsstr = vabsstr.substr(0, fracpos + digits);
  }
  if (vabsstr.back() == '.') vabsstr.pop_back();

  return (neg ? "-"s : "") + vabsstr;
}




R softclamp(R x)
{
  auto xa = abs(x);
  if (xa <= 1.0) return x;
  return copysign(1.0 + log(xa), x);
}

R softunclamp(R x)
{
  auto xa = abs(x);
  if (xa <= 1.0) return x;
  return copysign(exp(xa - 1.0), x);
}



F softclamp(F x)
{
  auto xa = abs(x);
  if (xa <= 1.0f) return x;
  return copysign(1.0f + log(xa), x);
}

F softunclamp(F x)
{
  auto xa = abs(x);
  if (xa <= 1.0f) return x;
  return copysign(exp(xa - 1.0f), x);
}
