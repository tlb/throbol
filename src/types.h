#pragma once
#include "./defs.h"
#include <type_traits>

  /*
    High level types. Some correspond to a concrete type, like `TK_F` -> `float`. The matrix
    types require a size also. See TypeTag below for how to completely specify a type.
    If it exceeds 4 bits, change the sizes of bit fields in TypeTag. There are a lot of TypeTags,
    so try to keep TypeTag down to 4 bytes.
  */
enum TypeKind : unsigned int {
  TK_undef,
  TK_error,
  TK_F, // float
  TK_CF, // complex
  TK_bool,
  TK_mat,
  TK_cmat,
  TK_str,
  TK_DrawOp,
  TK_Check,
  TK_SymbolSet,
  TK_Beh,
  TK_VideoFrame,
  TK_Assoc,
  TK_Api,
  // ADD type kind (search for this and add everywhere)
  TK_last = TK_Api
};

template<typename T> constexpr TypeTag typetag();

/*
  Cram an entire type into 32 bits. Includes matrix size if it's a matrix.
*/
struct TypeTag {

  /*
   If you add more types and this starts failing, increase KIND_WIDTH
   A large `nr` is more useful than a large `nc` for vectors.
  */
  static_assert(TK_last < 16);
  static constexpr size_t TOT_WIDTH = 32;
  static constexpr size_t KIND_WIDTH = 4;
  static constexpr size_t NR_WIDTH = (TOT_WIDTH - KIND_WIDTH + 1) / 2;
  static constexpr size_t NC_WIDTH = TOT_WIDTH - KIND_WIDTH - NR_WIDTH;

  TypeKind kind:KIND_WIDTH;
  unsigned int nr:NR_WIDTH;
  unsigned int nc:NC_WIDTH;

  constexpr TypeTag(TypeKind _kind, int _nr, int _nc)
    :kind(_kind), nr(_nr), nc(_nc)
  {
    assert(_nr >= 0 && _nr < (1 << NR_WIDTH) && int(nr) == _nr);
    assert(_nc >= 0 && _nc < (1 << NC_WIDTH) && int(nc) == _nc);
  }
  constexpr TypeTag(TypeKind _kind)
    :kind(_kind), nr(0), nc(0)
  {
  }
  TypeTag()
    :kind(TK_undef), nr(0), nc(0)
  {
  }

  bool isError() const {
    return kind == TK_error;
  }
  bool isUndef() const {
    return kind == TK_undef;
  }
  bool isMat() const {
    return kind == TK_mat;
  }
  bool isSquareMat() const {
    return kind == TK_mat && nr == nc;
  }
  bool isCmat() const {
    return kind == TK_cmat;
  }
  bool isSquareCmat() const {
    return kind == TK_cmat && nr == nc;
  }
  bool isVec() const {
    return kind == TK_mat && nc == 1;
  }
  bool isCvec() const {
    return kind == TK_cmat && nc == 1;
  }
  bool isF() const {
    return kind == TK_F;
  }
  bool isCF() const {
    return kind == TK_CF;
  }

  /*
    Does this type match a template m? Yes if the kind matches and either
    the size matches or the template size is 0, 0.
  */
  bool match(TypeTag m) const
  {
    if (kind == m.kind) {
      if ((nr == m.nr || m.nr == 0) && (nc == m.nc || m.nc == 0)) return true;
    }
    return false;
  }

  bool convmatch(TypeTag m) const
  {
    // 0 matches any array dimension
    if (kind == m.kind && 
      (nr == m.nr || m.nr == 0) && 
      (nc == m.nc || m.nc == 0)) return true;

    // convertible to bool
    if (m.kind == TK_bool && (
      kind == TK_F ||
      kind == TK_Check ||
      kind == TK_SymbolSet)) return true;

    // convert bool->F
    if (m.kind == TK_F && kind == TK_bool) return true;
    // convert F->CF
    if (m.kind == TK_CF && kind == TK_F) return true;

    return false;
  }

  template<typename T> bool ist() const {
    return match(typetag<T>());
  }
  template<typename T> bool convist() const {
    return convmatch(typetag<T>());
  }

};

inline bool operator == (TypeTag a, TypeTag b)
{
  return a.kind == b.kind && a.nr == b.nr && a.nc == b.nc;
}
inline bool operator != (TypeTag a, TypeTag b)
{
  return !(a == b);
}

bool operator < (TypeTag a, TypeTag b);

TypeTag constexpr T_undef(TK_undef);
TypeTag constexpr T_error(TK_error);

ostream & operator<< (ostream &s, TypeTag a);


/*
  Template monstrosity to get the TypeTag suitable for type `Foo` with `typetag<Foo>()`.
*/

template<typename T> struct typetag_helper {
};

// basic unparameterized types
template<> struct typetag_helper<F> {
  static constexpr TypeTag tag() { return TypeTag(TK_F); }
  static constexpr bool isLinear() { return true; }
};
template<> struct typetag_helper<CF> {
  static constexpr TypeTag tag() { return TypeTag(TK_CF); }
  static constexpr bool isLinear() { return true; }
};
template<> struct typetag_helper<bool> {
  static constexpr TypeTag tag() { return TypeTag(TK_bool); }
  static constexpr bool isLinear() { return false; }
};
template<> struct typetag_helper<string_view> {
  static constexpr TypeTag tag() { return TypeTag(TK_str); }
  static constexpr bool isLinear() { return false; }
};
template<> struct typetag_helper<DrawOp> {
  static constexpr TypeTag tag() { return TypeTag(TK_DrawOp); }
  static constexpr bool isLinear() { return false; }
};
template<> struct typetag_helper<DrawOpHandle> {
  static constexpr TypeTag tag() { return TypeTag(TK_DrawOp); }
  static constexpr bool isLinear() { return false; }
};
template<> struct typetag_helper<Check> {
  static constexpr TypeTag tag() { return TypeTag(TK_Check); }
  static constexpr bool isLinear() { return false; }
};
template<> struct typetag_helper<SymbolSet> {
  static constexpr TypeTag tag() { return TypeTag(TK_SymbolSet); }
  static constexpr bool isLinear() { return false; }
};
template<> struct typetag_helper<Beh> {
  static constexpr TypeTag tag() { return TypeTag(TK_Beh); }
  static constexpr bool isLinear() { return false; }
};
template<> struct typetag_helper<VideoFrame> {
  static constexpr TypeTag tag() { return TypeTag(TK_VideoFrame); }
  static constexpr bool isLinear() { return false; }
};
template<> struct typetag_helper<Assoc> {
  static constexpr TypeTag tag() { return TypeTag(TK_Assoc); }
  static constexpr bool isLinear() { return false; }
};
template<> struct typetag_helper<ApiHandle> {
  static constexpr TypeTag tag() { return TypeTag(TK_Api); }
  static constexpr bool isLinear() { return false; }
};


template<int _Rows> struct typetag_helper<Eigen::Matrix<F, _Rows, 1> > {
  static constexpr TypeTag tag() { return TypeTag(TK_mat, _Rows < 0 ? 0 : _Rows, 1); }
  static constexpr bool isLinear() { return true; }
};
template<int _Rows> struct typetag_helper<Eigen::Matrix<CF, _Rows, 1> > {
  static constexpr TypeTag tag() { return TypeTag(TK_cmat, _Rows < 0 ? 0 : _Rows, 1); }
  static constexpr bool isLinear() { return true; }
};


// Matrices of any fixed-at-compile-time size get mapped to a `TypeTag` with defined size
template<int _Rows, int _Cols> struct typetag_helper<Eigen::Matrix<F, _Rows, _Cols> > {
  static constexpr TypeTag tag() { return TypeTag(TK_mat, _Rows < 0 ? 0 : _Rows, _Cols < 0 ? 0 : _Cols); }
  static constexpr bool isLinear() { return true; }
};
template<int _Rows, int _Cols> struct typetag_helper<Eigen::Matrix<CF, _Rows, _Cols> > {
  static constexpr TypeTag tag() { return TypeTag(TK_cmat, _Rows < 0 ? 0 : _Rows, _Cols < 0 ? 0 : _Cols); }
  static constexpr bool isLinear() { return true; }
};

// Dynamic matrices get a `TypeTag` with `nr` and `nc` defaulting to 0 (whereas Dynamic == -1)
template<> struct typetag_helper<Eigen::Matrix<F, Dynamic, Dynamic> > {
  static constexpr TypeTag tag() { return TypeTag(TK_mat); }
  static constexpr bool isLinear() { return true; }
};
template<> struct typetag_helper<Eigen::Matrix<CF, Dynamic, Dynamic> > {
  static constexpr TypeTag tag() { return TypeTag(TK_cmat); }
  static constexpr bool isLinear() { return true; }
};

// We sometimes use `Map<Matrix>` instead of `Matrix`, but the corresponding typetag is the same
template<typename Mat> struct typetag_helper<Eigen::Map<Mat>> {
  static constexpr TypeTag tag() { return typetag_helper<Mat>::tag(); }
  static constexpr bool isLinear() { return typetag_helper<Mat>::isLinear(); }
};

// Convenience wrapper for all the above, so `typetag<Foo>()` gets us the typetag for Foo.
// Also variants like `Foo*`, and `Foo * const &`
template<typename T> constexpr TypeTag typetag() {
  return typetag_helper<std::remove_pointer_t<std::remove_cv_t<T> > >::tag();
}

template<typename T> constexpr bool isLinear() {
  return typetag_helper<std::remove_pointer_t<std::remove_cv_t<T> > >::isLinear();
}

// Convert throbol name to type.
TypeTag typeByName(string_view a);
// Convert type to throbol name
string typeName(TypeTag a);
// Convert type to C++ name, which is often the same except for arbitrary-sized matrices
optional<tuple<int, string_view>>  parseSize(string_view a);
optional<tuple<int, int, string_view>> parseSize2(string_view a);

/*
  wr_t and friends tell us how to hold a reference to a value in the pad.
  It's normally a reference, except it's a value for matrices which have
  a pointer into the pad where the entries are.
  wr_t is for a value we're going to write, rd_t to read, and up_t to update.
  So when rd_t is a ref, it's a const ref.
*/
template<typename T> struct wr_t {
  using t = T &;
};

template<> struct wr_t<mat> {
  using t = mat;
};

template<> struct wr_t<cmat> {
  using t = cmat;
};

template<typename T> struct rd_t {
  using t = T const &;
};

template<> struct rd_t<mat> {
  using t = mat;
};

template<> struct rd_t<cmat> {
  using t = cmat;
};

template<typename T> struct up_t {
  using t = T &;
};

template<> struct up_t<mat> {
  using t = mat;
};

template<> struct up_t<cmat> {
  using t = cmat;
};


template<typename T> T getZero();

template<> inline F getZero<F>() { return 0.0f; }
template<> inline CF getZero<CF>() { return CF(0.0f, 0.0f); }


template<typename T> typename rd_t<T>::t getZeroRd(TypeTag t);
template<typename T> typename up_t<T>::t getZeroUp(TypeTag t);

#define defGetZero(T) \
  template<> inline typename rd_t<T>::t getZeroRd<T>(TypeTag t) \
  { \
    static T ret; \
    setZero(ret); \
    return ret; \
  } \
  template<> inline typename up_t<T>::t getZeroUp<T>(TypeTag t) \
  { \
    static T ret; \
    setZero(ret); \
    return ret; \
  } \

defGetZero(F)
defGetZero(CF)
defGetZero(VideoFrame)
defGetZero(DrawOpHandle)
defGetZero(Beh)
defGetZero(vec2)
defGetZero(vec3)
defGetZero(vec4)
defGetZero(mat2)
defGetZero(mat3)
defGetZero(mat4)
defGetZero(Assocp)
defGetZero(ApiHandle)


/*
  A symbolic value, returned through every step of the expression tree.
  It contains a type and an offset within a Pad.
  Use .ist<T>() to see if it matches. For instances,
    if (v.ist<vec2>()) {
      ... emit code for vec2
    }
  As a special case, v.ist<mat>() matches any real matrix, and similarly for
  v.ist<cmat>()
  It's important to keep this small, because we capture a lot of them in
  lambdas and there's some small limit of captures before it 
  allocates a separate buffer.
*/
struct Val {

  TypeTag t;
  ValueOffset ofs;

  Val() : t(T_undef), ofs(0) {}
  explicit Val(TypeTag _t, ValueOffset _ofs = 0)
  : t(_t), ofs(_ofs)
  {    
  }

  template<typename T> bool ist() const {
    return t.match(typetag<T>());
  }
  template<typename T> bool convertible() const {
    return t.convmatch(typetag<T>());
  }
  bool valid() const { return !t.isUndef(); }

};

ostream & operator <<(ostream &s, Val const &a);

struct ValSuite {

  TypeTag t;
  ValueOffset ofs1, ofs8, ofs32;

  ValSuite()
    : t(T_undef), ofs1(0), ofs8(0), ofs32(0)
  {    
  }

  explicit ValSuite(TypeTag _t)
  : t(_t), ofs1(0), ofs8(0), ofs32(0)
  {    
  }

  explicit ValSuite(TypeTag _t, ValueOffset _ofs1, ValueOffset _ofs8, ValueOffset _ofs32)
  : t(_t), ofs1(_ofs1), ofs8(_ofs8), ofs32(_ofs32)
  {    
  }

  Val getRefForCompiledBatchSize(int compiledBatchSize) const
  {
    if (compiledBatchSize == 1) {
      return Val(t, ofs1);
    }
    else if (compiledBatchSize == 8) {
      return Val(t, ofs8);
    }
    else if (compiledBatchSize == 32) {
      return Val(t, ofs32);
    }
    // ADD batch size
    else {
      throw runtime_error("Bad batch");
    }
  }

  template<typename T> bool ist() const {
    return t.match(typetag<T>());
  }
  template<typename T> bool convertible() const {
    return t.convmatch(typetag<T>());
  }
  bool valid() const { return !t.isUndef(); }

};

ostream & operator <<(ostream &s, ValSuite const &a);


/*
  allocIncr: for allocating space in a Pad or a CellVals structure.
*/

template<typename T> inline U32 allocIncr(U32 &ofs, int count) {
  U32 align = alignof(T);
  ofs += (align - ofs) & (align - 1);
  auto ret = ofs;
  ofs += sizeof(T) * count;
  return ret;
}

U32 allocAlign(TypeTag t);
U32 allocSize(TypeTag t);
U32 allocIncr(U32 &ofs, TypeTag t, int count);

int typeVersion(TypeTag t);


