#pragma once
#include "./defs.h"

struct Paramset {

  vector<F> valueByParam_;
  bool hashValid = false;
  U64 paramHash_;

  Paramset() = default;
  Paramset(Paramset const &other) = default;

  const F &valueByParam(ParamIndex pi) const
  {
    assertlog(pi < valueByParam_.size(), LOGV(pi) << LOGV(valueByParam_.size()));
    return valueByParam_[pi];
  }

  F &mutValueByParam(ParamIndex pi)
  {
    hashValid = false;
    if (!(pi < valueByParam_.size())) {
      valueByParam_.resize(pi+1);
    }
    return valueByParam_[pi];
  }

  U64 hash()
  {
    if (!hashValid) updateHash();
    return paramHash_;
  }
  void updateHash();
};
