#include "./test_utils.h"

TEST_CASE("Topology", "[sheet][topology]")
{
  Sheet sh;
  REQUIRE(mkSheetCheck(sh, R"(
foo: bar + 1
bar: buz + wug
buz: 2
wug: 3~5
)", "topology1"));

  auto t = sh.getCellTopology();

}

