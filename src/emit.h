#pragma once
#include "./sheet.h"
#include "./ast.h"
#include "./execlist.h"

enum CellEmitState {
  CES_init,
  CES_injected,
  CES_working,
  CES_typing,
  CES_placeholder,
  CES_done,
  CES_err
};


template<int COMPILED_BATCH_SIZE> struct BatchDef
{
  static consteval int compiledBatchSize() { return COMPILED_BATCH_SIZE; }
  static Val getVal(ValSuite v);
};

template<> inline Val BatchDef<1>::getVal(ValSuite v) { return Val(v.t, v.ofs1); }
template<> inline Val BatchDef<8>::getVal(ValSuite v) { return Val(v.t, v.ofs8); }
template<> inline Val BatchDef<32>::getVal(ValSuite v) { return Val(v.t, v.ofs32); }
// ADD batch size

struct SheetEmitContext {

  CompiledSheet &she;
  SheetEmitContext *parent;
  unordered_map<CellIndex, BoxedRef> refByCell;
  vector<bool> overlay;
  vector<CellEmitState> progress;

  vector<tuple<CellIndex, FormulaSubstr>> origins;

  SheetEmitContext(CompiledSheet &_she, SheetEmitContext *_parent);
  void setProgress(CellIndex cell, CellEmitState newState);
  string ctxLabel();

};

struct CompiledSheet : AllocTrackingMixin<CompiledSheet> {

  Sheet &sh;
  CellProblems orderProblems;

  ValueOffset valueDataSize = 0;
  ValueOffset padSize1 = 0;
  ValueOffset padSize8 = 0;
  ValueOffset padSize32 = 0;
  // ADD batch size

  bool ok = false;
  bool doDebug = false;
  int perfLogBackoff = 0;
  clock_t compileTime = 0;
  EmitMode emitMode;

  vector<ApiHandle> apiDefByCell;
  vector<ApiDirection> apiDirectionByCell;

  vector<CellIndex> curCellStack;

  ExecList<CpuState &, Pad &, BatchDef<1>> evalFuncs1;
  ExecList<CpuState &, Pad &, BatchDef<8>> evalFuncs8;
  ExecList<CpuState &, Pad &, BatchDef<32>> evalFuncs32;
  // ADD batch size

  ExecList<CpuState &, Pad &, Pad &, BatchDef<1>> gradFuncs1;
  ExecList<CpuState &, Pad &, Pad &, BatchDef<8>> gradFuncs8;
  ExecList<CpuState &, Pad &, Pad &, BatchDef<32>> gradFuncs32;
  // ADD batch size

  void evalop(auto a) {
    evalFuncs1.push_back(a);
    evalFuncs8.push_back(a);
    evalFuncs32.push_back(a);
    // ADD batch size
  }

  void gradop(auto a) {
    gradFuncs1.push_back(a);
    gradFuncs8.push_back(a);
    gradFuncs32.push_back(a);
    // ADD batch size
  }

  vector<vector<DebugTestPoint>> debugsByCell;
  vector<string> nameByCell; // copied from sheet when compiled

  vector<shared_ptr<SheetEmitContext>> contexts;

  vector<string> emitErrorByCell;
  vector<FormulaSubstr> emitErrorLocByCell;

  shared_ptr<CellVals> zeroVals;

  BoxedRef dtRef;
  BoxedRef tickCountRef;

  map<string, shared_ptr<MatrixXf>> extMatByFilename;

  vector<shared_ptr<SheetApi>> apis;
  map<U64, shared_ptr<CellVals>> t0ValCacheByParamHash;

  vector<TypeTag> typeByCell;

  U32 totEmitCells = 0, totEmitOps = 0, totEmitErr = 0;

  vector<BoxedRef> refByVid;

  vector<bool> hasParamByCell;
  vector<bool> isPureParamByCell;
  vector<bool> hasRefByCell;
  vector<bool> hasPrevByCell;
  vector<bool> hasDtByCell;
  vector<bool> hasTimeByCell;
  vector<bool> isTimeInvariantByCell;
  vector<bool> isApiByCell;

  // Set by the specific API
  bool hasLiveApi = false;
  bool hasSimApi = false;

  Arena buildlife;
  U64 epoch = 0;

  explicit CompiledSheet(Sheet &_sh, EmitMode _emitMode);
  ~CompiledSheet();

  void doSheet();
  BoxedRef doCell(CellIndex cell, SheetEmitContext *ctx, bool deferrable);
  BoxedRef emitCell(CellIndex cell, SheetEmitContext *hypoCtx);
  BoxedRef typeCell(CellIndex cell, SheetEmitContext *hypoCtx);
  void reportCycle(string const &err);

  MatrixXf *loadCsv(string name, Index nr, Index nc);

  ValueOffset allocValue(TypeTag t);

  BoxedRef mkValue(CellIndex cell, TypeTag t);  
  BoxedRef getValue(CellIndex cell);
  BoxedRef getValue(CellIndex cell, SheetEmitContext *ctx);

  SheetEmitContext *mkContext(SheetEmitContext *parent);
  void addDeps(SheetEmitContext *ctx, CellIndex cell);
  void addDeps(SheetEmitContext *ctx);

  void setError(CellIndex cell, string const &e, AstNode *n=nullptr);

  map<string, string> getExpectedApiVersions();
  bool checkExpectedApiVersions(map<string, string> const &expectedApiVersions, function<void(string const &)> onErr);
  YAML::Node getApiSchema();
  shared_ptr<CellVals> getT0Vals(Paramset *params);

};

#if 0
void debugCellInfo(ostream &s, CellIndex cell, Pad &pad, shared_ptr<CompiledSheet> &compiled);
#endif

struct CellEmitter;

struct NodeEmitter {

  CellEmitter &ce;
  CompiledSheet &she;
  AstNode *n = nullptr;
  FormulaSubstr debugLoc;
  int depth = 0;
  bool evalDone = false;
  bool evalSpecialDone = false;
  bool inCheck = false;
  vector<ValSuite> args;

  NodeEmitter(CellEmitter &_ce, AstNode *_n, int _depth);
  NodeEmitter(NodeEmitter &_parent, AstNode *_n);

  int evalArgs();
  bool matchArgs(initializer_list<TypeTag> want, bool allowMore=false);

  void evalSpecial(AstNode *a)
  {
    assert(!evalDone);
    args.push_back(NodeEmitter(*this, a).emit());
    evalDone = evalSpecialDone = true;
  }

  void evalSpecial(AstNode *a, AstNode *b)
  {
    assert(!evalDone);
    args.push_back(NodeEmitter(*this, a).emit());
    args.push_back(NodeEmitter(*this, b).emit());
    evalDone = evalSpecialDone = true;
  }

  void evalSpecial(AstNode *a, AstNode *b, AstNode *c)
  {
    assert(!evalDone);
    args.push_back(NodeEmitter(*this, a).emit());
    args.push_back(NodeEmitter(*this, b).emit());
    args.push_back(NodeEmitter(*this, c).emit());
    evalDone = evalSpecialDone = true;
  }

  void unevalArgs()
  {
    evalDone = evalSpecialDone = false;
    args.clear();
  }

  void fmtCalledAs(ostream &s);

  ValSuite setError(string const &e, AstNode *n1=nullptr);

  ValSuite setInvalidArgs()
  {
    return setError("");
  }

  ValSuite allocPad(TypeTag t);
  ValSuite allocResult(TypeTag t);

  BoxedRef mkValue(TypeTag t);

  // Convenience
  template<typename T> ValSuite allocPad()
  {
    return allocPad(typetag<T>());
  }
  template<typename T> ValSuite allocResult()
  {
    return allocResult(typetag<T>());
  }

  ValSuite emit();

  #define declemit(T) ValSuite emit_##T();
  foreach_ast(declemit);
  #undef declemit
};

struct CellEmitter {

  CompiledSheet &she;
  Sheet &sh;
  CellIndex cell = nullCellIndex;
  AstNode *root = nullptr;
  bool ok = true;
  bool hasParam = false;
  bool hasRef = false;
  bool hasPrev = false;
  bool hasDt = false;
  bool hasTime = false;
  bool hasRandom = false;
  bool isPureParam = false;
  U32 lexIndex = 0;
  string error;
  FormulaSubstr errorLoc;
  unordered_map<string_view, ValSuite> locals;
  SheetEmitContext *hypoCtx = nullptr;

  CellEmitter(CompiledSheet &_she, CellIndex _cell, SheetEmitContext *_hypoCtx);

  /*
    allocPad, to get sratchpad space
  */
  ValSuite allocPad(TypeTag t)
  {
    if (t.isError()) return setError("Allocating error");
    if (t.isUndef()) return setError("Allocating undef");
    if (t.isMat() || t.isCmat()) {
      if (t.nr == 0 || t.nc == 0) {
        return setError("Allocating unsized matrix");
      }
    }
    return ValSuite(t,
      allocIncr(she.padSize1, t, 1), 
      allocIncr(she.padSize8, t, 8), 
      allocIncr(she.padSize32, t, 32)
    );
  }

  ValSuite setError(string const &e, AstNode *n=nullptr);
  ValSuite setError();

  void evalop(auto a)
  {
    she.evalop(a);
  }
  void gradop(auto a)
  {
    she.gradop(a);
  }

  void setDebugVal(AstType t, FormulaSubstr loc, ValSuite v)
  {
    if (!v.valid()) return;
    she.debugsByCell[cell].emplace_back(v, t, loc);
  }

  BoxedRef resolveScopedName(CellIndex srcCell, SheetEmitContext *ctx, bool deferrable);
  BoxedRef resolveName(AstNode *name, bool deferrable);

  ValSuite emitCell(AstNode *n);

  ParamIndex getParamIndex(AstNode *n);
  void setValue(BoxedRef dst, ValSuite v);

  /*
    mkValue, to get a slot in CellVals (every cell has one, but you can have internal
    ones for stateful operators also)
  */
  BoxedRef mkValue(TypeTag t)
  {
    return she.mkValue(cell, t);
  }

};

struct CellTypeAnalyzer {

  CompiledSheet &she;
  CellIndex cell;
  AstNode *root = nullptr;
  SheetEmitContext *hypoCtx = nullptr;
  TypeTag type{T_error};
  string error;
  FormulaSubstr errorLoc;
  
  bool ok = false;
  unordered_map<string_view, TypeTag> locals;

  CellTypeAnalyzer(CompiledSheet &_she, CellIndex _cell, SheetEmitContext *_hypoCtx);

  TypeTag eval(AstNode *n);
  TypeTag setError(string const &e, AstNode *n=nullptr);
  TypeTag setError();

  #define decleval(T) TypeTag eval_##T(AstNode *n);
  foreach_ast(decleval);
  #undef decleval

};


#define defemit(T) \
  ValSuite NodeEmitter::emit_##T()

#define deftype(T) \
  TypeTag CellTypeAnalyzer::eval_##T(AstNode *n)


struct DefTestInstance {
  
  char const *name;
  char const *dump;
  DefTestInstance *prev;
  static DefTestInstance *head;

  DefTestInstance(char const *_name, char const *_dump)
    :name(_name), dump(_dump), prev(head)
  {
    head = this;
  }

};

#define DEFTEST_NAME(NAME, LINE) NAME##LINE
#define DEFTEST_STR2(LINE) #LINE
#define DEFTEST_STR(LINE) DEFTEST_STR2(LINE)

#define deftest(T, DUMP) static DefTestInstance DEFTEST_NAME(T, __LINE__) (#T " at " __FILE__ ":" DEFTEST_STR(__LINE__), DUMP);

ostream & operator <<(ostream &s, tuple<Val, AstNode *> const &a);
ostream & operator <<(ostream &s, Val const &a);
