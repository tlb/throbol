#pragma once
#include "./defs.h"


string hashcodeStr(string_view src, size_t strLen=20);

U64 hashcodeU64(string_view src);

U64 hashcodeU64(vector<U64> const &src);

struct ObjectKey {
  void const *ptr;
  U64 const code;
  size_t hash;

  ObjectKey(void const *_ptr, U64 _code)
    : ptr(_ptr), code(_code)
  {
    setHash();
  }

  ObjectKey(void const *_ptr, string_view _code)
    : ObjectKey(_ptr, hashcodeU64(_code))
  {
    setHash();
  }

  void setHash()
  {
    size_t h1 = std::hash<char const *>{}((char const *)ptr);
    size_t h2 = std::hash<U64>{}(code);
    hash = h1 ^ (h2 << 1);
  }


};

inline bool operator == (ObjectKey const &a, ObjectKey const &b)
{
  return a.ptr == b.ptr && a.code == b.code;
}

template<>
struct std::hash<ObjectKey>
{
  size_t operator()(ObjectKey const& it) const noexcept
  {
    return it.hash;
  }
};