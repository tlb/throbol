#include "./emit.h"
#include "./emit_macros.h"


// operator !
deftype(not)
{
  return typetag<bool>();
}

defemit(not)
{
  if (inCheck) {
    defop1(Checkp, Checkp, r = op_not(cpu, a));
  }
  defop1_simd(bool, bool, r = !a);
  return setInvalidArgs();
}

defusage(not, R"(
  !F
  !Check)");


deftype(compare)
{
  return typetag<bool>();
}

defemit(compare)
{
  assert(n->nch() >= 2);

  vector<AstType> compareops;
  args.push_back(NodeEmitter(ce, n->ch(0), depth+1).emit()); // first arg
  vector<AstNode *> debugnodes;

  for (int i = 1; i < n->nch(); i++) {
    compareops.push_back(n->ch(i)->t);
    args.push_back(NodeEmitter(ce, n->ch(i)->ch(0), depth+1).emit());
    debugnodes.push_back(n->ch(i));
  }
  evalDone = true;

  ValSuite finalr;
  for (size_t argi = 0; argi + 1 < args.size(); argi++) {
    ValSuite subr;

    switch (compareops[argi]) {
      case A_le:
        if (inCheck) {
          defsubop2(F, args[argi], F, args[argi+1], Checkp, subr, r = cpu.pool->mk<CheckCompare>(a <= b ? Check_pass : Check_fail, A_le, a, b));
        }
        defsubop2(F, args[argi], F, args[argi+1], bool, subr, r = a <= b);
        return setInvalidArgs();
  
      case A_lt:
        if (inCheck) {
          defsubop2(F, args[argi], F, args[argi+1], Checkp, subr, r = cpu.pool->mk<CheckCompare>(a < b ? Check_pass : Check_fail, A_lt, a, b));
        }
        defsubop2(F, args[argi], F, args[argi+1], bool, subr, r = a < b);
        return setInvalidArgs();

      case A_ge:
        if (inCheck) {
          defsubop2(F, args[argi], F, args[argi+1], Checkp, subr, r = cpu.pool->mk<CheckCompare>(a >= b ? Check_pass : Check_fail, A_ge, a, b));
        }
        defsubop2(F, args[argi], F, args[argi+1], bool, subr, r = a >= b);
        return setInvalidArgs();


      case A_gt:
        if (inCheck) {
          defsubop2(F, args[argi], F, args[argi+1], Checkp, subr, r = cpu.pool->mk<CheckCompare>(a > b ? Check_pass : Check_fail, A_gt, a, b));
        }
        defsubop2(F, args[argi], F, args[argi+1], bool, subr, r = a > b);
        return setInvalidArgs();

      case A_sim:
        if (inCheck) {
          defsubop2(F, args[argi], F, args[argi+1], Checkp, subr, r = cpu.pool->mk<CheckCompare>(clamp01(1.0f - 100.0f * abs(a - b)) >= 0.5f ? Check_pass : Check_fail, A_sim, a, b));
        }
        defsubop2(F, args[argi], F, args[argi+1], F, subr, r = clamp01(1.0f - 100.0f * abs(a - b)));
        defsubop2(CF, args[argi], CF, args[argi+1], F, subr, r = clamp01(1.0f - 100.0f * abs(a - b)));
        defsubop2(vec2, args[argi], vec2, args[argi+1], F, subr, r = clamp01(1.0f - 100.0f * (a.array() - b.array()).abs().sum()));
        defsubop2(vec3, args[argi], vec3, args[argi+1], F, subr, r = clamp01(1.0f - 100.0f * (a.array() - b.array()).abs().sum()));
        defsubop2(vec4, args[argi], vec4, args[argi+1], F, subr, r = clamp01(1.0f - 100.0f * (a.array() - b.array()).abs().sum()));
        defsubop2(mat2, args[argi], mat2, args[argi+1], F, subr, r = clamp01(1.0f - 100.0f * (a.array() - b.array()).abs().sum()));
        defsubop2(mat3, args[argi], mat3, args[argi+1], F, subr, r = clamp01(1.0f - 100.0f * (a.array() - b.array()).abs().sum()));
        defsubop2(mat4, args[argi], mat4, args[argi+1], F, subr, r = clamp01(1.0f - 100.0f * (a.array() - b.array()).abs().sum()));
        defsubop2(mat, args[argi], mat, args[argi+1], F, subr, r = (a.cols() == b.cols() && a.rows() == b.rows()) ? clamp01(1.0f - 100.0f * (a.array() - b.array()).abs().sum()) : 0.0f);
        defsubop2(cmat, args[argi], cmat, args[argi+1], F, subr, r = (a.cols() == b.cols() && a.rows() == b.rows()) ? clamp01(1.0f - 100.0f * (a.array() - b.array()).abs().sum()) : 0.0f);
        defsubop2(string_view, args[argi], string_view, args[argi+1], F, subr, r = (a == b) ? 1.0f : 0.0f);
        return setInvalidArgs();

      default:
        return setError("Internal error: unknown compare symbol");
    }
    if (she.doDebug) ce.setDebugVal(debugnodes[argi]->t, debugnodes[argi]->loc, subr);

    if (argi > 0) {
      auto newfinalr = allocPad<F>();
      while (1) {
        defsubop2(Checkp, finalr, Checkp, subr, Checkp, newfinalr, r = op_and(cpu, a, b));
        defsubop2(F, finalr, Checkp, subr, Checkp, newfinalr, r = op_and(cpu, op_check_bool(cpu, a), b));
        defsubop2(Checkp, finalr, F, subr, Checkp, newfinalr, r = op_and(cpu, a, op_check_bool(cpu, b)));
        defsubop2(F, finalr, F, subr, F, newfinalr, r = a && b);
        return setError("Internal error: unknown compare symbol");
      }
      finalr = newfinalr;
    }
    else {
      finalr = subr;
    }
  }
  if (she.doDebug) ce.setDebugVal(n->t, n->loc, finalr);
  return finalr;
}

defusage(compare, R"(
  F <= F
  F >= F
  F < F
  F > F
  T ~~ T)");


// These should all be handled by emit_compare. These stubs are just here
// because we need something for every ast node type
// operator <
deftype(lt)
{
  return typetag<bool>();
}

defemit(lt)
{
  throw logic_error("emit_lt");
}

defusage(lt, R"(
  F < F)");


// operator <
deftype(le)
{
  return typetag<bool>();
}

defemit(le)
{
  throw logic_error("emit_le");
}

defusage(le, R"(
  F <= F)");


// operator >
deftype(gt)
{
  return typetag<bool>();
}

defemit(gt)
{
  throw logic_error("emit_gt");
}

defusage(gt, R"(
  F > F)");


// operator >=
deftype(ge)
{
  return typetag<bool>();
}

defemit(ge)
{
  throw logic_error("emit_ge");
}

defusage(ge, R"(
  F >= F)");


// operator ~~
deftype(sim)
{
  return typetag<bool>();
}

defemit(sim)
{
  throw logic_error("emit_sim");
}

defusage(sim, R"(
  T ~~ T)");


// operator ~~~
deftype(mbs)
{
  auto at = eval(n->ch(0));
  if (!at.isError()) return at;
  auto bt = eval(n->ch(1));
  if (!bt.isError()) return bt;
  return setError();
}

defemit(mbs)
{
  CellIndex cell = ce.cell;
  defop2a(F, F, cpu.checktrue(clamp01(1.0f - 100.0f * abs(a - b)), batchi, cell));
  defop2a(CF, CF, cpu.checktrue(clamp01(1.0f - 100.0f * abs(a - b)), batchi, cell));
  defop2a(vec2, vec2, cpu.checktrue(clamp01(1.0f - 100.0f * (a.array() - b.array()).abs().sum()), batchi, cell));
  defop2a(vec3, vec3, cpu.checktrue(clamp01(1.0f - 100.0f * (a.array() - b.array()).abs().sum()), batchi, cell));
  defop2a(vec4, vec4, cpu.checktrue(clamp01(1.0f - 100.0f * (a.array() - b.array()).abs().sum()), batchi, cell));
  defop2a(mat2, mat2, cpu.checktrue(clamp01(1.0f - 100.0f * (a.array() - b.array()).abs().sum()), batchi, cell));
  defop2a(mat3, mat3, cpu.checktrue(clamp01(1.0f - 100.0f * (a.array() - b.array()).abs().sum()), batchi, cell));
  defop2a(mat4, mat4, cpu.checktrue(clamp01(1.0f - 100.0f * (a.array() - b.array()).abs().sum()), batchi, cell));
  defop2a(mat, mat, cpu.checktrue(a.cols() == b.cols() && a.rows() == b.rows() ? clamp01(1.0f - 100.0f * (a.array() - b.array()).abs().sum()) : 0.0f, batchi, cell));
  defop2a(cmat, cmat, cpu.checktrue(a.cols() == b.cols() && a.rows() == b.rows() ? clamp01(1.0f - 100.0f * (a.array() - b.array()).abs().sum()) : 0.0f, batchi, cell));
  defop2a(mat4, mat, cpu.checktrue(a.cols() == b.cols() && a.rows() == b.rows() ? clamp01(1.0f - 100.0f * (a.array() - b.array()).abs().sum()) : 0.0f, batchi, cell));
  defop2a(mat, mat4, cpu.checktrue(a.cols() == b.cols() && a.rows() == b.rows() ? clamp01(1.0f - 100.0f * (a.array() - b.array()).abs().sum()) : 0.0f, batchi, cell));
  defop2a(string_view, string_view, cpu.checktrue(a == b, batchi, cell));
  return setInvalidArgs();
}

defusage(mbs, R"(
  T ~~~ T)");


// operator ==
deftype(eq)
{
  return typetag<bool>();
}

defemit(eq)
{
  defop2_simd(F, F, bool, r = a.array() == b.array());
  defop2(CF, CF, bool, r = a == b);
  defop2(vec2, vec2, bool, r = a == b);
  defop2(vec3, vec3, bool, r = a == b);
  defop2(vec4, vec4, bool, r = a == b);
  defop2(mat2, mat2, bool, r = a == b);
  defop2(mat3, mat3, bool, r = a == b);
  defop2(mat4, mat4, bool, r = a == b);
  defop2(mat, mat, bool, r = (a.cols() == b.cols() && a.rows() == b.rows()) && a == b);
  defop2(cmat, cmat, bool, r = (a.cols() == b.cols() && a.rows() == b.rows()) && a == b);
  defop2(string_view, string_view, bool, r = a == b);
  defop2(SymbolSet, SymbolSet, bool, r = op_eq(cpu, a, b));
  return setInvalidArgs();
}

defusage(eq, R"(
  T == T)");



// operator !=
deftype(neq)
{
  return typetag<bool>();
}

defemit(neq)
{
  defop2_simd(F, F, bool, r = a.array() != b.array());
  defop2(CF, CF, bool, r = a != b);
  defop2(vec2, vec2, bool, r = a != b);
  defop2(vec3, vec3, bool, r = a != b);
  defop2(vec4, vec4, bool, r = a != b);
  defop2(mat2, mat2, bool, r = a != b);
  defop2(mat3, mat3, bool, r = a != b);
  defop2(mat4, mat4, bool, r = a != b);
  defop2(mat, mat, bool, r = (a.cols() == b.cols() && a.rows() == b.rows()) && a != b);
  defop2(cmat, cmat, bool, r = (a.cols() == b.cols() && a.rows() == b.rows()) && a != b);
  defop2(string_view, string_view, bool, r = a != b);
  defop2(SymbolSet, SymbolSet, bool, r = op_eq(cpu, a, b));
  return setInvalidArgs();
}

defusage(neq, R"(
  T != T)");



// operator &&
deftype(and)
{
  assert(n->nch() == 2);
  auto at = eval(n->ch(0));
  if (at.ist<Checkp>()) return typetag<Checkp>();
  if (at.ist<Beh>()) return typetag<Beh>();
  auto bt = eval(n->ch(1));
  if (bt.ist<Checkp>()) return typetag<Check>();
  if (bt.ist<Beh>()) return typetag<Beh>();
  return typetag<bool>();
}

defemit(and)
{
  defop2(Checkp, Checkp, Checkp, r = op_and(cpu, a, b))

  defop2(Beh, Beh, Beh, r = op_seq(cpu, a, b));
  defop2(bool, Beh, Beh, r = op_seq(cpu, op_cond(cpu, a), b));
  defop2(Beh, F, Beh, r = op_seq(cpu, a, op_cond(cpu, b)));

  defop2_simd(bool, bool, bool, r = a && b);

  return setInvalidArgs();
}

defusage(and, R"(
  F && F
  Check && Check
  Beh && Beh
  F && Beh
  Beh && F)");


// operator ||
deftype(or)
{
  assert(n->nch() == 2);
  auto at = eval(n->ch(0));
  if (at.ist<Checkp>()) return typetag<Checkp>();
  if (at.ist<Beh>()) return typetag<Beh>();
  auto bt = eval(n->ch(1));
  if (bt.ist<Checkp>()) return typetag<Check>();
  if (bt.ist<Beh>()) return typetag<Beh>();
  return typetag<F>();
}

defemit(or)
{
  defop2_simd(F, F, bool, r = (a >= 0.5) || (b >= 0.5));
  defop2(Checkp, Checkp, Checkp, r = op_or(cpu, a, b))

  defop2(Beh, Beh, Beh, r = op_fallback(cpu, a, b));
  defop2(F, Beh, Beh, r = op_fallback(cpu, op_cond(cpu, a), b));
  defop2(Beh, F, Beh, r = op_fallback(cpu, a, op_cond(cpu, b)));

  return setInvalidArgs();
}

defusage(or, R"(
  F || F
  Check || Check
  Beh || Beh
  F || Beh
  Beh || F)");


// operator |
deftype(union)
{
  auto at = eval(n->ch(0));
  return at;
}

defemit(union)
{
  defop2(DrawOpHandle, DrawOpHandle, DrawOpHandle, r = op_union(cpu, a, b));
  defop2(SymbolSet, SymbolSet, SymbolSet, r = op_union(cpu, a, b));

  defop2(F, F, F, r = isnan(a) ? b : a);
  return setInvalidArgs();
}

defusage(union, R"(
  F | F
  DrawOp | DrawOp
  SymbolSet | SymbolSet)");


// operator &
deftype(intersection)
{
  auto at = eval(n->ch(0));
  return at;
}

defemit(intersection)
{
  defop2(SymbolSet, SymbolSet, SymbolSet, r = op_intersection(cpu, a, b));

  defop2(F, F, F, r = isnan(a) ? a : b);

  return setInvalidArgs();
}

defusage(intersection, R"(
  F & F
  DrawOp & DrawOp
  SymbolSet & SymbolSet)");


// operator ->
deftype(implies)
{
  return typetag<F>();
}

defemit(implies)
{
  defop2(bool, bool, bool, r = !a || b);
  return setInvalidArgs();
}

defusage(implies, R"(
  a ∊ bool -> b ∊ bool -- shorthand for !a || b)");


deftype(check)
{
  return typetag<Check>();
}

defemit(check)
{
  CellIndex cell = ce.cell;
  defop1(F, Checkp, r = op_checkalways(cpu, op_check_bool(cpu, a), batchi, cell));
  defop1(Checkp, Checkp, r = op_checkalways(cpu, a, batchi, cell));
  return setInvalidArgs();
}

defusage(check, R"(
  check(F) -- complain if false
  check(Check))");


deftype(checkat)
{
  return typetag<Check>();
}

defemit(checkat)
{
  CellIndex cell = ce.cell;
  defop2(F, Checkp, Checkp, r = op_checkat(cpu, a, b, batchi, cell));
  defop2(F, F, Checkp, r = op_checkat(cpu, a, op_check_bool(cpu, b), batchi, cell));
  return setInvalidArgs();
}

defusage(checkat, R"(
  checkat(time ∊ F, F) -- complain if false at the given time
  checkat(time ∊ F, Check))");



deftype(checkbetween)
{
  return typetag<Check>();
}

defemit(checkbetween)
{
  CellIndex cell = ce.cell;
  defop3(F, F, Checkp, Checkp, r = op_checkbetween(cpu, a, b, c, batchi, cell));
  defop3(F, F, F, Checkp, r = op_checkbetween(cpu, a, b, op_check_bool(cpu, c), batchi, cell));
  return setInvalidArgs();
}

defusage(checkbetween, R"(
  checkbetween(t0 ∊ F, t0 ∊ F, F) -- complain if false and t0 <= time <= t1
  checkbetween(t0 ∊ F, t1 ∊ F, Check))");


deftype(at)
{
  return typetag<F>();
}

defemit(at)
{
  defop1(F, bool, r = op_at(cpu, a, batchi));
  defop2(F, F, F, r = op_at(cpu, a, batchi) ? b : 0.0f);
  return setInvalidArgs();
}

defusage(at, R"(
  at(time ∊ F) -- 1 at given time, otherwise 0
  at(time ∊ F, value ∊ F) -- value at given time, otherwise 0)");


deftype(failCellCount)
{
  return typetag<F>();
}

defemit(failCellCount)
{
  defop0(F, r = (cpu.traces[batchi] ? cpu.traces[batchi]->checkFailuresByCell.size() * 1.0f : 0.0f));
  return setInvalidArgs();
}

defusage(failCellCount, R"(
  fail.cellCount -- number of cells with any failures so far)");


deftype(failTime)
{
  return typetag<F>();
}

defemit(failTime)
{
  defop0(F, r = (cpu.traces[batchi] ? cpu.traces[batchi]->failTime : 0.0f));
  return setInvalidArgs();
}

defusage(failTime, R"(
  fail.time -- total duration of failures)");


deftype(has)
{
  return typetag<bool>();
}

defemit(has)
{
  defop2(SymbolSet, SymbolSet, bool, r = op_hasany(cpu, a, b))
  defop2(Beh, SymbolSet, bool, r = (a.status == B_running && op_hasany(cpu, a.runset, b)));
  return setInvalidArgs();
}

defusage(has, R"(
  has(a ∊ SymbolSet, b ∊ SymbolSet) -- true if a and b have any symbols in common
  has(a ∊ Beh, b ∊ SymbolSet) -- true if a is running a member of b)");


deftype(switch)
{
  if (n->nch() > 1) {
    auto bt = eval(n->ch(1));
    if (!bt.isError()) return bt;
    if (n->nch() > 2) {
      auto ct = eval(n->ch(2));
      if (!ct.isError()) return ct;
    }
  }
  return setError();
}

defemit(switch)
{
  defop3_simd(bool, F, F, F, r = a.select(b, c));
  defop3_simd(bool, CF, CF, CF, r = a.select(b, c));
  defop3(bool, Beh, Beh, Beh, r = a ? b : c);
  defop3(bool, SymbolSet, SymbolSet, SymbolSet, r = a ? b : c);
  defop3(bool, string_view, string_view, string_view, r = a ? b : c);

  defop3(bool, vec2, vec2, vec2, r = a ? b : c);
  defop3(bool, vec3, vec3, vec3, r = a ? b : c);
  defop3(bool, vec4, vec4, vec4, r = a ? b : c);
  defop3(bool, mat2, mat2, mat2, r = a ? b : c);
  defop3(bool, mat3, mat3, mat3, r = a ? b : c);
  defop3(bool, mat4, mat4, mat4, r = a ? b : c);

  defop3rg(bool, mat, mat, mat,
    args[1].t == args[2].t,
    allocResult(args[1].t),
    r = a ? b : c,
    a ? (bg += rg) : (cg += rg));
  defop3rg(bool, cmat, cmat, cmat,
    args[1].t == args[2].t,
    allocResult(args[1].t),
    r = a ? b : c,
    a ? (bg += rg) : (cg += rg));


  defop2(bool, F, F, r = a ? b : numeric_limits<F>::quiet_NaN());
  defop2(bool, CF, CF, r = a ? b : complex<F>(numeric_limits<F>::quiet_NaN(), numeric_limits<F>::quiet_NaN()));

  defop2(bool, vec2, vec2, r = a ? b : vec2(numeric_limits<F>::quiet_NaN(), numeric_limits<F>::quiet_NaN()));
  defop2(bool, vec3, vec3, r = a ? b : vec3(numeric_limits<F>::quiet_NaN(), numeric_limits<F>::quiet_NaN(), numeric_limits<F>::quiet_NaN()));
  defop2(bool, vec4, vec4, r = a ? b : vec4(numeric_limits<F>::quiet_NaN(), numeric_limits<F>::quiet_NaN(), numeric_limits<F>::quiet_NaN(), numeric_limits<F>::quiet_NaN()));
  // WRITEME: mat2, ...

  return setInvalidArgs();
}

defusage(switch, R"(
  F ? T : T)");
