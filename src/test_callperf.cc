#include "./test_utils.h"

void timecall(vector<std::function<void(F *p)>> &fs, string const &label)
{
  F count = 0;
  auto t0 = clock();
  const long ITERS = 1000000;
  for (size_t i = 0; i < ITERS; i++) {
    for (auto &it : fs) {
      it(&count);
    }
  }
  auto t1 = clock();
  L() << "Std::function invoke time: " << label << ": " << repr_clockper(t1-t0, ITERS) << " count=" << count;
}


void timecall(vector<void (*)(F *p)> &fs, string const &label)
{
  F count = 0;
  auto t0 = clock();
  const long ITERS = 1000000;
  for (size_t i = 0; i < ITERS; i++) {
    for (auto &it : fs) {
      it(&count);
    }
  }
  auto t1 = clock();
  L() << "Raw function invoke time: " << label << ": " << repr_clockper(t1-t0, ITERS) << " count=" << to_string(count);
}


TEST_CASE("Perf of std::function invocation", "[.][perf]") {


  int i = 1, j = 2, k = 3, l = 4;

  {
    vector<void (*)(F *p)> fs;
    fs.push_back([](F *p) {
      *p += 16;
    });
    timecall(fs, "[]");
  }

  {
    vector<std::function<void(F *p)>> fs;
    fs.push_back([](F *p) {
      *p += 15;
    });
    timecall(fs, "[]");
  }

  {
    vector<std::function<void(F *p)>> fs;
    fs.push_back([i](F *p) {
      *p += i + 5;
    });
    timecall(fs, "[i]");
  }

  {
    vector<std::function<void(F *p)>> fs;
    fs.push_back([i, j, k](F *p) {
      *p += i + j + k;
    });
    timecall(fs, "[i, j, k]");
  }
  
  {
    vector<std::function<void(F *p)>> fs;
    fs.push_back([i, j, k, l](F *p) {
      *p += i + j + k + l;
    });
    timecall(fs, "[i, j, k, l]");
  }

  {
    struct Foo {
      size_t a, b, c;
    };
    Foo foo{1,2,3};
    vector<std::function<void(F *p)>> fs;
    fs.push_back([i, j, k, l, foo](F *p) {
      *p += i + j + k + l + foo.a;
    });
    timecall(fs, "[i, j, k, l, foo]");
  }

};
