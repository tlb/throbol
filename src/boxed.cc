#include "./core.h"
#include "./boxed.h"
#include "../geom/geom.h"
#include "./sheet.h"
#include "./emit.h"


BoxedPtr BoxedRef::getPtr(CellVals const *ref) const
{
  return BoxedPtr(t, (void *)&ref->buf[ofs]);
}


void BoxedPtr::assign(BoxedPtr const &rhs, int compiledBatchSize)
{
  memcpy((void *)p, (void *)rhs.p, allocSize(t) * compiledBatchSize);
}


ostream & operator <<(ostream &s, WithSheet<BoxedPtr> const &sa)
{
  auto [sh, a] = sa;
  switch (a.t.kind) {

    case TK_undef:
      return s << "undef";

    case TK_error:
      return s << "<error>";

    case TK_F:
      return s << a.get<F>();

    case TK_CF:
      return s << a.get<CF>();

    case TK_bool:
      return s << (a.get<bool>() ? 1 : 0);

    case TK_mat:
      if (a.ist<vec2>()) {
        return s << a.get<vec2>();
      }
      else if (a.ist<vec3>()) {
        return s << a.get<vec3>();
      }
      else if (a.ist<vec4>()) {
        return s << a.get<vec4>();
      }
      else if (a.ist<mat2>()) {
        return s << a.get<mat2>();
      }
      else if (a.ist<mat3>()) {
        return s << a.get<mat3>();
      }
      else if (a.ist<mat4>()) {
        return s << a.get<mat4>();
      }
      else {
        return s << a.get_mat();
      }

    case TK_cmat:
      return s << a.get_cmat();

    case TK_str:
      return s << cppEscape(a.get<string_view>());

    case TK_DrawOp:
      return s << WithSheet(sh, a.get<DrawOpHandle>());

    case TK_Check:
      return s << a.get<Checkp>();

    case TK_SymbolSet:
      return s << WithSheet(sh, a.get<SymbolSet>());
    
    case TK_Beh:
      return s << WithSheet(sh, a.get<Beh>());

    case TK_VideoFrame:
      return s << a.get<VideoFrame>();

    case TK_Assoc:
      return s << a.get<Assocp>();

    case TK_Api:
      return s << WithSheet(sh, a.get<ApiHandle>());

    // ADD type

  }
  return s << "WRITEME";
}

ostream & operator <<(ostream &s, BoxedPtr const &a)
{
  Sheet sh;
  return s << WithSheet(sh, a);
}


string repr(BoxedRef const &a, CellVals *ref)
{
  return repr(WithSheet(ref->compiled->sh, a.getPtr(ref)));
}


ostream & operator <<(ostream &s, WithSheet<BoxedRef> const &sha)
{
  auto &[sh, a] = sha;
  return s << a.t << "(cell=" << sh.nameByCell.at(a.cell) << " v=" << a.ofs << ")";
}

ostream & operator <<(ostream &s, BoxedRef const &a)
{
  return s << a.t << "(cell=" << a.cell << " v=" << a.ofs << ")";
}

bool operator < (BoxedRef const &a, BoxedRef const &b)
{
  if (a.t < b.t) return true;
  if (b.t < a.t) return false;
  return a.ofs < b.ofs;
}

bool operator == (BoxedRef const &a, BoxedRef const &b)
{
  return (a.t == b.t && a.ofs == b.ofs && a.cell == b.cell);
}


#define defBrGet(T) \
  template<> up_t<T>::t BoxedRef::get<T>(CellVals *ref) const \
  { \
    assertlog(ist<T>() && valid(), "get<" << #T << "> using " << *this); \
    return ref->up_##T(*this); \
  } \
  template<> rd_t<T>::t BoxedRef::get<T>(CellVals const *ref) const \
  { \
    assertlog(ist<T>() && valid(), "get<" << #T << "> using " << *this); \
    return ref->rd_##T(*this); \
  }

defBrGet(F)
defBrGet(CF)
defBrGet(VideoFrame)
defBrGet(DrawOpHandle)
defBrGet(Beh)
defBrGet(vec2)
defBrGet(vec3)
defBrGet(vec4)
defBrGet(mat2)
defBrGet(mat3)
defBrGet(mat4)
defBrGet(mat)
defBrGet(cmat)
defBrGet(ApiHandle)


#undef defBrGet



template<> typename rd_t<mat>::t getZeroRd<mat>(TypeTag t)
{
  static map<TypeTag, F *> dummys;
  auto &bufSlot = dummys[t];
  if (!bufSlot) {
    bufSlot = new F[t.nr * t.nc];
  }
  memset(bufSlot, 0, t.nr * t.nc * sizeof(F));
  return mat(bufSlot, t.nr, t.nc);
}

template<> typename rd_t<cmat>::t getZeroRd<cmat>(TypeTag t)
{
  static map<TypeTag, CF *> dummys;
  auto &bufSlot = dummys[t];
  if (!bufSlot) {
    bufSlot = new CF[t.nr * t.nc];
  }
  memset(bufSlot, 0, t.nr * t.nc * sizeof(CF));
  return cmat(bufSlot, t.nr, t.nc);
}


template<> typename up_t<mat>::t getZeroUp<mat>(TypeTag t)
{
  static map<TypeTag, F *> dummys;
  auto &bufSlot = dummys[t];
  if (!bufSlot) {
    bufSlot = new F[t.nr * t.nc];
  }
  memset(bufSlot, 0, t.nr * t.nc * sizeof(F));
  return mat(bufSlot, t.nr, t.nc);
}

template<> typename up_t<cmat>::t getZeroUp<cmat>(TypeTag t)
{
  static map<TypeTag, CF *> dummys;
  auto &bufSlot = dummys[t];
  if (!bufSlot) {
    bufSlot = new CF[t.nr * t.nc];
  }
  memset(bufSlot, 0, t.nr * t.nc * sizeof(CF));
  return cmat(bufSlot, t.nr, t.nc);
}

BoxedValue::BoxedValue()
  : t(TK_undef),
    p(nullptr)
{

}


BoxedValue::BoxedValue(BoxedPtr ptr)
  : t(ptr.t),
    p(nullptr)
{
  setp(ptr);
}

BoxedValue::~BoxedValue()
{
  freep();
}

/*
  Careful: the different types are mostly handled the same, but not quite.
   - BoxedPtr uses a string_view, while BoxedValue has a string.
   - BoxedPtr uses Assocp, while BoxedValue uses shared_ptr<AssocValue>.
*/
void BoxedValue::setp(BoxedPtr const &ptr)
{
  switch (t.kind) {
    case TK_undef:
    case TK_error:
      p = nullptr;
      break;
    case TK_F:
      p = new F(ptr.get<F>());
      break;
    case TK_CF:
      p = new CF(ptr.get<CF>());
      break;
    case TK_bool:
      p = new F(ptr.get<bool>());
      break;
    case TK_mat:
      p = new F[t.nr * t.nc];
      memcpy(p, ptr.p, t.nr * t.nc * sizeof(F));
      break;
    case TK_cmat:
      p = new CF[t.nr * t.nc];
      memcpy(p, ptr.p, t.nr * t.nc * sizeof(CF));
      break;
    case TK_str:
      p = new string(ptr.get<string_view>());
      break;
    case TK_Assoc:
      p = new shared_ptr<AssocValue>(toShared(ptr.get<Assocp>()));
      break;
    case TK_DrawOp:
    case TK_Check:
    case TK_SymbolSet:
    case TK_Beh:
    case TK_VideoFrame:
    case TK_Api:    
      L() << "BoxedValue::setp: unknown type " << t;
      t = TypeTag(TK_undef);
      p = nullptr;
      break;
    // ADD type kind
  }
}

void BoxedValue::setp(BoxedValue const &val)
{
  switch (t.kind) {
    case TK_undef:
    case TK_error:
      p = nullptr;
      break;
    case TK_F:
      p = new F(val.get<F>());
      break;
    case TK_CF:
      p = new CF(val.get<CF>());
      break;
    case TK_bool:
      p = new F(val.get<bool>());
      break;
    case TK_mat:
      p = new F[t.nr * t.nc];
      memcpy(p, val.p, t.nr * t.nc * sizeof(F));
      break;
    case TK_cmat:
      p = new CF[t.nr * t.nc];
      memcpy(p, val.p, t.nr * t.nc * sizeof(CF));
      break;
    case TK_str:
      p = new string(val.get<string>());
      break;
    case TK_Assoc:
      p = new shared_ptr<AssocValue>(val.get<shared_ptr<AssocValue>>());
      break;
    case TK_DrawOp:
    case TK_Check:
    case TK_SymbolSet:
    case TK_Beh:
    case TK_VideoFrame:
    case TK_Api:
      p = nullptr;
      break;    
    // ADD type kind
  }
}

void BoxedValue::freep()
{
  switch (t.kind) {
    case TK_undef:
    case TK_error:
      break;
    case TK_F:
      delete (F *)p;
      break;
    case TK_CF:
      delete (CF *)p;
      break;
    case TK_bool:
      delete (bool *)p;
      break;
    case TK_mat:
      delete [] (F *)p;
      break;
    case TK_cmat:
      delete [] (CF *)p;
      break;
    case TK_str:
      delete (string *)p;
      break;
    case TK_Assoc:
      delete (shared_ptr<AssocValue> *)p;
      break;
    case TK_DrawOp:
    case TK_Check:
    case TK_SymbolSet:
    case TK_Beh:
    case TK_VideoFrame:
    case TK_Api:
      // Not supported. 
      break;
    // ADD type kind
  }
  p = nullptr;
  t = TypeTag(TK_undef);
}

BoxedValue::BoxedValue(BoxedValue const &other)
  : t(other.t),
    p(nullptr)
{
  setp(other);
}

BoxedValue::BoxedValue(BoxedValue &&other) noexcept
  : t(TK_undef),
    p(nullptr)
{
  swap(t, other.t);
  swap(p, other.p);
}

BoxedValue & BoxedValue::operator = (BoxedValue const &other)
{
  t = other.t;
  freep();
  setp(other);
  return *this;
}


Assocp Arena::mkAssoc(Assocp _next, string_view _key, BoxedPtr _val)
{
  return mk<Assoc>(_next, _key, _val);
}


shared_ptr<AssocValue> mkAssoc(shared_ptr<AssocValue> _next, string_view _key, BoxedPtr _val)
{
  return make_shared<AssocValue>(_next, _key, _val);
}

