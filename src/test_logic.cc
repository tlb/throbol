#include "./test_utils.h"

TEST_CASE("Symbol sets", "[sheet][logic]")
{
  Sheet sh;
  REQUIRE(mkSheetCheck(sh, R"(
foo: |
  'foo
foobar: |
  foo | 'bar
foobarbuz: |
  foobar | 'buz
wug: |
  foobarbuz & 'bar
none: |
  wug & 'foo

)", "symbolsets"));

  auto v0 = CellVals::mk(sh.compiled.get());
  auto v1 = CellVals::mk(sh.compiled.get());

  {
    CpuState cpu(*sh.compiled, &sh.sheetlife, v1.get(), v0.get());
    cpu.dts[0] = 0.1f;
    cpu.execSheet();
    L() << "symbol set alloc: " << cpu.pool->totalAlloc();
    checkCpu(cpu);
    L() << "Values " << *v1;
    CHECK(repr(*v1) == "{{foo: 'foo, foobar: 'foo|'bar, foobarbuz: 'foo|'bar|'buz, wug: 'bar, none: nil}}");
  }

}


TEST_CASE("If/else", "[sheet][logic]")
{
  Sheet sh;
  REQUIRE(mkSheetCheck(sh, R"(

foo: time
bar: if (foo > 1) { 2 }
buz: if (foo > 1) { 2 } else { 3 }

baror: bar | 99
buzor: buz | 99

barand: bar & 88
buzand: buz & 88

)", "ifelse"));

  auto v1 = sampleSheetData(sh, 0.0);
  CHECK(repr(*v1) == "{{foo: 0, bar: nan, buz: 3, baror: 99, buzor: 3, barand: nan, buzand: 88}}");

  auto v2 = sampleSheetData(sh, 1.5);
  CHECK(repr(*v2) == "{{foo: 1.5, bar: 2, buz: 2, baror: 2, buzor: 2, barand: 88, buzand: 88}}");


}
