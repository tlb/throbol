#include "common/std_headers.h"
#include "catch2/catch_session.hpp"
#include "./test_utils.h"
#include "./core.h"
#include "./config.h"
#include <sqlite3.h>
#include <sqlite_modern_cpp.h>
#include <sqlite_modern_cpp/log.h>

ofstream testlog("testlog.md");

int main(int argc, char **argv)
{
  sqlite::error_log(
    [&](sqlite::sqlite_exception& e) {
        cerr  << e.get_code() << ": " << e.what() << "\n";
    }
  );

  setupPaths("");

  if (CpuState::execVerbose) {
    CpuState::logFile = make_shared<ofstream>("logs/cpu.log");
  }

  Catch::Session session;

  simulateStartup();

  using namespace Catch::Clara;
  auto cli = session.cli()
    | Opt(overridePerfIterCount, "overridePerfIterCount")["--perfIterCount"]("iterations for perf counting");
        
  session.cli(cli); 
  
  int rc = session.applyCommandLine(argc, argv);
  if (rc != 0) return rc;

  int exitCode = session.run();
  simulateShutdown();

  if (0) AllocTracker::printStats();

  return exitCode;
}
