#include "./emit.h"
#include "./emit_macros.h"

deftype(add)
{
  auto at = eval(n->ch(0));
  auto bt = eval(n->ch(1));
  if (at == bt) return at;
  if (at.ist<CF>() && bt.convist<F>()) return at;
  if (at.convist<F>() && bt.ist<CF>()) return bt;
  return setError();
}

defemit(add)
{
  defop2g_simd(F, F, F, r = a + b, (ag += rg, bg += rg));
  defop2g_simd(F, CF, CF, r = a + b, (ag += rg.real(), bg += rg));
  defop2g_simd(CF, F, CF, r = a + b, (ag += rg, bg += rg.real()));
  defop2g_simd(CF, CF, CF, r = a + b, (ag += rg, bg += rg));
  
  defop2g(vec2, vec2, vec2, r = a + b, (ag += rg, bg += rg));
  defop2g(vec3, vec3, vec3, r = a + b, (ag += rg, bg += rg));
  defop2g(vec4, vec4, vec4, r = a + b, (ag += rg, bg += rg));
  defop2g(mat2, mat2, mat2, r = a + b, (ag += rg, bg += rg));
  defop2g(mat3, mat3, mat3, r = a + b, (ag += rg, bg += rg));
  defop2g(mat4, mat4, mat4, r = a + b, (ag += rg, bg += rg));

  defop2rg(mat, mat, mat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_mat, args[0].t.nr, args[0].t.nc}),
    r = a + b,
    (ag += rg, bg += rg));
  defop2rg(cmat, cmat, cmat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_cmat, args[0].t.nr, args[0].t.nc}),
    r = a + b,
    (ag += rg, bg += rg));
  defop2rg(mat, cmat, cmat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_cmat, args[0].t.nr, args[0].t.nc}),
    r = a + b,
    (ag += rg.real(), bg += rg));
  defop2rg(cmat, mat, cmat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_cmat, args[0].t.nr, args[0].t.nc}),
    r = a + b,
    (ag += rg, bg += rg.real()));

  defop2(string_view, string_view, string_view, r = pad.pool->strconcatview(a, b));

  return setInvalidArgs();
}

defusage(add, R"(
  T + T)");

deftest(add, R"(
t.f: |
  2+2 ~~~ 4
t.cf: |
  (2+3im) + (2+5im) ~~~ 4+8im
t.f.cf: |
  2 + (2+5im) ~~~ 4+5im
t.1: |
  [2] + [4] ~~~ [6]
t.2: |
  [2,3] + [4,5] ~~~ [6,8]
t.3: |
  [2,3,4] + [4,5,6] ~~~ [6,8,10]
t.4: |
  [2,3,4,5] + [4,5,6,7] ~~~ [6,8,10,12]
t.5: |
  [2,3,4,5,6] + [4,5,6,7,8] ~~~ [6,8,10,12,14]
t.mat2: |
  [[1,2],[3,4]] + [[5,6],[7,8]] ~~~ [[6, 8], [10, 12]]
t.str: |
  "foo" + "bar" ~~~ "foobar"
)");


deftype(div)
{
  auto at = eval(n->ch(0));
  auto bt = eval(n->ch(1));
  if (at.convist<F>() && bt.convist<F>()) return typetag<F>();
  if (at.ist<mat>() && bt.convist<F>()) return at;
  if (at.ist<cmat>() && bt.convist<F>()) return at;
  return setError();
}

defemit(div)
{
  defop2g_simd(F, F, F, r = a / b, (ag += rg / b, bg -= rg * a / (b*b)));
  defop2_simd(F, CF, CF, r = a / b);
  defop2_simd(CF, F, CF, r = a / b);
  defop2_simd(CF, CF, CF, r = a / b);

  defop2(vec2, F, vec2, r = a / b);
  defop2(vec3, F, vec3, r = a / b);
  defop2(vec4, F, vec4, r = a / b);
  defop2(mat2, F, mat2, r = a / b);
  defop2(mat3, F, mat3, r = a / b);
  defop2(mat4, F, mat4, r = a / b);

  defop2r(mat, F, mat, 
    true,
    allocResult(args[0].t),
    r = a * (1.0f / b));
  
  //defop2(cmat, F, cmat, r = a / b);
  //defop2(cmat, CF, cmat, r = a / b);
  return setInvalidArgs();
}

defusage(div, R"(
  F|CF|vec|mat / F
  CF / CF)");


deftype(compdiv)
{
  auto at = eval(n->ch(0));
  auto bt = eval(n->ch(1));
  if (at == bt) return at;
  return setError();
}

defemit(compdiv)
{
  defop2g_simd(F, F, F, r = a / b, (ag += rg / b, bg -= rg * a / (b*b)));
  defop2_simd(F, CF, CF, r = a / b);
  defop2_simd(CF, F, CF, r = a / b);
  defop2_simd(CF, CF, CF, r = a / b);

  defop2(vec2, vec2, vec2, r = a.array() / b.array());
  defop2(vec3, vec3, vec3, r = a.array() / b.array());
  defop2(vec4, vec4, vec4, r = a.array() / b.array());
  defop2(mat2, mat2, mat2, r = a.array() / b.array());
  defop2(mat3, mat3, mat3, r = a.array() / b.array());
  defop2(mat4, mat4, mat4, r = a.array() / b.array());

  defop2rg(mat, mat, mat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_mat, args[0].t.nr, args[0].t.nc}),
    r = a.array() / b.array(),
    (ag.array() += rg.array() / b.array(), bg.array() -= rg.array() * a.array() / (b.array()*b.array())));
  defop2rg(cmat, cmat, cmat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_cmat, args[0].t.nr, args[0].t.nc}),
    r = a.array() / b.array(),
    (ag.array() += rg.array() / b.array(), bg.array() -= rg.array() * a.array() / (b.array()*b.array())));
  defop2rg(mat, cmat, cmat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_cmat, args[0].t.nr, args[0].t.nc}),
    r = a.array() / b.array(),
    (ag.array() += (rg.array() / b.array()).real(), bg.array() -= rg.array() * a.array() / (b.array()*b.array())));
  defop2rg(cmat, mat, cmat,
    args[0].t.nc == args[1].t.nc && args[0].t.nr == args[1].t.nr,
    allocResult({TK_cmat, args[0].t.nr, args[0].t.nc}),
    r = a.array() / b.array(),
    (ag.array() += rg.array() / b.array(), bg.array() -= (rg.array() * a.array() / (b.array()*b.array())).real()));

  return setInvalidArgs();
}

defusage(compdiv, R"(
  T ./ T)");


deftype(neg)
{
  return eval(n->ch(0));
}

defemit(neg)
{
  defop1g_simd(F, F, r = - a, (ag -= rg));
  defop1g_simd(CF, CF, r = - a, (ag -= rg));

  defop1g(vec2, vec2, r = - a, (ag -= rg));
  defop1g(vec3, vec3, r = - a, (ag -= rg));
  defop1g(vec4, vec4, r = - a, (ag -= rg));
  defop1g(mat2, mat2, r = - a, (ag -= rg));
  defop1g(mat3, mat3, r = - a, (ag -= rg));
  defop1g(mat4, mat4, r = - a, (ag -= rg));

  defop1rg(mat, mat,
    true,
    allocResult({TK_mat, args[0].t.nr, args[0].t.nc}),
    r = - a,
    (ag -= rg));
  defop1rg(cmat, cmat,
    true,
    allocResult({TK_mat, args[0].t.nr, args[0].t.nc}),
    r = - a,
    (ag -= rg));

  return setInvalidArgs();
}

defusage(neg, R"(
  - F|CF|vec|mat|cvec|cmat)");

