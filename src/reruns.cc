#include "./core.h"
#include "./reruns.h"
#include "./emit.h"
#include <sqlite3.h>
#include <sqlite_modern_cpp.h>
using namespace sqlite;

static atomic<int> rerunsPending = 0;

RerunSet::RerunSet(Sheet &_sh)
  :sh(_sh)
{
  pureRerun = make_shared<RerunResult>("");
  reruns.push_back(pureRerun);
  selectedRerun = pureRerun;
}

static const int verbose = 0;

char const * RerunSet::traceObsolete(Trace &trace)
{
  if (trace.compiled != sh.compiled) return "compiled mismatch";
  if (trace.params.hash() != sh.params.hash()) return "params mismatch";
#if 0
  int expectMaxIndex = sh.compiled->initialTickCount + 1;
  if (trace.replayBuffer) {
    for (auto const &api : sh.compiled->apis) {
      auto &blobs = trace.replayBuffer->replayByApiInstance[api->apiInstance];
      // WRITEME: check replayBuffer->failed
      if (!blobs.empty()) {
        expectMaxIndex = min(expectMaxIndex, int(blobs.size()));
      }
    }
  }
  if (trace.maxIndex < expectMaxIndex) return "too short";
  if (trace.maxIndex > expectMaxIndex) return "too long";
  if (trace.dt != sh.compiled->initialDt) return "wrong dt";
#endif
  return nullptr;
}

void RerunSet::update()
{
  if (selectedRerun) selectedRerun->enabled = true;

  for (auto rerunit = reruns.rbegin(); rerunit != reruns.rend(); rerunit++) {
    auto &rerun = *rerunit;

    if (rerunsPending > 50) break;
    if (!rerun || !rerun->enabled || rerun->failed) continue;
    if (rerun->running > 0) continue;

    rerun->traceBySeed_.resize(numSeeds);
    for (int seed = 0; seed < numSeeds; seed++) {
      auto trace = rerun->traceBySeed_[seed].second;
      char const *reason = nullptr;
      if (!trace) {
        reason = "nonexistent";
      }
      else {
        reason = traceObsolete(*trace);
      }

      if (reason) {
        rerun->running ++;
        rerunsPending ++;

        if (rerun->traceName.empty()) {
          rerun->replayBuffer = nullptr; // pure
        }
        else {
          if (!rerun->replayBuffer) {
            load(*rerun);
          }
        }
        if (rerun->failed) {
          rerun->running--;
          rerunsPending--;
          break;
        }
        if (verbose >= 1) {
          L() << "Start " << rerun->traceName
              << " " << seed
              << " " << reason;
        }
        auto newTrace = make_shared<Trace>(sh.compiled, false, true);
        newTrace->setupInitial();
        newTrace->randomSeed = seed;
        newTrace->replayBuffer = rerun->replayBuffer;
        simulateTraceAsync(std::move(newTrace), [rerun, seed](shared_ptr<Trace> newTrace2) {
          rerunsPending --;
          assert(newTrace2);
          rerun->traceBySeed_[seed] = pair(0.0, newTrace2);
          rerun->running --;
          uiPollLive = true;
          if (verbose) L() << "Done " << rerun->traceName << " " << seed << " size=" << newTrace2->nVals();
        }, 0);
      }
    }
  }
}

/*
  Return a trace suitable for displaying. To avoid blinking the screen every time
  we update anything, we notice if a trace has become obsolete but continue returning
  it for up to half a second.
*/
shared_ptr<Trace> RerunSet::displayTrace()
{
  if (selectedRerun && selectedSeed < int(selectedRerun->traceBySeed_.size())) {
    auto &[obsoleteSince, trace] = selectedRerun->traceBySeed_[selectedSeed];
    if (trace) {
      if (obsoleteSince == 0.0) {
        if (!trace || traceObsolete(*trace)) {
          obsoleteSince = realtime();
        }
      }
      else {
        uiPollLive = true;
        if (realtime() - obsoleteSince > 0.5) return nullptr;
      }
      if (verbose) L() << "RerunSet::displayTrace " << selectedRerun->traceName << " size=" << trace->nVals();
      return trace;
    }
  }
  return nullptr;
}

void RerunSet::insertFromDb()
{
  if (0) L() << "insertFromDb: 1. compiled=" << !!sh.compiled << 
    " hasLiveApi= " << (sh.compiled && sh.compiled->hasLiveApi) <<
    " needInsertFromDb=" << needInsertFromDb;

  if (!sh.compiled || !sh.compiled->hasLiveApi) return;
  if (!needInsertFromDb) return;

  needInsertFromDb = false;

  auto &db = sh.getTraceDb();

  set<string> done;
  for (auto &rerun : reruns) {
    if (rerun) {
      done.insert(rerun->traceName);
    }
  }

  db << R"(
    SELECT traceName, createTime, lastOpenTime, flags
      FROM replayInfo
      ORDER BY traceName;)"
    >> [this, &done](string const &traceName, R createTime, R lastOpenTime, string const &flags) {
      if (!done.count(traceName)) {
        auto newRerun = make_shared<RerunResult>(traceName);
        if (!reruns.empty() && reruns.back() == pureRerun) {
          // Leave the pure sheet at the end
          reruns.insert(reruns.end()-1, newRerun);
        }
        else {
          reruns.push_back(newRerun);
        }
        done.insert(traceName);
        selectedRerun = newRerun;
      }
    };
  // Enable some by default. Note that they're displayed in the UI in reverse order
#if defined(__EMSCRIPTEN__)
  int enableBudget = 1;
#else
  int enableBudget = 10;
#endif
  for (auto it = reruns.rbegin(); it != reruns.rend(); it++) {
    if (*it && enableBudget > 0 && !(*it)->failed) {
      (*it)->enabled = true;
      enableBudget--;
      if (enableBudget == 0) break;
    }
  }

}

/*
  Must not invalidate iterators into .reruns
*/
void RerunSet::remove(shared_ptr<RerunResult> rerun)
{
  if (!sh.compiled || !sh.compiled->hasLiveApi) return;
  auto &db = sh.getTraceDb();

  db << R"(
    DELETE FROM replayInfo
      WHERE traceName = ?;)"
    << rerun->traceName;

  if (selectedRerun == rerun) selectedRerun = nullptr;
  if (pureRerun == rerun) pureRerun = nullptr;
  for (auto &it : reruns) {
    if (it == rerun) it = nullptr;
  }
}

void RerunSet::select(shared_ptr<RerunResult> rerun, int seed)
{
#if 0
  if (selectedRerun == rerun && selectedSeed == seed) {
    showAllSeeds = true;
  }
  else {
    showAllSeeds = false;
  }
#endif
  selectedRerun = rerun;
  selectedSeed = seed;
}

void RerunSet::selectPrevSeed()
{
  selectedSeed = (selectedSeed + numSeeds - 1) % numSeeds;
}

void RerunSet::selectNextSeed()
{
  selectedSeed = (selectedSeed + 1) % numSeeds;
}

void RerunSet::toggleShowAllSeeds()
{
  showAllSeeds = !showAllSeeds;
}

void RerunSet::selectPureSheet()
{
  selectedRerun = pureRerun;
}


void RerunSet::add(shared_ptr<ReplayBuffer> replay, vector<BlobRef> const &chunkList)
{
  auto traceName = getNewTraceName(replay->startTime);
  save(traceName, *replay, chunkList);
  needInsertFromDb = true;
}

shared_ptr<RerunResult> RerunSet::byName(string const &name)
{
  for (auto &it : reruns) {
    if (it && it->traceName == name) return it;
  }
  return nullptr;
}


string getNewTraceName(R startTime, U64 randomSeed)
{
  time_t now(startTime);
  struct tm t{};
  localtime_r(&now, &t);
  auto ret = getTimeTok(&t);
  if (randomSeed != 0) {
    ret += to_string(randomSeed);
  }
  return ret;
}


void Sheet::openTraceDb(string const &fn)
{
  traceDb = make_shared<database>(fn);
  auto &db = *traceDb;


  int application_id = 0, user_version = 0;
  db << "PRAGMA application_id;" >> std::tie(application_id);
  db << "PRAGMA user_version;" >> std::tie(user_version);

  if (0) L() << "openTraceDb: " << fn << " application_id=" << application_id << " user_version=" << user_version;

  if (application_id == 0 && user_version == 0) {
    L() << fn << ": initializing database";
    db << "PRAGMA page_size = 16384;";

    db << "BEGIN;";
    db << "PRAGMA application_id = 0x544c4274;"; // TLBt
    db << "PRAGMA user_version = 2;";

    db << R"(
      CREATE TABLE replayInfo (
        traceName TEXT,
        createTime REAL,
        randomSeed INT,
        lastOpenTime REAL,
        flags TEXT,
        PRIMARY KEY(traceName)
      );)";
    
    db << R"(
      CREATE TABLE replaySchema (
        traceName TEXT,
        apiName TEXT,
        apiVersion TEXT,
        apiSchema TEXT,
        PRIMARY KEY(traceName, apiName)
      );)";

    db << R"(
      CREATE TABLE replayData (
        traceName TEXT,
        apiName TEXT,
        apiVersion TEXT,
        timeIndex INT,
        rawData BLOB,
        PRIMARY KEY(traceName, apiName, timeIndex)
      );)";

    db << R"(
      CREATE TABLE replayBlobs (
        traceName TEXT,
        blobName TEXT
      );)";

    db << "COMMIT;";
  }
  else if (application_id == 0x544c4274) { // TLBt
    if (user_version == 2) {
      if (0) L() << fn << ": loaded version 2";
      // OK
    }
    else if (user_version == 1) {
      db << "BEGIN;";
      db << R"(
        CREATE TABLE replayBlobs (
          traceName TEXT,
          blobName TEXT
        );)";
      db << "PRAGMA user_version = 2;";
      db << "COMMIT;";
      L() << fn << ": upgraded version 1->2";
    }
    else {
      L() << fn << ": unrecognized file version " << user_version;
      traceDb = nullptr;
      return;
    }
  }
  else {
    L() << fn << ": unrecognized file format " << repr_08x(application_id);
    return;
  }
  db << "PRAGMA synchronous = 0;"; // Don't call fsync after every commit. Safe as long as no power failure or kernel crash.
}


void RerunSet::load(RerunResult &rerun)
{
  auto &db = sh.getTraceDb();

  auto replayBuffer = make_shared<ReplayBuffer>();
  std::unique_lock lock(replayBuffer->replayMutex);

  db << R"(
    SELECT createTime
      FROM replayInfo
      WHERE traceName = ?;)"
    << rerun.traceName
    >>[&replayBuffer](R createTime) {
      replayBuffer->startTime = createTime;
    };

  for (auto const &api : sh.compiled->apis) {

    string dbSchemaStr;
    string dbVersion;
    db << R"(
      SELECT apiVersion, apiSchema
      FROM replaySchema
      WHERE traceName = ? AND apiName = ?;)"
      << rerun.traceName << api->apiInstance
      >> [&dbVersion, &dbSchemaStr](string const &apiVersion, string const &apiSchema) {
        dbVersion = apiVersion;
        dbSchemaStr = apiSchema;
      };
    YAML::Node dbSchema = YAML::Load(dbSchemaStr);


    /*
      The next two tests throw away reruns that don't match the current schema. These probably could
      be salvaged with a little effort. 
    */

    ApiSchemaConv conv(*api, dbSchema, dbVersion);
    if (!conv.ok) {
      L() << "Conversion failed for " << rerun.traceName <<
        ". Expected " << api->apiVersion <<
        " found " << dbVersion << "\n" << conv.notes.str();
      L() << conv.notes.str();
      rerun.failed = true;
      return;
    }

    rerun.convNotes += conv.notes.str();
    if (verbose || api->verbose >= 2) {
      L() << conv.notes.str();
    }

    auto &blobs = replayBuffer->replayByApiInstance[api->apiInstance];

    db << R"(
      SELECT timeIndex, rawData
        FROM replayData
        WHERE traceName = ? AND apiName = ? AND apiVersion = ?
        ORDER BY timeIndex;)"
      << rerun.traceName << api->apiInstance << api->apiVersion
      >> [&blobs, &conv](int timeIndex, vector<U8> const &buf) {
        assert(timeIndex >= 0 && size_t(timeIndex) == blobs.size());
        assert(buf.size() != 0);

        blobs.push_back(conv.run(buf.data(), buf.size()));
      };

    if (verbose) L() << "Loaded " << rerun.traceName;
  }
  rerun.replayBuffer = replayBuffer;
}

void RerunSet::save(string const &traceName, ReplayBuffer &replayBuffer, vector<BlobRef> const &chunkList)
{
  auto &db = sh.getTraceDb();

  std::shared_lock lock(replayBuffer.replayMutex);

  db << "BEGIN;";

  db << R"(
    INSERT INTO replayInfo
    (traceName, createTime, randomSeed, lastOpenTime, flags)
    VALUES (?, ?, 0, 0, '');)"
    << traceName
    << replayBuffer.startTime;

  for (auto const &api : sh.compiled->apis) {
    auto &blobs = replayBuffer.replayByApiInstance[api->apiInstance];
    if (blobs.empty()) continue;

    auto ps = db << R"(
      INSERT INTO replayData
        (traceName, apiName, apiVersion, timeIndex, rawData)
        VALUES (?, ?, ?, ?, ?);)";
    for (int timeIndex = 0; timeIndex < int(blobs.size()); timeIndex++) {
      assert(blobs[timeIndex].size() != 0);
      vector<U8> rbuf((U8 *)blobs[timeIndex].data(), (U8 *)blobs[timeIndex].data() + blobs[timeIndex].size());
      ps << traceName << api->apiInstance << api->apiVersion << timeIndex << rbuf;
      ps++;
    }

    db << R"(
      INSERT INTO replaySchema
        (traceName, apiName, apiVersion, apiSchema)
        VALUES (?, ?, ?, ?);)"
      << traceName << api->apiInstance << api->apiVersion << repr(api->getSchema());
  }

  {
    auto ps = db << R"(
      INSERT INTO replayBlobs
        (traceName, blobName)
        VALUES (?, ?);)";
    for (auto &chunk : chunkList) {
      string blobName = chunk.getFilename();
      ps << traceName << blobName;
      ps++;
    }
  }


  db << "COMMIT;";
}




ReplayBuffer::ReplayBuffer()
  : startTime(0.0)
{
}

ReplayBuffer::~ReplayBuffer()
{
}
