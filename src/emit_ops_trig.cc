#include "./emit.h"
#include "./emit_macros.h"

deftype(sin)
{
  return eval(n->ch(0));
}

defemit(sin)
{
  defop1g_simd(F, F, r = sin(a), ag += rg * cos(a));
  defop1g_simd(CF, CF, r = sin(a), ag += rg * cos(a));
  defop1r(mat, mat, true, allocResult(args[0].t), r = a.array().sin());
  return setInvalidArgs();
}

defusage(sin, R"(
  sin(angle ∊ F|CF)
  sin(angle ∊ vec|mat) -- elementwise)");

deftest(sin, R"(
y: sin(0.5pi) ~~~ 1
z: sin([0.5pi, 0, -0.5pi, pi]) ~~~ [1, 0, -1, 0]
)");



deftype(cos)
{
  return eval(n->ch(0));
}

defemit(cos)
{
  defop1g_simd(F, F, r = cos(a), ag += rg * sin(a));
  defop1g_simd(CF, CF, r = cos(a), ag += rg * sin(a));
  defop1r(mat, mat, true, allocResult(args[0].t), r = a.array().cos());
  return setInvalidArgs();
}

defusage(cos, R"(
  cos(angle ∊ F|CF)
  cos(angle ∊ vec|mat) -- elementwise)");

deftest(cos, R"(
y: cos(pi) ~~~ -1
)");


deftype(tan)
{
  return eval(n->ch(0));
}

defemit(tan)
{
  defop1g_simd(F, F, r = tan(a), ag += rg / a.cos().abs2());
  defop1r(mat, mat, true, allocResult(args[0].t), r = a.array().tan());
  return setInvalidArgs();
}

defusage(tan, R"(
  tan(angle ∊ F)
  tan(angle ∊ vec|mat) -- elementwise)");

deftest(tan, R"(
y: tan(0.25pi) ~~~ 1
)");


deftype(sinh)
{
  return eval(n->ch(0));
}

defemit(sinh)
{
  defop1g_simd(F, F, r = sinh(a), ag += rg * cosh(a));
  defop1g_simd(CF, CF, r = sinh(a), ag += rg * cosh(a));
  defop1r(mat, mat, true, allocResult(args[0].t), r = a.array().sinh());
  return setInvalidArgs();
}

defusage(sinh, R"(
  sinh(x ∊ F|CF)
  sinh(x ∊ vec|mat) -- elementwise)");


deftest(sinh, R"(
y: sinh(0) ~~~ 0
z: sinh([0,1,2]) ~~~ [0, 1.1752012, 3.62686041]
)");


deftype(cosh)
{
  return eval(n->ch(0));
}

defemit(cosh)
{
  defop1g_simd(F, F, r = cosh(a), ag += rg * sinh(a));
  defop1g_simd(CF, CF, r = cosh(a), ag += rg * sinh(a));
  defop1r(mat, mat, true, allocResult(args[0].t), r = a.array().cosh());
  return setInvalidArgs();
}

defusage(cosh, R"(
  cosh(x ∊ F|CF)
  cosh(x ∊ vec|mat) -- elementwise)");

deftest(cosh, R"(
y: cosh(0) ~~~ 1
z: cosh([0,1,2]) ~~~ [1, 1.54308, 3.762195]
)");


deftype(tanh)
{
  return eval(n->ch(0));
}

defemit(tanh)
{
  defop1g_simd(F, F, r = tanh(a), ag += rg * (1.0f - r.abs2()));
  defop1r(mat, mat, true, allocResult(args[0].t), r = a.array().tanh());
  return setInvalidArgs();
}

defusage(tanh, R"(
  tanh(x ∊ F)
  tanh(x ∊ vec|mat) -- elementwise)");

deftest(tanh, R"(
y: tanh(0) ~~~ 0
z: tanh([0,1,2]) ~~~ [0, 0.761594, 0.964027]
)");


deftype(normangle)
{
  return eval(n->ch(0));
}

defemit(normangle)
{
  defop1g(F, F, r = normangle(a), ag += rg);
  return setInvalidArgs();
}

defusage(normangle, R"(
  normangle(angle ∊ F))");


deftype(normangle2)
{
  return eval(n->ch(0));
}

defemit(normangle2)
{
  defop1g(F, F, r = normangle2(a), ag += rg);
  return setInvalidArgs();
}

defusage(normangle2, R"(
  normangle2(angle ∊ F))");


deftype(atan2)
{
  return typetag<F>();
}

defemit(atan2)
{
  defop2(F, F, F, r = atan2(a, b));
  return setInvalidArgs();
}

defusage(atan2, R"(
  atan2(y ∊ F, x ∊ F))");

deftest(atan2, R"(
x: atan2(1, 1) ~~~ 0.25pi
)");

deftype(fourierbasis)
{
  return n->data<TypeTag>();
}

void setFourierBasis(cvec &ret, float theta)
{
  ret[0] = CF(1.0f, 0.0f);
  for (int i = 1; i < ret.rows(); i += 2) {
    CF arg(0.0, theta * ((i+1) / 2));
    ret[i] = exp(arg);
    if (i + 1 < ret.rows()) ret[i+1] = exp(-arg);
  }
  if (0) L() << ret;
}

defemit(fourierbasis)
{
  auto wantType = n->data<TypeTag>();
  if (argmatch1(F)) {
    if (wantType.kind == TK_cmat) {
      auto rv = allocResult(wantType);
      emit_evalop1(F, cvec, setFourierBasis(r, a));
      return rv;
    }
  }

  return setInvalidArgs();
}

defusage(fourierbasis, R"(
  cvecN.fourierbasis(theta ∊ F))");

deftest(fourierbasis, R"(
x: cvec4.fourierbasis(0.25pi) ~~~ [1+0im, 0.707+0.707im, 0.707+-0.707im, 0+1im]
)");


deftype(chebbasis)
{
  return n->data<TypeTag>();
}

void setChebBasis(vec &ret, float x)
{
  // https://en.wikipedia.org/wiki/Chebyshev_polynomials
  auto nr = ret.rows();
  if (nr >= 1) ret[0] = 1.0f;
  if (nr >= 2) ret[1] = x;
  for (Index i = 2; i < nr; i++) {
    ret[i] = 2*x*ret[i-1] - ret[i-2];
  }
}

defemit(chebbasis)
{
  auto wantType = n->data<TypeTag>();
  if (argmatch1(F)) {
    if (wantType.kind == TK_mat) {
      auto rv = allocResult(wantType);
      emit_evalop1(F, vec, setChebBasis(r, a));
      return rv;
    }
  }

  return setInvalidArgs();
}

defusage(chebbasis, R"(
  vecN.chebbasis(x ∊ F))");
