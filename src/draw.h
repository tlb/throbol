#pragma once
#include "./defs.h"
#include "./policy.h"

struct DrawOp;
struct G8DrawList;
struct PolicyTerrainSpec;
struct ParticleSearchState;
struct MinpackSearchSpec;
struct MinpackSearchState;
struct ParticleSearchState;

struct DrawInfo {

  DrawStyle style{Draw_std};
  bool isPosOnly = false;
  bool scaleSet = false;
  F scale = 1.0;
  F autoRangeMax = 0.0;
  vector<U32> autoRangeSymbols;
  U64 autoRangeSignature = 0;

};

enum DrawOpType {
  DrawOp_null,
  DrawOp_union,
  DrawOp_transform,
  DrawOp_color,
  DrawOp_sweep,
  DrawOp_line,
  DrawOp_lines,
  DrawOp_spline,
  DrawOp_arrow,
  DrawOp_arcArrow,
  DrawOp_disk,
  DrawOp_rect,
  DrawOp_tube,
  DrawOp_text,
  DrawOp_solid,
  DrawOp_sphere,
  DrawOp_axisArrows,
  DrawOp_dial,
  DrawOp_label,
  DrawOp_policyTerrain,
  DrawOp_particleSearch,
  DrawOp_minpackSearch,
  DrawOp_linePlot,
  DrawOp_bandPlot,
  DrawOp_scatterPlot,
  DrawOp_plotColor,
  DrawOp_epicycles,
  DrawOp_mujocoScene
  // ADD DrawOp
};



/*
  DrawOp: representation of a drawing op. DrawOpHandle is a handle to it.
  Cells can return a DrawOpHandle pointing to a tree of DrawOp instances.
  These are pool-allocated (in a ChunkyAlloc) so careful about any non-POD.
*/
struct alignas(MAX_ALIGN) DrawOp {

  DrawOpType op;

  DrawOp(DrawOpType _op) : op(_op) {}

  template<typename T> bool isa() const { return op == T::tid; }
  template<typename T> T *as() {
    if (op == T::tid) return (T *)this;
    return nullptr;
  }
  template<typename T> T *uncheckedAs() {
    return (T *)this;
  }

};

struct DrawOpNull : DrawOp {

  static constexpr DrawOpType tid = DrawOp_null;

  DrawOpNull() : DrawOp(tid) {}

};

struct DrawOpUnion : DrawOp {

  static constexpr DrawOpType tid = DrawOp_union;

  DrawOpHandle lhs;
  DrawOpHandle rhs;

  DrawOpUnion(DrawOpHandle _lhs, DrawOpHandle _rhs) : DrawOp(tid), lhs(_lhs), rhs(_rhs) {}

};

struct DrawOpTransform : DrawOp {

  static constexpr DrawOpType tid = DrawOp_transform;

  mat4 transform;
  DrawOpHandle contents;

  DrawOpTransform(mat4 const &_transform, DrawOpHandle _contents) : DrawOp(tid), transform(_transform), contents(_contents) {}

};

struct DrawOpColor : DrawOp {

  static constexpr DrawOpType tid = DrawOp_color;

  mat4 transform;
  DrawOpHandle contents;

  DrawOpColor(mat4 const &_transform, DrawOpHandle _contents) : DrawOp(tid), transform(_transform), contents(_contents) {}

};

struct DrawOpSweep : DrawOp {

  static constexpr DrawOpType tid = DrawOp_sweep;

  F visRange;
  DrawOpHandle contents;

  DrawOpSweep(F _visRange, DrawOpHandle _contents) : DrawOp(tid), visRange(_visRange), contents(_contents) {}

};

struct DrawOpLine : DrawOp {

  static constexpr DrawOpType tid = DrawOp_line;

  vec3 p0, p1;

  DrawOpLine(vec3 const &_p0, vec3 const &_p1) : DrawOp(tid), p0(_p0), p1(_p1) {}
  DrawOpLine(DrawOpLine const &other) = default;

};

struct DrawOpLines : DrawOp {

  static constexpr DrawOpType tid = DrawOp_lines;

  mat pts;

  DrawOpLines(mat const &_pts) : DrawOp(tid), pts(_pts) {}
  DrawOpLines(DrawOpLines const &other) = default;

};

struct DrawOpSpline : DrawOp {

  static constexpr DrawOpType tid = DrawOp_spline;

  mat pts;

  DrawOpSpline(mat const &_pts) : DrawOp(tid), pts(_pts) {}
  DrawOpSpline(DrawOpSpline const &other) = default;

};


struct DrawOpArrow : DrawOp {

  static constexpr DrawOpType tid = DrawOp_arrow;

  vec3 origin;
  vec3 dir;

  DrawOpArrow(vec3 const &_origin, vec3 const &_dir) : DrawOp(tid), origin(_origin), dir(_dir) {}
  DrawOpArrow(DrawOpArrow const &other) = default;

};

struct DrawOpArcArrow : DrawOp {

  static constexpr DrawOpType tid = DrawOp_arcArrow;

  vec3 center;
  vec3 xDir;
  vec3 yDir;
  vec3 upDir;
  F radius;
  F arcFrom;
  F arcTo;

  DrawOpArcArrow(vec3 const &_center, vec3 const &_xDir, vec3 const &_yDir, vec3 const &_upDir, F _radius, F _arcFrom, F _arcTo)
    : DrawOp(tid), 
    center(_center), xDir(_xDir), yDir(_yDir), upDir(_upDir),
    radius(_radius), arcFrom(_arcFrom), arcTo(_arcTo)
  {    
  }
  DrawOpArcArrow(DrawOpArcArrow const &other) = default;

};

struct DrawOpSphere : DrawOp {

  static constexpr DrawOpType tid = DrawOp_sphere;

  vec3 center;
  F rad;

  DrawOpSphere(vec3 const &_center, F _rad) : DrawOp(tid), center(_center), rad(_rad) {}
  DrawOpSphere(DrawOpSphere const &other) = default;

};

struct DrawOpDisk : DrawOp {

  static constexpr DrawOpType tid = DrawOp_disk;

  vec3 center;
  F radius;

  DrawOpDisk(vec3 const &_center, F _radius) : DrawOp(tid), center(_center), radius(_radius) {}
  DrawOpDisk(DrawOpDisk const &other) = default;

};

struct DrawOpTube : DrawOp {

  static constexpr DrawOpType tid = DrawOp_tube;

  mat pts;
  F radius;

  DrawOpTube(mat _pts, F _radius) : DrawOp(tid), pts(_pts), radius(_radius) {}
  DrawOpTube(DrawOpTube const &other) = default;

};

struct DrawOpRect : DrawOp {

  static constexpr DrawOpType tid = DrawOp_rect;

  vec3 center;
  vec3 xdir;
  vec3 ydir;

  DrawOpRect(vec3 const &_center, vec3 const &_xdir, vec3 const &_ydir) : DrawOp(tid), center(_center), xdir(_xdir), ydir(_ydir) {}
  DrawOpRect(DrawOpRect const &other) = default;

};

struct DrawOpText : DrawOp {

  static constexpr DrawOpType tid = DrawOp_text;

  vec3 p0;
  F size;
  string_view text;

  DrawOpText(vec3 const &_p0, F _size, string_view _text) : DrawOp(tid), p0(_p0), size(_size), text(_text) {}
  DrawOpText(DrawOpText const &other) = default;

};

struct DrawOpSolid : DrawOp {

  static constexpr DrawOpType tid = DrawOp_solid;

  string_view fn;
  string_view pageFileDir;

  DrawOpSolid(string_view _fn, string_view _pageFileDir) : DrawOp(tid), fn(_fn), pageFileDir(_pageFileDir) {}
  DrawOpSolid(DrawOpSolid const &other) = default;

};

struct DrawOpAxisArrows : DrawOp {

  static constexpr DrawOpType tid = DrawOp_axisArrows;

  DrawOpAxisArrows() : DrawOp(tid) {}
  DrawOpAxisArrows(DrawOpAxisArrows const &other) = default;

};

struct DrawOpDial : DrawOp {

  static constexpr DrawOpType tid = DrawOp_dial;

  vec3 center;
  F radius;
  F value;
  F range;

  DrawOpDial(vec3 const &_center, F _radius, F _value, F _range) : DrawOp(tid), center(_center), radius(_radius), value(_value), range(_range) {}
  DrawOpDial(DrawOpDial const &other) = default;

};

struct DrawOpLabel : DrawOp {

  static constexpr DrawOpType tid = DrawOp_label;

  F value;
  string_view text;

  DrawOpLabel(F _value, string_view _text) : DrawOp(tid), value(_value), text(_text) {}
  DrawOpLabel(DrawOpLabel const &other) = default;

};

struct DrawOpPolicyTerrain : DrawOp {

  static constexpr DrawOpType tid = DrawOp_policyTerrain;

  BoxedRef rMetricRef;
  PolicySearchDim xVar, yVar;
  U64 paramHash;
  U64 compiledSheetEpoch;
  int sampleIndex;
  F baseValue;

  DrawOpPolicyTerrain(
    BoxedRef _rMetricRef,
    PolicySearchDim _xVar,
    PolicySearchDim _yVar,
    U64 _paramHash,
    U64 _compiledSheetEpoch,
    int _sampleIndex,
    F _baseValue)
    : DrawOp(tid),
      rMetricRef(_rMetricRef),
      xVar(_xVar),
      yVar(_yVar),      
      paramHash(_paramHash),
      compiledSheetEpoch(_compiledSheetEpoch),
      sampleIndex(_sampleIndex),
      baseValue(_baseValue)
  {
  }
  DrawOpPolicyTerrain(DrawOpPolicyTerrain const &other) = default;

};

struct DrawOpParticleSearch : DrawOp {

  static constexpr DrawOpType tid = DrawOp_particleSearch;

  ParticleSearchAlg alg;
  BoxedRef rMetricRef;
  std::span<PolicySearchDim> searchDims;
  U64 paramHash;
  U64 compiledSheetEpoch;
  int sampleIndex;
  F baseValue;

  DrawOpParticleSearch(
    ParticleSearchAlg _alg,
    BoxedRef _rMetricRef,
    std::span<PolicySearchDim> _searchDims,
    U64 _paramHash,
    U64 _compiledSheetEpoch,
    int _sampleIndex,
    F _baseValue)
    : DrawOp(tid),
      alg(_alg),
      rMetricRef(_rMetricRef),
      searchDims(_searchDims),
      paramHash(_paramHash),
      compiledSheetEpoch(_compiledSheetEpoch),
      sampleIndex(_sampleIndex),
      baseValue(_baseValue)
  {
  }
  DrawOpParticleSearch(DrawOpParticleSearch const &other) = default;
  
};

struct DrawOpMinpackSearch : DrawOp {

  static constexpr DrawOpType tid = DrawOp_minpackSearch;

  MinpackAlg alg;
  BoxedRef rMetricRef;
  std::span<PolicySearchDim> searchDims;
  U64 paramHash;
  U64 compiledSheetEpoch;
  int sampleIndex;
  F baseValue;

  DrawOpMinpackSearch(
    MinpackAlg _alg,
    BoxedRef _rMetricRef,
    std::span<PolicySearchDim> _searchDims,
    U64 _paramHash,
    U64 _compiledSheetEpoch,
    int _sampleIndex,
    F _baseValue)
    : DrawOp(tid),
      alg(_alg),
      rMetricRef(_rMetricRef),
      searchDims(_searchDims),
      paramHash(_paramHash),
      compiledSheetEpoch(_compiledSheetEpoch),
      sampleIndex(_sampleIndex),
      baseValue(_baseValue)
  {
  }

  DrawOpMinpackSearch(DrawOpMinpackSearch const &other) = default;

};

struct DrawOpLinePlot : DrawOp {

  static constexpr DrawOpType tid = DrawOp_linePlot;

  F x, y, z;

  DrawOpLinePlot(F _x, F _y, F _z) : DrawOp(tid), x(_x), y(_y), z(_z) {}
  DrawOpLinePlot(DrawOpLinePlot const &other) = default;

};

struct DrawOpBandPlot : DrawOp {

  static constexpr DrawOpType tid = DrawOp_bandPlot;

  F x, y0, y1, z;

  DrawOpBandPlot(F _x, F _y0, F _y1, F _z) : DrawOp(tid), x(_x), y0(_y0), y1(_y1), z(_z) {}
  DrawOpBandPlot(DrawOpBandPlot const &other) = default;

};

struct DrawOpScatterPlot : DrawOp {

  static constexpr DrawOpType tid = DrawOp_scatterPlot;

  F x, y, z;

  DrawOpScatterPlot(F _x, F _y, F _z) : DrawOp(tid), x(_x), y(_y), z(_z) {}
  DrawOpScatterPlot(DrawOpScatterPlot const &other) = default;

};

struct DrawOpEpicycles : DrawOp {

  static constexpr DrawOpType tid = DrawOp_epicycles;

  cvec v;
  F xRange, yRange;

  DrawOpEpicycles(cvec const &_v, F _xRange, F _yRange) : DrawOp(tid), v(_v), xRange(_xRange), yRange(_yRange) {}
  DrawOpEpicycles(DrawOpEpicycles const &other) = default;

};

struct DrawOpMujocoScene : DrawOp {

  static constexpr DrawOpType tid = DrawOp_mujocoScene;

  struct mjModel_ *m;
  struct mjvScene_ *scene;

  DrawOpMujocoScene(struct mjModel_ *_m, struct mjvScene_ *_scene) : DrawOp(tid), m(_m), scene(_scene) {}
  DrawOpMujocoScene(DrawOpMujocoScene const &other) = default;

};

// ADD DrawOp
