#include "./defs.h"
#include "mujoco/mujoco.h"

// Suitable for geom, which uses float regardless of mjtNum
inline mat4 mjMat4FromOrientPos(float mat[9], float pos[3])
{
  mat4 ret;
  ret <<
    mat[0], mat[1], mat[2], pos[0],
    mat[3], mat[4], mat[5], pos[1],
    mat[6], mat[7], mat[8], pos[2],
    0, 0, 0, 1;
  return ret;
}

inline mat4 mjMat4FromQuatPos(mjtNum *quat, mjtNum *pos)
{
  mjtNum mat[9];
  mju_quat2Mat(mat, quat);
  mat4 ret;
  ret <<
    mat[0], mat[1], mat[2], pos[0],
    mat[3], mat[4], mat[5], pos[1],
    mat[6], mat[7], mat[8], pos[2],
    0, 0, 0, 1;
  return ret;
}

inline mat3 mjMat3FromQuat(mjtNum *quat)
{
  mjtNum mat[9];
  mju_quat2Mat(mat, quat);
  mat3 ret;
  ret <<
    mat[0], mat[1], mat[2],
    mat[3], mat[4], mat[5],
    mat[6], mat[7], mat[8];
  return ret;
}
