#include "./core.h"
#include "../geom/gl_headers.h"
#include "../geom/g8.h"
#include "../geom/wgpu_utils.h"
#include "./main_window.h"
#include "./joybox.h"
#include "backends/imgui_impl_glfw.h"
#include "GLFW/glfw3.h"
#if defined(__EMSCRIPTEN__)
	#include <emscripten.h>
  #include <emscripten/html5.h>
  #include <emscripten/html5_webgpu.h>
  #include <webgpu/webgpu_cpp.h>
#else
  #include "glfw3webgpu.h"
#endif

shared_ptr<MainWindow> mainWindow0;
ImGuiContext *imguiContext = nullptr;

MainWindow::MainWindow()
{
  logEvents = 0;
}

MainWindow::~MainWindow()
{
  teardown();
}

void MainWindow::setUi(shared_ptr<MainUi> _ui)
{
  if (ui) {
    ui->pleaseClose();
  }
  ui = _ui;
  if (ui) {
    ui->setupMainUi();
  }
}


MainUi::MainUi(MainWindow &_win)
  :win(_win)
{
}

MainUi::~MainUi()
{
}

void MainUi::pleaseClose()
{
}

void MainUi::uiAutosave()
{
}

void MainUi::prepareDraw()
{
}


UiPollResult MainWindow::uiPoll()
{
  if (wantClose) return UiPoll_closeme;
  SetCurrentContext(imguiContext);

  glfwPollEvents();
#if defined(__EMSCRIPTEN__)
#else
#endif

  return UiPoll_userVisible;
}

void MainWindow::uiAutosave()
{
  if (ui) ui->uiAutosave();
}

void MainWindow::setup()
{
  setupWindow();

  // Setup Dear ImGui context
  IMGUI_CHECKVERSION();
  if (!imguiContext) {
    imguiContext = CreateContext();
  }
  SetCurrentContext(imguiContext);
  GetIO().IniFilename = nullptr;

  setupDisplayScale();

  ImGui_ImplGlfw_InitForOther(glfw_window, true);

  ImGui_ImplWGPU_Init(wgpu_device, 3, 
    WGPUTextureFormat_BGRA8Unorm,
    1 ? WGPUTextureFormat_Depth32Float : WGPUTextureFormat_Undefined);

  setupStyle();
  setupFonts();

  setupAudio();
  setupIcon();
}

void MainWindow::setupWindow()
{
#if defined(__EMSCRIPTEN__)
  glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
  int wantWidth = 100;
  int wantHeight = 100;
  glfw_window = glfwCreateWindow(wantWidth, wantHeight, "Throbol", nullptr, nullptr);
#else
  glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
  glfwWindowHint(GLFW_COCOA_RETINA_FRAMEBUFFER, GLFW_TRUE);
  glfwWindowHintString(GLFW_COCOA_FRAME_NAME, "Throbol");
  int wantWidth = 1280;
  int wantHeight = 720;

  int nMonitors = 0;
  auto monitors = glfwGetMonitors(&nMonitors);
  assert(nMonitors >= 1);
  for (int moni = 0; moni < nMonitors; moni++) {
    int mwidth = -1, mheight = -1;
    glfwGetMonitorWorkarea(monitors[moni], nullptr, nullptr, &mwidth, &mheight);
    int thisWantWidth = (mwidth * 9 / 10 / 8) * 8;
    int thisWantHeight = (mheight * 9 / 10 / 8) * 8;
    if (thisWantWidth > wantWidth || thisWantHeight > wantHeight) {
      wantWidth = thisWantWidth;
      wantHeight = thisWantHeight;
    }
  }
  glfw_window = glfwCreateWindow(wantWidth, wantHeight, "Throbol", nullptr, nullptr);
#endif

  if (!glfw_window) {
    L() << "glfwCreateWindow failed";
    wantClose = true;
    return;
  }

  static map<GLFWwindow *, weak_ptr<MainWindow>> winmap;
  winmap[glfw_window] = shared_from_this();

#if defined(__EMSCRIPTEN__)
#else
  glfwSetWindowCloseCallback(glfw_window, [](GLFWwindow *w) {
    auto p = winmap[w];
    if (auto it = p.lock()) {
      it->wantClose = true;
    }
  });
#endif


#if defined(__EMSCRIPTEN__)
#else
  wgpuSetLogLevel(WGPULogLevel_Debug);
#endif

#if defined(__EMSCRIPTEN__)
#else
  WGPUInstanceDescriptor instance_desc {};
  wgpu_instance = wgpuCreateInstance(&instance_desc);
  WGPURequestAdapterOptions adapter_options = {};
  wgpu_adapter = requestAdapter(wgpu_instance, &adapter_options);
  if (!wgpu_adapter) throw runtime_error("requestAdapter failed");
#endif

  vector<WGPUFeatureName> requiredFeatures;
#if defined(__EMSCRIPTEN__)
#else
  requiredFeatures.push_back(WGPUFeatureName_Depth32FloatStencil8);
  requiredFeatures.push_back(WGPUFeatureName(WGPUNativeFeature_TextureAdapterSpecificFormatFeatures));
#endif

#if defined(__EMSCRIPTEN__)
  wgpu_device = emscripten_webgpu_get_device();
#else
  WGPUDeviceDescriptor device_desc = {
    .requiredFeaturesCount = uint32_t(requiredFeatures.size()),
    .requiredFeatures = requiredFeatures.data(),
    //.requiredLimits = &required_limits
  };
  wgpu_device = requestDevice(wgpu_adapter, &device_desc);
#endif
  if (!wgpu_device) throw runtime_error("requestDevice failed");
  if (0) L() << "wgpu_device=" << wgpu_device;

  WGPUSupportedLimits supported_limits = {};
  wgpuDeviceGetLimits(wgpu_device, &supported_limits);

  
#if defined(__EMSCRIPTEN__)
#else
  wgpu_surface = glfwGetWGPUSurface(wgpu_instance, glfw_window);
  if (!wgpu_surface) throw runtime_error("glfwGetWGPUSurface failed");
#endif


#if defined(__EMSCRIPTEN__)
  // If we don't set some callback, library_glfw.js throws in onWindowContentScaleChanged.
  glfwSetWindowContentScaleCallback(glfw_window, [](GLFWwindow *w, float xscale, float yscale) {
    if (0) L() << "glfwSetWindowContentScaleCallback " << xscale << "," << yscale;
  });
#endif

  if (0) wgpuDeviceSetUncapturedErrorCallback(wgpu_device, 
    [](WGPUErrorType type, char const* message, void*) {
      L() << "Uncaptured device error: type " << type << ": " << message;
      throw runtime_error("wgpu error");
    }, nullptr);

  wgpu_queue = wgpuDeviceGetQueue(wgpu_device);
  if (!wgpu_queue) throw runtime_error("wgpuDeviceGetQueue failed");

}

void MainWindow::setupDisplayScale()
{
  const int verbose = 0;
  auto &io = GetIO();

  // wgpu_notyet FIXME: get this from glfw or something
  io.FontGlobalScale = 0.5f;
  if (verbose) L() << "Display: io.FontGlobalScale=" << io.FontGlobalScale;
}

void MainWindow::setupStyle()
{

#if defined(__EMSCRIPTEN__)
  if (EM_ASM_INT(
    return document.documentElement.classList.contains('light') ? 1 : 0;
  )) {
    uiStyle = UiLight;
  } else {
    uiStyle = UiDark;
  }
#endif

  auto &style = GetStyle();
  switch (uiStyle) {
    case UiDark:
      // Use ImGui's dark mode, but a slightly brighter window bg color to contrast with our
      // entirely black main background.
      StyleColorsDark();
      style.Colors[ImGuiCol_WindowBg] = ImVec4(0.16f, 0.16f, 0.16f, 0.80f);
      clearColor = ImVec4(0.0f, 0.0f, 0.0f, 1.0f);
      break;

    case UiClassic:
      StyleColorsClassic();
      clearColor = ImVec4(1.0f, 1.0f, 1.0f, 1.0f);
      break;

    case UiLight:
      StyleColorsLight();
      clearColor = ImVec4(1.0f, 1.0f, 1.0f, 1.0f);
      break;
  }

  style.WindowRounding = 4;
  style.FramePadding = ImVec2(4, 2); // normally (4, 3). Saves a little vertical space.
}

void MainWindow::setupFonts()
{
  static bool done;
  if (done) return;
  done = true;
  
  auto &io = GetIO();

  float fontScale = 1.0f / io.FontGlobalScale;
  io.Fonts->AddFontDefault();
  if (1) {
    auto sansFn = installDir + "/assets/fonts/SourceSansPro-Regular.ttf";
    // auto sansFn = installDir + "/deps/imgui/misc/fonts/DroidSans.ttf";

    ysFonts.smlFont = addFontWithIcons(sansFn, "", 9.0f * fontScale);
    ysFonts.medFont = addFontWithIcons(sansFn, "", 12.0f * fontScale);
    ysFonts.lrgFont = addFontWithIcons(sansFn, "", 14.0f * fontScale);
#if defined(__EMSCRIPTEN__)
    ysFonts.hugeFont = ysFonts.lrgFont;
#else
    ysFonts.hugeFont = addFontWithIcons(sansFn, "", 32.0f * fontScale);
#endif

    if (!ysFonts.smlFont || !ysFonts.medFont || !ysFonts.lrgFont || !ysFonts.hugeFont) {
      L() << "Failed to load font " << sansFn;
      abort();
    }    

    if (tweakForPodcast) {
      io.FontDefault = ysFonts.lrgFont;
    }
    else {
      io.FontDefault = ysFonts.medFont;
    }

    io.Fonts->Build();
    blitIconsIntoFont();
  }
}

void MainWindow::setupIcon()
{
#if 0 // doesn't seem to work
  auto img = readGlfwImagePng(installDir + "/images/appicon@256.png");
  if (img) {
    glfwSetWindowIcon(glfw_window, 1, img.get());
  }
#endif
}

void MainWindow::uiRedraw()
{
  auto &io = GetIO();

  ui->prepareDraw();

  uiRender1();

  // Start the Dear ImGui frame
  ImGui_ImplWGPU_NewFrame();
#if 0
  double pixrat = 2.0;
  io.DisplayFramebufferScale = ImVec2(pixrat, pixrat);
  io.DisplaySize = ImVec2(wgpu_swap_chain_width/pixrat, wgpu_swap_chain_height/pixrat);
  ImGui_ImplGlfw_UpdateMouseData();
  ImGui_ImplGlfw_UpdateMouseCursor();

  // Update game controllers (if enabled and available)
  ImGui_ImplGlfw_UpdateGamepads();

#else
  ImGui_ImplGlfw_NewFrame();
#endif

#if defined(__EMSCRIPTEN__)
  //io.DisplaySize = ImVec2(800, 600);
  //io.DisplayFramebufferScale = ImVec2(2, 2);
#else
#endif

  NewFrame();

  switch (uiStyle) {
    case UiDark:
      darkenFg = false;
      lightenBg = false;
      break;
    case UiLight:
      darkenFg = true;
      lightenBg = true;
      break;
    case UiClassic:
      darkenFg = true;
      lightenBg = true;
      break;
  }

  if (ui) {
    ui->initGlobalDo();
    ui->drawBackground();
  }

  if (io.ConfigMacOSXBehaviors) {
    if (IsModCmd() && IsKeyPressed(ImGuiKey_Q)) {
      wantClose = true;
    }
  }
  else {
    if (IsModCtrl() && IsKeyPressed(ImGuiKey_Q)) {
      wantClose = true;
    }
  }

  PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0);

  if (ui) ui->drawMainUi();
  PopStyleVar();

  if (showDemoWindow) {
    ShowDemoWindow(&showDemoWindow);
  }
  if (showMetricsWindow) {
    ShowMetricsWindow(&showMetricsWindow);
  }

  drawWindowLineq();

  Render();
  uiRender2();

  if (ui) {
    ui->execGlobalDoPost();
    if (wantClose) {
      ui->pleaseClose();
    }
  }
}

void MainWindow::uiRender1()
{
  //auto &io = GetIO();

  auto &perW = g8ResourcesPerWindow;
  if (perW.uniformsBuf.allocSize() > 10000) {
    perW.uniformsBuf.clear();
  }

  for (auto shader : *G8ShaderGeneric::allShaders) {
    auto &perSW = g8ResourcesPerShaderWindow[shader->shaderIndex];

#if 0
    if (0) L() << "Resource usage " << shader->name << 
      " vtx.validSize=" << perSW.vtxBufs.validSize() << 
      " idx.validSize=" << perSW.idxBufs.validSize() << 
      " uni.validSize=" << perSW.uniformsBuf.validSize() << 
      " geomCache.size=" << perSW.geomCache.size();
#endif

    size_t maxUsage = perSW.geomCache.empty() ? 16*1024*1024 : 64*1024*1024;

    for (size_t bi = 0; bi < perSW.vtxBufs.size() && bi < perSW.idxBufs.size(); bi++) {
      auto &vtxb = perSW.vtxBufs[bi];
      auto &idxb = perSW.idxBufs[bi];
      if (vtxb.validSize() + idxb.validSize() > maxUsage) {
        vtxb.clear();
        idxb.clear();
        for (auto p = perSW.geomCache.ents.begin(); p != perSW.geomCache.ents.end(); ) {
          if (!p->second.second) {
            p = perSW.geomCache.ents.erase(p);
          }
          if (p->second.second->viBufIndex == bi) {
            p = perSW.geomCache.ents.erase(p);
          }
          else {
            p++;
          }
        }
      }
    }
    perSW.textures.clear();

  }


  int width = -1, height = -1;
#if defined(__EMSCRIPTEN__)

  auto canvas_id = "canvas";
  double canvas_w = 0.0, canvas_h = 0.0;
  double pixel_ratio = emscripten_get_device_pixel_ratio(); // FIXME
  emscripten_get_element_css_size(canvas_id, &canvas_w, &canvas_h);
  width = int(canvas_w * pixel_ratio);
  height = int(canvas_h * pixel_ratio);
  emscripten_set_canvas_element_size(canvas_id, width, height);

  int glfw_w = -1, glfw_h = -1;
  glfwGetWindowSize(glfw_window, &glfw_w, &glfw_h);
  if (glfw_w != canvas_w || glfw_h != canvas_h) {
    glfwSetWindowSize(glfw_window, canvas_w, canvas_h);
  }

  if (0) L() << "canvas=" << canvas_w << "x" << canvas_h <<
    " raw=" << width << "x" << height <<
    " glfw=" << glfw_w << "x" << glfw_h;


#else
  glfwGetFramebufferSize(glfw_window, &width, &height);
#endif

  // React to changes in screen size
  if (width != wgpu_swap_chain_width || height != wgpu_swap_chain_height) {
    wgpu_swap_chain_width = width;
    wgpu_swap_chain_height = height;

    ImGui_ImplWGPU_InvalidateDeviceObjects();
#if defined(__EMSCRIPTEN__)
#else
    if (wgpu_swap_chain) {
      wgpuSwapChainRelease(wgpu_swap_chain);
    }
#endif
    if (0) L() << "Create swap chain " << width << "x" << height;

    if (wgpu_depth_tex) {
      wgpuTextureRelease(wgpu_depth_tex);
      wgpu_depth_tex = nullptr;
    }
    if (wgpu_depth_tex_view) {
      wgpuTextureViewRelease(wgpu_depth_tex_view);
      wgpu_depth_tex_view = nullptr;
    }
    {
      WGPUTextureDescriptor depth_tex_desc = {
        .label = "depth buffer",
        .usage = WGPUTextureUsage_RenderAttachment,
        .dimension = WGPUTextureDimension_2D,
        .size = {
          .width = uint32_t(width),
          .height = uint32_t(height),
          .depthOrArrayLayers = 1,
        },
        .format = WGPUTextureFormat_Depth32Float,
        .mipLevelCount = 1,
        .sampleCount = 1,
      };
      wgpu_depth_tex = wgpuDeviceCreateTexture(wgpu_device, &depth_tex_desc);
      if (!wgpu_depth_tex) throw runtime_error("wgpuDeviceCreateTexture failed");

      WGPUTextureViewDescriptor depth_tex_view = {
        .label = "depth buffer view",
        .format = depth_tex_desc.format,
        .dimension = WGPUTextureViewDimension_2D,
        .baseMipLevel = 0,
        .mipLevelCount = 1,
        .baseArrayLayer = 0,
        .arrayLayerCount = 1,
        .aspect = WGPUTextureAspect_All
      };
      wgpu_depth_tex_view = wgpuTextureCreateView(wgpu_depth_tex, &depth_tex_view);
      if (!wgpu_depth_tex_view) throw runtime_error("wgpuTextureCreateView failed");
    }        
    

#if defined(__EMSCRIPTEN__)
    WGPUTextureDescriptor render_tex_desc = {
      .label = "render buffer",
      .usage = WGPUTextureUsage_RenderAttachment,
      .dimension = WGPUTextureDimension_2D,
      .size = {
        .width = uint32_t(width),
        .height = uint32_t(height),
        .depthOrArrayLayers = 1,
      },
      .format = WGPUTextureFormat_BGRA8Unorm,
      .mipLevelCount = 1,
      .sampleCount = 1,
    };
    if (0) L() << "Create render buffer " << width << "x" << height;

    wgpu_render_tex = wgpuDeviceCreateTexture(wgpu_device, &render_tex_desc);
#else
    WGPUSwapChainDescriptor swap_chain_desc = {
      .label = "MainWindow swap chain",
      .usage = WGPUTextureUsage_RenderAttachment,
      .format = WGPUTextureFormat_BGRA8Unorm, // wgpuSurfaceGetPreferredFormat(wgpu_surface, wgpu_adapter);;
      .width = uint32_t(width),
      .height = uint32_t(height),
      .presentMode = WGPUPresentMode_Fifo
    };
    wgpu_swap_chain = wgpuDeviceCreateSwapChain(wgpu_device, wgpu_surface, &swap_chain_desc);
#endif
    if (!ImGui_ImplWGPU_CreateDeviceObjects()) {
      throw runtime_error("ImGui_ImplWGPU_CreateDeviceObjects failed");
    }
  }
}

void MainWindow::uiRender2()
{
  //auto &io = GetIO();


#if defined(__EMSCRIPTEN__)
  auto dd = ImGui::GetDrawData();
  assert(dd);

  dd->FramebufferScale.x = dd->FramebufferScale.y = emscripten_get_device_pixel_ratio();
  dd->DisplaySize.x = wgpu_swap_chain_width / dd->FramebufferScale.x;
  dd->DisplaySize.y = wgpu_swap_chain_height / dd->FramebufferScale.y;

  if (0) L() << "raw=" << 
    " fbscale=" << dd->FramebufferScale.x << "," << dd->FramebufferScale.y << 
    " ds=" << dd->DisplaySize.x << "," << dd->DisplaySize.y;

  auto canvas_tex_view = (WGPUTextureView)EM_ASM_PTR({
    let canvas = document.getElementById('canvas');
    canvas.width = $0;
    canvas.height = $1;
    let context = canvas.getContext('webgpu');
    let texture = context.getCurrentTexture();
    let view = texture.createView();

    let view1 = WebGPU.mgrTextureView.create(view);
    //console.log({canvas, context, texture, view, view1});

    return view1;
  }, wgpu_swap_chain_width, wgpu_swap_chain_height);

  if (!canvas_tex_view) {
    L() << "Failed to create canvas_tex_view";
    return;
  }

  WGPURenderPassColorAttachment color_attachments = {
    .view = canvas_tex_view,
    .resolveTarget = nullptr,
    .loadOp = WGPULoadOp_Clear,
    .storeOp = WGPUStoreOp_Store,
    .clearValue = { 
      clearColor.x * clearColor.w,
      clearColor.y * clearColor.w, 
      clearColor.z * clearColor.w,
      clearColor.w },
  };

#else
  auto next_texture_view = wgpuSwapChainGetCurrentTextureView(wgpu_swap_chain);
  if (!next_texture_view) throw runtime_error("wgpuSwapChainGetCurrentTextureView failed");

  WGPURenderPassColorAttachment color_attachments = {
    .view = next_texture_view,
    .resolveTarget = nullptr,
    .loadOp = WGPULoadOp_Clear,
    .storeOp = WGPUStoreOp_Store,
    .clearValue = { 
      clearColor.x * clearColor.w,
      clearColor.y * clearColor.w, 
      clearColor.z * clearColor.w,
      clearColor.w },
  };

#endif



  WGPURenderPassDepthStencilAttachment depth_stencil_attachment = {
    .view = wgpu_depth_tex_view,
    .depthLoadOp = WGPULoadOp_Clear,
    .depthStoreOp = WGPUStoreOp_Store,
    .depthClearValue = 1.0,
    .depthReadOnly = false,
    // Unclear who is right here
#if defined(WEBGPU_BACKEND_WGPU)
    .stencilLoadOp = WGPULoadOp_Clear,
    .stencilStoreOp = WGPUStoreOp_Discard,
#else
    .stencilLoadOp = WGPULoadOp_Undefined,
    .stencilStoreOp = WGPUStoreOp_Undefined,
#endif
    .stencilReadOnly = false
  };

  WGPURenderPassDescriptor render_pass_desc = {
    .colorAttachmentCount = 1,
    .colorAttachments = &color_attachments,
    .depthStencilAttachment = &depth_stencil_attachment
  };

  WGPUCommandEncoderDescriptor enc_desc = {};
  WGPUCommandEncoder encoder = wgpuDeviceCreateCommandEncoder(wgpu_device, &enc_desc);

  WGPURenderPassEncoder pass = wgpuCommandEncoderBeginRenderPass(encoder, &render_pass_desc);

  // colors can change if we switch dark/light mode
  SkinUniforms skinUniforms = {
    .dialBgCol = bgcolor(0x1e, 0x2a, 0x36, 0xff),
    .dialPosCol = fgcolor(0x3f, 0xf3, 0xff, 0xff),
    .dialNegCol = fgcolor(0xff, 0xa3, 0x2f, 0xff),
    .dialRangeCol = fgcolor(0xe0, 0xe4, 0xcc, 0xff),

    .traceBgColBase = bgcolor(0x22, 0x22, 0x22, 0xff),
    .traceBgColFailure = bgcolor(0x88, 0x22, 0x22, 0xff),
    .traceBgColReward = bgcolor(0x71, 0x6f, 0x4d, 0xff),
  };

  for (size_t i = 0; i < MAX_GRAPH_TRACES; i++) {
    skinUniforms.goodGraphColors[i] = goodGraphColor(i);
  }
  g8ResourcesPerWindow.skinUniformsBuf.clear();
  g8ResourcesPerWindow.skinUniformsBuf.addDynamic(skinUniforms);

  if (!g8ResourcesPerWindow.skinUniformsGpu) {
    g8ResourcesPerWindow.skinUniformsGpu.emplace(wgpu_device, "skinUniforms", WGPUBufferUsage_CopyDst | WGPUBufferUsage_Uniform, sizeof(SkinUniforms));
  }
  g8ResourcesPerWindow.skinUniformsGpu->sync(wgpu_device, wgpu_queue, g8ResourcesPerWindow.skinUniformsBuf);

  for (auto shader : *G8ShaderGeneric::allShaders) {
    auto &perSW = g8ResourcesPerShaderWindow[shader->shaderIndex];
    auto &perW = g8ResourcesPerWindow;

    perW.uniformsGpu->sync(wgpu_device, wgpu_queue, perW.uniformsBuf);
    size_t nb = min(perSW.idxBufs.size(), perSW.vtxBufs.size());
    while (perSW.idxGpus.size() < nb) {
      size_t vi = perSW.idxGpus.size();
      perSW.idxGpus.emplace_back(wgpu_device, "idx", WGPUBufferUsage_CopyDst | WGPUBufferUsage_Index, perSW.idxBufs[vi].allocSize());
    }
    while (perSW.vtxGpus.size() < nb) {
      size_t vi = perSW.vtxGpus.size();
      perSW.vtxGpus.emplace_back(wgpu_device, "vtx", WGPUBufferUsage_CopyDst | WGPUBufferUsage_Vertex, perSW.vtxBufs[vi].allocSize());
    }

    for (size_t bi = 0; bi < nb; bi++) {
      perSW.idxGpus[bi].sync(wgpu_device, wgpu_queue, perSW.idxBufs[bi]);
      perSW.vtxGpus[bi].sync(wgpu_device, wgpu_queue, perSW.vtxBufs[bi]);
    }
  }

  ui->renderBackground(pass);
  ImGui_ImplWGPU_RenderDrawData(ImGui::GetDrawData(), pass);

  wgpuRenderPassEncoderEnd(pass);

  WGPUCommandBufferDescriptor cmd_buffer_desc = {};
  WGPUCommandBuffer cmd_buffer = wgpuCommandEncoderFinish(encoder, &cmd_buffer_desc);
  wgpuQueueSubmit(wgpu_queue, 1, &cmd_buffer);

  //wgpuCommandBufferDrop(cmd_buffer);
#if defined(__EMSCRIPTEN__)
  wgpuTextureViewRelease(canvas_tex_view);
  canvas_tex_view = nullptr;

#else
  wgpuSwapChainPresent(wgpu_swap_chain);
  wgpuTextureViewRelease(next_texture_view);
#endif

  for (auto shader : *G8ShaderGeneric::allShaders) {
    auto &resources = g8ResourcesPerShaderWindow[shader->shaderIndex];
    for (auto it : resources.ephemeralBindGroups) {
      wgpuBindGroupRelease(it);
    }
    resources.ephemeralBindGroups.clear();
    for (auto it : resources.ephemeralTextureViews) {
      wgpuTextureViewRelease(it);
    }
    resources.ephemeralTextureViews.clear();
  }

  if (0) {
    double curTime = realtime();
    if (0) L() << "Swap " << loopCount << " " << curTime - lastLoopTime;
    lastLoopTime = curTime;
  }
  loopCount++;
}

void MainWindow::printWebgpuStats(ostream &s)
{
  for (auto shader : *G8ShaderGeneric::allShaders) {
    s << shader->name << " vt=" << shader->getVertexType() << " vertexSize=" << shader->getVertexSize() << "\n";
    auto &perSW = g8ResourcesPerShaderWindow[shader->shaderIndex];
    auto &perW = g8ResourcesPerWindow;
    s << "  uniforms " << perW.uniformsBuf.validSize() << "\n";
    s << "  index";
    for (auto &b : perSW.idxBufs) {
      s << " " << b.validSize();
    }
    s << "\n  vertex";
    for (auto &b : perSW.vtxBufs) {
      s << " " << b.validSize();
    }
    s << "\n  geomCache size=" << perSW.geomCache.size();
    s << " textures=" << perSW.textures.size();
    s << " cache=" << perW.textureCache.size();
    s << "\n";
  }

}


void MainWindow::drawWindowLineq()
{
  R now = realtime();
  R expiry = now - 5.0;
  while (!windowLineq.empty() && windowLineq.front().time < expiry) {
    windowLineq.pop_front();
    uiPollLive = true;
  }
  if (!windowLineq.empty()) {
    uiPollLive = true;
    SetNextWindowPos(ImVec2(
        GetIO().DisplaySize.x * 0.5f,
        GetIO().DisplaySize.y - 5),
      ImGuiCond_Always,
      ImVec2(0.5, 1.0));
    if (Begin("Log", nullptr, 
        ImGuiWindowFlags_AlwaysAutoResize|
        ImGuiWindowFlags_NoFocusOnAppearing|
        ImGuiWindowFlags_NoDecoration|
        ImGuiWindowFlags_NoInputs)) {
      for (auto const &ent : windowLineq) {
        auto color = ent.color;
        auto fade = min(1.0, ent.time - expiry + 0.25);
        if (fade != 1.0) {
          ImColor ic(color);
          ic.Value.w *= fade;
          color = (U32)ic;
        }
        PushStyleColor(ImGuiCol_Text, color);
        Text("%s", ent.line.c_str());
        PopStyleColor();
      }
    }
    End();
  }
}


void MainWindow::teardown()
{
#if defined(__EMSCRIPTEN__)
  // wgpu: free context?
#else
  if (glfw_window) {
    glfwDestroyWindow(glfw_window);
    glfw_window = nullptr;
  }

  if (wgpu_instance) {
    wgpuInstanceRelease(wgpu_instance);
    wgpu_instance = nullptr;
  }
  if (wgpu_adapter) {
    wgpuAdapterRelease(wgpu_adapter);
    wgpu_adapter = nullptr;
  }
#endif
}

void MainWindow::setupAudio()
{
#ifdef wgpu_notyet
  SDL_AudioSpec desired = {
    .freq = 48000,
    .format = AUDIO_S16SYS,
    .channels = 1,
    .samples = 1024,
    .callback = nullptr
  };

  audioDevice = SDL_OpenAudioDevice(
        nullptr, // name
        0, // iscapture
        &desired, &audioSpec,
        0);
  if (audioDevice == 0) {
    L() << "No audio available: " << SDL_GetError();
    return;
  }

  SDL_PauseAudioDevice(audioDevice, 0);

  if (0) {
    L() << "Audio got " << audioSpec;

    const int NSAMP = 4096*4;
    const int NCHAN = 2;
    vector<S16> s(NSAMP * NCHAN);
    for (auto i = 0; i < NSAMP; i++) {
      R t = R(i) / R(audioSpec.freq);
      R omega = 440.0 * M_2PI * (1.0 + t);
      R ampl = 2000.0 * 
          exp(min(0.0, (t - 0.02) / 0.01)) * 
          exp(min(0.0, (0.05 - t) / 0.05));
      s[i*NCHAN + 0] = (S16)(ampl * sin(t*omega));
      s[i*NCHAN + 1] = (S16)(ampl * sin(t*omega));
    }

    if (SDL_QueueAudio(audioDevice, s.data(), s.size() * sizeof(S16)) < 0) {
      L() << "Queue audio error: " << SDL_GetError();
    }
  }
#endif
}
