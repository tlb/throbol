
#test :: build

.PHONY: default build stage1 clean

clean ::
	rm -rf $(ODIR) build.src bin

.PHONY: test size lint

cross.% : pushsrc.%
	ssh $* 'cd throbol && make -j`nproc`'

cross-docker.% : pushsrc.%
	ssh $* 'cd throbol && make -j`nproc` dockers'

pushsrc.% :
	@echo rsync . to $*
	@git ls-files --recurse-submodules -z | fgrep -z -v -w \
		-e '.git' \
		-e '.gitignore' \
		-e '.gitmodules' \
		-e '.vscode' \
		-e '/yaml-cpp/test/' \
		-e '/tinyxml2/docs/' \
		| xargs -0 -J % rsync -lptgoDvR % $*:throbol/.

pushbin.% : build
	@echo rsync to $*
	@rsync -avR bin/. $*:throbol/.

rund.% : cross.%
	ssh -n $* 'sudo systemctl restart throbold-robot throbold-av'

force :


$(ODIR)/%.o : %.cc | build.deps
	@mkdir -p $(dir $@)
	@echo c++ $<
	@$(CXX) -o $@ -c -MMD -MF $(@:.o=.d) -MJ $(@:.o=.cc.compile_command) $(CXXFLAGS) $(INCLUDES) $<

.PRECIOUS: $(ODIR)/%.sdebug

$(ODIR)/%.sdebug : %.cc | build.deps
	@mkdir -p $(dir $@)
	@echo c++ $<
	@$(CXX) -o $@ -S -MMD -MF $(@:.sdebug=.d) $(CXXFLAGS) $(INCLUDES) $<


%.cc.analyze : %.cc | build.deps
	@mkdir -p $(dir $@)
	@echo c++ --analyze $<
	@$(CXX) -o $(ODIR) --analyze -Xclang -analyzer-output=html $(CXXFLAGS) $(INCLUDES) $<

$(ODIR)/%.o : %.cpp | build.deps
	@mkdir -p $(dir $@)
	@echo c++ $<
	@$(CXX) -o $@ -c -MMD -MF $(@:.o=.d) -MJ $(@:.o=.cpp.compile_command) $(CXXFLAGS) $(INCLUDES) $<

$(ODIR)/%.o : %.c | build.deps
	@mkdir -p $(dir $@)
	@echo cc $<
	@$(CC) -o $@ -c -MMD -MF $(@:.o=.d) -MJ $(@:.o=.c.compile_command) $(CFLAGS) $(INCLUDES) $<

$(ODIR)/%.o : %.m | build.deps
	@mkdir -p $(dir $@)
	@echo cc $<
	@$(CC) -o $@ -c -MMD -MF $(@:.o=.d) -MJ $(@:.o=.m.compile_command) $(CFLAGS) $(INCLUDES) $<

view/% : %
	code $*

MAKEFILES = Makefile mk/makerules.inc mk/makedefs.inc

compile_commands.json :: $(MAKEFILES) $(call to_objs, $(ALL_CXX_SRC))
	@echo "Generate $@"
	@( echo "["; cat /dev/null $(patsubst %, $(ODIR)/%.compile_command, $(ALL_CXX_SRC)) | sed -e '$$s/,$$//' ; echo "]" )> $@.tmp
	@mv $@.tmp $@
