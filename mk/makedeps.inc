
.PHONY: install.deps install.realsense setup.dirs

ifeq ($(CI_SERVER), yes)
SUDO =
else
SUDO = sudo
endif

ifeq ($(UNAME_SYSTEM),Linux)

DEB_RELEASE := jammy

setup.robotpkg.DONT:
	sudo mkdir -p /opt/openrobots
	sudo chown `whoami` /opt/openrobots
	echo "deb [arch=$(shell dpkg --print-architecture)] http://packages.ros.org/ros2/ubuntu $(DEB_RELEASE) main" | sudo tee /etc/apt/sources.list.d/ros2.list
	sudo curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key | sudo apt-key add -
	echo "deb [arch=$(shell dpkg --print-architecture)] http://robotpkg.openrobots.org/packages/debian/pub $(DEB_RELEASE) robotpkg" | sudo tee /etc/apt/sources.list.d/robotpkg.list
	sudo curl -sSL http://robotpkg.openrobots.org/packages/debian/robotpkg.key | sudo apt-key add -

# This should most be in sync with ../ci/jammy-batteries/Dockerfile

install.deps :: ## Install (using $(SUDO) apt-get install) dependencies on Ubuntu
	$(SUDO) DEBIAN_FRONTEND=noninteractive apt-get -y install \
		curl \
		git \
		make \
		software-properties-common \
		tzdata \
		pkg-config \
		cmake
	$(SUDO) apt-get -y install \
		libsqlite3-dev \
		libasound2-dev \
		libsdl2-dev \
		libjpeg-dev \
		libturbojpeg0-dev \
		libpng-dev \
		libz-dev \
		libspnav-dev \
		libx11-dev \
		libopenblas-dev \
		liblapack-dev \
		libarpack2-dev \
		libeigen3-dev \
		libcereal-dev \
		libhidapi-dev \
		libglfw3-dev \
		clang \
		libstdc++-12-dev \
		emscripten \
		libboost-dev \
		libyaml-cpp-dev \
		python3 \
		python3-pip \
		libboost-python-dev \
		libboost-filesystem-dev \
		doxygen \
		libedit-dev \
		libxmu-dev \
		graphviz \
		rustc \
		cargo
	$(SUDO) pip3 install \
		sphinx \
		xacro \
		breathe
endif

ifeq ($(UNAME_SYSTEM),Darwin)

install.deps :: ## Install (using brew install) deps for OSX
	brew install \
		cmake \
		pkg-config \
		jpeg-turbo \
		libpng \
		libspnav \
		eigen \
		git \
		sdl2 \
		hidapi \
		sqlite3 \
		catch2 \
		glfw \
		boost \
		yaml-cpp

install.dist-deps :: ## Install things only the main developers need
	brew install \
		emscripten

endif


check.deps :: 
	@which $(CC) || ( echo "Throbol requires an executable for $(CC)"; false )
	@which $(CXX) || ( echo "Throbol requires an executable for $(CXX)" ; false )


CMAKE ?= cmake


install.deps :: install.emsdk

install.emsdk :
	deps/emsdk/emsdk install 3.1.35
	deps/emsdk/emsdk activate 3.1.35

build.deps :: $(ODIR)/mujoco/.built

$(ODIR)/mujoco/.built : $(ODIR)/mujoco/Makefile
	env - PATH="$(PATH)" $(MAKE) -j8 -C "$(ODIR)/mujoco"
	touch $@

$(ODIR)/mujoco/Makefile : mk/makedeps.inc
	env - \
		PATH="$(PATH)" \
		CC="$(CC)" CXX="$(CXX)" \
		$(CMAKE) -S deps/mujoco -B "$(ODIR)/mujoco" \
		"-DMUJOCO_CXX_FLAGS=-include;$(abspath env/mujoco_port.h)" \
		-DCMAKE_BUILD_TYPE=Release \
		-DFETCHCONTENT_FULLY_DISCONNECTED=1 \
		-DFETCHCONTENT_SOURCE_DIR_CCD=deps/libccd \
		-DFETCHCONTENT_SOURCE_DIR_QHULL=deps/qhull \
		-DFETCHCONTENT_SOURCE_DIR_TINYXML2=deps/tinyxml2 \
		-DFETCHCONTENT_SOURCE_DIR_TINYOBJLOADER=deps/tinyobjloader \
		-DFETCHCONTENT_SOURCE_DIR_LODEPNG=deps/lodepng \
		-DMUJOCO_BUILD_TESTS=0 \
		-DMUJOCO_BUILD_EXAMPLES=0 \
		-DMUJOCO_BUILD_SIMULATE=0 \
		-DMUJOCO_TEST_PYTHON_UTIL=0 \
		-DBUILD_SHARED_LIBS=0 \
		-DMUJOCO_INSTALL=0

build.mujoco-doc :
	env - PATH="$(PATH)" $(MAKE) -C deps/mujoco/doc html
	rm -rf doc/mujoco
	mv deps/mujoco/doc/_build/html doc/mujoco
	rm -rf deps/mujoco/doc/_build


build.deps :: $(ODIR)/Catch2/.built
$(ODIR)/Catch2/Makefile :
	env - \
		PATH="$(PATH)" \
		CC="$(CC)" CXX="$(CXX)" \
		$(CMAKE) -S deps/Catch2 -B "$(ODIR)/Catch2" \
		-DCATCH_INSTALL_DOCS=0

$(ODIR)/Catch2/.built : $(ODIR)/Catch2/Makefile
	env - PATH="$(PATH)" $(MAKE) -j8 -C "$(ODIR)/Catch2"
	touch $@


build.deps ::

# Otherwise, this is built within Mujoco


ifneq ($(BUILD_FOR_WASM),)
#build.deps :: $(ODIR)/libccd/.built
endif

$(ODIR)/libccd/.built : $(ODIR)/libccd/Makefile
	env - PATH="$(PATH)" $(MAKE) -j8 -C "$(ODIR)/libccd"
	touch $@

$(ODIR)/libccd/Makefile :
	env - \
		PATH="$(PATH)" \
		CC="$(CC)" CXX="$(CXX)" \
		$(CMAKE) -S deps/libccd -B "$(ODIR)/libccd" \
		-DENABLE_DOUBLE_PRECISION=1


ifeq ($(BUILD_FOR_WASM),)
ifneq ($(WGPU_NATIVE_BUILD_TYPE),dist)
build.deps :: $(ODIR)/wgpu-native/.built

$(ODIR)/wgpu-native/.built :
	mkdir -p $(dir $@)
	env - PATH="$(PATH)" $(MAKE) -j8 -C "deps/wgpu-native"
	touch $@
endif
endif


ifneq ($(BUILD_FOR_WASM),)
build.deps :: $(ODIR)/yaml-cpp/libyaml-cpp.a
$(ODIR)/yaml-cpp/libyaml-cpp.a : 
	env - PATH="$(PATH)" emcmake cmake \
		-S deps/yaml-cpp -B "$(ODIR)/yaml-cpp" \
		-DBUILD_TESTING=0
	env - PATH="$(PATH)" emmake make -C "$(ODIR)/yaml-cpp" VERBOSE=0 COLOR=
endif

build : setup.dirs

setup.dirs ::
