
SHELL := bash
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

TMPDIR = /tmp

ifeq ($(CI_SERVER),yes)
DEBUG := -O
else
DEBUG := -O2
-include mk/makelocal.inc
endif

ODIR := build.$(UNAME_MACHINE)
BIN := bin

CFLAGS += 
CXXFLAGS +=
LDFLAGS +=
THROBOLD_LDFLAGS +=
GUI_LDFLAGS +=

ifneq (,$(SANITIZE))
DEBUG += -fsanitize=$(SANITIZE)
ODIR := $(ODIR).san-$(SANITIZE)
BIN := $(BIN).san-$(SANITIZE)
endif


to_objs = $(patsubst %.c,$(ODIR)/%.o,$(patsubst %.cc,$(ODIR)/%.o,$(patsubst %.cpp,$(ODIR)/%.o,$(1))))


ifeq ($(UNAME_SYSTEM),Darwin)
SUPPORTED_OS := yes
CXX = clang++
CC = clang

ifeq ($(UNAME_MACHINE),x64)
#CXXFLAGS += -mavx2
endif
ifeq ($(UNAME_MACHINE),arm64)
endif

LDFLAGS += -framework Metal -framework CoreFoundation -framework Cocoa -framework IOKit -framework CoreFoundation -framework CoreGraphics -framework Foundation -framework IOSurface -framework QuartzCore
LDFLAGS += -framework Accelerate

INCLUDES += -I /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/System/Library/Frameworks/Accelerate.framework/Frameworks/vecLib.framework/Headers


endif # Darwin




ifeq ($(UNAME_SYSTEM),Linux)
SUPPORTED_OS := yes

CXX = clang++
CC = clang

ifeq ($(UNAME_MACHINE),x64)
# avx2 changes alignment requirements of Eigen, so it's more trouble than it's worth
# because any library not built with the same flag will be incompatible
# and probably crash
# CXXFLAGS += -mavx2
endif

ifneq (,$(wildcard /usr/libexec/mold/ld))
LDFLAGS += -B/usr/libexec/mold
endif

LDFLAGS += -lpthread -lm -ldl

CXXFLAGS += -DUSE_V4L

endif # Linux




ifneq ($(BUILD_FOR_WASM),)
SUPPORTED_OS := yes
CXX = emcc
CC = emcc
CMAKE = emcmake cmake

export PATH := $(CURDIR)/deps/emsdk/upstream/emscripten:$(PATH)

CXXFLAGS += -Wno-deprecated-declarations

ifeq (GOOD,GOOD)
LDFLAGS += -sALLOW_MEMORY_GROWTH=1
LDFLAGS += -sINITIAL_MEMORY=33554432
LDFLAGS += -sTOTAL_STACK=16777216
LDFLAGS += -sSTACK_OVERFLOW_CHECK=1
else
# Too large seems to cause random 'The ArrayBufferView size exceeds the supported range' in the webgl glue code
# LDFLAGS += -sINITIAL_MEMORY=2147418112
LDFLAGS += -sINITIAL_MEMORY=1073741824
LDFLAGS += -sTOTAL_STACK=16777216
LDFLAGS += -sSTACK_OVERFLOW_CHECK=1
endif

CXXFLAGS += -sDISABLE_EXCEPTION_CATCHING
#CXXFLAGS += -fno-exceptions
#LDFLAGS += -fno-exceptions

#CXXFLAGS += -fexceptions
#LDFLAGS += -fexceptions

# TEMP
LDFLAGS += --minify 0

LDFLAGS += -sMODULARIZE=1 -sEXPORT_ES6=0

LDFLAGS += -sFETCH

LDFLAGS += -sUSE_WEBGPU=1 -sUSE_GLFW=3

LDFLAGS += -s 'EXPORTED_FUNCTIONS=_main,_tbOpenSheetWin,_tbQuit,_tbStyleChanged'

LDFLAGS += -s 'EXPORTED_RUNTIME_METHODS=callMain,stackAlloc,allocateUTF8OnStack,FS'

ifeq ($(BUILD_FOR_WASM),multithread)
CFLAGS += -pthread
CXXFLAGS += -pthread
LDFLAGS += -pthread -sPTHREAD_POOL_SIZE=12
endif

show.wasm:
	@echo $(PATH)
	which emcc
	emcc --version

endif # WASM




ifneq (yes,$(SUPPORTED_OS))
$(warning "Unsupported operating system $(UNAME_SYSTEM). Maybe you can add it in mk/makedefs.inc")
endif

# Eigen
INCLUDES += -I deps/eigen

# xxhash
INCLUDES += -I deps/xxHash
LIB_SRC += deps/xxHash/xxhash.c

# cereal
INCLUDES += -I deps/cereal/include

# dynamixel
INCLUDES += -I deps/DynamixelSDK/c++/include/dynamixel_sdk

INCLUDES += -I .

ifeq ($(BUILD_FOR_WASM),)
INCLUDES += -I geom/wgpu-port
INCLUDES += -I deps/glfw3webgpu

WGPU_NATIVE_BUILD_TYPE ?= dist

INCLUDES += -I deps/wgpu-native
ifeq ($(WGPU_NATIVE_BUILD_TYPE),dist)
ifeq ($(UNAME_SYSTEM),Darwin)
ifeq ($(UNAME_MACHINE),arm64)
LDFLAGS += -L deps/WebGPU-distribution-wgpu/bin/macos-arm64 -Wl,-rpath,$(abspath deps/WebGPU-distribution-wgpu/bin/macos-arm64) -lwgpu_native
endif
ifeq ($(UNAME_MACHINE),x64)
LDFLAGS += -L deps/WebGPU-distribution-wgpu/bin/macos-x86_64 -Wl,-rpath,$(abspath deps/WebGPU-distribution-wgpu/bin/macos-x86_64) -lwgpu_native
endif
endif
ifeq ($(UNAME_SYSTEM),Linux)
ifeq ($(UNAME_MACHINE),x64)
LDFLAGS += -L deps/WebGPU-distribution-wgpu/bin/linux-x86_64 -Wl,-rpath,$(abspath deps/WebGPU-distribution-wgpu/bin/linux-x86_64) -lwgpu_native
endif
endif
else
LDFLAGS += -L deps/wgpu-native/target/$(WGPU_NATIVE_BUILD_TYPE)/ -lwgpu_native
endif
LDFLAGS += $(shell pkg-config --libs glfw3)
endif


# yaml-cpp
ifneq ($(BUILD_FOR_WASM),)
# Use the in-tree includes, and the built library. See the yaml-cpp section in makedeps.inc
INCLUDES += -I deps/yaml-cpp/include
LDFLAGS += -L$(ODIR)/yaml-cpp -lyaml-cpp
else
# Use system instead of whatever is in deps
LDFLAGS += $(shell pkg-config --libs yaml-cpp)
INCLUDES += $(shell pkg-config --cflags yaml-cpp)
endif

# ODRI
INCLUDES += -I deps/master-board/sdk/master_board_sdk/include

# mujoco & friends

INCLUDES += -I deps/mujoco/include -I deps/lodepng
ifneq ($(BUILD_FOR_WASM),)
LDFLAGS += -L$(ODIR)/mujoco/lib -lmujoco -lccd -llodepng -lqhullstatic_r -ltinyobjloader -ltinyxml2
else
LDFLAGS += -L$(ODIR)/mujoco/lib -Wl,-rpath,$(abspath $(ODIR)/mujoco/lib) -lmujoco -lccd -llodepng -lqhullstatic_r -ltinyobjloader -ltinyxml2
endif

INCLUDES += -I deps/tinyobjloader

ifeq ($(UNAME_SYSTEM),Darwin)
INCLUDES += -I /opt/homebrew/include
endif

ifneq ($(BUILD_FOR_WASM),)
LDFLAGS += -lwebsocket.js
endif

# libjpeg
ifneq ($(BUILD_FOR_WASM),)
CXXFLAGS += -sUSE_LIBJPEG=1
LDFLAGS += -sUSE_LIBJPEG=1
else
INCLUDES += $(shell pkg-config libjpeg --cflags)
GUI_LDFLAGS += $(shell pkg-config libjpeg --libs)
endif

# libturbojpeg
ifeq ($(UNAME_SYSTEM),Linux)
THROBOLD_LDFLAGS += -lturbojpeg
endif

# libpng
ifneq ($(BUILD_FOR_WASM),)
CXXFLAGS += -sUSE_LIBPNG=1
LDFLAGS += -sUSE_LIBPNG=1
else
INCLUDES += $(shell pkg-config libpng --cflags)
GUI_LDFLAGS += $(shell pkg-config libpng --libs)
endif

# zlib
ifneq ($(BUILD_FOR_WASM),)
CXXFLAGS += -sUSE_ZLIB=1
LDFLAGS += -sUSE_ZLIB=1
else
LDFLAGS += -lz
endif

# hidapi
ifneq ($(BUILD_FOR_WASM),)
else
ifeq ($(UNAME_SYSTEM),Darwin)
INCLUDES += $(shell pkg-config hidapi --cflags)
GUI_LDFLAGS += $(shell pkg-config hidapi --libs)
else
INCLUDES += $(shell pkg-config hidapi-hidraw --cflags)
GUI_LDFLAGS += $(shell pkg-config hidapi-hidraw --libs)
endif
endif

# sqlite
INCLUDES += -I deps/sqlite
LIB_SRC += deps/sqlite/sqlite3.c

# sqlite_modern_cpp
INCLUDES += -I deps/sqlite_modern_cpp/hdr

# imgui
INCLUDES += \
	-I deps/imgui \
	-I deps/imgui/backends \
	-I deps/imgui/examples \
	-I deps/imgui/examples/libs

CXXFLAGS += -DIMGUI_USER_CONFIG=\"src/tb_imconfig.h\"

# catch2
ifeq (BAD,GOOD)
INCLUDES += $(shell pkg-config --cflags catch2)
TEST_LDFLAGS += $(shell pkg-config --libs catch2)
else
INCLUDES += -I deps/Catch2/src -I $(ODIR)/Catch2/generated-includes
TEST_LDFLAGS += -L $(ODIR)/Catch2/src -lCatch2
endif

# tlbcore
INCLUDES += -I deps/tlbcore

# CppNumericalSolvers
INCLUDES += -I deps/CppNumericalSolvers/include

# Pylon camera interface
ifneq (,$(wildcard /opt/pylon/include))
INCLUDES += -I/opt/pylon/include/
CXXFLAGS += -DUSE_PYLON
THROBOLD_LDFLAGS += \
	-L/opt/pylon/lib \
	-Wl,-rpath,/opt/pylon/lib \
	-lpylonbase \
	-lpylonutility \
	-lGenApi_gcc_v3_1_Basler_pylon \
	-lGCBase_gcc_v3_1_Basler_pylon
endif

WARNINGS = -Wall -Wformat -Wextra -Wno-unused-parameter -Wno-deprecated
# WARNINGS += -Wdouble-promotion

CFLAGS += $(DEBUG)
CXXFLAGS += $(DEBUG) $(WARNINGS)
CXXFLAGS += -std=c++20

# We depend on NaNs in emit_ops_logic.cc
# This doesn't seem to speed things up noticeably, so leave off for now
#CXXFLAGS += -ffast-math -fno-finite-math-only

CXXFLAGS += -DDEBUGFLAGS="\"$(DEBUG)\""

LDFLAGS += $(DEBUG)



$(ODIR)/src/formula_edit.o : WARNINGS += -Wno-unused-function

# The CppNumericalSolvers headers spew a lot of warnings
$(ODIR)/src/minpack_search.o : WARNINGS += -Wno-sign-compare -Wno-unknown-warning-option -Wno-bitwise-instead-of-logical

show.flags :: ## Show compiler flags
	@echo "$(CC) $(CFLAGS)"
	@echo "$(CXX) = $(CXXFLAGS)"
