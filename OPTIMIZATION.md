

Here's the arm64 code for emit_cos.
```asm
	sub	sp, sp, #64                     
	ldr	x9, [x0, #8]                    ; x9 = this[8]
	ldr	x8, [x2, #8]                    ; x8 = pad.pad
	ldr	w10, [x0, #20]                  ; w10 = va.ofs

	ldr	q0, [x8, x10]                   ; load 4 floats for a.
  
	fabs.4s	v2, v0                      ; v2 = abs(v0)

	mov	w10, #63875
	movk	w10, #16162, lsl #16
	dup.4s	v0, w10                     ; v0.all = constant, 2/PI

	fmul.4s	v0, v2, v0                  ; v0 = v2 * v0. So far, v0=abs(a)*(2/pi)

	mov	w10, #1262485504
	dup.4s	v1, w10                     ; v1 = constant, some tiny number like 2^-23 for rounding

	fadd.4s	v0, v0, v1                  ; v0 = v0 + v1

	str	q0, [sp, #48]                   ; stack[48] = q0. I think the point is to convert to integer.
	add	x10, sp, #48                    ; ???
	ldr	q1, [sp, #48]                   ; q1 = stack[48]. So now q1 = q0 = abs(a)*(2/pi)

	mov	w10, #-884998144
	dup.4s	v0, w10                     ; v0 = constant

	fadd.4s	v3, v1, v0                  ; v3 = v1 + v0

	mov	w10, #4056
	movk	w10, #49097, lsl #16
	dup.4s	v4, w10                     ; v4.all = constant

	mov.16b	v0, v2                      ; v0 = v2
	fmla.4s	v0, v4, v3                  ; v0 = v0 + v4 * v3    ?

	mov	w10, #34906
	movk	w10, #46248, lsl #16
	dup.4s	v4, w10                     ; v4.all = constant

	fmla.4s	v0, v4, v3                  ; v0 = v0 + v4 * v3

	mov	w10, #13508
	movk	w10, #42946, lsl #16
	dup.4s	v4, w10                     ; v4.all = constant

	mov	w10, #39432
	movk	w10, #18315, lsl #16
	dup.4s	v5, w10                     ; v5.all = constant

	fmla.4s	v0, v4, v3                  ; v0 = v0 + v4 * v3

	fcmge.4s	v3, v2, v5                ; v3 = v2 >= v5

	ext.16b	v4, v3, v3, #8
	orr.8b	v3, v4, v3                  
	umaxp.2s	v3, v3, v3                ; v3[0] = v3[0] | v3[1] | v3[2] | v3[4]

	fmov	w10, s3                      
	cbz	w10, LBB45_6                    ; if all zero (ie, none of v2 >= v5) goto _6

	mov	x10, #0
	stp	q0, q2, [sp, #16]
	add	x11, sp, #32                    ; =32
	mov	w12, #39432
	movk	w12, #18315, lsl #16
	mov	w13, #2139095040
	mov	x14, #2305843009213693952
	str	q1, [sp]
	mov	x15, sp
	adrp	x16, __ZZN5Eigen8internal16trig_reduce_hugeEfPiE11two_over_pi@GOTPAGE
	ldr	x16, [x16, __ZZN5Eigen8internal16trig_reduce_hugeEfPiE11two_over_pi@GOTPAGEOFF]
	mov	x17, #11544
	movk	x17, #21572, lsl #16
	movk	x17, #8699, lsl #32
	movk	x17, #15385, lsl #48
	add	x0, sp, #16                     ; =16
	b	LBB45_3
LBB45_2:                                ;   in Loop: Header=BB45_3 Depth=1
	add	x10, x10, #4                    ; =4
	cmp	x10, #16                        ; =16
	b.eq	LBB45_5
LBB45_3:                                ; =>This Inner Loop Header: Depth=1
	ldr	s0, [x11, x10]
	fmov	s1, w12
	fcmp	s0, s1
	fabs	s1, s0
	fccmp	s0, s0, #1, ge
	fmov	s2, w13
	fccmp	s1, s2, #4, vc
	b.eq	LBB45_2
	fmov	w1, s0
	lsr	w2, w1, #23
	sub	w2, w2, #118                    ; =118
	mov	w3, #8388608
	bfxil	w3, w1, #0, #23
	and	w1, w2, #0x7
	lsl	w1, w3, w1
	lsr	w2, w2, #3
	sub	w3, w2, #1                      ; =1
	ldr	w3, [x16, w3, uxtw #2]
	add	w4, w2, #3                      ; =3
	ldr	w4, [x16, w4, uxtw #2]
	add	w2, w2, #7                      ; =7
	ldr	w2, [x16, w2, uxtw #2]
	mul	x2, x2, x1
	mul	x4, x4, x1
	mul	w1, w3, w1
	add	x1, x4, x1, lsl #32
	add	x1, x1, x2, lsr #32
	add	x2, x1, x14
	lsr	x3, x2, #62
	str	w3, [x15, x10]
	and	x2, x2, #0xc000000000000000
	sub	x1, x1, x2
	scvtf	d0, x1
	fmov	d1, x17
	fmul	d0, d0, d1
	fcvt	s0, d0
	str	s0, [x0, x10]
	b	LBB45_2
LBB45_5:
	ldp	q1, q0, [sp]
LBB45_6:
	lsr	x9, x9, #32
	shl.4s	v2, v1, #30
	movi.4s	v3, #64, lsl #24
	add.4s	v2, v2, v3
	movi.4s	v3, #128, lsl #24
	and.16b	v2, v2, v3
	movi.4s	v3, #1
	and.16b	v1, v1, v3
	fmul.4s	v3, v0, v0
	mov	w10, #29451
	movk	w10, #14284, lsl #16
	dup.4s	v4, w10
	mov	w10, #878
	movk	w10, #47798, lsl #16
	dup.4s	v5, w10
	mov	w10, #43678
	movk	w10, #15658, lsl #16
	dup.4s	v6, w10
	fmla.4s	v5, v4, v3
	fmla.4s	v6, v3, v5
	movi.4s	v4, #191, lsl #24
	fmla.4s	v4, v3, v6
	fmov.4s	v5, #1.00000000
	fmla.4s	v5, v3, v4
	mov	w10, #28874
	movk	w10, #47437, lsl #16
	dup.4s	v4, w10
	mov	w10, #34259
	movk	w10, #15368, lsl #16
	dup.4s	v6, w10
	cmeq.4s	v1, v1, #0
	fmla.4s	v6, v4, v3
	mov	w10, #43688
	movk	w10, #48682, lsl #16
	dup.4s	v4, w10
	fmla.4s	v4, v3, v6
	fmul.4s	v3, v3, v4
	fmla.4s	v0, v0, v3
	bit.16b	v0, v5, v1
	eor.16b	v0, v2, v0
	str	q0, [x8, x9]
	add	sp, sp, #64                     ; =64
	ret
```
