#pragma once
#include "./protocol.h"

struct CameraClient;

struct SimCameraServer : AsyncWebsocket, AllocTrackingMixin<SimCameraServer> {

  string localHostName;
  map<string, shared_ptr<CameraClient>> clients;
  vector<tuple<asio::ip::udp::endpoint, string, double>> lightRequests;
  asio::ip::udp::socket lightsUdp{io_context};
  asio::ip::udp::resolver resolver{io_context};

  SimCameraServer(asio::ip::tcp::socket &&_sock);
  ~SimCameraServer();

  void onRx(cereal::BinaryInputArchive &rx) override;
  void onError(string_view error) override;
  void onEof() override;

  void txQueuedFrames(CameraClient *client);
  void txLightRequests();

  shared_ptr<CameraClient> mkCameraClient(RemoteCameraConfig &cameraConfig);
  void stopCameras();

  static bool isValidName(string const &a);

};

