#include "./camera_client.h"

struct AxisWebClient;

struct AxisCameraClient : CameraClient {

  bool running = false;
  string camera_host;
  shared_ptr<AxisWebClient> webClient;

  AxisCameraClient(RemoteCameraConfig const &_config)
    : CameraClient(_config)
  {
    if (auto it = assoc_string_view(cameraConfig.apiProps, "camera_host")) {
      camera_host = *it;
    }
  }

  ~AxisCameraClient()
  {
    pleaseStop();
  }

  bool isRunning() override { return running; }
  void pleaseStop() override;

  void start();

};

struct AxisWebClient : AsyncTcpStream {

  AxisCameraClient *parent;
  string boundary;

  AxisWebClient(AxisCameraClient *_parent)
    : AsyncTcpStream(),
      parent(_parent)
  {
    label = "AxisWebClient";
    verbose = parent->verbose;
  }

  void startAxisWebClient(string const &host)
  {
    startClient(host, "80");
  }

  virtual void onClientConnect() override
  {
    L() << label << ": connected";

    string httpCmd = "GET "s + string(assoc_string_view(parent->cameraConfig.apiProps, "url", "/")) + " HTTP/1.1\r\n"s;
    httpCmd += "Accept: */*\r\n"s;
    if (auto auth = assoc_string_view(parent->cameraConfig.apiProps, "auth_header")) {
      httpCmd += "Authorization: "s + string(*auth) + "\r\n"s;
    }
    httpCmd += "Host: "s + parent->camera_host + "\r\n"s;
    httpCmd += "\r\n"s;

    txRaw(httpCmd);
    asyncRxHeader();
  }

  void asyncRxHeader()
  {
    asyncRxUntil("\r\n\r\n", [this](string const &rsp) {
      if (!startsWith(rsp, "HTTP/1.0 200 OK\r\n")) {
        L() << label << ": got response " << rsp;
        sock.close();
        return; // end rx
      }

      auto headers = parseHeaders(rsp, 1);

      string ct = headers["content-type"];
      string lookfor = "multipart/x-mixed-replace; boundary=";
      auto lookforPos = ct.find("multipart/x-mixed-replace; boundary=");
      if (lookforPos == string::npos) {
        L() << label << ": bad content-type " << ct;
        sock.close();
        return; // end rx
      }
      boundary = ct.substr(lookforPos + lookfor.size());
      if (verbose >= 1) L() << "Found boundary=" << shellEscape(boundary) << " in " << shellEscape(ct);

      asyncRxMultipartBoundary("--"s + boundary + "\r\n");
    });
  }

  map<string, string> parseHeaders(string const &rsp, int skipFirst=0)
  {
    map<string, string> ret;
    for (auto &l : splitString(rsp, "\r\n")) {
      if (skipFirst > 0) {
        skipFirst--;
        continue;
      }
      if (l.empty()) continue;
      auto colonPos = l.find(':');
      if (colonPos == string::npos) {
        L() << label << ": bad header line " << l;
        continue;
      }
      auto key = l.substr(0, colonPos);
      auto valPos = colonPos+1;
      while (valPos < l.size() && l[valPos] == ' ') valPos++;
      auto value = l.substr(valPos);

      ret[canonHeader(key)] = value;
    }
    return ret;
  }

  void asyncRxMultipartBoundary(string const &separator)
  {
    asyncRxUntil(separator, [this](string const &filler) {
      if (verbose >= 2) L() << label << ": filler=" << shellEscape(filler);
      asyncRxUntil("\r\n\r\n", [this](string const &mpHeadStr) {
        if (verbose >= 2) L() << label << ": mpHeadStr=" << shellEscape(mpHeadStr);
        auto headers = parseHeaders(mpHeadStr);

        string clStr = headers["content-length"s];
        if (clStr.empty()) {
          L() << label << ": no content-length";
          sock.close();
          return; // end rx
        }
        size_t cl = std::stol(clStr.c_str());
        if (verbose >= 2) L() << label << ": content-length=" << cl;
        asyncRxCount(cl, [this](string const &s) {

          VideoFrame frame;
          frame.format = VFF_JPEG;
          frame.width = 640.0; // FIXME
          frame.height = 480.0;
          frame.orientation = parent->base_orientation;
          rotateBlobChunkId(parent->curChunkId);
          frame.blobRef = mkBlob(parent->curChunkId, (U8 *)s.data(), (U64)s.size());

          if (parent) {
            std::lock_guard lock(parent->avMutex);
            parent->videoQueue.emplace_back(realtime(), frame);
            parent->onNewFrame();
          }
          asyncRxMultipartBoundary("\r\n--"s + boundary + "\r\n");
        });
      });
    });
  }

  string canonHeader(string const &s)
  {
    string ret;
    for (auto c : s) {
      if (c >= 'A' && c <= 'Z') {
        ret.push_back(c + ('a' - 'A'));
      }
      else {
        ret.push_back(c);
      }
    }
    return ret;
  }

};

shared_ptr<CameraClient>
CameraClient::mkAxisCameraClient(RemoteCameraConfig const &config)
{
  auto c = make_shared<AxisCameraClient>(config);
  c->start();

  return static_pointer_cast<CameraClient>(c);
}

void AxisCameraClient::pleaseStop()
{
  CameraClient::pleaseStop();
  if (webClient) {
    webClient->stop();
    webClient = nullptr;
  }
  running = false;
}

void AxisCameraClient::start()
{
  if (isRunning()) return;

  L() << "Start AxisWebClient(" << cameraConfig.camName << ")\n";
  webClient = make_shared<AxisWebClient>(this);
  webClient->startAxisWebClient(cameraConfig.camName);

  running = true;
}
