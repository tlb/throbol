#include "./sim_robot_client.h"
#include "../src/api.h"
#include "../src/emit.h"
#include "./protocol.h"

SimRobotClient::SimRobotClient(shared_ptr<CompiledSheet> const &_compiled)
  : compiled(_compiled)
{
  label = "SimRobotClient";
  replayBuffer = make_shared<ReplayBuffer>();
  for (int i = 0; i < API_KBD_NKEYS; i++) lastTxKbd[i] = 0;
}

SimRobotClient::~SimRobotClient()
{
}

void SimRobotClient::stopRobot()
{
  if (ready) {
    txCereal(RPC_stopRobot);
  }
}

void SimRobotClient::onClientConnect()
{
  txCereal(RPC_acquireRobot, robotName, remoteAuth);
}

void SimRobotClient::onError(string_view error)
{
  state = S_error;
}

void SimRobotClient::onEof()
{
  if (state != S_done) {
    state = S_error;
  }
}

bool SimRobotClient::running() {
  return state == S_running;
}

bool SimRobotClient::done() {
  return state == S_done;
}

bool SimRobotClient::error() {
  return state == S_error;
}

void SimRobotClient::onRx(cereal::BinaryInputArchive &rx)
{
  RemoteProtoCmd cmd = RPC_none;
  rx(cmd);
  if (verbose >= 2) L() << label << ": cmd=" << cmd;
  if (cmd == RPC_appendReplay) {
    std::unique_lock lock(replayBuffer->replayMutex);
    while (1) {
      bool more = false;
      rx(more);
      if (!more) break;

      string apiInstance;
      U32 valStreamPos = 0;
      rx(apiInstance, valStreamPos);
      auto &replays = replayBuffer->replayByApiInstance[apiInstance];
      if (!(valStreamPos < replays.size())) {
        replays.resize(size_t(valStreamPos)+1);
      }
      rx(replays[valStreamPos]);
      uiPollLive = true;
      if (verbose >= 2) L() << label << ": appendReplay " << valStreamPos;
    }
  }
  else if (cmd == RPC_acquireRobotOk) {
    txLoadSheet();
    state = S_compiling;
  }
  else if (cmd == RPC_loadSheetOk) {
    YAML::Node remoteApiSchemaYaml;
    vector<string> errors;
    rx(errors, remoteApiSchemaYaml);

    if (!errors.empty()) {
      W() << label << ": loadSheetOk " << errors;
      state = S_error;
    }
    else {
      if (verbose >= 1) L() << label << ": loadSheetOk " << errors;
      auto localApiSchemaYaml = compiled->getApiSchema();
      ostringstream l;
      if (!checkSchemaMatch(localApiSchemaYaml, remoteApiSchemaYaml, l)) {
        Wred() << l.str();
        state = S_error;
      }
      else {
        if (verbose >= 1) W() << label << ": Schema matches exactly, starting robot";
        txCereal(RPC_startRobot);
        state = S_starting;
      }
    }
  }
  else if (cmd == RPC_loadSheetFail) {
    if (verbose >= 0) W() << label << ": loadSheetFail";
    state = S_error;
    stop();
    return;
  }
  else if (cmd == RPC_startRobotOk) {
    R startTime = 0.0;
    rx(startTime);
    if (verbose >= 0) W() << label << ": startRobotOk " << startTime;
    replayBuffer->startTime = startTime;
    state = S_running;
  }
  else if (cmd == RPC_robotStopped) {
    if (verbose >= 0) W() << label << ": robotStopped";
    // WRITEME: snarf any more data that wasn't sent in real time
    state = S_done;
    stop();
    return;
  }
  else if (cmd == RPC_chunkList) {
    vector<BlobRef> newChunkList;
    rx(newChunkList);
    if (verbose >= 1) L() << "chunkList " << newChunkList;
    for (auto &it : newChunkList) {
      chunkList.push_back(it);
    }
  }
  else if (cmd == RPC_log) {
    string msg;
    rx(msg);
    Wred()  << label << ": " << msg;
  }
  else {
    W() << label << ": unknown cmd " << cmd;
  }
}


void SimRobotClient::txLoadSheet()
{
  ostringstream txs(std::ios::binary);
  {
    cereal::BinaryOutputArchive ar(txs);
    ar(RPC_loadSheet);

    if (verbose >= 1) L() << label << ": acquireRobotOk ";

    compiled->sh.updatePagesYamlFromSheet();
    ar(U32(compiled->sh.nPages));
    for (PageIndex page = 0; page < compiled->sh.nPages; page++) {
      ar(compiled->sh.fileNameByPage.at(page));
      YAML::Emitter contents;
      compiled->sh.dumpPage(contents, page);
      ar(string(contents.c_str(), contents.size()));
    }
    ar(compiled->getExpectedApiVersions());
  }
  txPkt(txs);
}

void SimRobotClient::txUpdateCellParams(CellIndex cell)
{
  auto &sh = compiled->sh;
  auto &cellParams = sh.paramsByCell[cell];
  if (cellParams.empty()) return;
  vector<F> values;
  for (auto param : cellParams) {
    values.push_back(sh.params.valueByParam(param));
  }
  string cellName = sh.nameByCell[cell];

  L() << "tx updateCellParams: " << cellName << " " << values;
  txCereal(RPC_updateCellParams, cellName, values);
}


void SimRobotClient::txUpdateKbd(bool *down)
{
  if (state == S_running) {
    bool needTx = false;
    for (int i = 0; i < API_KBD_NKEYS; i++) {
      if (down[i] != lastTxKbd[i]) {
        needTx = true;
        lastTxKbd[i] = down[i];
      }
    }
    if (needTx) {
      txCereal(RPC_updateKbd, lastTxKbd);
    }
  }
}

int SimRobotClient::getRunningTick()
{
  int ret = 0;
  bool retSet = false;
  std::shared_lock lock(replayBuffer->replayMutex);
  for (auto const &api : compiled->apis) {
    auto &blobs = replayBuffer->replayByApiInstance[api->apiInstance];
    if (!blobs.empty()) {
      int apiEnd = max(0, int(blobs.size()) - 1);
      ret = retSet ? min(ret, apiEnd) : apiEnd;
      retSet = true;
    }
  }
  return ret;
}

ostream & operator <<(ostream &s, SimRobotClient::State const &a)
{
  switch (a) {
    case SimRobotClient::S_idle: return s << "idle";
    case SimRobotClient::S_acquiring: return s << "acquiring";
    case SimRobotClient::S_compiling: return s << "compiling";
    case SimRobotClient::S_starting: return s << "starting";
    case SimRobotClient::S_running: return s << "running";
    case SimRobotClient::S_done: return s << "done";
    case SimRobotClient::S_error: return s << "error";
    default: return s << "(" << int(a) << ")";
  }
}

