#include "./camera_client.h"

CameraClient::CameraClient(RemoteCameraConfig const &_config) 
  : cameraConfig(_config)
{
  label = cameraConfig.camName;
  base_orientation = mat4::Identity();
  if (auto it = assoc_mat4(cameraConfig.apiProps, "base_orientation")) {
    base_orientation = *it;
  }
  verbose = int(assoc_F(cameraConfig.apiProps, "verbose", 0));
  curChunkId = mkBlobChunkId();
}

CameraClient::~CameraClient()
{
  L() << label << ": destructed";
}

void CameraClient::pleaseStop()
{
  stopRequested = true;
  onNewFrameHandlers.clear();
};

void CameraClient::onNewFrame()
{  
  for (auto &f : onNewFrameHandlers) {
    f(this);
  }
}
