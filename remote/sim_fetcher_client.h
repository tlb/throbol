#pragma once
#include "../src/defs.h"
#include "./protocol.h"
#include "../src/api.h"

struct SimFetcherClient : AsyncWebsocket {
  
  RemoteCameraConfig cameraConfig;
  int fetchesOutstanding = 0;
  vector<BlobRef> blobFilesWanted;

  inline static unordered_map<BlobRef, BlobRef *> blobFetchSent;
  inline static map<string, shared_ptr<SimFetcherClient>> clientByHost;

  SimFetcherClient();
  virtual ~SimFetcherClient();

  void onRx(cereal::BinaryInputArchive &rx) override;

  void txFetchBlob(BlobRef const &blobRef);
  void txFetchBlobFile(BlobRef const &blobRef);
  void maybeFetchSome();

  static SimFetcherClient *getFetcher(string const &blobHostName);
  static void fetchBlob(BlobRef const &blobRef, BlobRef *writeTo);
  static void fetchBlobFiles();

  static void fetchBlobFile(BlobRef blobRef);
  static void closeAll();
};
