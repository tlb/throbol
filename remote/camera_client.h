#pragma once
#include "./protocol.h"

struct CameraClient : enable_shared_from_this<CameraClient>, AllocTrackingMixin<CameraClient> {

  RemoteCameraConfig cameraConfig;
  string label;

  U64 curChunkId = 0;
  int verbose = 0;
  atomic<bool> stopRequested = false;

  mat4 base_orientation;
  std::mutex avMutex;
  deque<pair<R, VideoFrame>> videoQueue;
  deque<pair<R, AudioFrame>> audioQueue;

  vector<function<void(CameraClient *src)>> onNewFrameHandlers;

  CameraClient(RemoteCameraConfig const &_config);
  virtual ~CameraClient();

  virtual void pleaseStop();
  virtual bool isRunning() = 0;

  void onNewFrame();

  static shared_ptr<CameraClient>
  mkAxisCameraClient(RemoteCameraConfig const &config);
  static shared_ptr<CameraClient>
  mkPylonCameraClient(RemoteCameraConfig const &config);
  static shared_ptr<CameraClient>
  mkV4lCameraClient(RemoteCameraConfig const &config);
  static shared_ptr<CameraClient>
  mkAlsaAudioClient(RemoteCameraConfig const &config);

  static void enumeratePylonCameras();
};
