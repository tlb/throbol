#include "./camera_client.h"
#include "../src/test_utils.h"

TEST_CASE("AxisCameraClient", "[.][remote][axis]")
{
  RemoteCameraConfig config;
  config.camName = "axis8";
  config.camType = "axis";
  config.spoolHost = "localhost";
  config.cellName = "test";

  Arena pool;

  Assocp apiProps = nullptr;
  apiProps = pool.mkAssoc(apiProps, "verbose", BoxedPtr(pool.mk<F>(0.0f)));
  apiProps = pool.mkAssoc(apiProps, "camera_host", BoxedPtr(pool.mk<string_view>("axis8")));
  apiProps = pool.mkAssoc(apiProps, "url", BoxedPtr(pool.mk<string_view>("/axis-cgi/mjpg/video.cgi?compression=30&rotation=0&resolution=320x200")));

  config.apiProps = toShared(apiProps);



  auto cc = CameraClient::mkAxisCameraClient(config);
  io_context.run_for(std::chrono::milliseconds(1000));
  L() << "Got " << cc->videoQueue.size() << " frames";
  CHECK(cc->videoQueue.size() >= 5);
  cc->pleaseStop();
  io_context.run();
  CHECK(!cc->isRunning());
}

TEST_CASE("RemoteCameraConfig", "[remote][axis]")
{
  RemoteCameraConfig config;
  config.camName = "axis8";
  config.camType = "axis";
  config.spoolHost = "localhost";
  config.cellName = "test";

  Arena pool;

  Assocp apiProps = nullptr;
  apiProps = pool.mkAssoc(apiProps, "verbose", BoxedPtr(pool.mk<F>(1.0f)));
  apiProps = pool.mkAssoc(apiProps, "camera_host", BoxedPtr(pool.mk<string_view>("axis8")));
  apiProps = pool.mkAssoc(apiProps, "url", BoxedPtr(pool.mk<string_view>("someurl")));
  Assocp cc = nullptr;
  cc = pool.mkAssoc(cc, "foo", BoxedPtr(pool.mk<string_view>("bar")));
  cc = pool.mkAssoc(cc, "buz", BoxedPtr(pool.mk<string_view>("wug")));
  apiProps = pool.mkAssoc(apiProps, "cc", BoxedPtr(pool.mk<Assocp>(cc)));

  mat4 testmat;
  testmat << 
    0.0f,1.0f,2.0f,3.0f,
    4.0f,5.0f,6.0f,7.0f,
    8.0f,9.0f,10.0f,11.0f,
    12.0f,13.0f,14.0f,15.0f;

  apiProps = pool.mkAssoc(apiProps, "orientation", BoxedPtr(pool.mk<mat4>(testmat)));


  {
    auto a = assoc_string_view(apiProps, "camera_host", "x");
    CHECK((a == "axis8"));
    auto b = assoc_string_view(apiProps, "url", "x");
    CHECK((b == "someurl"));
    auto c = assoc_F(apiProps, "verbose", -5.0f);
    CHECK((c == 1.0f));
    auto d = assoc_mat4(apiProps, "orientation");
    REQUIRE(d);
    CHECK((*d == testmat));
  }
  {
    auto a = assoc_assoc(apiProps, "cc");
    CHECK(!!a);
    auto b = assoc_string_view(a, "foo", "x");
    CHECK((b == "bar"));
    auto c = assoc_string_view(a, "buz", "x");
    CHECK((c == "wug"));
  }


  config.apiProps = toShared(apiProps);
  //L() << config.apiProps;

  {
    auto a = assoc_string_view(config.apiProps, "camera_host", "foo");
    CHECK((a == "axis8"));
    auto b = assoc_string_view(config.apiProps, "url", "x");
    CHECK((b == "someurl"));
    auto c = assoc_F(config.apiProps, "verbose", -5.0f);
    CHECK((c == 1.0f));
    auto d = assoc_mat4(config.apiProps, "orientation");
    REQUIRE(d);
    CHECK((*d == testmat));
  }
  {
    auto a = assoc_assoc(config.apiProps, "cc");
    CHECK(!!a);
    auto b = assoc_string_view(a, "foo", "x");
    CHECK((b == "bar"));
    auto c = assoc_string_view(a, "buz", "x");
    CHECK((c == "wug"));
  }

  ostringstream txs(std::ios::binary);
  {
    cereal::BinaryOutputArchive arch(txs);
    arch(config);
  }

  istringstream rxs(txs.str(), std::ios::binary);
  RemoteCameraConfig config2;
  {
    cereal::BinaryInputArchive arch(rxs);
    arch(config2);
  }

  {
    auto a = assoc_string_view(config2.apiProps, "camera_host", "foo");
    CHECK((a == "axis8"));
    auto b = assoc_string_view(config2.apiProps, "url", "x");
    CHECK((b == "someurl"));
    auto c = assoc_F(config2.apiProps, "verbose", -5.0f);
    CHECK((c == 1.0f));
    auto d = assoc_mat4(config2.apiProps, "orientation");
    REQUIRE(d);
    CHECK((*d == testmat));
  }
  {
    auto a = assoc_assoc(config2.apiProps, "cc");
    CHECK(!!a);
    auto b = assoc_string_view(a, "foo", "x");
    CHECK((b == "bar"));
    auto c = assoc_string_view(a, "buz", "x");
    CHECK((c == "wug"));
  }


}

