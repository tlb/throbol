#include "./sim_camera_server.h"
#include "./camera_client.h"
#include "./protocol.h"

SimCameraServer::SimCameraServer(asio::ip::tcp::socket &&_sock)
  : AsyncWebsocket(std::move(_sock))
{
  label = "SimCameraServer";
  localHostName = asio::ip::host_name();
}

SimCameraServer::~SimCameraServer()
{
  if (lightsUdp.is_open()) {
    lightsUdp.close();
  }
  assert(clients.empty());
  stopCameras();
  clearBlobCache();
}

void SimCameraServer::txLightRequests()
{
  if (!lightsUdp.is_open()) {
    lightsUdp.open(asio::ip::udp::v4());
  }
  double now = realtime();
  for (auto &[ctlHostAddr, lightNameCmd, lastTime] : lightRequests) {
    if (now - lastTime > 1.0) {
      ostringstream txs(std::ios::binary);
      {
        cereal::BinaryOutputArchive tx(txs);
        tx(RPC_lightson, lightNameCmd);
      }
      lightsUdp.send_to(asio::buffer(txs.str()), ctlHostAddr);
      lastTime = now;
    }
  }
}

shared_ptr<CameraClient> SimCameraServer::mkCameraClient(RemoteCameraConfig &cameraConfig)
{
  L() << "Create client " << cameraConfig.camName;
  shared_ptr<CameraClient> client;
  if (cameraConfig.camType == "pylon") {
#ifdef USE_PYLON
    client = CameraClient::mkPylonCameraClient(cameraConfig);
#endif
  }
  else if (cameraConfig.camType == "v4l") {
#ifdef USE_V4L
    client = CameraClient::mkV4lCameraClient(cameraConfig);
#endif
  }
  else if (cameraConfig.camType == "axis") {
    client = CameraClient::mkAxisCameraClient(cameraConfig);
  }
  else if (cameraConfig.camType == "alsa") {
    // FIXME client = CameraClient::mkAlsaAudioClient(cameraConfig);
  }

  if (!client) {
    txCereal(RPC_log, string("Bad camera type " + cameraConfig.camType));
    return nullptr;
  }

  rotateBlobChunkId(client->curChunkId);

  client->onNewFrameHandlers.push_back([this, keepalive=shared_from_this()](CameraClient *src) {
    asio::post(io_context, [this, keepalive, src]() {
      txQueuedFrames(src);
      txLightRequests();
    });
  });

  if (auto lightsStr = assoc_string_view(client->cameraConfig.apiProps, "lights")) {
    auto lights = splitChar(string(*lightsStr), ' ');
    for (auto &light : lights) {
      auto colonPos = light.find(':');
      if (colonPos != string::npos) {
        auto ctlHost = light.substr(0, colonPos);
        auto lightName = light.substr(colonPos+1);

        resolver.async_resolve(ctlHost.c_str(), "6916", 
          [this, keepalive=shared_from_this(), ctlHost, lightName](
            boost::system::error_code const &error, auto eps) {
            if (error) {
              L() << "resolve(" << shellEscape(ctlHost) << ", 6916): " << error;
              return;
            }
            auto ep0 = eps.begin()->endpoint();
            lightRequests.emplace_back(ep0, lightName, 0.0);
          }
        );
      }
    }
  }

  return client;
}

void SimCameraServer::onRx(cereal::BinaryInputArchive &rx)
{
  RemoteProtoCmd cmd = RPC_none;
  rx(cmd);
  if (cmd == RPC_record) {
    RemoteCameraConfig cameraConfig;
    rx(cameraConfig);

    string clientName = cameraConfig.camName;
    auto &client = clients[clientName];
    if (!client) {
      client = mkCameraClient(cameraConfig);
    }
    txLightRequests();
  }
  else if (cmd == RPC_fetchBlob) {
    BlobRef blobRef;
    rx(blobRef);
    Blob data;
    if (!loadBlob(blobRef, data)) {
      txCereal(RPC_fetchBlobFail, blobRef);
      return;
    }
    txCereal(RPC_fetchBlobOk, blobRef, data);
  }
  else if (cmd == RPC_fetchBlobFile) {
    U64 chunkId = 0;
    rx(chunkId);

    if (!failed) {
      Blob data;
      if (!loadBlobFile(chunkId, data)) {
        txCereal(RPC_fetchBlobFileFail, chunkId);
        return;
      }

      txCereal(RPC_fetchBlobFileOk, chunkId, data);
    }
  }
  else if (cmd == RPC_fetchTraceInfo) {
    // WRITEME
  }
  else if (cmd == RPC_fetchTraceSeq) {
    // WRITEME
  }
  else if (cmd == RPC_log) {
    string msg;
    rx(msg);
    L() << label << ": " << msg;
  }
  else {
    L() << label << ": unknown cmd " << cmd;
  }
}

void SimCameraServer::txQueuedFrames(CameraClient *client)
{
  std::unique_lock lock(client->avMutex);
  if (client->videoQueue.empty() && client->audioQueue.empty()) return;

  R now = realtime();
  while (!client->videoQueue.empty()) {
    auto x = std::move(client->videoQueue.front());
    client->videoQueue.pop_front();
    lock.unlock();
    if (!failed) txCereal(RPC_videoFrame, now, x.first, x.second, localHostName);
    lock.lock();
  }

  while (!client->audioQueue.empty()) {
    auto x = std::move(client->audioQueue.front());
    client->audioQueue.pop_front();
    lock.unlock();
    if (!failed) txCereal(RPC_audioFrame, now, x.first, x.second, localHostName);
    lock.lock();
  }
}

void SimCameraServer::onError(string_view error)
{
  stopCameras();
}

void SimCameraServer::onEof()
{
  L() << label << ": EOF";
  stopCameras();
}

void SimCameraServer::stopCameras()
{
  for (auto it : clients) {
    if (it.second) {
      it.second->pleaseStop();
    }
  }
  clients.clear();
}

bool SimCameraServer::isValidName(string const &a)
{
  if (a.empty()) return false;
  for (auto &it : a) {
    if (!isalnum(it) && it != '_' && it != '-' && it != '.') return false;
  }
  return true;
}
