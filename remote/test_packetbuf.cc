#include "../src/test_utils.h"
#include "common/packetbuf.h"
#include "common/packetbuf_types.h"

struct MyRecord
{
  int x, y;
  vector<float> foo;
  
  template <class Archive>
  void serialize( Archive & ar )
  {
    ar( x, y, foo );
  }
};

TEST_CASE("packet out", "[.][perf][env]")
{
  perfReport("packet speed 3 floats", [] {
    MyRecord myData {1, 2, {1.0f, 2.0f, 3.14f}};
    for (int iter = 0; iter < 1000; iter++) {

      packet wr;
      wr.add(myData.x);
      wr.add(myData.y);
      wr.add(myData.foo);
      if (iter == 0) {
        CHECK(wr.size() == 24);
      }
    }
  }, 1000);


  perfReport("packet speed 1000 floats", [] {
    MyRecord myData {1, 2, vector<float>(1000, 3.14f)};
    for (int iter = 0; iter < 1000; iter++) {

      packet wr;
      wr.add(myData.x);
      wr.add(myData.y);
      wr.add(myData.foo);
      if (iter == 0) {
        CHECK(wr.size() == 4012);
      }
    }
  }, 1000);
}
