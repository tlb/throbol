#pragma once

#ifdef USE_PYLON
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Woverloaded-virtual"
#pragma clang diagnostic ignored "-Wmacro-redefined"
#pragma clang diagnostic ignored "-Wunknown-warning-option"
#pragma clang diagnostic ignored "-Wunused-variable"
#include <pylon/PylonIncludes.h>       // file:///../../ur-doc/pylon/C++/pylon_programmingguide.html
#include <pylon/usb/BaslerUsbCamera.h> // file:///../../ur-doc/Basler%20USB/files/index.htm
#pragma clang diagnostic pop
#endif
