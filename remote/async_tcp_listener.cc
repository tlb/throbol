#include "./cereal_box.h"

// ----------

AsyncTcpListener::AsyncTcpListener(
  function<void (asio::ip::tcp::socket &&, AsyncTcpListener *)> &&spawn)
  : acceptor(io_context),
    spawn(std::move(spawn))
{
}

AsyncTcpListener::~AsyncTcpListener()
{
  acceptor.close();
}


int AsyncTcpListener::startTcpListener(string const &bindAddr, string const & bindPort)
{
  label = "Listener on "s + bindAddr + ":" + bindPort;
  asio::ip::tcp::resolver resolver(io_context);
  boost::system::error_code resolveError;
  // leave this as sync, since it's only used at server startup
  // and async causes trouble with tests
  auto eps = resolver.resolve(asio::ip::tcp::v4(), bindAddr, bindPort, resolveError);
  if (resolveError) {
    L() << label << ": resolve: " << resolveError;
    return -1;
  }
  auto ep = eps.begin()->endpoint();
  acceptor.open(asio::ip::tcp::v4());
  acceptor.set_option(asio::ip::tcp::acceptor::reuse_address(true));
  boost::system::error_code bindError;
  acceptor.bind(ep, bindError);
  if (bindError) {
    L() << label << ": bind(" << ep << "): " << bindError.message();
    return -1;
  }
  acceptor.listen();
  L() << label << ": listening on " << ep;
  asyncAccept();
  return 0;
}

void AsyncTcpListener::asyncAccept()
{
  socket.emplace(io_context);

  acceptor.async_accept(*socket,
    [this, keepalive=shared_from_this()] (boost::system::error_code error) {
      if (error) {
        L() << label << ": " << error.message();
        acceptor.close();
        return;
      }
      socket->set_option(asio::ip::tcp::no_delay(true));
      spawn(std::move(*socket), this);
      if (!acceptOnce) asyncAccept();
    });
}
