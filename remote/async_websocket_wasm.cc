#include "./cereal_box.h"


AsyncWebsocket::AsyncWebsocket()
{
  label = "AsyncWebsocket";
  verbose = 1;
}

AsyncWebsocket::~AsyncWebsocket()
{
  stop();
  label += "(deleted)";
}

void AsyncWebsocket::onClientConnect()
{
  // override me
}

void AsyncWebsocket::onRx(cereal::BinaryInputArchive &rx)
{
  // override me
}

void AsyncWebsocket::onEof()
{
  // override me
}


void AsyncWebsocket::onError(string_view error)
{
  // override me. A message will already have been printed and failed=true
}

void AsyncWebsocket::startWebsocketClient(string const &host, string const &port, string const &uri)
{
  label += " to " + host + ":" + port;

  string fullurl = "ws://" + host + ":" + port + uri;

  L() << label << ": startWebsocketClient to " << fullurl;

  EmscriptenWebSocketCreateAttributes ws_attrs = {
    fullurl.c_str(),
    "binary",
    EM_FALSE
  };

  ws = emscripten_websocket_new(&ws_attrs);
  if (ws < 0) {
    onError("Failed to create websocket");
    return;
  }

  internalKeepalive = shared_from_this();
  emscripten_websocket_set_onopen_callback(ws, (void *)this, 
    [](int eventType, const EmscriptenWebSocketOpenEvent *ev, void *userData) -> int {
      auto this1 = reinterpret_cast<AsyncWebsocket *>(userData);
      if (this1->verbose >= 1) L() << this1->label << ": connected";
      this1->ready = true;
      this1->onClientConnect();
      return 0;
    }
  );

  emscripten_websocket_set_onerror_callback(ws, this,
    [](int eventType, const EmscriptenWebSocketErrorEvent *ev, void *userData) -> int {
      auto this1 = reinterpret_cast<AsyncWebsocket *>(userData);
      L() << this1->label << ": error " << eventType;
      this1->onError("unknown");
      this1->failed = true;
      return 0;
    }
  );

  emscripten_websocket_set_onclose_callback(ws, this,
    [](int eventType, const EmscriptenWebSocketCloseEvent *ev, void *userData) -> int {
      auto this1 = reinterpret_cast<AsyncWebsocket *>(userData);
      if (this1->verbose >= 1) L() << this1->label << ": EOF";
      this1->onEof();
      this1->internalKeepalive = nullptr;
      return 0;
    }
  );

  emscripten_websocket_set_onmessage_callback(ws, this,
    [](int eventType, const EmscriptenWebSocketMessageEvent *ev, void *userData) -> int {
      auto this1 = reinterpret_cast<AsyncWebsocket *>(userData);
      if (this1->verbose >= 2) L() << this1->label << ": rx len=" << ev->numBytes;

      string rxData((char *)ev->data, (size_t)ev->numBytes);
      istringstream rxStream(rxData);
      cereal::BinaryInputArchive rxArch(rxStream);

      this1->onRx(rxArch);
      return 0;
    }
  );

}

void AsyncWebsocket::stop()
{
  if (ws != -1) {
    if (verbose >= 1) L() << label << ": close";
    emscripten_websocket_close(ws, 1000, "stop");
    ws = -1;
  }
}

void AsyncWebsocket::txPkt(ostringstream &txs)
{
  auto s = txs.str();
  if (verbose >= 2) L() << label << ": tx len=" << s.size() << (failed ? " dropped" : "");
  if (failed) return; // but !ready is fine, packets will be queued until it starts

  auto res = emscripten_websocket_send_binary(ws, s.data(), (uint32_t)s.size());
  if (res != EMSCRIPTEN_RESULT_SUCCESS) {
    L() << label << ": send_binary: " << res;
    failed = true;
  }
}
