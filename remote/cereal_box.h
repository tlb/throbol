#pragma once
#include "../src/defs.h"
#include "../src/vbmainloop.h"
#if defined(__EMSCRIPTEN__)
#include <emscripten/websocket.h>
#else
#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>
#endif
#include <cereal/types/array.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/tuple.hpp>
#include <cereal/types/utility.hpp>
#include <cereal/archives/binary.hpp>
#include "../src/yamlio.h"


#if !defined(__EMSCRIPTEN__)

namespace beast = boost::beast;
namespace http = beast::http;
namespace websocket = beast::websocket;
namespace asio = boost::asio;
using boost::system::error_code;
#endif


#if !defined(__EMSCRIPTEN__)

struct AsyncTcpStream;

struct AsyncTcpListener : enable_shared_from_this<void> {
  
  string label;
  int verbose = 0;
  bool acceptOnce = false;

  AsyncTcpListener(function<void (asio::ip::tcp::socket &&, AsyncTcpListener *)> &&spawn);
  virtual ~AsyncTcpListener();

  int startTcpListener(string const &bindAddr, string const &bindPort);
  void asyncAccept();

  asio::ip::tcp::acceptor acceptor;
  std::optional<asio::ip::tcp::socket> socket;
  function<void (asio::ip::tcp::socket &&, AsyncTcpListener *)> spawn;

};

/*
  Start a session of the given type.
  Note: args are captured by value. Use a shared_ptr or something
  if you don't want a copy
  Could do perfect forwarding with https://stackoverflow.com/questions/47496358/c-lambdas-how-to-capture-variadic-parameter-pack-from-the-upper-scope
*/
template<typename SessionClass, typename ...Args>
auto sessionStarter(Args ...args)
{
  return [args...](asio::ip::tcp::socket &&_sock, AsyncTcpListener *parent) {
    auto s = make_shared<SessionClass>(std::move(_sock), args...);
    s->verbose = parent->verbose;
    s->onServerConnect();
  };
}

struct AsyncTcpStream : enable_shared_from_this<AsyncTcpStream> {

  string label;
  deque<string> txq;
  asio::streambuf rxStreambuf;
  asio::ip::tcp::socket sock;
  string rxBody;
  int verbose = 0;
  bool ready = false, failed = false, txActive = false;

  AsyncTcpStream();
  AsyncTcpStream(asio::ip::tcp::socket &&_sock);
  virtual ~AsyncTcpStream();

  virtual void startClient(string const &rHost, string const &rPort);
  virtual void onClientConnect(); // when a client complete a connection
  virtual void onServerConnect(); // when a server is connected to
  virtual void onError(string_view error);
  virtual void onEof();
  virtual void stop();
  void asyncRxUntil(string const &separator, std::function<void(string const &s)> &&onRx);
  void asyncRxCount(size_t toRead, std::function<void(string const &s)> &&onRx);

  void txRaw(string const &it);
  void txRaw(string &&it);

  void txFlush();

};

struct AsyncUdpSocket : enable_shared_from_this<AsyncUdpSocket> {

  string label;
  asio::ip::udp::socket sock;
  asio::ip::udp::endpoint rxSrc;
  string rxBuf;
  int verbose = 0;

  AsyncUdpSocket();
  virtual ~AsyncUdpSocket();

  virtual void stop();
  virtual int startUdpServer(string const &bindAddr, string const &bindPort);
  virtual void onServerBound();
  void asyncRxCereal(std::function<void(cereal::BinaryInputArchive &rx, asio::ip::udp::endpoint const &src)> &&onRx);

};
#endif

struct AsyncWebsocket : enable_shared_from_this<AsyncWebsocket> {

  string label;
#if defined(__EMSCRIPTEN__)
  EMSCRIPTEN_WEBSOCKET_T ws = -1;
  shared_ptr<AsyncWebsocket> internalKeepalive;
#else
  asio::ip::tcp::resolver resolver{io_context};
  beast::websocket::stream<beast::tcp_stream> ws;
  asio::streambuf wsRxBuf;
  deque<string> wsTxQ;
  bool txShutdownReq = false, txActive = false;
#endif
  bool ready = false, failed = false;
  int verbose = 0;

#if !defined(__EMSCRIPTEN__)
  AsyncWebsocket(asio::ip::tcp::socket &&_sock);
#endif
  AsyncWebsocket();
  virtual ~AsyncWebsocket();

  virtual void onClientConnect();
#if !defined(__EMSCRIPTEN__)
  virtual void onServerConnect();
  virtual void onWebsocketUpgrade(http::request<http::string_body> &req);
#endif
  virtual void onError(string_view error);
  virtual void onRx(cereal::BinaryInputArchive &rx);
  virtual void onEof();

  void startWebsocketClient(string const &host, string const &port, string const &uri);

  virtual void stop();

  void txPkt(ostringstream &txs);

#if !defined(__EMSCRIPTEN__)
  void asyncRxForever();
  void txFlush();
#endif

  template<typename... Args>
  void txCereal(Args &&... args)
  {
    ostringstream txs(std::ios::binary);
    {
      cereal::BinaryOutputArchive arch(txs);
      arch(std::forward<Args>(args)...);
    }
    txPkt(txs);
  }

};

#if !defined(__EMSCRIPTEN__)
struct HttpHandlers {
  map<string, function<void(asio::ip::tcp::socket &&, http::request<http::string_body> &)>> ws;
  map<string, function<shared_ptr<http::request<http::string_body>> (http::request<http::string_body> &)>> uri;
};


struct HttpSession : enable_shared_from_this<HttpSession> {
 
  beast::tcp_stream stream;
  string label;
  shared_ptr<HttpHandlers> handlers;
  http::request<http::string_body> rxReq;
  beast::flat_buffer rxBuffer;
  int verbose = 0;

  HttpSession(asio::ip::tcp::socket &&_sock, shared_ptr<HttpHandlers> _handlers);
  virtual ~HttpSession();

  virtual void onServerConnect();
  void asyncRxMain();

  void txEof();
  void handleRequest();

};
#endif



namespace Eigen {

template<class Archive> void serialize(Archive &ar, mat2 &m)
{
  ar(m(0, 0), m(0, 1));
  ar(m(1, 0), m(1, 1));
}

template<class Archive> void serialize(Archive &ar, mat3 &m)
{
  ar(m(0, 0), m(0, 1), m(0, 2));
  ar(m(1, 0), m(1, 1), m(1, 2));
  ar(m(2, 0), m(2, 1), m(2, 2));
}

template<class Archive> void serialize(Archive &ar, mat4 &m)
{
  ar(m(0, 0), m(0, 1), m(0, 2), m(0, 3));
  ar(m(1, 0), m(1, 1), m(1, 2), m(1, 3));
  ar(m(2, 0), m(2, 1), m(2, 2), m(2, 3));
  ar(m(3, 0), m(3, 1), m(3, 2), m(3, 3));
}

template<class Archive> void serialize(Archive &ar, vec2 &m)
{
  ar(m(0), m(1));
}

template<class Archive> void serialize(Archive &ar, vec3 &m)
{
  ar(m(0), m(1), m(2));
}

template<class Archive> void serialize(Archive &ar, vec4 &m)
{
  ar(m(0), m(1), m(2), m(3));
}

}


template<class Archive> void save(Archive &ar, Blob const &b)
{
  ar(U32(b.size()));
  ar.saveBinary(b.data(), b.size());
}

template<class Archive> void load(Archive &ar, Blob &b)
{
  U32 size;
  ar(size);
  if (size > 100000000) throw runtime_error("Silly blob size loading blob");
  b.alloc(size);
  ar.loadBinary(b.data(), size);
}


namespace YAML {

template<class Archive> void save(Archive &ar, YAML::Node const &b)
{
  YAML::Emitter e;
  e << b;
  ar(U32(e.size()));
  ar.saveBinary(e.c_str(), e.size());
}

template<class Archive> void load(Archive &ar, YAML::Node &b)
{
  U32 size;
  ar(size);
  if (size > 10000000) throw runtime_error("Silly string size loading yaml");
  char *buf = new char[size + 1];
  ar.loadBinary(buf, size);
  buf[size] = 0;
  b = YAML::Load(buf);
  delete [] buf;
}

}
