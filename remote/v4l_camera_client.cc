#include "./camera_client.h"
#include <thread>
#include <condition_variable>
#include <sys/file.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#if defined(USE_V4L)
#include <linux/videodev2.h>

/*
  Interface for V4L cameras.
*/

struct V4lBuffer {
  void   *start;
  size_t  length;
};

struct V4lCameraClient : CameraClient {


  int devFd = -1;
  double grabStartTime = 0.0;
  string v4lDeviceName;
  atomic<bool> threadRunning = false;

  std::array<V4lBuffer, 4> buffers;
  size_t nBuffers = 0;

  V4lCameraClient(RemoteCameraConfig const &_config)
   :CameraClient(_config)
  {
  }

  ~V4lCameraClient()
  {
    closeDev();
  }

  void openDev();
  void closeDev();
  int devctl(u_long request, void *arg);

  bool isRunning() override { return threadRunning; }

};

shared_ptr<CameraClient>
CameraClient::mkV4lCameraClient(RemoteCameraConfig const &cameraConfig)
{
  auto c = make_shared<V4lCameraClient>(cameraConfig);
  c->openDev();

  return static_pointer_cast<CameraClient>(c);
}


int V4lCameraClient::devctl(u_long request, void *arg)
{
	int rc;

	do {
		rc = ioctl(devFd, request, arg);
	} while (rc < 0 && errno == EINTR);

	return rc;
}

void V4lCameraClient::openDev()
{
  threadRunning = true;
  std::thread grabThread([this, keepalive=shared_from_this()]() {
    {
      string deviceName(assoc_string_view(cameraConfig.apiProps, "device", ""));

      devFd = open(deviceName.c_str(), O_RDWR | O_NONBLOCK, 0);
      if (devFd < 0) {
        L() << label << ": open " << deviceName << ": " << strerror(errno);
        goto fail;
      }

      L() << label << ": opened";

      struct v4l2_capability cap;
      memset(&cap, 0, sizeof(cap));
      if (devctl(VIDIOC_QUERYCAP, &cap) < 0) {
        L() << label << ": VIDIOC_QUERYCAP: " << strerror(errno);
        goto fail;
      }

      if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
        L() << label << ": not a video capture device";
        goto fail;
      }
      if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
        L() << label << ": doesn't support streaming";
        goto fail;
      }

      struct v4l2_cropcap cropcap;
      memset(&cropcap, 0, sizeof(cropcap));
      cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
      if (devctl(VIDIOC_CROPCAP, &cropcap) < 0) {
        L() << label << ": VIDIOC_CROPCAP: doesn't support cropping (continuing)";
      }
      else {
        struct v4l2_crop crop;
        memset(&crop, 0, sizeof(crop));
        crop.c = cropcap.defrect; /* reset to default */
        if (devctl(VIDIOC_S_CROP, &crop) < 0) {
          if (errno == EINVAL || errno == ENOTTY) {
            // not supported I guess
          }
          else {
            L() << label << ": VIDIOC_S_CROP to default: " << strerror(errno);
            goto fail;
          }
        }
      }

      struct v4l2_format fmt;
      memset(&fmt, 0, sizeof(fmt));
      fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
      fmt.fmt.pix.width       = int(assoc_F(cameraConfig.apiProps, "width", 0.0f));
      fmt.fmt.pix.height      = int(assoc_F(cameraConfig.apiProps, "height", 0.0f));
      fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_MJPEG;
      fmt.fmt.pix.field       = V4L2_FIELD_NONE;
      fmt.fmt.pix.colorspace  = V4L2_COLORSPACE_SRGB;

      if (devctl(VIDIOC_S_FMT, &fmt) < 0) {
        L() << label << ": VIDIOC_S_FMT: " << strerror(errno);
        goto fail;
      }

      struct v4l2_requestbuffers req;
      memset(&req, 0, sizeof(req));
      req.count = buffers.size();
      req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
      req.memory = V4L2_MEMORY_MMAP;
      if (devctl(VIDIOC_REQBUFS, &req) < 0) {
        L() << label << ": VIDIOC_REQBUFS: " << strerror(errno);
        goto fail;
      }

      if (req.count < 2) {
        L() << label << ": VIDIOC_REQBUFS: only got " << req.count;
        goto fail;
      }

      while (nBuffers < req.count) {
        struct v4l2_buffer buf;
        memset(&buf, 0, sizeof(buf));

        buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory      = V4L2_MEMORY_MMAP;
        buf.index       = nBuffers;

        if (devctl(VIDIOC_QUERYBUF, &buf) < 0) {
          L() << label << ": VIDIOC_QUERYBUF: " << strerror(errno);
          goto fail;
        }
        buffers[nBuffers].length = buf.length;

        buffers[nBuffers].start =
          mmap(nullptr, buf.length,
              PROT_READ | PROT_WRITE, MAP_SHARED,
              devFd, buf.m.offset);
        if (buffers[nBuffers].start == MAP_FAILED) {
          L() << label << ": mmap: " << strerror(errno);
          goto fail;
        }
        nBuffers++;
      }

      for (size_t bi = 0; bi < nBuffers; bi++) {
        struct v4l2_buffer buf;

        memset(&buf, 0, sizeof(buf));
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.index = bi;

        if (devctl(VIDIOC_QBUF, &buf) < 0) {
          L() << label << ": VIDIOC_QBUF: " << strerror(errno);
          goto fail;
        }
      }
      v4l2_buf_type streamType = V4L2_BUF_TYPE_VIDEO_CAPTURE;
      if (devctl(VIDIOC_STREAMON, &streamType) < 0) {
        L() << label << ": VIDIOC_STREAMON: " << strerror(errno);
        goto fail;
      }

      L() << label << ": capturing from " << deviceName << " size=" <<
        fmt.fmt.pix.width << "x" << fmt.fmt.pix.height;

      while (!stopRequested) {

        fd_set fds;
        FD_ZERO(&fds);
        FD_SET(devFd, &fds);

        struct timeval tv;
        tv.tv_sec = 1;
        tv.tv_usec = 0;

        int rc = select(devFd + 1, &fds, NULL, NULL, &tv);
        if (rc < 0 && errno == EINTR) {
          continue;
        }
        if (rc < 0) {
          L() << label << ": select: " << strerror(errno);
          goto fail;
        }
        if (rc == 0) {
          L() << label << ": timeout";
          break;
        }

        if (stopRequested) break;

        R frameTs = realtime();

        struct v4l2_buffer buf;
        memset(&buf, 0, sizeof(buf));
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;

        if (devctl(VIDIOC_DQBUF, &buf) < 0) {
          if (errno == EAGAIN) continue;
          L() << label << ": VIDIOC_DQBUF: " << strerror(errno);
          goto fail;
        }

        assert(buf.index < nBuffers);

        rotateBlobChunkId(curChunkId);
        auto mainFrame = VideoFrame();
        mainFrame.format = VFF_JPEG;
        mainFrame.width = fmt.fmt.pix.width;
        mainFrame.height = fmt.fmt.pix.width;;
        mainFrame.orientation = base_orientation;
        mainFrame.blobRef = mkBlob(curChunkId, (U8 *)buffers[buf.index].start, size_t(buf.bytesused));

        if (devctl(VIDIOC_QBUF, &buf) < 0) {
          L() << label << ": VIDIOC_QBUF: " << strerror(errno);
          goto fail;
        }

        {
          std::lock_guard lock(avMutex);
          if (stopRequested) break;
          if (videoQueue.size() < 10) {
            videoQueue.emplace_back(frameTs, mainFrame);
          }
          else {
            L() << label << ": discard compressed frame because videoQueue.size=" << videoQueue.size();
          }
          onNewFrame();
        }
      }
    }
  fail:
    closeDev();
    threadRunning = false;
  });
  grabThread.detach();
}

void V4lCameraClient::closeDev()
{
  while (nBuffers > 0) {
    nBuffers --;
    if (munmap(buffers[nBuffers].start, buffers[nBuffers].length) < 0) {
      L() << label << ": munmap: " << strerror(errno);
    }
  }

  if (!(devFd < 0)) {
    close(devFd);
    devFd = -1;
    L() << label << ": closed";
  }
}



#else


shared_ptr<CameraClient>
CameraClient::mkV4lCameraClient(RemoteCameraConfig const &cameraConfig)
{
  return nullptr;
}

#endif
