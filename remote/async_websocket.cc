#include "./cereal_box.h"

// --------------

AsyncWebsocket::AsyncWebsocket(asio::ip::tcp::socket &&_sock)
  : ws(std::move(_sock))
{
  label = "AsyncWebsocket";
}

AsyncWebsocket::AsyncWebsocket()
  : ws(io_context)
{
  label = "AsyncWebsocket";
}

AsyncWebsocket::~AsyncWebsocket()
{
}

void AsyncWebsocket::onClientConnect()
{
}

void AsyncWebsocket::onServerConnect()
{
  label = label + " from " + repr(beast::get_lowest_layer(ws).socket().remote_endpoint());
  ws.async_accept(
    [this, keepalive=shared_from_this()](beast::error_code error) {
      if (error) {
        L() << label << ": server handshake: " << error.message();
        failed = true;
        onError(error.message());
        return;
      }
      ready = true;
      asyncRxForever();
      txFlush();
    });
}

void AsyncWebsocket::onWebsocketUpgrade(http::request<http::string_body> &req)
{
  label = label + " from " + repr(beast::get_lowest_layer(ws).socket().remote_endpoint());

  ws.set_option(websocket::stream_base::decorator(
      [](websocket::response_type& req) {
        req.set(http::field::user_agent, "Throbol Server");
      }));

  ws.async_accept(
    req,
    [this, keepalive=shared_from_this()](beast::error_code error) {
      if (error) {
        L() << label << ": server upgrade: " << error.message();
        failed = true;
        onError(error.message());
        return;
      }
      ready = true;
      asyncRxForever();
      txFlush();
    });
}

void AsyncWebsocket::asyncRxForever()
{
  if (failed) return;
  ws.async_read(wsRxBuf,
    [this, keepalive=shared_from_this()](beast::error_code error, size_t nr) {
      if (error) {
        if (error == websocket::error::closed || 
            error == asio::error::eof ||
            error == asio::error::operation_aborted) {
          ready = false;
          failed = true;
          onEof();
          return;
        }
        L() << label << ": read: " << error.message();
        ready = false;
        failed = true;
        onError(error.message());
        return;
      }
      if (verbose >= 2) {
        L() << label << ": rx " << nr << " wsRxBuf.size=" << wsRxBuf.size();
      }

      wsRxBuf.commit(nr);
      istream rxStream(&wsRxBuf);
      cereal::BinaryInputArchive rxArch(rxStream);
      onRx(rxArch);
      wsRxBuf.consume(nr);

      asyncRxForever();
    });
}

void AsyncWebsocket::onRx(cereal::BinaryInputArchive &rx)
{
  // override me
}

void AsyncWebsocket::onEof()
{
  // override me
}

void AsyncWebsocket::onError(string_view error)
{
  // override me. A message will already have been printed and failed=true
}

void AsyncWebsocket::startWebsocketClient(string const &host, string const &port, string const &uri)
{
  label += " to " + host + ":" + port;

  
  resolver.async_resolve(asio::ip::tcp::v4(), host, port, 
    [this, keepalive=shared_from_this(), host, port, uri](auto error, auto eps) {
      if (error) {
        Wred() << label << ": name lookup: " << error;
        failed = true;
        onError(error.message());
        return;
      }

      string httpHost = host + ':' + port;

      // Set timeout for TCP connection
      beast::get_lowest_layer(ws).expires_after(std::chrono::seconds(5));

      beast::get_lowest_layer(ws).async_connect(eps.begin()->endpoint(),
        [this, keepalive, httpHost, uri](auto error) {
          if (error) {
            Wred() << label << ": client connect: " << error.message();
            failed = true;
            onError(error.message());
            return;
          }
          // Turn off timeout for TCP connection
          beast::get_lowest_layer(ws).expires_never();
          // Set timeout for handshakes
          websocket::stream_base::timeout opt{
            std::chrono::seconds(5),   // handshake timeout
            websocket::stream_base::none(),        // idle timeout
            false
          };
          ws.set_option(opt);          

          ws.set_option(websocket::stream_base::decorator(
              [](websocket::request_type& req) {
                req.set(http::field::user_agent, "Throbol Client");
              }));

          ws.async_handshake(httpHost, uri, 
            [this, keepalive](beast::error_code error) {
              if (error) {
                L() << label << ": client handshake: " << error.message();
                failed = true;
                onError(error.message());
                return;
              }
              ready = true;
              txFlush();
              // 16MB is the default according to https://www.boost.org/doc/libs/1_80_0/libs/beast/doc/html/beast/ref/boost__beast__websocket__stream/read_message_max/overload1.html
              // ws.read_message_max(16 * 1024 * 1024 * 1024);
              onClientConnect();
              asyncRxForever();
            }
          );
        }
      );
    }
  );
}


void AsyncWebsocket::stop()
{
  txShutdownReq = true;
  txFlush();
}

void AsyncWebsocket::txPkt(ostringstream &txs)
{
  if (failed) return; // but !ready is fine, packets will be queued until it starts
  wsTxQ.push_back(txs.str());
  txFlush();
}

void AsyncWebsocket::txFlush()
{
  if (ready && !failed && !txActive) {
    if (!wsTxQ.empty()) {
      txActive = true;
      auto &d = wsTxQ.front();
      if (verbose >= 2) L() << label << ": tx " << d.size();
      ws.binary(true);
      ws.async_write(asio::buffer(d.data(), d.size()),
        [this, keepalive=shared_from_this()](const boost::system::error_code error, size_t nw) {
          wsTxQ.pop_front();
          txActive = false;
          if (error) {
            L() << label << ": write: " << error.message();
            failed = true;
            return;
          }
          txFlush();
        }
      );
    }
    else if (txShutdownReq) {
      txShutdownReq = false;
      ready = false;
      ws.async_close(websocket::close_code::normal,
        [this, keepalive=shared_from_this()](beast::error_code error) {
          if (error) {
            L() << label << ": async handshake: " << error.message();
            failed = true;
            return;
          }
          onEof();
        }
      );
    }
  }
}
