#pragma once
#include "../src/defs.h"
#if !defined(EMSCRIPTEN_NOTYET)
#include "./protocol.h"

struct SimCameraClient : AsyncWebsocket, AllocTrackingMixin<SimCameraClient> {

  Sheet &sh;
  RemoteCameraConfig cameraConfig;
  R localStartTime;
  F dt;

  vector<VideoFrame> videoFrames;
  vector<AudioFrame> audioFrames;

  SimCameraClient(Sheet &_sh, RemoteCameraConfig const &_cameraConfig);
  ~SimCameraClient();

  void startCameraClient();

  void onClientConnect() override;
  void txRecord();

  size_t tickFromTime(R serverTime, R rxTime, R frameTime);

  void onRx(cereal::BinaryInputArchive &rx) override;

};

#endif
