#include "./cereal_box.h"



AsyncWebsocket::AsyncWebsocket()
{
  label = "AsyncWebsocket";
}

AsyncWebsocket::~AsyncWebsocket()
{
}

void AsyncWebsocket::onClientConnect()
{
  // WRITEME
}

void AsyncWebsocket::asyncRxForever()
{
  if (failed) return;
  abort();
}

void AsyncWebsocket::onRx(cereal::BinaryInputArchive &rx)
{
  // override me
}

void AsyncWebsocket::onEof()
{
  // override me
}


void AsyncWebsocket::onError(string_view error)
{
  // override me. A message will already have been printed and failed=true
}

void AsyncWebsocket::startWebsocketClient(string const &host, string const &port, string const &uri)
{
  label += " to " + host + ":" + port;

  // WRITEME
}


void AsyncWebsocket::stop()
{
  txShutdownReq = true;
  txFlush();
}

void AsyncWebsocket::txPkt(ostringstream &txs)
{
  if (failed) return; // but !ready is fine, packets will be queued until it starts

  // WRITEME
  wsTxQ.push_back(txs.str());
  txFlush();
}

void AsyncWebsocket::txFlush()
{
  // WRITEME
}



#endif