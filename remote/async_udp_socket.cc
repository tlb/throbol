#include "./cereal_box.h"


AsyncUdpSocket::AsyncUdpSocket()
  :sock(io_context)
{
}

AsyncUdpSocket::~AsyncUdpSocket()
{
  sock.close();
}

int AsyncUdpSocket::startUdpServer(string const &bindAddr, string const &bindPort)
{ 
  label = "UDP on "s + bindAddr + ":" + bindPort;
  asio::ip::udp::resolver resolver(io_context);
  boost::system::error_code resolveError;
  // leave this as sync, since it's only used at server startup
  // and async causes trouble with tests
  auto eps = resolver.resolve(asio::ip::udp::v4(), bindAddr, bindPort, resolveError);
  if (resolveError) {
    L() << label << ": resolve: " << resolveError;
    return -1;
  }
  auto ep = eps.begin()->endpoint();
  sock.open(asio::ip::udp::v4());
  sock.bind(ep);
  label = label + " on " + ":" + bindPort;
  onServerBound();
  return 0;
}

void AsyncUdpSocket::onServerBound()
{
}

void AsyncUdpSocket::stop()
{
}


void AsyncUdpSocket::asyncRxCereal(std::function<void(cereal::BinaryInputArchive &rx, asio::ip::udp::endpoint const &src)> &&onRx)
{
  rxBuf.resize(4096);
  sock.async_receive_from(asio::buffer(rxBuf.data(), rxBuf.size()),
    rxSrc,
    [this, onRx=std::move(onRx)](const boost::system::error_code error, size_t nr) mutable {
      if (error) {
        L() << label << ": " << error.message();
        return;
      }
      if (nr == 0) {
        return asyncRxCereal(std::move(onRx));
      }

      istringstream rxs(string(rxBuf.data(), rxBuf.data() + nr));
      cereal::BinaryInputArchive arch(rxs);
      onRx(arch, rxSrc);
    });
}
