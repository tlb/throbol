#include "./camera_client.h"
#include "./pylon_includes.h"
#ifdef __linux__
#include <turbojpeg.h> // http://www.libjpeg-turbo.org/Documentation/Documentation
#endif
#include <sys/file.h>
#include <thread>
#include <condition_variable>

/*
  Interface for Pylon cameras. The driver is a closed-source library 
*/

#if defined(USE_PYLON)
using namespace Pylon;

struct PylonCameraClient : CameraClient {

  IPylonDevice *pdev = nullptr;
  CInstantCamera *cam = nullptr;
  double grabStartTime = 0.0;
  string pylonDeviceName;

  std::mutex compressMutex;
  std::condition_variable compressCond;
  deque<tuple<CGrabResultPtr, double>> compressQueue;

  PylonCameraClient(RemoteCameraConfig const &_config)
   :CameraClient(_config)
  {
  }

  ~PylonCameraClient()
  {
  }

  void startPylonClient();
  void openPylonCamera();

  bool isRunning() override { return cam != nullptr; }

  void pleaseStop() override
  {
    CameraClient::pleaseStop();
    compressCond.notify_all();
  }

};

void CameraClient::enumeratePylonCameras()
{
  CTlFactory &TlFactory = CTlFactory::GetInstance();
  ITransportLayer *tl = TlFactory.CreateTl(CBaslerUsbCamera::DeviceClass());

  DeviceInfoList_t allDevices;
  tl->EnumerateDevices(allDevices);
  auto l = L();
  l << "Devices:\n";
  for (auto &dev : allDevices) {
    l << "name=" << dev.GetFullName() <<
      " vendor=" << dev.GetVendorName() <<
      " model=" << dev.GetModelName() <<
      " serial=" << dev.GetSerialNumber();
  }
}

shared_ptr<CameraClient>
CameraClient::mkPylonCameraClient(RemoteCameraConfig const &cameraConfig)
{
  auto c = make_shared<PylonCameraClient>(cameraConfig);
  c->startPylonClient();

  return static_pointer_cast<CameraClient>(c);
}


void compressJpeg(tjhandle tjh, CGrabResultPtr const &grabResult, u_char *&jBuf, u_long &jSize)
{
  /*
    Called from multiple (libuv-worker) threads, so no static data here
  */
  /*
    tjInitCompress only takes 2 uS.
  */
  jBuf = nullptr;
  jSize = 0;

  u_char *rawBuf = reinterpret_cast<u_char *>(grabResult->GetBuffer());
  size_t width = grabResult->GetWidth();
  size_t height = grabResult->GetHeight();

  auto pixelType = grabResult->GetPixelType();
  if (pixelType == PixelType_BGR8packed) {
    size_t stride = 0;
    if (!grabResult->GetStride(stride)) {
      L() << "pylon grabResult: No stride\n";
      abort();
    }

    /*
      tjCompress takes around 2500 uS for 800x600
      (on an Intel NUC7i7BNH).
    */
    tjCompress2(tjh, rawBuf, width, stride, height, TJPF_BGR, &jBuf, &jSize, TJSAMP_420, 90, 0);
  }
  else if (pixelType == PixelType_YUV422_YUYV_Packed) {

    auto expectSize = tjBufSizeYUV2(grabResult->GetWidth(), 4, grabResult->GetHeight(), TJSAMP_422);
    if (expectSize != grabResult->GetImageSize()) {
      L() << "YUV Format error, expected " << expectSize <<
        " got " << grabResult->GetImageSize();
        return;
    }

    /*
      Pylon delivers it as YUYV, but jpeg-turbo expects separate planes.
      YUV planarization takes about 350 uS
      (on an Intel NUC7i7BNH).
    */

    size_t ySize = width * height;
    size_t uSize = ySize / 2;
    size_t vSize = ySize / 2;
    u_char *yuvBuf = new u_char[ySize + uSize + vSize];

    u_char *yPlane = yuvBuf + 0;
    u_char *uPlane = yuvBuf + ySize;
    u_char *vPlane = yuvBuf + ySize + uSize;
    for (size_t i = 0; i < expectSize; i += 4) {
      *yPlane++ = rawBuf[i + 0];
      *uPlane++ = rawBuf[i + 1];
      *yPlane++ = rawBuf[i + 2];
      *vPlane++ = rawBuf[i + 3];
    }
    /*
      tjCompressFromYUV takes around 2400 uS for 800x600
      (on an Intel NUC7i7BNH).
    */
    if (tjCompressFromYUV(tjh, yuvBuf, width, 4, height, TJSAMP_422, &jBuf, &jSize, 80, 0) < 0) {
      L() << "jCompressFromYUV: " <<  tjGetErrorStr();
      return;
    }
    delete[] yuvBuf;
  }
  else {
    L() << "Unknown pixel type " << pixelType;
  }
}

std::mutex pylonDeviceMutex;

void PylonCameraClient::openPylonCamera()
{

  try {
    CTlFactory &TlFactory = CTlFactory::GetInstance();
    CDeviceInfo di;
    if (auto usbId = assoc_string_view(cameraConfig.apiProps, "usb_id")) {
      di.SetFullName(String_t(string(*usbId).c_str()));
    }
    if (auto dev_serial = assoc_string_view(cameraConfig.apiProps, "dev_serial")) {
      di.SetSerialNumber(String_t(string(*dev_serial).c_str()));
    }

    L() << label << ": Opening Basler camera...\n";
    std::unique_lock lock(pylonDeviceMutex);
    di.SetDeviceClass(BaslerUsbDeviceClass);
    pdev = TlFactory.CreateDevice(di);
    assert(pdev);
    cam = new CInstantCamera(pdev);
    cam->Open();
    L() << label << ": Started device '" << cam->GetDeviceInfo().GetFullName() << "'\n";
    {
      using namespace GenApi;

      INodeMap &camCtl = cam->GetNodeMap();

      if (auto camSettings = assoc_assoc(cameraConfig.apiProps, "cam_settings")) {
        vector<shared_ptr<AssocValue>> settings;
        for (auto it = camSettings; it; it = it->next) {
          settings.push_back(it);
        }
        for (auto itp = settings.rbegin(); itp != settings.rend(); itp++) {
          auto it = *itp;

          try {
            if (it->val.ist<string_view>()) {
              CParameter(camCtl, it->key.c_str()).FromString(it->val.get<string>().c_str());
            }
            else if (it->val.ist<F>()) {
              auto vr = repr(it->val.get<F>());
              CParameter(camCtl, it->key.c_str()).FromString(vr.c_str());
            }
            else {
              L() << "Unknown type in cam_settings: " << it->key; // << " t=" << it->val̆.t;
            }
          }
          catch (GenericException const &ex) {
            L() << label << ": Setting " << it->key << ": " << ex.what();
          }
        }

        try {
          double d = CFloatPtr(camCtl.GetNode("ResultingFrameRate"))->GetValue();
          L() << label << ": ResultingFrameRate=" << d;
        }
        catch (LogicalErrorException const &ex) {
          L() << label << ": Getting ResultingFrameRate: " << ex.what();
          abort();
        }
        try {
          double d = CFloatPtr(camCtl.GetNode("ExposureTime"))->GetValue();
          L() << label << ": ExposureTime=" << d;
        }
        catch (LogicalErrorException const &ex) {
          L() << label << ": Getting ExposureTime: " << ex.what();
          abort();
        }

        try {
          CCommandPtr(camCtl.GetNode("AcquisitionStart"))->Execute();
        }
        catch (LogicalErrorException const &ex) {
          L() << label << ": Triggering AcquisitionStart: " << ex.what();
          abort();
        }
      }
    }

    grabStartTime = realtime();
    if (1) {
      cam->StartGrabbing(GrabStrategy_LatestImages);
    }
    else {
      cam->StartGrabbing();
    }
  }
  catch (GenericException const &ex) {
    L() << label << ": Opening camera: " << ex.what();
    if (cam) cam->Close();
    delete cam;
    cam = nullptr;
  }
}

void PylonCameraClient::startPylonClient()
{
  if (cam) return;
  openPylonCamera();
  if (!cam) return;

  std::thread grabThread([this, keepalive=shared_from_this()] {
    // On non-uv thread
    CGrabResultPtr grabResult;
    double prevFrameTs = 0.0;
    double baseTime = 0.0;
    int64_t baseTicks = 0;
    {
      using namespace GenApi;
      INodeMap &camCtl = cam->GetNodeMap();

      auto tsLatchCmd = CCommandPtr(camCtl.GetNode("TimestampLatch"));
      auto tsLatchVal = CIntegerPtr(camCtl.GetNode("TimestampLatchValue"));

      double bestDur = 1e9;
      for (int synci = 0; synci < 5; synci++) {
        double t0 = realtime();
        tsLatchCmd->Execute();
        int64_t ticks = tsLatchVal->GetValue();
        double t1 = realtime();
        if (synci == 0 || t1 - t0 < bestDur) {
          bestDur = t1 - t0;
          baseTime = t0;
          baseTicks = ticks;
        }
      }
      if (verbose >= 0 || bestDur > 0.001) {
        L() << label << ": Synced in " << bestDur << " baseTime=" << baseTime << " baseTicks=" << baseTicks;
      }
    }

    while (cam->IsGrabbing() && !stopRequested) {
      /*
        If we can't keep up, throttling happens here. The pylon library keeps its own pool of buffers (should be 10)
        and will drop frames if we don't free up grabResult.
      */
      try {
        cam->RetrieveResult(5000, grabResult, TimeoutHandling_Return);
      }
      catch (RuntimeException const &ex) {
        L() << label << ": Grabbing frame: " << ex.what();
        abort();
      }
      catch (TimeoutException const &ex) {
        L() << label << ": Grabbing frame: " << ex.what();
        stopRequested = true;
        break;
      }
      if (!grabResult.IsValid()) {
        L() << label << ": pylon: invalid grab result";
        continue;
      }
      if (stopRequested) {
        break;
      }
      int64_t grabTicks = grabResult->GetTimeStamp();
      double frameTs = baseTime + (double)(grabTicks - baseTicks) * 0.000000001;
      double ts = realtime();
      if (grabResult->GrabSucceeded()) {
        if (verbose >= 3 || (verbose >= 2 && grabResult->GetImageNumber() % 20 == 1)) {
          L() << label << ": Grabbed " <<
            grabResult->GetWidth() << "x" << grabResult->GetHeight() <<
            " imageSize=" << grabResult->GetImageSize() <<
            " imageNumber=" << grabResult->GetImageNumber() <<
            " frameTs=" << frameTs <<
            " diff=" << frameTs - prevFrameTs <<
            " 1/diff=" << 1.0 / (frameTs - prevFrameTs) <<
            " delay=" << ts - frameTs;
        }
        prevFrameTs = frameTs;

        /*
          We want to do the compression & writing on a uv worker thread. We can only enqueue work from the
          main thread, so first we use the asyncQueue to move to the main thread.
        */
        {
          std::lock_guard lock(compressMutex);
          if (compressQueue.size() > 10) {
            // drop frame
            if (verbose >= 0) L() << label << ": dropped frame because compressQueue.size=" << compressQueue.size();
          }
          else {
            compressQueue.emplace_back(grabResult, frameTs);
            compressCond.notify_one();
          }
        }
      }
      else {
        L() << label << ": Grab error: " << grabResult->GetErrorCode() << " " << grabResult->GetErrorDescription();
      }
    }
    try {
      if (cam) {
        L() << label << ": Closing pylon camera\n";
        cam->StopGrabbing();
        cam->DestroyDevice();
        delete cam;
        cam = nullptr;
      }
    }
    catch (RuntimeException const &ex) {
      L() << "Closing camera: " << ex.what();
      cam = nullptr;
    }
  });
  grabThread.detach();

  std::thread compressThread([this, keepalive=shared_from_this()] {

    auto tjh = tjInitCompress();

    while (!stopRequested) {
      CGrabResultPtr grabResult;
      double frameTs;
      {
        std::unique_lock lock(compressMutex);
        compressCond.wait(lock, [this]() { return stopRequested || !compressQueue.empty(); });
        if (stopRequested) break;
        auto &[grabResult1, frameTs1] = compressQueue.front();
        grabResult = std::move(grabResult1);
        frameTs = frameTs1;
        compressQueue.pop_front();
      }
      if (stopRequested) break;

      auto mainFrame = VideoFrame();
      mainFrame.format = VFF_JPEG;
      mainFrame.width = grabResult->GetWidth();
      mainFrame.height = grabResult->GetHeight();
      mainFrame.orientation = base_orientation;

      U8 *jBuf = nullptr;
      size_t jSize = 0;
      compressJpeg(tjh, grabResult, jBuf, jSize);

      /*
        Note that these might not be written in order, but the ChunkFile guarantees atomic writing of
        the entire part + length and will return the correct starting position.
      */

      rotateBlobChunkId(curChunkId);
      mainFrame.blobRef = mkBlob(curChunkId, jBuf, jSize);

      tjFree(jBuf);
      if (stopRequested) break;

      {
        std::unique_lock lock(avMutex);
        if (videoQueue.size() < 10) {
          videoQueue.emplace_back(frameTs, mainFrame);
        }
        else {
          L() << label << ": discard compressed frame because videoQueue.size=" << videoQueue.size();
        }
        onNewFrame();
      }

      // WRITEME
    }

    tjDestroy(tjh);

  });
  compressThread.detach();
}


#else


shared_ptr<CameraClient>
CameraClient::mkPylonCameraClient(RemoteCameraConfig const &cameraConfig)
{
  return nullptr;
}

#endif
