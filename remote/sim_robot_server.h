#pragma once
#include "./protocol.h"
#include "../src/core.h"
#include "../src/emit.h"

#if !defined(EMSCRIPTEN_NOTYET)

struct SimRobotServer : AsyncWebsocket {

  map<string, size_t> valStreamPosByApiInstance;

  enum State {
    S_idle,
    S_loadSheetDone,
    S_acquireWait,
    S_acquireRobotDone,
    S_liveTicker,
    S_done,
    S_error,
  } state{S_idle};

  shared_ptr<Sheet> activeSheet;
  shared_ptr<LiveRobotTicker> liveRobotTicker;

  shared_ptr<set<BlobRef>> chunks;
  int lastTraceTimeSent = 0;
  
  SimRobotServer(asio::ip::tcp::socket &&_sock);
  ~SimRobotServer();

  void startRobot();

  void onEof() override;
  void onRx(cereal::BinaryInputArchive &rx) override;

  void txLog(string const &msg);
  void txUpdate();
  void txChunkList();

  void robotHasStopped();

};

ostream & operator <<(ostream &s, SimRobotServer::State const &a);

#endif
