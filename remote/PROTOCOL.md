# Remote protocol


<!-- See https://plantuml.com/sequence-diagram -->
```plantuml
@startuml
  participant UI as UI
  participant Daemon as D
  participant "Robot\nHardware" as HW 
  participant "Video\nSpooler" as Spool 
  participant Camera as Cam

  UI->D: acquireRobot(robotName)
  Note over D: set owner lock
  D->UI: acquireRobotOk
  UI->D: loadSheet(contents)
  D->UI: loadSheetOk
  UI->D: startRobot
  Note over D: start running sheet
  D->UI: startRobotOk
  D->Spool: record(cameraConfig)
  Spool->Cam: start streaming
  D->HW: startLive
  loop Live
    HW->D: sensor values
    Cam-->Spool: Frame
    Spool-->D: newFrames(blobRef, time)
    D-->UI: chunkLocation(chunkId, hostname)
    Note over D: Compute sheet
    D->HW: actuator values
    D->UI: replay
    Note over UI: recompute & display
    UI-->Spool: fetchBlob
    Spool-->UI: fetchBlobOk
  end
  UI-->D: stopRobot
  D-->UI: robotStopped
@enduml

```

