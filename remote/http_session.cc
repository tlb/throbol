#include "./cereal_box.h"


HttpSession::HttpSession(asio::ip::tcp::socket &&_sock, shared_ptr<HttpHandlers> _handlers)
  : stream(std::move(_sock)),
    handlers(_handlers)
{
  label = "HttpSession";
}

HttpSession::~HttpSession()
{
}

void HttpSession::onServerConnect()
{
  label = label + " from " + repr(beast::get_lowest_layer(stream).socket().remote_endpoint());
  asyncRxMain();
}

void HttpSession::asyncRxMain()
{
  rxReq = {};
  http::async_read(stream, rxBuffer, rxReq,
    [this, keepalive=shared_from_this()](beast::error_code error, size_t nr) {
      if (error) {
        if (error == http::error::end_of_stream) {
          txEof();
          return;
        }
        L() << label << ": " << error.message();
        stream.close();
        return;
      }

      handleRequest();
    });
}

void HttpSession::txEof()
{
  beast::error_code error;
  stream.socket().shutdown(asio::ip::tcp::socket::shutdown_send, error);
  if (error) {
    L() << label << ": close: " << error.message();
  }
}


void HttpSession::handleRequest()
{
  string uri(rxReq.target());
  L() << label << ": request " << rxReq.method() << " " << uri;

  if (websocket::is_upgrade(rxReq)) {
    auto &h = handlers->ws[uri];
    if (h) {
      h(stream.release_socket(), rxReq);
      return;
    }
  }

  auto &h = handlers->uri[uri];
  if (h) {
    auto res = h(rxReq);
    res->keep_alive(rxReq.keep_alive());
    res->prepare_payload();
    http::async_write(stream, *res,
      [keepalive=std::move(res)](beast::error_code error, size_t nw) {

      });
    asyncRxMain();
    return;
  }

  L() << label << ": no uri " << uri;
  auto res = std::make_shared<http::response<http::string_body>>(http::status::bad_request, rxReq.version());
  res->set(http::field::content_type, "text/html");
  res->keep_alive(rxReq.keep_alive());
  res->body() = "Not found";
  res->prepare_payload();

  http::async_write(stream, *res,
    [keepalive=std::move(res)](beast::error_code error, size_t nw) {

    });
  asyncRxMain();
}

