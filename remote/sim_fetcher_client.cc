#include "./sim_fetcher_client.h"

SimFetcherClient::SimFetcherClient()
{
  label = "SimFetcherClient";
}

SimFetcherClient::~SimFetcherClient()
{
}

void SimFetcherClient::onRx(cereal::BinaryInputArchive &rx)
{
  RemoteProtoCmd cmd = RPC_none;
  rx(cmd); 
  if (cmd == RPC_fetchBlobOk) {
    BlobRef blobRef;
    Blob blob;
    rx(blobRef, blob);
    stashBlob(blobRef, blob);
    if (0) saveBlob(blobRef, blob);

    BlobRef *outstanding = blobFetchSent[blobRef];
    if (outstanding) {
      *outstanding = blobRef;
    }
    if (verbose >= 1) L() << label << ": fetchBlobOk " << blobRef;
    fetchesOutstanding --;
    uiPollLive = true;
    maybeFetchSome();
  }
  else if (cmd == RPC_fetchBlobFail) {
    BlobRef blobRef;
    rx(blobRef);
    L() << label << ": fetchBlobFail " << blobRef;
    fetchesOutstanding --;
    maybeFetchSome();
  }
  else if (cmd == RPC_fetchBlobFileOk) {
    U64 chunkId = 0;
    Blob data;
    rx(chunkId, data);
    if (verbose >= 1) L() << label << ": fetchBlobFileOk " << repr_016x(chunkId) << " size=" << data.size();
    saveBlobFile(chunkId, data);
    fetchesOutstanding --;
    maybeFetchSome();
  }
  else if (cmd == RPC_fetchBlobFileFail) {
    U64 chunkId = 0;
    rx(chunkId);
    L() << label << ": fetchBlobFail " << repr_016x(chunkId);
    fetchesOutstanding --;
    maybeFetchSome();
  }
  else if (cmd == RPC_log) {
    string msg;
    rx(msg);
    L() << label << ": " << msg;
  }
  else {
    L() << label << ": unknown cmd " << cmd;
  }
}

void SimFetcherClient::txFetchBlob(BlobRef const &blobRef)
{
  if (!ready) return;
  if (verbose >= 1) L() << label << ": fetchBlob " << blobRef;
  fetchesOutstanding ++;
  txCereal(RPC_fetchBlob, blobRef);
}

void SimFetcherClient::txFetchBlobFile(BlobRef const &blobRef)
{
  if (!ready) return;
  if (verbose >= 1) L() << label << ": fetchBlobFile " << blobRef;
  fetchesOutstanding ++;
  txCereal(RPC_fetchBlobFile, blobRef.chunkId);
}

void SimFetcherClient::maybeFetchSome()
{
  if (!ready) return;
  while (!blobFilesWanted.empty() && fetchesOutstanding < 5) {
    auto blobRef = blobFilesWanted.back();
    blobFilesWanted.pop_back();
    txFetchBlobFile(blobRef);
  }
}

SimFetcherClient *SimFetcherClient::getFetcher(string const &blobHostName)
{
  auto &fetcher = clientByHost[blobHostName];
  if (!fetcher) {
    fetcher = make_shared<SimFetcherClient>();
    fetcher->verbose = 0;
    fetcher->startWebsocketClient(blobHostName, "6914", "/throbol-camera");
  }
  else if (fetcher->failed) {
    fetcher = nullptr;
  }
  return fetcher.get();
}

void SimFetcherClient::fetchBlob(BlobRef const &blobRef, BlobRef *writeTo)
{
  BlobRef *&outstanding = blobFetchSent[blobRef];
  if (!outstanding) {
    string loc(blobRef.originHost);
    if (loc.empty()) {
      if (1) L() << "Fetch " << blobRef << ": no originHost";
    }
    else {
      auto fetcher = getFetcher(loc);
      if (fetcher && fetcher->ready && fetcher->fetchesOutstanding < 5) {
        outstanding = writeTo;
        fetcher->txFetchBlob(blobRef);
      }
    }
  }
}

void SimFetcherClient::fetchBlobFiles()
{
  for (auto [name, fetcher] : clientByHost) {
    if (fetcher) {
      fetcher->maybeFetchSome();
    }
  }
}

void SimFetcherClient::fetchBlobFile(BlobRef blobRef)
{
  string loc(blobRef.originHost);
  if (loc.empty()) {
    L() << "Fetch " << blobRef << ": no originHost";
    return;
  }
  auto fetcher = getFetcher(loc);
  if (fetcher) {
    fetcher->blobFilesWanted.push_back(blobRef);
  }
}

void SimFetcherClient::closeAll()
{
  for (auto &[name, fetcher] : clientByHost) {
    if (fetcher) {
      fetcher->stop();
      fetcher = nullptr;
    }
  }
}
