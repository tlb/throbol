#include "../src/test_utils.h"
#include "./sim_robot_client.h"
#include "./sim_robot_server.h"
#include "./sim_fetcher_client.h"
#include "./sim_camera_server.h"


TEST_CASE("SimRobotClient-Server works", "[remote]")
{
  io_context.restart();
  string testPort = "6916";

  auto handlers = make_shared<HttpHandlers>();
  handlers->ws["/test/throbol-robot"] = [](asio::ip::tcp::socket &&sock, http::request<http::string_body> &req) {
    auto t = make_shared<SimRobotServer>(std::move(sock));
    t->onWebsocketUpgrade(req);
  };

  auto s = make_shared<AsyncTcpListener>(sessionStarter<HttpSession>(handlers));
  s->verbose = 1;
  s->acceptOnce = true;
  REQUIRE(s->startTcpListener("127.0.0.1", testPort) == 0);


  Sheet sh;
  REQUIRE(sh.load({"examples/test-api.tb"}));
  sh.compile(Emit_ui);
  REQUIRE(checkSheet(sh));

  auto client = make_shared<SimRobotClient>(sh.compiled);
  client->verbose = 1;
  client->startWebsocketClient("127.0.0.1", testPort, "/test/throbol-robot");

  L() << "Start io_context\n";
  io_context.run_for(std::chrono::milliseconds(1500));
  CHECK(client->state == SimRobotClient::S_done);
  client->stop();
  io_context.run_for(std::chrono::milliseconds(100));

}

