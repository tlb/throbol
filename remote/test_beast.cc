#include "../src/test_utils.h"
#include "./cereal_box.h"
#include "../src/vbmainloop.h"

struct AsyncTestStream : AsyncWebsocket {

  enum Cmd : U32 {
    CMD_ping = 1234,
    CMD_pong
  };

  int pongCount = 0;

  AsyncTestStream(asio::ip::tcp::socket &&_sock)
    : AsyncWebsocket(std::move(_sock))
  {
    label = "AsyncTestStream";
  }

  AsyncTestStream()
    : AsyncWebsocket()
  {
    label = "AsyncTestStream";
  }

  virtual void onRx(cereal::BinaryInputArchive &rx) override
  {
    Cmd cmd;
    rx(cmd);
    switch (cmd) {
      case CMD_ping:
        {
          int seq; string dummy;
          rx(seq, dummy);
          if (verbose >= 1) L() << label << ": ping(" << seq << "," << dummy.size() << ")";
          if (dummy.size() > 0 && dummy[0] == '*') {
            txCereal(CMD_pong, seq, dummy + dummy);
          }
          else {
            txCereal(CMD_pong, seq, dummy + 's');
          }
        }
        break;

      case CMD_pong:
        {
          int seq; string dummy;
          rx(seq, dummy);
          if (verbose >= 1) L() << label << ": pong(" << seq << "," << dummy.size() << ")";
          pongCount++;
          if (seq > 0) {
            txCereal(CMD_ping, seq - 1, dummy);
          }
        }
        break;

      default:
        L() << label << ": bad cmd " << cmd;
    }
  }

};


TEST_CASE("boost::beast works", "[remote]")
{
  io_context.restart();
  constexpr int verbose = 0;

  string testPort = "6916";

  auto handlers = make_shared<HttpHandlers>();
  handlers->ws["/testbeast"] = [](asio::ip::tcp::socket &&sock, http::request<http::string_body> &req) {
    auto t = make_shared<AsyncTestStream>(std::move(sock));
    t->verbose = verbose;
    t->onWebsocketUpgrade(req);
  };

  auto s = make_shared<AsyncTcpListener>(sessionStarter<HttpSession>(handlers));
  s->verbose = verbose;
  s->acceptOnce = true;
  REQUIRE(s->startTcpListener("localhost", testPort) == 0);

  io_context.run_for(chrono::milliseconds(100));

  auto c = make_shared<AsyncTestStream>();
  c->verbose = verbose;
  c->startWebsocketClient("localhost", testPort, "/testbeast");

  c->txCereal(AsyncTestStream::CMD_ping, 20, string("*foobar"));
  c->txCereal(AsyncTestStream::CMD_ping, 200, string("+zorp"));

  io_context.run_for(chrono::milliseconds(2000));
  CHECK(c->pongCount == 222);
  c->stop();

  //io_context.run_for(chrono::milliseconds(100));
  //L() << "End io_context.run 2";

  
}

