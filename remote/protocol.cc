#include "./protocol.h"

ostream & operator <<(ostream &s, const RemoteCameraConfig &a)
{
  return s <<
    "RemoteCameraConfig("s <<
      "camName=" << quoted(a.camName) << ", " <<
      "camType=" << quoted(a.camType) << ", " <<
      "spoolHost=" << quoted(a.spoolHost) << ", " <<
      "apiProps=[WRITEME])";
      // << a.apiProps << ")";
}



