#include "./lightswitch_server.h"

/*
  Cyberpower PDU commands:
  snmpwalk -v 1 -c private 192.168.1.63 iso.3.6.1.4.1.3808.1.1.3.3.3.1.1.2

  snmpset  -v 1 -c private 192.168.1.63 iso.3.6.1.4.1.3808.1.1.3.3.3.1.1.4.${n} i {1|2}
*/

LightswitchServer::LightswitchServer()
{
  label = "LightswitchServer";
}

LightswitchServer::~LightswitchServer()
{
  timeout.cancel();
}

void LightswitchServer::onServerBound()
{
  AsyncUdpSocket::onServerBound();
  asyncTimeout();
  asyncRxMain();
}

void LightswitchServer::asyncTimeout()
{
  timeout.expires_after(chrono::milliseconds(100));
  timeout.async_wait(
    [this](boost::system::error_code error) {
      if (error) {
        L() << label << ": tick: " << error.message();
        return;
      }
      timeoutLights();
      asyncTimeout();
    });
}

void LightswitchServer::asyncRxMain()
{
  asyncRxCereal(
    [this](cereal::BinaryInputArchive &rx, asio::ip::udp::endpoint const &src) {
      R now = realtime();
      RemoteProtoCmd cmd = RPC_none;
      rx(cmd);
      if (cmd == RPC_lightson) {
        string lights;
        rx(lights);
        for (auto light : splitChar(lights, ',')) {
          if (verbose >= 1) L() << "Turn on " + light + "\n";
          auto &req = activeRequests[light];
          req.lastTime = now;
          req.desiredState = true;
        }
        syncLights();
      }
      else {
        L() << label << ": unknown cmd " << cmd;
      }
      asyncRxMain();
    });
}

void LightswitchServer::stop()
{
  timeout.cancel();
  AsyncUdpSocket::stop();
}

void LightswitchServer::setLight(string const &lightName, bool on)
{
  char const *script = "/etc/powerctl/lights";

  if (access(script, X_OK) != 0) {
    L() << "No " << script;
    return;
  }

  vector<const char *> cargs {
    script,
    lightName.c_str(),
    on ? "on" : "off",
    nullptr
  };

  int pid = fork();

  if (pid == 0) {
    close(0);
    dup2(2, 1);
    execvp(script, (char *const *)&cargs[0]);
    exit(2);
  }
  else if (pid == -1) {
    L() << "fork: " << strerror(errno);
  }
  else {

  }

}

void LightswitchServer::syncLights()
{
  for (auto &[lightName, lightReq] : activeRequests) {
    if (lightReq.desiredState != lightReq.actualState) {
      lightReq.actualState = lightReq.desiredState;
      setLight(lightName, lightReq.desiredState);
    }
  }
}

void LightswitchServer::timeoutLights()
{
  R now = realtime();
  bool didSome = false;
  for (auto &[lightName, lightReq] : activeRequests) {
    if (lightReq.desiredState) {
      if (now - lightReq.lastTime > 5.0) {
        lightReq.desiredState = false;
        didSome = true;
      }
    }
  }
  if (didSome) {
    syncLights();
  }
}



