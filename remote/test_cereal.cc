#include "../src/test_utils.h"
#include <cereal/types/vector.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/archives/binary.hpp>

enum class MyEnum:uint16_t { V1,V2,V3 };
struct MyStruct {
  uint32_t i;
  MyEnum e;
  std::vector<float> fs;
};

template <class Archive> void serialize(Archive &ar, MyStruct &m)
{
  ar(m.i, m.e, m.fs);
}

TEST_CASE("cereal out", "[.][perf][env]")
{
  perfReport("cereal speed 3 floats", [] {
    MyStruct data{8941, MyEnum::V2, {15.0f, -8.5f, 0.045f}};
    for (int iter = 0; iter < 1000; iter++) {
      ostringstream os(std::ios::binary);

      {
        cereal::BinaryOutputArchive archive(os);
        archive(data);
      }
      if (iter == 0) {
        //CHECK(os.str().size() == 26);
      }
    }
  }, 1000);

  perfReport("cereal speed 1000 floats", [] {
    MyStruct data{8941, MyEnum::V2, vector<float>(1000, 3.14)};
    for (int iter = 0; iter < 1000; iter++) {
      ostringstream os(std::ios::binary);

      {
        cereal::BinaryOutputArchive archive(os);
        archive(data);
      }
      if (iter == 0) {
        //CHECK(os.str().size() == 26);
      }
    }
  }, 1000);

}
