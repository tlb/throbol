#include "./camera_client.h"
#include "./alsa_includes.h"
#include <thread>


#ifdef USE_ALSA
struct AlsaAudioClient : CameraClient {

  bool running = false;

  double sampleRate = 0.0;

  shared_ptr<std::thread> captureThread;
  snd_pcm_t *capture_handle = nullptr;

  AlsaAudioClient();
  ~AlsaAudioClient() noexcept;

  AlsaAudioClient(AlsaAudioClient const &other) = delete;
  AlsaAudioClient(AlsaAudioClient &&other) = delete;
  AlsaAudioClient &operator=(const AlsaAudioClient &other) = delete;
  AlsaAudioClient &operator=(AlsaAudioClient &&other) = delete;

  bool isRunning() override { return running; }
  void pleaseStop() override;

  void startAlsaClient();

};

AlsaAudioClient::AlsaAudioClient() : CameraClient() {}

AlsaAudioClient::~AlsaAudioClient() noexcept {}

shared_ptr<CameraClient>
CameraClient::mkAlsaAudioClient(RemoteCameraConfig const &cameraConfig)
{
  auto c = make_shared<AlsaAudioClient>();
  c->cameraConfig = cameraConfig;
  c->startAlsaClient();

  return static_pointer_cast<CameraClient>(c);
}

void AlsaAudioClient::startAlsaClient()
{
  int err = 0;
  snd_pcm_hw_params_t *hw_params = nullptr;

  if ((err = snd_pcm_open(&capture_handle, cameraConfig.dev_serial.c_str(), SND_PCM_STREAM_CAPTURE, 0)) < 0) {L()
    L() << "open audio device " << cameraConfig.dev_serial << ": " << snd_strerror(err);
    return;
  }

  if ((err = snd_pcm_hw_params_malloc(&hw_params)) < 0) {
    L() << "malloc audio device: " << snd_strerror(err);
    return;
  }

  if ((err = snd_pcm_hw_params_any(capture_handle, hw_params)) < 0) {
    L() << "Initialize hardware parameter structure: " << snd_strerror(err);
    return;
  }

  if ((err = snd_pcm_hw_params_set_access(capture_handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) {
    L() << "Set access type to RW_INTERLEAVED: " << snd_strerror(err);
    return;
  }

  if ((err = snd_pcm_hw_params_set_format(capture_handle, hw_params, SND_PCM_FORMAT_S16_LE)) < 0) {
    L() << "Set sample format to S16_LE: " << snd_strerror(err);
    return;
  }

  u_int exactRate = 48000;
  if ((err = snd_pcm_hw_params_set_rate_near(capture_handle, hw_params, &exactRate, 0)) < 0) {
    L() << "Set sample rate to " << exactRate << ": " << snd_strerror(err);
    return;
  }
  sampleRate = exactRate;

  int channelCount = 2;
  if ((err = snd_pcm_hw_params_set_channels(capture_handle, hw_params, channelCount)) < 0) {
    L() << "Set channel count to " << channelCount << ": " << snd_strerror(err);
    return;
  }

  int periods = 2;
  snd_pcm_uframes_t frames = 2048;

  if ((err = snd_pcm_hw_params_set_period_size(capture_handle, hw_params, frames, 0)) < 0) {
    L() << "Set period_size to " << frames << ": " << snd_strerror(err);
    return;
  }

  if ((err = snd_pcm_hw_params_set_periods(capture_handle, hw_params, periods, 0)) < 0) {
    L() << "Set periods to " << periods << ": " << snd_strerror(err);
    return;
  }

  if ((err = snd_pcm_hw_params(capture_handle, hw_params)) < 0) {
    L() << "Set hw params: " << snd_strerror(err);
    return;
  }

#if 1
  int dir = 0;
  snd_pcm_hw_params_get_period_size(hw_params, &frames, &dir);
#endif

  L() << "periods=" << periods << " frames=" << frames << " dir=" << dir;

  snd_pcm_hw_params_free(hw_params);

  if ((err = snd_pcm_prepare(capture_handle)) < 0) {
    L() << "prepare audio interface: " << snd_strerror(err);
    return;
  }

  running = true;

  captureThread = make_shared<std::thread>([this, frames, channelCount]() {
    int err;
    vector<char> buf(frames * channelCount * sizeof(int16_t));
    while (running) {
      int nr = snd_pcm_readi(capture_handle, buf.data(), frames);
      /*
        ALSA really does use these return values. See
        http://www.alsa-project.org/alsa-doc/alsa-lib/group___p_c_m.html#ga4c2c7bd26cf221268d59dc3bbeb9c048
      */
      if (nr == -EPIPE) {
        L() << "also audio device overrun";
        if ((err = snd_pcm_prepare(capture_handle)) < 0) {
          L() << "also prepare audio interface after overrun: " << snd_strerror(err);
        }
        continue;
      }
      else if (nr < 0) {
        L() << "also read from device: " + snd_strerror(nr);
      }
      double frameTs = realtime() - ((double)frames / (double)sampleRate);

      vector<char> leftChan(frames * sizeof(int16_t));
      uint16_t *bothData = (uint16_t *)buf.data();
      uint16_t *lcData = (uint16_t *)leftChan.data();
      for (int i = 0; i < frames; i++) {
        lcData[i] = bothData[i*2];
      }

      AudioFrame frame;
      frame.format = YAFF_S16;
      frame.sampleRate = sampleRate;
      frame.sampleCount = frames;
      frame.blobRef = mkBlob(curChunkId, (U8 *)lcData, frames*sizeof(int16_t));
      audioQueue.emplace_back(frameTs, frame);
      if (parent->onNewFrame) {
        parent->onNewFrame->send();
      }
    }
    snd_pcm_close(capture_handle);
    capture_handle = nullptr;
    running = false;
  });
}

void AlsaAudioClient::pleaseStop()
{
  CameraClient::pleaseStop();
  if (running) {
    captureThread->join();
    L() << "Shut down alsa client\n";
  }
}


#else

shared_ptr<CameraClient>
CameraClient::mkAlsaAudioClient(RemoteCameraConfig const &cameraConfig)
{
  return nullptr;
}

#endif
