#pragma once
#include "./cereal_box.h"
#include "../src/boxed.h"

/*
  These are used between parts of the system, like throbol and throbold.
*/
enum RemoteProtoCmd : U32 {
  RPC_none,
  RPC_log = 1337, // less likely to collide with misframed bytes

  // robot client-server (UI - throbold)
  RPC_loadSheet = 1437,
  RPC_loadSheetOk,
  RPC_loadSheetFail,
  RPC_acquireRobot,
  RPC_acquireRobotOk,
  RPC_startRobot,
  RPC_startRobotOk,
  RPC_stopRobot,
  RPC_robotStopped,
  RPC_updateCellParams,
  RPC_appendReplay,
  RPC_robotStatus,
  RPC_chunkList,
  RPC_updateKbd,

  // camera daemon
  RPC_record = 1537,
  RPC_fetchBlob,
  RPC_fetchBlobOk,
  RPC_fetchBlobFail,
  RPC_fetchBlobFile,
  RPC_fetchBlobFileOk,
  RPC_fetchBlobFileFail,
  RPC_fetchTraceInfo,
  RPC_fetchTraceSeq,
  RPC_videoFrame,
  RPC_audioFrame,

  // light daemon
  RPC_lightson = 1637,
  
};


struct RemoteCameraConfig {
  
  string camName;
  string camType;
  string spoolHost;
  string cellName;
  shared_ptr<AssocValue> apiProps;

};

ostream & operator <<(ostream &s, const RemoteCameraConfig &a);

template<class Archive>
void save(Archive &archive, TypeTag const &a)
{
  archive(U32(a.kind), U32(a.nr), U32(a.nc));
}

template<class Archive>
void load(Archive &archive, TypeTag &a)
{
  U32 kind, nr, nc;
  archive(kind, nr, nc);
  a = TypeTag(TypeKind(kind), nr, nc);
}

template<class Archive>
void save(Archive &archive, BoxedValue a)
{
  archive(a.t);
  if (a.t.ist<F>()) {
    archive(a.get<F>());
  }
  else if (a.t.ist<string_view>()) {
    archive(a.get<string>()); // yes, these are stored as a string rather than a string_view
  }
  else if (a.t.ist<mat>()) {
    F *p = (F *)a.p;
    Index ne = a.t.nr * a.t.nc;
    for (Index i = 0; i < ne; i++) {
      archive(p[i]);
    }
  }
  else if (a.t.ist<Assocp>()) {
    archive(*a.get<shared_ptr<AssocValue>>());
  }
  else if (a.t.kind == TK_undef || a.t.kind == TK_error) {
  }
  else {
    throw runtime_error("Save " + repr(a.t));
  }
}

template<class Archive>
void load(Archive &archive, BoxedValue &a)
{
  TypeTag t;
  archive(t);  
  if (t.ist<F>()) {
    auto p = new F;
    archive(*p);
    a.t = t;
    a.p = p;
  }
  else if (t.ist<string_view>()) {
    auto p = new string;
    archive(*p);
    a.t = t;
    a.p = p;
  }
  else if (t.ist<mat>()) {
    Index ne = t.nr * t.nc;
    auto p = new F[ne];
    for (Index i = 0; i < ne; i++) {
      archive(p[i]);
    }
    a.t = t;
    a.p = p;
  }
  else if (t.ist<Assocp>()) {
    a.t = t;
    a.p = new shared_ptr<AssocValue>(new AssocValue());
    archive(**(shared_ptr<AssocValue> *)(a.p));
  }
  else if (t.kind == TK_undef || t.kind == TK_error) {
  }
  else {
    throw runtime_error("Load " + repr(a.t));
  }
}

template<class Archive>
void save(Archive &archive, AssocValue const &a)
{
  archive(a.key, a.val, a.next);
}

template<class Archive>
void load(Archive &archive, AssocValue &a)
{
  archive(a.key, a.val, a.next);
}

template <typename Archive> void serialize(Archive & ar, RemoteCameraConfig &a)
{
  ar(a.camName, a.camType, a.spoolHost, a.cellName, a.apiProps);
}


template <typename Archive> void serialize(Archive & ar, CF &a)
{
  ar(real(a), imag(a));
}
