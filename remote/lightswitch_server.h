#pragma once
#include "./protocol.h"

struct LightswitchServer : AsyncUdpSocket {

  struct RequestState {
    double lastTime = 0.0;
    bool desiredState = false;
    bool actualState = false;
  };

  map<string, RequestState> activeRequests;

  asio::steady_timer timeout{io_context};

  LightswitchServer();
  ~LightswitchServer();

  void onServerBound() override;
  void asyncTimeout();
  void asyncRxMain();
  void stop() override;

  void powerbarCmd(string const &host, string const &port, string const &cmd);
  void setLight(string const &lightName, bool on);
  void syncLights();
  void timeoutLights();
};


