#include "./sim_camera_client.h"
#if !defined(EMSCRIPTEN_NOTYET)

SimCameraClient::SimCameraClient(Sheet &_sh, RemoteCameraConfig const &_cameraConfig)
  : AsyncWebsocket(),
    sh(_sh),
    cameraConfig(_cameraConfig)
{
  label = "SimCameraClient";
  localStartTime = realtime();
  dt = 0.01f; // FIXME
}

SimCameraClient::~SimCameraClient()
{
}

void SimCameraClient::startCameraClient()
{
  startWebsocketClient(cameraConfig.spoolHost, "6914", "/throbol-camera");
}

void SimCameraClient::onClientConnect()
{
  AsyncWebsocket::onClientConnect();
  txRecord();
}

void SimCameraClient::txRecord()
{
  txCereal(RPC_record, cameraConfig);
}

size_t SimCameraClient::tickFromTime(R serverTime, R rxTime, R frameTime)
{
  R frameRel = frameTime - serverTime; // negative, how old the frame is before being sent
  R localFrameTime = rxTime + frameRel;
  size_t ofs = size_t(floor(max(0.0, localFrameTime - localStartTime) / R(dt)));
  return ofs;
}

void SimCameraClient::onRx(cereal::BinaryInputArchive &rx)
{
  RemoteProtoCmd cmd = RPC_none;
  rx(cmd);
  if (cmd == RPC_audioFrame) {
    R rxTime = realtime();
    R serverTime, frameTime;
    AudioFrame frame;
    string chunkLoc;
    rx(serverTime, frameTime, frame, chunkLoc);
    if (verbose >= 1) L() << label << ": audioFrame " << frame;
    aref(audioFrames, tickFromTime(serverTime, rxTime, frameTime)) = frame;
    uiPollLive = true;
  }
  else if (cmd == RPC_videoFrame) {
    R rxTime = realtime();
    R serverTime, frameTime;
    VideoFrame frame;
    string chunkLoc;
    rx(serverTime, frameTime, frame, chunkLoc);
    if (verbose >= 1) L() << label << ": videoFrame " << frame;
    aref(videoFrames, tickFromTime(serverTime, rxTime, frameTime)) = frame;
    uiPollLive = true;
  }
  else if (cmd == RPC_fetchBlobOk) {
    BlobRef blobRef;
    Blob blob;
    rx(blobRef, blob);
    stashBlob(blobRef, blob);
  }
  else if (cmd == RPC_fetchBlobFail) {
    BlobRef blobRef;
    rx(blobRef);
    L() << label << ": fetchBlobFail " << blobRef;
  }
  else if (cmd == RPC_log) {
    string msg;
    rx(msg);
    L() << label << ": " << msg;
  }
  else {
    L() << label << ": unknown cmd " << cmd;
  }
}

#endif
