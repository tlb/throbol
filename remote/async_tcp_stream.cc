#include "./cereal_box.h"

/*
  See https://www.boost.org/doc/libs/1_80_0/doc/html/boost_asio.html
  and https://www.boost.org/doc/libs/1_80_0/libs/beast/doc/html/index.html

*/

AsyncTcpStream::AsyncTcpStream()
  : sock(io_context)
{
}

AsyncTcpStream::AsyncTcpStream(asio::ip::tcp::socket &&_sock)
  : sock(std::move(_sock))
{
}

AsyncTcpStream::~AsyncTcpStream()
{
  //assert(!txActive);
  label += " (destroyed)";
}

void AsyncTcpStream::startClient(string const &rHost, string const &rPort)
{
  label = label + " to " + rHost + ":" + rPort;

  static asio::ip::tcp::resolver resolver(io_context);
  resolver.async_resolve(asio::ip::tcp::v4(), rHost, rPort,
    [this, keepalive=shared_from_this()](auto error, auto eps) {
      if (error) {
        L() << label << ": resolve: " << error;
        return;
      }
      if (verbose >= 1) L() << label << ": connecting...";
      asio::async_connect(sock, eps,
        [this, keepalive=shared_from_this()](auto error, auto const &endpoint) {
          if (error) {
            L() << label << ": " << error.message();
            failed = true;
            onError(error.message());
            return;
          }
          if (verbose >= 1) L() << label << ": connected to " << endpoint;

          sock.set_option(asio::ip::tcp::no_delay(true));
          onClientConnect();
        }
      );
    }
  );
}


void AsyncTcpStream::stop()
{
  if (sock.is_open()) {
    if (verbose >= 1) L() << label << ": stop";
    sock.close();
  }
  ready = false;
}


void AsyncTcpStream::asyncRxUntil(string const &separator, std::function<void(string const &s)> &&onRx)
{
  if (verbose) L() << label << ": read until " << shellEscape(separator);
  asio::async_read_until(sock, rxStreambuf, separator, 
    [this, onRx=std::move(onRx), keepalive=shared_from_this()](boost::system::error_code error, size_t nr) {
      if (error) {
        if (verbose >= 0) L() << label << ": " << error.message();
        if (error == asio::error::eof) {
          onEof();
          failed = true;
          sock.close();
          return;
        }
        failed = true;
        onError(error.message());
        sock.close();
        return;
      }
      if (verbose) L() << label << ": read " << nr;

      string str(asio::buffers_begin(rxStreambuf.data()), asio::buffers_begin(rxStreambuf.data()) + nr);
      rxStreambuf.consume(nr);
      onRx(str);
    });
}

void AsyncTcpStream::asyncRxCount(size_t toRead, std::function<void(string const &s)> &&onRx)
{
  if (verbose) L() << label << ": read exactly " << toRead;
  asio::async_read(sock, rxStreambuf, asio::transfer_exactly(toRead), 
    [this, onRx=std::move(onRx), keepalive=shared_from_this()](boost::system::error_code error, size_t nr) {
      if (error) {
        if (verbose >= 0) L() << label << ": " << error.message();
        if (error == asio::error::eof) {
          onEof();
          failed = true;
          sock.close();
          return;
        }
        failed = true;
        onError(error.message());
        sock.close();
        return;
      }

      string str(asio::buffers_begin(rxStreambuf.data()), asio::buffers_begin(rxStreambuf.data()) + nr);
      rxStreambuf.consume(nr);

      onRx(str);
    });
}

void AsyncTcpStream::onClientConnect()
{
}

void AsyncTcpStream::onServerConnect()
{
  label = label + " from " + repr(sock.remote_endpoint());
}

void AsyncTcpStream::onError(string_view error)
{
}

void AsyncTcpStream::onEof()
{
}

void AsyncTcpStream::txRaw(string const &it)
{
  txq.push_back(it);
  txFlush();
}

void AsyncTcpStream::txRaw(string &&it)
{
  txq.push_back(std::move(it));
  txFlush();
}

void AsyncTcpStream::txFlush()
{
  if (!failed && !txActive && !txq.empty()) {
    txActive = true;
    vector<asio::const_buffer> bufs;
    auto pktCount = min(size_t(10), txq.size());
    for (size_t i = 0; i < pktCount; i++) {
      auto &d = txq[i];
      bufs.push_back(asio::const_buffer(d.data(), d.size()));
    }
    async_write(sock, bufs,
      [this, pktCount, keepalive=shared_from_this()](const boost::system::error_code error, size_t nw) {
        for (size_t i = 0; i < pktCount; i++) {
          txq.pop_front();
        }
        txActive = false;
        if (error) {
          L() << "write: " << error.message();
          failed = true;
          return;
        }
        txFlush();
      });
  }
}

