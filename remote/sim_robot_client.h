#pragma once
#include "../src/defs.h"
#include "../src/api.h"
#include "./protocol.h"


struct SimRobotClient : AsyncWebsocket, AllocTrackingMixin<SimRobotClient> {

  shared_ptr<CompiledSheet> compiled;
  string robotName;
  string remoteAuth;

  vector<BlobRef> chunkList;

  shared_ptr<ReplayBuffer> replayBuffer;

  std::array<bool, API_KBD_NKEYS> lastTxKbd;

  enum State {
    S_idle,
    S_acquiring,
    S_compiling,
    S_starting,
    S_running,
    S_done,
    S_error,
  };
  State state{S_idle};

  SimRobotClient(shared_ptr<CompiledSheet> const &_compiled);
  virtual ~SimRobotClient();

  void stopRobot();

  void onClientConnect() override;
  void onError(string_view error) override;
  void onEof() override;
  void onRx(cereal::BinaryInputArchive &rx) override;

  bool running();
  bool done();
  bool error();

  void txLoadSheet();
  void txUpdateCellParams(CellIndex cell);
  void txUpdateKbd(bool *down);

  int getRunningTick();

};

ostream & operator <<(ostream &s, SimRobotClient::State const &a);
