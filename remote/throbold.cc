#include "./sim_robot_server.h"
#include "./sim_camera_server.h"
#include "./lightswitch_server.h"
#include "./camera_client.h"
#include "./pylon_includes.h"
#include <getopt.h>
#include <sys/mman.h>
#include <sys/wait.h>

struct DaemonOpt {
  int verbose = 1;
  int acceptOnce = 0;
  int enumeratePylonCameras = 0;
  string onlyServer;
  char const *bindAddr = nullptr;
};

DaemonOpt opt;

void usage()
{
  L() << R"(usage: throbold [-C dir] [-v] [--once] [-s server] [-b addr]
  -v: increase verbosity
  -s: only run robot|camera|light|av server
  -b: bind addr (default 0.0.0.0)
  --once: exit after one connection
)";
}

void asyncReap()
{
  static boost::asio::signal_set onSigchild(io_context, SIGCHLD);

  onSigchild.async_wait(
    [](const boost::system::error_code &error, int signal) {
      if (error) return;
      int status = 0;
      auto pid = ::wait(&status);
      if (pid != -1) {
        if (status != 0) {
          L() << "Child " << pid << " exited " << status;
        }
      }
      asyncReap();
    }
  );
}


int main(int argc, char * const *argv)
{
  if (1) {
    struct sigaction act;
    memset(&act, 0, sizeof(act));
    act.sa_handler = SIG_IGN;
    sigaction(SIGPIPE, &act, nullptr);
  }

  if (1) {
    asyncReap();
  }

  struct option longopts[] = {
    {"once", no_argument, &opt.acceptOnce, 1},
    {"enumerate-pylon-cameras", no_argument, &opt.enumeratePylonCameras, 1},
    {nullptr, 0, nullptr, 0}
  };

  opt.bindAddr = "0.0.0.0";

  int ch;
  while ((ch = getopt_long(argc, argv, "C:s:b:", longopts, nullptr)) != -1) {
    switch (ch) {
      case 'C':
        if (chdir(optarg) < 0) {
          L() << string(optarg) << ": " << strerror(errno);
          return 1;
        }
        break;

      case 'v':
        opt.verbose ++;
        break;

      case 's':
        opt.onlyServer = optarg;
        break;

      case 'b':
        opt.bindAddr = optarg;
        break;

      case 0:
        break;

      default:
        usage();
        return 2;
    }
  }

  cerr.precision(3);
  // Cope with odri_control_interface logging to stdout
  dup2(1, 2);

#ifdef USE_PYLON
  if (opt.verbose) L() << "Init Pylon camera library";
  Pylon::PylonAutoInitTerm autoInitTerm;

  if (opt.enumeratePylonCameras) {
    CameraClient::enumeratePylonCameras();
    return 0;
  }
#endif

  auto handlers = make_shared<HttpHandlers>();

  if (opt.onlyServer.empty() || opt.onlyServer == "robot") {
    handlers->ws["/throbol-robot"] = [](asio::ip::tcp::socket &&sock, http::request<http::string_body> &req) {
      auto t = make_shared<SimRobotServer>(std::move(sock));
      t->onWebsocketUpgrade(req);
    };

    if (opt.verbose) L() << "Start robot server on TCP " << opt.bindAddr << ":6912";
    auto s = make_shared<AsyncTcpListener>(sessionStarter<HttpSession>(handlers));
    s->acceptOnce = opt.acceptOnce;
    if (s->startTcpListener(opt.bindAddr, "6912") < 0) {
      L() << "Failed to start robot server";
      return 1;
    }
  }

  if (opt.onlyServer.empty() || opt.onlyServer == "camera" || opt.onlyServer == "av") {
    handlers->ws["/throbol-camera"] = [](asio::ip::tcp::socket &&sock, http::request<http::string_body> &req) {
      auto t = make_shared<SimCameraServer>(std::move(sock));
      t->onWebsocketUpgrade(req);
    };
    if (opt.verbose) L() << "Start camera server on TCP " << opt.bindAddr << ":6914";
    auto s = make_shared<AsyncTcpListener>(sessionStarter<HttpSession>(handlers));
    s->acceptOnce = opt.acceptOnce;
    if (s->startTcpListener(opt.bindAddr, "6914") < 0) {
      L() << "Failed to start camera server";
      return 1;
    }
  }

  shared_ptr<LightswitchServer> lightServer;
  if (opt.onlyServer.empty() || opt.onlyServer == "light" || opt.onlyServer == "av") {
    if (opt.verbose) L() << "Start light server on UDP " << opt.bindAddr << ":6916";
    lightServer = make_shared<LightswitchServer>();
    if (lightServer->startUdpServer(opt.bindAddr, "6916") < 0) {
      L() << "Failed to start light server";
      return 1;
    }
  }

  if (opt.verbose) L() << "Start io_context";

  if (1) {
    int rc = mlockall(MCL_CURRENT|MCL_FUTURE);
    if (rc < 0) {
      L() << "mlockall: " << strerror(errno) << " (proceeding anyway)";
    }
  }

  io_context.run();
  if (opt.verbose) L() << "End io_context";
  return 0;
}
