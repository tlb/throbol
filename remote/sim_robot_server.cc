#include "./sim_robot_server.h"
#include "../src/live_robot_ticker.h"


SimRobotServer::SimRobotServer(asio::ip::tcp::socket &&_sock)
  : AsyncWebsocket(std::move(_sock))
{
  label = "SimRobotServer";
  chunks = make_shared<set<BlobRef>>();
}

SimRobotServer::~SimRobotServer()
{
  if (0) L() << label << ": destructor\n";
}

void SimRobotServer::onEof()
{
  if (liveRobotTicker) liveRobotTicker->lrt.pleaseStopFromUi();
}

void SimRobotServer::txLog(string const &msg)
{
  L() << label << ": " << msg;
  if (!failed) txCereal(RPC_log, msg);
}

void SimRobotServer::startRobot()
{
  assert(activeSheet);
  liveRobotTicker = make_shared<LiveRobotTicker>(static_pointer_cast<SimRobotServer>(shared_from_this()));

  auto compiled1 = activeSheet->compiled; // because this can get nulled elsewhere and we need it

  if (!liveRobotTicker->startLive()) {
    for (auto const &api : compiled1->apis) {
      for (auto &e : api->errors) {
        txLog(e);
      }
    }
    txCereal(RPC_robotStopped);
    state = S_error;
    if (liveRobotTicker) liveRobotTicker->stopLive();
    return;
  }

  txCereal(RPC_startRobotOk, liveRobotTicker->trace->startTime);
  state = S_liveTicker;
}

void SimRobotServer::robotHasStopped()
{
  txChunkList();
  liveRobotTicker = nullptr;
  activeSheet = nullptr;
  state = S_done;
  if (!failed) txCereal(RPC_robotStopped);
}

void SimRobotServer::onRx(cereal::BinaryInputArchive &rx)
{
  RemoteProtoCmd cmd = RPC_none;
  rx(cmd);
  if (verbose) L() << label << ": onRx cmd=" << cmd;
  if (cmd == RPC_loadSheet) {
    assert(!activeSheet);
    string sheetName, contents;
    activeSheet = make_shared<Sheet>();
    auto &sh = *activeSheet;
    U32 nPages = -1;
    rx(nPages);
    sh.nPages = nPages;
    sh.fileNameByPage.resize(nPages);
    sh.includeNameByPage.resize(nPages);
    sh.yamlByPage.resize(nPages);
    sh.includePosByPage.resize(nPages, vec2(0, 0));
    sh.includeParentByPage.resize(nPages, nullPageIndex);
    for (PageIndex page = 0; page < nPages; page++) {
      string pageFileName;
      string pageContents;
      rx(pageFileName);
      rx(pageContents);
      if (!sh.undumpPage(pageContents, page)) {
        L() << label << ": Error loading " << pageFileName;
        txCereal(RPC_loadSheetFail);
        state = S_error;
        return;
      }
    }
    sh.updateSheetFromPagesYaml();

    map<string, string> expectedApiVersions;
    rx(expectedApiVersions);

    L() << label << ": loadSheet " << shellEscape(sheetName) << " expectedApiVersion=" << expectedApiVersions;
    sh.compile(Emit_hw);
    if (!sh.compiled->checkExpectedApiVersions(expectedApiVersions,
      [this](string const &err) {
        txLog(err);
      })) {
      txCereal(RPC_loadSheetFail);
      state = S_error;
      return;
    }
    for (auto const &api : sh.compiled->apis) {
      api->chunks = chunks;
    }

    auto errors = sh.errors();
    auto actualApiSchemas = sh.compiled->getApiSchema();
    txCereal(RPC_loadSheetOk, errors, actualApiSchemas);

    state = S_loadSheetDone;
  }
  else if (cmd == RPC_acquireRobot) {
    string robotName, remoteAuth;
    rx(robotName, remoteAuth);

    L() << label << ": acquireRobot " << robotName;
    // WRITEME
    /*
      We probably want to keep a sqlite file open representing
      currently held locks
    */
    txCereal(RPC_acquireRobotOk);

    state = S_acquireRobotDone;
  }
  else if (cmd == RPC_startRobot) {
    if (!activeSheet) return;
    startRobot();
  }
  else if (cmd == RPC_stopRobot) {
    if (liveRobotTicker) {
      auto oldState = liveRobotTicker->lrt.state;
      liveRobotTicker->lrt.pleaseStopFromUi();
      L() << label << ": Requested stop " << oldState << " -> " << liveRobotTicker->lrt.state;
    }
  }
  else if (cmd == RPC_updateCellParams) {
    if (!activeSheet) return;
    auto &sh = *activeSheet;
    string cellName;
    vector<F> values;
    rx(cellName);
    rx(values);
    auto cell = sh.getCell(cellName);
    if (cell == nullCellIndex) {
      txLog("No cell named " + cellName);
      return;
    }
    auto &cellParams = sh.paramsByCell[cell];
    if (cellParams.size() != values.size()) {
      txLog("Param count mismatch in " + cellName + ": " + repr(values.size()) + " / " + repr(cellParams.size()) +
        " continuing");
    }

    for (size_t i = 0; i < cellParams.size() && i < values.size(); i++) {
      auto paramIndex = cellParams[i];
      if (paramIndex == nullParamIndex) {
        txLog("Missing param in " + cellName + ": " + repr(i));
        continue;
      }
      // It should be only the params in the trace that are used. But also set the copy
      // in the sheet for the avoidance of doubt
      sh.params.mutValueByParam(paramIndex) = values[i];
      if (liveRobotTicker && liveRobotTicker->trace) liveRobotTicker->trace->params.mutValueByParam(paramIndex) = values[i];
      //txLog("Param " + cellName + " " + repr(paramIndex) + " " + repr(values[i]));
    }

    //txLog("rx updateCellParams " + cellName + " " + repr(values));
  }
  else if (cmd == RPC_updateKbd) {
    if (!activeSheet) return;
    auto &sh = *activeSheet;
    std::array<bool, API_KBD_NKEYS> down;
    rx(down);
    for (auto const &api : sh.compiled->apis) {
      api->setKbd(down);
    }
  }
  else if (cmd == RPC_log) {
    string msg;
    rx(msg);
    L() << label << ": " << msg;
  }
  else {
    L() << label << ": unknown cmd " << cmd;
  }
}


void SimRobotServer::txUpdate()
{
  if (!activeSheet || !liveRobotTicker || failed) return;
  auto &sh = *activeSheet;
  auto &tr = *liveRobotTicker->trace;
  if (verbose >= 2) L() << label << ": txUpdate\n";

  bool doSend = false;
  ostringstream txs(std::ios::binary);
  {
    cereal::BinaryOutputArchive arch(txs);
    arch(RPC_appendReplay);
    std::unique_lock lock(tr.replayBuffer->replayMutex);
    for (auto const &api : sh.compiled->apis) {
      auto &replays = tr.replayBuffer->replayByApiInstance[api->apiInstance];
      auto &valStreamPos = valStreamPosByApiInstance[api->apiInstance];
      // Careful here: apiId may not be the same between client & server. Have to use apiInstance
      while (valStreamPos < replays.size()) {
        arch(true, api->apiInstance, U32(valStreamPos), replays[valStreamPos]);
        valStreamPos++;
        doSend = true;
      }
      if (verbose >= 2) L() << label << ": txUpdate " << api->apiInstance << " valStreamPos=" << valStreamPos;
    }
    arch(false);
  }
  if (doSend) {
    txPkt(txs);
  }

}

void SimRobotServer::txChunkList()
{
  if (!chunks) return;
  vector<BlobRef> chunkList;
  for (auto &it : *chunks) {
    chunkList.push_back(it);
  }
  if (!failed) txCereal(RPC_chunkList, chunkList);
}

ostream & operator <<(ostream &s, SimRobotServer::State const &a)
{
  switch (a) {
    case SimRobotServer::S_idle: return s << "idle";
    case SimRobotServer::S_loadSheetDone: return s << "loadSheetDone";
    case SimRobotServer::S_acquireWait: return s << "acquireWait";
    case SimRobotServer::S_acquireRobotDone: return s << "acquireRobotDone";
    case SimRobotServer::S_liveTicker: return s << "liveTicker";
    case SimRobotServer::S_done: return s << "done";
    case SimRobotServer::S_error: return s << "error";
    default: return s << "(" << int(a) << ")";
  }
}
