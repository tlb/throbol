# TODO

## Optimization
 - policyTerrain should take the average across all seeds, rather than showing nSeeds policyTerrain surfaces

## webgpu:
  - [x] get working in browser
  - [x] Manage texture trace cache
  - Show debugs for remaining objects, like DrawOp, SymbolSet, VideoFrame
  - Support cut & paste of text
  - ArcArrow has lost its point. Maybe its color too.
  - Seem to have lost special font characters
  - [x] Why the preventDefault warnings?
  - [x] Fix Cmd-Q
  - All dark/light in shader?
  - [x] Headings
  - [x] Make tubes be a canned vertex list.
  - Time cursor can be faint depending on pixel alignment. Do anti-aliasing better.
  - Avoid wgpuBufferGetSize? Not currently supported in deps/wgpu-native.

## Include
 - Cells can have empty name. No matching on empty names.
  - Or could be nonce names like $001 that aren't shown.
  - empty name is a problem for YAML. Might be the time to switch formats
 - value like `include("./foo.tb")`
  - or `using "./foo.tb"
 - 


## Feedback from Matt:

- could you make the mouse zoom functionality center around where the cursor is? 

- could you make click-dragging the mouse scroll wheel (holding down the mouse scroll wheel) pan around the page?

## General:

- Better arrows, using mesh files
- Implement SaveSheet_precrash?
- e437 as implicit [0,0,0,...1] vector. Or some other way of extracting elements.

- Implement gotocell (starting from Sheet.js, through the C++/WASM code)

- Add rename cell, also renaming all references.
  - could just be a key combo after changing the name.
  - can I use the parser for this? And a similar mechanism to param changes?

- Save aggressively. Like after every change.
  - Save backups (as `.bak`) only on first save
  - Emit to string internally, then write with a single write and ftruncate.
    (So we keep the file open)
  - Alt (bad) idea: append changes, and write clean copy on command-S.
  - Alt idea: notice changes to file on disk, and reload so we can edit side-by-side in XCode.
  - Syntax:
  ```
  foo 3 5
    code
    code
  ```
  ie, one line for `name X Y`. Indented lines for the code.
  We could add other things after the position, like visualization tags

- Toggle to show noodles or not.

- Fix infinite recursion when a circularity is combined with a hypothetical

- [x] Control lights through CyberPower PDU.

- Could I put the whole trace in a GPU buffer and have a geometry shader do the work of rendering graphs?
  - Don't need to custom-generate GLSL. We just have to bind names to the right offsets in the buffer, with `glVertexAttribPointer`.
  - Would help a lot if it works in webgpu.
  - Can I reference previous or next input elements in a geometry shader?
  - Trace copied into gl buffer management.
  - Can I have arbitrary-sized arrays as vertex attributes? (For vecN).
    - But I think I do vecN as N calls to graph a scalar, handling colors in C++.
  - Should I make many gl buffers, basically the column-major layout?

- Read and think through https://tomasp.net/techdims/

- Set up clone hand mujoco sim.
- Set up clone hand interface, direct through grpc.

- Go back and get good demo replays of solo12, upload to website, clear broken ones, link to best.
- Same for dex4.

- curves as params? Need editing UI.

- solo12 facility: VPN for remote use

- some kind of cloud hub for websocket connections from outside to robots
- security audit for malicious data into:
- SimRobotServer
- SimFetcherServer
- SimCameraServer?
- code
- api spec. eg, connecting to silly IP addresses

- solo12 example:
  - more behaviors
    - [x] sit up
  - optimize behaviors
    - smoother walk
    - faster turn
    - faster strafe

- Language semantics

  - [ ] Have include be part of the language
    - A cell `include("./foo.tb")` both includes it and defines its
      visual offset. (probably +1, -1 relative to parent)
      - this is probably a top-level thing in the parser
      - Need an icon for such a cell
      - Moving an include cell has to move every cell in that sheet.
        So we need `vector<PageIndex> includedPageByCell`
        and 
    - The whole supabase/session loading project seems kinda bogus. Just serve static `.tb` files from the website. It could allow URLs from other places. And the C++ code could fetch the files directly instead of preloading a file system so I wouldn't need to parse the include files in JS.

  - [ ] Support foo[dt/2], which averages cur & prev. And in general any fractional dt. dt < 1.0 creates an order dependency, dt >= 1.0 doesn't.

  - [ ] Convolve operator for easy FIR filters.
    Eg `foo@[0.4, 0.3, 0.3]` is like `foo*0.4 + foo[-dt]*0.3 + foo[-2dt]*0.3`
      - Or just `convolve(foo, [...])`?
      - 

  - Support forward-mode diff, allowing deriv(y, x).
    - Usable for inverse kinematics solvers
    - | foo = bar * wug
      | D(foo){D(bar) = 1, D(wug) = 2}
    - instead of
      (foo - foo{bar=bar+eps, wug=wug+2eps}) / eps
    - Consider using triples instead of duals -- see https://arxiv.org/pdf/2210.08572.pdf.
  - sg operator (stop-gradient) for backprop

- UI to rename cell & change all referers
- Maybe should be the default when changing cell name? When would I not want this?

- Test suite
  - Make a deftest for every op & type
  - deftests could be useful as tooltip examples

- Live:
  - [x]make cameras be a separate API
  - extending trace built into SimRobotServer and shared with SheetUi
  - (later) efficient bool type in replays. Group separately into words
  - string type in replays? Could be handy for logging status.
    - alternatively: make sets work. It's just painful when they change representation size.

- Infrastructure
  - [x] Switch to OBJ from STL, using tinyobjloader. Makes it consistent with mujoco.
  - Use https://nlopt.readthedocs.io/en/latest/ instead of CppOptimization
    - Bite the bullet and require threads?
    - 

- Reliability
  - sometimes it takes a while to start Pylon cameras, and there are several USB errors from the kernel
  - Solo12 api should report errors from the actuators back to the UI
  - 

- A richer behaviortree example.

- In EmitMode::Emit_hw, can I skip all DrawOp cells?

- Mode to lay out L-R in depdendency order, with different color noodles for delayed refs

- Specify a time for policyTerrain

- Semantics for cell names shadowing others in include files?

- Document live simrobotclient/server and camera architecture

- [0xbad1dea] Pointer-free cellvals:
  - symbols become a bitmap
  - store DrawOps in an array in place. The DrawOp type needs a size.
  - instead of pointers, just use a relative offset

- Filter design package
  - generate filter coeffs
  - Bode plots directly from coeffs
  - implement coeffs

- Redo fourier.tb to be self-contained.

- Hardware api
- [x] Need to close UDP sockets

- Optimization
- [x] Try compiling both a 1x and a wider (32x?) engine.
- [x] Is SIMD being used well?

- Engine
- Included sheets should have a relative location, with a handle there to drag them.
- Then make subordinate sheets be centered around the origin
- Needs to be hierarachical. Data might be:
      ```
      vector<vec2> pageOffsetByPage;
      vector<PageIndex> parentPageByPage;
      ```
- When moving a page handle, we can quickly scan parentPageByPage to determine if each cell is a member or not
- PickItem needs a type for picking a page

- poly(x, p[...]~5).
- Should make a special param editor for the polynomial, showing the curve
- Also spline, piecewise linear, DCT, maybe a couple others
- Get derivatives right

- Rename cell? I should be able to find refs in the AST and replace.

- Add log file of formula changes in case it crashes

- UI
- Why does anything other than [-dt] make the cell go red?
- Time scrubber under video
- Show circular dependencies better. Can be detected at type inference time or compile time.
- New cell (^o) should be on same sheet as nearby cell
- vecN.randn(mean, std) or matNxM.randn
- Does this impl support outboard parameters?
- Zoom in on time window

- Fix:
- [] Pong, which uses 'input float'. Shouldn't it use the videogame api in interactive mode?

- Big
- better vectorization.
- 1x and 4x compilations modes.
- Only operations on F really matter for vectorization

<!-- * Language details -->
- do I need nil (an empty SymbolSet?)
- add log10
- add graphcolor(index) => vec4
- -6^2 seems to be 36, because OOO?
- Should I get rid of Symbol as a concrete type and make everything a SymbolSet?

- System
- Should an API give us a web UI for configuring & mode setting?

- Features:
- [] Quiver plot
- Everything in [Levoy's Spreadsheet with Images](https://graphics.stanford.edu/papers/spreadsheets/spreadsheets-with-figs.pdf)

- Doc:
- Summary of how to solve various problems. Usage of
    - gradients back to params
    - hypotheticals as alt function call
- Reference for Behavior tree system (with theory for purely functional implementation)

- univarm
- Kinematics -> rendering. Write equations equiv to https://github.com/ros-industrial/universal_robot/blob/06d8b9e2f5f86aa54f9f2845f11edbc84e2f951e/ur_description/urdf/ur5.urdf.xacro


- Dynamical systems to demo:
- https://twitter.com/matthen2/status/1388249825167171586

- Support an actual robot
- Universal
- Greppy1
- Dynamixel
- Dex42
- Stretch


- Stretch plan:
- Use stretch_body api
- A daemon (in py2) on the robot listens on a socket and communicates with the StretchApi in throbold

- Echo state
- Make some canned input/feedback matrices
- No, better: a deterministic random process where the seed is specified in Sim. Then it's easy to switch when needed.

- Develop cleaner acrobot controller, maybe with easier plant model

- Automatic differentiation, using similar infrastructure to hypotheticals.
- Want a general jacobian
- Should be able to plug this together with OSQP to build an MPC system
- Gather up constraints from cells. We can parse into
- sqr(x-a)
- l <= x <= u
- 

- Handy functions:

```
l1bound(x, lb, ub) = relu(lb - x) + relu(x - ub)

huber(x) = min(x^2, 2*abs(x)-1)

```

- lambda(t){1 + 2t + 3t^2}
- T_lambda1 (1 arg may be all we need)

- Should be part of CameraClient, to ping the relevant light daemon (can still be UDP)

- api should set hasInput (or something to ensure that isInvariantByCell isn't true)

- Draw a picture of how video streaming works

- Real time
- need a super-simple robot to test on

- Some kind of URDF capture for the Stretch?

- Array of test cases, guiding parameter search
- Show failing tests, and values, with color-coded formulas. Values might be ugly.

- Add new-file, load-file, save-as

## More

- Add more FIR/IIR filters
- high pass, bandpass, 3rd & 4th order
- Make work on types other than F

## Rethinks
- Make the axis arrow use the perm cache, and add a DropOp for it
- Think more clearly about fixed/variable/deletable object allocation.
- Think more clearly about runtime errors like multiplying incompatible matrices. Can I truncate to smallest?
- Better gradient shape for freedom? Gentler inside [-1..1]
- Global (per Cpu) loss gradient???

- Port to imgui_impl_glfw & imgui_impl_osx
- Possibly better input character handling, such as queuing editing control characters.

- Font with more visible - sign? And ~ up high?
- Click on names in formulas to jump to that cell (smoothly)
- Show tubes from picked cell, even when viewed from directly overhead

- Embeddable engine to provide policy for anything

- Native mode that does the whole sheet in one function, suitable for deployment. Only params are indirected.

- Ranking lists? By creating a scalar score, and a sort function that takes 2 vectors of the same length.

- Add more Eigen linear algebra functions

- Cell value display types:
- graph
- constant (just show formula & value, no graph)
- custom
- 2D, 3D. Need STL import. Also finish mat4, vec4 ops.

- [x] Cell editor: CRUD

- [~] Wires between values rather than cells?

- [x] drawSheetCellData: Label current value and Y axis.

- Limit width of formula displayed in sheet view

- Simpler rules for cell size. Maybe just 5 if not time-invariant, 2 if it is.

- [x] Maybe show params to the left of their cell, as if they were first-class.
- [x] Highlight adjustable ones

- Use higher-level Tangent API?
- https://www.tangentwave.co.uk/developer_support/

- [x] Save sheet


- [x] Drag to move cells around

- Hook up to a robot

- Write spreadsheet manifesto

- Think through backprop

- Implement nice-looking dial, test it

- [x] Explain the fancy stuff in emit.h

- Highlight current value, show both past and future with lower alpha.
- Angles: high, 45 (like current), -30.

- Let cells define color of wiring. Default like current.
- Textures on wires to define direction instead of color change? Or smooth variation in color? Or animation???
- Something better for cycle indication
- Visually separate tubes at dst (not src)
- 

- Multiple pages in a sheet?

- Cycle detection & reporting (with visual loop?)

- Cell group activations? Esp for latch
- Param search
- Better lighting. Adjustable? 
- Annotations on page.
- Maybe just render html to pngs to textures

- Separate most sheet logic into a separate class from MainWindow.
  
## Someday

- Update imgui and use glfw, osx backends. Maybe docking.

## Cleanup

## Sheet details

- Prohibit cells named pi, e, ...
- How to handle duplicate named cells
- Test all operators

## Possible simplifying assumptions between spreadsheet and throbol

Many sheets, mostly small

Undotted names are local to a sheet?

Cell is often a value, overlaid with a squiggle graph.

## Array language examples

- https://github.com/google-research/dex-lang
- 

## Spreadsheet-related

- https://treenotation.org/
- https://news.ycombinator.com/item?id=26645253

## Repl-related

- http://mikelevins.github.io/posts/2020-12-18-repl-driven/

## Temporal programming

- https://github.com/aappleby/Metron/blob/master/docs/TemporalTLDR.md


## Demos

- Monty?
- Dexter?
- Baxter?
- Wobbler? http://web.engr.oregonstate.edu/~balasubr/pub/Balasubramanian-Rizzi-Mason-ND-2012.pdf
- Air hockey/foosball/ping pong (2 player!)
- Ballbot? http://robohub.org/designed-to-mingle-rezero-ballbots-smooth-moves-can-handle-a-crowd/
- Triple Pendulum? https://www.youtube.com/watch?v=cyN-CRNrb3E
- Quadcopters? https://www.youtube.com/watch?v=w2itwFJCgFQ
- Differentiable state machines? https://google-research.github.io/self-organising-systems/2022/diff-fsm/
