
default: build build.deps

export YOGA3_DIR := $(realpath .)

include deps/tlbcore/mk/makesystem.inc
include mk/makedefs.inc

TLBCORE_SRC += \
	deps/tlbcore/common/hacks.cc \
	deps/tlbcore/common/hash.cc \
	deps/tlbcore/common/host_debug.cc \
	deps/tlbcore/common/host_profts.cc \
	deps/tlbcore/common/str_utils.cc \
	deps/tlbcore/common/parengine.cc \
	deps/tlbcore/common/packetbuf.cc \
	deps/tlbcore/common/chunky.cc \
	deps/tlbcore/common/asyncjoy.cc \
	deps/tlbcore/numerical/haltonseq.cc \
	deps/tlbcore/numerical/numerical.cc \
	deps/tlbcore/numerical/polyfit.cc \
	deps/tlbcore/numerical/windowfunc.cc

YOGA3_EMIT_SRC += \
	src/emit.cc \
	src/emit_type.cc \
	src/emit_sheet.cc \
	src/emit_ops_beh.cc \
	src/emit_ops_core.cc \
	src/emit_ops_complex.cc \
	src/emit_ops_draw.cc \
	src/emit_ops_filter.cc \
	src/emit_ops_geom.cc \
	src/emit_ops_geom2.cc \
	src/emit_ops_interp.cc \
	src/emit_ops_logic.cc \
	src/emit_ops_math.cc \
	src/emit_ops_math2.cc \
	src/emit_ops_math3.cc \
	src/emit_ops_math4.cc \
	src/emit_ops_math5.cc \
	src/emit_ops_minimize.cc \
	src/emit_ops_misc.cc \
	src/emit_ops_polynomial.cc \
	src/emit_ops_prob.cc \
	src/emit_ops_round.cc \
	src/emit_ops_time.cc \
	src/emit_ops_transcendental.cc \
	src/emit_ops_trig.cc \
	src/emit_ops_windowfunc.cc

YOGA3_COMMON_SRC += \
	$(YOGA3_EMIT_SRC) \
	src/api.cc \
	src/api_schema.cc \
	src/arena.cc \
	src/assoc.cc \
	src/parse.cc \
	src/annotate.cc \
	src/ast.cc \
	src/beh.cc \
	src/blobs.cc \
	src/boxed.cc \
	src/cellvals.cc \
	src/check.cc \
	src/config.cc \
	src/cpu.cc \
	src/draw.cc \
	src/dump.cc \
	src/flow.cc \
	src/hashcode.cc \
	src/minpack_search.cc \
	src/media.cc \
	src/params.cc \
	src/reruns.cc \
	src/scope_math.cc \
	src/sheet.cc \
	src/sheet_undo.cc \
	src/symbol.cc \
	src/token.cc \
	src/trace.cc \
	src/types.cc \
	src/policy.cc \
	src/policy_terrain.cc \
	src/particle_search.cc \
	src/uipoll.cc \
	geom/geom.cc \
	geom/colors.cc \
	geom/gl_buffer.cc \
	geom/wgpu_utils.cc \
	env/mujoco_api.cc \
	remote/protocol.cc \
	remote/sim_fetcher_client.cc \
	remote/sim_robot_client.cc \
	remote/camera_client.cc \
	remote/sim_camera_client.cc \
	src/live_robot_ticker.cc \
	env/solo12_api.cc \
	env/kbd_api.cc \
	env/camera_api.cc \
	env/dex4_api.cc \
	env/test_api.cc \
	env/aloha_api.cc

ifeq ($(BUILD_FOR_WASM),)
YOGA3_COMMON_SRC += \
	remote/async_tcp_stream.cc \
	remote/async_udp_socket.cc \
	remote/async_tcp_listener.cc \
	remote/async_websocket.cc \
	remote/http_session.cc \
	remote/sim_robot_server.cc \
	env/videogame_api.cc \
	env/ssc_network_engine.cc \
	env/univarm_api.cc \
	env/hellostretch_api.cc
else
YOGA3_COMMON_SRC += \
	remote/async_websocket_wasm.cc
endif

ifeq ($(BUILD_FOR_WASM),)
YOGA3_COMMON_SRC += \
	deps/DynamixelSDK/c++/src/dynamixel_sdk/group_bulk_read.cpp \
	deps/DynamixelSDK/c++/src/dynamixel_sdk/group_bulk_write.cpp \
	deps/DynamixelSDK/c++/src/dynamixel_sdk/group_sync_read.cpp \
	deps/DynamixelSDK/c++/src/dynamixel_sdk/group_sync_write.cpp \
	deps/DynamixelSDK/c++/src/dynamixel_sdk/packet_handler.cpp \
	deps/DynamixelSDK/c++/src/dynamixel_sdk/port_handler.cpp \
	deps/DynamixelSDK/c++/src/dynamixel_sdk/protocol1_packet_handler.cpp \
	deps/DynamixelSDK/c++/src/dynamixel_sdk/protocol2_packet_handler.cpp
endif
ifeq ($(UNAME_SYSTEM),Darwin)
YOGA3_COMMON_SRC += \
	deps/DynamixelSDK/c++/src/dynamixel_sdk/port_handler_mac.cpp
endif
ifeq ($(UNAME_SYSTEM),Linux)
YOGA3_COMMON_SRC += \
	deps/DynamixelSDK/c++/src/dynamixel_sdk/port_handler_linux.cpp
endif

IMGUI_SRC += \
	deps/imgui/backends/imgui_impl_wgpu.cpp \
	deps/imgui/backends/imgui_impl_glfw.cpp \
	deps/imgui/imgui.cpp \
	deps/imgui/imgui_draw.cpp \
	deps/imgui/imgui_widgets.cpp \
	deps/imgui/imgui_tables.cpp \
	deps/imgui/imgui_demo.cpp \
	deps/imgui/misc/cpp/imgui_stdlib.cpp

ifeq ($(BUILD_FOR_WASM),)
IMGUI_SRC += deps/glfw3webgpu/glfw3webgpu.c
endif

THROBOL_TEST_SRC += \
	$(YOGA3_COMMON_SRC) \
	src/test_main.cc \
	src/test_core.cc \
	src/test_grads.cc \
	src/test_sheet.cc \
	src/test_debug.cc \
	src/test_callperf.cc \
	src/test_err.cc \
	src/test_utils.cc \
	src/test_env.cc \
	src/test_execlist.cc \
	src/test_trace_db.cc \
	src/test_db_versioning.cc \
	src/test_policy_search.cc \
	src/test_logic.cc \
	src/test_topology.cc \
	src/test_asio.cc \
	src/test_api_schema.cc \
	deps/tlbcore/common/test_chunky.cc \
	remote/test_camera_client.cc \
	remote/test_cereal.cc \
	remote/test_sim_robot.cc \
	remote/axis_camera_client.cc \
	remote/test_beast.cc \
	$(TLBCORE_SRC)

#	remote/test_bitsery.cc
#	remote/test_packetbuf.cc

THROBOL_UI_SRC += \
	$(YOGA3_COMMON_SRC) \
	src/main_window.cc \
	src/celledit.cc \
	src/sheet_ui.cc \
	src/sheet_gl_export.cc \
	src/draw_gl.cc \
	src/draw_terrain_gl.cc \
	src/draw_policy_gl.cc \
	src/draw_mujoco_gl.cc \
	src/terrain_shader_gl.cc \
	src/joybox.cc \
	geom/gl_utils.cc \
	geom/solid_geometry.cc \
	geom/texcache.cc \
	geom/g8.cc \
	geom/shaders.cc \
	geom/vbtext.cc \
	$(TLBCORE_SRC)

ifneq ($(BUILD_FOR_WASM),)
THROBOL_UI_SRC += \
	src/wasm_main.cc
else
THROBOL_UI_SRC += \
	src/sim_main.cc
endif

THROBOL_DAEMON_SRC += \
	$(YOGA3_COMMON_SRC) \
	remote/throbold.cc \
	remote/sim_camera_server.cc \
	remote/axis_camera_client.cc \
	remote/pylon_camera_client.cc \
	remote/v4l_camera_client.cc \
	remote/lightswitch_server.cc \
	$(TLBCORE_SRC)
#	remote/simcamera.cc


HIDTEST_SRC += src/hidtest.cc
	
$(ODIR)/src/exec.o : WARNINGS = -Wimplicit-fallthrough

CXX_HDRS += \
	src/annotation.h \
	src/ast.h \
	src/boxed.h \
	src/celledit.h \
	src/index_set.h \
	src/cellvals.h \
	src/check.h \
	src/core.h \
	src/cpu.h \
	src/defs.h \
	src/draw_gl.h \
	src/draw.h \
	src/emit_macros.h \
	src/emit.h \
	src/hashcode.h \
	src/main_window.h \
	src/policy.h \
	src/scope_math.h \
	src/sheet.h \
	src/sheet_ui.h \
	src/test_utils.h \
	src/token.h \
	src/trace.h \
	src/types.h \

ALL_CXX_SRC = $(sort $(THROBOL_UI_SRC) $(THROBOL_TEST_SRC) $(THROBOL_DAEMON_SRC))


WASM_PRELOAD_FILES = \
	$(wildcard images/*@*.png) \
	$(wildcard assets/fonts/SourceSansPro-Regular.ttf)
	

ifneq ($(BUILD_FOR_WASM),)
build : tbwasm/tb-$(BUILD_FOR_WASM).mjs
else
build : $(BIN)/throbol_test $(BIN)/tb $(BIN)/throbold ./compile_commands.json
endif

test : $(BIN)/throbol_test
	$(BIN)/throbol_test

testnew :: $(BIN)/throbol_test ## Compile and run only the tests tagged with [new]
	$(BIN)/throbol_test "[new]"

testperf :: $(BIN)/throbol_test ## Compile and run only the tests tagged with [perf]
	$(BIN)/throbol_test "[perf]"

testsimperf :: $(BIN)/throbol_test ## Compile and run only the tests tagged with [simperf]
	$(BIN)/throbol_test "[simperf]"

$(BIN)/throbol_test : $(call to_objs, $(THROBOL_TEST_SRC) $(IMGUI_SRC) $(LIB_SRC))
	@echo link $@
	@mkdir -p $(dir $@)
	@$(CXX) -o $@ $+ $(LDFLAGS) $(GUI_LDFLAGS) $(TEST_LDFLAGS)

$(BIN)/tb : $(call to_objs, $(THROBOL_UI_SRC) $(IMGUI_SRC) $(LIB_SRC))
	@echo link $@
	@mkdir -p $(dir $@)
	@$(CXX) -o $@ $+ $(LDFLAGS) $(GUI_LDFLAGS)

$(BIN)/throbold : $(call to_objs, $(THROBOL_DAEMON_SRC) $(LIB_SRC))
	@echo link $@
	@mkdir -p $(dir $@)
	@$(CXX) -o $@ $+ $(LDFLAGS) $(THROBOLD_LDFLAGS)
ifeq ($(UNAME_SYSTEM),Linux)
ifneq ($(CI_SERVER),yes)
	sudo setcap cap_net_raw,cap_net_admin,cap_ipc_lock=eip $@
endif
endif

## WASM output
# End result is to populate ./tbwasm with tb-*.wasm, tb-*.mjs, tb-*.data.
#

TBWASM_OBJS += $(call to_objs, $(THROBOL_UI_SRC))
TBWASM_OBJS += $(call to_objs, $(IMGUI_SRC))
TBWASM_OBJS += $(call to_objs, $(LIB_SRC))

ifneq ($(BUILD_FOR_WASM),)

# also $(ODIR)/tb.wasm $(ODIR)/tb.data
tbwasm/tb-$(BUILD_FOR_WASM).mjs : $(TBWASM_OBJS) $(ODIR)/preload/.timestamp mk/makedefs.inc tbwasm/fixes.sed
	@echo Link tbwasm/tb-$(BUILD_FOR_WASM).mjs tbwasm/tb-$(BUILD_FOR_WASM).wasm tbwasm/tb-$(BUILD_FOR_WASM).data
	@$(CXX) -o $(TMPDIR)/tb-$(BUILD_FOR_WASM).mjs $(TBWASM_OBJS) $(LDFLAGS) $(GUI_LDFLAGS) \
		--preload-file $(ODIR)/preload@/ \
		--exclude-file '*/.timestamp' \
		--emit-symbol-map -g1 \
		-sEXPORT_NAME=tbwasm_export
	# Bugfix
	sed -i.orig -f tbwasm/fixes.sed	$(TMPDIR)/tb-$(BUILD_FOR_WASM).mjs
	diff -u --text --label tbwasm/tb-$(BUILD_FOR_WASM).mjs.orig --label tbwasm/tb-$(BUILD_FOR_WASM).mjs $(TMPDIR)/tb-$(BUILD_FOR_WASM).mjs.orig $(TMPDIR)/tb-$(BUILD_FOR_WASM).mjs >tbwasm/fixes.patch || true
	mv $(TMPDIR)/tb-$(BUILD_FOR_WASM).mjs $(TMPDIR)/tb-$(BUILD_FOR_WASM).wasm $(TMPDIR)/tb-$(BUILD_FOR_WASM).data tbwasm/

$(ODIR)/preload/.timestamp : $(WASM_PRELOAD_FILES)
	@echo copy to $(ODIR)/preload/
	@mkdir -p $(ODIR)/preload/
	@rsync -aR $(WASM_PRELOAD_FILES) $(ODIR)/preload
	@touch $@

build.wasm: tbwasm/tb-$(BUILD_FOR_WASM).mjs
	@echo Copy files to tbwasm/doc
	@mkdir -p tbwasm/doc
	@rsync LANGUAGE.md DOC.md tbwasm/doc/.

else
build.wasm : force
	@env - PATH="$(PATH)" make -j8 build.wasm BUILD_FOR_WASM=singlethread
endif

build.docker.wasm : force
	sudo docker run -t -i -v $(PWD):$(PWD) -w $(PWD) registry.gitlab.com/tlb/throbol/jammy-batteries:latest make -j9 build.wasm

# For Mac we have to compile this as ObjC++. For Linux, it's just C.
ifeq ($(UNAME_SYSTEM),Darwin)
$(ODIR)/deps/glfw3webgpu/glfw3webgpu.o : deps/glfw3webgpu/glfw3webgpu.c | build.deps
	@mkdir -p $(dir $@)
	@echo objc++ $<
	@$(CC) -o $@ -ObjC++ -c -MMD -MF $(@:.o=.d) -MJ $(@:.o=.m.compile_command) $(CFLAGS) $(INCLUDES) -Wno-address-of-temporary $<
endif

$(BIN)/hidtest : $(call to_objs, $(HIDTEST_SRC))
	@echo link $@
	@mkdir -p $(dir $@)
	@$(CXX) -o $@ $+ $(LDFLAGS) $(GUI_LDFLAGS)

$(BIN)/perftest_vectorize: $(ODIR)/test/perftest_vectorize.o
	@echo link $@
	@mkdir -p $(dir $@)
	@$(CXX) -o $@ $+ $(LDFLAGS)

clean ::
	rm -rf build.wasm-singlethread build.dist

clean.solo:
	rm -rf $(ODIR)/build.solo $(ODIR)/install.solo

clean.mujoco:
	rm -rf $(ODIR)/mujoco

include $(YOGA3_DIR)/mk/makerules.inc
include $(YOGA3_DIR)/mk/makedeps.inc

.PHONY: analyze
analyze: $(ALL_CXX_SRC:.cc=.cc.analyze)


perfhist:
	grep 'exec test[-_]plant 100* traces' logs/perflog.txt

tb.sample : $(BIN)/tb
	sample $(BIN)/tb 30 -wait -file $@

%.emit.sample: %.sample
	grep 'NodeEmitter::emit_' $< | sed -e 's/^[ +!:|]*//g' | sort -nr >$@

%.api.sample: %.sample
	grep 'Api::simAdvance' $< | sed -e 's/^[ +!:|]*//g' | sort -nr >$@

throbol_test.simperf-plant.sample: $(BIN)/throbol_test
	rm -f $@
	sample throbol_test 10 -wait -file $@ &
	sleep 0.5
	$(BIN)/throbol_test "[simperf-plant]"
	sleep 15
	code $@

throbol_test.perf-cereal.sample: $(BIN)/throbol_test
	rm -f $@
	sample throbol_test 5 -wait -file $@ &
	sleep 0.5
	$(BIN)/throbol_test "[perf-cereal]"
	sleep 10
	code $@


vb.uiperf-dex4.sample: $(BIN)/tb
	rm -f $@
	sample vb 10 -wait -file $@.raw &
	sleep 0.5
	-$(BIN)/tb --ui-perf-test-mode ../dex4/src/walk.tb
	filtercalltree $@.raw -pruneCount 10 | c++filt >$@
	code $@

vb.uiperf-solo12.sample: $(BIN)/tb
	rm -f $@
	sample vb 10 -wait -file $@.raw &
	sleep 0.5
	-$(BIN)/tb --ui-perf-test-mode examples/solo12-stand.tb
	filtercalltree $@.raw -pruneCount 10 | c++filt >$@
	code $@


install.etc: etc/$(UNAME_HOST)
	sudo rsync -rv etc/$(UNAME_HOST)/. /


viewasm/trig: view/build.arm64/src/emit_ops_trig.sdebug
viewasm/math: view/build.arm64/src/emit_ops_math.sdebug

dockers: docker.jammy-batteries
#dockers: docker.focal-batteries

docker.%: ci/%
	sudo docker build -t registry.gitlab.com/tlb/throbol/$*:latest ci/$*
	sudo docker push registry.gitlab.com/tlb/throbol/$*:latest

ifneq (,$(wildcard $(ODIR)))
-include $(shell find $(ODIR) -name '*.d' 2>/dev/null)
endif
